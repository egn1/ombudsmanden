# Ombudsmanden.dk

Source code for `www.ombudsmanden.dk` version of Obvius CMS.

## Installation

Fetch images using `docker-compose pull`. You should now
be able to run `docker-compose up -d`.

### Prerequisites

`docker` and `docker-compose` must be installed. Additionally, you need
read access to Magenta's Docker Hub repos.

The [Obvius base repo](https://git.magenta.dk/obvius/obvius) must live
in a neighboring folder as it is mounted into the running container.

## Local development

~~The main site is available at https://www.ombud.localhost.magenta.dk/.~~

~~The Børnekontoret site is available at
https://boernekontoret.ombud.localhost.magenta.dk/.~~

See #47889 for more information about populating the dev database with
test documents.

You can rebuild and restart the web image with the following command:

```
docker-compose up --build -d ombud-web
```

The entire process should take no more than ~15-20 seconds and ensures
that css is re-compiled and cache is cleared.

Many changes will propagate immediately without any further action
required; see the `x-obvius-volumes` section of `docker-compose.yml` for
a list of directories that are mounted into the running containers.
Changes to `perl/` and `../obvius` require a `docker-compose restart
ombud-web` as perl modules are loaded on container start.

## Copying certificates

MacOS users may need to add the bogus localhost certificate to their
keychain. The certificate can be found in
`./dev-environment/traefik/certs/`.
