<div class="obvius-editengine obvius-link-decor">
<h2><& /shared/trans, en => 'Restore documents', da => 'Genopret dokumenter' &></h2>
<table class="obvius-editengine-list">
<tbody>
<tr class="obvius-editengine-list">
<td colspan="<% $all_docs ? 5 : 4 %>">
<a href='<% $uri . "&offset=$min_5" %>'>«</a>
<a href='<% $uri . "&offset=$min_1" %>'>&lt;</a>
% for (my $i = 0; $i < ($nr_documents + 1)/20; $i++) {
<a href='<% $uri . "&offset=" . ($i * $count) %>'><% $i + 1 %></a>
% }
<a href='<% $uri . "&offset=$max_1" %>'>&gt;</a>
<a href='<% $uri . "&offset=$max_5" %>'>»</a>
</td>
</tr>
<tr>
<th>
  <a href="<% $sort_uri %>&sort_by=name">
    <& /shared/trans, en => 'Filename', da => 'Filnavn' &>
  </a>
</th>
<th>
  <a href="<% $sort_uri %>&sort_by=id">
    Docid
  </a>
</th>
<th>
  <a href="<% $sort_uri %>&sort_by=date_deleted">
    <& /shared/trans, en => 'Date deleted', da => 'Sletningsdato' &>
  </a>
</th>
<th>
  <a href="<% $sort_uri %>&sort_by=path">
    <& /shared/trans, da => 'Placering før sletning', en => 'Placement before deletion' &>
  </a>
</th>
% if ($all_docs) {
<th>
  <a href="<% $sort_uri %>&sort_by=login">
    <& /shared/trans, en => 'Deleted by', da => 'Slettet af' &>
  </a>
</th>
% }
</tr>
% for my $i (0..$#$docs_to_restore) {
<tr class='<% $i % 2 ? 'obvius-a' : 'obvius-b' %>'>
% my $doc = $docs_to_restore->[$i];
  <td><% $doc->{name} |h %></td>
  <td><% $doc->{id} |h %></td>
  <td><% $doc->{date_deleted} |h %></td>
  <td><% $doc->{path} |h %></td>
% if ($all_docs) {
  <td><% $doc->{login} |h %></td>
% }
  <td>
    <a href="?obvius_command_restore=1&docid=<% $doc->{id} %>&return_link=<% $return_link %>">
      <& /shared/trans, en => 'Restore', da => 'Genopret' &>
    </a>
  </td>
</tr>
% }
</tbody>
</table>
</div>

<%args>
$old_sort_by=>'path'
$sort_by=>undef
$offset=>0
$count=>20
$reverse=>0
$msg=>''
$msgstatus=>''
</%args>

<%init>
$r->notes(inactive_handlingarea=>1);
$r->notes(inactive_path=>1);
$r->notes(inactive_subdocs=>1);
$r->notes(inactive_versions=>1);
$r->notes(inactive_information=>1);
$r->notes(inactive_editing=>1);
$r->notes(inactive_newsboxes=>1);

my $gb = Obvius::GarbageHandler->new($obvius);
my $is_okay_sort = sub { grep { lc $_ eq lc $_[0] } ('path', 'id', 'date_deleted', 'name', 'login') };
$sort_by = undef if !$is_okay_sort->($sort_by);
$old_sort_by = undef if !$is_okay_sort->($old_sort_by);

$reverse = 0 if ($reverse ne "1" && $reverse ne "0");
if ($sort_by eq $old_sort_by) {
     $reverse = int(!$reverse);
} elsif ($sort_by) {
     $reverse = 0;
} else {
     $sort_by = $old_sort_by;
}

$offset = 0 if $offset !~ /^\d+$/;
$count = 20 if $count !~ /^\d+$/;

my $min = sub { $_[0] < $_[1] ? $_[0] : $_[1] };
my $max = sub { $_[0] > $_[1] ? $_[0] : $_[1] };
my $nr_documents = $gb->document_count;

my $min_5 = $max->($offset - 5*$count, 0);
my $min_1  = $max->($offset - $count, 0);
my $max_5 = $min->($offset + 5*$count, int($nr_documents/$count)*$count);
my $max_1 = $min->($offset + $count, int($nr_documents/$count)*$count);

my $docs_to_restore = $gb->get_documents($obvius->{USER}, $offset, $count,
                                        sort_by => $sort_by, reverse => $reverse);

my $all_docs = $gb->can_access_all($obvius->{USER});

$docs_to_restore ||= [];
for my $doc (@$docs_to_restore) {
     $doc->{path} =~ s![^/]+/$!!;
}

my $uri = $r->uri . "?obvius_command_garbage=1&count=$count&reverse=$reverse";
$uri .= "&old_sort_by=$sort_by";
my $sort_uri = $uri . "&offset=$offset";
my $return_link = uri_escape($sort_uri);

if ($msg) {
  $m->comp("/shared/set_status_message",
           status => $msgstatus || "WARNING",
           message => uri_unescape($msg));
}

</%init>
<%once>
use URI::Escape;
use Obvius::GarbageHandler;
</%once>
