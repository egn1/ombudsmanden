% if ($show_toc || $legal) { # enable sticky-ness if needed
    <script>
      $(document).ready(function(){
        $("#toggle-wrapper").stick_in_parent({
          parent:"#content"
        });
      });
    </script>
% }

<%perl>
my $rownum = 0;
for my $row (@$data) {
  my $rowtype = $row->{type};
  my $coltype = '';
  my $rowclass = 'row';
  my $asideclass = 'col-2';
  if($rowtype eq 'single-column'){
    if($show_aside && $rownum == 0) { # if we also want an aside (TOC or legal), we need to use a narrower column
      $coltype = 'col-8';
      $rowclass = 'row-left';
    }
    else {
      $coltype = 'col-12';
    }
  } elsif($rowtype eq 'one-one-column'){
    if($show_aside && $rownum == 0) { # if we also want an aside (TOC or legal), we need to use a narrower column
      $coltype = 'col-4';
      $rowclass = 'row-left';
    }
    else {
      $coltype = 'col-6';
    }
  }
  elsif($rowtype eq 'single-column-narrow'){
    $coltype = 'col-8';
    if($show_aside && $rownum == 0) {
      $rowclass = 'row-left';
    }
    else { # only if we are not adding an aside column do we want to center the col-8
      $rowclass = 'row-center'
    }
  }
  elsif($rowtype eq 'single-column-half'){
    $coltype = 'col-6';
    $asideclass = 'col-3';
    if($show_aside && $rownum == 0) {
      $rowclass = 'row-left';
    }
    else { # only if we are not adding an aside column do we want to center the col-6
      $rowclass = 'row-center';
    }
  }
</%perl>

    <section class="<% $rowclass %> <% $row->{backgroundcolor} %>">
%   if($show_aside && $rownum == 0) {
      <div id="aside" class="<% $asideclass %>">
        <div id="toggle-wrapper">
%       if($show_toc) {

          <button id="toc-nav-toggle" class="no-styling slide-in-toggle" data-slide-target="toc-nav" aria-expanded="false">
            <span class="circular-button">
              <span class="sr-only">Åbn indholdsfortegnelse</span>
            </span>
            <span class="label">Indhold</span>
          </button>
          
          <nav id="toc-nav" class="slide-in push-from-left">
            <h3 class="slide-in-title"><span class="sr-only">Indholdsfortegnelse: </span><% $title %></h3>
            __TOC_PLACEHOLDER__
            <button class="slide-in-toggle no-styling" data-slide-target="toc-nav">
              <span class="circular-button">
                <span class="sr-only">Luk indholdsfortegnelse</span>
              </span>
            </button>
          </nav>

%       }
%       if($legal) {
          <button id="legal-code-toggle" class="no-styling slide-in-toggle" data-slide-target="legal-code" aria-expanded="false">
            <span class="circular-button">
              <span class="sr-only">Åbn lovtekst</span>
            </span>
            <span class="label">Regler</span>
          </button>

          <section id="legal-code" class="slide-in push-from-left">
            <article class="slide-in-content">
              <% $legal %>
            </article>
            <button class="slide-in-toggle no-styling" data-slide-target="legal-code">
              <span class="circular-button">
                <span class="sr-only">Luk lovtekst</span>
              </span>
            </button>
          </section>
%       }
        </div>
%       if ($pdf_link) {
%       my $pdf_html_link = $pdf_link; $pdf_html_link =~ s!/$!!;
          <a class="pdf-link circular-button-wrapper" href="<% $pdf_html_link %>" <% $pdf_link_title ? $pdf_link_title : 'title="Link til PDF-version"' %>>
            <span class="circular-button" aria-hidden="true"></span>
            <span class="label">PDF</span>
          </a>
%       }
      </div>
%   }
% my $colnum = 0;
%   for my $col (@{$row->{columns} || []}) {
        <article class="gridside-article <% $coltype %> <% @{$row->{columnbgs} || []}[$colnum] || '' %>">
%           for my $item (@{$col || []}) {
%               my $type = $item->{type} || 'html';
% # We don't actually need this wrapping widget-div
% #                <div class="widget-<% $type %>">
                    <& "/doctypes/gridcontent_files/widgets/$type", %$item &>
% #                </div>
%           }
%           # nbsp hvis der ikke er noget indhold:
        </article>
%   $colnum++; #increment the col iterator
%   }
    </section>
% $rownum++; # increment row iterator
%} # close the for{} that started in the <%perl> block
<%once>
use JSON;
use Ombudsmanden::TocBuilder;
</%once>
<%shared>
my $doc_date = $m->scomp('/shared/display_date', date=>$vdoc->Docdate, month=>'dot-numeric');
my $mat_type = $vdoc->field('mat_type');
my $forvret_emne = $vdoc->field('forvret_emne');
my $show_toc = 0;
</%shared>
<%init>
$obvius->get_version_fields($vdoc, ['gridcontent']);
my $data = $vdoc->field('gridcontent') || "[]";

my $pdf_link  = $vdoc->field('pdf_link');
my $html_link = $vdoc->field('html_link');
$pdf_link  = undef if $pdf_link eq '/';
$html_link = undef if $html_link eq '/';
my $pdf_link_title;
if ($pdf_link) {
  my $pdf_doc = $obvius->lookup_document($pdf_link);
  if ($pdf_doc) {
    $pdf_link_title =
      $obvius->get_public_version_field( $pdf_doc, 'title' );
  }
  $pdf_link_title = 'title="' . $pdf_link_title . '"' if ($pdf_link_title);
}

$show_toc = $vdoc->field('show_toc');
my $legal = $vdoc->field('legal_code');
my $show_aside = $show_toc || $legal || $pdf_link ? 1 : 0;
my $title = $vdoc->Title;

eval { $data = from_json($data); };
if($@) {
    print STDERR "Malformed gridcontent data: $data\n";
    $data = [];
}
</%init>

<%filter>
$_ = inject_toc_links($_) if $show_toc;

</%filter>
