#!/usr/bin/perl

use strict;
use warnings;
use utf8;

use Obvius;
use Obvius::Config;
use Ombudsmanden::MasonToolBase;
use Ombudsmanden::ComplaintHandler;

# Hardcoded formdata, used for the email
my $formdata = [
    {
        'title' => 'Navn',
        'name' => 'Navn',
        'type' => 'text',
        'validaterules' => '',
        'unique' => '0',
        '_submitted_value' => "My name is ÆØÅæøå",
        'description' => '',
        'imagepath' => '',
        'mandatory' => '0',
        'options' => '',
        'params' => {'param_autocomplete' => 'on'},
        'maxfiles' => '1'
    },
    {
        'imagepath' => '',
        'mandatory' => '0',
        'maxfiles' => '1',
        'options' => {
            'option' => [
                {'optionvalue' => 'Ja', 'optiontitle' => 'Ja'},
                {'optionvalue' => 'Nej', 'optiontitle' => 'Nej'}
            ]
        },
        'params' => {'param_autocomplete' => 'on'},
        'name' => 'Er_du_glad_',
        'title' => 'Er du glad?',
        'type' => 'radio',
        'unique' => '0',
        'validaterules' => '',
        '_submitted_value' => 'Ja',
        'description' => ''
    }
];

my $masontool = Ombudsmanden::MasonToolBase->new('ombud');
my $obvius = $masontool->obvius;

my $doc = $obvius->get_doc_by_id(1);
$masontool->set_document($doc);

# Fake that the root document is a complaint form.
$masontool->set_request($obvius->get_doc_uri($doc));

$masontool->run_in_mason_context(
    sub {
        my ($m, $r) = @_;

        # Lookup all vfields, so email looks nice
        $obvius->get_version_fields($masontool->vdoc, 256);

        # Set stuff that is expected in the request object
        my $uuid = $masontool->obvius->execute_select(
            'select uuid() as uuid'
        )->[0]->{uuid};
        $r->param('obvius_submission_uuid' => $uuid);
        $r->param('obvius_anonymized_user' => $uuid);

        my $prefix = '';
        my $handler = Ombudsmanden::ComplaintHandler->new(
            $masontool->obvius, $m, $r, $prefix
        );

        my $output = Obvius::Data->new({formdata => $formdata});
        $handler->send_complaint(1, $output);
    }
);
