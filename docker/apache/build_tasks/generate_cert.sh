#!/bin/bash

ALTNAME="DNS:${DOCKER_HOSTNAME}",DNS:"cms.${DOCKER_HOSTNAME}"

while read word; do
    ALTNAME="${ALTNAME},DNS:$word"
done < <(echo $EXTRA_HOSTNAMES | sed -n 1'p' | tr ',' '\n')
echo $ALTNAME

mkdir -p /etc/apache2/certificates/
openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes \
  -keyout /etc/apache2/certificates/localhost.key -out /etc/apache2/certificates/localhost.crt -subj /CN="${DOCKER_HOSTNAME}" \
  -addext subjectAltName=$ALTNAME
