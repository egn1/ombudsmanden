
var cat_hoved_data = {

         'Afgifter': {
             'subs': {

                  'Afgifter 1': {
                      'name': 'Retsafgifter',
                      'id': 'Afgifter 1',
                      'key': '1',
                      'display_title': 'Afgifter 1 - Retsafgifter'
                  },


                  'Afgifter 2': {
                      'name': 'Stempelafgifter',
                      'id': 'Afgifter 2',
                      'key': '2',
                      'display_title': 'Afgifter 2 - Stempelafgifter'
                  },


                  'Afgifter 3': {
                      'subs': {

                           'Afgifter 3.1': {
                               'name': 'Fuld og begrænset skattepligt',
                               'id': 'Afgifter 3.1',
                               'key': '1',
                               'display_title': 'Afgifter 3.1 - Fuld og begrænset skattepligt'
                           },


                           'Afgifter 3.2': {
                               'name': 'Afgiftsfrihed',
                               'id': 'Afgifter 3.2',
                               'key': '2',
                               'display_title': 'Afgifter 3.2 - Afgiftsfrihed'
                           },


                           'Afgifter 3.3': {
                               'name': 'Boafgift',
                               'id': 'Afgifter 3.3',
                               'key': '3',
                               'display_title': 'Afgifter 3.3 - Boafgift'
                           },


                           'Afgifter 3.4': {
                               'name': 'Tillægsboafgift',
                               'id': 'Afgifter 3.4',
                               'key': '4',
                               'display_title': 'Afgifter 3.4 - Tillægsboafgift'
                           },


                           'Afgifter 3.5': {
                               'name': 'Gaveafgift',
                               'id': 'Afgifter 3.5',
                               'key': '5',
                               'display_title': 'Afgifter 3.5 - Gaveafgift'
                           },


                           'Afgifter 3.6': {
                               'name': 'Arveforskud',
                               'id': 'Afgifter 3.6',
                               'key': '6',
                               'display_title': 'Afgifter 3.6 - Arveforskud'
                           },


                           'Afgifter 3.7': {
                               'name': 'Arveafkald',
                               'id': 'Afgifter 3.7',
                               'key': '7',
                               'display_title': 'Afgifter 3.7 - Arveafkald'
                           },


                           'Afgifter 3.8': {
                               'name': 'Opgørelse og indbetaling',
                               'id': 'Afgifter 3.8',
                               'key': '8',
                               'display_title': 'Afgifter 3.8 - Opgørelse og indbetaling'
                           },


                           'Afgifter 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Afgifter 3.9',
                               'key': '9',
                               'display_title': 'Afgifter 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Boafgifter',
                      'id': 'Afgifter 3',
                      'key': '3',
                      'display_title': 'Afgifter 3 - Boafgifter'
                  },


                  'Afgifter 4': {
                      'subs': {

                           'Afgifter 4.1': {
                               'name': 'Toldområdet',
                               'id': 'Afgifter 4.1',
                               'key': '1',
                               'display_title': 'Afgifter 4.1 - Toldområdet'
                           },


                           'Afgifter 4.2': {
                               'name': 'Told- og afgiftsfrihed',
                               'id': 'Afgifter 4.2',
                               'key': '2',
                               'display_title': 'Afgifter 4.2 - Told- og afgiftsfrihed'
                           },


                           'Afgifter 4.3': {
                               'name': 'Toldkontrol og toldbehandling',
                               'id': 'Afgifter 4.3',
                               'key': '3',
                               'display_title': 'Afgifter 4.3 - Toldkontrol og toldbehandling'
                           },


                           'Afgifter 4.4': {
                               'name': 'Frihavne m.v.',
                               'id': 'Afgifter 4.4',
                               'key': '4',
                               'display_title': 'Afgifter 4.4 - Frihavne m.v.'
                           },


                           'Afgifter 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Afgifter 4.9',
                               'key': '9',
                               'display_title': 'Afgifter 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Told',
                      'id': 'Afgifter 4',
                      'key': '4',
                      'display_title': 'Afgifter 4 - Told'
                  },


                  'Afgifter 5': {
                      'subs': {

                           'Afgifter 5.1': {
                               'name': 'Momsområdet',
                               'id': 'Afgifter 5.1',
                               'key': '1',
                               'display_title': 'Afgifter 5.1 - Momsområdet'
                           },


                           'Afgifter 5.2': {
                               'name': 'Afgiftspligtige personer',
                               'id': 'Afgifter 5.2',
                               'key': '2',
                               'display_title': 'Afgifter 5.2 - Afgiftspligtige personer'
                           },


                           'Afgifter 5.3': {
                               'name': 'Afgiftspligtige transaktioner',
                               'id': 'Afgifter 5.3',
                               'key': '3',
                               'display_title': 'Afgifter 5.3 - Afgiftspligtige transaktioner'
                           },


                           'Afgifter 5.4': {
                               'name': 'Afgiftsfritagne transaktioner',
                               'id': 'Afgifter 5.4',
                               'key': '4',
                               'display_title': 'Afgifter 5.4 - Afgiftsfritagne transaktioner'
                           },


                           'Afgifter 5.5': {
                               'name': 'Afgiftsgrundlaget',
                               'id': 'Afgifter 5.5',
                               'key': '5',
                               'display_title': 'Afgifter 5.5 - Afgiftsgrundlaget'
                           },


                           'Afgifter 5.6': {
                               'name': 'Fradrag og godtgørelse',
                               'id': 'Afgifter 5.6',
                               'key': '6',
                               'display_title': 'Afgifter 5.6 - Fradrag og godtgørelse'
                           },


                           'Afgifter 5.7': {
                               'name': 'Registrering',
                               'id': 'Afgifter 5.7',
                               'key': '7',
                               'display_title': 'Afgifter 5.7 - Registrering'
                           },


                           'Afgifter 5.8': {
                               'name': 'Opgørelse og betaling',
                               'id': 'Afgifter 5.8',
                               'key': '8',
                               'display_title': 'Afgifter 5.8 - Opgørelse og betaling'
                           },


                           'Afgifter 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Afgifter 5.9',
                               'key': '9',
                               'display_title': 'Afgifter 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Moms m.v.',
                      'id': 'Afgifter 5',
                      'key': '5',
                      'display_title': 'Afgifter 5 - Moms m.v.'
                  },


                  'Afgifter 6': {
                      'subs': {

                           'Afgifter 6.1': {
                               'subs': {

                                    'Afgifter 61.1': {
                                        'name': 'Elafgift',
                                        'id': 'Afgifter 61.1',
                                        'key': '1',
                                        'display_title': 'Afgifter 61.1 - Elafgift'
                                    },


                                    'Afgifter 61.2': {
                                        'name': 'Gasafgift',
                                        'id': 'Afgifter 61.2',
                                        'key': '2',
                                        'display_title': 'Afgifter 61.2 - Gasafgift'
                                    },


                                    'Afgifter 61.3': {
                                        'name': 'Kulafgift',
                                        'id': 'Afgifter 61.3',
                                        'key': '3',
                                        'display_title': 'Afgifter 61.3 - Kulafgift'
                                    },


                                    'Afgifter 61.4': {
                                        'name': 'Mineralolieafgift',
                                        'id': 'Afgifter 61.4',
                                        'key': '4',
                                        'display_title': 'Afgifter 61.4 - Mineralolieafgift'
                                    },


                                    'Afgifter 61.5': {
                                        'name': 'Brændstofforbrugsafgift',
                                        'id': 'Afgifter 61.5',
                                        'key': '5',
                                        'display_title': 'Afgifter 61.5 - Brændstofforbrugsafgift'
                                    },


                                    'Afgifter 61.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Afgifter 61.9',
                                        'key': '9',
                                        'display_title': 'Afgifter 61.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Energiafgifter',
                               'id': 'Afgifter 6.1',
                               'key': '1',
                               'display_title': 'Afgifter 6.1 - Energiafgifter'
                           },


                           'Afgifter 6.2': {
                               'subs': {

                                    'Afgifter 62.1': {
                                        'subs': {

                                             'Afgifter 621.1': {
                                                 'name': 'Affaldsafgift',
                                                 'id': 'Afgifter 621.1',
                                                 'key': '1',
                                                 'display_title': 'Afgifter 621.1 - Affaldsafgift'
                                             },


                                             'Afgifter 621.2': {
                                                 'name': 'CFC-afgift',
                                                 'id': 'Afgifter 621.2',
                                                 'key': '2',
                                                 'display_title': 'Afgifter 621.2 - CFC-afgift'
                                             },


                                             'Afgifter 621.3': {
                                                 'name': 'Kuldioxidafgift',
                                                 'id': 'Afgifter 621.3',
                                                 'key': '3',
                                                 'display_title': 'Afgifter 621.3 - Kuldioxidafgift'
                                             },


                                             'Afgifter 621.4': {
                                                 'name': 'Kvælstofafgift',
                                                 'id': 'Afgifter 621.4',
                                                 'key': '4',
                                                 'display_title': 'Afgifter 621.4 - Kvælstofafgift'
                                             },


                                             'Afgifter 621.5': {
                                                 'name': 'Spildevandsafgift',
                                                 'id': 'Afgifter 621.5',
                                                 'key': '5',
                                                 'display_title': 'Afgifter 621.5 - Spildevandsafgift'
                                             },


                                             'Afgifter 621.6': {
                                                 'name': 'Svovlafgifter',
                                                 'id': 'Afgifter 621.6',
                                                 'key': '6',
                                                 'display_title': 'Afgifter 621.6 - Svovlafgifter'
                                             },


                                             'Afgifter 621.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Afgifter 621.9',
                                                 'key': '9',
                                                 'display_title': 'Afgifter 621.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Udledninger',
                                        'id': 'Afgifter 62.1',
                                        'key': '1',
                                        'display_title': 'Afgifter 62.1 - Udledninger'
                                    },


                                    'Afgifter 62.2': {
                                        'subs': {

                                             'Afgifter 622.1': {
                                                 'name': 'Vandafgift',
                                                 'id': 'Afgifter 622.1',
                                                 'key': '1',
                                                 'display_title': 'Afgifter 622.1 - Vandafgift'
                                             },


                                             'Afgifter 622.2': {
                                                 'name': 'Batteriafgift',
                                                 'id': 'Afgifter 622.2',
                                                 'key': '2',
                                                 'display_title': 'Afgifter 622.2 - Batteriafgift'
                                             },


                                             'Afgifter 622.3': {
                                                 'name': 'Bekæmpelsesmiddelafgift',
                                                 'id': 'Afgifter 622.3',
                                                 'key': '3',
                                                 'display_title': 'Afgifter 622.3 - Bekæmpelsesmiddelafgift'
                                             },


                                             'Afgifter 622.4': {
                                                 'name': 'Emballageafgift',
                                                 'id': 'Afgifter 622.4',
                                                 'key': '4',
                                                 'display_title': 'Afgifter 622.4 - Emballageafgift'
                                             },


                                             'Afgifter 622.5': {
                                                 'name': 'Opløsningsmiddelafgift',
                                                 'id': 'Afgifter 622.5',
                                                 'key': '5',
                                                 'display_title': 'Afgifter 622.5 - Opløsningsmiddelafgift'
                                             },


                                             'Afgifter 622.6': {
                                                 'name': 'Pvc-afgift',
                                                 'id': 'Afgifter 622.6',
                                                 'key': '6',
                                                 'display_title': 'Afgifter 622.6 - Pvc-afgift'
                                             },


                                             'Afgifter 622.7': {
                                                 'name': 'Vækstfremmerafgift',
                                                 'id': 'Afgifter 622.7',
                                                 'key': '7',
                                                 'display_title': 'Afgifter 622.7 - Vækstfremmerafgift'
                                             },


                                             'Afgifter 622.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Afgifter 622.9',
                                                 'key': '9',
                                                 'display_title': 'Afgifter 622.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Forbrug',
                                        'id': 'Afgifter 62.2',
                                        'key': '2',
                                        'display_title': 'Afgifter 62.2 - Forbrug'
                                    },


                                    'Afgifter 62.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Afgifter 62.9',
                                        'key': '9',
                                        'display_title': 'Afgifter 62.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Grønne afgifter',
                               'id': 'Afgifter 6.2',
                               'key': '2',
                               'display_title': 'Afgifter 6.2 - Grønne afgifter'
                           },


                           'Afgifter 6.3': {
                               'subs': {

                                    'Afgifter 63.1': {
                                        'name': 'Spiritusafgifter',
                                        'id': 'Afgifter 63.1',
                                        'key': '1',
                                        'display_title': 'Afgifter 63.1 - Spiritusafgifter'
                                    },


                                    'Afgifter 63.2': {
                                        'name': 'Øl- og vinafgift',
                                        'id': 'Afgifter 63.2',
                                        'key': '2',
                                        'display_title': 'Afgifter 63.2 - Øl- og vinafgift'
                                    },


                                    'Afgifter 63.3': {
                                        'name': 'Tobaksafgift',
                                        'id': 'Afgifter 63.3',
                                        'key': '3',
                                        'display_title': 'Afgifter 63.3 - Tobaksafgift'
                                    },


                                    'Afgifter 63.4': {
                                        'name': 'Chokoladeafgift',
                                        'id': 'Afgifter 63.4',
                                        'key': '4',
                                        'display_title': 'Afgifter 63.4 - Chokoladeafgift'
                                    },


                                    'Afgifter 63.5': {
                                        'name': 'Konsum-isafgift',
                                        'id': 'Afgifter 63.5',
                                        'key': '5',
                                        'display_title': 'Afgifter 63.5 - Konsum-isafgift'
                                    },


                                    'Afgifter 63.6': {
                                        'name': 'Mineralvandsafgift',
                                        'id': 'Afgifter 63.6',
                                        'key': '6',
                                        'display_title': 'Afgifter 63.6 - Mineralvandsafgift'
                                    },


                                    'Afgifter 63.9': {
                                        'name': 'Andre konsumafgifter',
                                        'id': 'Afgifter 63.9',
                                        'key': '9',
                                        'display_title': 'Afgifter 63.9 - Andre konsumafgifter'
                                    }

                               },
                               'name': 'Konsumafgifter',
                               'id': 'Afgifter 6.3',
                               'key': '3',
                               'display_title': 'Afgifter 6.3 - Konsumafgifter'
                           },


                           'Afgifter 6.4': {
                               'subs': {

                                    'Afgifter 64.1': {
                                        'name': 'Registreringsafgift',
                                        'id': 'Afgifter 64.1',
                                        'key': '1',
                                        'display_title': 'Afgifter 64.1 - Registreringsafgift'
                                    },


                                    'Afgifter 64.2': {
                                        'name': 'Vægtafgift',
                                        'id': 'Afgifter 64.2',
                                        'key': '2',
                                        'display_title': 'Afgifter 64.2 - Vægtafgift'
                                    },


                                    'Afgifter 64.3': {
                                        'name': 'Vejbenyttelsesafgift',
                                        'id': 'Afgifter 64.3',
                                        'key': '3',
                                        'display_title': 'Afgifter 64.3 - Vejbenyttelsesafgift'
                                    },


                                    'Afgifter 64.4': {
                                        'name': 'Ansvarsforsikringsafgift',
                                        'id': 'Afgifter 64.4',
                                        'key': '4',
                                        'display_title': 'Afgifter 64.4 - Ansvarsforsikringsafgift'
                                    },


                                    'Afgifter 64.5': {
                                        'name': 'Flyrejseafgift',
                                        'id': 'Afgifter 64.5',
                                        'key': '5',
                                        'display_title': 'Afgifter 64.5 - Flyrejseafgift'
                                    },


                                    'Afgifter 64.6': {
                                        'name': 'Lystfartøjsforsikringsafgift',
                                        'id': 'Afgifter 64.6',
                                        'key': '6',
                                        'display_title': 'Afgifter 64.6 - Lystfartøjsforsikringsafgift'
                                    },


                                    'Afgifter 64.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Afgifter 64.9',
                                        'key': '9',
                                        'display_title': 'Afgifter 64.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Transportafgifter m.v.',
                               'id': 'Afgifter 6.4',
                               'key': '4',
                               'display_title': 'Afgifter 6.4 - Transportafgifter m.v.'
                           },


                           'Afgifter 6.5': {
                               'subs': {

                                    'Afgifter 65.1': {
                                        'name': 'Kasinoafgift',
                                        'id': 'Afgifter 65.1',
                                        'key': '1',
                                        'display_title': 'Afgifter 65.1 - Kasinoafgift'
                                    },


                                    'Afgifter 65.2': {
                                        'name': 'Lotteriafgift',
                                        'id': 'Afgifter 65.2',
                                        'key': '2',
                                        'display_title': 'Afgifter 65.2 - Lotteriafgift'
                                    },


                                    'Afgifter 65.3': {
                                        'name': 'Afgift af spilleautomater',
                                        'id': 'Afgifter 65.3',
                                        'key': '3',
                                        'display_title': 'Afgifter 65.3 - Afgift af spilleautomater'
                                    },


                                    'Afgifter 65.4': {
                                        'name': 'Totalisatorspilafgift',
                                        'id': 'Afgifter 65.4',
                                        'key': '4',
                                        'display_title': 'Afgifter 65.4 - Totalisatorspilafgift'
                                    },


                                    'Afgifter 65.9': {
                                        'name': 'Afgift af andre spil, lotterier og væddemål',
                                        'id': 'Afgifter 65.9',
                                        'key': '9',
                                        'display_title': 'Afgifter 65.9 - Afgift af andre spil, lotterier og væddemål'
                                    }

                               },
                               'name': 'Afgift på spil og væddemål',
                               'id': 'Afgifter 6.5',
                               'key': '5',
                               'display_title': 'Afgifter 6.5 - Afgift på spil og væddemål'
                           },


                           'Afgifter 6.6': {
                               'name': 'Lønsumafgift',
                               'id': 'Afgifter 6.6',
                               'key': '6',
                               'display_title': 'Afgifter 6.6 - Lønsumafgift'
                           },


                           'Afgifter 6.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Afgifter 6.9',
                               'key': '9',
                               'display_title': 'Afgifter 6.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Forbrugsafgifter',
                      'id': 'Afgifter 6',
                      'key': '6',
                      'display_title': 'Afgifter 6 - Forbrugsafgifter'
                  },


                  'Afgifter 7': {
                      'name': 'Tinglysning, se Tinglysning 7',
                      'id': 'Afgifter 7',
                      'key': '7',
                      'display_title': 'Afgifter 7 - Tinglysning, se Tinglysning 7'
                  },


                  'Afgifter 9': {
                      'name': 'Andre afgifter',
                      'id': 'Afgifter 9',
                      'key': '9',
                      'display_title': 'Afgifter 9 - Andre afgifter'
                  }

             },
             'name': 'Afgifter',
             'id': 'Afgifter',
             'key': 'afgifter',
             'display_title': 'Afgifter'
         },


         'Aftaler': {
             'subs': {

                  'Aftaler 1': {
                      'subs': {

                           'Aftaler 1.1': {
                               'name': 'Tilbud',
                               'id': 'Aftaler 1.1',
                               'key': '1',
                               'display_title': 'Aftaler 1.1 - Tilbud'
                           },


                           'Aftaler 1.2': {
                               'name': 'Accept',
                               'id': 'Aftaler 1.2',
                               'key': '2',
                               'display_title': 'Aftaler 1.2 - Accept'
                           },


                           'Aftaler 1.3': {
                               'name': 'Stiltiende løfte',
                               'id': 'Aftaler 1.3',
                               'key': '3',
                               'display_title': 'Aftaler 1.3 - Stiltiende løfte'
                           },


                           'Aftaler 1.4': {
                               'name': 'Standardvilkår',
                               'id': 'Aftaler 1.4',
                               'key': '4',
                               'display_title': 'Aftaler 1.4 - Standardvilkår'
                           },


                           'Aftaler 1.5': {
                               'name': 'Kontraheringspligt',
                               'id': 'Aftaler 1.5',
                               'key': '5',
                               'display_title': 'Aftaler 1.5 - Kontraheringspligt'
                           },


                           'Aftaler 1.6': {
                               'name': 'Tilbagetrædelsesret (Fortrydelsesret)',
                               'id': 'Aftaler 1.6',
                               'key': '6',
                               'display_title': 'Aftaler 1.6 - Tilbagetrædelsesret (Fortrydelsesret)'
                           },


                           'Aftaler 1.7': {
                               'name': 'Gaver',
                               'id': 'Aftaler 1.7',
                               'key': '7',
                               'display_title': 'Aftaler 1.7 - Gaver'
                           },


                           'Aftaler 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Aftaler 1.9',
                               'key': '9',
                               'display_title': 'Aftaler 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Indgåelse',
                      'id': 'Aftaler 1',
                      'key': '1',
                      'display_title': 'Aftaler 1 - Indgåelse'
                  },


                  'Aftaler 2': {
                      'subs': {

                           'Aftaler 2.1': {
                               'subs': {

                                    'Aftaler 21.1': {
                                        'name': 'Falsk og forfalskning',
                                        'id': 'Aftaler 21.1',
                                        'key': '1',
                                        'display_title': 'Aftaler 21.1 - Falsk og forfalskning'
                                    },


                                    'Aftaler 21.2': {
                                        'name': 'Tvang',
                                        'id': 'Aftaler 21.2',
                                        'key': '2',
                                        'display_title': 'Aftaler 21.2 - Tvang'
                                    },


                                    'Aftaler 21.3': {
                                        'name': 'Svig',
                                        'id': 'Aftaler 21.3',
                                        'key': '3',
                                        'display_title': 'Aftaler 21.3 - Svig'
                                    },


                                    'Aftaler 21.4': {
                                        'name': 'Udnyttelse',
                                        'id': 'Aftaler 21.4',
                                        'key': '4',
                                        'display_title': 'Aftaler 21.4 - Udnyttelse'
                                    },


                                    'Aftaler 21.5': {
                                        'name': 'Proforma',
                                        'id': 'Aftaler 21.5',
                                        'key': '5',
                                        'display_title': 'Aftaler 21.5 - Proforma'
                                    },


                                    'Aftaler 21.6': {
                                        'name': 'Forvanskning og fejltagelse',
                                        'id': 'Aftaler 21.6',
                                        'key': '6',
                                        'display_title': 'Aftaler 21.6 - Forvanskning og fejltagelse'
                                    },


                                    'Aftaler 21.7': {
                                        'name': 'Forudsætninger',
                                        'id': 'Aftaler 21.7',
                                        'key': '7',
                                        'display_title': 'Aftaler 21.7 - Forudsætninger'
                                    },


                                    'Aftaler 21.8': {
                                        'name': 'Formkrav',
                                        'id': 'Aftaler 21.8',
                                        'key': '8',
                                        'display_title': 'Aftaler 21.8 - Formkrav'
                                    },


                                    'Aftaler 21.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Aftaler 21.9',
                                        'key': '9',
                                        'display_title': 'Aftaler 21.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Tilblivelsesmangler',
                               'id': 'Aftaler 2.1',
                               'key': '1',
                               'display_title': 'Aftaler 2.1 - Tilblivelsesmangler'
                           },


                           'Aftaler 2.2': {
                               'subs': {

                                    'Aftaler 22.1': {
                                        'name': 'Præceptive regler',
                                        'id': 'Aftaler 22.1',
                                        'key': '1',
                                        'display_title': 'Aftaler 22.1 - Præceptive regler'
                                    },


                                    'Aftaler 22.2': {
                                        'name': 'Lov og ærbarhed',
                                        'id': 'Aftaler 22.2',
                                        'key': '2',
                                        'display_title': 'Aftaler 22.2 - Lov og ærbarhed'
                                    },


                                    'Aftaler 22.3': {
                                        'name': 'Almindelig hæderlighed',
                                        'id': 'Aftaler 22.3',
                                        'key': '3',
                                        'display_title': 'Aftaler 22.3 - Almindelig hæderlighed'
                                    },


                                    'Aftaler 22.4': {
                                        'subs': {

                                             'Aftaler 224.1': {
                                                 'name': 'Forbrugeraftaler',
                                                 'id': 'Aftaler 224.1',
                                                 'key': '1',
                                                 'display_title': 'Aftaler 224.1 - Forbrugeraftaler'
                                             },


                                             'Aftaler 224.2': {
                                                 'name': 'Aftaler mellem private',
                                                 'id': 'Aftaler 224.2',
                                                 'key': '2',
                                                 'display_title': 'Aftaler 224.2 - Aftaler mellem private'
                                             },


                                             'Aftaler 224.3': {
                                                 'name': 'Aftaler mellem erhvervsdrivende',
                                                 'id': 'Aftaler 224.3',
                                                 'key': '3',
                                                 'display_title': 'Aftaler 224.3 - Aftaler mellem erhvervsdrivende'
                                             },


                                             'Aftaler 224.4': {
                                                 'name': 'Andre retsforhold',
                                                 'id': 'Aftaler 224.4',
                                                 'key': '4',
                                                 'display_title': 'Aftaler 224.4 - Andre retsforhold'
                                             },


                                             'Aftaler 224.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Aftaler 224.9',
                                                 'key': '9',
                                                 'display_title': 'Aftaler 224.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Generalklausulen',
                                        'id': 'Aftaler 22.4',
                                        'key': '4',
                                        'display_title': 'Aftaler 22.4 - Generalklausulen'
                                    },


                                    'Aftaler 22.5': {
                                        'name': 'Omgåelse',
                                        'id': 'Aftaler 22.5',
                                        'key': '5',
                                        'display_title': 'Aftaler 22.5 - Omgåelse'
                                    },


                                    'Aftaler 22.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Aftaler 22.9',
                                        'key': '9',
                                        'display_title': 'Aftaler 22.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Indholdsmangler',
                               'id': 'Aftaler 2.2',
                               'key': '2',
                               'display_title': 'Aftaler 2.2 - Indholdsmangler'
                           },


                           'Aftaler 2.3': {
                               'subs': {

                                    'Aftaler 23.1': {
                                        'name': 'Umyndighed og handleevnefratagelse',
                                        'id': 'Aftaler 23.1',
                                        'key': '1',
                                        'display_title': 'Aftaler 23.1 - Umyndighed og handleevnefratagelse'
                                    },


                                    'Aftaler 23.2': {
                                        'name': 'Fornuftsmangel',
                                        'id': 'Aftaler 23.2',
                                        'key': '2',
                                        'display_title': 'Aftaler 23.2 - Fornuftsmangel'
                                    },


                                    'Aftaler 23.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Aftaler 23.9',
                                        'key': '9',
                                        'display_title': 'Aftaler 23.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Inhabilitet, se også Personspørgsmål 5',
                               'id': 'Aftaler 2.3',
                               'key': '3',
                               'display_title': 'Aftaler 2.3 - Inhabilitet, se også Personspørgsmål 5'
                           },


                           'Aftaler 2.9': {
                               'name': 'Andre ugyldighedsgrunde',
                               'id': 'Aftaler 2.9',
                               'key': '9',
                               'display_title': 'Aftaler 2.9 - Andre ugyldighedsgrunde'
                           }

                      },
                      'name': 'Gyldighed',
                      'id': 'Aftaler 2',
                      'key': '2',
                      'display_title': 'Aftaler 2 - Gyldighed'
                  },


                  'Aftaler 3': {
                      'subs': {

                           'Aftaler 3.1': {
                               'name': 'Forbrugeraftaler. Forbrugerbegrebet',
                               'id': 'Aftaler 3.1',
                               'key': '1',
                               'display_title': 'Aftaler 3.1 - Forbrugeraftaler. Forbrugerbegrebet'
                           },


                           'Aftaler 3.2': {
                               'name': 'Konkurrenceklausul, se også Ansættelses- og arbejdsret 2.6',
                               'id': 'Aftaler 3.2',
                               'key': '2',
                               'display_title': 'Aftaler 3.2 - Konkurrenceklausul, se også Ansættelses- og arbejdsret 2.6'
                           },


                           'Aftaler 3.3': {
                               'name': 'Konventionalbod',
                               'id': 'Aftaler 3.3',
                               'key': '3',
                               'display_title': 'Aftaler 3.3 - Konventionalbod'
                           },


                           'Aftaler 3.4': {
                               'name': 'Tredjemandsaftaler',
                               'id': 'Aftaler 3.4',
                               'key': '4',
                               'display_title': 'Aftaler 3.4 - Tredjemandsaftaler'
                           },


                           'Aftaler 3.5': {
                               'name': 'Tjenesteydelser, se også Erhvervsret 2.2',
                               'id': 'Aftaler 3.5',
                               'key': '5',
                               'display_title': 'Aftaler 3.5 - Tjenesteydelser, se også Erhvervsret 2.2'
                           },


                           'Aftaler 3.9': {
                               'name': 'Andre særlige aftaleforhold',
                               'id': 'Aftaler 3.9',
                               'key': '9',
                               'display_title': 'Aftaler 3.9 - Andre særlige aftaleforhold'
                           }

                      },
                      'name': 'Særlige aftaleforhold',
                      'id': 'Aftaler 3',
                      'key': '3',
                      'display_title': 'Aftaler 3 - Særlige aftaleforhold'
                  },


                  'Aftaler 4': {
                      'name': 'Fortolkning og udfyldning',
                      'id': 'Aftaler 4',
                      'key': '4',
                      'display_title': 'Aftaler 4 - Fortolkning og udfyldning'
                  },


                  'Aftaler 5': {
                      'subs': {

                           'Aftaler 5.1': {
                               'name': 'Fuldmagt',
                               'id': 'Aftaler 5.1',
                               'key': '1',
                               'display_title': 'Aftaler 5.1 - Fuldmagt'
                           },


                           'Aftaler 5.2': {
                               'name': 'Kommission',
                               'id': 'Aftaler 5.2',
                               'key': '2',
                               'display_title': 'Aftaler 5.2 - Kommission'
                           },


                           'Aftaler 5.3': {
                               'name': 'Mæglere',
                               'id': 'Aftaler 5.3',
                               'key': '3',
                               'display_title': 'Aftaler 5.3 - Mæglere'
                           },


                           'Aftaler 5.4': {
                               'name': 'Agenter, bude og lignende',
                               'id': 'Aftaler 5.4',
                               'key': '4',
                               'display_title': 'Aftaler 5.4 - Agenter, bude og lignende'
                           },


                           'Aftaler 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Aftaler 5.9',
                               'key': '9',
                               'display_title': 'Aftaler 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Aftaler indgået ved mellemmand',
                      'id': 'Aftaler 5',
                      'key': '5',
                      'display_title': 'Aftaler 5 - Aftaler indgået ved mellemmand'
                  },


                  'Aftaler 6': {
                      'name': 'Erstatning i kontraktforhold, se også Ansættelses- og arbejdsret 272.3, Entrepriseret 2.2, Køb 52.3 og 53.3',
                      'id': 'Aftaler 6',
                      'key': '6',
                      'display_title': 'Aftaler 6 - Erstatning i kontraktforhold, se også Ansættelses- og arbejdsret 272.3, Entrepriseret 2.2, Køb 52.3 og 53.3'
                  },


                  'Aftaler 7': {
                      'name': 'Misligholdelse, se også Ansættelses- og arbejdsret 2.4, Entrepriseret 2.2, Leje af fast ejendom 5',
                      'id': 'Aftaler 7',
                      'key': '7',
                      'display_title': 'Aftaler 7 - Misligholdelse, se også Ansættelses- og arbejdsret 2.4, Entrepriseret 2.2, Leje af fast ejendom 5'
                  },


                  'Aftaler 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Aftaler 9',
                      'key': '9',
                      'display_title': 'Aftaler 9 - Andre spørgsmål'
                  }

             },
             'name': 'Aftaler (se tillige de enkelte aftaletyper)',
             'id': 'Aftaler',
             'key': 'aftaler',
             'display_title': 'Aftaler'
         },


         'Almindelige emner': {
             'subs': {

                  'Almindelige emner 1': {
                      'subs': {

                           'Almindelige emner 1.1': {
                               'subs': {

                                    'Almindelige emner 11.1': {
                                        'name': 'Offentligretlige regler',
                                        'id': 'Almindelige emner 11.1',
                                        'key': '1',
                                        'display_title': 'Almindelige emner 11.1 - Offentligretlige regler'
                                    },


                                    'Almindelige emner 11.2': {
                                        'name': 'Privatretlige regler',
                                        'id': 'Almindelige emner 11.2',
                                        'key': '2',
                                        'display_title': 'Almindelige emner 11.2 - Privatretlige regler'
                                    },


                                    'Almindelige emner 11.3': {
                                        'name': 'Strafferetlige regler',
                                        'id': 'Almindelige emner 11.3',
                                        'key': '3',
                                        'display_title': 'Almindelige emner 11.3 - Strafferetlige regler'
                                    },


                                    'Almindelige emner 11.4': {
                                        'name': 'Procesretlige spørgsmål',
                                        'id': 'Almindelige emner 11.4',
                                        'key': '4',
                                        'display_title': 'Almindelige emner 11.4 - Procesretlige spørgsmål'
                                    },


                                    'Almindelige emner 11.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Almindelige emner 11.9',
                                        'key': '9',
                                        'display_title': 'Almindelige emner 11.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Analogi og modsætningsslutning, se også Forvaltningsret 1.2',
                               'id': 'Almindelige emner 1.1',
                               'key': '1',
                               'display_title': 'Almindelige emner 1.1 - Analogi og modsætningsslutning, se også Forvaltningsret 1.2'
                           },


                           'Almindelige emner 1.2': {
                               'name': 'Motiver',
                               'id': 'Almindelige emner 1.2',
                               'key': '2',
                               'display_title': 'Almindelige emner 1.2 - Motiver'
                           },


                           'Almindelige emner 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Almindelige emner 1.9',
                               'key': '9',
                               'display_title': 'Almindelige emner 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Lovfortolkning',
                      'id': 'Almindelige emner 1',
                      'key': '1',
                      'display_title': 'Almindelige emner 1 - Lovfortolkning'
                  },


                  'Almindelige emner 2': {
                      'subs': {

                           'Almindelige emner 2.1': {
                               'subs': {

                                    'Almindelige emner 21.1': {
                                        'name': 'Stiltiende accept m.v., se også Aftaler 1.3',
                                        'id': 'Almindelige emner 21.1',
                                        'key': '1',
                                        'display_title': 'Almindelige emner 21.1 - Stiltiende accept m.v., se også Aftaler 1.3'
                                    },


                                    'Almindelige emner 21.2': {
                                        'name': 'Reklamationsregler',
                                        'id': 'Almindelige emner 21.2',
                                        'key': '2',
                                        'display_title': 'Almindelige emner 21.2 - Reklamationsregler'
                                    },


                                    'Almindelige emner 21.3': {
                                        'name': 'Bevisvirkninger',
                                        'id': 'Almindelige emner 21.3',
                                        'key': '3',
                                        'display_title': 'Almindelige emner 21.3 - Bevisvirkninger'
                                    },


                                    'Almindelige emner 21.9': {
                                        'name': 'Andre retsfortabende virkninger',
                                        'id': 'Almindelige emner 21.9',
                                        'key': '9',
                                        'display_title': 'Almindelige emner 21.9 - Andre retsfortabende virkninger'
                                    }

                               },
                               'name': 'Retsfortabende, se også Pengevæsen m.v. 5.8',
                               'id': 'Almindelige emner 2.1',
                               'key': '1',
                               'display_title': 'Almindelige emner 2.1 - Retsfortabende, se også Pengevæsen m.v. 5.8'
                           },


                           'Almindelige emner 2.2': {
                               'subs': {

                                    'Almindelige emner 22.1': {
                                        'name': 'Erstatningsretlige virkninger',
                                        'id': 'Almindelige emner 22.1',
                                        'key': '1',
                                        'display_title': 'Almindelige emner 22.1 - Erstatningsretlige virkninger'
                                    },


                                    'Almindelige emner 22.2': {
                                        'name': 'Bevisvirkninger',
                                        'id': 'Almindelige emner 22.2',
                                        'key': '2',
                                        'display_title': 'Almindelige emner 22.2 - Bevisvirkninger'
                                    },


                                    'Almindelige emner 22.3': {
                                        'name': 'Særlige regler',
                                        'id': 'Almindelige emner 22.3',
                                        'key': '3',
                                        'display_title': 'Almindelige emner 22.3 - Særlige regler'
                                    },


                                    'Almindelige emner 22.9': {
                                        'name': 'Andre retsudløsende virkninger',
                                        'id': 'Almindelige emner 22.9',
                                        'key': '9',
                                        'display_title': 'Almindelige emner 22.9 - Andre retsudløsende virkninger'
                                    }

                               },
                               'name': 'Retsudløsende',
                               'id': 'Almindelige emner 2.2',
                               'key': '2',
                               'display_title': 'Almindelige emner 2.2 - Retsudløsende'
                           },


                           'Almindelige emner 2.9': {
                               'name': 'Andre passivitetsspørgsmål',
                               'id': 'Almindelige emner 2.9',
                               'key': '9',
                               'display_title': 'Almindelige emner 2.9 - Andre passivitetsspørgsmål'
                           }

                      },
                      'name': 'Passivitet',
                      'id': 'Almindelige emner 2',
                      'key': '2',
                      'display_title': 'Almindelige emner 2 - Passivitet'
                  },


                  'Almindelige emner 3': {
                      'name': 'Bevis, se også Retspleje 1.4 samt de enkelte retsforhold',
                      'id': 'Almindelige emner 3',
                      'key': '3',
                      'display_title': 'Almindelige emner 3 - Bevis, se også Retspleje 1.4 samt de enkelte retsforhold'
                  },


                  'Almindelige emner 4': {
                      'name': 'Retssædvane',
                      'id': 'Almindelige emner 4',
                      'key': '4',
                      'display_title': 'Almindelige emner 4 - Retssædvane'
                  },


                  'Almindelige emner 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Almindelige emner 9',
                      'key': '9',
                      'display_title': 'Almindelige emner 9 - Andre spørgsmål'
                  }

             },
             'name': 'Almindelige emner',
             'id': 'Almindelige emner',
             'key': 'almindelige_emner',
             'display_title': 'Almindelige emner'
         },


         'Ansættelses- og arbejdsret': {
             'subs': {

                  'Ansættelses- og arbejdsret 1': {
                      'subs': {

                           'Ansættelses- og arbejdsret 1.1': {
                               'name': 'Ansættelse og ansættelsesvilkår',
                               'id': 'Ansættelses- og arbejdsret 1.1',
                               'key': '1',
                               'display_title': 'Ansættelses- og arbejdsret 1.1 - Ansættelse og ansættelsesvilkår'
                           },


                           'Ansættelses- og arbejdsret 1.2': {
                               'name': 'Pligter - Afsked',
                               'id': 'Ansættelses- og arbejdsret 1.2',
                               'key': '2',
                               'display_title': 'Ansættelses- og arbejdsret 1.2 - Pligter - Afsked'
                           },


                           'Ansættelses- og arbejdsret 1.3': {
                               'name': 'Pension',
                               'id': 'Ansættelses- og arbejdsret 1.3',
                               'key': '3',
                               'display_title': 'Ansættelses- og arbejdsret 1.3 - Pension'
                           },


                           'Ansættelses- og arbejdsret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Ansættelses- og arbejdsret 1.9',
                               'key': '9',
                               'display_title': 'Ansættelses- og arbejdsret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Tjenestemænd',
                      'id': 'Ansættelses- og arbejdsret 1',
                      'key': '1',
                      'display_title': 'Ansættelses- og arbejdsret 1 - Tjenestemænd'
                  },


                  'Ansættelses- og arbejdsret 2': {
                      'subs': {

                           'Ansættelses- og arbejdsret 2.1': {
                               'name': 'Funktionærbegrebet',
                               'id': 'Ansættelses- og arbejdsret 2.1',
                               'key': '1',
                               'display_title': 'Ansættelses- og arbejdsret 2.1 - Funktionærbegrebet'
                           },


                           'Ansættelses- og arbejdsret 2.2': {
                               'name': 'Ansættelse og ansættelsesvilkår',
                               'id': 'Ansættelses- og arbejdsret 2.2',
                               'key': '2',
                               'display_title': 'Ansættelses- og arbejdsret 2.2 - Ansættelse og ansættelsesvilkår'
                           },


                           'Ansættelses- og arbejdsret 2.3': {
                               'name': 'Sygdom',
                               'id': 'Ansættelses- og arbejdsret 2.3',
                               'key': '3',
                               'display_title': 'Ansættelses- og arbejdsret 2.3 - Sygdom'
                           },


                           'Ansættelses- og arbejdsret 2.4': {
                               'subs': {

                                    'Ansættelses- og arbejdsret 24.1': {
                                        'name': 'Tilsidesættelse af arbejdsinstrukser',
                                        'id': 'Ansættelses- og arbejdsret 24.1',
                                        'key': '1',
                                        'display_title': 'Ansættelses- og arbejdsret 24.1 - Tilsidesættelse af arbejdsinstrukser'
                                    },


                                    'Ansættelses- og arbejdsret 24.2': {
                                        'name': 'Udeblivelse',
                                        'id': 'Ansættelses- og arbejdsret 24.2',
                                        'key': '2',
                                        'display_title': 'Ansættelses- og arbejdsret 24.2 - Udeblivelse'
                                    },


                                    'Ansættelses- og arbejdsret 24.3': {
                                        'name': 'Spiritusnydelse',
                                        'id': 'Ansættelses- og arbejdsret 24.3',
                                        'key': '3',
                                        'display_title': 'Ansættelses- og arbejdsret 24.3 - Spiritusnydelse'
                                    },


                                    'Ansættelses- og arbejdsret 24.9': {
                                        'name': 'Andet',
                                        'id': 'Ansættelses- og arbejdsret 24.9',
                                        'key': '9',
                                        'display_title': 'Ansættelses- og arbejdsret 24.9 - Andet'
                                    }

                               },
                               'name': 'Misligholdelse',
                               'id': 'Ansættelses- og arbejdsret 2.4',
                               'key': '4',
                               'display_title': 'Ansættelses- og arbejdsret 2.4 - Misligholdelse'
                           },


                           'Ansættelses- og arbejdsret 2.5': {
                               'name': 'Lønspørgsmål',
                               'id': 'Ansættelses- og arbejdsret 2.5',
                               'key': '5',
                               'display_title': 'Ansættelses- og arbejdsret 2.5 - Lønspørgsmål'
                           },


                           'Ansættelses- og arbejdsret 2.6': {
                               'name': 'Konkurrenceklausuler m.v.',
                               'id': 'Ansættelses- og arbejdsret 2.6',
                               'key': '6',
                               'display_title': 'Ansættelses- og arbejdsret 2.6 - Konkurrenceklausuler m.v.'
                           },


                           'Ansættelses- og arbejdsret 2.7': {
                               'subs': {

                                    'Ansættelses- og arbejdsret 27.1': {
                                        'subs': {

                                             'Ansættelses- og arbejdsret 271.1': {
                                                 'name': 'Aftalespørgsmål',
                                                 'id': 'Ansættelses- og arbejdsret 271.1',
                                                 'key': '1',
                                                 'display_title': 'Ansættelses- og arbejdsret 271.1 - Aftalespørgsmål'
                                             },


                                             'Ansættelses- og arbejdsret 271.2': {
                                                 'name': 'Opsigelsesbetingelser, fristberegning m.v.',
                                                 'id': 'Ansættelses- og arbejdsret 271.2',
                                                 'key': '2',
                                                 'display_title': 'Ansættelses- og arbejdsret 271.2 - Opsigelsesbetingelser, fristberegning m.v.'
                                             },


                                             'Ansættelses- og arbejdsret 271.3': {
                                                 'name': 'Opsigelsens afgivelse',
                                                 'id': 'Ansættelses- og arbejdsret 271.3',
                                                 'key': '3',
                                                 'display_title': 'Ansættelses- og arbejdsret 271.3 - Opsigelsens afgivelse'
                                             },


                                             'Ansættelses- og arbejdsret 271.4': {
                                                 'name': 'Usaglig opsigelse',
                                                 'id': 'Ansættelses- og arbejdsret 271.4',
                                                 'key': '4',
                                                 'display_title': 'Ansættelses- og arbejdsret 271.4 - Usaglig opsigelse'
                                             },


                                             'Ansættelses- og arbejdsret 271.9': {
                                                 'name': 'Andre opsigelsesspørgsmål',
                                                 'id': 'Ansættelses- og arbejdsret 271.9',
                                                 'key': '9',
                                                 'display_title': 'Ansættelses- og arbejdsret 271.9 - Andre opsigelsesspørgsmål'
                                             }

                                        },
                                        'name': 'Opsigelse',
                                        'id': 'Ansættelses- og arbejdsret 27.1',
                                        'key': '1',
                                        'display_title': 'Ansættelses- og arbejdsret 27.1 - Opsigelse'
                                    },


                                    'Ansættelses- og arbejdsret 27.2': {
                                        'subs': {

                                             'Ansættelses- og arbejdsret 272.1': {
                                                 'name': 'Bortvisningsgrunde',
                                                 'id': 'Ansættelses- og arbejdsret 272.1',
                                                 'key': '1',
                                                 'display_title': 'Ansættelses- og arbejdsret 272.1 - Bortvisningsgrunde'
                                             },


                                             'Ansættelses- og arbejdsret 272.2': {
                                                 'name': 'Bortvisningens afvigelse',
                                                 'id': 'Ansættelses- og arbejdsret 272.2',
                                                 'key': '2',
                                                 'display_title': 'Ansættelses- og arbejdsret 272.2 - Bortvisningens afvigelse'
                                             },


                                             'Ansættelses- og arbejdsret 272.3': {
                                                 'name': 'Erstatningsspørgsmål',
                                                 'id': 'Ansættelses- og arbejdsret 272.3',
                                                 'key': '3',
                                                 'display_title': 'Ansættelses- og arbejdsret 272.3 - Erstatningsspørgsmål'
                                             },


                                             'Ansættelses- og arbejdsret 272.9': {
                                                 'name': 'Andre bortvisningsspørgsmål',
                                                 'id': 'Ansættelses- og arbejdsret 272.9',
                                                 'key': '9',
                                                 'display_title': 'Ansættelses- og arbejdsret 272.9 - Andre bortvisningsspørgsmål'
                                             }

                                        },
                                        'name': 'Bortvisning',
                                        'id': 'Ansættelses- og arbejdsret 27.2',
                                        'key': '2',
                                        'display_title': 'Ansættelses- og arbejdsret 27.2 - Bortvisning'
                                    },


                                    'Ansættelses- og arbejdsret 27.3': {
                                        'name': 'Andre ophørsgrunde',
                                        'id': 'Ansættelses- og arbejdsret 27.3',
                                        'key': '3',
                                        'display_title': 'Ansættelses- og arbejdsret 27.3 - Andre ophørsgrunde'
                                    },


                                    'Ansættelses- og arbejdsret 27.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Ansættelses- og arbejdsret 27.9',
                                        'key': '9',
                                        'display_title': 'Ansættelses- og arbejdsret 27.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Opsigelse, bortvisning eller bortgang',
                               'id': 'Ansættelses- og arbejdsret 2.7',
                               'key': '7',
                               'display_title': 'Ansættelses- og arbejdsret 2.7 - Opsigelse, bortvisning eller bortgang'
                           },


                           'Ansættelses- og arbejdsret 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Ansættelses- og arbejdsret 2.9',
                               'key': '9',
                               'display_title': 'Ansættelses- og arbejdsret 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Funktionærforhold',
                      'id': 'Ansættelses- og arbejdsret 2',
                      'key': '2',
                      'display_title': 'Ansættelses- og arbejdsret 2 - Funktionærforhold'
                  },


                  'Ansættelses- og arbejdsret 3': {
                      'name': 'Medhjælpere',
                      'id': 'Ansættelses- og arbejdsret 3',
                      'key': '3',
                      'display_title': 'Ansættelses- og arbejdsret 3 - Medhjælpere'
                  },


                  'Ansættelses- og arbejdsret 4': {
                      'name': 'Erhvervsuddannelser',
                      'id': 'Ansættelses- og arbejdsret 4',
                      'key': '4',
                      'display_title': 'Ansættelses- og arbejdsret 4 - Erhvervsuddannelser'
                  },


                  'Ansættelses- og arbejdsret 5': {
                      'subs': {

                           'Ansættelses- og arbejdsret 5.1': {
                               'name': 'Den berettigede personkreds',
                               'id': 'Ansættelses- og arbejdsret 5.1',
                               'key': '1',
                               'display_title': 'Ansættelses- og arbejdsret 5.1 - Den berettigede personkreds'
                           },


                           'Ansættelses- og arbejdsret 5.2': {
                               'name': 'Feriepenge',
                               'id': 'Ansættelses- og arbejdsret 5.2',
                               'key': '2',
                               'display_title': 'Ansættelses- og arbejdsret 5.2 - Feriepenge'
                           },


                           'Ansættelses- og arbejdsret 5.3': {
                               'name': 'Feriens afholdelse',
                               'id': 'Ansættelses- og arbejdsret 5.3',
                               'key': '3',
                               'display_title': 'Ansættelses- og arbejdsret 5.3 - Feriens afholdelse'
                           },


                           'Ansættelses- og arbejdsret 5.4': {
                               'name': 'Aftalespørgsmål',
                               'id': 'Ansættelses- og arbejdsret 5.4',
                               'key': '4',
                               'display_title': 'Ansættelses- og arbejdsret 5.4 - Aftalespørgsmål'
                           },


                           'Ansættelses- og arbejdsret 5.5': {
                               'name': 'Strafferetlige spørgsmål',
                               'id': 'Ansættelses- og arbejdsret 5.5',
                               'key': '5',
                               'display_title': 'Ansættelses- og arbejdsret 5.5 - Strafferetlige spørgsmål'
                           },


                           'Ansættelses- og arbejdsret 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Ansættelses- og arbejdsret 5.9',
                               'key': '9',
                               'display_title': 'Ansættelses- og arbejdsret 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Ferie',
                      'id': 'Ansættelses- og arbejdsret 5',
                      'key': '5',
                      'display_title': 'Ansættelses- og arbejdsret 5 - Ferie'
                  },


                  'Ansættelses- og arbejdsret 6': {
                      'name': 'Pension (bortset fra tjenestemandspension)',
                      'id': 'Ansættelses- og arbejdsret 6',
                      'key': '6',
                      'display_title': 'Ansættelses- og arbejdsret 6 - Pension (bortset fra tjenestemandspension)'
                  },


                  'Ansættelses- og arbejdsret 7': {
                      'subs': {

                           'Ansættelses- og arbejdsret 7.1': {
                               'name': 'Kollektive aftaler',
                               'id': 'Ansættelses- og arbejdsret 7.1',
                               'key': '1',
                               'display_title': 'Ansættelses- og arbejdsret 7.1 - Kollektive aftaler'
                           },


                           'Ansættelses- og arbejdsret 7.2': {
                               'name': 'Arbejdsstandsning og arbejdsstridigheder',
                               'id': 'Ansættelses- og arbejdsret 7.2',
                               'key': '2',
                               'display_title': 'Ansættelses- og arbejdsret 7.2 - Arbejdsstandsning og arbejdsstridigheder'
                           },


                           'Ansættelses- og arbejdsret 7.3': {
                               'name': 'Arbejdsret og faglig voldgift',
                               'id': 'Ansættelses- og arbejdsret 7.3',
                               'key': '3',
                               'display_title': 'Ansættelses- og arbejdsret 7.3 - Arbejdsret og faglig voldgift'
                           },


                           'Ansættelses- og arbejdsret 7.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Ansættelses- og arbejdsret 7.9',
                               'key': '9',
                               'display_title': 'Ansættelses- og arbejdsret 7.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Kollektiv arbejdsret',
                      'id': 'Ansættelses- og arbejdsret 7',
                      'key': '7',
                      'display_title': 'Ansættelses- og arbejdsret 7 - Kollektiv arbejdsret'
                  },


                  'Ansættelses- og arbejdsret 8': {
                      'name': 'Arbejdsmiljø',
                      'id': 'Ansættelses- og arbejdsret 8',
                      'key': '8',
                      'display_title': 'Ansættelses- og arbejdsret 8 - Arbejdsmiljø'
                  },


                  'Ansættelses- og arbejdsret 9': {
                      'subs': {

                           'Ansættelses- og arbejdsret 9.1': {
                               'name': 'Lønmodtagernes Garantifond, se også Konkurs- og anden insolvensret 24.2',
                               'id': 'Ansættelses- og arbejdsret 9.1',
                               'key': '1',
                               'display_title': 'Ansættelses- og arbejdsret 9.1 - Lønmodtagernes Garantifond, se også Konkurs- og anden insolvensret 24.2'
                           },


                           'Ansættelses- og arbejdsret 9.2': {
                               'name': 'Retsstilling ved virksomhedsoverdragelse',
                               'id': 'Ansættelses- og arbejdsret 9.2',
                               'key': '2',
                               'display_title': 'Ansættelses- og arbejdsret 9.2 - Retsstilling ved virksomhedsoverdragelse'
                           },


                           'Ansættelses- og arbejdsret 9.3': {
                               'name': 'Ansættelsesbeviser',
                               'id': 'Ansættelses- og arbejdsret 9.3',
                               'key': '3',
                               'display_title': 'Ansættelses- og arbejdsret 9.3 - Ansættelsesbeviser'
                           },


                           'Ansættelses- og arbejdsret 9.9': {
                               'name': 'Andre ansættelsesforhold',
                               'id': 'Ansættelses- og arbejdsret 9.9',
                               'key': '9',
                               'display_title': 'Ansættelses- og arbejdsret 9.9 - Andre ansættelsesforhold'
                           }

                      },
                      'name': 'Andre spørgsmål',
                      'id': 'Ansættelses- og arbejdsret 9',
                      'key': '9',
                      'display_title': 'Ansættelses- og arbejdsret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Ansættelses- og arbejdsret',
             'id': 'Ansættelses- og arbejdsret',
             'key': 'ans_ttelses__og_arbejdsret',
             'display_title': 'Ansættelses- og arbejdsret'
         },


         'Arveret': {
             'subs': {

                  'Arveret 1': {
                      'subs': {

                           'Arveret 1.1': {
                               'name': 'Børn',
                               'id': 'Arveret 1.1',
                               'key': '1',
                               'display_title': 'Arveret 1.1 - Børn'
                           },


                           'Arveret 1.2': {
                               'name': 'Ægtefælle, herunder ægtefællefordele',
                               'id': 'Arveret 1.2',
                               'key': '2',
                               'display_title': 'Arveret 1.2 - Ægtefælle, herunder ægtefællefordele'
                           },


                           'Arveret 1.3': {
                               'name': 'Andre slægtninge',
                               'id': 'Arveret 1.3',
                               'key': '3',
                               'display_title': 'Arveret 1.3 - Andre slægtninge'
                           },


                           'Arveret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Arveret 1.9',
                               'key': '9',
                               'display_title': 'Arveret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Arv efter loven',
                      'id': 'Arveret 1',
                      'key': '1',
                      'display_title': 'Arveret 1 - Arv efter loven'
                  },


                  'Arveret 2': {
                      'subs': {

                           'Arveret 2.1': {
                               'subs': {

                                    'Arveret 21.1': {
                                        'name': 'Notartestamenter',
                                        'id': 'Arveret 21.1',
                                        'key': '1',
                                        'display_title': 'Arveret 21.1 - Notartestamenter'
                                    },


                                    'Arveret 21.2': {
                                        'name': 'Vidnetestamenter',
                                        'id': 'Arveret 21.2',
                                        'key': '2',
                                        'display_title': 'Arveret 21.2 - Vidnetestamenter'
                                    },


                                    'Arveret 21.3': {
                                        'name': 'Nødtestamenter',
                                        'id': 'Arveret 21.3',
                                        'key': '3',
                                        'display_title': 'Arveret 21.3 - Nødtestamenter'
                                    },


                                    'Arveret 21.4': {
                                        'subs': {

                                             'Arveret 214.1': {
                                                 'name': 'Fornuftsmangel',
                                                 'id': 'Arveret 214.1',
                                                 'key': '1',
                                                 'display_title': 'Arveret 214.1 - Fornuftsmangel'
                                             },


                                             'Arveret 214.2': {
                                                 'name': 'Misbrug m.v.',
                                                 'id': 'Arveret 214.2',
                                                 'key': '2',
                                                 'display_title': 'Arveret 214.2 - Misbrug m.v.'
                                             },


                                             'Arveret 214.9': {
                                                 'name': 'Andre mangelspørgsmål',
                                                 'id': 'Arveret 214.9',
                                                 'key': '9',
                                                 'display_title': 'Arveret 214.9 - Andre mangelspørgsmål'
                                             }

                                        },
                                        'name': 'Mangler',
                                        'id': 'Arveret 21.4',
                                        'key': '4',
                                        'display_title': 'Arveret 21.4 - Mangler'
                                    },


                                    'Arveret 21.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Arveret 21.9',
                                        'key': '9',
                                        'display_title': 'Arveret 21.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Oprettelse',
                               'id': 'Arveret 2.1',
                               'key': '1',
                               'display_title': 'Arveret 2.1 - Oprettelse'
                           },


                           'Arveret 2.2': {
                               'name': 'Indhold og fortolkning',
                               'id': 'Arveret 2.2',
                               'key': '2',
                               'display_title': 'Arveret 2.2 - Indhold og fortolkning'
                           },


                           'Arveret 2.3': {
                               'name': 'Tilbagekaldelse.',
                               'id': 'Arveret 2.3',
                               'key': '3',
                               'display_title': 'Arveret 2.3 - Tilbagekaldelse.'
                           },


                           'Arveret 2.4': {
                               'name': 'Gensidige testamenter',
                               'id': 'Arveret 2.4',
                               'key': '4',
                               'display_title': 'Arveret 2.4 - Gensidige testamenter'
                           },


                           'Arveret 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Arveret 2.9',
                               'key': '9',
                               'display_title': 'Arveret 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Testamentsarv',
                      'id': 'Arveret 2',
                      'key': '2',
                      'display_title': 'Arveret 2 - Testamentsarv'
                  },


                  'Arveret 3': {
                      'subs': {

                           'Arveret 3.1': {
                               'name': 'Udlevering',
                               'id': 'Arveret 3.1',
                               'key': '1',
                               'display_title': 'Arveret 3.1 - Udlevering'
                           },


                           'Arveret 3.2': {
                               'name': 'Rådighed',
                               'id': 'Arveret 3.2',
                               'key': '2',
                               'display_title': 'Arveret 3.2 - Rådighed'
                           },


                           'Arveret 3.3': {
                               'name': 'Misbrug',
                               'id': 'Arveret 3.3',
                               'key': '3',
                               'display_title': 'Arveret 3.3 - Misbrug'
                           },


                           'Arveret 3.4': {
                               'name': 'Hæftelse',
                               'id': 'Arveret 3.4',
                               'key': '4',
                               'display_title': 'Arveret 3.4 - Hæftelse'
                           },


                           'Arveret 3.5': {
                               'name': 'Skifte',
                               'id': 'Arveret 3.5',
                               'key': '5',
                               'display_title': 'Arveret 3.5 - Skifte'
                           },


                           'Arveret 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Arveret 3.9',
                               'key': '9',
                               'display_title': 'Arveret 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Uskiftet bo',
                      'id': 'Arveret 3',
                      'key': '3',
                      'display_title': 'Arveret 3 - Uskiftet bo'
                  },


                  'Arveret 4': {
                      'subs': {

                           'Arveret 4.1': {
                               'name': 'Arveafkald',
                               'id': 'Arveret 4.1',
                               'key': '1',
                               'display_title': 'Arveret 4.1 - Arveafkald'
                           },


                           'Arveret 4.2': {
                               'name': 'Arveforskud',
                               'id': 'Arveret 4.2',
                               'key': '2',
                               'display_title': 'Arveret 4.2 - Arveforskud'
                           },


                           'Arveret 4.3': {
                               'name': 'Uigenkaldelighed',
                               'id': 'Arveret 4.3',
                               'key': '3',
                               'display_title': 'Arveret 4.3 - Uigenkaldelighed'
                           },


                           'Arveret 4.4': {
                               'name': 'Fradømmelse af arveret',
                               'id': 'Arveret 4.4',
                               'key': '4',
                               'display_title': 'Arveret 4.4 - Fradømmelse af arveret'
                           },


                           'Arveret 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Arveret 4.9',
                               'key': '9',
                               'display_title': 'Arveret 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Arveafkald, arveforskud, arvepagter',
                      'id': 'Arveret 4',
                      'key': '4',
                      'display_title': 'Arveret 4 - Arveafkald, arveforskud, arvepagter'
                  },


                  'Arveret 5': {
                      'subs': {

                           'Arveret 5.1': {
                               'name': 'Tilsidesættelse m.v.',
                               'id': 'Arveret 5.1',
                               'key': '1',
                               'display_title': 'Arveret 5.1 - Tilsidesættelse m.v.'
                           },


                           'Arveret 5.2': {
                               'subs': {

                                    'Arveret 52.1': {
                                        'name': 'Fritagelse',
                                        'id': 'Arveret 52.1',
                                        'key': '1',
                                        'display_title': 'Arveret 52.1 - Fritagelse'
                                    },


                                    'Arveret 52.2': {
                                        'name': 'Frigivelse',
                                        'id': 'Arveret 52.2',
                                        'key': '2',
                                        'display_title': 'Arveret 52.2 - Frigivelse'
                                    },


                                    'Arveret 52.3': {
                                        'name': 'Administration',
                                        'id': 'Arveret 52.3',
                                        'key': '3',
                                        'display_title': 'Arveret 52.3 - Administration'
                                    },


                                    'Arveret 52.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Arveret 52.9',
                                        'key': '9',
                                        'display_title': 'Arveret 52.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Genstand',
                               'id': 'Arveret 5.2',
                               'key': '2',
                               'display_title': 'Arveret 5.2 - Genstand'
                           },


                           'Arveret 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Arveret 5.9',
                               'key': '9',
                               'display_title': 'Arveret 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Båndlæggelse',
                      'id': 'Arveret 5',
                      'key': '5',
                      'display_title': 'Arveret 5 - Båndlæggelse'
                  },


                  'Arveret 6': {
                      'name': 'Dødsgaver',
                      'id': 'Arveret 6',
                      'key': '6',
                      'display_title': 'Arveret 6 - Dødsgaver'
                  },


                  'Arveret 7': {
                      'name': 'Borteblevne',
                      'id': 'Arveret 7',
                      'key': '7',
                      'display_title': 'Arveret 7 - Borteblevne'
                  },


                  'Arveret 8': {
                      'name': 'Boafgifter, se Afgifter 3',
                      'id': 'Arveret 8',
                      'key': '8',
                      'display_title': 'Arveret 8 - Boafgifter, se Afgifter 3'
                  },


                  'Arveret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Arveret 9',
                      'key': '9',
                      'display_title': 'Arveret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Arveret',
             'id': 'Arveret',
             'key': 'arveret',
             'display_title': 'Arveret'
         },


         'Boligformer': {
             'subs': {

                  'Boligformer 1': {
                      'subs': {

                           'Boligformer 1.1': {
                               'name': 'Opdeling',
                               'id': 'Boligformer 1.1',
                               'key': '1',
                               'display_title': 'Boligformer 1.1 - Opdeling'
                           },


                           'Boligformer 1.2': {
                               'name': 'Foreningsforhold',
                               'id': 'Boligformer 1.2',
                               'key': '2',
                               'display_title': 'Boligformer 1.2 - Foreningsforhold'
                           },


                           'Boligformer 1.3': {
                               'subs': {

                                    'Boligformer 13.1': {
                                        'name': 'Salg',
                                        'id': 'Boligformer 13.1',
                                        'key': '1',
                                        'display_title': 'Boligformer 13.1 - Salg'
                                    },


                                    'Boligformer 13.2': {
                                        'name': 'Arv, ægteskabs- og samlivsophør o.l.',
                                        'id': 'Boligformer 13.2',
                                        'key': '2',
                                        'display_title': 'Boligformer 13.2 - Arv, ægteskabs- og samlivsophør o.l.'
                                    },


                                    'Boligformer 13.3': {
                                        'name': 'Pantsætning',
                                        'id': 'Boligformer 13.3',
                                        'key': '3',
                                        'display_title': 'Boligformer 13.3 - Pantsætning'
                                    },


                                    'Boligformer 13.4': {
                                        'name': 'Retsforfølgning',
                                        'id': 'Boligformer 13.4',
                                        'key': '4',
                                        'display_title': 'Boligformer 13.4 - Retsforfølgning'
                                    },


                                    'Boligformer 13.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Boligformer 13.9',
                                        'key': '9',
                                        'display_title': 'Boligformer 13.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Råden over ejerlejligheden',
                               'id': 'Boligformer 1.3',
                               'key': '3',
                               'display_title': 'Boligformer 1.3 - Råden over ejerlejligheden'
                           },


                           'Boligformer 1.4': {
                               'name': 'Skatter og afgifter',
                               'id': 'Boligformer 1.4',
                               'key': '4',
                               'display_title': 'Boligformer 1.4 - Skatter og afgifter'
                           },


                           'Boligformer 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Boligformer 1.9',
                               'key': '9',
                               'display_title': 'Boligformer 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Ejerlejligheder',
                      'id': 'Boligformer 1',
                      'key': '1',
                      'display_title': 'Boligformer 1 - Ejerlejligheder'
                  },


                  'Boligformer 2': {
                      'subs': {

                           'Boligformer 2.1': {
                               'name': 'Stiftelse af andelsboligforeninger',
                               'id': 'Boligformer 2.1',
                               'key': '1',
                               'display_title': 'Boligformer 2.1 - Stiftelse af andelsboligforeninger'
                           },


                           'Boligformer 2.2': {
                               'name': 'Foreningsforhold',
                               'id': 'Boligformer 2.2',
                               'key': '2',
                               'display_title': 'Boligformer 2.2 - Foreningsforhold'
                           },


                           'Boligformer 2.3': {
                               'subs': {

                                    'Boligformer 23.1': {
                                        'subs': {

                                             'Boligformer 231.1': {
                                                 'name': 'Prisfastsættelse',
                                                 'id': 'Boligformer 231.1',
                                                 'key': '1',
                                                 'display_title': 'Boligformer 231.1 - Prisfastsættelse'
                                             },


                                             'Boligformer 231.2': {
                                                 'name': 'Godkendelse',
                                                 'id': 'Boligformer 231.2',
                                                 'key': '2',
                                                 'display_title': 'Boligformer 231.2 - Godkendelse'
                                             },


                                             'Boligformer 231.3': {
                                                 'name': 'Frister',
                                                 'id': 'Boligformer 231.3',
                                                 'key': '3',
                                                 'display_title': 'Boligformer 231.3 - Frister'
                                             },


                                             'Boligformer 231.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Boligformer 231.9',
                                                 'key': '9',
                                                 'display_title': 'Boligformer 231.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Salg',
                                        'id': 'Boligformer 23.1',
                                        'key': '1',
                                        'display_title': 'Boligformer 23.1 - Salg'
                                    },


                                    'Boligformer 23.2': {
                                        'name': 'Arv, ægteskabs- og samlivsophør o.l.',
                                        'id': 'Boligformer 23.2',
                                        'key': '2',
                                        'display_title': 'Boligformer 23.2 - Arv, ægteskabs- og samlivsophør o.l.'
                                    },


                                    'Boligformer 23.3': {
                                        'name': 'Pantsætning',
                                        'id': 'Boligformer 23.3',
                                        'key': '3',
                                        'display_title': 'Boligformer 23.3 - Pantsætning'
                                    },


                                    'Boligformer 23.4': {
                                        'name': 'Retsforfølgning',
                                        'id': 'Boligformer 23.4',
                                        'key': '4',
                                        'display_title': 'Boligformer 23.4 - Retsforfølgning'
                                    },


                                    'Boligformer 23.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Boligformer 23.9',
                                        'key': '9',
                                        'display_title': 'Boligformer 23.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Råden',
                               'id': 'Boligformer 2.3',
                               'key': '3',
                               'display_title': 'Boligformer 2.3 - Råden'
                           },


                           'Boligformer 2.4': {
                               'name': 'Skat',
                               'id': 'Boligformer 2.4',
                               'key': '4',
                               'display_title': 'Boligformer 2.4 - Skat'
                           },


                           'Boligformer 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Boligformer 2.9',
                               'key': '9',
                               'display_title': 'Boligformer 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Andelsboliger',
                      'id': 'Boligformer 2',
                      'key': '2',
                      'display_title': 'Boligformer 2 - Andelsboliger'
                  },


                  'Boligformer 3': {
                      'subs': {

                           'Boligformer 3.1': {
                               'name': 'Almennyttige boligselskaber',
                               'id': 'Boligformer 3.1',
                               'key': '1',
                               'display_title': 'Boligformer 3.1 - Almennyttige boligselskaber'
                           },


                           'Boligformer 3.2': {
                               'name': 'Ungdomsboliger',
                               'id': 'Boligformer 3.2',
                               'key': '2',
                               'display_title': 'Boligformer 3.2 - Ungdomsboliger'
                           },


                           'Boligformer 3.3': {
                               'name': 'Ældreboliger',
                               'id': 'Boligformer 3.3',
                               'key': '3',
                               'display_title': 'Boligformer 3.3 - Ældreboliger'
                           },


                           'Boligformer 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Boligformer 3.9',
                               'key': '9',
                               'display_title': 'Boligformer 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Andre boligformer',
                      'id': 'Boligformer 3',
                      'key': '3',
                      'display_title': 'Boligformer 3 - Andre boligformer'
                  },


                  'Boligformer 4': {
                      'name': 'Byfornyelse (Sanering)',
                      'id': 'Boligformer 4',
                      'key': '4',
                      'display_title': 'Boligformer 4 - Byfornyelse (Sanering)'
                  },


                  'Boligformer 5': {
                      'subs': {

                           'Boligformer 5.1': {
                               'name': 'Beboelsesforbud',
                               'id': 'Boligformer 5.1',
                               'key': '1',
                               'display_title': 'Boligformer 5.1 - Beboelsesforbud'
                           },


                           'Boligformer 5.2': {
                               'name': 'Dusørregler',
                               'id': 'Boligformer 5.2',
                               'key': '2',
                               'display_title': 'Boligformer 5.2 - Dusørregler'
                           },


                           'Boligformer 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Boligformer 5.9',
                               'key': '9',
                               'display_title': 'Boligformer 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Offentligretlig boligregulering',
                      'id': 'Boligformer 5',
                      'key': '5',
                      'display_title': 'Boligformer 5 - Offentligretlig boligregulering'
                  },


                  'Boligformer 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Boligformer 9',
                      'key': '9',
                      'display_title': 'Boligformer 9 - Andre spørgsmål'
                  }

             },
             'name': 'Boligformer (se også Leje af fast ejendom)',
             'id': 'Boligformer',
             'key': 'boligformer',
             'display_title': 'Boligformer'
         },


         'Dødsboskifte': {
             'subs': {

                  'Dødsboskifte 1': {
                      'subs': {

                           'Dødsboskifte 1.1': {
                               'name': 'Kompetence',
                               'id': 'Dødsboskifte 1.1',
                               'key': '1',
                               'display_title': 'Dødsboskifte 1.1 - Kompetence'
                           },


                           'Dødsboskifte 1.2': {
                               'name': 'Dødsfaldskendelse',
                               'id': 'Dødsboskifte 1.2',
                               'key': '2',
                               'display_title': 'Dødsboskifte 1.2 - Dødsfaldskendelse'
                           },


                           'Dødsboskifte 1.3': {
                               'name': 'Skifterettens vejledning',
                               'id': 'Dødsboskifte 1.3',
                               'key': '3',
                               'display_title': 'Dødsboskifte 1.3 - Skifterettens vejledning'
                           },


                           'Dødsboskifte 1.4': {
                               'subs': {

                                    'Dødsboskifte 14.1': {
                                        'name': 'Udpegning',
                                        'id': 'Dødsboskifte 14.1',
                                        'key': '1',
                                        'display_title': 'Dødsboskifte 14.1 - Udpegning'
                                    },


                                    'Dødsboskifte 14.2': {
                                        'name': 'Midlertidig',
                                        'id': 'Dødsboskifte 14.2',
                                        'key': '2',
                                        'display_title': 'Dødsboskifte 14.2 - Midlertidig'
                                    },


                                    'Dødsboskifte 14.9': {
                                        'name': 'Andre bobestyrerspørgsmål',
                                        'id': 'Dødsboskifte 14.9',
                                        'key': '9',
                                        'display_title': 'Dødsboskifte 14.9 - Andre bobestyrerspørgsmål'
                                    }

                               },
                               'name': 'Bobestyrere',
                               'id': 'Dødsboskifte 1.4',
                               'key': '4',
                               'display_title': 'Dødsboskifte 1.4 - Bobestyrere'
                           },


                           'Dødsboskifte 1.5': {
                               'name': 'Skifteværger',
                               'id': 'Dødsboskifte 1.5',
                               'key': '5',
                               'display_title': 'Dødsboskifte 1.5 - Skifteværger'
                           },


                           'Dødsboskifte 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Dødsboskifte 1.9',
                               'key': '9',
                               'display_title': 'Dødsboskifte 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Skiftets begyndelse',
                      'id': 'Dødsboskifte 1',
                      'key': '1',
                      'display_title': 'Dødsboskifte 1 - Skiftets begyndelse'
                  },


                  'Dødsboskifte 2': {
                      'name': 'Boudlæg',
                      'id': 'Dødsboskifte 2',
                      'key': '2',
                      'display_title': 'Dødsboskifte 2 - Boudlæg'
                  },


                  'Dødsboskifte 3': {
                      'name': 'Ægtefælleudlæg',
                      'id': 'Dødsboskifte 3',
                      'key': '3',
                      'display_title': 'Dødsboskifte 3 - Ægtefælleudlæg'
                  },


                  'Dødsboskifte 4': {
                      'subs': {

                           'Dødsboskifte 4.1': {
                               'name': 'Almindeligt privat skifte',
                               'id': 'Dødsboskifte 4.1',
                               'key': '1',
                               'display_title': 'Dødsboskifte 4.1 - Almindeligt privat skifte'
                           },


                           'Dødsboskifte 4.2': {
                               'name': 'Forenklet privat skifte',
                               'id': 'Dødsboskifte 4.2',
                               'key': '2',
                               'display_title': 'Dødsboskifte 4.2 - Forenklet privat skifte'
                           },


                           'Dødsboskifte 4.3': {
                               'name': 'Sagsbehandlingsspørgsmål',
                               'id': 'Dødsboskifte 4.3',
                               'key': '3',
                               'display_title': 'Dødsboskifte 4.3 - Sagsbehandlingsspørgsmål'
                           },


                           'Dødsboskifte 4.9': {
                               'name': 'Andre spørgsmål, herunder skifte efter tidligere regler',
                               'id': 'Dødsboskifte 4.9',
                               'key': '9',
                               'display_title': 'Dødsboskifte 4.9 - Andre spørgsmål, herunder skifte efter tidligere regler'
                           }

                      },
                      'name': 'Privat skifte',
                      'id': 'Dødsboskifte 4',
                      'key': '4',
                      'display_title': 'Dødsboskifte 4 - Privat skifte'
                  },


                  'Dødsboskifte 5': {
                      'subs': {

                           'Dødsboskifte 5.1': {
                               'name': 'Solvente boer',
                               'id': 'Dødsboskifte 5.1',
                               'key': '1',
                               'display_title': 'Dødsboskifte 5.1 - Solvente boer'
                           },


                           'Dødsboskifte 5.2': {
                               'name': 'Insolvente boer',
                               'id': 'Dødsboskifte 5.2',
                               'key': '2',
                               'display_title': 'Dødsboskifte 5.2 - Insolvente boer'
                           },


                           'Dødsboskifte 5.3': {
                               'name': 'Sagsbehandlingsspørgsmål',
                               'id': 'Dødsboskifte 5.3',
                               'key': '3',
                               'display_title': 'Dødsboskifte 5.3 - Sagsbehandlingsspørgsmål'
                           },


                           'Dødsboskifte 5.9': {
                               'name': 'Andre spørgsmål, herunder skifte efter tidligere regler',
                               'id': 'Dødsboskifte 5.9',
                               'key': '9',
                               'display_title': 'Dødsboskifte 5.9 - Andre spørgsmål, herunder skifte efter tidligere regler'
                           }

                      },
                      'name': 'Behandling ved bobestyrer',
                      'id': 'Dødsboskifte 5',
                      'key': '5',
                      'display_title': 'Dødsboskifte 5 - Behandling ved bobestyrer'
                  },


                  'Dødsboskifte 6': {
                      'name': 'Præklusivt proklama',
                      'id': 'Dødsboskifte 6',
                      'key': '6',
                      'display_title': 'Dødsboskifte 6 - Præklusivt proklama'
                  },


                  'Dødsboskifte 7': {
                      'name': 'Omgørelse og genoptagelse',
                      'id': 'Dødsboskifte 7',
                      'key': '7',
                      'display_title': 'Dødsboskifte 7 - Omgørelse og genoptagelse'
                  },


                  'Dødsboskifte 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Dødsboskifte 9',
                      'key': '9',
                      'display_title': 'Dødsboskifte 9 - Andre spørgsmål'
                  }

             },
             'name': 'Dødsboskifte',
             'id': 'Dødsboskifte',
             'key': 'd_dsboskifte',
             'display_title': 'Dødsboskifte'
         },


         'Ejendomsret': {
             'subs': {

                  'Ejendomsret 1': {
                      'subs': {

                           'Ejendomsret 1.1': {
                               'name': 'Matrikelspørgsmål',
                               'id': 'Ejendomsret 1.1',
                               'key': '1',
                               'display_title': 'Ejendomsret 1.1 - Matrikelspørgsmål'
                           },


                           'Ejendomsret 1.2': {
                               'name': 'Inddæmning. Kystforhold. Diger. Øer',
                               'id': 'Ejendomsret 1.2',
                               'key': '2',
                               'display_title': 'Ejendomsret 1.2 - Inddæmning. Kystforhold. Diger. Øer'
                           },


                           'Ejendomsret 1.9': {
                               'name': 'Andre spørgsmål, herunder hævd',
                               'id': 'Ejendomsret 1.9',
                               'key': '9',
                               'display_title': 'Ejendomsret 1.9 - Andre spørgsmål, herunder hævd'
                           }

                      },
                      'name': 'Fast ejendom',
                      'id': 'Ejendomsret 1',
                      'key': '1',
                      'display_title': 'Ejendomsret 1 - Fast ejendom'
                  },


                  'Ejendomsret 2': {
                      'subs': {

                           'Ejendomsret 2.1': {
                               'name': 'Beskyttelse over for overdragerens kreditorer',
                               'id': 'Ejendomsret 2.1',
                               'key': '1',
                               'display_title': 'Ejendomsret 2.1 - Beskyttelse over for overdragerens kreditorer'
                           },


                           'Ejendomsret 2.2': {
                               'subs': {

                                    'Ejendomsret 22.1': {
                                        'name': 'Krav til aftalen',
                                        'id': 'Ejendomsret 22.1',
                                        'key': '1',
                                        'display_title': 'Ejendomsret 22.1 - Krav til aftalen'
                                    },


                                    'Ejendomsret 22.2': {
                                        'name': 'Mindsteudbetaling',
                                        'id': 'Ejendomsret 22.2',
                                        'key': '2',
                                        'display_title': 'Ejendomsret 22.2 - Mindsteudbetaling'
                                    },


                                    'Ejendomsret 22.3': {
                                        'name': 'Kontantforbehold',
                                        'id': 'Ejendomsret 22.3',
                                        'key': '3',
                                        'display_title': 'Ejendomsret 22.3 - Kontantforbehold'
                                    },


                                    'Ejendomsret 22.4': {
                                        'name': 'Eksstinktionsspørgsmål',
                                        'id': 'Ejendomsret 22.4',
                                        'key': '4',
                                        'display_title': 'Ejendomsret 22.4 - Eksstinktionsspørgsmål'
                                    },


                                    'Ejendomsret 22.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Ejendomsret 22.9',
                                        'key': '9',
                                        'display_title': 'Ejendomsret 22.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ejendomsforbehold',
                               'id': 'Ejendomsret 2.2',
                               'key': '2',
                               'display_title': 'Ejendomsret 2.2 - Ejendomsforbehold'
                           },


                           'Ejendomsret 2.3': {
                               'name': 'Konsignation',
                               'id': 'Ejendomsret 2.3',
                               'key': '3',
                               'display_title': 'Ejendomsret 2.3 - Konsignation'
                           },


                           'Ejendomsret 2.4': {
                               'name': 'Andre spørgsmål om forhold til erhververens kreditorer',
                               'id': 'Ejendomsret 2.4',
                               'key': '4',
                               'display_title': 'Ejendomsret 2.4 - Andre spørgsmål om forhold til erhververens kreditorer'
                           },


                           'Ejendomsret 2.5': {
                               'name': 'Andre spørgsmål om forhold til afledede aftaleerhververe',
                               'id': 'Ejendomsret 2.5',
                               'key': '5',
                               'display_title': 'Ejendomsret 2.5 - Andre spørgsmål om forhold til afledede aftaleerhververe'
                           },


                           'Ejendomsret 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Ejendomsret 2.9',
                               'key': '9',
                               'display_title': 'Ejendomsret 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Løsøre',
                      'id': 'Ejendomsret 2',
                      'key': '2',
                      'display_title': 'Ejendomsret 2 - Løsøre'
                  },


                  'Ejendomsret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Ejendomsret 9',
                      'key': '9',
                      'display_title': 'Ejendomsret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Ejendomsret (se også Landbrug m.v., Selskabsret, Statsforfatningsret, Tinglysning m.v.)',
             'id': 'Ejendomsret',
             'key': 'ejendomsret',
             'display_title': 'Ejendomsret'
         },


         'Entrepriseret': {
             'subs': {

                  'Entrepriseret 1': {
                      'subs': {

                           'Entrepriseret 1.1': {
                               'name': 'Tilbud og accept',
                               'id': 'Entrepriseret 1.1',
                               'key': '1',
                               'display_title': 'Entrepriseret 1.1 - Tilbud og accept'
                           },


                           'Entrepriseret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Entrepriseret 1.9',
                               'key': '9',
                               'display_title': 'Entrepriseret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Licitation',
                      'id': 'Entrepriseret 1',
                      'key': '1',
                      'display_title': 'Entrepriseret 1 - Licitation'
                  },


                  'Entrepriseret 2': {
                      'subs': {

                           'Entrepriseret 2.1': {
                               'name': 'Arbejdets omfang (ekstraarbejde)',
                               'id': 'Entrepriseret 2.1',
                               'key': '1',
                               'display_title': 'Entrepriseret 2.1 - Arbejdets omfang (ekstraarbejde)'
                           },


                           'Entrepriseret 2.2': {
                               'subs': {

                                    'Entrepriseret 22.1': {
                                        'name': 'Forsinkelse',
                                        'id': 'Entrepriseret 22.1',
                                        'key': '1',
                                        'display_title': 'Entrepriseret 22.1 - Forsinkelse'
                                    },


                                    'Entrepriseret 22.2': {
                                        'name': 'Mangler',
                                        'id': 'Entrepriseret 22.2',
                                        'key': '2',
                                        'display_title': 'Entrepriseret 22.2 - Mangler'
                                    },


                                    'Entrepriseret 22.3': {
                                        'name': 'Rådgivningsansvar',
                                        'id': 'Entrepriseret 22.3',
                                        'key': '3',
                                        'display_title': 'Entrepriseret 22.3 - Rådgivningsansvar'
                                    },


                                    'Entrepriseret 22.4': {
                                        'name': 'Erstatningsopgørelsen m.v.',
                                        'id': 'Entrepriseret 22.4',
                                        'key': '4',
                                        'display_title': 'Entrepriseret 22.4 - Erstatningsopgørelsen m.v.'
                                    },


                                    'Entrepriseret 22.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Entrepriseret 22.9',
                                        'key': '9',
                                        'display_title': 'Entrepriseret 22.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Misligholdelse',
                               'id': 'Entrepriseret 2.2',
                               'key': '2',
                               'display_title': 'Entrepriseret 2.2 - Misligholdelse'
                           },


                           'Entrepriseret 2.3': {
                               'name': 'Betaling, sikkerhed, insolvens',
                               'id': 'Entrepriseret 2.3',
                               'key': '3',
                               'display_title': 'Entrepriseret 2.3 - Betaling, sikkerhed, insolvens'
                           },


                           'Entrepriseret 2.4': {
                               'name': 'Dagbøder',
                               'id': 'Entrepriseret 2.4',
                               'key': '4',
                               'display_title': 'Entrepriseret 2.4 - Dagbøder'
                           },


                           'Entrepriseret 2.5': {
                               'subs': {

                                    'Entrepriseret 25.1': {
                                        'name': 'Syn og skøn, se også Retspleje 14.2',
                                        'id': 'Entrepriseret 25.1',
                                        'key': '1',
                                        'display_title': 'Entrepriseret 25.1 - Syn og skøn, se også Retspleje 14.2'
                                    },


                                    'Entrepriseret 25.2': {
                                        'name': 'Voldgift, se også Retspleje 2.8',
                                        'id': 'Entrepriseret 25.2',
                                        'key': '2',
                                        'display_title': 'Entrepriseret 25.2 - Voldgift, se også Retspleje 2.8'
                                    },


                                    'Entrepriseret 25.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Entrepriseret 25.9',
                                        'key': '9',
                                        'display_title': 'Entrepriseret 25.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Tvister',
                               'id': 'Entrepriseret 2.5',
                               'key': '5',
                               'display_title': 'Entrepriseret 2.5 - Tvister'
                           },


                           'Entrepriseret 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Entrepriseret 2.9',
                               'key': '9',
                               'display_title': 'Entrepriseret 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Entreprise',
                      'id': 'Entrepriseret 2',
                      'key': '2',
                      'display_title': 'Entrepriseret 2 - Entreprise'
                  },


                  'Entrepriseret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Entrepriseret 9',
                      'key': '9',
                      'display_title': 'Entrepriseret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Entrepriseret',
             'id': 'Entrepriseret',
             'key': 'entrepriseret',
             'display_title': 'Entrepriseret'
         },


         'Erhvervsret': {
             'subs': {

                  'Erhvervsret 1': {
                      'subs': {

                           'Erhvervsret 1.1': {
                               'name': 'Registre, se også Personspørgsmål 1',
                               'id': 'Erhvervsret 1.1',
                               'key': '1',
                               'display_title': 'Erhvervsret 1.1 - Registre, se også Personspørgsmål 1'
                           },


                           'Erhvervsret 1.2': {
                               'name': 'Bogføring',
                               'id': 'Erhvervsret 1.2',
                               'key': '2',
                               'display_title': 'Erhvervsret 1.2 - Bogføring'
                           },


                           'Erhvervsret 1.3': {
                               'name': 'Næringslov',
                               'id': 'Erhvervsret 1.3',
                               'key': '3',
                               'display_title': 'Erhvervsret 1.3 - Næringslov'
                           },


                           'Erhvervsret 1.4': {
                               'name': 'Butikstid',
                               'id': 'Erhvervsret 1.4',
                               'key': '4',
                               'display_title': 'Erhvervsret 1.4 - Butikstid'
                           },


                           'Erhvervsret 1.5': {
                               'name': 'Standardisering',
                               'id': 'Erhvervsret 1.5',
                               'key': '5',
                               'display_title': 'Erhvervsret 1.5 - Standardisering'
                           },


                           'Erhvervsret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Erhvervsret 1.9',
                               'key': '9',
                               'display_title': 'Erhvervsret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Almindelige emner',
                      'id': 'Erhvervsret 1',
                      'key': '1',
                      'display_title': 'Erhvervsret 1 - Almindelige emner'
                  },


                  'Erhvervsret 2': {
                      'subs': {

                           'Erhvervsret 2.1': {
                               'name': 'Forlystelser',
                               'id': 'Erhvervsret 2.1',
                               'key': '1',
                               'display_title': 'Erhvervsret 2.1 - Forlystelser'
                           },


                           'Erhvervsret 2.2': {
                               'name': 'Liberale erhverv, se også Aftaler 5.3, Retspleje 1.2, Selskabsret 13.4',
                               'id': 'Erhvervsret 2.2',
                               'key': '2',
                               'display_title': 'Erhvervsret 2.2 - Liberale erhverv, se også Aftaler 5.3, Retspleje 1.2, Selskabsret 13.4'
                           },


                           'Erhvervsret 2.3': {
                               'name': 'Energiforsyning',
                               'id': 'Erhvervsret 2.3',
                               'key': '3',
                               'display_title': 'Erhvervsret 2.3 - Energiforsyning'
                           },


                           'Erhvervsret 2.4': {
                               'name': 'Lotteri, spil, tipning',
                               'id': 'Erhvervsret 2.4',
                               'key': '4',
                               'display_title': 'Erhvervsret 2.4 - Lotteri, spil, tipning'
                           },


                           'Erhvervsret 2.5': {
                               'name': 'Brugthandel',
                               'id': 'Erhvervsret 2.5',
                               'key': '5',
                               'display_title': 'Erhvervsret 2.5 - Brugthandel'
                           },


                           'Erhvervsret 2.6': {
                               'name': 'Hoteller og restaurationer',
                               'id': 'Erhvervsret 2.6',
                               'key': '6',
                               'display_title': 'Erhvervsret 2.6 - Hoteller og restaurationer'
                           },


                           'Erhvervsret 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Erhvervsret 2.9',
                               'key': '9',
                               'display_title': 'Erhvervsret 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Særlige emner',
                      'id': 'Erhvervsret 2',
                      'key': '2',
                      'display_title': 'Erhvervsret 2 - Særlige emner'
                  },


                  'Erhvervsret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Erhvervsret 9',
                      'key': '9',
                      'display_title': 'Erhvervsret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Erhvervsret (se også Markedsret)',
             'id': 'Erhvervsret',
             'key': 'erhvervsret',
             'display_title': 'Erhvervsret'
         },


         'Erstatning uden for kontraktforhold': {
             'subs': {

                  'Erstatning uden for kontraktforhold 1': {
                      'subs': {

                           'Erstatning uden for kontraktforhold 1.1': {
                               'subs': {

                                    'Erstatning uden for kontraktforhold 11.1': {
                                        'subs': {

                                             'Erstatning uden for kontraktforhold 111.1': {
                                                 'name': 'Privates ansvar, se også Aftaler 6, Færdselsret 43.4 og 6, Immaterialret 53.3, Retspleje 12.3 og 3.9 samt Selskabsret 14.2',
                                                 'id': 'Erstatning uden for kontraktforhold 111.1',
                                                 'key': '1',
                                                 'display_title': 'Erstatning uden for kontraktforhold 111.1 - Privates ansvar, se også Aftaler 6, Færdselsret 43.4 og 6, Immaterialret 53.3, Retspleje 12.3 og 3.9 samt Selskabsret 14.2'
                                             },


                                             'Erstatning uden for kontraktforhold 111.2': {
                                                 'name': 'Offentligt ansvar',
                                                 'id': 'Erstatning uden for kontraktforhold 111.2',
                                                 'key': '2',
                                                 'display_title': 'Erstatning uden for kontraktforhold 111.2 - Offentligt ansvar'
                                             },


                                             'Erstatning uden for kontraktforhold 111.3': {
                                                 'name': 'Undladelser',
                                                 'id': 'Erstatning uden for kontraktforhold 111.3',
                                                 'key': '3',
                                                 'display_title': 'Erstatning uden for kontraktforhold 111.3 - Undladelser'
                                             },


                                             'Erstatning uden for kontraktforhold 111.4': {
                                                 'name': 'Professionsansvar',
                                                 'id': 'Erstatning uden for kontraktforhold 111.4',
                                                 'key': '4',
                                                 'display_title': 'Erstatning uden for kontraktforhold 111.4 - Professionsansvar'
                                             },


                                             'Erstatning uden for kontraktforhold 111.5': {
                                                 'name': 'Børns erstatningsansvar',
                                                 'id': 'Erstatning uden for kontraktforhold 111.5',
                                                 'key': '5',
                                                 'display_title': 'Erstatning uden for kontraktforhold 111.5 - Børns erstatningsansvar'
                                             },


                                             'Erstatning uden for kontraktforhold 111.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Erstatning uden for kontraktforhold 111.9',
                                                 'key': '9',
                                                 'display_title': 'Erstatning uden for kontraktforhold 111.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Culpa',
                                        'id': 'Erstatning uden for kontraktforhold 11.1',
                                        'key': '1',
                                        'display_title': 'Erstatning uden for kontraktforhold 11.1 - Culpa'
                                    },


                                    'Erstatning uden for kontraktforhold 11.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Erstatning uden for kontraktforhold 11.9',
                                        'key': '9',
                                        'display_title': 'Erstatning uden for kontraktforhold 11.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Culpa',
                               'id': 'Erstatning uden for kontraktforhold 1.1',
                               'key': '1',
                               'display_title': 'Erstatning uden for kontraktforhold 1.1 - Culpa'
                           },


                           'Erstatning uden for kontraktforhold 1.2': {
                               'name': 'Formodningsansvar (Omvendt bevisbyrde)',
                               'id': 'Erstatning uden for kontraktforhold 1.2',
                               'key': '2',
                               'display_title': 'Erstatning uden for kontraktforhold 1.2 - Formodningsansvar (Omvendt bevisbyrde)'
                           },


                           'Erstatning uden for kontraktforhold 1.3': {
                               'name': 'Principalansvar (Husbondansvar)',
                               'id': 'Erstatning uden for kontraktforhold 1.3',
                               'key': '3',
                               'display_title': 'Erstatning uden for kontraktforhold 1.3 - Principalansvar (Husbondansvar)'
                           },


                           'Erstatning uden for kontraktforhold 1.4': {
                               'subs': {

                                    'Erstatning uden for kontraktforhold 14.1': {
                                        'subs': {

                                             'Erstatning uden for kontraktforhold 141.1': {
                                                 'name': 'Færdselslov, se Færdselsret 6',
                                                 'id': 'Erstatning uden for kontraktforhold 141.1',
                                                 'key': '1',
                                                 'display_title': 'Erstatning uden for kontraktforhold 141.1 - Færdselslov, se Færdselsret 6'
                                             },


                                             'Erstatning uden for kontraktforhold 141.2': {
                                                 'name': 'Hundelov',
                                                 'id': 'Erstatning uden for kontraktforhold 141.2',
                                                 'key': '2',
                                                 'display_title': 'Erstatning uden for kontraktforhold 141.2 - Hundelov'
                                             },


                                             'Erstatning uden for kontraktforhold 141.3': {
                                                 'name': 'Jernbaner m.v.',
                                                 'id': 'Erstatning uden for kontraktforhold 141.3',
                                                 'key': '3',
                                                 'display_title': 'Erstatning uden for kontraktforhold 141.3 - Jernbaner m.v.'
                                             },


                                             'Erstatning uden for kontraktforhold 141.4': {
                                                 'name': 'Luftfartslov',
                                                 'id': 'Erstatning uden for kontraktforhold 141.4',
                                                 'key': '4',
                                                 'display_title': 'Erstatning uden for kontraktforhold 141.4 - Luftfartslov'
                                             },


                                             'Erstatning uden for kontraktforhold 141.9': {
                                                 'name': 'Andre love',
                                                 'id': 'Erstatning uden for kontraktforhold 141.9',
                                                 'key': '9',
                                                 'display_title': 'Erstatning uden for kontraktforhold 141.9 - Andre love'
                                             }

                                        },
                                        'name': 'Lovhjemlet objektivt ansvar',
                                        'id': 'Erstatning uden for kontraktforhold 14.1',
                                        'key': '1',
                                        'display_title': 'Erstatning uden for kontraktforhold 14.1 - Lovhjemlet objektivt ansvar'
                                    },


                                    'Erstatning uden for kontraktforhold 14.2': {
                                        'subs': {

                                             'Erstatning uden for kontraktforhold 142.1': {
                                                 'name': 'Offentligt ansvar',
                                                 'id': 'Erstatning uden for kontraktforhold 142.1',
                                                 'key': '1',
                                                 'display_title': 'Erstatning uden for kontraktforhold 142.1 - Offentligt ansvar'
                                             },


                                             'Erstatning uden for kontraktforhold 142.2': {
                                                 'name': 'Privat ansvar',
                                                 'id': 'Erstatning uden for kontraktforhold 142.2',
                                                 'key': '2',
                                                 'display_title': 'Erstatning uden for kontraktforhold 142.2 - Privat ansvar'
                                             },


                                             'Erstatning uden for kontraktforhold 142.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Erstatning uden for kontraktforhold 142.9',
                                                 'key': '9',
                                                 'display_title': 'Erstatning uden for kontraktforhold 142.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Objektivt ansvar uden lovhjemmel',
                                        'id': 'Erstatning uden for kontraktforhold 14.2',
                                        'key': '2',
                                        'display_title': 'Erstatning uden for kontraktforhold 14.2 - Objektivt ansvar uden lovhjemmel'
                                    },


                                    'Erstatning uden for kontraktforhold 14.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Erstatning uden for kontraktforhold 14.9',
                                        'key': '9',
                                        'display_title': 'Erstatning uden for kontraktforhold 14.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Objektivt ansvar',
                               'id': 'Erstatning uden for kontraktforhold 1.4',
                               'key': '4',
                               'display_title': 'Erstatning uden for kontraktforhold 1.4 - Objektivt ansvar'
                           },


                           'Erstatning uden for kontraktforhold 1.5': {
                               'name': 'Produktansvar',
                               'id': 'Erstatning uden for kontraktforhold 1.5',
                               'key': '5',
                               'display_title': 'Erstatning uden for kontraktforhold 1.5 - Produktansvar'
                           },


                           'Erstatning uden for kontraktforhold 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Erstatning uden for kontraktforhold 1.9',
                               'key': '9',
                               'display_title': 'Erstatning uden for kontraktforhold 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Ansvarsgrundlag',
                      'id': 'Erstatning uden for kontraktforhold 1',
                      'key': '1',
                      'display_title': 'Erstatning uden for kontraktforhold 1 - Ansvarsgrundlag'
                  },


                  'Erstatning uden for kontraktforhold 2': {
                      'subs': {

                           'Erstatning uden for kontraktforhold 2.1': {
                               'subs': {

                                    'Erstatning uden for kontraktforhold 21.1': {
                                        'name': 'Beviskrav',
                                        'id': 'Erstatning uden for kontraktforhold 21.1',
                                        'key': '1',
                                        'display_title': 'Erstatning uden for kontraktforhold 21.1 - Beviskrav'
                                    },


                                    'Erstatning uden for kontraktforhold 21.2': {
                                        'name': 'Bevisbyrde',
                                        'id': 'Erstatning uden for kontraktforhold 21.2',
                                        'key': '2',
                                        'display_title': 'Erstatning uden for kontraktforhold 21.2 - Bevisbyrde'
                                    },


                                    'Erstatning uden for kontraktforhold 21.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Erstatning uden for kontraktforhold 21.9',
                                        'key': '9',
                                        'display_title': 'Erstatning uden for kontraktforhold 21.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Årsagsforbindelse',
                               'id': 'Erstatning uden for kontraktforhold 2.1',
                               'key': '1',
                               'display_title': 'Erstatning uden for kontraktforhold 2.1 - Årsagsforbindelse'
                           },


                           'Erstatning uden for kontraktforhold 2.2': {
                               'subs': {

                                    'Erstatning uden for kontraktforhold 22.1': {
                                        'name': 'Tingsskade',
                                        'id': 'Erstatning uden for kontraktforhold 22.1',
                                        'key': '1',
                                        'display_title': 'Erstatning uden for kontraktforhold 22.1 - Tingsskade'
                                    },


                                    'Erstatning uden for kontraktforhold 22.2': {
                                        'name': 'Personskade',
                                        'id': 'Erstatning uden for kontraktforhold 22.2',
                                        'key': '2',
                                        'display_title': 'Erstatning uden for kontraktforhold 22.2 - Personskade'
                                    },


                                    'Erstatning uden for kontraktforhold 22.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Erstatning uden for kontraktforhold 22.9',
                                        'key': '9',
                                        'display_title': 'Erstatning uden for kontraktforhold 22.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Adækvans',
                               'id': 'Erstatning uden for kontraktforhold 2.2',
                               'key': '2',
                               'display_title': 'Erstatning uden for kontraktforhold 2.2 - Adækvans'
                           },


                           'Erstatning uden for kontraktforhold 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Erstatning uden for kontraktforhold 2.9',
                               'key': '9',
                               'display_title': 'Erstatning uden for kontraktforhold 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Årsagsforbindelse og adækvans',
                      'id': 'Erstatning uden for kontraktforhold 2',
                      'key': '2',
                      'display_title': 'Erstatning uden for kontraktforhold 2 - Årsagsforbindelse og adækvans'
                  },


                  'Erstatning uden for kontraktforhold 3': {
                      'subs': {

                           'Erstatning uden for kontraktforhold 3.1': {
                               'subs': {

                                    'Erstatning uden for kontraktforhold 31.1': {
                                        'name': 'Værditab',
                                        'id': 'Erstatning uden for kontraktforhold 31.1',
                                        'key': '1',
                                        'display_title': 'Erstatning uden for kontraktforhold 31.1 - Værditab'
                                    },


                                    'Erstatning uden for kontraktforhold 31.2': {
                                        'name': 'Drifts- og afsavnserstatning',
                                        'id': 'Erstatning uden for kontraktforhold 31.2',
                                        'key': '2',
                                        'display_title': 'Erstatning uden for kontraktforhold 31.2 - Drifts- og afsavnserstatning'
                                    },


                                    'Erstatning uden for kontraktforhold 31.3': {
                                        'name': 'Andre tab',
                                        'id': 'Erstatning uden for kontraktforhold 31.3',
                                        'key': '3',
                                        'display_title': 'Erstatning uden for kontraktforhold 31.3 - Andre tab'
                                    },


                                    'Erstatning uden for kontraktforhold 31.4': {
                                        'name': 'Tabsbegrænsning',
                                        'id': 'Erstatning uden for kontraktforhold 31.4',
                                        'key': '4',
                                        'display_title': 'Erstatning uden for kontraktforhold 31.4 - Tabsbegrænsning'
                                    },


                                    'Erstatning uden for kontraktforhold 31.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Erstatning uden for kontraktforhold 31.9',
                                        'key': '9',
                                        'display_title': 'Erstatning uden for kontraktforhold 31.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Tingsskade',
                               'id': 'Erstatning uden for kontraktforhold 3.1',
                               'key': '1',
                               'display_title': 'Erstatning uden for kontraktforhold 3.1 - Tingsskade'
                           },


                           'Erstatning uden for kontraktforhold 3.2': {
                               'subs': {

                                    'Erstatning uden for kontraktforhold 32.1': {
                                        'subs': {

                                             'Erstatning uden for kontraktforhold 321.1': {
                                                 'subs': {

                                                      'Erstatning uden for kontraktforhold 3211.1': {
                                                          'name': 'Helbredelsesudgifter m.v.',
                                                          'id': 'Erstatning uden for kontraktforhold 3211.1',
                                                          'key': '1',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3211.1 - Helbredelsesudgifter m.v.'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3211.2': {
                                                          'name': 'Tabt arbejdsfortjeneste',
                                                          'id': 'Erstatning uden for kontraktforhold 3211.2',
                                                          'key': '2',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3211.2 - Tabt arbejdsfortjeneste'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3211.3': {
                                                          'name': 'Svie og smerte',
                                                          'id': 'Erstatning uden for kontraktforhold 3211.3',
                                                          'key': '3',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3211.3 - Svie og smerte'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3211.4': {
                                                          'name': 'Varigt mén',
                                                          'id': 'Erstatning uden for kontraktforhold 3211.4',
                                                          'key': '4',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3211.4 - Varigt mén'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3211.5': {
                                                          'name': 'Erhvervsevnetab',
                                                          'id': 'Erstatning uden for kontraktforhold 3211.5',
                                                          'key': '5',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3211.5 - Erhvervsevnetab'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3211.6': {
                                                          'subs': {

                                                               'Erstatning uden for kontraktforhold 32116.1': {
                                                                   'name': 'Ægtefælle eller samlever',
                                                                   'id': 'Erstatning uden for kontraktforhold 32116.1',
                                                                   'key': '1',
                                                                   'display_title': 'Erstatning uden for kontraktforhold 32116.1 - Ægtefælle eller samlever'
                                                               },


                                                               'Erstatning uden for kontraktforhold 32116.2': {
                                                                   'name': 'Børn',
                                                                   'id': 'Erstatning uden for kontraktforhold 32116.2',
                                                                   'key': '2',
                                                                   'display_title': 'Erstatning uden for kontraktforhold 32116.2 - Børn'
                                                               },


                                                               'Erstatning uden for kontraktforhold 32116.3': {
                                                                   'name': 'Andre',
                                                                   'id': 'Erstatning uden for kontraktforhold 32116.3',
                                                                   'key': '3',
                                                                   'display_title': 'Erstatning uden for kontraktforhold 32116.3 - Andre'
                                                               },


                                                               'Erstatning uden for kontraktforhold 32116.9': {
                                                                   'name': 'Andre spørgsmål',
                                                                   'id': 'Erstatning uden for kontraktforhold 32116.9',
                                                                   'key': '9',
                                                                   'display_title': 'Erstatning uden for kontraktforhold 32116.9 - Andre spørgsmål'
                                                               }

                                                          },
                                                          'name': 'Forsørgertab',
                                                          'id': 'Erstatning uden for kontraktforhold 3211.6',
                                                          'key': '6',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3211.6 - Forsørgertab'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3211.7': {
                                                          'name': 'Tort',
                                                          'id': 'Erstatning uden for kontraktforhold 3211.7',
                                                          'key': '7',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3211.7 - Tort'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3211.9': {
                                                          'name': 'Andet tab',
                                                          'id': 'Erstatning uden for kontraktforhold 3211.9',
                                                          'key': '9',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3211.9 - Andet tab'
                                                      }

                                                 },
                                                 'name': 'Erstatnings- og godtgørelsesposterne',
                                                 'id': 'Erstatning uden for kontraktforhold 321.1',
                                                 'key': '1',
                                                 'display_title': 'Erstatning uden for kontraktforhold 321.1 - Erstatnings- og godtgørelsesposterne'
                                             },


                                             'Erstatning uden for kontraktforhold 321.2': {
                                                 'subs': {

                                                      'Erstatning uden for kontraktforhold 3212.1': {
                                                          'name': 'Ydelser efter lov om arbejdsskadeforsikring, se også Erstatning uden for kontraktforhold 32.3 og Forvaltningsret 26.1',
                                                          'id': 'Erstatning uden for kontraktforhold 3212.1',
                                                          'key': '1',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3212.1 - Ydelser efter lov om arbejdsskadeforsikring, se også Erstatning uden for kontraktforhold 32.3 og Forvaltningsret 26.1'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3212.2': {
                                                          'name': 'Private forsikringer',
                                                          'id': 'Erstatning uden for kontraktforhold 3212.2',
                                                          'key': '2',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3212.2 - Private forsikringer'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3212.3': {
                                                          'name': 'Sociale ydelser',
                                                          'id': 'Erstatning uden for kontraktforhold 3212.3',
                                                          'key': '3',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3212.3 - Sociale ydelser'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3212.4': {
                                                          'name': 'Andre ydelser',
                                                          'id': 'Erstatning uden for kontraktforhold 3212.4',
                                                          'key': '4',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3212.4 - Andre ydelser'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3212.5': {
                                                          'name': 'Regres',
                                                          'id': 'Erstatning uden for kontraktforhold 3212.5',
                                                          'key': '5',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3212.5 - Regres'
                                                      },


                                                      'Erstatning uden for kontraktforhold 3212.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Erstatning uden for kontraktforhold 3212.9',
                                                          'key': '9',
                                                          'display_title': 'Erstatning uden for kontraktforhold 3212.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Ydelser fra andre',
                                                 'id': 'Erstatning uden for kontraktforhold 321.2',
                                                 'key': '2',
                                                 'display_title': 'Erstatning uden for kontraktforhold 321.2 - Ydelser fra andre'
                                             },


                                             'Erstatning uden for kontraktforhold 321.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Erstatning uden for kontraktforhold 321.9',
                                                 'key': '9',
                                                 'display_title': 'Erstatning uden for kontraktforhold 321.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Erstatningsansvarsloven',
                                        'id': 'Erstatning uden for kontraktforhold 32.1',
                                        'key': '1',
                                        'display_title': 'Erstatning uden for kontraktforhold 32.1 - Erstatningsansvarsloven'
                                    },


                                    'Erstatning uden for kontraktforhold 32.2': {
                                        'name': 'Lov om erstatning fra staten til ofre for forbrydelser',
                                        'id': 'Erstatning uden for kontraktforhold 32.2',
                                        'key': '2',
                                        'display_title': 'Erstatning uden for kontraktforhold 32.2 - Lov om erstatning fra staten til ofre for forbrydelser'
                                    },


                                    'Erstatning uden for kontraktforhold 32.3': {
                                        'name': 'Lov om arbejdsskadeforsikring, se også Forvaltningsret 26.1',
                                        'id': 'Erstatning uden for kontraktforhold 32.3',
                                        'key': '3',
                                        'display_title': 'Erstatning uden for kontraktforhold 32.3 - Lov om arbejdsskadeforsikring, se også Forvaltningsret 26.1'
                                    },


                                    'Erstatning uden for kontraktforhold 32.9': {
                                        'name': 'Andre spørgsmål, herunder erstatningsposter før EAL',
                                        'id': 'Erstatning uden for kontraktforhold 32.9',
                                        'key': '9',
                                        'display_title': 'Erstatning uden for kontraktforhold 32.9 - Andre spørgsmål, herunder erstatningsposter før EAL'
                                    }

                               },
                               'name': 'Personskade',
                               'id': 'Erstatning uden for kontraktforhold 3.2',
                               'key': '2',
                               'display_title': 'Erstatning uden for kontraktforhold 3.2 - Personskade'
                           },


                           'Erstatning uden for kontraktforhold 3.3': {
                               'name': 'Almindelig formueskade',
                               'id': 'Erstatning uden for kontraktforhold 3.3',
                               'key': '3',
                               'display_title': 'Erstatning uden for kontraktforhold 3.3 - Almindelig formueskade'
                           },


                           'Erstatning uden for kontraktforhold 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Erstatning uden for kontraktforhold 3.9',
                               'key': '9',
                               'display_title': 'Erstatning uden for kontraktforhold 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Erstatningsberegning',
                      'id': 'Erstatning uden for kontraktforhold 3',
                      'key': '3',
                      'display_title': 'Erstatning uden for kontraktforhold 3 - Erstatningsberegning'
                  },


                  'Erstatning uden for kontraktforhold 4': {
                      'subs': {

                           'Erstatning uden for kontraktforhold 4.1': {
                               'subs': {

                                    'Erstatning uden for kontraktforhold 41.1': {
                                        'name': 'Tingsskade',
                                        'id': 'Erstatning uden for kontraktforhold 41.1',
                                        'key': '1',
                                        'display_title': 'Erstatning uden for kontraktforhold 41.1 - Tingsskade'
                                    },


                                    'Erstatning uden for kontraktforhold 41.2': {
                                        'name': 'Personskade',
                                        'id': 'Erstatning uden for kontraktforhold 41.2',
                                        'key': '2',
                                        'display_title': 'Erstatning uden for kontraktforhold 41.2 - Personskade'
                                    },


                                    'Erstatning uden for kontraktforhold 41.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Erstatning uden for kontraktforhold 41.9',
                                        'key': '9',
                                        'display_title': 'Erstatning uden for kontraktforhold 41.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Kravets beskyttelse og overførelse',
                               'id': 'Erstatning uden for kontraktforhold 4.1',
                               'key': '1',
                               'display_title': 'Erstatning uden for kontraktforhold 4.1 - Kravets beskyttelse og overførelse'
                           },


                           'Erstatning uden for kontraktforhold 4.2': {
                               'name': 'Kravets forældelse, se også Pengevæsen m.v. 5.8',
                               'id': 'Erstatning uden for kontraktforhold 4.2',
                               'key': '2',
                               'display_title': 'Erstatning uden for kontraktforhold 4.2 - Kravets forældelse, se også Pengevæsen m.v. 5.8'
                           },


                           'Erstatning uden for kontraktforhold 4.3': {
                               'subs': {

                                    'Erstatning uden for kontraktforhold 43.1': {
                                        'name': 'Tingsskade',
                                        'id': 'Erstatning uden for kontraktforhold 43.1',
                                        'key': '1',
                                        'display_title': 'Erstatning uden for kontraktforhold 43.1 - Tingsskade'
                                    },


                                    'Erstatning uden for kontraktforhold 43.2': {
                                        'name': 'Personskade',
                                        'id': 'Erstatning uden for kontraktforhold 43.2',
                                        'key': '2',
                                        'display_title': 'Erstatning uden for kontraktforhold 43.2 - Personskade'
                                    },


                                    'Erstatning uden for kontraktforhold 43.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Erstatning uden for kontraktforhold 43.9',
                                        'key': '9',
                                        'display_title': 'Erstatning uden for kontraktforhold 43.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Kravets forrentning',
                               'id': 'Erstatning uden for kontraktforhold 4.3',
                               'key': '3',
                               'display_title': 'Erstatning uden for kontraktforhold 4.3 - Kravets forrentning'
                           },


                           'Erstatning uden for kontraktforhold 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Erstatning uden for kontraktforhold 4.9',
                               'key': '9',
                               'display_title': 'Erstatning uden for kontraktforhold 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Generelle spørgsmål vedrørende erstatningskrav',
                      'id': 'Erstatning uden for kontraktforhold 4',
                      'key': '4',
                      'display_title': 'Erstatning uden for kontraktforhold 4 - Generelle spørgsmål vedrørende erstatningskrav'
                  },


                  'Erstatning uden for kontraktforhold 5': {
                      'name': 'Medvirken og egen skyld',
                      'id': 'Erstatning uden for kontraktforhold 5',
                      'key': '5',
                      'display_title': 'Erstatning uden for kontraktforhold 5 - Medvirken og egen skyld'
                  },


                  'Erstatning uden for kontraktforhold 6': {
                      'subs': {

                           'Erstatning uden for kontraktforhold 6.1': {
                               'subs': {

                                    'Erstatning uden for kontraktforhold 61.1': {
                                        'name': 'Solidarisk ansvar',
                                        'id': 'Erstatning uden for kontraktforhold 61.1',
                                        'key': '1',
                                        'display_title': 'Erstatning uden for kontraktforhold 61.1 - Solidarisk ansvar'
                                    },


                                    'Erstatning uden for kontraktforhold 61.2': {
                                        'name': 'Undtagelser fra det solidariske ansvar',
                                        'id': 'Erstatning uden for kontraktforhold 61.2',
                                        'key': '2',
                                        'display_title': 'Erstatning uden for kontraktforhold 61.2 - Undtagelser fra det solidariske ansvar'
                                    },


                                    'Erstatning uden for kontraktforhold 61.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Erstatning uden for kontraktforhold 61.9',
                                        'key': '9',
                                        'display_title': 'Erstatning uden for kontraktforhold 61.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Forholdet over for skadelidte',
                               'id': 'Erstatning uden for kontraktforhold 6.1',
                               'key': '1',
                               'display_title': 'Erstatning uden for kontraktforhold 6.1 - Forholdet over for skadelidte'
                           },


                           'Erstatning uden for kontraktforhold 6.2': {
                               'subs': {

                                    'Erstatning uden for kontraktforhold 62.1': {
                                        'name': 'Den almindelige regresregel (EAL § 25)',
                                        'id': 'Erstatning uden for kontraktforhold 62.1',
                                        'key': '1',
                                        'display_title': 'Erstatning uden for kontraktforhold 62.1 - Den almindelige regresregel (EAL § 25)'
                                    },


                                    'Erstatning uden for kontraktforhold 62.2': {
                                        'name': 'Særlovgivningen',
                                        'id': 'Erstatning uden for kontraktforhold 62.2',
                                        'key': '2',
                                        'display_title': 'Erstatning uden for kontraktforhold 62.2 - Særlovgivningen'
                                    },


                                    'Erstatning uden for kontraktforhold 62.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Erstatning uden for kontraktforhold 62.9',
                                        'key': '9',
                                        'display_title': 'Erstatning uden for kontraktforhold 62.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Forholdet mellem de solidarisk ansvarlige indbyrdes',
                               'id': 'Erstatning uden for kontraktforhold 6.2',
                               'key': '2',
                               'display_title': 'Erstatning uden for kontraktforhold 6.2 - Forholdet mellem de solidarisk ansvarlige indbyrdes'
                           },


                           'Erstatning uden for kontraktforhold 6.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Erstatning uden for kontraktforhold 6.9',
                               'key': '9',
                               'display_title': 'Erstatning uden for kontraktforhold 6.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Flere erstatningsansvarlige',
                      'id': 'Erstatning uden for kontraktforhold 6',
                      'key': '6',
                      'display_title': 'Erstatning uden for kontraktforhold 6 - Flere erstatningsansvarlige'
                  },


                  'Erstatning uden for kontraktforhold 7': {
                      'subs': {

                           'Erstatning uden for kontraktforhold 7.1': {
                               'name': 'Nødværge',
                               'id': 'Erstatning uden for kontraktforhold 7.1',
                               'key': '1',
                               'display_title': 'Erstatning uden for kontraktforhold 7.1 - Nødværge'
                           },


                           'Erstatning uden for kontraktforhold 7.2': {
                               'name': 'Nødret',
                               'id': 'Erstatning uden for kontraktforhold 7.2',
                               'key': '2',
                               'display_title': 'Erstatning uden for kontraktforhold 7.2 - Nødret'
                           },


                           'Erstatning uden for kontraktforhold 7.3': {
                               'name': 'Negotiorum gestio',
                               'id': 'Erstatning uden for kontraktforhold 7.3',
                               'key': '3',
                               'display_title': 'Erstatning uden for kontraktforhold 7.3 - Negotiorum gestio'
                           },


                           'Erstatning uden for kontraktforhold 7.4': {
                               'name': 'Samtykke',
                               'id': 'Erstatning uden for kontraktforhold 7.4',
                               'key': '4',
                               'display_title': 'Erstatning uden for kontraktforhold 7.4 - Samtykke'
                           },


                           'Erstatning uden for kontraktforhold 7.5': {
                               'name': 'Accept af risiko',
                               'id': 'Erstatning uden for kontraktforhold 7.5',
                               'key': '5',
                               'display_title': 'Erstatning uden for kontraktforhold 7.5 - Accept af risiko'
                           },


                           'Erstatning uden for kontraktforhold 7.6': {
                               'name': 'Forsikringers betydning',
                               'id': 'Erstatning uden for kontraktforhold 7.6',
                               'key': '6',
                               'display_title': 'Erstatning uden for kontraktforhold 7.6 - Forsikringers betydning'
                           },


                           'Erstatning uden for kontraktforhold 7.7': {
                               'name': 'Tilregnelighed og tilregnelse',
                               'id': 'Erstatning uden for kontraktforhold 7.7',
                               'key': '7',
                               'display_title': 'Erstatning uden for kontraktforhold 7.7 - Tilregnelighed og tilregnelse'
                           },


                           'Erstatning uden for kontraktforhold 7.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Erstatning uden for kontraktforhold 7.9',
                               'key': '9',
                               'display_title': 'Erstatning uden for kontraktforhold 7.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Ansvarsfrihed og ansvarsnedsættelse (bortset fra medvirken)',
                      'id': 'Erstatning uden for kontraktforhold 7',
                      'key': '7',
                      'display_title': 'Erstatning uden for kontraktforhold 7 - Ansvarsfrihed og ansvarsnedsættelse (bortset fra medvirken)'
                  },


                  'Erstatning uden for kontraktforhold 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Erstatning uden for kontraktforhold 9',
                      'key': '9',
                      'display_title': 'Erstatning uden for kontraktforhold 9 - Andre spørgsmål'
                  }

             },
             'name': 'Erstatning uden for kontraktforhold',
             'id': 'Erstatning uden for kontraktforhold',
             'key': 'erstatning_uden_for_kontraktforhold',
             'display_title': 'Erstatning uden for kontraktforhold'
         },


         'EU-ret': {
             'subs': {

                  'EU-ret 1': {
                      'subs': {

                           'EU-ret 1.1': {
                               'name': 'Rådet',
                               'id': 'EU-ret 1.1',
                               'key': '1',
                               'display_title': 'EU-ret 1.1 - Rådet'
                           },


                           'EU-ret 1.2': {
                               'name': 'Kommissionen',
                               'id': 'EU-ret 1.2',
                               'key': '2',
                               'display_title': 'EU-ret 1.2 - Kommissionen'
                           },


                           'EU-ret 1.3': {
                               'name': 'Parlamentet',
                               'id': 'EU-ret 1.3',
                               'key': '3',
                               'display_title': 'EU-ret 1.3 - Parlamentet'
                           },


                           'EU-ret 1.4': {
                               'name': 'Domstolen, herunder præjudicielle spørgsmål',
                               'id': 'EU-ret 1.4',
                               'key': '4',
                               'display_title': 'EU-ret 1.4 - Domstolen, herunder præjudicielle spørgsmål'
                           },


                           'EU-ret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'EU-ret 1.9',
                               'key': '9',
                               'display_title': 'EU-ret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Fællesskabsmyndighederne',
                      'id': 'EU-ret 1',
                      'key': '1',
                      'display_title': 'EU-ret 1 - Fællesskabsmyndighederne'
                  },


                  'EU-ret 2': {
                      'name': 'EU-retsakters inkorporering',
                      'id': 'EU-ret 2',
                      'key': '2',
                      'display_title': 'EU-ret 2 - EU-retsakters inkorporering'
                  },


                  'EU-ret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'EU-ret 9',
                      'key': '9',
                      'display_title': 'EU-ret 9 - Andre spørgsmål'
                  }

             },
             'name': 'EU-ret',
             'id': 'EU-ret',
             'key': 'eu_ret',
             'display_title': 'EU-ret'
         },


         'Familieret': {
             'subs': {

                  'Familieret 1': {
                      'subs': {

                           'Familieret 1.1': {
                               'subs': {

                                    'Familieret 11.1': {
                                        'name': 'Ægteskabsbetingelser',
                                        'id': 'Familieret 11.1',
                                        'key': '1',
                                        'display_title': 'Familieret 11.1 - Ægteskabsbetingelser'
                                    },


                                    'Familieret 11.2': {
                                        'name': 'Vielse',
                                        'id': 'Familieret 11.2',
                                        'key': '2',
                                        'display_title': 'Familieret 11.2 - Vielse'
                                    },


                                    'Familieret 11.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Familieret 11.9',
                                        'key': '9',
                                        'display_title': 'Familieret 11.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ægteskabets indgåelse',
                               'id': 'Familieret 1.1',
                               'key': '1',
                               'display_title': 'Familieret 1.1 - Ægteskabets indgåelse'
                           },


                           'Familieret 1.2': {
                               'name': 'Ægtefællers formueordning. Ægtepagter',
                               'id': 'Familieret 1.2',
                               'key': '2',
                               'display_title': 'Familieret 1.2 - Ægtefællers formueordning. Ægtepagter'
                           },


                           'Familieret 1.3': {
                               'subs': {

                                    'Familieret 13.1': {
                                        'name': 'Separations- og skilsmissegrunde',
                                        'id': 'Familieret 13.1',
                                        'key': '1',
                                        'display_title': 'Familieret 13.1 - Separations- og skilsmissegrunde'
                                    },


                                    'Familieret 13.2': {
                                        'subs': {

                                             'Familieret 132.1': {
                                                 'subs': {

                                                      'Familieret 1321.1': {
                                                          'subs': {

                                                               'Familieret 13211.1': {
                                                                   'name': 'Intet bidrag',
                                                                   'id': 'Familieret 13211.1',
                                                                   'key': '1',
                                                                   'display_title': 'Familieret 13211.1 - Intet bidrag'
                                                               },


                                                               'Familieret 13211.2': {
                                                                   'name': '1-6 års bidrag',
                                                                   'id': 'Familieret 13211.2',
                                                                   'key': '2',
                                                                   'display_title': 'Familieret 13211.2 - 1-6 års bidrag'
                                                               },


                                                               'Familieret 13211.3': {
                                                                   'name': '8-10 års bidrag',
                                                                   'id': 'Familieret 13211.3',
                                                                   'key': '3',
                                                                   'display_title': 'Familieret 13211.3 - 8-10 års bidrag'
                                                               },


                                                               'Familieret 13211.4': {
                                                                   'name': 'Tidsubegrænset bidrag',
                                                                   'id': 'Familieret 13211.4',
                                                                   'key': '4',
                                                                   'display_title': 'Familieret 13211.4 - Tidsubegrænset bidrag'
                                                               },


                                                               'Familieret 13211.9': {
                                                                   'name': 'Andre spørgsmål',
                                                                   'id': 'Familieret 13211.9',
                                                                   'key': '9',
                                                                   'display_title': 'Familieret 13211.9 - Andre spørgsmål'
                                                               }

                                                          },
                                                          'name': 'Bidragspligt',
                                                          'id': 'Familieret 1321.1',
                                                          'key': '1',
                                                          'display_title': 'Familieret 1321.1 - Bidragspligt'
                                                      },


                                                      'Familieret 1321.2': {
                                                          'name': 'Ændring',
                                                          'id': 'Familieret 1321.2',
                                                          'key': '2',
                                                          'display_title': 'Familieret 1321.2 - Ændring'
                                                      },


                                                      'Familieret 1321.3': {
                                                          'subs': {

                                                               'Familieret 13213.1': {
                                                                   'name': 'Størrelsen',
                                                                   'id': 'Familieret 13213.1',
                                                                   'key': '1',
                                                                   'display_title': 'Familieret 13213.1 - Størrelsen'
                                                               },


                                                               'Familieret 13213.2': {
                                                                   'name': 'Begyndelses- og ændringstidspunkter',
                                                                   'id': 'Familieret 13213.2',
                                                                   'key': '2',
                                                                   'display_title': 'Familieret 13213.2 - Begyndelses- og ændringstidspunkter'
                                                               },


                                                               'Familieret 13213.9': {
                                                                   'name': 'Andre spørgsmål',
                                                                   'id': 'Familieret 13213.9',
                                                                   'key': '9',
                                                                   'display_title': 'Familieret 13213.9 - Andre spørgsmål'
                                                               }

                                                          },
                                                          'name': 'Bidragsstørrelse',
                                                          'id': 'Familieret 1321.3',
                                                          'key': '3',
                                                          'display_title': 'Familieret 1321.3 - Bidragsstørrelse'
                                                      },


                                                      'Familieret 1321.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Familieret 1321.9',
                                                          'key': '9',
                                                          'display_title': 'Familieret 1321.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Ægtefællebidrag',
                                                 'id': 'Familieret 132.1',
                                                 'key': '1',
                                                 'display_title': 'Familieret 132.1 - Ægtefællebidrag'
                                             },


                                             'Familieret 132.2': {
                                                 'name': '§ 56-godtgørelse',
                                                 'id': 'Familieret 132.2',
                                                 'key': '2',
                                                 'display_title': 'Familieret 132.2 - § 56-godtgørelse'
                                             },


                                             'Familieret 132.3': {
                                                 'name': 'Retten til lejligheden',
                                                 'id': 'Familieret 132.3',
                                                 'key': '3',
                                                 'display_title': 'Familieret 132.3 - Retten til lejligheden'
                                             },


                                             'Familieret 132.4': {
                                                 'name': 'Enkepension',
                                                 'id': 'Familieret 132.4',
                                                 'key': '4',
                                                 'display_title': 'Familieret 132.4 - Enkepension'
                                             },


                                             'Familieret 132.5': {
                                                 'name': 'Ændring af aftaler',
                                                 'id': 'Familieret 132.5',
                                                 'key': '5',
                                                 'display_title': 'Familieret 132.5 - Ændring af aftaler'
                                             },


                                             'Familieret 132.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Familieret 132.9',
                                                 'key': '9',
                                                 'display_title': 'Familieret 132.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Separations- og skilsmissevilkår og ændring heraf',
                                        'id': 'Familieret 13.2',
                                        'key': '2',
                                        'display_title': 'Familieret 13.2 - Separations- og skilsmissevilkår og ændring heraf'
                                    },


                                    'Familieret 13.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Familieret 13.9',
                                        'key': '9',
                                        'display_title': 'Familieret 13.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ægteskabs opløsning (separation og skilsmisse)',
                               'id': 'Familieret 1.3',
                               'key': '3',
                               'display_title': 'Familieret 1.3 - Ægteskabs opløsning (separation og skilsmisse)'
                           },


                           'Familieret 1.4': {
                               'subs': {

                                    'Familieret 14.1': {
                                        'name': 'Fællesboets aktiver og passiver',
                                        'id': 'Familieret 14.1',
                                        'key': '1',
                                        'display_title': 'Familieret 14.1 - Fællesboets aktiver og passiver'
                                    },


                                    'Familieret 14.2': {
                                        'name': '§ 15, stk. 2-rettigheder',
                                        'id': 'Familieret 14.2',
                                        'key': '2',
                                        'display_title': 'Familieret 14.2 - § 15, stk. 2-rettigheder'
                                    },


                                    'Familieret 14.3': {
                                        'name': 'Skævdeling',
                                        'id': 'Familieret 14.3',
                                        'key': '3',
                                        'display_title': 'Familieret 14.3 - Skævdeling'
                                    },


                                    'Familieret 14.4': {
                                        'name': 'Aktivfordelingen',
                                        'id': 'Familieret 14.4',
                                        'key': '4',
                                        'display_title': 'Familieret 14.4 - Aktivfordelingen'
                                    },


                                    'Familieret 14.5': {
                                        'name': 'Sagsbehandlingsspørgsmål',
                                        'id': 'Familieret 14.5',
                                        'key': '5',
                                        'display_title': 'Familieret 14.5 - Sagsbehandlingsspørgsmål'
                                    },


                                    'Familieret 14.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Familieret 14.9',
                                        'key': '9',
                                        'display_title': 'Familieret 14.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Fællesboskifte',
                               'id': 'Familieret 1.4',
                               'key': '4',
                               'display_title': 'Familieret 1.4 - Fællesboskifte'
                           },


                           'Familieret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Familieret 1.9',
                               'key': '9',
                               'display_title': 'Familieret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Ægteskab',
                      'id': 'Familieret 1',
                      'key': '1',
                      'display_title': 'Familieret 1 - Ægteskab'
                  },


                  'Familieret 2': {
                      'subs': {

                           'Familieret 2.1': {
                               'name': 'Kompensation',
                               'id': 'Familieret 2.1',
                               'key': '1',
                               'display_title': 'Familieret 2.1 - Kompensation'
                           },


                           'Familieret 2.2': {
                               'name': 'Samejespørgsmål',
                               'id': 'Familieret 2.2',
                               'key': '2',
                               'display_title': 'Familieret 2.2 - Samejespørgsmål'
                           },


                           'Familieret 2.3': {
                               'name': 'Retten til lejligheden',
                               'id': 'Familieret 2.3',
                               'key': '3',
                               'display_title': 'Familieret 2.3 - Retten til lejligheden'
                           },


                           'Familieret 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Familieret 2.9',
                               'key': '9',
                               'display_title': 'Familieret 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Samlivsforhold uden ægteskab',
                      'id': 'Familieret 2',
                      'key': '2',
                      'display_title': 'Familieret 2 - Samlivsforhold uden ægteskab'
                  },


                  'Familieret 3': {
                      'subs': {

                           'Familieret 3.1': {
                               'name': 'Faderskab',
                               'id': 'Familieret 3.1',
                               'key': '1',
                               'display_title': 'Familieret 3.1 - Faderskab'
                           },


                           'Familieret 3.2': {
                               'name': 'Adoption',
                               'id': 'Familieret 3.2',
                               'key': '2',
                               'display_title': 'Familieret 3.2 - Adoption'
                           },


                           'Familieret 3.3': {
                               'subs': {

                                    'Familieret 33.1': {
                                        'subs': {

                                             'Familieret 331.1': {
                                                 'name': 'Aftaler',
                                                 'id': 'Familieret 331.1',
                                                 'key': '1',
                                                 'display_title': 'Familieret 331.1 - Aftaler'
                                             },


                                             'Familieret 331.2': {
                                                 'name': 'Ophævelse af fælles forældremyndighed',
                                                 'id': 'Familieret 331.2',
                                                 'key': '2',
                                                 'display_title': 'Familieret 331.2 - Ophævelse af fælles forældremyndighed'
                                             },


                                             'Familieret 331.3': {
                                                 'name': 'Ugift fader',
                                                 'id': 'Familieret 331.3',
                                                 'key': '3',
                                                 'display_title': 'Familieret 331.3 - Ugift fader'
                                             },


                                             'Familieret 331.4': {
                                                 'name': 'Ændring af aftale eller afgørelse',
                                                 'id': 'Familieret 331.4',
                                                 'key': '4',
                                                 'display_title': 'Familieret 331.4 - Ændring af aftale eller afgørelse'
                                             },


                                             'Familieret 331.5': {
                                                 'name': 'Dødsfald',
                                                 'id': 'Familieret 331.5',
                                                 'key': '5',
                                                 'display_title': 'Familieret 331.5 - Dødsfald'
                                             },


                                             'Familieret 331.6': {
                                                 'name': 'Midlertidig forældremyndighed',
                                                 'id': 'Familieret 331.6',
                                                 'key': '6',
                                                 'display_title': 'Familieret 331.6 - Midlertidig forældremyndighed'
                                             },


                                             'Familieret 331.7': {
                                                 'name': 'Bevisførelse',
                                                 'id': 'Familieret 331.7',
                                                 'key': '7',
                                                 'display_title': 'Familieret 331.7 - Bevisførelse'
                                             },


                                             'Familieret 331.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Familieret 331.9',
                                                 'key': '9',
                                                 'display_title': 'Familieret 331.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Forældremyndighed',
                                        'id': 'Familieret 33.1',
                                        'key': '1',
                                        'display_title': 'Familieret 33.1 - Forældremyndighed'
                                    },


                                    'Familieret 33.2': {
                                        'subs': {

                                             'Familieret 332.1': {
                                                 'name': 'Afslag på eller ophævelse af samvær',
                                                 'id': 'Familieret 332.1',
                                                 'key': '1',
                                                 'display_title': 'Familieret 332.1 - Afslag på eller ophævelse af samvær'
                                             },


                                             'Familieret 332.2': {
                                                 'name': 'Omfang',
                                                 'id': 'Familieret 332.2',
                                                 'key': '2',
                                                 'display_title': 'Familieret 332.2 - Omfang'
                                             },


                                             'Familieret 332.3': {
                                                 'name': 'Vilkår',
                                                 'id': 'Familieret 332.3',
                                                 'key': '3',
                                                 'display_title': 'Familieret 332.3 - Vilkår'
                                             },


                                             'Familieret 332.4': {
                                                 'name': 'Prøvelse af samværsafgørelser',
                                                 'id': 'Familieret 332.4',
                                                 'key': '4',
                                                 'display_title': 'Familieret 332.4 - Prøvelse af samværsafgørelser'
                                             },


                                             'Familieret 332.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Familieret 332.9',
                                                 'key': '9',
                                                 'display_title': 'Familieret 332.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Samvær',
                                        'id': 'Familieret 33.2',
                                        'key': '2',
                                        'display_title': 'Familieret 33.2 - Samvær'
                                    },


                                    'Familieret 33.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Familieret 33.9',
                                        'key': '9',
                                        'display_title': 'Familieret 33.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Forældremyndighed og samvær',
                               'id': 'Familieret 3.3',
                               'key': '3',
                               'display_title': 'Familieret 3.3 - Forældremyndighed og samvær'
                           },


                           'Familieret 3.4': {
                               'subs': {

                                    'Familieret 34.1': {
                                        'name': 'Pligten',
                                        'id': 'Familieret 34.1',
                                        'key': '1',
                                        'display_title': 'Familieret 34.1 - Pligten'
                                    },


                                    'Familieret 34.2': {
                                        'name': 'Størrelsen',
                                        'id': 'Familieret 34.2',
                                        'key': '2',
                                        'display_title': 'Familieret 34.2 - Størrelsen'
                                    },


                                    'Familieret 34.3': {
                                        'name': 'Begyndelses- og ændringstidspunkter',
                                        'id': 'Familieret 34.3',
                                        'key': '3',
                                        'display_title': 'Familieret 34.3 - Begyndelses- og ændringstidspunkter'
                                    },


                                    'Familieret 34.4': {
                                        'name': 'Konfirmationsbidrag og andre særlige bidrag',
                                        'id': 'Familieret 34.4',
                                        'key': '4',
                                        'display_title': 'Familieret 34.4 - Konfirmationsbidrag og andre særlige bidrag'
                                    },


                                    'Familieret 34.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Familieret 34.9',
                                        'key': '9',
                                        'display_title': 'Familieret 34.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Børns forsørgelse',
                               'id': 'Familieret 3.4',
                               'key': '4',
                               'display_title': 'Familieret 3.4 - Børns forsørgelse'
                           },


                           'Familieret 3.5': {
                               'name': 'Umyndighed',
                               'id': 'Familieret 3.5',
                               'key': '5',
                               'display_title': 'Familieret 3.5 - Umyndighed'
                           },


                           'Familieret 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Familieret 3.9',
                               'key': '9',
                               'display_title': 'Familieret 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Børn',
                      'id': 'Familieret 3',
                      'key': '3',
                      'display_title': 'Familieret 3 - Børn'
                  },


                  'Familieret 4': {
                      'name': 'Registreret partnerskab',
                      'id': 'Familieret 4',
                      'key': '4',
                      'display_title': 'Familieret 4 - Registreret partnerskab'
                  },


                  'Familieret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Familieret 9',
                      'key': '9',
                      'display_title': 'Familieret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Familieret',
             'id': 'Familieret',
             'key': 'familieret',
             'display_title': 'Familieret'
         },


         'Fogedret': {
             'subs': {

                  'Fogedret 1': {
                      'subs': {

                           'Fogedret 1.1': {
                               'name': 'Grundlaget',
                               'id': 'Fogedret 1.1',
                               'key': '1',
                               'display_title': 'Fogedret 1.1 - Grundlaget'
                           },


                           'Fogedret 1.2': {
                               'subs': {

                                    'Fogedret 12.1': {
                                        'name': 'Saglig kompetence',
                                        'id': 'Fogedret 12.1',
                                        'key': '1',
                                        'display_title': 'Fogedret 12.1 - Saglig kompetence'
                                    },


                                    'Fogedret 12.2': {
                                        'name': 'Udeblivelse',
                                        'id': 'Fogedret 12.2',
                                        'key': '2',
                                        'display_title': 'Fogedret 12.2 - Udeblivelse'
                                    },


                                    'Fogedret 12.3': {
                                        'name': 'Forretningens gennemførelse',
                                        'id': 'Fogedret 12.3',
                                        'key': '3',
                                        'display_title': 'Fogedret 12.3 - Forretningens gennemførelse'
                                    },


                                    'Fogedret 12.4': {
                                        'name': 'Udsættelse',
                                        'id': 'Fogedret 12.4',
                                        'key': '4',
                                        'display_title': 'Fogedret 12.4 - Udsættelse'
                                    },


                                    'Fogedret 12.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Fogedret 12.9',
                                        'key': '9',
                                        'display_title': 'Fogedret 12.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Fremgangsmåde',
                               'id': 'Fogedret 1.2',
                               'key': '2',
                               'display_title': 'Fogedret 1.2 - Fremgangsmåde'
                           },


                           'Fogedret 1.3': {
                               'name': 'Genstand',
                               'id': 'Fogedret 1.3',
                               'key': '3',
                               'display_title': 'Fogedret 1.3 - Genstand'
                           },


                           'Fogedret 1.4': {
                               'name': 'Retsvirkning',
                               'id': 'Fogedret 1.4',
                               'key': '4',
                               'display_title': 'Fogedret 1.4 - Retsvirkning'
                           },


                           'Fogedret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Fogedret 1.9',
                               'key': '9',
                               'display_title': 'Fogedret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Udlæg',
                      'id': 'Fogedret 1',
                      'key': '1',
                      'display_title': 'Fogedret 1 - Udlæg'
                  },


                  'Fogedret 2': {
                      'name': 'Lønindeholdelse',
                      'id': 'Fogedret 2',
                      'key': '2',
                      'display_title': 'Fogedret 2 - Lønindeholdelse'
                  },


                  'Fogedret 3': {
                      'subs': {

                           'Fogedret 3.1': {
                               'name': 'Betingelser',
                               'id': 'Fogedret 3.1',
                               'key': '1',
                               'display_title': 'Fogedret 3.1 - Betingelser'
                           },


                           'Fogedret 3.2': {
                               'name': 'Fremgangsmåde',
                               'id': 'Fogedret 3.2',
                               'key': '2',
                               'display_title': 'Fogedret 3.2 - Fremgangsmåde'
                           },


                           'Fogedret 3.3': {
                               'name': 'Genstand',
                               'id': 'Fogedret 3.3',
                               'key': '3',
                               'display_title': 'Fogedret 3.3 - Genstand'
                           },


                           'Fogedret 3.4': {
                               'name': 'Retsvirkning',
                               'id': 'Fogedret 3.4',
                               'key': '4',
                               'display_title': 'Fogedret 3.4 - Retsvirkning'
                           },


                           'Fogedret 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Fogedret 3.9',
                               'key': '9',
                               'display_title': 'Fogedret 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Arrest',
                      'id': 'Fogedret 3',
                      'key': '3',
                      'display_title': 'Fogedret 3 - Arrest'
                  },


                  'Fogedret 4': {
                      'subs': {

                           'Fogedret 4.1': {
                               'name': 'Løsøre',
                               'id': 'Fogedret 4.1',
                               'key': '1',
                               'display_title': 'Fogedret 4.1 - Løsøre'
                           },


                           'Fogedret 4.2': {
                               'subs': {

                                    'Fogedret 42.1': {
                                        'name': 'Betingelser',
                                        'id': 'Fogedret 42.1',
                                        'key': '1',
                                        'display_title': 'Fogedret 42.1 - Betingelser'
                                    },


                                    'Fogedret 42.2': {
                                        'name': 'Afværgelse',
                                        'id': 'Fogedret 42.2',
                                        'key': '2',
                                        'display_title': 'Fogedret 42.2 - Afværgelse'
                                    },


                                    'Fogedret 42.3': {
                                        'name': 'Forberedelse',
                                        'id': 'Fogedret 42.3',
                                        'key': '3',
                                        'display_title': 'Fogedret 42.3 - Forberedelse'
                                    },


                                    'Fogedret 42.4': {
                                        'name': 'Gennemførelse',
                                        'id': 'Fogedret 42.4',
                                        'key': '4',
                                        'display_title': 'Fogedret 42.4 - Gennemførelse'
                                    },


                                    'Fogedret 42.5': {
                                        'name': 'Retsvirkning',
                                        'id': 'Fogedret 42.5',
                                        'key': '5',
                                        'display_title': 'Fogedret 42.5 - Retsvirkning'
                                    },


                                    'Fogedret 42.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Fogedret 42.9',
                                        'key': '9',
                                        'display_title': 'Fogedret 42.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Fast ejendom',
                               'id': 'Fogedret 4.2',
                               'key': '2',
                               'display_title': 'Fogedret 4.2 - Fast ejendom'
                           },


                           'Fogedret 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Fogedret 4.9',
                               'key': '9',
                               'display_title': 'Fogedret 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Tvangsauktion',
                      'id': 'Fogedret 4',
                      'key': '4',
                      'display_title': 'Fogedret 4 - Tvangsauktion'
                  },


                  'Fogedret 5': {
                      'subs': {

                           'Fogedret 5.1': {
                               'name': 'Betingelser',
                               'id': 'Fogedret 5.1',
                               'key': '1',
                               'display_title': 'Fogedret 5.1 - Betingelser'
                           },


                           'Fogedret 5.2': {
                               'name': 'Fremgangsmåde',
                               'id': 'Fogedret 5.2',
                               'key': '2',
                               'display_title': 'Fogedret 5.2 - Fremgangsmåde'
                           },


                           'Fogedret 5.3': {
                               'name': 'Indhold',
                               'id': 'Fogedret 5.3',
                               'key': '3',
                               'display_title': 'Fogedret 5.3 - Indhold'
                           },


                           'Fogedret 5.4': {
                               'name': 'Retsvirkning',
                               'id': 'Fogedret 5.4',
                               'key': '4',
                               'display_title': 'Fogedret 5.4 - Retsvirkning'
                           },


                           'Fogedret 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Fogedret 5.9',
                               'key': '9',
                               'display_title': 'Fogedret 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Fogedforbud',
                      'id': 'Fogedret 5',
                      'key': '5',
                      'display_title': 'Fogedret 5 - Fogedforbud'
                  },


                  'Fogedret 6': {
                      'subs': {

                           'Fogedret 6.1': {
                               'subs': {

                                    'Fogedret 61.1': {
                                        'name': 'Forældremyndighed',
                                        'id': 'Fogedret 61.1',
                                        'key': '1',
                                        'display_title': 'Fogedret 61.1 - Forældremyndighed'
                                    },


                                    'Fogedret 61.2': {
                                        'name': 'Samvær',
                                        'id': 'Fogedret 61.2',
                                        'key': '2',
                                        'display_title': 'Fogedret 61.2 - Samvær'
                                    },


                                    'Fogedret 61.3': {
                                        'name': 'Internationale børnebortførelser',
                                        'id': 'Fogedret 61.3',
                                        'key': '3',
                                        'display_title': 'Fogedret 61.3 - Internationale børnebortførelser'
                                    },


                                    'Fogedret 61.4': {
                                        'name': 'Fremgangsmåden',
                                        'id': 'Fogedret 61.4',
                                        'key': '4',
                                        'display_title': 'Fogedret 61.4 - Fremgangsmåden'
                                    },


                                    'Fogedret 61.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Fogedret 61.9',
                                        'key': '9',
                                        'display_title': 'Fogedret 61.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Børn',
                               'id': 'Fogedret 6.1',
                               'key': '1',
                               'display_title': 'Fogedret 6.1 - Børn'
                           },


                           'Fogedret 6.2': {
                               'subs': {

                                    'Fogedret 62.1': {
                                        'name': 'Indsættelsesforretninger',
                                        'id': 'Fogedret 62.1',
                                        'key': '1',
                                        'display_title': 'Fogedret 62.1 - Indsættelsesforretninger'
                                    },


                                    'Fogedret 62.2': {
                                        'name': 'Udsættelsesforretninger',
                                        'id': 'Fogedret 62.2',
                                        'key': '2',
                                        'display_title': 'Fogedret 62.2 - Udsættelsesforretninger'
                                    },


                                    'Fogedret 62.3': {
                                        'name': 'Omkostningsspørgsmål',
                                        'id': 'Fogedret 62.3',
                                        'key': '3',
                                        'display_title': 'Fogedret 62.3 - Omkostningsspørgsmål'
                                    },


                                    'Fogedret 62.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Fogedret 62.9',
                                        'key': '9',
                                        'display_title': 'Fogedret 62.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Umiddelbare fogedforretninger',
                               'id': 'Fogedret 6.2',
                               'key': '2',
                               'display_title': 'Fogedret 6.2 - Umiddelbare fogedforretninger'
                           },


                           'Fogedret 6.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Fogedret 6.9',
                               'key': '9',
                               'display_title': 'Fogedret 6.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Krav på andet end penge',
                      'id': 'Fogedret 6',
                      'key': '6',
                      'display_title': 'Fogedret 6 - Krav på andet end penge'
                  },


                  'Fogedret 7': {
                      'name': 'Appel',
                      'id': 'Fogedret 7',
                      'key': '7',
                      'display_title': 'Fogedret 7 - Appel'
                  },


                  'Fogedret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Fogedret 9',
                      'key': '9',
                      'display_title': 'Fogedret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Fogedret',
             'id': 'Fogedret',
             'key': 'fogedret',
             'display_title': 'Fogedret'
         },


         'Forsikring': {
             'subs': {

                  'Forsikring 1': {
                      'name': 'Forsikringsbegivenheden',
                      'id': 'Forsikring 1',
                      'key': '1',
                      'display_title': 'Forsikring 1 - Forsikringsbegivenheden'
                  },


                  'Forsikring 2': {
                      'name': 'Opgørelse af skaden',
                      'id': 'Forsikring 2',
                      'key': '2',
                      'display_title': 'Forsikring 2 - Opgørelse af skaden'
                  },


                  'Forsikring 3': {
                      'subs': {

                           'Forsikring 3.1': {
                               'name': 'Urigtige risikooplysninger',
                               'id': 'Forsikring 3.1',
                               'key': '1',
                               'display_title': 'Forsikring 3.1 - Urigtige risikooplysninger'
                           },


                           'Forsikring 3.2': {
                               'name': 'Fremkaldelse af forsikringsbegivenheden',
                               'id': 'Forsikring 3.2',
                               'key': '2',
                               'display_title': 'Forsikring 3.2 - Fremkaldelse af forsikringsbegivenheden'
                           },


                           'Forsikring 3.3': {
                               'name': 'Fareforøgelse',
                               'id': 'Forsikring 3.3',
                               'key': '3',
                               'display_title': 'Forsikring 3.3 - Fareforøgelse'
                           },


                           'Forsikring 3.4': {
                               'name': 'Sikkerhedsforskrifter',
                               'id': 'Forsikring 3.4',
                               'key': '4',
                               'display_title': 'Forsikring 3.4 - Sikkerhedsforskrifter'
                           },


                           'Forsikring 3.5': {
                               'name': 'Underforsikring',
                               'id': 'Forsikring 3.5',
                               'key': '5',
                               'display_title': 'Forsikring 3.5 - Underforsikring'
                           },


                           'Forsikring 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Forsikring 3.9',
                               'key': '9',
                               'display_title': 'Forsikring 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Nedsættelse eller bortfald af forsikringsydelsen',
                      'id': 'Forsikring 3',
                      'key': '3',
                      'display_title': 'Forsikring 3 - Nedsættelse eller bortfald af forsikringsydelsen'
                  },


                  'Forsikring 4': {
                      'name': 'Præmiebetaling',
                      'id': 'Forsikring 4',
                      'key': '4',
                      'display_title': 'Forsikring 4 - Præmiebetaling'
                  },


                  'Forsikring 5': {
                      'name': 'Forsikring af tredjemands interesser',
                      'id': 'Forsikring 5',
                      'key': '5',
                      'display_title': 'Forsikring 5 - Forsikring af tredjemands interesser'
                  },


                  'Forsikring 6': {
                      'name': 'Selskabets regreskrav, se også Erstatning uden for kontraktforhold 7.6',
                      'id': 'Forsikring 6',
                      'key': '6',
                      'display_title': 'Forsikring 6 - Selskabets regreskrav, se også Erstatning uden for kontraktforhold 7.6'
                  },


                  'Forsikring 7': {
                      'subs': {

                           'Forsikring 7.1': {
                               'name': 'Brand- og bygningsforsikring',
                               'id': 'Forsikring 7.1',
                               'key': '1',
                               'display_title': 'Forsikring 7.1 - Brand- og bygningsforsikring'
                           },


                           'Forsikring 7.2': {
                               'name': 'Vand- og tøbrudsskadeforsikring',
                               'id': 'Forsikring 7.2',
                               'key': '2',
                               'display_title': 'Forsikring 7.2 - Vand- og tøbrudsskadeforsikring'
                           },


                           'Forsikring 7.3': {
                               'name': 'Tyveri- og røveriforsikring',
                               'id': 'Forsikring 7.3',
                               'key': '3',
                               'display_title': 'Forsikring 7.3 - Tyveri- og røveriforsikring'
                           },


                           'Forsikring 7.4': {
                               'name': 'Ansvarsforsikring (se også Færdselsret 71.1)',
                               'id': 'Forsikring 7.4',
                               'key': '4',
                               'display_title': 'Forsikring 7.4 - Ansvarsforsikring (se også Færdselsret 71.1)'
                           },


                           'Forsikring 7.5': {
                               'name': 'Sø- og transportforsikring',
                               'id': 'Forsikring 7.5',
                               'key': '5',
                               'display_title': 'Forsikring 7.5 - Sø- og transportforsikring'
                           },


                           'Forsikring 7.6': {
                               'name': 'Garantiforsikring',
                               'id': 'Forsikring 7.6',
                               'key': '6',
                               'display_title': 'Forsikring 7.6 - Garantiforsikring'
                           },


                           'Forsikring 7.7': {
                               'subs': {

                                    'Forsikring 77.1': {
                                        'subs': {

                                             'Forsikring 771.1': {
                                                 'name': 'Nødssituationer',
                                                 'id': 'Forsikring 771.1',
                                                 'key': '1',
                                                 'display_title': 'Forsikring 771.1 - Nødssituationer'
                                             },


                                             'Forsikring 771.2': {
                                                 'name': 'Bortfald',
                                                 'id': 'Forsikring 771.2',
                                                 'key': '2',
                                                 'display_title': 'Forsikring 771.2 - Bortfald'
                                             },


                                             'Forsikring 771.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Forsikring 771.9',
                                                 'key': '9',
                                                 'display_title': 'Forsikring 771.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Begunstigelse',
                                        'id': 'Forsikring 77.1',
                                        'key': '1',
                                        'display_title': 'Forsikring 77.1 - Begunstigelse'
                                    },


                                    'Forsikring 77.2': {
                                        'name': 'Skiftebehandling',
                                        'id': 'Forsikring 77.2',
                                        'key': '2',
                                        'display_title': 'Forsikring 77.2 - Skiftebehandling'
                                    },


                                    'Forsikring 77.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Forsikring 77.9',
                                        'key': '9',
                                        'display_title': 'Forsikring 77.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Livsforsikring',
                               'id': 'Forsikring 7.7',
                               'key': '7',
                               'display_title': 'Forsikring 7.7 - Livsforsikring'
                           },


                           'Forsikring 7.8': {
                               'name': 'Ulykkes-, syge- og begravelsesforsikring',
                               'id': 'Forsikring 7.8',
                               'key': '8',
                               'display_title': 'Forsikring 7.8 - Ulykkes-, syge- og begravelsesforsikring'
                           },


                           'Forsikring 7.9': {
                               'name': 'Anden forsikring',
                               'id': 'Forsikring 7.9',
                               'key': '9',
                               'display_title': 'Forsikring 7.9 - Anden forsikring'
                           }

                      },
                      'name': 'Andre spørgsmål vedrørende de enkelte forsikringsformer',
                      'id': 'Forsikring 7',
                      'key': '7',
                      'display_title': 'Forsikring 7 - Andre spørgsmål vedrørende de enkelte forsikringsformer'
                  },


                  'Forsikring 8': {
                      'name': 'Gensidige forsikringsselskaber',
                      'id': 'Forsikring 8',
                      'key': '8',
                      'display_title': 'Forsikring 8 - Gensidige forsikringsselskaber'
                  },


                  'Forsikring 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Forsikring 9',
                      'key': '9',
                      'display_title': 'Forsikring 9 - Andre spørgsmål'
                  }

             },
             'name': 'Forsikring',
             'id': 'Forsikring',
             'key': 'forsikring',
             'display_title': 'Forsikring'
         },


         'Forvaltningsret': {
             'subs': {

                  'Forvaltningsret 1': {
                      'subs': {

                           'Forvaltningsret 1.1': {
                               'subs': {

                                    'Forvaltningsret 11.1': {
                                        'subs': {

                                             'Forvaltningsret 111.1': {
                                                 'name': 'Sagsbegrebet',
                                                 'id': 'Forvaltningsret 111.1',
                                                 'key': '1',
                                                 'display_title': 'Forvaltningsret 111.1 - Sagsbegrebet'
                                             },


                                             'Forvaltningsret 111.2': {
                                                 'name': 'Inititativ til sagens rejsning',
                                                 'id': 'Forvaltningsret 111.2',
                                                 'key': '2',
                                                 'display_title': 'Forvaltningsret 111.2 - Inititativ til sagens rejsning'
                                             },


                                             'Forvaltningsret 111.3': {
                                                 'subs': {

                                                      'Forvaltningsret 1113.1': {
                                                          'name': 'Originær kompetence',
                                                          'id': 'Forvaltningsret 1113.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 1113.1 - Originær kompetence'
                                                      },


                                                      'Forvaltningsret 1113.2': {
                                                          'name': 'Intern delegation',
                                                          'id': 'Forvaltningsret 1113.2',
                                                          'key': '2',
                                                          'display_title': 'Forvaltningsret 1113.2 - Intern delegation'
                                                      },


                                                      'Forvaltningsret 1113.3': {
                                                          'name': 'Ekstern delegation',
                                                          'id': 'Forvaltningsret 1113.3',
                                                          'key': '3',
                                                          'display_title': 'Forvaltningsret 1113.3 - Ekstern delegation'
                                                      },


                                                      'Forvaltningsret 1113.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Forvaltningsret 1113.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 1113.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Kompetence (herunder delegation)',
                                                 'id': 'Forvaltningsret 111.3',
                                                 'key': '3',
                                                 'display_title': 'Forvaltningsret 111.3 - Kompetence (herunder delegation)'
                                             },


                                             'Forvaltningsret 111.4': {
                                                 'subs': {

                                                      'Forvaltningsret 1114.1': {
                                                          'name': 'Speciel habilitet',
                                                          'id': 'Forvaltningsret 1114.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 1114.1 - Speciel habilitet'
                                                      },


                                                      'Forvaltningsret 1114.2': {
                                                          'name': 'Generel habilitet',
                                                          'id': 'Forvaltningsret 1114.2',
                                                          'key': '2',
                                                          'display_title': 'Forvaltningsret 1114.2 - Generel habilitet'
                                                      },


                                                      'Forvaltningsret 1114.3': {
                                                          'name': 'Værdighedskrav m.v.',
                                                          'id': 'Forvaltningsret 1114.3',
                                                          'key': '3',
                                                          'display_title': 'Forvaltningsret 1114.3 - Værdighedskrav m.v.'
                                                      },


                                                      'Forvaltningsret 1114.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Forvaltningsret 1114.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 1114.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Habilitet m.v.',
                                                 'id': 'Forvaltningsret 111.4',
                                                 'key': '4',
                                                 'display_title': 'Forvaltningsret 111.4 - Habilitet m.v.'
                                             },


                                             'Forvaltningsret 111.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Forvaltningsret 111.9',
                                                 'key': '9',
                                                 'display_title': 'Forvaltningsret 111.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Sagens rejsning m.v.',
                                        'id': 'Forvaltningsret 11.1',
                                        'key': '1',
                                        'display_title': 'Forvaltningsret 11.1 - Sagens rejsning m.v.'
                                    },


                                    'Forvaltningsret 11.2': {
                                        'subs': {

                                             'Forvaltningsret 112.1': {
                                                 'subs': {

                                                      'Forvaltningsret 1121.1': {
                                                          'name': 'Officialprincippet',
                                                          'id': 'Forvaltningsret 1121.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 1121.1 - Officialprincippet'
                                                      },


                                                      'Forvaltningsret 1121.2': {
                                                          'name': 'Privates oplysningspligt',
                                                          'id': 'Forvaltningsret 1121.2',
                                                          'key': '2',
                                                          'display_title': 'Forvaltningsret 1121.2 - Privates oplysningspligt'
                                                      },


                                                      'Forvaltningsret 1121.3': {
                                                          'name': 'Myndigheders oplysningspligt',
                                                          'id': 'Forvaltningsret 1121.3',
                                                          'key': '3',
                                                          'display_title': 'Forvaltningsret 1121.3 - Myndigheders oplysningspligt'
                                                      },


                                                      'Forvaltningsret 1121.4': {
                                                          'name': 'Høring',
                                                          'id': 'Forvaltningsret 1121.4',
                                                          'key': '4',
                                                          'display_title': 'Forvaltningsret 1121.4 - Høring'
                                                      },


                                                      'Forvaltningsret 1121.5': {
                                                          'name': 'Enkelte sagsoplysningsskridt',
                                                          'id': 'Forvaltningsret 1121.5',
                                                          'key': '5',
                                                          'display_title': 'Forvaltningsret 1121.5 - Enkelte sagsoplysningsskridt'
                                                      },


                                                      'Forvaltningsret 1121.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Forvaltningsret 1121.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 1121.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Sagens oplysning',
                                                 'id': 'Forvaltningsret 112.1',
                                                 'key': '1',
                                                 'display_title': 'Forvaltningsret 112.1 - Sagens oplysning'
                                             },


                                             'Forvaltningsret 112.2': {
                                                 'name': 'Tavshedspligt',
                                                 'id': 'Forvaltningsret 112.2',
                                                 'key': '2',
                                                 'display_title': 'Forvaltningsret 112.2 - Tavshedspligt'
                                             },


                                             'Forvaltningsret 112.3': {
                                                 'subs': {

                                                      'Forvaltningsret 1123.1': {
                                                          'name': 'Inden for samme myndighed',
                                                          'id': 'Forvaltningsret 1123.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 1123.1 - Inden for samme myndighed'
                                                      },


                                                      'Forvaltningsret 1123.2': {
                                                          'name': 'Til anden myndighed',
                                                          'id': 'Forvaltningsret 1123.2',
                                                          'key': '2',
                                                          'display_title': 'Forvaltningsret 1123.2 - Til anden myndighed'
                                                      },


                                                      'Forvaltningsret 1123.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Forvaltningsret 1123.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 1123.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Myndighedens videregivelse af oplysninger',
                                                 'id': 'Forvaltningsret 112.3',
                                                 'key': '3',
                                                 'display_title': 'Forvaltningsret 112.3 - Myndighedens videregivelse af oplysninger'
                                             },


                                             'Forvaltningsret 112.4': {
                                                 'subs': {

                                                      'Forvaltningsret 1124.1': {
                                                          'subs': {

                                                               'Forvaltningsret 11241.1': {
                                                                   'name': 'Undtagne sagstyper',
                                                                   'id': 'Forvaltningsret 11241.1',
                                                                   'key': '1',
                                                                   'display_title': 'Forvaltningsret 11241.1 - Undtagne sagstyper'
                                                               },


                                                               'Forvaltningsret 11241.2': {
                                                                   'name': 'Undtagne dokumenter',
                                                                   'id': 'Forvaltningsret 11241.2',
                                                                   'key': '2',
                                                                   'display_title': 'Forvaltningsret 11241.2 - Undtagne dokumenter'
                                                               },


                                                               'Forvaltningsret 11241.3': {
                                                                   'name': 'Undtagne oplysninger',
                                                                   'id': 'Forvaltningsret 11241.3',
                                                                   'key': '3',
                                                                   'display_title': 'Forvaltningsret 11241.3 - Undtagne oplysninger'
                                                               },


                                                               'Forvaltningsret 11241.9': {
                                                                   'name': 'Andre spørgsmål',
                                                                   'id': 'Forvaltningsret 11241.9',
                                                                   'key': '9',
                                                                   'display_title': 'Forvaltningsret 11241.9 - Andre spørgsmål'
                                                               }

                                                          },
                                                          'name': 'Dokumentoffentlighed, se også Forvaltningsret 113.4',
                                                          'id': 'Forvaltningsret 1124.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 1124.1 - Dokumentoffentlighed, se også Forvaltningsret 113.4'
                                                      },


                                                      'Forvaltningsret 1124.2': {
                                                          'name': 'Projektoffentlighed',
                                                          'id': 'Forvaltningsret 1124.2',
                                                          'key': '2',
                                                          'display_title': 'Forvaltningsret 1124.2 - Projektoffentlighed'
                                                      },


                                                      'Forvaltningsret 1124.3': {
                                                          'name': 'Mødeoffentlighed',
                                                          'id': 'Forvaltningsret 1124.3',
                                                          'key': '3',
                                                          'display_title': 'Forvaltningsret 1124.3 - Mødeoffentlighed'
                                                      },


                                                      'Forvaltningsret 1124.9': {
                                                          'name': 'Andre former for offentlighed',
                                                          'id': 'Forvaltningsret 1124.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 1124.9 - Andre former for offentlighed'
                                                      }

                                                 },
                                                 'name': 'Offentlighed',
                                                 'id': 'Forvaltningsret 112.4',
                                                 'key': '4',
                                                 'display_title': 'Forvaltningsret 112.4 - Offentlighed'
                                             },


                                             'Forvaltningsret 112.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Forvaltningsret 112.9',
                                                 'key': '9',
                                                 'display_title': 'Forvaltningsret 112.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Sagens oplysning, tavshedspligt, offentlighed.',
                                        'id': 'Forvaltningsret 11.2',
                                        'key': '2',
                                        'display_title': 'Forvaltningsret 11.2 - Sagens oplysning, tavshedspligt, offentlighed.'
                                    },


                                    'Forvaltningsret 11.3': {
                                        'subs': {

                                             'Forvaltningsret 113.1': {
                                                 'name': 'Partsbegrebet',
                                                 'id': 'Forvaltningsret 113.1',
                                                 'key': '1',
                                                 'display_title': 'Forvaltningsret 113.1 - Partsbegrebet'
                                             },


                                             'Forvaltningsret 113.2': {
                                                 'name': 'Partsrepræsentation',
                                                 'id': 'Forvaltningsret 113.2',
                                                 'key': '2',
                                                 'display_title': 'Forvaltningsret 113.2 - Partsrepræsentation'
                                             },


                                             'Forvaltningsret 113.3': {
                                                 'subs': {

                                                      'Forvaltningsret 1133.1': {
                                                          'name': 'Ulovbestemt',
                                                          'id': 'Forvaltningsret 1133.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 1133.1 - Ulovbestemt'
                                                      },


                                                      'Forvaltningsret 1133.2': {
                                                          'name': 'Lovbestemt',
                                                          'id': 'Forvaltningsret 1133.2',
                                                          'key': '2',
                                                          'display_title': 'Forvaltningsret 1133.2 - Lovbestemt'
                                                      },


                                                      'Forvaltningsret 1133.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Forvaltningsret 1133.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 1133.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Partshøring',
                                                 'id': 'Forvaltningsret 113.3',
                                                 'key': '3',
                                                 'display_title': 'Forvaltningsret 113.3 - Partshøring'
                                             },


                                             'Forvaltningsret 113.4': {
                                                 'subs': {

                                                      'Forvaltningsret 1134.1': {
                                                          'name': 'Undtagne sagstyper',
                                                          'id': 'Forvaltningsret 1134.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 1134.1 - Undtagne sagstyper'
                                                      },


                                                      'Forvaltningsret 1134.2': {
                                                          'name': 'Undtagne dokumenter',
                                                          'id': 'Forvaltningsret 1134.2',
                                                          'key': '2',
                                                          'display_title': 'Forvaltningsret 1134.2 - Undtagne dokumenter'
                                                      },


                                                      'Forvaltningsret 1134.3': {
                                                          'name': 'Undtagne oplysninger',
                                                          'id': 'Forvaltningsret 1134.3',
                                                          'key': '3',
                                                          'display_title': 'Forvaltningsret 1134.3 - Undtagne oplysninger'
                                                      },


                                                      'Forvaltningsret 1134.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Forvaltningsret 1134.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 1134.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Partsaktindsigt, se også Forvaltningsret 1124.1',
                                                 'id': 'Forvaltningsret 113.4',
                                                 'key': '4',
                                                 'display_title': 'Forvaltningsret 113.4 - Partsaktindsigt, se også Forvaltningsret 1124.1'
                                             },


                                             'Forvaltningsret 113.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Forvaltningsret 113.9',
                                                 'key': '9',
                                                 'display_title': 'Forvaltningsret 113.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Partsmedvirken',
                                        'id': 'Forvaltningsret 11.3',
                                        'key': '3',
                                        'display_title': 'Forvaltningsret 11.3 - Partsmedvirken'
                                    },


                                    'Forvaltningsret 11.4': {
                                        'subs': {

                                             'Forvaltningsret 114.1': {
                                                 'name': 'Afgørelsesbegrebet',
                                                 'id': 'Forvaltningsret 114.1',
                                                 'key': '1',
                                                 'display_title': 'Forvaltningsret 114.1 - Afgørelsesbegrebet'
                                             },


                                             'Forvaltningsret 114.2': {
                                                 'name': 'Den interne beslutningsprocedure',
                                                 'id': 'Forvaltningsret 114.2',
                                                 'key': '2',
                                                 'display_title': 'Forvaltningsret 114.2 - Den interne beslutningsprocedure'
                                             },


                                             'Forvaltningsret 114.3': {
                                                 'name': 'Begrundelse',
                                                 'id': 'Forvaltningsret 114.3',
                                                 'key': '3',
                                                 'display_title': 'Forvaltningsret 114.3 - Begrundelse'
                                             },


                                             'Forvaltningsret 114.4': {
                                                 'name': 'Klagevejledning',
                                                 'id': 'Forvaltningsret 114.4',
                                                 'key': '4',
                                                 'display_title': 'Forvaltningsret 114.4 - Klagevejledning'
                                             },


                                             'Forvaltningsret 114.5': {
                                                 'name': 'Bekendtgørelse og underretning',
                                                 'id': 'Forvaltningsret 114.5',
                                                 'key': '5',
                                                 'display_title': 'Forvaltningsret 114.5 - Bekendtgørelse og underretning'
                                             },


                                             'Forvaltningsret 114.6': {
                                                 'subs': {

                                                      'Forvaltningsret 1146.1': {
                                                          'name': 'Skriftlighed',
                                                          'id': 'Forvaltningsret 1146.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 1146.1 - Skriftlighed'
                                                      },


                                                      'Forvaltningsret 1146.2': {
                                                          'name': 'Formulering',
                                                          'id': 'Forvaltningsret 1146.2',
                                                          'key': '2',
                                                          'display_title': 'Forvaltningsret 1146.2 - Formulering'
                                                      },


                                                      'Forvaltningsret 1146.9': {
                                                          'name': 'Andre formkrav',
                                                          'id': 'Forvaltningsret 1146.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 1146.9 - Andre formkrav'
                                                      }

                                                 },
                                                 'name': 'Formkrav',
                                                 'id': 'Forvaltningsret 114.6',
                                                 'key': '6',
                                                 'display_title': 'Forvaltningsret 114.6 - Formkrav'
                                             },


                                             'Forvaltningsret 114.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Forvaltningsret 114.9',
                                                 'key': '9',
                                                 'display_title': 'Forvaltningsret 114.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Sagens afslutning',
                                        'id': 'Forvaltningsret 11.4',
                                        'key': '4',
                                        'display_title': 'Forvaltningsret 11.4 - Sagens afslutning'
                                    },


                                    'Forvaltningsret 11.5': {
                                        'subs': {

                                             'Forvaltningsret 115.1': {
                                                 'name': 'Vejledning',
                                                 'id': 'Forvaltningsret 115.1',
                                                 'key': '1',
                                                 'display_title': 'Forvaltningsret 115.1 - Vejledning'
                                             },


                                             'Forvaltningsret 115.2': {
                                                 'name': 'Sagsbehandlingstid',
                                                 'id': 'Forvaltningsret 115.2',
                                                 'key': '2',
                                                 'display_title': 'Forvaltningsret 115.2 - Sagsbehandlingstid'
                                             },


                                             'Forvaltningsret 115.3': {
                                                 'name': 'God forvaltningsskik',
                                                 'id': 'Forvaltningsret 115.3',
                                                 'key': '3',
                                                 'display_title': 'Forvaltningsret 115.3 - God forvaltningsskik'
                                             },


                                             'Forvaltningsret 115.4': {
                                                 'name': 'Arkivering',
                                                 'id': 'Forvaltningsret 115.4',
                                                 'key': '4',
                                                 'display_title': 'Forvaltningsret 115.4 - Arkivering'
                                             },


                                             'Forvaltningsret 115.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Forvaltningsret 115.9',
                                                 'key': '9',
                                                 'display_title': 'Forvaltningsret 115.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Særlige sagsbehandlingsspørgsmål',
                                        'id': 'Forvaltningsret 11.5',
                                        'key': '5',
                                        'display_title': 'Forvaltningsret 11.5 - Særlige sagsbehandlingsspørgsmål'
                                    },


                                    'Forvaltningsret 11.9': {
                                        'name': 'Andre sagsbehandlingsspørgsmål',
                                        'id': 'Forvaltningsret 11.9',
                                        'key': '9',
                                        'display_title': 'Forvaltningsret 11.9 - Andre sagsbehandlingsspørgsmål'
                                    }

                               },
                               'name': 'Sagsbehandlingsspørgsmål',
                               'id': 'Forvaltningsret 1.1',
                               'key': '1',
                               'display_title': 'Forvaltningsret 1.1 - Sagsbehandlingsspørgsmål'
                           },


                           'Forvaltningsret 1.2': {
                               'subs': {

                                    'Forvaltningsret 12.1': {
                                        'name': 'Krav til hjemmelsgrundlaget',
                                        'id': 'Forvaltningsret 12.1',
                                        'key': '1',
                                        'display_title': 'Forvaltningsret 12.1 - Krav til hjemmelsgrundlaget'
                                    },


                                    'Forvaltningsret 12.2': {
                                        'name': 'Fortolkning',
                                        'id': 'Forvaltningsret 12.2',
                                        'key': '2',
                                        'display_title': 'Forvaltningsret 12.2 - Fortolkning'
                                    },


                                    'Forvaltningsret 12.3': {
                                        'subs': {

                                             'Forvaltningsret 123.1': {
                                                 'name': 'Inddragelse af kriterier',
                                                 'id': 'Forvaltningsret 123.1',
                                                 'key': '1',
                                                 'display_title': 'Forvaltningsret 123.1 - Inddragelse af kriterier'
                                             },


                                             'Forvaltningsret 123.2': {
                                                 'name': 'Skønsafvejningen',
                                                 'id': 'Forvaltningsret 123.2',
                                                 'key': '2',
                                                 'display_title': 'Forvaltningsret 123.2 - Skønsafvejningen'
                                             },


                                             'Forvaltningsret 123.3': {
                                                 'name': 'Proportionalitetsprincippet',
                                                 'id': 'Forvaltningsret 123.3',
                                                 'key': '3',
                                                 'display_title': 'Forvaltningsret 123.3 - Proportionalitetsprincippet'
                                             },


                                             'Forvaltningsret 123.4': {
                                                 'name': 'Vilkår, procedurefordrejning m.v.',
                                                 'id': 'Forvaltningsret 123.4',
                                                 'key': '4',
                                                 'display_title': 'Forvaltningsret 123.4 - Vilkår, procedurefordrejning m.v.'
                                             },


                                             'Forvaltningsret 123.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Forvaltningsret 123.9',
                                                 'key': '9',
                                                 'display_title': 'Forvaltningsret 123.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Udfyldning',
                                        'id': 'Forvaltningsret 12.3',
                                        'key': '3',
                                        'display_title': 'Forvaltningsret 12.3 - Udfyldning'
                                    },


                                    'Forvaltningsret 12.4': {
                                        'name': 'Administrativ praksis',
                                        'id': 'Forvaltningsret 12.4',
                                        'key': '4',
                                        'display_title': 'Forvaltningsret 12.4 - Administrativ praksis'
                                    },


                                    'Forvaltningsret 12.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Forvaltningsret 12.9',
                                        'key': '9',
                                        'display_title': 'Forvaltningsret 12.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Hjemmelsspørgsmål',
                               'id': 'Forvaltningsret 1.2',
                               'key': '2',
                               'display_title': 'Forvaltningsret 1.2 - Hjemmelsspørgsmål'
                           },


                           'Forvaltningsret 1.3': {
                               'subs': {

                                    'Forvaltningsret 13.1': {
                                        'name': 'Rekurs',
                                        'id': 'Forvaltningsret 13.1',
                                        'key': '1',
                                        'display_title': 'Forvaltningsret 13.1 - Rekurs'
                                    },


                                    'Forvaltningsret 13.2': {
                                        'name': 'Specielt tilsyn, se også Forvaltningsret 3.5',
                                        'id': 'Forvaltningsret 13.2',
                                        'key': '2',
                                        'display_title': 'Forvaltningsret 13.2 - Specielt tilsyn, se også Forvaltningsret 3.5'
                                    },


                                    'Forvaltningsret 13.3': {
                                        'name': 'Remonstration og tilbagekaldelse, se også Skatter 6',
                                        'id': 'Forvaltningsret 13.3',
                                        'key': '3',
                                        'display_title': 'Forvaltningsret 13.3 - Remonstration og tilbagekaldelse, se også Skatter 6'
                                    },


                                    'Forvaltningsret 13.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Forvaltningsret 13.9',
                                        'key': '9',
                                        'display_title': 'Forvaltningsret 13.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Administrativ prøvelse m.v.',
                               'id': 'Forvaltningsret 1.3',
                               'key': '3',
                               'display_title': 'Forvaltningsret 1.3 - Administrativ prøvelse m.v.'
                           },


                           'Forvaltningsret 1.4': {
                               'name': 'Ombudsmandsprøvelse',
                               'id': 'Forvaltningsret 1.4',
                               'key': '4',
                               'display_title': 'Forvaltningsret 1.4 - Ombudsmandsprøvelse'
                           },


                           'Forvaltningsret 1.5': {
                               'name': 'Domstolsprøvelse',
                               'id': 'Forvaltningsret 1.5',
                               'key': '5',
                               'display_title': 'Forvaltningsret 1.5 - Domstolsprøvelse'
                           },


                           'Forvaltningsret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Forvaltningsret 1.9',
                               'key': '9',
                               'display_title': 'Forvaltningsret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Almindelige emner',
                      'id': 'Forvaltningsret 1',
                      'key': '1',
                      'display_title': 'Forvaltningsret 1 - Almindelige emner'
                  },


                  'Forvaltningsret 2': {
                      'subs': {

                           'Forvaltningsret 2.1': {
                               'name': 'Offentlig forsyningsvirksomhed',
                               'id': 'Forvaltningsret 2.1',
                               'key': '1',
                               'display_title': 'Forvaltningsret 2.1 - Offentlig forsyningsvirksomhed'
                           },


                           'Forvaltningsret 2.2': {
                               'name': 'Personaleadministration, se også Ansættelses- og arbejdsret',
                               'id': 'Forvaltningsret 2.2',
                               'key': '2',
                               'display_title': 'Forvaltningsret 2.2 - Personaleadministration, se også Ansættelses- og arbejdsret'
                           },


                           'Forvaltningsret 2.3': {
                               'name': 'Undervisning og forskning (herunder skoler)',
                               'id': 'Forvaltningsret 2.3',
                               'key': '3',
                               'display_title': 'Forvaltningsret 2.3 - Undervisning og forskning (herunder skoler)'
                           },


                           'Forvaltningsret 2.4': {
                               'subs': {

                                    'Forvaltningsret 24.1': {
                                        'name': 'Hospitalsforhold',
                                        'id': 'Forvaltningsret 24.1',
                                        'key': '1',
                                        'display_title': 'Forvaltningsret 24.1 - Hospitalsforhold'
                                    },


                                    'Forvaltningsret 24.2': {
                                        'name': 'Læger',
                                        'id': 'Forvaltningsret 24.2',
                                        'key': '2',
                                        'display_title': 'Forvaltningsret 24.2 - Læger'
                                    },


                                    'Forvaltningsret 24.3': {
                                        'name': 'Tandlæger',
                                        'id': 'Forvaltningsret 24.3',
                                        'key': '3',
                                        'display_title': 'Forvaltningsret 24.3 - Tandlæger'
                                    },


                                    'Forvaltningsret 24.4': {
                                        'name': 'Sygepleje m.v.',
                                        'id': 'Forvaltningsret 24.4',
                                        'key': '4',
                                        'display_title': 'Forvaltningsret 24.4 - Sygepleje m.v.'
                                    },


                                    'Forvaltningsret 24.5': {
                                        'name': 'Andre omsorgsfunktioner',
                                        'id': 'Forvaltningsret 24.5',
                                        'key': '5',
                                        'display_title': 'Forvaltningsret 24.5 - Andre omsorgsfunktioner'
                                    },


                                    'Forvaltningsret 24.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Forvaltningsret 24.9',
                                        'key': '9',
                                        'display_title': 'Forvaltningsret 24.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Sundhedsvæsen, læger og sygehuse',
                               'id': 'Forvaltningsret 2.4',
                               'key': '4',
                               'display_title': 'Forvaltningsret 2.4 - Sundhedsvæsen, læger og sygehuse'
                           },


                           'Forvaltningsret 2.5': {
                               'subs': {

                                    'Forvaltningsret 25.1': {
                                        'subs': {

                                             'Forvaltningsret 251.1': {
                                                 'subs': {

                                                      'Forvaltningsret 2511.1': {
                                                          'name': 'Pengeydelser',
                                                          'id': 'Forvaltningsret 2511.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 2511.1 - Pengeydelser'
                                                      },


                                                      'Forvaltningsret 2511.2': {
                                                          'name': 'Hjælpemidler',
                                                          'id': 'Forvaltningsret 2511.2',
                                                          'key': '2',
                                                          'display_title': 'Forvaltningsret 2511.2 - Hjælpemidler'
                                                      },


                                                      'Forvaltningsret 2511.3': {
                                                          'name': 'Tjenesteydelser',
                                                          'id': 'Forvaltningsret 2511.3',
                                                          'key': '3',
                                                          'display_title': 'Forvaltningsret 2511.3 - Tjenesteydelser'
                                                      },


                                                      'Forvaltningsret 2511.9': {
                                                          'name': 'Andre ydelser',
                                                          'id': 'Forvaltningsret 2511.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 2511.9 - Andre ydelser'
                                                      }

                                                 },
                                                 'name': 'Ydelser',
                                                 'id': 'Forvaltningsret 251.1',
                                                 'key': '1',
                                                 'display_title': 'Forvaltningsret 251.1 - Ydelser'
                                             },


                                             'Forvaltningsret 251.2': {
                                                 'subs': {

                                                      'Forvaltningsret 2512.1': {
                                                          'name': 'Visitation',
                                                          'id': 'Forvaltningsret 2512.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 2512.1 - Visitation'
                                                      },


                                                      'Forvaltningsret 2512.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Forvaltningsret 2512.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 2512.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Institutioner',
                                                 'id': 'Forvaltningsret 251.2',
                                                 'key': '2',
                                                 'display_title': 'Forvaltningsret 251.2 - Institutioner'
                                             },


                                             'Forvaltningsret 251.3': {
                                                 'subs': {

                                                      'Forvaltningsret 2513.1': {
                                                          'name': 'Godkendelser',
                                                          'id': 'Forvaltningsret 2513.1',
                                                          'key': '1',
                                                          'display_title': 'Forvaltningsret 2513.1 - Godkendelser'
                                                      },


                                                      'Forvaltningsret 2513.2': {
                                                          'name': 'Forvaltningsaftaler',
                                                          'id': 'Forvaltningsret 2513.2',
                                                          'key': '2',
                                                          'display_title': 'Forvaltningsret 2513.2 - Forvaltningsaftaler'
                                                      },


                                                      'Forvaltningsret 2513.3': {
                                                          'name': 'Tvangsindgreb',
                                                          'id': 'Forvaltningsret 2513.3',
                                                          'key': '3',
                                                          'display_title': 'Forvaltningsret 2513.3 - Tvangsindgreb'
                                                      },


                                                      'Forvaltningsret 2513.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Forvaltningsret 2513.9',
                                                          'key': '9',
                                                          'display_title': 'Forvaltningsret 2513.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Retlig regulering',
                                                 'id': 'Forvaltningsret 251.3',
                                                 'key': '3',
                                                 'display_title': 'Forvaltningsret 251.3 - Retlig regulering'
                                             },


                                             'Forvaltningsret 251.4': {
                                                 'name': 'Opkrævning',
                                                 'id': 'Forvaltningsret 251.4',
                                                 'key': '4',
                                                 'display_title': 'Forvaltningsret 251.4 - Opkrævning'
                                             },


                                             'Forvaltningsret 251.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Forvaltningsret 251.9',
                                                 'key': '9',
                                                 'display_title': 'Forvaltningsret 251.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Social bistand',
                                        'id': 'Forvaltningsret 25.1',
                                        'key': '1',
                                        'display_title': 'Forvaltningsret 25.1 - Social bistand'
                                    },


                                    'Forvaltningsret 25.2': {
                                        'name': 'Social pension',
                                        'id': 'Forvaltningsret 25.2',
                                        'key': '2',
                                        'display_title': 'Forvaltningsret 25.2 - Social pension'
                                    },


                                    'Forvaltningsret 25.3': {
                                        'name': 'Syge- og barselsdagpenge',
                                        'id': 'Forvaltningsret 25.3',
                                        'key': '3',
                                        'display_title': 'Forvaltningsret 25.3 - Syge- og barselsdagpenge'
                                    },


                                    'Forvaltningsret 25.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Forvaltningsret 25.9',
                                        'key': '9',
                                        'display_title': 'Forvaltningsret 25.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Socialvæsen',
                               'id': 'Forvaltningsret 2.5',
                               'key': '5',
                               'display_title': 'Forvaltningsret 2.5 - Socialvæsen'
                           },


                           'Forvaltningsret 2.6': {
                               'subs': {

                                    'Forvaltningsret 26.1': {
                                        'name': 'Arbejdsskadeforsikring, se også Erstatning uden for kontraktforhold 3212.1 og 32.3',
                                        'id': 'Forvaltningsret 26.1',
                                        'key': '1',
                                        'display_title': 'Forvaltningsret 26.1 - Arbejdsskadeforsikring, se også Erstatning uden for kontraktforhold 3212.1 og 32.3'
                                    },


                                    'Forvaltningsret 26.2': {
                                        'name': 'Børnetilskud og familieydelse',
                                        'id': 'Forvaltningsret 26.2',
                                        'key': '2',
                                        'display_title': 'Forvaltningsret 26.2 - Børnetilskud og familieydelse'
                                    },


                                    'Forvaltningsret 26.3': {
                                        'name': 'Uddannelsesstøtte',
                                        'id': 'Forvaltningsret 26.3',
                                        'key': '3',
                                        'display_title': 'Forvaltningsret 26.3 - Uddannelsesstøtte'
                                    },


                                    'Forvaltningsret 26.4': {
                                        'name': 'Individuel boligstøtte',
                                        'id': 'Forvaltningsret 26.4',
                                        'key': '4',
                                        'display_title': 'Forvaltningsret 26.4 - Individuel boligstøtte'
                                    },


                                    'Forvaltningsret 26.5': {
                                        'name': 'Arbejdsløshedsforsikring',
                                        'id': 'Forvaltningsret 26.5',
                                        'key': '5',
                                        'display_title': 'Forvaltningsret 26.5 - Arbejdsløshedsforsikring'
                                    },


                                    'Forvaltningsret 26.6': {
                                        'name': 'ATP',
                                        'id': 'Forvaltningsret 26.6',
                                        'key': '6',
                                        'display_title': 'Forvaltningsret 26.6 - ATP'
                                    },


                                    'Forvaltningsret 26.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Forvaltningsret 26.9',
                                        'key': '9',
                                        'display_title': 'Forvaltningsret 26.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Andre tilskud til personer, se også Forvaltningsret 2.5',
                               'id': 'Forvaltningsret 2.6',
                               'key': '6',
                               'display_title': 'Forvaltningsret 2.6 - Andre tilskud til personer, se også Forvaltningsret 2.5'
                           },


                           'Forvaltningsret 2.7': {
                               'name': 'Kultur',
                               'id': 'Forvaltningsret 2.7',
                               'key': '7',
                               'display_title': 'Forvaltningsret 2.7 - Kultur'
                           },


                           'Forvaltningsret 2.9': {
                               'subs': {

                                    'Forvaltningsret 29.1': {
                                        'name': 'Arbejdsformidling og arbejdsmarkedsuddannelser, se også Forvaltningsret 26.5',
                                        'id': 'Forvaltningsret 29.1',
                                        'key': '1',
                                        'display_title': 'Forvaltningsret 29.1 - Arbejdsformidling og arbejdsmarkedsuddannelser, se også Forvaltningsret 26.5'
                                    },


                                    'Forvaltningsret 29.2': {
                                        'subs': {

                                             'Forvaltningsret 292.1': {
                                                 'name': 'Værnepligt og civilt arbejde',
                                                 'id': 'Forvaltningsret 292.1',
                                                 'key': '1',
                                                 'display_title': 'Forvaltningsret 292.1 - Værnepligt og civilt arbejde'
                                             },


                                             'Forvaltningsret 292.2': {
                                                 'name': 'Det militære forsvar',
                                                 'id': 'Forvaltningsret 292.2',
                                                 'key': '2',
                                                 'display_title': 'Forvaltningsret 292.2 - Det militære forsvar'
                                             },


                                             'Forvaltningsret 292.3': {
                                                 'name': 'Civilforsvaret og det civile beredskab',
                                                 'id': 'Forvaltningsret 292.3',
                                                 'key': '3',
                                                 'display_title': 'Forvaltningsret 292.3 - Civilforsvaret og det civile beredskab'
                                             },


                                             'Forvaltningsret 292.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Forvaltningsret 292.9',
                                                 'key': '9',
                                                 'display_title': 'Forvaltningsret 292.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Totalforsvar',
                                        'id': 'Forvaltningsret 29.2',
                                        'key': '2',
                                        'display_title': 'Forvaltningsret 29.2 - Totalforsvar'
                                    },


                                    'Forvaltningsret 29.3': {
                                        'name': 'Erhvervsstøtte, se også Landbrug m.v. 2',
                                        'id': 'Forvaltningsret 29.3',
                                        'key': '3',
                                        'display_title': 'Forvaltningsret 29.3 - Erhvervsstøtte, se også Landbrug m.v. 2'
                                    },


                                    'Forvaltningsret 29.4': {
                                        'name': 'Udenrigsforhold og udviklingsbistand, se også EU-ret 1 og International ret 1',
                                        'id': 'Forvaltningsret 29.4',
                                        'key': '4',
                                        'display_title': 'Forvaltningsret 29.4 - Udenrigsforhold og udviklingsbistand, se også EU-ret 1 og International ret 1'
                                    },


                                    'Forvaltningsret 29.5': {
                                        'name': 'Politi',
                                        'id': 'Forvaltningsret 29.5',
                                        'key': '5',
                                        'display_title': 'Forvaltningsret 29.5 - Politi'
                                    },


                                    'Forvaltningsret 29.6': {
                                        'subs': {

                                             'Forvaltningsret 296.1': {
                                                 'name': 'Opholds- og arbejdstilladelse',
                                                 'id': 'Forvaltningsret 296.1',
                                                 'key': '1',
                                                 'display_title': 'Forvaltningsret 296.1 - Opholds- og arbejdstilladelse'
                                             },


                                             'Forvaltningsret 296.2': {
                                                 'name': 'Udlevering og udvisning',
                                                 'id': 'Forvaltningsret 296.2',
                                                 'key': '2',
                                                 'display_title': 'Forvaltningsret 296.2 - Udlevering og udvisning'
                                             },


                                             'Forvaltningsret 296.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Forvaltningsret 296.9',
                                                 'key': '9',
                                                 'display_title': 'Forvaltningsret 296.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Udlændinge',
                                        'id': 'Forvaltningsret 29.6',
                                        'key': '6',
                                        'display_title': 'Forvaltningsret 29.6 - Udlændinge'
                                    },


                                    'Forvaltningsret 29.7': {
                                        'name': 'Kirken og kirkelige forhold',
                                        'id': 'Forvaltningsret 29.7',
                                        'key': '7',
                                        'display_title': 'Forvaltningsret 29.7 - Kirken og kirkelige forhold'
                                    },


                                    'Forvaltningsret 29.9': {
                                        'name': 'Særlige forvaltningsområder',
                                        'id': 'Forvaltningsret 29.9',
                                        'key': '9',
                                        'display_title': 'Forvaltningsret 29.9 - Særlige forvaltningsområder'
                                    }

                               },
                               'name': 'Andre forvaltningsområder',
                               'id': 'Forvaltningsret 2.9',
                               'key': '9',
                               'display_title': 'Forvaltningsret 2.9 - Andre forvaltningsområder'
                           }

                      },
                      'name': 'Enkelte forvaltningsområder',
                      'id': 'Forvaltningsret 2',
                      'key': '2',
                      'display_title': 'Forvaltningsret 2 - Enkelte forvaltningsområder'
                  },


                  'Forvaltningsret 3': {
                      'subs': {

                           'Forvaltningsret 3.1': {
                               'name': 'Valg til kommunale organer',
                               'id': 'Forvaltningsret 3.1',
                               'key': '1',
                               'display_title': 'Forvaltningsret 3.1 - Valg til kommunale organer'
                           },


                           'Forvaltningsret 3.2': {
                               'name': 'Beslutningsproceduren i kommunale organer',
                               'id': 'Forvaltningsret 3.2',
                               'key': '2',
                               'display_title': 'Forvaltningsret 3.2 - Beslutningsproceduren i kommunale organer'
                           },


                           'Forvaltningsret 3.3': {
                               'name': 'De kommunale opgaver',
                               'id': 'Forvaltningsret 3.3',
                               'key': '3',
                               'display_title': 'Forvaltningsret 3.3 - De kommunale opgaver'
                           },


                           'Forvaltningsret 3.4': {
                               'name': 'Kommunernes økonomi og budgetter, se også Statsforfatningsret 4',
                               'id': 'Forvaltningsret 3.4',
                               'key': '4',
                               'display_title': 'Forvaltningsret 3.4 - Kommunernes økonomi og budgetter, se også Statsforfatningsret 4'
                           },


                           'Forvaltningsret 3.5': {
                               'name': 'Det kommunale tilsyn',
                               'id': 'Forvaltningsret 3.5',
                               'key': '5',
                               'display_title': 'Forvaltningsret 3.5 - Det kommunale tilsyn'
                           },


                           'Forvaltningsret 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Forvaltningsret 3.9',
                               'key': '9',
                               'display_title': 'Forvaltningsret 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Kommunale forhold',
                      'id': 'Forvaltningsret 3',
                      'key': '3',
                      'display_title': 'Forvaltningsret 3 - Kommunale forhold'
                  },


                  'Forvaltningsret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Forvaltningsret 9',
                      'key': '9',
                      'display_title': 'Forvaltningsret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Forvaltningsret',
             'id': 'Forvaltningsret',
             'key': 'forvaltningsret',
             'display_title': 'Forvaltningsret'
         },


         'Færdselsret': {
             'subs': {

                  'Færdselsret 1': {
                      'name': 'Færdselslovens område',
                      'id': 'Færdselsret 1',
                      'key': '1',
                      'display_title': 'Færdselsret 1 - Færdselslovens område'
                  },


                  'Færdselsret 2': {
                      'name': 'Registrering af køretøjer',
                      'id': 'Færdselsret 2',
                      'key': '2',
                      'display_title': 'Færdselsret 2 - Registrering af køretøjer'
                  },


                  'Færdselsret 3': {
                      'subs': {

                           'Færdselsret 3.1': {
                               'subs': {

                                    'Færdselsret 31.1': {
                                        'name': 'Kørsel med motordrevne køretøjer, (frakendelse af førerret, se Færdselsret 4.3 og 5, kørsel uden kørekort, se Færdselsret 4.4)',
                                        'id': 'Færdselsret 31.1',
                                        'key': '1',
                                        'display_title': 'Færdselsret 31.1 - Kørsel med motordrevne køretøjer, (frakendelse af førerret, se Færdselsret 4.3 og 5, kørsel uden kørekort, se Færdselsret 4.4)'
                                    },


                                    'Færdselsret 31.2': {
                                        'name': 'Andre straffesager',
                                        'id': 'Færdselsret 31.2',
                                        'key': '2',
                                        'display_title': 'Færdselsret 31.2 - Andre straffesager'
                                    },


                                    'Færdselsret 31.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Færdselsret 31.9',
                                        'key': '9',
                                        'display_title': 'Færdselsret 31.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Færdselsloven',
                               'id': 'Færdselsret 3.1',
                               'key': '1',
                               'display_title': 'Færdselsret 3.1 - Færdselsloven'
                           },


                           'Færdselsret 3.2': {
                               'name': 'Andre offentlige forskrifter',
                               'id': 'Færdselsret 3.2',
                               'key': '2',
                               'display_title': 'Færdselsret 3.2 - Andre offentlige forskrifter'
                           },


                           'Færdselsret 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Færdselsret 3.9',
                               'key': '9',
                               'display_title': 'Færdselsret 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Strafansvar',
                      'id': 'Færdselsret 3',
                      'key': '3',
                      'display_title': 'Færdselsret 3 - Strafansvar'
                  },


                  'Færdselsret 4': {
                      'subs': {

                           'Færdselsret 4.1': {
                               'name': 'Erhvervelse af førerret',
                               'id': 'Færdselsret 4.1',
                               'key': '1',
                               'display_title': 'Færdselsret 4.1 - Erhvervelse af førerret'
                           },


                           'Færdselsret 4.2': {
                               'subs': {

                                    'Færdselsret 42.1': {
                                        'name': 'Betingelser for erhvervelse ikke længere opfyldt',
                                        'id': 'Færdselsret 42.1',
                                        'key': '1',
                                        'display_title': 'Færdselsret 42.1 - Betingelser for erhvervelse ikke længere opfyldt'
                                    },


                                    'Færdselsret 42.2': {
                                        'name': 'Midlertidig inddragelse',
                                        'id': 'Færdselsret 42.2',
                                        'key': '2',
                                        'display_title': 'Færdselsret 42.2 - Midlertidig inddragelse'
                                    },


                                    'Færdselsret 42.3': {
                                        'name': 'Erstatning',
                                        'id': 'Færdselsret 42.3',
                                        'key': '3',
                                        'display_title': 'Færdselsret 42.3 - Erstatning'
                                    },


                                    'Færdselsret 42.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Færdselsret 42.9',
                                        'key': '9',
                                        'display_title': 'Færdselsret 42.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Inddragelse af førerret',
                               'id': 'Færdselsret 4.2',
                               'key': '2',
                               'display_title': 'Færdselsret 4.2 - Inddragelse af førerret'
                           },


                           'Færdselsret 4.3': {
                               'subs': {

                                    'Færdselsret 43.1': {
                                        'name': 'Spiritus- og promillekørsel, se Færdselsret 5',
                                        'id': 'Færdselsret 43.1',
                                        'key': '1',
                                        'display_title': 'Færdselsret 43.1 - Spiritus- og promillekørsel, se Færdselsret 5'
                                    },


                                    'Færdselsret 43.2': {
                                        'subs': {

                                             'Færdselsret 432.1': {
                                                 'name': 'Indretning og udstyr',
                                                 'id': 'Færdselsret 432.1',
                                                 'key': '1',
                                                 'display_title': 'Færdselsret 432.1 - Indretning og udstyr'
                                             },


                                             'Færdselsret 432.2': {
                                                 'name': 'Vigepligt',
                                                 'id': 'Færdselsret 432.2',
                                                 'key': '2',
                                                 'display_title': 'Færdselsret 432.2 - Vigepligt'
                                             },


                                             'Færdselsret 432.3': {
                                                 'name': 'Fodgængerfelt',
                                                 'id': 'Færdselsret 432.3',
                                                 'key': '3',
                                                 'display_title': 'Færdselsret 432.3 - Fodgængerfelt'
                                             },


                                             'Færdselsret 432.4': {
                                                 'name': 'Hastighed',
                                                 'id': 'Færdselsret 432.4',
                                                 'key': '4',
                                                 'display_title': 'Færdselsret 432.4 - Hastighed'
                                             },


                                             'Færdselsret 432.5': {
                                                 'name': 'Hensynsløs kørsel',
                                                 'id': 'Færdselsret 432.5',
                                                 'key': '5',
                                                 'display_title': 'Færdselsret 432.5 - Hensynsløs kørsel'
                                             },


                                             'Færdselsret 432.6': {
                                                 'name': 'Straffelovens § 241 og § 249',
                                                 'id': 'Færdselsret 432.6',
                                                 'key': '6',
                                                 'display_title': 'Færdselsret 432.6 - Straffelovens § 241 og § 249'
                                             },


                                             'Færdselsret 432.7': {
                                                 'name': 'Sygdom, medicin, narkotika m.v.',
                                                 'id': 'Færdselsret 432.7',
                                                 'key': '7',
                                                 'display_title': 'Færdselsret 432.7 - Sygdom, medicin, narkotika m.v.'
                                             },


                                             'Færdselsret 432.9': {
                                                 'name': 'Øvrige tilfælde',
                                                 'id': 'Færdselsret 432.9',
                                                 'key': '9',
                                                 'display_title': 'Færdselsret 432.9 - Øvrige tilfælde'
                                             }

                                        },
                                        'name': 'Andre sager',
                                        'id': 'Færdselsret 43.2',
                                        'key': '2',
                                        'display_title': 'Færdselsret 43.2 - Andre sager'
                                    },


                                    'Færdselsret 43.3': {
                                        'name': 'Gentagelse, se også Færdselsret 5.4',
                                        'id': 'Færdselsret 43.3',
                                        'key': '3',
                                        'display_title': 'Færdselsret 43.3 - Gentagelse, se også Færdselsret 5.4'
                                    },


                                    'Færdselsret 43.4': {
                                        'name': 'Erstatning',
                                        'id': 'Færdselsret 43.4',
                                        'key': '4',
                                        'display_title': 'Færdselsret 43.4 - Erstatning'
                                    },


                                    'Færdselsret 43.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Færdselsret 43.9',
                                        'key': '9',
                                        'display_title': 'Færdselsret 43.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Frakendelse af førerret',
                               'id': 'Færdselsret 4.3',
                               'key': '3',
                               'display_title': 'Færdselsret 4.3 - Frakendelse af førerret'
                           },


                           'Færdselsret 4.4': {
                               'subs': {

                                    'Færdselsret 44.1': {
                                        'name': 'Kørsel i frakendelsestiden',
                                        'id': 'Færdselsret 44.1',
                                        'key': '1',
                                        'display_title': 'Færdselsret 44.1 - Kørsel i frakendelsestiden'
                                    },


                                    'Færdselsret 44.2': {
                                        'name': 'Anden kørsel uden kørekort',
                                        'id': 'Færdselsret 44.2',
                                        'key': '2',
                                        'display_title': 'Færdselsret 44.2 - Anden kørsel uden kørekort'
                                    },


                                    'Færdselsret 44.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Færdselsret 44.9',
                                        'key': '9',
                                        'display_title': 'Færdselsret 44.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Kørsel uden kørekort',
                               'id': 'Færdselsret 4.4',
                               'key': '4',
                               'display_title': 'Færdselsret 4.4 - Kørsel uden kørekort'
                           },


                           'Færdselsret 4.5': {
                               'name': 'Generhvervelse af førerret',
                               'id': 'Færdselsret 4.5',
                               'key': '5',
                               'display_title': 'Færdselsret 4.5 - Generhvervelse af førerret'
                           },


                           'Færdselsret 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Færdselsret 4.9',
                               'key': '9',
                               'display_title': 'Færdselsret 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Førerret til motordrevet køretøj',
                      'id': 'Færdselsret 4',
                      'key': '4',
                      'display_title': 'Færdselsret 4 - Førerret til motordrevet køretøj'
                  },


                  'Færdselsret 5': {
                      'subs': {

                           'Færdselsret 5.1': {
                               'subs': {

                                    'Færdselsret 51.1': {
                                        'subs': {

                                             'Færdselsret 511.1': {
                                                 'name': 'Motorkøretøj',
                                                 'id': 'Færdselsret 511.1',
                                                 'key': '1',
                                                 'display_title': 'Færdselsret 511.1 - Motorkøretøj'
                                             },


                                             'Færdselsret 511.2': {
                                                 'name': 'Andet motordrevet køretøj',
                                                 'id': 'Færdselsret 511.2',
                                                 'key': '2',
                                                 'display_title': 'Færdselsret 511.2 - Andet motordrevet køretøj'
                                             },


                                             'Færdselsret 511.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Færdselsret 511.9',
                                                 'key': '9',
                                                 'display_title': 'Færdselsret 511.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Færdselslovens § 53, stk. 1',
                                        'id': 'Færdselsret 51.1',
                                        'key': '1',
                                        'display_title': 'Færdselsret 51.1 - Færdselslovens § 53, stk. 1'
                                    },


                                    'Færdselsret 51.2': {
                                        'name': 'Færdselslovens § 53, stk. 2',
                                        'id': 'Færdselsret 51.2',
                                        'key': '2',
                                        'display_title': 'Færdselsret 51.2 - Færdselslovens § 53, stk. 2'
                                    },


                                    'Færdselsret 51.3': {
                                        'name': 'Straffelovens § 241 og § 249',
                                        'id': 'Færdselsret 51.3',
                                        'key': '3',
                                        'display_title': 'Færdselsret 51.3 - Straffelovens § 241 og § 249'
                                    },


                                    'Færdselsret 51.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Færdselsret 51.9',
                                        'key': '9',
                                        'display_title': 'Færdselsret 51.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Spirituskørsel',
                               'id': 'Færdselsret 5.1',
                               'key': '1',
                               'display_title': 'Færdselsret 5.1 - Spirituskørsel'
                           },


                           'Færdselsret 5.2': {
                               'name': 'Promillekørsel',
                               'id': 'Færdselsret 5.2',
                               'key': '2',
                               'display_title': 'Færdselsret 5.2 - Promillekørsel'
                           },


                           'Færdselsret 5.3': {
                               'name': 'Kørsel i udlandet',
                               'id': 'Færdselsret 5.3',
                               'key': '3',
                               'display_title': 'Færdselsret 5.3 - Kørsel i udlandet'
                           },


                           'Færdselsret 5.4': {
                               'name': 'Gentagelse',
                               'id': 'Færdselsret 5.4',
                               'key': '4',
                               'display_title': 'Færdselsret 5.4 - Gentagelse'
                           },


                           'Færdselsret 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Færdselsret 5.9',
                               'key': '9',
                               'display_title': 'Færdselsret 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Spiritus- og promillekørsel',
                      'id': 'Færdselsret 5',
                      'key': '5',
                      'display_title': 'Færdselsret 5 - Spiritus- og promillekørsel'
                  },


                  'Færdselsret 6': {
                      'subs': {

                           'Færdselsret 6.1': {
                               'subs': {

                                    'Færdselsret 61.1': {
                                        'subs': {

                                             'Færdselsret 611.1': {
                                                 'name': 'Skade på tredjemands person og/eller gods',
                                                 'id': 'Færdselsret 611.1',
                                                 'key': '1',
                                                 'display_title': 'Færdselsret 611.1 - Skade på tredjemands person og/eller gods'
                                             },


                                             'Færdselsret 611.2': {
                                                 'name': 'Skade på egen passager',
                                                 'id': 'Færdselsret 611.2',
                                                 'key': '2',
                                                 'display_title': 'Færdselsret 611.2 - Skade på egen passager'
                                             },


                                             'Færdselsret 611.3': {
                                                 'name': 'Medvirken/egen skyld',
                                                 'id': 'Færdselsret 611.3',
                                                 'key': '3',
                                                 'display_title': 'Færdselsret 611.3 - Medvirken/egen skyld'
                                             },


                                             'Færdselsret 611.4': {
                                                 'name': 'Førerens ansvar',
                                                 'id': 'Færdselsret 611.4',
                                                 'key': '4',
                                                 'display_title': 'Færdselsret 611.4 - Førerens ansvar'
                                             },


                                             'Færdselsret 611.9': {
                                                 'name': 'Andre færdselsuheld',
                                                 'id': 'Færdselsret 611.9',
                                                 'key': '9',
                                                 'display_title': 'Færdselsret 611.9 - Andre færdselsuheld'
                                             }

                                        },
                                        'name': 'Påkørsel og andet færdselsuheld',
                                        'id': 'Færdselsret 61.1',
                                        'key': '1',
                                        'display_title': 'Færdselsret 61.1 - Påkørsel og andet færdselsuheld'
                                    },


                                    'Færdselsret 61.2': {
                                        'name': 'Anden skadeforvoldelse',
                                        'id': 'Færdselsret 61.2',
                                        'key': '2',
                                        'display_title': 'Færdselsret 61.2 - Anden skadeforvoldelse'
                                    },


                                    'Færdselsret 61.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Færdselsret 61.9',
                                        'key': '9',
                                        'display_title': 'Færdselsret 61.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Motordrevet køretøj',
                               'id': 'Færdselsret 6.1',
                               'key': '1',
                               'display_title': 'Færdselsret 6.1 - Motordrevet køretøj'
                           },


                           'Færdselsret 6.2': {
                               'name': 'Andet skadeserstatningsansvar',
                               'id': 'Færdselsret 6.2',
                               'key': '2',
                               'display_title': 'Færdselsret 6.2 - Andet skadeserstatningsansvar'
                           },


                           'Færdselsret 6.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Færdselsret 6.9',
                               'key': '9',
                               'display_title': 'Færdselsret 6.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Erstatningsansvar',
                      'id': 'Færdselsret 6',
                      'key': '6',
                      'display_title': 'Færdselsret 6 - Erstatningsansvar'
                  },


                  'Færdselsret 7': {
                      'subs': {

                           'Færdselsret 7.1': {
                               'subs': {

                                    'Færdselsret 71.1': {
                                        'name': 'Lovpligtig ansvarsforsikring',
                                        'id': 'Færdselsret 71.1',
                                        'key': '1',
                                        'display_title': 'Færdselsret 71.1 - Lovpligtig ansvarsforsikring'
                                    },


                                    'Færdselsret 71.2': {
                                        'name': 'Kaskoforsikring, se også Forsikring 7.9',
                                        'id': 'Færdselsret 71.2',
                                        'key': '2',
                                        'display_title': 'Færdselsret 71.2 - Kaskoforsikring, se også Forsikring 7.9'
                                    },


                                    'Færdselsret 71.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Færdselsret 71.9',
                                        'key': '9',
                                        'display_title': 'Færdselsret 71.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Motordrevet køretøj',
                               'id': 'Færdselsret 7.1',
                               'key': '1',
                               'display_title': 'Færdselsret 7.1 - Motordrevet køretøj'
                           },


                           'Færdselsret 7.2': {
                               'name': 'Anden forsikring, se Forsikring',
                               'id': 'Færdselsret 7.2',
                               'key': '2',
                               'display_title': 'Færdselsret 7.2 - Anden forsikring, se Forsikring'
                           },


                           'Færdselsret 7.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Færdselsret 7.9',
                               'key': '9',
                               'display_title': 'Færdselsret 7.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Forsikring',
                      'id': 'Færdselsret 7',
                      'key': '7',
                      'display_title': 'Færdselsret 7 - Forsikring'
                  },


                  'Færdselsret 8': {
                      'subs': {

                           'Færdselsret 8.1': {
                               'name': 'Motorkøretøj',
                               'id': 'Færdselsret 8.1',
                               'key': '1',
                               'display_title': 'Færdselsret 8.1 - Motorkøretøj'
                           },


                           'Færdselsret 8.2': {
                               'name': 'Knallert',
                               'id': 'Færdselsret 8.2',
                               'key': '2',
                               'display_title': 'Færdselsret 8.2 - Knallert'
                           },


                           'Færdselsret 8.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Færdselsret 8.9',
                               'key': '9',
                               'display_title': 'Færdselsret 8.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Konfiskation',
                      'id': 'Færdselsret 8',
                      'key': '8',
                      'display_title': 'Færdselsret 8 - Konfiskation'
                  },


                  'Færdselsret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Færdselsret 9',
                      'key': '9',
                      'display_title': 'Færdselsret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Færdselsret',
             'id': 'Færdselsret',
             'key': 'f_rdselsret',
             'display_title': 'Færdselsret'
         },


         'Færøerne og Grønland': {
             'subs': {

                  'Færøerne og Grønland 1': {
                      'name': 'Færøerne',
                      'id': 'Færøerne og Grønland 1',
                      'key': '1',
                      'display_title': 'Færøerne og Grønland 1 - Færøerne'
                  },


                  'Færøerne og Grønland 2': {
                      'name': 'Grønland',
                      'id': 'Færøerne og Grønland 2',
                      'key': '2',
                      'display_title': 'Færøerne og Grønland 2 - Grønland'
                  }

             },
             'name': 'Færøerne og Grønland',
             'id': 'Færøerne og Grønland',
             'key': 'f_r_erne_og_gr_nland',
             'display_title': 'Færøerne og Grønland'
         },


         'Immaterialret': {
             'subs': {

                  'Immaterialret 1': {
                      'subs': {

                           'Immaterialret 1.1': {
                               'name': 'Ophavsretslovens saglige afgrænsning',
                               'id': 'Immaterialret 1.1',
                               'key': '1',
                               'display_title': 'Immaterialret 1.1 - Ophavsretslovens saglige afgrænsning'
                           },


                           'Immaterialret 1.2': {
                               'name': 'Enerettens indhold og de lovbestemte indskrænkninger',
                               'id': 'Immaterialret 1.2',
                               'key': '2',
                               'display_title': 'Immaterialret 1.2 - Enerettens indhold og de lovbestemte indskrænkninger'
                           },


                           'Immaterialret 1.3': {
                               'name': 'Rettens overførelse. Arbejdstagernes ophavsret. Forlagskontrakter m.v.',
                               'id': 'Immaterialret 1.3',
                               'key': '3',
                               'display_title': 'Immaterialret 1.3 - Rettens overførelse. Arbejdstagernes ophavsret. Forlagskontrakter m.v.'
                           },


                           'Immaterialret 1.4': {
                               'name': 'Retskrænkelser og sanktioner',
                               'id': 'Immaterialret 1.4',
                               'key': '4',
                               'display_title': 'Immaterialret 1.4 - Retskrænkelser og sanktioner'
                           },


                           'Immaterialret 1.5': {
                               'name': 'Udøvende kunstnere og andre rettigheder i lovens kapitel V',
                               'id': 'Immaterialret 1.5',
                               'key': '5',
                               'display_title': 'Immaterialret 1.5 - Udøvende kunstnere og andre rettigheder i lovens kapitel V'
                           },


                           'Immaterialret 1.6': {
                               'name': 'Internationale forhold',
                               'id': 'Immaterialret 1.6',
                               'key': '6',
                               'display_title': 'Immaterialret 1.6 - Internationale forhold'
                           },


                           'Immaterialret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Immaterialret 1.9',
                               'key': '9',
                               'display_title': 'Immaterialret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Ophavsret',
                      'id': 'Immaterialret 1',
                      'key': '1',
                      'display_title': 'Immaterialret 1 - Ophavsret'
                  },


                  'Immaterialret 2': {
                      'name': 'Fotografiret',
                      'id': 'Immaterialret 2',
                      'key': '2',
                      'display_title': 'Immaterialret 2 - Fotografiret'
                  },


                  'Immaterialret 3': {
                      'name': 'Designret',
                      'id': 'Immaterialret 3',
                      'key': '3',
                      'display_title': 'Immaterialret 3 - Designret'
                  },


                  'Immaterialret 4': {
                      'subs': {

                           'Immaterialret 4.1': {
                               'name': 'Patenterbare opfindelser. Krænkelsesspørgsmål',
                               'id': 'Immaterialret 4.1',
                               'key': '1',
                               'display_title': 'Immaterialret 4.1 - Patenterbare opfindelser. Krænkelsesspørgsmål'
                           },


                           'Immaterialret 4.2': {
                               'name': 'Licensaftaler m.v.',
                               'id': 'Immaterialret 4.2',
                               'key': '2',
                               'display_title': 'Immaterialret 4.2 - Licensaftaler m.v.'
                           },


                           'Immaterialret 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Immaterialret 4.9',
                               'key': '9',
                               'display_title': 'Immaterialret 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Patentret',
                      'id': 'Immaterialret 4',
                      'key': '4',
                      'display_title': 'Immaterialret 4 - Patentret'
                  },


                  'Immaterialret 5': {
                      'subs': {

                           'Immaterialret 5.1': {
                               'name': 'Retserhvervelsen ved registrering eller ibrugtagning. Særprægskrav m.v.',
                               'id': 'Immaterialret 5.1',
                               'key': '1',
                               'display_title': 'Immaterialret 5.1 - Retserhvervelsen ved registrering eller ibrugtagning. Særprægskrav m.v.'
                           },


                           'Immaterialret 5.2': {
                               'name': 'Rettens overførelse. Licenskontrakter m.v.',
                               'id': 'Immaterialret 5.2',
                               'key': '2',
                               'display_title': 'Immaterialret 5.2 - Rettens overførelse. Licenskontrakter m.v.'
                           },


                           'Immaterialret 5.3': {
                               'subs': {

                                    'Immaterialret 53.1': {
                                        'name': 'Bedømmelse af varemærkers lighed',
                                        'id': 'Immaterialret 53.1',
                                        'key': '1',
                                        'display_title': 'Immaterialret 53.1 - Bedømmelse af varemærkers lighed'
                                    },


                                    'Immaterialret 53.2': {
                                        'name': 'Varelighedskriteriet',
                                        'id': 'Immaterialret 53.2',
                                        'key': '2',
                                        'display_title': 'Immaterialret 53.2 - Varelighedskriteriet'
                                    },


                                    'Immaterialret 53.3': {
                                        'name': 'Straf, erstatning m.v.',
                                        'id': 'Immaterialret 53.3',
                                        'key': '3',
                                        'display_title': 'Immaterialret 53.3 - Straf, erstatning m.v.'
                                    },


                                    'Immaterialret 53.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Immaterialret 53.9',
                                        'key': '9',
                                        'display_title': 'Immaterialret 53.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Varemærkekrænkelser',
                               'id': 'Immaterialret 5.3',
                               'key': '3',
                               'display_title': 'Immaterialret 5.3 - Varemærkekrænkelser'
                           },


                           'Immaterialret 5.4': {
                               'name': 'Ophør, herunder degeneration',
                               'id': 'Immaterialret 5.4',
                               'key': '4',
                               'display_title': 'Immaterialret 5.4 - Ophør, herunder degeneration'
                           },


                           'Immaterialret 5.5': {
                               'name': 'Internationale forhold',
                               'id': 'Immaterialret 5.5',
                               'key': '5',
                               'display_title': 'Immaterialret 5.5 - Internationale forhold'
                           },


                           'Immaterialret 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Immaterialret 5.9',
                               'key': '9',
                               'display_title': 'Immaterialret 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Varemærkeret',
                      'id': 'Immaterialret 5',
                      'key': '5',
                      'display_title': 'Immaterialret 5 - Varemærkeret'
                  },


                  'Immaterialret 6': {
                      'subs': {

                           'Immaterialret 6.1': {
                               'name': 'Plantenyheder',
                               'id': 'Immaterialret 6.1',
                               'key': '1',
                               'display_title': 'Immaterialret 6.1 - Plantenyheder'
                           },


                           'Immaterialret 6.2': {
                               'name': 'Halvlederrettigheder',
                               'id': 'Immaterialret 6.2',
                               'key': '2',
                               'display_title': 'Immaterialret 6.2 - Halvlederrettigheder'
                           },


                           'Immaterialret 6.3': {
                               'name': 'Brugsmodeller',
                               'id': 'Immaterialret 6.3',
                               'key': '3',
                               'display_title': 'Immaterialret 6.3 - Brugsmodeller'
                           },


                           'Immaterialret 6.9': {
                               'name': 'Andre rettigheder',
                               'id': 'Immaterialret 6.9',
                               'key': '9',
                               'display_title': 'Immaterialret 6.9 - Andre rettigheder'
                           }

                      },
                      'name': 'Andre rettigheder',
                      'id': 'Immaterialret 6',
                      'key': '6',
                      'display_title': 'Immaterialret 6 - Andre rettigheder'
                  },


                  'Immaterialret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Immaterialret 9',
                      'key': '9',
                      'display_title': 'Immaterialret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Immaterialret',
             'id': 'Immaterialret',
             'key': 'immaterialret',
             'display_title': 'Immaterialret'
         },


         'International ret': {
             'subs': {

                  'International ret 1': {
                      'name': 'Folkeret. Internationale organisationer',
                      'id': 'International ret 1',
                      'key': '1',
                      'display_title': 'International ret 1 - Folkeret. Internationale organisationer'
                  },


                  'International ret 2': {
                      'name': 'International privatret',
                      'id': 'International ret 2',
                      'key': '2',
                      'display_title': 'International ret 2 - International privatret'
                  },


                  'International ret 3': {
                      'name': 'International offentlig ret (se også Retspleje 21.4)',
                      'id': 'International ret 3',
                      'key': '3',
                      'display_title': 'International ret 3 - International offentlig ret (se også Retspleje 21.4)'
                  },


                  'International ret 4': {
                      'name': 'International strafferet',
                      'id': 'International ret 4',
                      'key': '4',
                      'display_title': 'International ret 4 - International strafferet'
                  },


                  'International ret 5': {
                      'name': 'International skatteret',
                      'id': 'International ret 5',
                      'key': '5',
                      'display_title': 'International ret 5 - International skatteret'
                  },


                  'International ret 6': {
                      'subs': {

                           'International ret 6.1': {
                               'name': 'Adoption',
                               'id': 'International ret 6.1',
                               'key': '1',
                               'display_title': 'International ret 6.1 - Adoption'
                           },


                           'International ret 6.2': {
                               'name': 'Arveret',
                               'id': 'International ret 6.2',
                               'key': '2',
                               'display_title': 'International ret 6.2 - Arveret'
                           },


                           'International ret 6.3': {
                               'name': 'Faderskab',
                               'id': 'International ret 6.3',
                               'key': '3',
                               'display_title': 'International ret 6.3 - Faderskab'
                           },


                           'International ret 6.4': {
                               'name': 'Forældremyndighed og samvær',
                               'id': 'International ret 6.4',
                               'key': '4',
                               'display_title': 'International ret 6.4 - Forældremyndighed og samvær'
                           },


                           'International ret 6.5': {
                               'name': 'Værgemål',
                               'id': 'International ret 6.5',
                               'key': '5',
                               'display_title': 'International ret 6.5 - Værgemål'
                           },


                           'International ret 6.6': {
                               'subs': {

                                    'International ret 66.1': {
                                        'name': 'Indgåelse',
                                        'id': 'International ret 66.1',
                                        'key': '1',
                                        'display_title': 'International ret 66.1 - Indgåelse'
                                    },


                                    'International ret 66.2': {
                                        'name': 'Separation og skilsmisse',
                                        'id': 'International ret 66.2',
                                        'key': '2',
                                        'display_title': 'International ret 66.2 - Separation og skilsmisse'
                                    },


                                    'International ret 66.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'International ret 66.9',
                                        'key': '9',
                                        'display_title': 'International ret 66.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ægteskab',
                               'id': 'International ret 6.6',
                               'key': '6',
                               'display_title': 'International ret 6.6 - Ægteskab'
                           },


                           'International ret 6.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'International ret 6.9',
                               'key': '9',
                               'display_title': 'International ret 6.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'International familieret',
                      'id': 'International ret 6',
                      'key': '6',
                      'display_title': 'International ret 6 - International familieret'
                  },


                  'International ret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'International ret 9',
                      'key': '9',
                      'display_title': 'International ret 9 - Andre spørgsmål'
                  }

             },
             'name': 'International ret',
             'id': 'International ret',
             'key': 'international_ret',
             'display_title': 'International ret'
         },


         'Kaution og garanti': {
             'subs': {

                  'Kaution og garanti 1': {
                      'subs': {

                           'Kaution og garanti 1.1': {
                               'name': 'Stiftelse, gyldighed og fortolkning',
                               'id': 'Kaution og garanti 1.1',
                               'key': '1',
                               'display_title': 'Kaution og garanti 1.1 - Stiftelse, gyldighed og fortolkning'
                           },


                           'Kaution og garanti 1.2': {
                               'name': 'Omfang og ydelsestid',
                               'id': 'Kaution og garanti 1.2',
                               'key': '2',
                               'display_title': 'Kaution og garanti 1.2 - Omfang og ydelsestid'
                           },


                           'Kaution og garanti 1.3': {
                               'name': 'Retsforholdet mellem hovedmand og kautionist',
                               'id': 'Kaution og garanti 1.3',
                               'key': '3',
                               'display_title': 'Kaution og garanti 1.3 - Retsforholdet mellem hovedmand og kautionist'
                           },


                           'Kaution og garanti 1.4': {
                               'name': 'Kautionsforpligtelsens selvstændige ophør',
                               'id': 'Kaution og garanti 1.4',
                               'key': '4',
                               'display_title': 'Kaution og garanti 1.4 - Kautionsforpligtelsens selvstændige ophør'
                           },


                           'Kaution og garanti 1.5': {
                               'name': 'Flere kautionister',
                               'id': 'Kaution og garanti 1.5',
                               'key': '5',
                               'display_title': 'Kaution og garanti 1.5 - Flere kautionister'
                           },


                           'Kaution og garanti 1.6': {
                               'subs': {

                                    'Kaution og garanti 16.1': {
                                        'name': 'Hovedmandens konkurs',
                                        'id': 'Kaution og garanti 16.1',
                                        'key': '1',
                                        'display_title': 'Kaution og garanti 16.1 - Hovedmandens konkurs'
                                    },


                                    'Kaution og garanti 16.2': {
                                        'name': 'Kautionistens konkurs',
                                        'id': 'Kaution og garanti 16.2',
                                        'key': '2',
                                        'display_title': 'Kaution og garanti 16.2 - Kautionistens konkurs'
                                    },


                                    'Kaution og garanti 16.3': {
                                        'name': 'Både hovedmanden og kautionisten konkurs',
                                        'id': 'Kaution og garanti 16.3',
                                        'key': '3',
                                        'display_title': 'Kaution og garanti 16.3 - Både hovedmanden og kautionisten konkurs'
                                    },


                                    'Kaution og garanti 16.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Kaution og garanti 16.9',
                                        'key': '9',
                                        'display_title': 'Kaution og garanti 16.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Konkurs',
                               'id': 'Kaution og garanti 1.6',
                               'key': '6',
                               'display_title': 'Kaution og garanti 1.6 - Konkurs'
                           },


                           'Kaution og garanti 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Kaution og garanti 1.9',
                               'key': '9',
                               'display_title': 'Kaution og garanti 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Kaution',
                      'id': 'Kaution og garanti 1',
                      'key': '1',
                      'display_title': 'Kaution og garanti 1 - Kaution'
                  },


                  'Kaution og garanti 2': {
                      'name': 'Garanti',
                      'id': 'Kaution og garanti 2',
                      'key': '2',
                      'display_title': 'Kaution og garanti 2 - Garanti'
                  },


                  'Kaution og garanti 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Kaution og garanti 9',
                      'key': '9',
                      'display_title': 'Kaution og garanti 9 - Andre spørgsmål'
                  }

             },
             'name': 'Kaution og garanti',
             'id': 'Kaution og garanti',
             'key': 'kaution_og_garanti',
             'display_title': 'Kaution og garanti'
         },


         'Konkurs- og anden insolvensret': {
             'subs': {

                  'Konkurs- og anden insolvensret 1': {
                      'name': 'Betalingsstandsning',
                      'id': 'Konkurs- og anden insolvensret 1',
                      'key': '1',
                      'display_title': 'Konkurs- og anden insolvensret 1 - Betalingsstandsning'
                  },


                  'Konkurs- og anden insolvensret 2': {
                      'subs': {

                           'Konkurs- og anden insolvensret 2.1': {
                               'name': 'Konkursbetingelser',
                               'id': 'Konkurs- og anden insolvensret 2.1',
                               'key': '1',
                               'display_title': 'Konkurs- og anden insolvensret 2.1 - Konkursbetingelser'
                           },


                           'Konkurs- og anden insolvensret 2.2': {
                               'name': 'Konkursvirkninger',
                               'id': 'Konkurs- og anden insolvensret 2.2',
                               'key': '2',
                               'display_title': 'Konkurs- og anden insolvensret 2.2 - Konkursvirkninger'
                           },


                           'Konkurs- og anden insolvensret 2.3': {
                               'name': 'Konkursboets aktiver',
                               'id': 'Konkurs- og anden insolvensret 2.3',
                               'key': '3',
                               'display_title': 'Konkurs- og anden insolvensret 2.3 - Konkursboets aktiver'
                           },


                           'Konkurs- og anden insolvensret 2.4': {
                               'subs': {

                                    'Konkurs- og anden insolvensret 24.1': {
                                        'name': 'Konkursmassekrav',
                                        'id': 'Konkurs- og anden insolvensret 24.1',
                                        'key': '1',
                                        'display_title': 'Konkurs- og anden insolvensret 24.1 - Konkursmassekrav'
                                    },


                                    'Konkurs- og anden insolvensret 24.2': {
                                        'name': 'Privilegerede krav',
                                        'id': 'Konkurs- og anden insolvensret 24.2',
                                        'key': '2',
                                        'display_title': 'Konkurs- og anden insolvensret 24.2 - Privilegerede krav'
                                    },


                                    'Konkurs- og anden insolvensret 24.3': {
                                        'name': 'Simple konkurskrav',
                                        'id': 'Konkurs- og anden insolvensret 24.3',
                                        'key': '3',
                                        'display_title': 'Konkurs- og anden insolvensret 24.3 - Simple konkurskrav'
                                    },


                                    'Konkurs- og anden insolvensret 24.4': {
                                        'name': 'Efterstillede krav',
                                        'id': 'Konkurs- og anden insolvensret 24.4',
                                        'key': '4',
                                        'display_title': 'Konkurs- og anden insolvensret 24.4 - Efterstillede krav'
                                    },


                                    'Konkurs- og anden insolvensret 24.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Konkurs- og anden insolvensret 24.9',
                                        'key': '9',
                                        'display_title': 'Konkurs- og anden insolvensret 24.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Konkursboets passiver',
                               'id': 'Konkurs- og anden insolvensret 2.4',
                               'key': '4',
                               'display_title': 'Konkurs- og anden insolvensret 2.4 - Konkursboets passiver'
                           },


                           'Konkurs- og anden insolvensret 2.5': {
                               'name': 'Modregning i konkurs',
                               'id': 'Konkurs- og anden insolvensret 2.5',
                               'key': '5',
                               'display_title': 'Konkurs- og anden insolvensret 2.5 - Modregning i konkurs'
                           },


                           'Konkurs- og anden insolvensret 2.6': {
                               'name': 'Konkursboets behandling',
                               'id': 'Konkurs- og anden insolvensret 2.6',
                               'key': '6',
                               'display_title': 'Konkurs- og anden insolvensret 2.6 - Konkursboets behandling'
                           },


                           'Konkurs- og anden insolvensret 2.7': {
                               'subs': {

                                    'Konkurs- og anden insolvensret 27.1': {
                                        'name': 'Fællesbetingelser',
                                        'id': 'Konkurs- og anden insolvensret 27.1',
                                        'key': '1',
                                        'display_title': 'Konkurs- og anden insolvensret 27.1 - Fællesbetingelser'
                                    },


                                    'Konkurs- og anden insolvensret 27.2': {
                                        'name': 'Gaver, arveafkald og forsikringer',
                                        'id': 'Konkurs- og anden insolvensret 27.2',
                                        'key': '2',
                                        'display_title': 'Konkurs- og anden insolvensret 27.2 - Gaver, arveafkald og forsikringer'
                                    },


                                    'Konkurs- og anden insolvensret 27.3': {
                                        'name': 'Dispositioner over for nærtstående',
                                        'id': 'Konkurs- og anden insolvensret 27.3',
                                        'key': '3',
                                        'display_title': 'Konkurs- og anden insolvensret 27.3 - Dispositioner over for nærtstående'
                                    },


                                    'Konkurs- og anden insolvensret 27.4': {
                                        'name': 'Betalinger',
                                        'id': 'Konkurs- og anden insolvensret 27.4',
                                        'key': '4',
                                        'display_title': 'Konkurs- og anden insolvensret 27.4 - Betalinger'
                                    },


                                    'Konkurs- og anden insolvensret 27.5': {
                                        'name': 'Panterettigheder m.v.',
                                        'id': 'Konkurs- og anden insolvensret 27.5',
                                        'key': '5',
                                        'display_title': 'Konkurs- og anden insolvensret 27.5 - Panterettigheder m.v.'
                                    },


                                    'Konkurs- og anden insolvensret 27.6': {
                                        'name': 'Betalingsstandsningsdispositioner',
                                        'id': 'Konkurs- og anden insolvensret 27.6',
                                        'key': '6',
                                        'display_title': 'Konkurs- og anden insolvensret 27.6 - Betalingsstandsningsdispositioner'
                                    },


                                    'Konkurs- og anden insolvensret 27.7': {
                                        'name': 'Generalklausulen',
                                        'id': 'Konkurs- og anden insolvensret 27.7',
                                        'key': '7',
                                        'display_title': 'Konkurs- og anden insolvensret 27.7 - Generalklausulen'
                                    },


                                    'Konkurs- og anden insolvensret 27.8': {
                                        'name': 'Retsvirkninger af omstødelse',
                                        'id': 'Konkurs- og anden insolvensret 27.8',
                                        'key': '8',
                                        'display_title': 'Konkurs- og anden insolvensret 27.8 - Retsvirkninger af omstødelse'
                                    },


                                    'Konkurs- og anden insolvensret 27.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Konkurs- og anden insolvensret 27.9',
                                        'key': '9',
                                        'display_title': 'Konkurs- og anden insolvensret 27.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Omstødelse og lignende forhold',
                               'id': 'Konkurs- og anden insolvensret 2.7',
                               'key': '7',
                               'display_title': 'Konkurs- og anden insolvensret 2.7 - Omstødelse og lignende forhold'
                           },


                           'Konkurs- og anden insolvensret 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Konkurs- og anden insolvensret 2.9',
                               'key': '9',
                               'display_title': 'Konkurs- og anden insolvensret 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Konkurs',
                      'id': 'Konkurs- og anden insolvensret 2',
                      'key': '2',
                      'display_title': 'Konkurs- og anden insolvensret 2 - Konkurs'
                  },


                  'Konkurs- og anden insolvensret 3': {
                      'name': 'Tvangsakkord uden for konkurs',
                      'id': 'Konkurs- og anden insolvensret 3',
                      'key': '3',
                      'display_title': 'Konkurs- og anden insolvensret 3 - Tvangsakkord uden for konkurs'
                  },


                  'Konkurs- og anden insolvensret 4': {
                      'subs': {

                           'Konkurs- og anden insolvensret 4.1': {
                               'name': 'Betingelser for gældssanering',
                               'id': 'Konkurs- og anden insolvensret 4.1',
                               'key': '1',
                               'display_title': 'Konkurs- og anden insolvensret 4.1 - Betingelser for gældssanering'
                           },


                           'Konkurs- og anden insolvensret 4.2': {
                               'name': 'Gældssaneringens indhold',
                               'id': 'Konkurs- og anden insolvensret 4.2',
                               'key': '2',
                               'display_title': 'Konkurs- og anden insolvensret 4.2 - Gældssaneringens indhold'
                           },


                           'Konkurs- og anden insolvensret 4.3': {
                               'name': 'Gældssaneringsprocessen',
                               'id': 'Konkurs- og anden insolvensret 4.3',
                               'key': '3',
                               'display_title': 'Konkurs- og anden insolvensret 4.3 - Gældssaneringsprocessen'
                           },


                           'Konkurs- og anden insolvensret 4.4': {
                               'name': 'Gældssaneringens virkninger',
                               'id': 'Konkurs- og anden insolvensret 4.4',
                               'key': '4',
                               'display_title': 'Konkurs- og anden insolvensret 4.4 - Gældssaneringens virkninger'
                           },


                           'Konkurs- og anden insolvensret 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Konkurs- og anden insolvensret 4.9',
                               'key': '9',
                               'display_title': 'Konkurs- og anden insolvensret 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Gældssanering',
                      'id': 'Konkurs- og anden insolvensret 4',
                      'key': '4',
                      'display_title': 'Konkurs- og anden insolvensret 4 - Gældssanering'
                  },


                  'Konkurs- og anden insolvensret 5': {
                      'name': 'Frivillige gældsordninger',
                      'id': 'Konkurs- og anden insolvensret 5',
                      'key': '5',
                      'display_title': 'Konkurs- og anden insolvensret 5 - Frivillige gældsordninger'
                  },


                  'Konkurs- og anden insolvensret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Konkurs- og anden insolvensret 9',
                      'key': '9',
                      'display_title': 'Konkurs- og anden insolvensret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Konkurs- og anden insolvensret',
             'id': 'Konkurs- og anden insolvensret',
             'key': 'konkurs__og_anden_insolvensret',
             'display_title': 'Konkurs- og anden insolvensret'
         },


         'Køb': {
             'subs': {

                  'Køb 1': {
                      'name': 'Købsaftalens indgåelse og tolkning',
                      'id': 'Køb 1',
                      'key': '1',
                      'display_title': 'Køb 1 - Købsaftalens indgåelse og tolkning'
                  },


                  'Køb 2': {
                      'subs': {

                           'Køb 2.1': {
                               'name': 'Kreditkøb',
                               'id': 'Køb 2.1',
                               'key': '1',
                               'display_title': 'Køb 2.1 - Kreditkøb'
                           },


                           'Køb 2.2': {
                               'name': 'Forsinket betaling. Morarenter',
                               'id': 'Køb 2.2',
                               'key': '2',
                               'display_title': 'Køb 2.2 - Forsinket betaling. Morarenter'
                           },


                           'Køb 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Køb 2.9',
                               'key': '9',
                               'display_title': 'Køb 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Købesummen og dens betaling',
                      'id': 'Køb 2',
                      'key': '2',
                      'display_title': 'Køb 2 - Købesummen og dens betaling'
                  },


                  'Køb 3': {
                      'name': 'Leveringssted',
                      'id': 'Køb 3',
                      'key': '3',
                      'display_title': 'Køb 3 - Leveringssted'
                  },


                  'Køb 4': {
                      'name': 'Leveringstid',
                      'id': 'Køb 4',
                      'key': '4',
                      'display_title': 'Køb 4 - Leveringstid'
                  },


                  'Køb 5': {
                      'subs': {

                           'Køb 5.1': {
                               'name': 'Mangler. Købers undersøgelsespligt',
                               'id': 'Køb 5.1',
                               'key': '1',
                               'display_title': 'Køb 5.1 - Mangler. Købers undersøgelsespligt'
                           },


                           'Køb 5.2': {
                               'subs': {

                                    'Køb 52.1': {
                                        'name': 'Afhjælpning',
                                        'id': 'Køb 52.1',
                                        'key': '1',
                                        'display_title': 'Køb 52.1 - Afhjælpning'
                                    },


                                    'Køb 52.2': {
                                        'name': 'Ophævelse af købet',
                                        'id': 'Køb 52.2',
                                        'key': '2',
                                        'display_title': 'Køb 52.2 - Ophævelse af købet'
                                    },


                                    'Køb 52.3': {
                                        'subs': {

                                             'Køb 523.1': {
                                                 'name': 'Erstatningsbetingelser',
                                                 'id': 'Køb 523.1',
                                                 'key': '1',
                                                 'display_title': 'Køb 523.1 - Erstatningsbetingelser'
                                             },


                                             'Køb 523.2': {
                                                 'name': 'Erstatningsberegning',
                                                 'id': 'Køb 523.2',
                                                 'key': '2',
                                                 'display_title': 'Køb 523.2 - Erstatningsberegning'
                                             },


                                             'Køb 523.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Køb 523.9',
                                                 'key': '9',
                                                 'display_title': 'Køb 523.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Erstatning',
                                        'id': 'Køb 52.3',
                                        'key': '3',
                                        'display_title': 'Køb 52.3 - Erstatning'
                                    },


                                    'Køb 52.4': {
                                        'name': 'Forholdsmæssigt afslag',
                                        'id': 'Køb 52.4',
                                        'key': '4',
                                        'display_title': 'Køb 52.4 - Forholdsmæssigt afslag'
                                    },


                                    'Køb 52.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Køb 52.9',
                                        'key': '9',
                                        'display_title': 'Køb 52.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Mangler ved fast ejendom',
                               'id': 'Køb 5.2',
                               'key': '2',
                               'display_title': 'Køb 5.2 - Mangler ved fast ejendom'
                           },


                           'Køb 5.3': {
                               'subs': {

                                    'Køb 53.1': {
                                        'name': 'Afhjælpning',
                                        'id': 'Køb 53.1',
                                        'key': '1',
                                        'display_title': 'Køb 53.1 - Afhjælpning'
                                    },


                                    'Køb 53.2': {
                                        'name': 'Ophævelse af købet',
                                        'id': 'Køb 53.2',
                                        'key': '2',
                                        'display_title': 'Køb 53.2 - Ophævelse af købet'
                                    },


                                    'Køb 53.3': {
                                        'subs': {

                                             'Køb 533.1': {
                                                 'name': 'Erstatningsbetingelser',
                                                 'id': 'Køb 533.1',
                                                 'key': '1',
                                                 'display_title': 'Køb 533.1 - Erstatningsbetingelser'
                                             },


                                             'Køb 533.2': {
                                                 'name': 'Erstatningsberegning',
                                                 'id': 'Køb 533.2',
                                                 'key': '2',
                                                 'display_title': 'Køb 533.2 - Erstatningsberegning'
                                             },


                                             'Køb 533.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Køb 533.9',
                                                 'key': '9',
                                                 'display_title': 'Køb 533.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Erstatning',
                                        'id': 'Køb 53.3',
                                        'key': '3',
                                        'display_title': 'Køb 53.3 - Erstatning'
                                    },


                                    'Køb 53.4': {
                                        'name': 'Forholdsmæssigt afslag',
                                        'id': 'Køb 53.4',
                                        'key': '4',
                                        'display_title': 'Køb 53.4 - Forholdsmæssigt afslag'
                                    },


                                    'Køb 53.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Køb 53.9',
                                        'key': '9',
                                        'display_title': 'Køb 53.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Mangler ved løsøre',
                               'id': 'Køb 5.3',
                               'key': '3',
                               'display_title': 'Køb 5.3 - Mangler ved løsøre'
                           },


                           'Køb 5.4': {
                               'name': 'Mangler ved køb af forretninger og lignende',
                               'id': 'Køb 5.4',
                               'key': '4',
                               'display_title': 'Køb 5.4 - Mangler ved køb af forretninger og lignende'
                           },


                           'Køb 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Køb 5.9',
                               'key': '9',
                               'display_title': 'Køb 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Kvalitet og mængde',
                      'id': 'Køb 5',
                      'key': '5',
                      'display_title': 'Køb 5 - Kvalitet og mængde'
                  },


                  'Køb 6': {
                      'name': 'Køb af fordringer',
                      'id': 'Køb 6',
                      'key': '6',
                      'display_title': 'Køb 6 - Køb af fordringer'
                  },


                  'Køb 7': {
                      'subs': {

                           'Køb 7.1': {
                               'name': 'Fast ejendom (adkomstmangel eller uoplyste behæftelser)',
                               'id': 'Køb 7.1',
                               'key': '1',
                               'display_title': 'Køb 7.1 - Fast ejendom (adkomstmangel eller uoplyste behæftelser)'
                           },


                           'Køb 7.2': {
                               'name': 'Løsøre',
                               'id': 'Køb 7.2',
                               'key': '2',
                               'display_title': 'Køb 7.2 - Løsøre'
                           },


                           'Køb 7.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Køb 7.9',
                               'key': '9',
                               'display_title': 'Køb 7.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Vanhjemmel',
                      'id': 'Køb 7',
                      'key': '7',
                      'display_title': 'Køb 7 - Vanhjemmel'
                  },


                  'Køb 8': {
                      'name': 'Fordringshavermora',
                      'id': 'Køb 8',
                      'key': '8',
                      'display_title': 'Køb 8 - Fordringshavermora'
                  },


                  'Køb 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Køb 9',
                      'key': '9',
                      'display_title': 'Køb 9 - Andre spørgsmål'
                  }

             },
             'name': 'Køb',
             'id': 'Køb',
             'key': 'k_b',
             'display_title': 'Køb'
         },


         'Landbrug m.v.': {
             'subs': {

                  'Landbrug m.v. 1': {
                      'subs': {

                           'Landbrug m.v. 1.1': {
                               'subs': {

                                    'Landbrug m.v. 11.1': {
                                        'name': 'Pålæggelse og ophævelse af landbrugspligt',
                                        'id': 'Landbrug m.v. 11.1',
                                        'key': '1',
                                        'display_title': 'Landbrug m.v. 11.1 - Pålæggelse og ophævelse af landbrugspligt'
                                    },


                                    'Landbrug m.v. 11.2': {
                                        'name': 'Landbrugsplanlægning',
                                        'id': 'Landbrug m.v. 11.2',
                                        'key': '2',
                                        'display_title': 'Landbrug m.v. 11.2 - Landbrugsplanlægning'
                                    },


                                    'Landbrug m.v. 11.3': {
                                        'subs': {

                                             'Landbrug m.v. 113.1': {
                                                 'name': 'Bopæl og egendrift',
                                                 'id': 'Landbrug m.v. 113.1',
                                                 'key': '1',
                                                 'display_title': 'Landbrug m.v. 113.1 - Bopæl og egendrift'
                                             },


                                             'Landbrug m.v. 113.2': {
                                                 'name': 'Forsvarlig drift',
                                                 'id': 'Landbrug m.v. 113.2',
                                                 'key': '2',
                                                 'display_title': 'Landbrug m.v. 113.2 - Forsvarlig drift'
                                             },


                                             'Landbrug m.v. 113.3': {
                                                 'name': 'Miljøkrav',
                                                 'id': 'Landbrug m.v. 113.3',
                                                 'key': '3',
                                                 'display_title': 'Landbrug m.v. 113.3 - Miljøkrav'
                                             },


                                             'Landbrug m.v. 113.4': {
                                                 'name': 'Bygninger',
                                                 'id': 'Landbrug m.v. 113.4',
                                                 'key': '4',
                                                 'display_title': 'Landbrug m.v. 113.4 - Bygninger'
                                             },


                                             'Landbrug m.v. 113.5': {
                                                 'name': 'Råstoffer',
                                                 'id': 'Landbrug m.v. 113.5',
                                                 'key': '5',
                                                 'display_title': 'Landbrug m.v. 113.5 - Råstoffer'
                                             },


                                             'Landbrug m.v. 113.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Landbrug m.v. 113.9',
                                                 'key': '9',
                                                 'display_title': 'Landbrug m.v. 113.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Drift',
                                        'id': 'Landbrug m.v. 11.3',
                                        'key': '3',
                                        'display_title': 'Landbrug m.v. 11.3 - Drift'
                                    },


                                    'Landbrug m.v. 11.4': {
                                        'subs': {

                                             'Landbrug m.v. 114.1': {
                                                 'name': 'Samdrift',
                                                 'id': 'Landbrug m.v. 114.1',
                                                 'key': '1',
                                                 'display_title': 'Landbrug m.v. 114.1 - Samdrift'
                                             },


                                             'Landbrug m.v. 114.2': {
                                                 'name': 'Brugsret og forpagtning',
                                                 'id': 'Landbrug m.v. 114.2',
                                                 'key': '2',
                                                 'display_title': 'Landbrug m.v. 114.2 - Brugsret og forpagtning'
                                             },


                                             'Landbrug m.v. 114.3': {
                                                 'name': 'Landbrugsfællesskaber',
                                                 'id': 'Landbrug m.v. 114.3',
                                                 'key': '3',
                                                 'display_title': 'Landbrug m.v. 114.3 - Landbrugsfællesskaber'
                                             },


                                             'Landbrug m.v. 114.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Landbrug m.v. 114.9',
                                                 'key': '9',
                                                 'display_title': 'Landbrug m.v. 114.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Samdrift og forpagtning',
                                        'id': 'Landbrug m.v. 11.4',
                                        'key': '4',
                                        'display_title': 'Landbrug m.v. 11.4 - Samdrift og forpagtning'
                                    },


                                    'Landbrug m.v. 11.5': {
                                        'subs': {

                                             'Landbrug m.v. 115.1': {
                                                 'name': 'Betingelser for sammenlægning',
                                                 'id': 'Landbrug m.v. 115.1',
                                                 'key': '1',
                                                 'display_title': 'Landbrug m.v. 115.1 - Betingelser for sammenlægning'
                                             },


                                             'Landbrug m.v. 115.2': {
                                                 'name': 'Fortrinsstilling til suppleringsjord',
                                                 'id': 'Landbrug m.v. 115.2',
                                                 'key': '2',
                                                 'display_title': 'Landbrug m.v. 115.2 - Fortrinsstilling til suppleringsjord'
                                             },


                                             'Landbrug m.v. 115.3': {
                                                 'name': 'Oprettelse af nye landbrugsejendomme',
                                                 'id': 'Landbrug m.v. 115.3',
                                                 'key': '3',
                                                 'display_title': 'Landbrug m.v. 115.3 - Oprettelse af nye landbrugsejendomme'
                                             },


                                             'Landbrug m.v. 115.4': {
                                                 'name': 'Fraskillelse af bygninger',
                                                 'id': 'Landbrug m.v. 115.4',
                                                 'key': '4',
                                                 'display_title': 'Landbrug m.v. 115.4 - Fraskillelse af bygninger'
                                             },


                                             'Landbrug m.v. 115.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Landbrug m.v. 115.9',
                                                 'key': '9',
                                                 'display_title': 'Landbrug m.v. 115.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Omlægning af jorder',
                                        'id': 'Landbrug m.v. 11.5',
                                        'key': '5',
                                        'display_title': 'Landbrug m.v. 11.5 - Omlægning af jorder'
                                    },


                                    'Landbrug m.v. 11.6': {
                                        'subs': {

                                             'Landbrug m.v. 116.1': {
                                                 'subs': {

                                                      'Landbrug m.v. 1161.1': {
                                                          'name': 'Fri handel',
                                                          'id': 'Landbrug m.v. 1161.1',
                                                          'key': '1',
                                                          'display_title': 'Landbrug m.v. 1161.1 - Fri handel'
                                                      },


                                                      'Landbrug m.v. 1161.2': {
                                                          'name': 'Familieoverdragelse',
                                                          'id': 'Landbrug m.v. 1161.2',
                                                          'key': '2',
                                                          'display_title': 'Landbrug m.v. 1161.2 - Familieoverdragelse'
                                                      },


                                                      'Landbrug m.v. 1161.3': {
                                                          'name': 'Erhvervelse af anparter',
                                                          'id': 'Landbrug m.v. 1161.3',
                                                          'key': '3',
                                                          'display_title': 'Landbrug m.v. 1161.3 - Erhvervelse af anparter'
                                                      },


                                                      'Landbrug m.v. 1161.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Landbrug m.v. 1161.9',
                                                          'key': '9',
                                                          'display_title': 'Landbrug m.v. 1161.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Personer',
                                                 'id': 'Landbrug m.v. 116.1',
                                                 'key': '1',
                                                 'display_title': 'Landbrug m.v. 116.1 - Personer'
                                             },


                                             'Landbrug m.v. 116.2': {
                                                 'subs': {

                                                      'Landbrug m.v. 1162.1': {
                                                          'name': 'Aktie- og anpartsselskaber',
                                                          'id': 'Landbrug m.v. 1162.1',
                                                          'key': '1',
                                                          'display_title': 'Landbrug m.v. 1162.1 - Aktie- og anpartsselskaber'
                                                      },


                                                      'Landbrug m.v. 1162.2': {
                                                          'name': 'Andre selskaber m.v.',
                                                          'id': 'Landbrug m.v. 1162.2',
                                                          'key': '2',
                                                          'display_title': 'Landbrug m.v. 1162.2 - Andre selskaber m.v.'
                                                      },


                                                      'Landbrug m.v. 1162.9': {
                                                          'name': 'Andre spørgsmål',
                                                          'id': 'Landbrug m.v. 1162.9',
                                                          'key': '9',
                                                          'display_title': 'Landbrug m.v. 1162.9 - Andre spørgsmål'
                                                      }

                                                 },
                                                 'name': 'Selskaber m.v.',
                                                 'id': 'Landbrug m.v. 116.2',
                                                 'key': '2',
                                                 'display_title': 'Landbrug m.v. 116.2 - Selskaber m.v.'
                                             },


                                             'Landbrug m.v. 116.3': {
                                                 'name': 'Fortrinsstilling til suppleringsjord',
                                                 'id': 'Landbrug m.v. 116.3',
                                                 'key': '3',
                                                 'display_title': 'Landbrug m.v. 116.3 - Fortrinsstilling til suppleringsjord'
                                             },


                                             'Landbrug m.v. 116.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Landbrug m.v. 116.9',
                                                 'key': '9',
                                                 'display_title': 'Landbrug m.v. 116.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Betingelse for erhvervelse af adkomst',
                                        'id': 'Landbrug m.v. 11.6',
                                        'key': '6',
                                        'display_title': 'Landbrug m.v. 11.6 - Betingelse for erhvervelse af adkomst'
                                    },


                                    'Landbrug m.v. 11.7': {
                                        'name': 'Proforma og omgåelse',
                                        'id': 'Landbrug m.v. 11.7',
                                        'key': '7',
                                        'display_title': 'Landbrug m.v. 11.7 - Proforma og omgåelse'
                                    },


                                    'Landbrug m.v. 11.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Landbrug m.v. 11.9',
                                        'key': '9',
                                        'display_title': 'Landbrug m.v. 11.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Landbrugsejendomme',
                               'id': 'Landbrug m.v. 1.1',
                               'key': '1',
                               'display_title': 'Landbrug m.v. 1.1 - Landbrugsejendomme'
                           },


                           'Landbrug m.v. 1.2': {
                               'name': 'Jordfordeling',
                               'id': 'Landbrug m.v. 1.2',
                               'key': '2',
                               'display_title': 'Landbrug m.v. 1.2 - Jordfordeling'
                           },


                           'Landbrug m.v. 1.3': {
                               'name': 'Offentlige jordkøb',
                               'id': 'Landbrug m.v. 1.3',
                               'key': '3',
                               'display_title': 'Landbrug m.v. 1.3 - Offentlige jordkøb'
                           },


                           'Landbrug m.v. 1.4': {
                               'name': 'Huse på landet',
                               'id': 'Landbrug m.v. 1.4',
                               'key': '4',
                               'display_title': 'Landbrug m.v. 1.4 - Huse på landet'
                           },


                           'Landbrug m.v. 1.5': {
                               'name': 'Ekspropriation, se også Statsforfatningret 3.1',
                               'id': 'Landbrug m.v. 1.5',
                               'key': '5',
                               'display_title': 'Landbrug m.v. 1.5 - Ekspropriation, se også Statsforfatningret 3.1'
                           },


                           'Landbrug m.v. 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Landbrug m.v. 1.9',
                               'key': '9',
                               'display_title': 'Landbrug m.v. 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Strukturforhold',
                      'id': 'Landbrug m.v. 1',
                      'key': '1',
                      'display_title': 'Landbrug m.v. 1 - Strukturforhold'
                  },


                  'Landbrug m.v. 2': {
                      'subs': {

                           'Landbrug m.v. 2.1': {
                               'name': 'Statsgaranti og lånefinansiering',
                               'id': 'Landbrug m.v. 2.1',
                               'key': '1',
                               'display_title': 'Landbrug m.v. 2.1 - Statsgaranti og lånefinansiering'
                           },


                           'Landbrug m.v. 2.2': {
                               'name': 'Tilskud til strukturudvikling og særlig produktion',
                               'id': 'Landbrug m.v. 2.2',
                               'key': '2',
                               'display_title': 'Landbrug m.v. 2.2 - Tilskud til strukturudvikling og særlig produktion'
                           },


                           'Landbrug m.v. 2.3': {
                               'name': 'Straf og tilbagebetaling',
                               'id': 'Landbrug m.v. 2.3',
                               'key': '3',
                               'display_title': 'Landbrug m.v. 2.3 - Straf og tilbagebetaling'
                           },


                           'Landbrug m.v. 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Landbrug m.v. 2.9',
                               'key': '9',
                               'display_title': 'Landbrug m.v. 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Offentlig støtte til landbrug',
                      'id': 'Landbrug m.v. 2',
                      'key': '2',
                      'display_title': 'Landbrug m.v. 2 - Offentlig støtte til landbrug'
                  },


                  'Landbrug m.v. 3': {
                      'subs': {

                           'Landbrug m.v. 3.1': {
                               'subs': {

                                    'Landbrug m.v. 31.1': {
                                        'name': 'Arter, nyheder og sortsbeskyttelse',
                                        'id': 'Landbrug m.v. 31.1',
                                        'key': '1',
                                        'display_title': 'Landbrug m.v. 31.1 - Arter, nyheder og sortsbeskyttelse'
                                    },


                                    'Landbrug m.v. 31.2': {
                                        'name': 'Planteskadegørere og plantesygdomme',
                                        'id': 'Landbrug m.v. 31.2',
                                        'key': '2',
                                        'display_title': 'Landbrug m.v. 31.2 - Planteskadegørere og plantesygdomme'
                                    },


                                    'Landbrug m.v. 31.3': {
                                        'name': 'Gødning og jordforbedring',
                                        'id': 'Landbrug m.v. 31.3',
                                        'key': '3',
                                        'display_title': 'Landbrug m.v. 31.3 - Gødning og jordforbedring'
                                    },


                                    'Landbrug m.v. 31.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Landbrug m.v. 31.9',
                                        'key': '9',
                                        'display_title': 'Landbrug m.v. 31.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Planter',
                               'id': 'Landbrug m.v. 3.1',
                               'key': '1',
                               'display_title': 'Landbrug m.v. 3.1 - Planter'
                           },


                           'Landbrug m.v. 3.2': {
                               'subs': {

                                    'Landbrug m.v. 32.1': {
                                        'name': 'Dyreværn',
                                        'id': 'Landbrug m.v. 32.1',
                                        'key': '1',
                                        'display_title': 'Landbrug m.v. 32.1 - Dyreværn'
                                    },


                                    'Landbrug m.v. 32.2': {
                                        'name': 'Dyrehold og -opdræt',
                                        'id': 'Landbrug m.v. 32.2',
                                        'key': '2',
                                        'display_title': 'Landbrug m.v. 32.2 - Dyrehold og -opdræt'
                                    },


                                    'Landbrug m.v. 32.3': {
                                        'name': 'Mark og vejfred',
                                        'id': 'Landbrug m.v. 32.3',
                                        'key': '3',
                                        'display_title': 'Landbrug m.v. 32.3 - Mark og vejfred'
                                    },


                                    'Landbrug m.v. 32.4': {
                                        'name': 'Miljøkrav',
                                        'id': 'Landbrug m.v. 32.4',
                                        'key': '4',
                                        'display_title': 'Landbrug m.v. 32.4 - Miljøkrav'
                                    },


                                    'Landbrug m.v. 32.5': {
                                        'name': 'Husdyrsygdomme',
                                        'id': 'Landbrug m.v. 32.5',
                                        'key': '5',
                                        'display_title': 'Landbrug m.v. 32.5 - Husdyrsygdomme'
                                    },


                                    'Landbrug m.v. 32.6': {
                                        'name': 'Foderstoffer',
                                        'id': 'Landbrug m.v. 32.6',
                                        'key': '6',
                                        'display_title': 'Landbrug m.v. 32.6 - Foderstoffer'
                                    },


                                    'Landbrug m.v. 32.7': {
                                        'name': 'Veterinærvæsen',
                                        'id': 'Landbrug m.v. 32.7',
                                        'key': '7',
                                        'display_title': 'Landbrug m.v. 32.7 - Veterinærvæsen'
                                    },


                                    'Landbrug m.v. 32.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Landbrug m.v. 32.9',
                                        'key': '9',
                                        'display_title': 'Landbrug m.v. 32.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Dyr',
                               'id': 'Landbrug m.v. 3.2',
                               'key': '2',
                               'display_title': 'Landbrug m.v. 3.2 - Dyr'
                           },


                           'Landbrug m.v. 3.3': {
                               'name': 'Produktansvar',
                               'id': 'Landbrug m.v. 3.3',
                               'key': '3',
                               'display_title': 'Landbrug m.v. 3.3 - Produktansvar'
                           },


                           'Landbrug m.v. 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Landbrug m.v. 3.9',
                               'key': '9',
                               'display_title': 'Landbrug m.v. 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Planter og dyr',
                      'id': 'Landbrug m.v. 3',
                      'key': '3',
                      'display_title': 'Landbrug m.v. 3 - Planter og dyr'
                  },


                  'Landbrug m.v. 4': {
                      'name': 'Fødevarer',
                      'id': 'Landbrug m.v. 4',
                      'key': '4',
                      'display_title': 'Landbrug m.v. 4 - Fødevarer'
                  },


                  'Landbrug m.v. 5': {
                      'name': 'Skove og skovbrug',
                      'id': 'Landbrug m.v. 5',
                      'key': '5',
                      'display_title': 'Landbrug m.v. 5 - Skove og skovbrug'
                  },


                  'Landbrug m.v. 6': {
                      'name': 'Søer og vandløb, se også Veje og vand 2',
                      'id': 'Landbrug m.v. 6',
                      'key': '6',
                      'display_title': 'Landbrug m.v. 6 - Søer og vandløb, se også Veje og vand 2'
                  },


                  'Landbrug m.v. 7': {
                      'name': 'Jagt og fiskeri',
                      'id': 'Landbrug m.v. 7',
                      'key': '7',
                      'display_title': 'Landbrug m.v. 7 - Jagt og fiskeri'
                  },


                  'Landbrug m.v. 8': {
                      'subs': {

                           'Landbrug m.v. 8.1': {
                               'name': 'Efteruddannelse',
                               'id': 'Landbrug m.v. 8.1',
                               'key': '1',
                               'display_title': 'Landbrug m.v. 8.1 - Efteruddannelse'
                           },


                           'Landbrug m.v. 8.2': {
                               'name': 'Vikarordninger',
                               'id': 'Landbrug m.v. 8.2',
                               'key': '2',
                               'display_title': 'Landbrug m.v. 8.2 - Vikarordninger'
                           },


                           'Landbrug m.v. 8.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Landbrug m.v. 8.9',
                               'key': '9',
                               'display_title': 'Landbrug m.v. 8.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Ansættelsesforhold, se også Ansættelses- og arbejdsret 3',
                      'id': 'Landbrug m.v. 8',
                      'key': '8',
                      'display_title': 'Landbrug m.v. 8 - Ansættelsesforhold, se også Ansættelses- og arbejdsret 3'
                  },


                  'Landbrug m.v. 9': {
                      'name': 'Andre spørgsmål, herunder aftægt',
                      'id': 'Landbrug m.v. 9',
                      'key': '9',
                      'display_title': 'Landbrug m.v. 9 - Andre spørgsmål, herunder aftægt'
                  }

             },
             'name': 'Landbrug m.v. (Om skel, matrikulære forhold, inddæmning, kystforhold, diger og søer, se også Ejendomsret og Miljøret)',
             'id': 'Landbrug m.v.',
             'key': 'landbrug_m_v_',
             'display_title': 'Landbrug m.v.'
         },


         'Leje af fast ejendom': {
             'subs': {

                  'Leje af fast ejendom 1': {
                      'subs': {

                           'Leje af fast ejendom 1.1': {
                               'name': 'Huslejebegrebet',
                               'id': 'Leje af fast ejendom 1.1',
                               'key': '1',
                               'display_title': 'Leje af fast ejendom 1.1 - Huslejebegrebet'
                           },


                           'Leje af fast ejendom 1.2': {
                               'name': 'Lejeaftalens stiftelse',
                               'id': 'Leje af fast ejendom 1.2',
                               'key': '2',
                               'display_title': 'Leje af fast ejendom 1.2 - Lejeaftalens stiftelse'
                           },


                           'Leje af fast ejendom 1.3': {
                               'name': 'Lejeaftalens fortolkning',
                               'id': 'Leje af fast ejendom 1.3',
                               'key': '3',
                               'display_title': 'Leje af fast ejendom 1.3 - Lejeaftalens fortolkning'
                           },


                           'Leje af fast ejendom 1.4': {
                               'name': 'Depositum, forudbetaling og lejerindskud',
                               'id': 'Leje af fast ejendom 1.4',
                               'key': '4',
                               'display_title': 'Leje af fast ejendom 1.4 - Depositum, forudbetaling og lejerindskud'
                           },


                           'Leje af fast ejendom 1.5': {
                               'name': 'Aftalte ændringer',
                               'id': 'Leje af fast ejendom 1.5',
                               'key': '5',
                               'display_title': 'Leje af fast ejendom 1.5 - Aftalte ændringer'
                           },


                           'Leje af fast ejendom 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Leje af fast ejendom 1.9',
                               'key': '9',
                               'display_title': 'Leje af fast ejendom 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Lejeaftalen',
                      'id': 'Leje af fast ejendom 1',
                      'key': '1',
                      'display_title': 'Leje af fast ejendom 1 - Lejeaftalen'
                  },


                  'Leje af fast ejendom 2': {
                      'subs': {

                           'Leje af fast ejendom 2.1': {
                               'subs': {

                                    'Leje af fast ejendom 21.1': {
                                        'name': 'Retsforfølgning mod ejendommen',
                                        'id': 'Leje af fast ejendom 21.1',
                                        'key': '1',
                                        'display_title': 'Leje af fast ejendom 21.1 - Retsforfølgning mod ejendommen'
                                    },


                                    'Leje af fast ejendom 21.2': {
                                        'name': 'Overdragelse af ejendommen',
                                        'id': 'Leje af fast ejendom 21.2',
                                        'key': '2',
                                        'display_title': 'Leje af fast ejendom 21.2 - Overdragelse af ejendommen'
                                    },


                                    'Leje af fast ejendom 21.3': {
                                        'name': 'Ejendommens overgang ved skifte',
                                        'id': 'Leje af fast ejendom 21.3',
                                        'key': '3',
                                        'display_title': 'Leje af fast ejendom 21.3 - Ejendommens overgang ved skifte'
                                    },


                                    'Leje af fast ejendom 21.4': {
                                        'name': 'Lejeaftalens prioritetsstilling og tinglysning',
                                        'id': 'Leje af fast ejendom 21.4',
                                        'key': '4',
                                        'display_title': 'Leje af fast ejendom 21.4 - Lejeaftalens prioritetsstilling og tinglysning'
                                    },


                                    'Leje af fast ejendom 21.5': {
                                        'name': 'Tilbudspligt',
                                        'id': 'Leje af fast ejendom 21.5',
                                        'key': '5',
                                        'display_title': 'Leje af fast ejendom 21.5 - Tilbudspligt'
                                    },


                                    'Leje af fast ejendom 21.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Leje af fast ejendom 21.9',
                                        'key': '9',
                                        'display_title': 'Leje af fast ejendom 21.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Overgang på udlejerside',
                               'id': 'Leje af fast ejendom 2.1',
                               'key': '1',
                               'display_title': 'Leje af fast ejendom 2.1 - Overgang på udlejerside'
                           },


                           'Leje af fast ejendom 2.2': {
                               'subs': {

                                    'Leje af fast ejendom 22.1': {
                                        'name': 'Afståelsesret',
                                        'id': 'Leje af fast ejendom 22.1',
                                        'key': '1',
                                        'display_title': 'Leje af fast ejendom 22.1 - Afståelsesret'
                                    },


                                    'Leje af fast ejendom 22.2': {
                                        'name': 'Bytteret',
                                        'id': 'Leje af fast ejendom 22.2',
                                        'key': '2',
                                        'display_title': 'Leje af fast ejendom 22.2 - Bytteret'
                                    },


                                    'Leje af fast ejendom 22.3': {
                                        'subs': {

                                             'Leje af fast ejendom 223.1': {
                                                 'name': 'Dødsfald',
                                                 'id': 'Leje af fast ejendom 223.1',
                                                 'key': '1',
                                                 'display_title': 'Leje af fast ejendom 223.1 - Dødsfald'
                                             },


                                             'Leje af fast ejendom 223.2': {
                                                 'name': 'Separation og skilsmisse',
                                                 'id': 'Leje af fast ejendom 223.2',
                                                 'key': '2',
                                                 'display_title': 'Leje af fast ejendom 223.2 - Separation og skilsmisse'
                                             },


                                             'Leje af fast ejendom 223.3': {
                                                 'name': 'Samlivsforhold uden ægteskab',
                                                 'id': 'Leje af fast ejendom 223.3',
                                                 'key': '3',
                                                 'display_title': 'Leje af fast ejendom 223.3 - Samlivsforhold uden ægteskab'
                                             },


                                             'Leje af fast ejendom 223.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Leje af fast ejendom 223.9',
                                                 'key': '9',
                                                 'display_title': 'Leje af fast ejendom 223.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Overgang i familieforhold',
                                        'id': 'Leje af fast ejendom 22.3',
                                        'key': '3',
                                        'display_title': 'Leje af fast ejendom 22.3 - Overgang i familieforhold'
                                    },


                                    'Leje af fast ejendom 22.4': {
                                        'name': 'Fremleje',
                                        'id': 'Leje af fast ejendom 22.4',
                                        'key': '4',
                                        'display_title': 'Leje af fast ejendom 22.4 - Fremleje'
                                    },


                                    'Leje af fast ejendom 22.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Leje af fast ejendom 22.9',
                                        'key': '9',
                                        'display_title': 'Leje af fast ejendom 22.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Overgang på lejerside',
                               'id': 'Leje af fast ejendom 2.2',
                               'key': '2',
                               'display_title': 'Leje af fast ejendom 2.2 - Overgang på lejerside'
                           },


                           'Leje af fast ejendom 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Leje af fast ejendom 2.9',
                               'key': '9',
                               'display_title': 'Leje af fast ejendom 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Lejerettigheders overgang',
                      'id': 'Leje af fast ejendom 2',
                      'key': '2',
                      'display_title': 'Leje af fast ejendom 2 - Lejerettigheders overgang'
                  },


                  'Leje af fast ejendom 3': {
                      'subs': {

                           'Leje af fast ejendom 3.1': {
                               'name': 'Varme',
                               'id': 'Leje af fast ejendom 3.1',
                               'key': '1',
                               'display_title': 'Leje af fast ejendom 3.1 - Varme'
                           },


                           'Leje af fast ejendom 3.2': {
                               'name': 'Vand',
                               'id': 'Leje af fast ejendom 3.2',
                               'key': '2',
                               'display_title': 'Leje af fast ejendom 3.2 - Vand'
                           },


                           'Leje af fast ejendom 3.3': {
                               'name': 'Fællesantenneanlæg',
                               'id': 'Leje af fast ejendom 3.3',
                               'key': '3',
                               'display_title': 'Leje af fast ejendom 3.3 - Fællesantenneanlæg'
                           },


                           'Leje af fast ejendom 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Leje af fast ejendom 3.9',
                               'key': '9',
                               'display_title': 'Leje af fast ejendom 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Forsyningsspørgsmål',
                      'id': 'Leje af fast ejendom 3',
                      'key': '3',
                      'display_title': 'Leje af fast ejendom 3 - Forsyningsspørgsmål'
                  },


                  'Leje af fast ejendom 4': {
                      'subs': {

                           'Leje af fast ejendom 4.1': {
                               'name': 'I almindelighed',
                               'id': 'Leje af fast ejendom 4.1',
                               'key': '1',
                               'display_title': 'Leje af fast ejendom 4.1 - I almindelighed'
                           },


                           'Leje af fast ejendom 4.2': {
                               'subs': {

                                    'Leje af fast ejendom 42.1': {
                                        'name': 'Aftalte lejeforhøjelser',
                                        'id': 'Leje af fast ejendom 42.1',
                                        'key': '1',
                                        'display_title': 'Leje af fast ejendom 42.1 - Aftalte lejeforhøjelser'
                                    },


                                    'Leje af fast ejendom 42.2': {
                                        'name': 'Det lejedes værdi',
                                        'id': 'Leje af fast ejendom 42.2',
                                        'key': '2',
                                        'display_title': 'Leje af fast ejendom 42.2 - Det lejedes værdi'
                                    },


                                    'Leje af fast ejendom 42.3': {
                                        'name': 'Bevisregler',
                                        'id': 'Leje af fast ejendom 42.3',
                                        'key': '3',
                                        'display_title': 'Leje af fast ejendom 42.3 - Bevisregler'
                                    },


                                    'Leje af fast ejendom 42.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Leje af fast ejendom 42.9',
                                        'key': '9',
                                        'display_title': 'Leje af fast ejendom 42.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Erhvervslejemål',
                               'id': 'Leje af fast ejendom 4.2',
                               'key': '2',
                               'display_title': 'Leje af fast ejendom 4.2 - Erhvervslejemål'
                           },


                           'Leje af fast ejendom 4.3': {
                               'subs': {

                                    'Leje af fast ejendom 43.1': {
                                        'name': 'Omkostningsbestemt leje',
                                        'id': 'Leje af fast ejendom 43.1',
                                        'key': '1',
                                        'display_title': 'Leje af fast ejendom 43.1 - Omkostningsbestemt leje'
                                    },


                                    'Leje af fast ejendom 43.2': {
                                        'name': 'Forbedringsforhøjelser',
                                        'id': 'Leje af fast ejendom 43.2',
                                        'key': '2',
                                        'display_title': 'Leje af fast ejendom 43.2 - Forbedringsforhøjelser'
                                    },


                                    'Leje af fast ejendom 43.3': {
                                        'name': 'Skatte- og afgiftsforhøjelser',
                                        'id': 'Leje af fast ejendom 43.3',
                                        'key': '3',
                                        'display_title': 'Leje af fast ejendom 43.3 - Skatte- og afgiftsforhøjelser'
                                    },


                                    'Leje af fast ejendom 43.4': {
                                        'name': 'Vedligeholdelseskonti',
                                        'id': 'Leje af fast ejendom 43.4',
                                        'key': '4',
                                        'display_title': 'Leje af fast ejendom 43.4 - Vedligeholdelseskonti'
                                    },


                                    'Leje af fast ejendom 43.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Leje af fast ejendom 43.9',
                                        'key': '9',
                                        'display_title': 'Leje af fast ejendom 43.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Boliglejemål',
                               'id': 'Leje af fast ejendom 4.3',
                               'key': '3',
                               'display_title': 'Leje af fast ejendom 4.3 - Boliglejemål'
                           },


                           'Leje af fast ejendom 4.4': {
                               'name': 'Huslejeregulering for mindre ejendomme',
                               'id': 'Leje af fast ejendom 4.4',
                               'key': '4',
                               'display_title': 'Leje af fast ejendom 4.4 - Huslejeregulering for mindre ejendomme'
                           },


                           'Leje af fast ejendom 4.5': {
                               'name': 'Varslingen',
                               'id': 'Leje af fast ejendom 4.5',
                               'key': '5',
                               'display_title': 'Leje af fast ejendom 4.5 - Varslingen'
                           },


                           'Leje af fast ejendom 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Leje af fast ejendom 4.9',
                               'key': '9',
                               'display_title': 'Leje af fast ejendom 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Lejefastsættelse',
                      'id': 'Leje af fast ejendom 4',
                      'key': '4',
                      'display_title': 'Leje af fast ejendom 4 - Lejefastsættelse'
                  },


                  'Leje af fast ejendom 5': {
                      'subs': {

                           'Leje af fast ejendom 5.1': {
                               'subs': {

                                    'Leje af fast ejendom 51.1': {
                                        'name': 'Mangler ved det lejede',
                                        'id': 'Leje af fast ejendom 51.1',
                                        'key': '1',
                                        'display_title': 'Leje af fast ejendom 51.1 - Mangler ved det lejede'
                                    },


                                    'Leje af fast ejendom 51.2': {
                                        'name': 'Anden misligholdelse',
                                        'id': 'Leje af fast ejendom 51.2',
                                        'key': '2',
                                        'display_title': 'Leje af fast ejendom 51.2 - Anden misligholdelse'
                                    },


                                    'Leje af fast ejendom 51.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Leje af fast ejendom 51.9',
                                        'key': '9',
                                        'display_title': 'Leje af fast ejendom 51.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Udlejers misligholdelse',
                               'id': 'Leje af fast ejendom 5.1',
                               'key': '1',
                               'display_title': 'Leje af fast ejendom 5.1 - Udlejers misligholdelse'
                           },


                           'Leje af fast ejendom 5.2': {
                               'subs': {

                                    'Leje af fast ejendom 52.1': {
                                        'name': 'Lejebetaling',
                                        'id': 'Leje af fast ejendom 52.1',
                                        'key': '1',
                                        'display_title': 'Leje af fast ejendom 52.1 - Lejebetaling'
                                    },


                                    'Leje af fast ejendom 52.2': {
                                        'name': 'Anden misligholdelse',
                                        'id': 'Leje af fast ejendom 52.2',
                                        'key': '2',
                                        'display_title': 'Leje af fast ejendom 52.2 - Anden misligholdelse'
                                    },


                                    'Leje af fast ejendom 52.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Leje af fast ejendom 52.9',
                                        'key': '9',
                                        'display_title': 'Leje af fast ejendom 52.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Lejers misligholdelse',
                               'id': 'Leje af fast ejendom 5.2',
                               'key': '2',
                               'display_title': 'Leje af fast ejendom 5.2 - Lejers misligholdelse'
                           },


                           'Leje af fast ejendom 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Leje af fast ejendom 5.9',
                               'key': '9',
                               'display_title': 'Leje af fast ejendom 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Misligholdelse',
                      'id': 'Leje af fast ejendom 5',
                      'key': '5',
                      'display_title': 'Leje af fast ejendom 5 - Misligholdelse'
                  },


                  'Leje af fast ejendom 6': {
                      'subs': {

                           'Leje af fast ejendom 6.1': {
                               'subs': {

                                    'Leje af fast ejendom 61.1': {
                                        'name': 'Lejebetaling',
                                        'id': 'Leje af fast ejendom 61.1',
                                        'key': '1',
                                        'display_title': 'Leje af fast ejendom 61.1 - Lejebetaling'
                                    },


                                    'Leje af fast ejendom 61.2': {
                                        'name': 'Husorden',
                                        'id': 'Leje af fast ejendom 61.2',
                                        'key': '2',
                                        'display_title': 'Leje af fast ejendom 61.2 - Husorden'
                                    },


                                    'Leje af fast ejendom 61.3': {
                                        'name': 'Vanrøgt',
                                        'id': 'Leje af fast ejendom 61.3',
                                        'key': '3',
                                        'display_title': 'Leje af fast ejendom 61.3 - Vanrøgt'
                                    },


                                    'Leje af fast ejendom 61.9': {
                                        'name': 'Anden misligholdelse, se også Fogedret 62.2',
                                        'id': 'Leje af fast ejendom 61.9',
                                        'key': '9',
                                        'display_title': 'Leje af fast ejendom 61.9 - Anden misligholdelse, se også Fogedret 62.2'
                                    }

                               },
                               'name': 'Udlejers ophævelse',
                               'id': 'Leje af fast ejendom 6.1',
                               'key': '1',
                               'display_title': 'Leje af fast ejendom 6.1 - Udlejers ophævelse'
                           },


                           'Leje af fast ejendom 6.2': {
                               'subs': {

                                    'Leje af fast ejendom 62.1': {
                                        'name': 'Tidsbegrænsede lejemål',
                                        'id': 'Leje af fast ejendom 62.1',
                                        'key': '1',
                                        'display_title': 'Leje af fast ejendom 62.1 - Tidsbegrænsede lejemål'
                                    },


                                    'Leje af fast ejendom 62.2': {
                                        'name': 'Uopsigelighed',
                                        'id': 'Leje af fast ejendom 62.2',
                                        'key': '2',
                                        'display_title': 'Leje af fast ejendom 62.2 - Uopsigelighed'
                                    },


                                    'Leje af fast ejendom 62.3': {
                                        'name': 'Udlejers egen brug',
                                        'id': 'Leje af fast ejendom 62.3',
                                        'key': '3',
                                        'display_title': 'Leje af fast ejendom 62.3 - Udlejers egen brug'
                                    },


                                    'Leje af fast ejendom 62.9': {
                                        'name': 'Andre opsigelsesgrunde',
                                        'id': 'Leje af fast ejendom 62.9',
                                        'key': '9',
                                        'display_title': 'Leje af fast ejendom 62.9 - Andre opsigelsesgrunde'
                                    }

                               },
                               'name': 'Udlejers opsigelse',
                               'id': 'Leje af fast ejendom 6.2',
                               'key': '2',
                               'display_title': 'Leje af fast ejendom 6.2 - Udlejers opsigelse'
                           },


                           'Leje af fast ejendom 6.3': {
                               'name': 'Lejers ophævelse',
                               'id': 'Leje af fast ejendom 6.3',
                               'key': '3',
                               'display_title': 'Leje af fast ejendom 6.3 - Lejers ophævelse'
                           },


                           'Leje af fast ejendom 6.4': {
                               'name': 'Lejers opsigelse',
                               'id': 'Leje af fast ejendom 6.4',
                               'key': '4',
                               'display_title': 'Leje af fast ejendom 6.4 - Lejers opsigelse'
                           },


                           'Leje af fast ejendom 6.5': {
                               'subs': {

                                    'Leje af fast ejendom 65.1': {
                                        'name': 'Fremgangsmåde',
                                        'id': 'Leje af fast ejendom 65.1',
                                        'key': '1',
                                        'display_title': 'Leje af fast ejendom 65.1 - Fremgangsmåde'
                                    },


                                    'Leje af fast ejendom 65.2': {
                                        'name': 'Kravets størrelse',
                                        'id': 'Leje af fast ejendom 65.2',
                                        'key': '2',
                                        'display_title': 'Leje af fast ejendom 65.2 - Kravets størrelse'
                                    },


                                    'Leje af fast ejendom 65.3': {
                                        'name': 'Særligt om almennyttige boliger',
                                        'id': 'Leje af fast ejendom 65.3',
                                        'key': '3',
                                        'display_title': 'Leje af fast ejendom 65.3 - Særligt om almennyttige boliger'
                                    },


                                    'Leje af fast ejendom 65.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Leje af fast ejendom 65.9',
                                        'key': '9',
                                        'display_title': 'Leje af fast ejendom 65.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Flytteopgør',
                               'id': 'Leje af fast ejendom 6.5',
                               'key': '5',
                               'display_title': 'Leje af fast ejendom 6.5 - Flytteopgør'
                           },


                           'Leje af fast ejendom 6.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Leje af fast ejendom 6.9',
                               'key': '9',
                               'display_title': 'Leje af fast ejendom 6.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Lejemålets ophør',
                      'id': 'Leje af fast ejendom 6',
                      'key': '6',
                      'display_title': 'Leje af fast ejendom 6 - Lejemålets ophør'
                  },


                  'Leje af fast ejendom 7': {
                      'subs': {

                           'Leje af fast ejendom 7.1': {
                               'name': 'Huslejenævnssager',
                               'id': 'Leje af fast ejendom 7.1',
                               'key': '1',
                               'display_title': 'Leje af fast ejendom 7.1 - Huslejenævnssager'
                           },


                           'Leje af fast ejendom 7.2': {
                               'name': 'Boligretssager',
                               'id': 'Leje af fast ejendom 7.2',
                               'key': '2',
                               'display_title': 'Leje af fast ejendom 7.2 - Boligretssager'
                           },


                           'Leje af fast ejendom 7.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Leje af fast ejendom 7.9',
                               'key': '9',
                               'display_title': 'Leje af fast ejendom 7.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Boligsager, se også Fogedret 6.2',
                      'id': 'Leje af fast ejendom 7',
                      'key': '7',
                      'display_title': 'Leje af fast ejendom 7 - Boligsager, se også Fogedret 6.2'
                  },


                  'Leje af fast ejendom 9': {
                      'subs': {

                           'Leje af fast ejendom 9.1': {
                               'name': 'Beboerrepræsentation',
                               'id': 'Leje af fast ejendom 9.1',
                               'key': '1',
                               'display_title': 'Leje af fast ejendom 9.1 - Beboerrepræsentation'
                           },


                           'Leje af fast ejendom 9.2': {
                               'name': 'Beboerklagenævn',
                               'id': 'Leje af fast ejendom 9.2',
                               'key': '2',
                               'display_title': 'Leje af fast ejendom 9.2 - Beboerklagenævn'
                           },


                           'Leje af fast ejendom 9.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Leje af fast ejendom 9.9',
                               'key': '9',
                               'display_title': 'Leje af fast ejendom 9.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Andre spørgsmål',
                      'id': 'Leje af fast ejendom 9',
                      'key': '9',
                      'display_title': 'Leje af fast ejendom 9 - Andre spørgsmål'
                  }

             },
             'name': 'Leje af fast ejendom',
             'id': 'Leje af fast ejendom',
             'key': 'leje_af_fast_ejendom',
             'display_title': 'Leje af fast ejendom'
         },


         'Leje, brugsret og forvaring vedrørende løsøre': {
             'subs': {

                  'Leje, brugsret og forvaring vedrørende løsøre 1': {
                      'name': 'Leasing',
                      'id': 'Leje, brugsret og forvaring vedrørende løsøre 1',
                      'key': '1',
                      'display_title': 'Leje, brugsret og forvaring vedrørende løsøre 1 - Leasing'
                  },


                  'Leje, brugsret og forvaring vedrørende løsøre 2': {
                      'name': 'Leje og lån i øvrigt',
                      'id': 'Leje, brugsret og forvaring vedrørende løsøre 2',
                      'key': '2',
                      'display_title': 'Leje, brugsret og forvaring vedrørende løsøre 2 - Leje og lån i øvrigt'
                  },


                  'Leje, brugsret og forvaring vedrørende løsøre 3': {
                      'name': 'Forvaring',
                      'id': 'Leje, brugsret og forvaring vedrørende løsøre 3',
                      'key': '3',
                      'display_title': 'Leje, brugsret og forvaring vedrørende løsøre 3 - Forvaring'
                  },


                  'Leje, brugsret og forvaring vedrørende løsøre 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Leje, brugsret og forvaring vedrørende løsøre 9',
                      'key': '9',
                      'display_title': 'Leje, brugsret og forvaring vedrørende løsøre 9 - Andre spørgsmål'
                  }

             },
             'name': 'Leje, brugsret og forvaring vedrørende løsøre',
             'id': 'Leje, brugsret og forvaring vedrørende løsøre',
             'key': 'leje__brugsret_og_forvaring_vedr_rende_l_s_re',
             'display_title': 'Leje, brugsret og forvaring vedrørende løsøre'
         },


         'Markedsret': {
             'subs': {

                  'Markedsret 1': {
                      'subs': {

                           'Markedsret 1.1': {
                               'name': 'Vildledende reklame m.v.',
                               'id': 'Markedsret 1.1',
                               'key': '1',
                               'display_title': 'Markedsret 1.1 - Vildledende reklame m.v.'
                           },


                           'Markedsret 1.2': {
                               'name': 'Efterligning',
                               'id': 'Markedsret 1.2',
                               'key': '2',
                               'display_title': 'Markedsret 1.2 - Efterligning'
                           },


                           'Markedsret 1.3': {
                               'name': 'Forretningskendetegn',
                               'id': 'Markedsret 1.3',
                               'key': '3',
                               'display_title': 'Markedsret 1.3 - Forretningskendetegn'
                           },


                           'Markedsret 1.4': {
                               'name': 'Tilgift',
                               'id': 'Markedsret 1.4',
                               'key': '4',
                               'display_title': 'Markedsret 1.4 - Tilgift'
                           },


                           'Markedsret 1.5': {
                               'name': 'Lodtrækning, konkurrencer',
                               'id': 'Markedsret 1.5',
                               'key': '5',
                               'display_title': 'Markedsret 1.5 - Lodtrækning, konkurrencer'
                           },


                           'Markedsret 1.6': {
                               'name': 'Erhvervshemmeligheder',
                               'id': 'Markedsret 1.6',
                               'key': '6',
                               'display_title': 'Markedsret 1.6 - Erhvervshemmeligheder'
                           },


                           'Markedsret 1.7': {
                               'name': 'God markedsføringsskik',
                               'id': 'Markedsret 1.7',
                               'key': '7',
                               'display_title': 'Markedsret 1.7 - God markedsføringsskik'
                           },


                           'Markedsret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Markedsret 1.9',
                               'key': '9',
                               'display_title': 'Markedsret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Markedsføring',
                      'id': 'Markedsret 1',
                      'key': '1',
                      'display_title': 'Markedsret 1 - Markedsføring'
                  },


                  'Markedsret 2': {
                      'name': 'Forbrugerbeskyttelse',
                      'id': 'Markedsret 2',
                      'key': '2',
                      'display_title': 'Markedsret 2 - Forbrugerbeskyttelse'
                  },


                  'Markedsret 3': {
                      'name': 'Monopoler, andre konkurrencebegrænsninger samt prisregulering',
                      'id': 'Markedsret 3',
                      'key': '3',
                      'display_title': 'Markedsret 3 - Monopoler, andre konkurrencebegrænsninger samt prisregulering'
                  },


                  'Markedsret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Markedsret 9',
                      'key': '9',
                      'display_title': 'Markedsret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Markedsret (se også Erhvervsret)',
             'id': 'Markedsret',
             'key': 'markedsret',
             'display_title': 'Markedsret'
         },


         'Menneskerettigheder': {
             'subs': {

                  'Menneskerettigheder 1': {
                      'subs': {

                           'Menneskerettigheder 1.1': {
                               'name': 'Frihed og sikkerhed',
                               'id': 'Menneskerettigheder 1.1',
                               'key': '1',
                               'display_title': 'Menneskerettigheder 1.1 - Frihed og sikkerhed'
                           },


                           'Menneskerettigheder 1.2': {
                               'subs': {

                                    'Menneskerettigheder 12.1': {
                                        'name': 'Retfærdig rettergang',
                                        'id': 'Menneskerettigheder 12.1',
                                        'key': '1',
                                        'display_title': 'Menneskerettigheder 12.1 - Retfærdig rettergang'
                                    },


                                    'Menneskerettigheder 12.2': {
                                        'name': 'Inden rimelig tid',
                                        'id': 'Menneskerettigheder 12.2',
                                        'key': '2',
                                        'display_title': 'Menneskerettigheder 12.2 - Inden rimelig tid'
                                    },


                                    'Menneskerettigheder 12.3': {
                                        'subs': {

                                             'Menneskerettigheder 123.1': {
                                                 'name': 'Vidneafhøring',
                                                 'id': 'Menneskerettigheder 123.1',
                                                 'key': '1',
                                                 'display_title': 'Menneskerettigheder 123.1 - Vidneafhøring'
                                             },


                                             'Menneskerettigheder 123.2': {
                                                 'name': 'Politirapporter',
                                                 'id': 'Menneskerettigheder 123.2',
                                                 'key': '2',
                                                 'display_title': 'Menneskerettigheder 123.2 - Politirapporter'
                                             },


                                             'Menneskerettigheder 123.9': {
                                                 'name': 'Andre bevisspørgsmål',
                                                 'id': 'Menneskerettigheder 123.9',
                                                 'key': '9',
                                                 'display_title': 'Menneskerettigheder 123.9 - Andre bevisspørgsmål'
                                             }

                                        },
                                        'name': 'Bevismidler',
                                        'id': 'Menneskerettigheder 12.3',
                                        'key': '3',
                                        'display_title': 'Menneskerettigheder 12.3 - Bevismidler'
                                    },


                                    'Menneskerettigheder 12.4': {
                                        'name': 'Dommere',
                                        'id': 'Menneskerettigheder 12.4',
                                        'key': '4',
                                        'display_title': 'Menneskerettigheder 12.4 - Dommere'
                                    },


                                    'Menneskerettigheder 12.5': {
                                        'name': 'Processuel ligestilling',
                                        'id': 'Menneskerettigheder 12.5',
                                        'key': '5',
                                        'display_title': 'Menneskerettigheder 12.5 - Processuel ligestilling'
                                    },


                                    'Menneskerettigheder 12.6': {
                                        'name': 'Adgang til domstolsprøvelse',
                                        'id': 'Menneskerettigheder 12.6',
                                        'key': '6',
                                        'display_title': 'Menneskerettigheder 12.6 - Adgang til domstolsprøvelse'
                                    },


                                    'Menneskerettigheder 12.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Menneskerettigheder 12.9',
                                        'key': '9',
                                        'display_title': 'Menneskerettigheder 12.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Processpørgsmål',
                               'id': 'Menneskerettigheder 1.2',
                               'key': '2',
                               'display_title': 'Menneskerettigheder 1.2 - Processpørgsmål'
                           },


                           'Menneskerettigheder 1.3': {
                               'name': 'Hjemmel',
                               'id': 'Menneskerettigheder 1.3',
                               'key': '3',
                               'display_title': 'Menneskerettigheder 1.3 - Hjemmel'
                           },


                           'Menneskerettigheder 1.4': {
                               'name': 'Privatliv og familieliv',
                               'id': 'Menneskerettigheder 1.4',
                               'key': '4',
                               'display_title': 'Menneskerettigheder 1.4 - Privatliv og familieliv'
                           },


                           'Menneskerettigheder 1.5': {
                               'name': 'Ytrings og informationsfrihed',
                               'id': 'Menneskerettigheder 1.5',
                               'key': '5',
                               'display_title': 'Menneskerettigheder 1.5 - Ytrings og informationsfrihed'
                           },


                           'Menneskerettigheder 1.6': {
                               'name': 'Forenings- og forsamlingsfrihed',
                               'id': 'Menneskerettigheder 1.6',
                               'key': '6',
                               'display_title': 'Menneskerettigheder 1.6 - Forenings- og forsamlingsfrihed'
                           },


                           'Menneskerettigheder 1.7': {
                               'name': 'Diskrimination',
                               'id': 'Menneskerettigheder 1.7',
                               'key': '7',
                               'display_title': 'Menneskerettigheder 1.7 - Diskrimination'
                           },


                           'Menneskerettigheder 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Menneskerettigheder 1.9',
                               'key': '9',
                               'display_title': 'Menneskerettigheder 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'EMRK',
                      'id': 'Menneskerettigheder 1',
                      'key': '1',
                      'display_title': 'Menneskerettigheder 1 - EMRK'
                  },


                  'Menneskerettigheder 2': {
                      'name': 'FN-menneskeretskonventioner',
                      'id': 'Menneskerettigheder 2',
                      'key': '2',
                      'display_title': 'Menneskerettigheder 2 - FN-menneskeretskonventioner'
                  },


                  'Menneskerettigheder 3': {
                      'name': 'ILO-konventioner',
                      'id': 'Menneskerettigheder 3',
                      'key': '3',
                      'display_title': 'Menneskerettigheder 3 - ILO-konventioner'
                  },


                  'Menneskerettigheder 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Menneskerettigheder 9',
                      'key': '9',
                      'display_title': 'Menneskerettigheder 9 - Andre spørgsmål'
                  }

             },
             'name': 'Menneskerettigheder',
             'id': 'Menneskerettigheder',
             'key': 'menneskerettigheder',
             'display_title': 'Menneskerettigheder'
         },


         'Miljøret': {
             'subs': {

                  'Miljøret 1': {
                      'subs': {

                           'Miljøret 1.1': {
                               'name': 'Planlovgivning',
                               'id': 'Miljøret 1.1',
                               'key': '1',
                               'display_title': 'Miljøret 1.1 - Planlovgivning'
                           },


                           'Miljøret 1.2': {
                               'subs': {

                                    'Miljøret 12.1': {
                                        'name': 'Naturfredning',
                                        'id': 'Miljøret 12.1',
                                        'key': '1',
                                        'display_title': 'Miljøret 12.1 - Naturfredning'
                                    },


                                    'Miljøret 12.2': {
                                        'name': 'Bygningsfredning',
                                        'id': 'Miljøret 12.2',
                                        'key': '2',
                                        'display_title': 'Miljøret 12.2 - Bygningsfredning'
                                    },


                                    'Miljøret 12.9': {
                                        'name': 'Anden fredning',
                                        'id': 'Miljøret 12.9',
                                        'key': '9',
                                        'display_title': 'Miljøret 12.9 - Anden fredning'
                                    }

                               },
                               'name': 'Fredning',
                               'id': 'Miljøret 1.2',
                               'key': '2',
                               'display_title': 'Miljøret 1.2 - Fredning'
                           },


                           'Miljøret 1.3': {
                               'name': 'Forurening, se også Veje og vand 2.2',
                               'id': 'Miljøret 1.3',
                               'key': '3',
                               'display_title': 'Miljøret 1.3 - Forurening, se også Veje og vand 2.2'
                           },


                           'Miljøret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Miljøret 1.9',
                               'key': '9',
                               'display_title': 'Miljøret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Miljøbeskyttelse',
                      'id': 'Miljøret 1',
                      'key': '1',
                      'display_title': 'Miljøret 1 - Miljøbeskyttelse'
                  },


                  'Miljøret 2': {
                      'subs': {

                           'Miljøret 2.1': {
                               'subs': {

                                    'Miljøret 21.1': {
                                        'name': 'Opførelse og indretning',
                                        'id': 'Miljøret 21.1',
                                        'key': '1',
                                        'display_title': 'Miljøret 21.1 - Opførelse og indretning'
                                    },


                                    'Miljøret 21.2': {
                                        'name': 'Vedligeholdelse',
                                        'id': 'Miljøret 21.2',
                                        'key': '2',
                                        'display_title': 'Miljøret 21.2 - Vedligeholdelse'
                                    },


                                    'Miljøret 21.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Miljøret 21.9',
                                        'key': '9',
                                        'display_title': 'Miljøret 21.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Byggebetingelser',
                               'id': 'Miljøret 2.1',
                               'key': '1',
                               'display_title': 'Miljøret 2.1 - Byggebetingelser'
                           },


                           'Miljøret 2.2': {
                               'name': 'Byfornyelse, sanering, se også Boligformer 4',
                               'id': 'Miljøret 2.2',
                               'key': '2',
                               'display_title': 'Miljøret 2.2 - Byfornyelse, sanering, se også Boligformer 4'
                           },


                           'Miljøret 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Miljøret 2.9',
                               'key': '9',
                               'display_title': 'Miljøret 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Byggeret',
                      'id': 'Miljøret 2',
                      'key': '2',
                      'display_title': 'Miljøret 2 - Byggeret'
                  },


                  'Miljøret 3': {
                      'subs': {

                           'Miljøret 3.1': {
                               'name': 'Hegn',
                               'id': 'Miljøret 3.1',
                               'key': '1',
                               'display_title': 'Miljøret 3.1 - Hegn'
                           },


                           'Miljøret 3.2': {
                               'name': 'Naboretlige grundsætninger',
                               'id': 'Miljøret 3.2',
                               'key': '2',
                               'display_title': 'Miljøret 3.2 - Naboretlige grundsætninger'
                           },


                           'Miljøret 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Miljøret 3.9',
                               'key': '9',
                               'display_title': 'Miljøret 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Naboforhold',
                      'id': 'Miljøret 3',
                      'key': '3',
                      'display_title': 'Miljøret 3 - Naboforhold'
                  },


                  'Miljøret 4': {
                      'subs': {

                           'Miljøret 4.1': {
                               'name': 'Private servitutter',
                               'id': 'Miljøret 4.1',
                               'key': '1',
                               'display_title': 'Miljøret 4.1 - Private servitutter'
                           },


                           'Miljøret 4.2': {
                               'name': 'Offentlige servitutter',
                               'id': 'Miljøret 4.2',
                               'key': '2',
                               'display_title': 'Miljøret 4.2 - Offentlige servitutter'
                           },


                           'Miljøret 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Miljøret 4.9',
                               'key': '9',
                               'display_title': 'Miljøret 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Servitutter',
                      'id': 'Miljøret 4',
                      'key': '4',
                      'display_title': 'Miljøret 4 - Servitutter'
                  },


                  'Miljøret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Miljøret 9',
                      'key': '9',
                      'display_title': 'Miljøret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Miljøret (se også Landbrug m.v., Veje og vand)',
             'id': 'Miljøret',
             'key': 'milj_ret',
             'display_title': 'Miljøret'
         },


         'Pant og tilbageholdsret': {
             'subs': {

                  'Pant og tilbageholdsret 1': {
                      'subs': {

                           'Pant og tilbageholdsret 1.1': {
                               'name': 'Panteret',
                               'id': 'Pant og tilbageholdsret 1.1',
                               'key': '1',
                               'display_title': 'Pant og tilbageholdsret 1.1 - Panteret'
                           },


                           'Pant og tilbageholdsret 1.2': {
                               'name': 'Panterets stiftelse',
                               'id': 'Pant og tilbageholdsret 1.2',
                               'key': '2',
                               'display_title': 'Pant og tilbageholdsret 1.2 - Panterets stiftelse'
                           },


                           'Pant og tilbageholdsret 1.3': {
                               'name': 'Pantet',
                               'id': 'Pant og tilbageholdsret 1.3',
                               'key': '3',
                               'display_title': 'Pant og tilbageholdsret 1.3 - Pantet'
                           },


                           'Pant og tilbageholdsret 1.4': {
                               'name': 'Pantekrav',
                               'id': 'Pant og tilbageholdsret 1.4',
                               'key': '4',
                               'display_title': 'Pant og tilbageholdsret 1.4 - Pantekrav'
                           },


                           'Pant og tilbageholdsret 1.5': {
                               'name': 'Overdragelse',
                               'id': 'Pant og tilbageholdsret 1.5',
                               'key': '5',
                               'display_title': 'Pant og tilbageholdsret 1.5 - Overdragelse'
                           },


                           'Pant og tilbageholdsret 1.6': {
                               'name': 'Ophør',
                               'id': 'Pant og tilbageholdsret 1.6',
                               'key': '6',
                               'display_title': 'Pant og tilbageholdsret 1.6 - Ophør'
                           },


                           'Pant og tilbageholdsret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Pant og tilbageholdsret 1.9',
                               'key': '9',
                               'display_title': 'Pant og tilbageholdsret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Almindelige spørgsmål',
                      'id': 'Pant og tilbageholdsret 1',
                      'key': '1',
                      'display_title': 'Pant og tilbageholdsret 1 - Almindelige spørgsmål'
                  },


                  'Pant og tilbageholdsret 2': {
                      'subs': {

                           'Pant og tilbageholdsret 2.1': {
                               'subs': {

                                    'Pant og tilbageholdsret 21.1': {
                                        'name': 'Betalingspligten',
                                        'id': 'Pant og tilbageholdsret 21.1',
                                        'key': '1',
                                        'display_title': 'Pant og tilbageholdsret 21.1 - Betalingspligten'
                                    },


                                    'Pant og tilbageholdsret 21.2': {
                                        'name': 'Restance på prioriteter',
                                        'id': 'Pant og tilbageholdsret 21.2',
                                        'key': '2',
                                        'display_title': 'Pant og tilbageholdsret 21.2 - Restance på prioriteter'
                                    },


                                    'Pant og tilbageholdsret 21.3': {
                                        'name': 'Forringelse af pantet',
                                        'id': 'Pant og tilbageholdsret 21.3',
                                        'key': '3',
                                        'display_title': 'Pant og tilbageholdsret 21.3 - Forringelse af pantet'
                                    },


                                    'Pant og tilbageholdsret 21.4': {
                                        'name': 'Ejerskifte',
                                        'id': 'Pant og tilbageholdsret 21.4',
                                        'key': '4',
                                        'display_title': 'Pant og tilbageholdsret 21.4 - Ejerskifte'
                                    },


                                    'Pant og tilbageholdsret 21.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Pant og tilbageholdsret 21.9',
                                        'key': '9',
                                        'display_title': 'Pant og tilbageholdsret 21.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Forholdet mellem panthaver og pantsætter',
                               'id': 'Pant og tilbageholdsret 2.1',
                               'key': '1',
                               'display_title': 'Pant og tilbageholdsret 2.1 - Forholdet mellem panthaver og pantsætter'
                           },


                           'Pant og tilbageholdsret 2.2': {
                               'subs': {

                                    'Pant og tilbageholdsret 22.1': {
                                        'subs': {

                                             'Pant og tilbageholdsret 221.1': {
                                                 'name': 'Indrettelse',
                                                 'id': 'Pant og tilbageholdsret 221.1',
                                                 'key': '1',
                                                 'display_title': 'Pant og tilbageholdsret 221.1 - Indrettelse'
                                             },


                                             'Pant og tilbageholdsret 221.2': {
                                                 'name': 'Tilbehør',
                                                 'id': 'Pant og tilbageholdsret 221.2',
                                                 'key': '2',
                                                 'display_title': 'Pant og tilbageholdsret 221.2 - Tilbehør'
                                             },


                                             'Pant og tilbageholdsret 221.3': {
                                                 'name': 'Udskillelse',
                                                 'id': 'Pant og tilbageholdsret 221.3',
                                                 'key': '3',
                                                 'display_title': 'Pant og tilbageholdsret 221.3 - Udskillelse'
                                             },


                                             'Pant og tilbageholdsret 221.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Pant og tilbageholdsret 221.9',
                                                 'key': '9',
                                                 'display_title': 'Pant og tilbageholdsret 221.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'TL § 37',
                                        'id': 'Pant og tilbageholdsret 22.1',
                                        'key': '1',
                                        'display_title': 'Pant og tilbageholdsret 22.1 - TL § 37'
                                    },


                                    'Pant og tilbageholdsret 22.2': {
                                        'subs': {

                                             'Pant og tilbageholdsret 222.1': {
                                                 'name': 'Indrettelse',
                                                 'id': 'Pant og tilbageholdsret 222.1',
                                                 'key': '1',
                                                 'display_title': 'Pant og tilbageholdsret 222.1 - Indrettelse'
                                             },


                                             'Pant og tilbageholdsret 222.2': {
                                                 'name': 'Ejers bekostning',
                                                 'id': 'Pant og tilbageholdsret 222.2',
                                                 'key': '2',
                                                 'display_title': 'Pant og tilbageholdsret 222.2 - Ejers bekostning'
                                             },


                                             'Pant og tilbageholdsret 222.3': {
                                                 'name': 'Tilbehør',
                                                 'id': 'Pant og tilbageholdsret 222.3',
                                                 'key': '3',
                                                 'display_title': 'Pant og tilbageholdsret 222.3 - Tilbehør'
                                             },


                                             'Pant og tilbageholdsret 222.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Pant og tilbageholdsret 222.9',
                                                 'key': '9',
                                                 'display_title': 'Pant og tilbageholdsret 222.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'TL § 38',
                                        'id': 'Pant og tilbageholdsret 22.2',
                                        'key': '2',
                                        'display_title': 'Pant og tilbageholdsret 22.2 - TL § 38'
                                    },


                                    'Pant og tilbageholdsret 22.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Pant og tilbageholdsret 22.9',
                                        'key': '9',
                                        'display_title': 'Pant og tilbageholdsret 22.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Pant i tilbehør og frugter',
                               'id': 'Pant og tilbageholdsret 2.2',
                               'key': '2',
                               'display_title': 'Pant og tilbageholdsret 2.2 - Pant i tilbehør og frugter'
                           },


                           'Pant og tilbageholdsret 2.3': {
                               'subs': {

                                    'Pant og tilbageholdsret 23.1': {
                                        'name': 'Ophør af forprioriteter',
                                        'id': 'Pant og tilbageholdsret 23.1',
                                        'key': '1',
                                        'display_title': 'Pant og tilbageholdsret 23.1 - Ophør af forprioriteter'
                                    },


                                    'Pant og tilbageholdsret 23.2': {
                                        'name': 'Andre ændringer af forprioriteter',
                                        'id': 'Pant og tilbageholdsret 23.2',
                                        'key': '2',
                                        'display_title': 'Pant og tilbageholdsret 23.2 - Andre ændringer af forprioriteter'
                                    },


                                    'Pant og tilbageholdsret 23.3': {
                                        'name': 'Særlig om ejerpantebreve og skadesløsbreve',
                                        'id': 'Pant og tilbageholdsret 23.3',
                                        'key': '3',
                                        'display_title': 'Pant og tilbageholdsret 23.3 - Særlig om ejerpantebreve og skadesløsbreve'
                                    },


                                    'Pant og tilbageholdsret 23.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Pant og tilbageholdsret 23.9',
                                        'key': '9',
                                        'display_title': 'Pant og tilbageholdsret 23.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Forholdet mellem flere panthavere',
                               'id': 'Pant og tilbageholdsret 2.3',
                               'key': '3',
                               'display_title': 'Pant og tilbageholdsret 2.3 - Forholdet mellem flere panthavere'
                           },


                           'Pant og tilbageholdsret 2.4': {
                               'name': 'Panthaverens fyldestgørelse',
                               'id': 'Pant og tilbageholdsret 2.4',
                               'key': '4',
                               'display_title': 'Pant og tilbageholdsret 2.4 - Panthaverens fyldestgørelse'
                           },


                           'Pant og tilbageholdsret 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Pant og tilbageholdsret 2.9',
                               'key': '9',
                               'display_title': 'Pant og tilbageholdsret 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Pant i fast ejendom',
                      'id': 'Pant og tilbageholdsret 2',
                      'key': '2',
                      'display_title': 'Pant og tilbageholdsret 2 - Pant i fast ejendom'
                  },


                  'Pant og tilbageholdsret 3': {
                      'subs': {

                           'Pant og tilbageholdsret 3.1': {
                               'name': 'Stiftelse',
                               'id': 'Pant og tilbageholdsret 3.1',
                               'key': '1',
                               'display_title': 'Pant og tilbageholdsret 3.1 - Stiftelse'
                           },


                           'Pant og tilbageholdsret 3.2': {
                               'name': 'Panteforholdets forløb',
                               'id': 'Pant og tilbageholdsret 3.2',
                               'key': '2',
                               'display_title': 'Pant og tilbageholdsret 3.2 - Panteforholdets forløb'
                           },


                           'Pant og tilbageholdsret 3.3': {
                               'name': 'Ophør',
                               'id': 'Pant og tilbageholdsret 3.3',
                               'key': '3',
                               'display_title': 'Pant og tilbageholdsret 3.3 - Ophør'
                           },


                           'Pant og tilbageholdsret 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Pant og tilbageholdsret 3.9',
                               'key': '9',
                               'display_title': 'Pant og tilbageholdsret 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Håndpant i løsøre',
                      'id': 'Pant og tilbageholdsret 3',
                      'key': '3',
                      'display_title': 'Pant og tilbageholdsret 3 - Håndpant i løsøre'
                  },


                  'Pant og tilbageholdsret 4': {
                      'subs': {

                           'Pant og tilbageholdsret 4.1': {
                               'name': 'Stiftelse',
                               'id': 'Pant og tilbageholdsret 4.1',
                               'key': '1',
                               'display_title': 'Pant og tilbageholdsret 4.1 - Stiftelse'
                           },


                           'Pant og tilbageholdsret 4.2': {
                               'name': 'Panteforholdets forløb',
                               'id': 'Pant og tilbageholdsret 4.2',
                               'key': '2',
                               'display_title': 'Pant og tilbageholdsret 4.2 - Panteforholdets forløb'
                           },


                           'Pant og tilbageholdsret 4.3': {
                               'name': 'Ophør',
                               'id': 'Pant og tilbageholdsret 4.3',
                               'key': '3',
                               'display_title': 'Pant og tilbageholdsret 4.3 - Ophør'
                           },


                           'Pant og tilbageholdsret 4.4': {
                               'name': 'Særligt om ejerpantebreve',
                               'id': 'Pant og tilbageholdsret 4.4',
                               'key': '4',
                               'display_title': 'Pant og tilbageholdsret 4.4 - Særligt om ejerpantebreve'
                           },


                           'Pant og tilbageholdsret 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Pant og tilbageholdsret 4.9',
                               'key': '9',
                               'display_title': 'Pant og tilbageholdsret 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Pant i fordringer',
                      'id': 'Pant og tilbageholdsret 4',
                      'key': '4',
                      'display_title': 'Pant og tilbageholdsret 4 - Pant i fordringer'
                  },


                  'Pant og tilbageholdsret 5': {
                      'subs': {

                           'Pant og tilbageholdsret 5.1': {
                               'name': 'Stiftelse',
                               'id': 'Pant og tilbageholdsret 5.1',
                               'key': '1',
                               'display_title': 'Pant og tilbageholdsret 5.1 - Stiftelse'
                           },


                           'Pant og tilbageholdsret 5.2': {
                               'name': 'Panteforholdets forløb',
                               'id': 'Pant og tilbageholdsret 5.2',
                               'key': '2',
                               'display_title': 'Pant og tilbageholdsret 5.2 - Panteforholdets forløb'
                           },


                           'Pant og tilbageholdsret 5.3': {
                               'name': 'Ophør',
                               'id': 'Pant og tilbageholdsret 5.3',
                               'key': '3',
                               'display_title': 'Pant og tilbageholdsret 5.3 - Ophør'
                           },


                           'Pant og tilbageholdsret 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Pant og tilbageholdsret 5.9',
                               'key': '9',
                               'display_title': 'Pant og tilbageholdsret 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Underpant i løsøre',
                      'id': 'Pant og tilbageholdsret 5',
                      'key': '5',
                      'display_title': 'Pant og tilbageholdsret 5 - Underpant i løsøre'
                  },


                  'Pant og tilbageholdsret 6': {
                      'name': 'Pant i skibe og luftfartøjer',
                      'id': 'Pant og tilbageholdsret 6',
                      'key': '6',
                      'display_title': 'Pant og tilbageholdsret 6 - Pant i skibe og luftfartøjer'
                  },


                  'Pant og tilbageholdsret 7': {
                      'name': 'Pant i fondsaktiver',
                      'id': 'Pant og tilbageholdsret 7',
                      'key': '7',
                      'display_title': 'Pant og tilbageholdsret 7 - Pant i fondsaktiver'
                  },


                  'Pant og tilbageholdsret 8': {
                      'name': 'Høstpantebreve',
                      'id': 'Pant og tilbageholdsret 8',
                      'key': '8',
                      'display_title': 'Pant og tilbageholdsret 8 - Høstpantebreve'
                  },


                  'Pant og tilbageholdsret 9': {
                      'name': 'Tilbageholdsret',
                      'id': 'Pant og tilbageholdsret 9',
                      'key': '9',
                      'display_title': 'Pant og tilbageholdsret 9 - Tilbageholdsret'
                  }

             },
             'name': 'Pant og tilbageholdsret (se også Tinglysning, Pengevæsen m.v., Fogedret 4)',
             'id': 'Pant og tilbageholdsret',
             'key': 'pant_og_tilbageholdsret',
             'display_title': 'Pant og tilbageholdsret'
         },


         'Pengevæsen m.v.': {
             'subs': {

                  'Pengevæsen m.v. 1': {
                      'name': 'Børs og børsomsætning',
                      'id': 'Pengevæsen m.v. 1',
                      'key': '1',
                      'display_title': 'Pengevæsen m.v. 1 - Børs og børsomsætning'
                  },


                  'Pengevæsen m.v. 2': {
                      'name': 'Banker og sparekasser',
                      'id': 'Pengevæsen m.v. 2',
                      'key': '2',
                      'display_title': 'Pengevæsen m.v. 2 - Banker og sparekasser'
                  },


                  'Pengevæsen m.v. 3': {
                      'name': 'Realkreditinstitutter',
                      'id': 'Pengevæsen m.v. 3',
                      'key': '3',
                      'display_title': 'Pengevæsen m.v. 3 - Realkreditinstitutter'
                  },


                  'Pengevæsen m.v. 4': {
                      'subs': {

                           'Pengevæsen m.v. 4.1': {
                               'subs': {

                                    'Pengevæsen m.v. 41.1': {
                                        'name': 'Omsætningsgældsbreve',
                                        'id': 'Pengevæsen m.v. 41.1',
                                        'key': '1',
                                        'display_title': 'Pengevæsen m.v. 41.1 - Omsætningsgældsbreve'
                                    },


                                    'Pengevæsen m.v. 41.2': {
                                        'name': 'Simple gældsbreve',
                                        'id': 'Pengevæsen m.v. 41.2',
                                        'key': '2',
                                        'display_title': 'Pengevæsen m.v. 41.2 - Simple gældsbreve'
                                    },


                                    'Pengevæsen m.v. 41.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Pengevæsen m.v. 41.9',
                                        'key': '9',
                                        'display_title': 'Pengevæsen m.v. 41.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Gældsbreve',
                               'id': 'Pengevæsen m.v. 4.1',
                               'key': '1',
                               'display_title': 'Pengevæsen m.v. 4.1 - Gældsbreve'
                           },


                           'Pengevæsen m.v. 4.2': {
                               'name': 'Veksler',
                               'id': 'Pengevæsen m.v. 4.2',
                               'key': '2',
                               'display_title': 'Pengevæsen m.v. 4.2 - Veksler'
                           },


                           'Pengevæsen m.v. 4.3': {
                               'name': 'Checks',
                               'id': 'Pengevæsen m.v. 4.3',
                               'key': '3',
                               'display_title': 'Pengevæsen m.v. 4.3 - Checks'
                           },


                           'Pengevæsen m.v. 4.4': {
                               'name': 'Obligationer',
                               'id': 'Pengevæsen m.v. 4.4',
                               'key': '4',
                               'display_title': 'Pengevæsen m.v. 4.4 - Obligationer'
                           },


                           'Pengevæsen m.v. 4.5': {
                               'name': 'Fondsaktiver i værdipapircentral',
                               'id': 'Pengevæsen m.v. 4.5',
                               'key': '5',
                               'display_title': 'Pengevæsen m.v. 4.5 - Fondsaktiver i værdipapircentral'
                           },


                           'Pengevæsen m.v. 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Pengevæsen m.v. 4.9',
                               'key': '9',
                               'display_title': 'Pengevæsen m.v. 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Veksler, checks og gældsbreve',
                      'id': 'Pengevæsen m.v. 4',
                      'key': '4',
                      'display_title': 'Pengevæsen m.v. 4 - Veksler, checks og gældsbreve'
                  },


                  'Pengevæsen m.v. 5': {
                      'subs': {

                           'Pengevæsen m.v. 5.1': {
                               'name': 'Stiftelse og indhold',
                               'id': 'Pengevæsen m.v. 5.1',
                               'key': '1',
                               'display_title': 'Pengevæsen m.v. 5.1 - Stiftelse og indhold'
                           },


                           'Pengevæsen m.v. 5.2': {
                               'name': 'Indfrielse og overtagelse, se også Pant og tilbageholdsret 21.4',
                               'id': 'Pengevæsen m.v. 5.2',
                               'key': '2',
                               'display_title': 'Pengevæsen m.v. 5.2 - Indfrielse og overtagelse, se også Pant og tilbageholdsret 21.4'
                           },


                           'Pengevæsen m.v. 5.3': {
                               'name': 'Betaling',
                               'id': 'Pengevæsen m.v. 5.3',
                               'key': '3',
                               'display_title': 'Pengevæsen m.v. 5.3 - Betaling'
                           },


                           'Pengevæsen m.v. 5.4': {
                               'name': 'Modregning, se også Konkurs- og anden insolvensret 2.5',
                               'id': 'Pengevæsen m.v. 5.4',
                               'key': '4',
                               'display_title': 'Pengevæsen m.v. 5.4 - Modregning, se også Konkurs- og anden insolvensret 2.5'
                           },


                           'Pengevæsen m.v. 5.5': {
                               'name': 'Tilbagesøgning af ydet betaling',
                               'id': 'Pengevæsen m.v. 5.5',
                               'key': '5',
                               'display_title': 'Pengevæsen m.v. 5.5 - Tilbagesøgning af ydet betaling'
                           },


                           'Pengevæsen m.v. 5.6': {
                               'name': 'Krav om efterbetaling',
                               'id': 'Pengevæsen m.v. 5.6',
                               'key': '6',
                               'display_title': 'Pengevæsen m.v. 5.6 - Krav om efterbetaling'
                           },


                           'Pengevæsen m.v. 5.7': {
                               'name': 'Renter, se også Køb 2.2',
                               'id': 'Pengevæsen m.v. 5.7',
                               'key': '7',
                               'display_title': 'Pengevæsen m.v. 5.7 - Renter, se også Køb 2.2'
                           },


                           'Pengevæsen m.v. 5.8': {
                               'subs': {

                                    'Pengevæsen m.v. 58.1': {
                                        'name': '1908-loven',
                                        'id': 'Pengevæsen m.v. 58.1',
                                        'key': '1',
                                        'display_title': 'Pengevæsen m.v. 58.1 - 1908-loven'
                                    },


                                    'Pengevæsen m.v. 58.2': {
                                        'name': 'Danske Lov',
                                        'id': 'Pengevæsen m.v. 58.2',
                                        'key': '2',
                                        'display_title': 'Pengevæsen m.v. 58.2 - Danske Lov'
                                    },


                                    'Pengevæsen m.v. 58.9': {
                                        'name': 'Andre regler',
                                        'id': 'Pengevæsen m.v. 58.9',
                                        'key': '9',
                                        'display_title': 'Pengevæsen m.v. 58.9 - Andre regler'
                                    }

                               },
                               'name': 'Forældelse',
                               'id': 'Pengevæsen m.v. 5.8',
                               'key': '8',
                               'display_title': 'Pengevæsen m.v. 5.8 - Forældelse'
                           },


                           'Pengevæsen m.v. 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Pengevæsen m.v. 5.9',
                               'key': '9',
                               'display_title': 'Pengevæsen m.v. 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Gæld',
                      'id': 'Pengevæsen m.v. 5',
                      'key': '5',
                      'display_title': 'Pengevæsen m.v. 5 - Gæld'
                  },


                  'Pengevæsen m.v. 6': {
                      'name': 'Betalings- og kreditkort m.v.',
                      'id': 'Pengevæsen m.v. 6',
                      'key': '6',
                      'display_title': 'Pengevæsen m.v. 6 - Betalings- og kreditkort m.v.'
                  },


                  'Pengevæsen m.v. 7': {
                      'name': 'Valutalovgivning',
                      'id': 'Pengevæsen m.v. 7',
                      'key': '7',
                      'display_title': 'Pengevæsen m.v. 7 - Valutalovgivning'
                  },


                  'Pengevæsen m.v. 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Pengevæsen m.v. 9',
                      'key': '9',
                      'display_title': 'Pengevæsen m.v. 9 - Andre spørgsmål'
                  }

             },
             'name': 'Pengevæsen m.v.',
             'id': 'Pengevæsen m.v.',
             'key': 'pengev_sen_m_v_',
             'display_title': 'Pengevæsen m.v.'
         },


         'Personspørgsmål': {
             'subs': {

                  'Personspørgsmål 1': {
                      'name': 'Persondatabeskyttelse',
                      'id': 'Personspørgsmål 1',
                      'key': '1',
                      'display_title': 'Personspørgsmål 1 - Persondatabeskyttelse'
                  },


                  'Personspørgsmål 2': {
                      'name': 'Navn',
                      'id': 'Personspørgsmål 2',
                      'key': '2',
                      'display_title': 'Personspørgsmål 2 - Navn'
                  },


                  'Personspørgsmål 3': {
                      'name': 'Ligestilling og ligebehandling m.v.',
                      'id': 'Personspørgsmål 3',
                      'key': '3',
                      'display_title': 'Personspørgsmål 3 - Ligestilling og ligebehandling m.v.'
                  },


                  'Personspørgsmål 4': {
                      'name': 'Personlig retsbeskyttelse',
                      'id': 'Personspørgsmål 4',
                      'key': '4',
                      'display_title': 'Personspørgsmål 4 - Personlig retsbeskyttelse'
                  },


                  'Personspørgsmål 5': {
                      'subs': {

                           'Personspørgsmål 5.1': {
                               'subs': {

                                    'Personspørgsmål 51.1': {
                                        'name': 'Værgemål',
                                        'id': 'Personspørgsmål 51.1',
                                        'key': '1',
                                        'display_title': 'Personspørgsmål 51.1 - Værgemål'
                                    },


                                    'Personspørgsmål 51.2': {
                                        'name': 'Handleevnefratagelse, se også Aftaler 23.1 og 23.2 og Familieret 3.5',
                                        'id': 'Personspørgsmål 51.2',
                                        'key': '2',
                                        'display_title': 'Personspørgsmål 51.2 - Handleevnefratagelse, se også Aftaler 23.1 og 23.2 og Familieret 3.5'
                                    },


                                    'Personspørgsmål 51.3': {
                                        'name': 'Samværgemål',
                                        'id': 'Personspørgsmål 51.3',
                                        'key': '3',
                                        'display_title': 'Personspørgsmål 51.3 - Samværgemål'
                                    },


                                    'Personspørgsmål 51.4': {
                                        'name': 'Ændring',
                                        'id': 'Personspørgsmål 51.4',
                                        'key': '4',
                                        'display_title': 'Personspørgsmål 51.4 - Ændring'
                                    },


                                    'Personspørgsmål 51.5': {
                                        'name': 'Ophævelse',
                                        'id': 'Personspørgsmål 51.5',
                                        'key': '5',
                                        'display_title': 'Personspørgsmål 51.5 - Ophævelse'
                                    },


                                    'Personspørgsmål 51.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Personspørgsmål 51.9',
                                        'key': '9',
                                        'display_title': 'Personspørgsmål 51.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Værgemålsbetingelser',
                               'id': 'Personspørgsmål 5.1',
                               'key': '1',
                               'display_title': 'Personspørgsmål 5.1 - Værgemålsbetingelser'
                           },


                           'Personspørgsmål 5.2': {
                               'subs': {

                                    'Personspørgsmål 52.1': {
                                        'name': 'Værgemålssager',
                                        'id': 'Personspørgsmål 52.1',
                                        'key': '1',
                                        'display_title': 'Personspørgsmål 52.1 - Værgemålssager'
                                    },


                                    'Personspørgsmål 52.2': {
                                        'name': 'Prøvelse af statsamtets afgørelse',
                                        'id': 'Personspørgsmål 52.2',
                                        'key': '2',
                                        'display_title': 'Personspørgsmål 52.2 - Prøvelse af statsamtets afgørelse'
                                    },


                                    'Personspørgsmål 52.3': {
                                        'subs': {

                                             'Personspørgsmål 523.1': {
                                                 'name': 'Beskikkelse og fratagelse',
                                                 'id': 'Personspørgsmål 523.1',
                                                 'key': '1',
                                                 'display_title': 'Personspørgsmål 523.1 - Beskikkelse og fratagelse'
                                             },


                                             'Personspørgsmål 523.2': {
                                                 'name': 'Faste værger',
                                                 'id': 'Personspørgsmål 523.2',
                                                 'key': '2',
                                                 'display_title': 'Personspørgsmål 523.2 - Faste værger'
                                             },


                                             'Personspørgsmål 523.3': {
                                                 'name': 'Honorar',
                                                 'id': 'Personspørgsmål 523.3',
                                                 'key': '3',
                                                 'display_title': 'Personspørgsmål 523.3 - Honorar'
                                             },


                                             'Personspørgsmål 523.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Personspørgsmål 523.9',
                                                 'key': '9',
                                                 'display_title': 'Personspørgsmål 523.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Værger',
                                        'id': 'Personspørgsmål 52.3',
                                        'key': '3',
                                        'display_title': 'Personspørgsmål 52.3 - Værger'
                                    },


                                    'Personspørgsmål 52.4': {
                                        'name': 'Midlertidige værgemål',
                                        'id': 'Personspørgsmål 52.4',
                                        'key': '4',
                                        'display_title': 'Personspørgsmål 52.4 - Midlertidige værgemål'
                                    },


                                    'Personspørgsmål 52.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Personspørgsmål 52.9',
                                        'key': '9',
                                        'display_title': 'Personspørgsmål 52.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Sagsbehandlingsspørgsmål',
                               'id': 'Personspørgsmål 5.2',
                               'key': '2',
                               'display_title': 'Personspørgsmål 5.2 - Sagsbehandlingsspørgsmål'
                           },


                           'Personspørgsmål 5.3': {
                               'subs': {

                                    'Personspørgsmål 53.1': {
                                        'name': 'Anbringelse',
                                        'id': 'Personspørgsmål 53.1',
                                        'key': '1',
                                        'display_title': 'Personspørgsmål 53.1 - Anbringelse'
                                    },


                                    'Personspørgsmål 53.2': {
                                        'name': 'Bestyrelse',
                                        'id': 'Personspørgsmål 53.2',
                                        'key': '2',
                                        'display_title': 'Personspørgsmål 53.2 - Bestyrelse'
                                    },


                                    'Personspørgsmål 53.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Personspørgsmål 53.9',
                                        'key': '9',
                                        'display_title': 'Personspørgsmål 53.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Anbringelse og bestyrelse',
                               'id': 'Personspørgsmål 5.3',
                               'key': '3',
                               'display_title': 'Personspørgsmål 5.3 - Anbringelse og bestyrelse'
                           },


                           'Personspørgsmål 5.4': {
                               'subs': {

                                    'Personspørgsmål 54.1': {
                                        'name': 'Godkendelse',
                                        'id': 'Personspørgsmål 54.1',
                                        'key': '1',
                                        'display_title': 'Personspørgsmål 54.1 - Godkendelse'
                                    },


                                    'Personspørgsmål 54.2': {
                                        'name': 'Formueforbrug',
                                        'id': 'Personspørgsmål 54.2',
                                        'key': '2',
                                        'display_title': 'Personspørgsmål 54.2 - Formueforbrug'
                                    },


                                    'Personspørgsmål 54.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Personspørgsmål 54.9',
                                        'key': '9',
                                        'display_title': 'Personspørgsmål 54.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Værgemålstilsyn',
                               'id': 'Personspørgsmål 5.4',
                               'key': '4',
                               'display_title': 'Personspørgsmål 5.4 - Værgemålstilsyn'
                           },


                           'Personspørgsmål 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Personspørgsmål 5.9',
                               'key': '9',
                               'display_title': 'Personspørgsmål 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Værgemål',
                      'id': 'Personspørgsmål 5',
                      'key': '5',
                      'display_title': 'Personspørgsmål 5 - Værgemål'
                  },


                  'Personspørgsmål 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Personspørgsmål 9',
                      'key': '9',
                      'display_title': 'Personspørgsmål 9 - Andre spørgsmål'
                  }

             },
             'name': 'Personspørgsmål',
             'id': 'Personspørgsmål',
             'key': 'personsp_rgsm_l',
             'display_title': 'Personspørgsmål'
         },


         'Presse og radio': {
             'subs': {

                  'Presse og radio 1': {
                      'name': 'Pressen',
                      'id': 'Presse og radio 1',
                      'key': '1',
                      'display_title': 'Presse og radio 1 - Pressen'
                  },


                  'Presse og radio 2': {
                      'name': 'Radio og fjernsyn',
                      'id': 'Presse og radio 2',
                      'key': '2',
                      'display_title': 'Presse og radio 2 - Radio og fjernsyn'
                  }

             },
             'name': 'Presse og radio (Ophavsret, se Immaterialret 1)',
             'id': 'Presse og radio',
             'key': 'presse_og_radio',
             'display_title': 'Presse og radio'
         },


         'Retspleje': {
             'subs': {

                  'Retspleje 1': {
                      'subs': {

                           'Retspleje 1.1': {
                               'subs': {

                                    'Retspleje 11.1': {
                                        'name': 'Retsmøder',
                                        'id': 'Retspleje 11.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 11.1 - Retsmøder'
                                    },


                                    'Retspleje 11.2': {
                                        'name': 'Retsbøger',
                                        'id': 'Retspleje 11.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 11.2 - Retsbøger'
                                    },


                                    'Retspleje 11.3': {
                                        'name': 'Dommere, lægdommere',
                                        'id': 'Retspleje 11.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 11.3 - Dommere, lægdommere'
                                    },


                                    'Retspleje 11.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 11.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 11.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Domstolenes ordning',
                               'id': 'Retspleje 1.1',
                               'key': '1',
                               'display_title': 'Retspleje 1.1 - Domstolenes ordning'
                           },


                           'Retspleje 1.2': {
                               'subs': {

                                    'Retspleje 12.1': {
                                        'name': 'Beskikkelse',
                                        'id': 'Retspleje 12.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 12.1 - Beskikkelse'
                                    },


                                    'Retspleje 12.2': {
                                        'name': 'Møderet',
                                        'id': 'Retspleje 12.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 12.2 - Møderet'
                                    },


                                    'Retspleje 12.3': {
                                        'name': 'Erstatningsansvar',
                                        'id': 'Retspleje 12.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 12.3 - Erstatningsansvar'
                                    },


                                    'Retspleje 12.4': {
                                        'name': 'Salær, se også Retspleje 332.3',
                                        'id': 'Retspleje 12.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 12.4 - Salær, se også Retspleje 332.3'
                                    },


                                    'Retspleje 12.5': {
                                        'name': 'Disciplinærforfølgning og ophør af retten til at udøve advokatvirksomhed',
                                        'id': 'Retspleje 12.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 12.5 - Disciplinærforfølgning og ophør af retten til at udøve advokatvirksomhed'
                                    },


                                    'Retspleje 12.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 12.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 12.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Advokater',
                               'id': 'Retspleje 1.2',
                               'key': '2',
                               'display_title': 'Retspleje 1.2 - Advokater'
                           },


                           'Retspleje 1.3': {
                               'subs': {

                                    'Retspleje 13.1': {
                                        'name': 'Retsledelse',
                                        'id': 'Retspleje 13.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 13.1 - Retsledelse'
                                    },


                                    'Retspleje 13.2': {
                                        'name': 'Retsafgørelser',
                                        'id': 'Retspleje 13.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 13.2 - Retsafgørelser'
                                    },


                                    'Retspleje 13.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 13.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 13.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Procesmåden',
                               'id': 'Retspleje 1.3',
                               'key': '3',
                               'display_title': 'Retspleje 1.3 - Procesmåden'
                           },


                           'Retspleje 1.4': {
                               'subs': {

                                    'Retspleje 14.1': {
                                        'name': 'Vidner',
                                        'id': 'Retspleje 14.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 14.1 - Vidner'
                                    },


                                    'Retspleje 14.2': {
                                        'name': 'Syn og skøn',
                                        'id': 'Retspleje 14.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 14.2 - Syn og skøn'
                                    },


                                    'Retspleje 14.3': {
                                        'name': 'Sagkyndige erklæringer',
                                        'id': 'Retspleje 14.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 14.3 - Sagkyndige erklæringer'
                                    },


                                    'Retspleje 14.4': {
                                        'name': 'Søforklaringer',
                                        'id': 'Retspleje 14.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 14.4 - Søforklaringer'
                                    },


                                    'Retspleje 14.5': {
                                        'name': 'Andre bevismidler',
                                        'id': 'Retspleje 14.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 14.5 - Andre bevismidler'
                                    },


                                    'Retspleje 14.6': {
                                        'name': 'Bevisbyrde',
                                        'id': 'Retspleje 14.6',
                                        'key': '6',
                                        'display_title': 'Retspleje 14.6 - Bevisbyrde'
                                    },


                                    'Retspleje 14.7': {
                                        'name': 'Bevisvurderingen',
                                        'id': 'Retspleje 14.7',
                                        'key': '7',
                                        'display_title': 'Retspleje 14.7 - Bevisvurderingen'
                                    },


                                    'Retspleje 14.8': {
                                        'subs': {

                                             'Retspleje 148.1': {
                                                 'name': 'Bevisumiddelbarhed',
                                                 'id': 'Retspleje 148.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 148.1 - Bevisumiddelbarhed'
                                             },


                                             'Retspleje 148.2': {
                                                 'name': 'Isoleret bevisoptagelse',
                                                 'id': 'Retspleje 148.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 148.2 - Isoleret bevisoptagelse'
                                             },


                                             'Retspleje 148.3': {
                                                 'name': 'Politirapporter',
                                                 'id': 'Retspleje 148.3',
                                                 'key': '3',
                                                 'display_title': 'Retspleje 148.3 - Politirapporter'
                                             },


                                             'Retspleje 148.9': {
                                                 'name': 'Andre Spørgsmål',
                                                 'id': 'Retspleje 148.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 148.9 - Andre Spørgsmål'
                                             }

                                        },
                                        'name': 'Bevisførelse',
                                        'id': 'Retspleje 14.8',
                                        'key': '8',
                                        'display_title': 'Retspleje 14.8 - Bevisførelse'
                                    },


                                    'Retspleje 14.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 14.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 14.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Bevis',
                               'id': 'Retspleje 1.4',
                               'key': '4',
                               'display_title': 'Retspleje 1.4 - Bevis'
                           },


                           'Retspleje 1.5': {
                               'name': 'Forkyndelse',
                               'id': 'Retspleje 1.5',
                               'key': '5',
                               'display_title': 'Retspleje 1.5 - Forkyndelse'
                           },


                           'Retspleje 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Retspleje 1.9',
                               'key': '9',
                               'display_title': 'Retspleje 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Generelle emner',
                      'id': 'Retspleje 1',
                      'key': '1',
                      'display_title': 'Retspleje 1 - Generelle emner'
                  },


                  'Retspleje 2': {
                      'subs': {

                           'Retspleje 2.1': {
                               'subs': {

                                    'Retspleje 21.1': {
                                        'name': 'Saglig kompetence',
                                        'id': 'Retspleje 21.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 21.1 - Saglig kompetence'
                                    },


                                    'Retspleje 21.2': {
                                        'name': 'Sø- og handelssager',
                                        'id': 'Retspleje 21.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 21.2 - Sø- og handelssager'
                                    },


                                    'Retspleje 21.3': {
                                        'name': 'Stedlig kompetence (værneting)',
                                        'id': 'Retspleje 21.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 21.3 - Stedlig kompetence (værneting)'
                                    },


                                    'Retspleje 21.4': {
                                        'name': 'International kompetence',
                                        'id': 'Retspleje 21.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 21.4 - International kompetence'
                                    },


                                    'Retspleje 21.5': {
                                        'name': 'Sammenlægning og adskillelse af krav',
                                        'id': 'Retspleje 21.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 21.5 - Sammenlægning og adskillelse af krav'
                                    },


                                    'Retspleje 21.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 21.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 21.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Retternes virkekreds',
                               'id': 'Retspleje 2.1',
                               'key': '1',
                               'display_title': 'Retspleje 2.1 - Retternes virkekreds'
                           },


                           'Retspleje 2.2': {
                               'subs': {

                                    'Retspleje 22.1': {
                                        'name': 'Habilitet',
                                        'id': 'Retspleje 22.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 22.1 - Habilitet'
                                    },


                                    'Retspleje 22.2': {
                                        'name': 'Søgsmålskompetence',
                                        'id': 'Retspleje 22.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 22.2 - Søgsmålskompetence'
                                    },


                                    'Retspleje 22.3': {
                                        'name': 'Retshjælp og retshjælpsforsikring',
                                        'id': 'Retspleje 22.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 22.3 - Retshjælp og retshjælpsforsikring'
                                    },


                                    'Retspleje 22.4': {
                                        'name': 'Fri proces',
                                        'id': 'Retspleje 22.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 22.4 - Fri proces'
                                    },


                                    'Retspleje 22.5': {
                                        'name': 'Rettergangsfuldmagt',
                                        'id': 'Retspleje 22.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 22.5 - Rettergangsfuldmagt'
                                    },


                                    'Retspleje 22.6': {
                                        'name': 'Nødvendigt procesfællesskab',
                                        'id': 'Retspleje 22.6',
                                        'key': '6',
                                        'display_title': 'Retspleje 22.6 - Nødvendigt procesfællesskab'
                                    },


                                    'Retspleje 22.7': {
                                        'name': 'Succession',
                                        'id': 'Retspleje 22.7',
                                        'key': '7',
                                        'display_title': 'Retspleje 22.7 - Succession'
                                    },


                                    'Retspleje 22.8': {
                                        'name': 'Sagsomkostninger',
                                        'id': 'Retspleje 22.8',
                                        'key': '8',
                                        'display_title': 'Retspleje 22.8 - Sagsomkostninger'
                                    },


                                    'Retspleje 22.9': {
                                        'name': 'Rettergangsbøder',
                                        'id': 'Retspleje 22.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 22.9 - Rettergangsbøder'
                                    }

                               },
                               'name': 'Parterne',
                               'id': 'Retspleje 2.2',
                               'key': '2',
                               'display_title': 'Retspleje 2.2 - Parterne'
                           },


                           'Retspleje 2.3': {
                               'subs': {

                                    'Retspleje 23.1': {
                                        'name': 'Fuldbyrdelsessøgsmål',
                                        'id': 'Retspleje 23.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 23.1 - Fuldbyrdelsessøgsmål'
                                    },


                                    'Retspleje 23.2': {
                                        'name': 'Anerkendelsessøgsmål',
                                        'id': 'Retspleje 23.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 23.2 - Anerkendelsessøgsmål'
                                    },


                                    'Retspleje 23.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 23.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 23.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Proceskravet',
                               'id': 'Retspleje 2.3',
                               'key': '3',
                               'display_title': 'Retspleje 2.3 - Proceskravet'
                           },


                           'Retspleje 2.4': {
                               'subs': {

                                    'Retspleje 24.1': {
                                        'name': 'Bevisførelse, se også Retspleje 1.4',
                                        'id': 'Retspleje 24.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 24.1 - Bevisførelse, se også Retspleje 1.4'
                                    },


                                    'Retspleje 24.2': {
                                        'name': 'Udsættelse',
                                        'id': 'Retspleje 24.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 24.2 - Udsættelse'
                                    },


                                    'Retspleje 24.3': {
                                        'name': 'Forlig',
                                        'id': 'Retspleje 24.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 24.3 - Forlig'
                                    },


                                    'Retspleje 24.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 24.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 24.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Sagsbehandlingen',
                               'id': 'Retspleje 2.4',
                               'key': '4',
                               'display_title': 'Retspleje 2.4 - Sagsbehandlingen'
                           },


                           'Retspleje 2.5': {
                               'subs': {

                                    'Retspleje 25.1': {
                                        'name': 'Sagens anlæg',
                                        'id': 'Retspleje 25.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 25.1 - Sagens anlæg'
                                    },


                                    'Retspleje 25.2': {
                                        'name': 'Procestilvarsling',
                                        'id': 'Retspleje 25.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 25.2 - Procestilvarsling'
                                    },


                                    'Retspleje 25.3': {
                                        'name': 'Nye påstande, anbringender og beviser',
                                        'id': 'Retspleje 25.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 25.3 - Nye påstande, anbringender og beviser'
                                    },


                                    'Retspleje 25.4': {
                                        'name': 'Udeblivelse',
                                        'id': 'Retspleje 25.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 25.4 - Udeblivelse'
                                    },


                                    'Retspleje 25.5': {
                                        'name': 'Sagsbehandling',
                                        'id': 'Retspleje 25.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 25.5 - Sagsbehandling'
                                    },


                                    'Retspleje 25.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 25.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 25.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Førsteinstanssager',
                               'id': 'Retspleje 2.5',
                               'key': '5',
                               'display_title': 'Retspleje 2.5 - Førsteinstanssager'
                           },


                           'Retspleje 2.6': {
                               'subs': {

                                    'Retspleje 26.1': {
                                        'subs': {

                                             'Retspleje 261.1': {
                                                 'name': 'Ankefristen',
                                                 'id': 'Retspleje 261.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 261.1 - Ankefristen'
                                             },


                                             'Retspleje 261.2': {
                                                 'name': 'Ankeafkald',
                                                 'id': 'Retspleje 261.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 261.2 - Ankeafkald'
                                             },


                                             'Retspleje 261.3': {
                                                 'name': 'Nye påstande, anbringender og beviser',
                                                 'id': 'Retspleje 261.3',
                                                 'key': '3',
                                                 'display_title': 'Retspleje 261.3 - Nye påstande, anbringender og beviser'
                                             },


                                             'Retspleje 261.4': {
                                                 'name': 'Udeblivelse',
                                                 'id': 'Retspleje 261.4',
                                                 'key': '4',
                                                 'display_title': 'Retspleje 261.4 - Udeblivelse'
                                             },


                                             'Retspleje 261.5': {
                                                 'name': 'Sagsbehandling',
                                                 'id': 'Retspleje 261.5',
                                                 'key': '5',
                                                 'display_title': 'Retspleje 261.5 - Sagsbehandling'
                                             },


                                             'Retspleje 261.6': {
                                                 'name': 'Særlig om Højesteret',
                                                 'id': 'Retspleje 261.6',
                                                 'key': '6',
                                                 'display_title': 'Retspleje 261.6 - Særlig om Højesteret'
                                             },


                                             'Retspleje 261.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Retspleje 261.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 261.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Anke',
                                        'id': 'Retspleje 26.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 26.1 - Anke'
                                    },


                                    'Retspleje 26.2': {
                                        'name': 'Kære, se også Fogedret 7, Tinglysning 6',
                                        'id': 'Retspleje 26.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 26.2 - Kære, se også Fogedret 7, Tinglysning 6'
                                    },


                                    'Retspleje 26.3': {
                                        'name': 'Genoptagelse',
                                        'id': 'Retspleje 26.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 26.3 - Genoptagelse'
                                    },


                                    'Retspleje 26.4': {
                                        'name': 'Oprejsningsbevilling og andre anketilladelser',
                                        'id': 'Retspleje 26.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 26.4 - Oprejsningsbevilling og andre anketilladelser'
                                    },


                                    'Retspleje 26.5': {
                                        'name': 'Tredjeinstansbevilling',
                                        'id': 'Retspleje 26.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 26.5 - Tredjeinstansbevilling'
                                    },


                                    'Retspleje 26.6': {
                                        'subs': {

                                             'Retspleje 266.1': {
                                                 'name': 'Positiv retskraft',
                                                 'id': 'Retspleje 266.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 266.1 - Positiv retskraft'
                                             },


                                             'Retspleje 266.2': {
                                                 'name': 'Negativ retskraft',
                                                 'id': 'Retspleje 266.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 266.2 - Negativ retskraft'
                                             },


                                             'Retspleje 266.3': {
                                                 'name': 'Udenlandske afgørelser',
                                                 'id': 'Retspleje 266.3',
                                                 'key': '3',
                                                 'display_title': 'Retspleje 266.3 - Udenlandske afgørelser'
                                             },


                                             'Retspleje 266.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Retspleje 266.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 266.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Retskraft',
                                        'id': 'Retspleje 26.6',
                                        'key': '6',
                                        'display_title': 'Retspleje 26.6 - Retskraft'
                                    },


                                    'Retspleje 26.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 26.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 26.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Retsmidler',
                               'id': 'Retspleje 2.6',
                               'key': '6',
                               'display_title': 'Retspleje 2.6 - Retsmidler'
                           },


                           'Retspleje 2.7': {
                               'subs': {

                                    'Retspleje 27.1': {
                                        'subs': {

                                             'Retspleje 271.1': {
                                                 'name': 'Kompetence',
                                                 'id': 'Retspleje 271.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 271.1 - Kompetence'
                                             },


                                             'Retspleje 271.2': {
                                                 'name': 'Advokatbistand',
                                                 'id': 'Retspleje 271.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 271.2 - Advokatbistand'
                                             },


                                             'Retspleje 271.3': {
                                                 'name': 'Sagsbehandlingen',
                                                 'id': 'Retspleje 271.3',
                                                 'key': '3',
                                                 'display_title': 'Retspleje 271.3 - Sagsbehandlingen'
                                             },


                                             'Retspleje 271.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Retspleje 271.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 271.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Ægteskabs- og forældremyndighedssager',
                                        'id': 'Retspleje 27.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 27.1 - Ægteskabs- og forældremyndighedssager'
                                    },


                                    'Retspleje 27.2': {
                                        'name': 'Faderskabssager',
                                        'id': 'Retspleje 27.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 27.2 - Faderskabssager'
                                    },


                                    'Retspleje 27.3': {
                                        'name': 'Værgemålssager',
                                        'id': 'Retspleje 27.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 27.3 - Værgemålssager'
                                    },


                                    'Retspleje 27.4': {
                                        'name': 'Prøvelse af administrativt bestemt frihedsberøvelse',
                                        'id': 'Retspleje 27.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 27.4 - Prøvelse af administrativt bestemt frihedsberøvelse'
                                    },


                                    'Retspleje 27.5': {
                                        'name': 'Prøvelse af beslutning om adoption uden samtykke',
                                        'id': 'Retspleje 27.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 27.5 - Prøvelse af beslutning om adoption uden samtykke'
                                    },


                                    'Retspleje 27.6': {
                                        'name': 'Mortifikations- og ejendomsdom',
                                        'id': 'Retspleje 27.6',
                                        'key': '6',
                                        'display_title': 'Retspleje 27.6 - Mortifikations- og ejendomsdom'
                                    },


                                    'Retspleje 27.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 27.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 27.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Særlige retssager',
                               'id': 'Retspleje 2.7',
                               'key': '7',
                               'display_title': 'Retspleje 2.7 - Særlige retssager'
                           },


                           'Retspleje 2.8': {
                               'subs': {

                                    'Retspleje 28.1': {
                                        'name': 'Voldgiftsaftalen',
                                        'id': 'Retspleje 28.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 28.1 - Voldgiftsaftalen'
                                    },


                                    'Retspleje 28.2': {
                                        'name': 'Saglig kompetence',
                                        'id': 'Retspleje 28.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 28.2 - Saglig kompetence'
                                    },


                                    'Retspleje 28.3': {
                                        'name': 'Habilitetsspørgsmål',
                                        'id': 'Retspleje 28.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 28.3 - Habilitetsspørgsmål'
                                    },


                                    'Retspleje 28.4': {
                                        'name': 'Sagsbehandlingen',
                                        'id': 'Retspleje 28.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 28.4 - Sagsbehandlingen'
                                    },


                                    'Retspleje 28.5': {
                                        'name': 'Voldgiftskendelsers gyldighed',
                                        'id': 'Retspleje 28.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 28.5 - Voldgiftskendelsers gyldighed'
                                    },


                                    'Retspleje 28.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 28.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 28.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Voldgift, se også Entrepriseret 25.2',
                               'id': 'Retspleje 2.8',
                               'key': '8',
                               'display_title': 'Retspleje 2.8 - Voldgift, se også Entrepriseret 25.2'
                           },


                           'Retspleje 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Retspleje 2.9',
                               'key': '9',
                               'display_title': 'Retspleje 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Retspleje i borgerlige sager',
                      'id': 'Retspleje 2',
                      'key': '2',
                      'display_title': 'Retspleje 2 - Retspleje i borgerlige sager'
                  },


                  'Retspleje 3': {
                      'subs': {

                           'Retspleje 3.1': {
                               'subs': {

                                    'Retspleje 31.1': {
                                        'name': 'Saglig kompetence',
                                        'id': 'Retspleje 31.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 31.1 - Saglig kompetence'
                                    },


                                    'Retspleje 31.2': {
                                        'name': 'Stedlig kompetence (værneting)',
                                        'id': 'Retspleje 31.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 31.2 - Stedlig kompetence (værneting)'
                                    },


                                    'Retspleje 31.3': {
                                        'name': 'International kompetence',
                                        'id': 'Retspleje 31.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 31.3 - International kompetence'
                                    },


                                    'Retspleje 31.4': {
                                        'name': 'Kumulation',
                                        'id': 'Retspleje 31.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 31.4 - Kumulation'
                                    },


                                    'Retspleje 31.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 31.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 31.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Retternes virkekreds',
                               'id': 'Retspleje 3.1',
                               'key': '1',
                               'display_title': 'Retspleje 3.1 - Retternes virkekreds'
                           },


                           'Retspleje 3.2': {
                               'subs': {

                                    'Retspleje 32.1': {
                                        'subs': {

                                             'Retspleje 321.1': {
                                                 'name': 'Statsadvokatsager',
                                                 'id': 'Retspleje 321.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 321.1 - Statsadvokatsager'
                                             },


                                             'Retspleje 321.2': {
                                                 'name': 'Politisager',
                                                 'id': 'Retspleje 321.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 321.2 - Politisager'
                                             },


                                             'Retspleje 321.3': {
                                                 'name': 'Omgørelse',
                                                 'id': 'Retspleje 321.3',
                                                 'key': '3',
                                                 'display_title': 'Retspleje 321.3 - Omgørelse'
                                             },


                                             'Retspleje 321.4': {
                                                 'name': 'Betinget offentlig påtale',
                                                 'id': 'Retspleje 321.4',
                                                 'key': '4',
                                                 'display_title': 'Retspleje 321.4 - Betinget offentlig påtale'
                                             },


                                             'Retspleje 321.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Retspleje 321.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 321.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Anklagemyndigheden',
                                        'id': 'Retspleje 32.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 32.1 - Anklagemyndigheden'
                                    },


                                    'Retspleje 32.2': {
                                        'subs': {

                                             'Retspleje 322.1': {
                                                 'name': 'Påtaleopgivelse',
                                                 'id': 'Retspleje 322.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 322.1 - Påtaleopgivelse'
                                             },


                                             'Retspleje 322.2': {
                                                 'name': 'Påtalefrafald',
                                                 'id': 'Retspleje 322.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 322.2 - Påtalefrafald'
                                             },


                                             'Retspleje 322.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Retspleje 322.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 322.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Påtaleundladelse',
                                        'id': 'Retspleje 32.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 32.2 - Påtaleundladelse'
                                    },


                                    'Retspleje 32.3': {
                                        'name': 'Forholdet mellem påtale og dom',
                                        'id': 'Retspleje 32.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 32.3 - Forholdet mellem påtale og dom'
                                    },


                                    'Retspleje 32.4': {
                                        'name': 'Privat påtale',
                                        'id': 'Retspleje 32.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 32.4 - Privat påtale'
                                    },


                                    'Retspleje 32.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 32.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 32.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Påtalen',
                               'id': 'Retspleje 3.2',
                               'key': '2',
                               'display_title': 'Retspleje 3.2 - Påtalen'
                           },


                           'Retspleje 3.3': {
                               'subs': {

                                    'Retspleje 33.1': {
                                        'name': 'Sigtede',
                                        'id': 'Retspleje 33.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 33.1 - Sigtede'
                                    },


                                    'Retspleje 33.2': {
                                        'subs': {

                                             'Retspleje 332.1': {
                                                 'name': 'Forsvarerbeskikkelse',
                                                 'id': 'Retspleje 332.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 332.1 - Forsvarerbeskikkelse'
                                             },


                                             'Retspleje 332.2': {
                                                 'name': 'Beføjelser',
                                                 'id': 'Retspleje 332.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 332.2 - Beføjelser'
                                             },


                                             'Retspleje 332.3': {
                                                 'name': 'Salær',
                                                 'id': 'Retspleje 332.3',
                                                 'key': '3',
                                                 'display_title': 'Retspleje 332.3 - Salær'
                                             },


                                             'Retspleje 332.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Retspleje 332.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 332.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Forsvaret',
                                        'id': 'Retspleje 33.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 33.2 - Forsvaret'
                                    },


                                    'Retspleje 33.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 33.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 33.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Sigtede og forsvaret',
                               'id': 'Retspleje 3.3',
                               'key': '3',
                               'display_title': 'Retspleje 3.3 - Sigtede og forsvaret'
                           },


                           'Retspleje 3.4': {
                               'name': 'Adhæsionsproces, forurettede',
                               'id': 'Retspleje 3.4',
                               'key': '4',
                               'display_title': 'Retspleje 3.4 - Adhæsionsproces, forurettede'
                           },


                           'Retspleje 3.5': {
                               'subs': {

                                    'Retspleje 35.1': {
                                        'name': 'Afhøringer',
                                        'id': 'Retspleje 35.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 35.1 - Afhøringer'
                                    },


                                    'Retspleje 35.2': {
                                        'name': 'Anholdelse',
                                        'id': 'Retspleje 35.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 35.2 - Anholdelse'
                                    },


                                    'Retspleje 35.3': {
                                        'name': 'Varetægtsfængsling',
                                        'id': 'Retspleje 35.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 35.3 - Varetægtsfængsling'
                                    },


                                    'Retspleje 35.4': {
                                        'name': 'Besigtigelse',
                                        'id': 'Retspleje 35.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 35.4 - Besigtigelse'
                                    },


                                    'Retspleje 35.5': {
                                        'name': 'Beslaglæggelse',
                                        'id': 'Retspleje 35.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 35.5 - Beslaglæggelse'
                                    },


                                    'Retspleje 35.6': {
                                        'name': 'Ransagning',
                                        'id': 'Retspleje 35.6',
                                        'key': '6',
                                        'display_title': 'Retspleje 35.6 - Ransagning'
                                    },


                                    'Retspleje 35.7': {
                                        'name': 'Indgreb i meddelelseshemmeligheden',
                                        'id': 'Retspleje 35.7',
                                        'key': '7',
                                        'display_title': 'Retspleje 35.7 - Indgreb i meddelelseshemmeligheden'
                                    },


                                    'Retspleje 35.8': {
                                        'name': 'Personundersøgelse',
                                        'id': 'Retspleje 35.8',
                                        'key': '8',
                                        'display_title': 'Retspleje 35.8 - Personundersøgelse'
                                    },


                                    'Retspleje 35.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 35.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 35.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Efterforskning',
                               'id': 'Retspleje 3.5',
                               'key': '5',
                               'display_title': 'Retspleje 3.5 - Efterforskning'
                           },


                           'Retspleje 3.6': {
                               'subs': {

                                    'Retspleje 36.1': {
                                        'subs': {

                                             'Retspleje 361.1': {
                                                 'name': 'Bevisumiddelbarhed',
                                                 'id': 'Retspleje 361.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 361.1 - Bevisumiddelbarhed'
                                             },


                                             'Retspleje 361.2': {
                                                 'name': 'Bevismidler',
                                                 'id': 'Retspleje 361.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 361.2 - Bevismidler'
                                             },


                                             'Retspleje 361.3': {
                                                 'name': 'Politirapporter',
                                                 'id': 'Retspleje 361.3',
                                                 'key': '3',
                                                 'display_title': 'Retspleje 361.3 - Politirapporter'
                                             },


                                             'Retspleje 361.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Retspleje 361.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 361.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Sagsoplysning',
                                        'id': 'Retspleje 36.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 36.1 - Sagsoplysning'
                                    },


                                    'Retspleje 36.2': {
                                        'name': 'Sagsomkostninger',
                                        'id': 'Retspleje 36.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 36.2 - Sagsomkostninger'
                                    },


                                    'Retspleje 36.3': {
                                        'name': 'Fuldbyrdelse',
                                        'id': 'Retspleje 36.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 36.3 - Fuldbyrdelse'
                                    },


                                    'Retspleje 36.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 36.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 36.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Procesmåden',
                               'id': 'Retspleje 3.6',
                               'key': '6',
                               'display_title': 'Retspleje 3.6 - Procesmåden'
                           },


                           'Retspleje 3.7': {
                               'subs': {

                                    'Retspleje 37.1': {
                                        'name': 'Tiltalens iværksættelse, anklageskrift',
                                        'id': 'Retspleje 37.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 37.1 - Tiltalens iværksættelse, anklageskrift'
                                    },


                                    'Retspleje 37.2': {
                                        'name': 'Sagsforberedelse',
                                        'id': 'Retspleje 37.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 37.2 - Sagsforberedelse'
                                    },


                                    'Retspleje 37.3': {
                                        'name': 'Domsforhandling',
                                        'id': 'Retspleje 37.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 37.3 - Domsforhandling'
                                    },


                                    'Retspleje 37.4': {
                                        'subs': {

                                             'Retspleje 374.1': {
                                                 'name': 'Tilståelsessager',
                                                 'id': 'Retspleje 374.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 374.1 - Tilståelsessager'
                                             },


                                             'Retspleje 374.2': {
                                                 'name': 'Bødevedtagelser og advarsler',
                                                 'id': 'Retspleje 374.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 374.2 - Bødevedtagelser og advarsler'
                                             },


                                             'Retspleje 374.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Retspleje 374.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 374.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Særligt om byretssager',
                                        'id': 'Retspleje 37.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 37.4 - Særligt om byretssager'
                                    },


                                    'Retspleje 37.5': {
                                        'name': 'Særligt om nævningesager',
                                        'id': 'Retspleje 37.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 37.5 - Særligt om nævningesager'
                                    },


                                    'Retspleje 37.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 37.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 37.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Sagsbehandling i første instans',
                               'id': 'Retspleje 3.7',
                               'key': '7',
                               'display_title': 'Retspleje 3.7 - Sagsbehandling i første instans'
                           },


                           'Retspleje 3.8': {
                               'subs': {

                                    'Retspleje 38.1': {
                                        'subs': {

                                             'Retspleje 381.1': {
                                                 'name': 'Ankefristen',
                                                 'id': 'Retspleje 381.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 381.1 - Ankefristen'
                                             },


                                             'Retspleje 381.2': {
                                                 'name': 'Ankeafkald',
                                                 'id': 'Retspleje 381.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 381.2 - Ankeafkald'
                                             },


                                             'Retspleje 381.3': {
                                                 'name': 'Sagsbehandling',
                                                 'id': 'Retspleje 381.3',
                                                 'key': '3',
                                                 'display_title': 'Retspleje 381.3 - Sagsbehandling'
                                             },


                                             'Retspleje 381.4': {
                                                 'name': 'Nye påstande, anbringender og beviser',
                                                 'id': 'Retspleje 381.4',
                                                 'key': '4',
                                                 'display_title': 'Retspleje 381.4 - Nye påstande, anbringender og beviser'
                                             },


                                             'Retspleje 381.5': {
                                                 'name': 'Særlig om anke til Højesteret',
                                                 'id': 'Retspleje 381.5',
                                                 'key': '5',
                                                 'display_title': 'Retspleje 381.5 - Særlig om anke til Højesteret'
                                             },


                                             'Retspleje 381.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Retspleje 381.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 381.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Anke',
                                        'id': 'Retspleje 38.1',
                                        'key': '1',
                                        'display_title': 'Retspleje 38.1 - Anke'
                                    },


                                    'Retspleje 38.2': {
                                        'subs': {

                                             'Retspleje 382.1': {
                                                 'name': 'Frist',
                                                 'id': 'Retspleje 382.1',
                                                 'key': '1',
                                                 'display_title': 'Retspleje 382.1 - Frist'
                                             },


                                             'Retspleje 382.2': {
                                                 'name': 'Sagsbehandling',
                                                 'id': 'Retspleje 382.2',
                                                 'key': '2',
                                                 'display_title': 'Retspleje 382.2 - Sagsbehandling'
                                             },


                                             'Retspleje 382.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Retspleje 382.9',
                                                 'key': '9',
                                                 'display_title': 'Retspleje 382.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Kære',
                                        'id': 'Retspleje 38.2',
                                        'key': '2',
                                        'display_title': 'Retspleje 38.2 - Kære'
                                    },


                                    'Retspleje 38.3': {
                                        'name': 'Genoptagelse',
                                        'id': 'Retspleje 38.3',
                                        'key': '3',
                                        'display_title': 'Retspleje 38.3 - Genoptagelse'
                                    },


                                    'Retspleje 38.4': {
                                        'name': 'Retskraft',
                                        'id': 'Retspleje 38.4',
                                        'key': '4',
                                        'display_title': 'Retspleje 38.4 - Retskraft'
                                    },


                                    'Retspleje 38.5': {
                                        'name': 'Tredjeinstansbevilling',
                                        'id': 'Retspleje 38.5',
                                        'key': '5',
                                        'display_title': 'Retspleje 38.5 - Tredjeinstansbevilling'
                                    },


                                    'Retspleje 38.6': {
                                        'name': 'Andre straffeprocessuelle bevillinger',
                                        'id': 'Retspleje 38.6',
                                        'key': '6',
                                        'display_title': 'Retspleje 38.6 - Andre straffeprocessuelle bevillinger'
                                    },


                                    'Retspleje 38.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Retspleje 38.9',
                                        'key': '9',
                                        'display_title': 'Retspleje 38.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Retsmidler',
                               'id': 'Retspleje 3.8',
                               'key': '8',
                               'display_title': 'Retspleje 3.8 - Retsmidler'
                           },


                           'Retspleje 3.9': {
                               'name': 'Erstatning i anledning af strafferetlig forfølgning',
                               'id': 'Retspleje 3.9',
                               'key': '9',
                               'display_title': 'Retspleje 3.9 - Erstatning i anledning af strafferetlig forfølgning'
                           }

                      },
                      'name': 'Strafferetspleje',
                      'id': 'Retspleje 3',
                      'key': '3',
                      'display_title': 'Retspleje 3 - Strafferetspleje'
                  },


                  'Retspleje 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Retspleje 9',
                      'key': '9',
                      'display_title': 'Retspleje 9 - Andre spørgsmål'
                  }

             },
             'name': 'Retspleje',
             'id': 'Retspleje',
             'key': 'retspleje',
             'display_title': 'Retspleje'
         },


         'Selskabsret': {
             'subs': {

                  'Selskabsret 1': {
                      'subs': {

                           'Selskabsret 1.1': {
                               'name': 'Stiftelse',
                               'id': 'Selskabsret 1.1',
                               'key': '1',
                               'display_title': 'Selskabsret 1.1 - Stiftelse'
                           },


                           'Selskabsret 1.2': {
                               'name': 'Kapital og regnskabsforhold',
                               'id': 'Selskabsret 1.2',
                               'key': '2',
                               'display_title': 'Selskabsret 1.2 - Kapital og regnskabsforhold'
                           },


                           'Selskabsret 1.3': {
                               'subs': {

                                    'Selskabsret 13.1': {
                                        'name': 'Generalforsamling',
                                        'id': 'Selskabsret 13.1',
                                        'key': '1',
                                        'display_title': 'Selskabsret 13.1 - Generalforsamling'
                                    },


                                    'Selskabsret 13.2': {
                                        'name': 'Bestyrelse',
                                        'id': 'Selskabsret 13.2',
                                        'key': '2',
                                        'display_title': 'Selskabsret 13.2 - Bestyrelse'
                                    },


                                    'Selskabsret 13.3': {
                                        'name': 'Direktion',
                                        'id': 'Selskabsret 13.3',
                                        'key': '3',
                                        'display_title': 'Selskabsret 13.3 - Direktion'
                                    },


                                    'Selskabsret 13.4': {
                                        'name': 'Revisor',
                                        'id': 'Selskabsret 13.4',
                                        'key': '4',
                                        'display_title': 'Selskabsret 13.4 - Revisor'
                                    },


                                    'Selskabsret 13.9': {
                                        'name': 'Andre',
                                        'id': 'Selskabsret 13.9',
                                        'key': '9',
                                        'display_title': 'Selskabsret 13.9 - Andre'
                                    }

                               },
                               'name': 'Selskabsorganer',
                               'id': 'Selskabsret 1.3',
                               'key': '3',
                               'display_title': 'Selskabsret 1.3 - Selskabsorganer'
                           },


                           'Selskabsret 1.4': {
                               'subs': {

                                    'Selskabsret 14.1': {
                                        'name': 'Minoritetsbeskyttelse',
                                        'id': 'Selskabsret 14.1',
                                        'key': '1',
                                        'display_title': 'Selskabsret 14.1 - Minoritetsbeskyttelse'
                                    },


                                    'Selskabsret 14.2': {
                                        'name': 'Erstatningsretligt ansvar',
                                        'id': 'Selskabsret 14.2',
                                        'key': '2',
                                        'display_title': 'Selskabsret 14.2 - Erstatningsretligt ansvar'
                                    },


                                    'Selskabsret 14.3': {
                                        'name': 'Strafansvar',
                                        'id': 'Selskabsret 14.3',
                                        'key': '3',
                                        'display_title': 'Selskabsret 14.3 - Strafansvar'
                                    },


                                    'Selskabsret 14.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Selskabsret 14.9',
                                        'key': '9',
                                        'display_title': 'Selskabsret 14.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Aktionærers og anpartshaveres retsstilling',
                               'id': 'Selskabsret 1.4',
                               'key': '4',
                               'display_title': 'Selskabsret 1.4 - Aktionærers og anpartshaveres retsstilling'
                           },


                           'Selskabsret 1.5': {
                               'name': 'Opløsning og fusion',
                               'id': 'Selskabsret 1.5',
                               'key': '5',
                               'display_title': 'Selskabsret 1.5 - Opløsning og fusion'
                           },


                           'Selskabsret 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Selskabsret 1.9',
                               'key': '9',
                               'display_title': 'Selskabsret 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Aktieselskaber og anpartsselskaber',
                      'id': 'Selskabsret 1',
                      'key': '1',
                      'display_title': 'Selskabsret 1 - Aktieselskaber og anpartsselskaber'
                  },


                  'Selskabsret 2': {
                      'name': 'Forsikringsselskaber',
                      'id': 'Selskabsret 2',
                      'key': '2',
                      'display_title': 'Selskabsret 2 - Forsikringsselskaber'
                  },


                  'Selskabsret 3': {
                      'subs': {

                           'Selskabsret 3.1': {
                               'name': 'Stiftelse og indhold',
                               'id': 'Selskabsret 3.1',
                               'key': '1',
                               'display_title': 'Selskabsret 3.1 - Stiftelse og indhold'
                           },


                           'Selskabsret 3.2': {
                               'subs': {

                                    'Selskabsret 32.1': {
                                        'name': 'Ind- og udtræden',
                                        'id': 'Selskabsret 32.1',
                                        'key': '1',
                                        'display_title': 'Selskabsret 32.1 - Ind- og udtræden'
                                    },


                                    'Selskabsret 32.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Selskabsret 32.9',
                                        'key': '9',
                                        'display_title': 'Selskabsret 32.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Medlemmers retsstilling',
                               'id': 'Selskabsret 3.2',
                               'key': '2',
                               'display_title': 'Selskabsret 3.2 - Medlemmers retsstilling'
                           },


                           'Selskabsret 3.3': {
                               'name': 'Forholdet til tredjemand',
                               'id': 'Selskabsret 3.3',
                               'key': '3',
                               'display_title': 'Selskabsret 3.3 - Forholdet til tredjemand'
                           },


                           'Selskabsret 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Selskabsret 3.9',
                               'key': '9',
                               'display_title': 'Selskabsret 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Andelsforetagender, herunder brugsforeninger m.v.',
                      'id': 'Selskabsret 3',
                      'key': '3',
                      'display_title': 'Selskabsret 3 - Andelsforetagender, herunder brugsforeninger m.v.'
                  },


                  'Selskabsret 4': {
                      'name': 'Firma',
                      'id': 'Selskabsret 4',
                      'key': '4',
                      'display_title': 'Selskabsret 4 - Firma'
                  },


                  'Selskabsret 5': {
                      'subs': {

                           'Selskabsret 5.1': {
                               'name': 'Stiftelse, funktion og ophør',
                               'id': 'Selskabsret 5.1',
                               'key': '1',
                               'display_title': 'Selskabsret 5.1 - Stiftelse, funktion og ophør'
                           },


                           'Selskabsret 5.2': {
                               'subs': {

                                    'Selskabsret 52.1': {
                                        'name': 'Ind- og udtræden',
                                        'id': 'Selskabsret 52.1',
                                        'key': '1',
                                        'display_title': 'Selskabsret 52.1 - Ind- og udtræden'
                                    },


                                    'Selskabsret 52.2': {
                                        'name': 'Udelukkelse af medlemmer',
                                        'id': 'Selskabsret 52.2',
                                        'key': '2',
                                        'display_title': 'Selskabsret 52.2 - Udelukkelse af medlemmer'
                                    },


                                    'Selskabsret 52.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Selskabsret 52.9',
                                        'key': '9',
                                        'display_title': 'Selskabsret 52.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Medlemmers retsstilling',
                               'id': 'Selskabsret 5.2',
                               'key': '2',
                               'display_title': 'Selskabsret 5.2 - Medlemmers retsstilling'
                           },


                           'Selskabsret 5.3': {
                               'name': 'Investeringsforeninger',
                               'id': 'Selskabsret 5.3',
                               'key': '3',
                               'display_title': 'Selskabsret 5.3 - Investeringsforeninger'
                           },


                           'Selskabsret 5.4': {
                               'name': 'Pensionskasser',
                               'id': 'Selskabsret 5.4',
                               'key': '4',
                               'display_title': 'Selskabsret 5.4 - Pensionskasser'
                           },


                           'Selskabsret 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Selskabsret 5.9',
                               'key': '9',
                               'display_title': 'Selskabsret 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Foreninger. Ejerlejlighedsforeninger, se Boligformer 1.2. Andelsboligforeninger, se Boligformer 2.2.',
                      'id': 'Selskabsret 5',
                      'key': '5',
                      'display_title': 'Selskabsret 5 - Foreninger. Ejerlejlighedsforeninger, se Boligformer 1.2. Andelsboligforeninger, se Boligformer 2.2.'
                  },


                  'Selskabsret 6': {
                      'name': 'Kommanditselskaber',
                      'id': 'Selskabsret 6',
                      'key': '6',
                      'display_title': 'Selskabsret 6 - Kommanditselskaber'
                  },


                  'Selskabsret 7': {
                      'name': 'Interessentskaber og sameje, Om sameje i faste samlivsforhold, se Familieret 2',
                      'id': 'Selskabsret 7',
                      'key': '7',
                      'display_title': 'Selskabsret 7 - Interessentskaber og sameje, Om sameje i faste samlivsforhold, se Familieret 2'
                  },


                  'Selskabsret 8': {
                      'name': 'Fonde',
                      'id': 'Selskabsret 8',
                      'key': '8',
                      'display_title': 'Selskabsret 8 - Fonde'
                  },


                  'Selskabsret 9': {
                      'name': 'Legater. Stiftelser',
                      'id': 'Selskabsret 9',
                      'key': '9',
                      'display_title': 'Selskabsret 9 - Legater. Stiftelser'
                  }

             },
             'name': 'Selskabsret',
             'id': 'Selskabsret',
             'key': 'selskabsret',
             'display_title': 'Selskabsret'
         },


         'Skatter': {
             'subs': {

                  'Skatter 1': {
                      'subs': {

                           'Skatter 1.1': {
                               'subs': {

                                    'Skatter 11.1': {
                                        'name': 'Bopæl',
                                        'id': 'Skatter 11.1',
                                        'key': '1',
                                        'display_title': 'Skatter 11.1 - Bopæl'
                                    },


                                    'Skatter 11.2': {
                                        'name': 'Ophold',
                                        'id': 'Skatter 11.2',
                                        'key': '2',
                                        'display_title': 'Skatter 11.2 - Ophold'
                                    },


                                    'Skatter 11.3': {
                                        'name': 'Hjemmehørende',
                                        'id': 'Skatter 11.3',
                                        'key': '3',
                                        'display_title': 'Skatter 11.3 - Hjemmehørende'
                                    },


                                    'Skatter 11.4': {
                                        'name': 'Hjemting',
                                        'id': 'Skatter 11.4',
                                        'key': '4',
                                        'display_title': 'Skatter 11.4 - Hjemting'
                                    },


                                    'Skatter 11.5': {
                                        'name': 'Nøglemedarbejderordningen',
                                        'id': 'Skatter 11.5',
                                        'key': '5',
                                        'display_title': 'Skatter 11.5 - Nøglemedarbejderordningen'
                                    },


                                    'Skatter 11.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 11.9',
                                        'key': '9',
                                        'display_title': 'Skatter 11.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Fuld skattepligt',
                               'id': 'Skatter 1.1',
                               'key': '1',
                               'display_title': 'Skatter 1.1 - Fuld skattepligt'
                           },


                           'Skatter 1.2': {
                               'subs': {

                                    'Skatter 12.1': {
                                        'name': 'Arbejdsindkomst m.v.',
                                        'id': 'Skatter 12.1',
                                        'key': '1',
                                        'display_title': 'Skatter 12.1 - Arbejdsindkomst m.v.'
                                    },


                                    'Skatter 12.2': {
                                        'name': 'Renteindtægter',
                                        'id': 'Skatter 12.2',
                                        'key': '2',
                                        'display_title': 'Skatter 12.2 - Renteindtægter'
                                    },


                                    'Skatter 12.3': {
                                        'name': 'Arbejdsudleje',
                                        'id': 'Skatter 12.3',
                                        'key': '3',
                                        'display_title': 'Skatter 12.3 - Arbejdsudleje'
                                    },


                                    'Skatter 12.4': {
                                        'name': 'Fast driftssted',
                                        'id': 'Skatter 12.4',
                                        'key': '4',
                                        'display_title': 'Skatter 12.4 - Fast driftssted'
                                    },


                                    'Skatter 12.5': {
                                        'name': 'Udbytte',
                                        'id': 'Skatter 12.5',
                                        'key': '5',
                                        'display_title': 'Skatter 12.5 - Udbytte'
                                    },


                                    'Skatter 12.6': {
                                        'name': 'Royalty',
                                        'id': 'Skatter 12.6',
                                        'key': '6',
                                        'display_title': 'Skatter 12.6 - Royalty'
                                    },


                                    'Skatter 12.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 12.9',
                                        'key': '9',
                                        'display_title': 'Skatter 12.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Begrænset skattepligt',
                               'id': 'Skatter 1.2',
                               'key': '2',
                               'display_title': 'Skatter 1.2 - Begrænset skattepligt'
                           },


                           'Skatter 1.3': {
                               'name': 'Dobbeltbeskatning, se også International ret 5',
                               'id': 'Skatter 1.3',
                               'key': '3',
                               'display_title': 'Skatter 1.3 - Dobbeltbeskatning, se også International ret 5'
                           },


                           'Skatter 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Skatter 1.9',
                               'key': '9',
                               'display_title': 'Skatter 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Subjektiv skattepligt, se også International ret 5',
                      'id': 'Skatter 1',
                      'key': '1',
                      'display_title': 'Skatter 1 - Subjektiv skattepligt, se også International ret 5'
                  },


                  'Skatter 2': {
                      'subs': {

                           'Skatter 2.1': {
                               'subs': {

                                    'Skatter 21.1': {
                                        'name': 'Personlig indkomst',
                                        'id': 'Skatter 21.1',
                                        'key': '1',
                                        'display_title': 'Skatter 21.1 - Personlig indkomst'
                                    },


                                    'Skatter 21.2': {
                                        'name': 'Kapitalindkomst',
                                        'id': 'Skatter 21.2',
                                        'key': '2',
                                        'display_title': 'Skatter 21.2 - Kapitalindkomst'
                                    },


                                    'Skatter 21.3': {
                                        'name': 'Aktieindkomst',
                                        'id': 'Skatter 21.3',
                                        'key': '3',
                                        'display_title': 'Skatter 21.3 - Aktieindkomst'
                                    },


                                    'Skatter 21.4': {
                                        'name': 'CFC-indkomst',
                                        'id': 'Skatter 21.4',
                                        'key': '4',
                                        'display_title': 'Skatter 21.4 - CFC-indkomst'
                                    },


                                    'Skatter 21.9': {
                                        'name': 'Anden indkomst',
                                        'id': 'Skatter 21.9',
                                        'key': '9',
                                        'display_title': 'Skatter 21.9 - Anden indkomst'
                                    }

                               },
                               'name': 'Indkomstbeskatning',
                               'id': 'Skatter 2.1',
                               'key': '1',
                               'display_title': 'Skatter 2.1 - Indkomstbeskatning'
                           },


                           'Skatter 2.2': {
                               'subs': {

                                    'Skatter 22.1': {
                                        'name': 'Ægtefæller',
                                        'id': 'Skatter 22.1',
                                        'key': '1',
                                        'display_title': 'Skatter 22.1 - Ægtefæller'
                                    },


                                    'Skatter 22.2': {
                                        'name': 'Ugifte samlevende',
                                        'id': 'Skatter 22.2',
                                        'key': '2',
                                        'display_title': 'Skatter 22.2 - Ugifte samlevende'
                                    },


                                    'Skatter 22.3': {
                                        'name': 'Børn',
                                        'id': 'Skatter 22.3',
                                        'key': '3',
                                        'display_title': 'Skatter 22.3 - Børn'
                                    },


                                    'Skatter 22.4': {
                                        'name': 'Forældre',
                                        'id': 'Skatter 22.4',
                                        'key': '4',
                                        'display_title': 'Skatter 22.4 - Forældre'
                                    },


                                    'Skatter 22.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 22.9',
                                        'key': '9',
                                        'display_title': 'Skatter 22.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Familiebeskatning',
                               'id': 'Skatter 2.2',
                               'key': '2',
                               'display_title': 'Skatter 2.2 - Familiebeskatning'
                           },


                           'Skatter 2.3': {
                               'subs': {

                                    'Skatter 23.1': {
                                        'name': 'Ordning med løbende udbetaling',
                                        'id': 'Skatter 23.1',
                                        'key': '1',
                                        'display_title': 'Skatter 23.1 - Ordning med løbende udbetaling'
                                    },


                                    'Skatter 23.2': {
                                        'name': 'Rateforsikring og -opsparing',
                                        'id': 'Skatter 23.2',
                                        'key': '2',
                                        'display_title': 'Skatter 23.2 - Rateforsikring og -opsparing'
                                    },


                                    'Skatter 23.3': {
                                        'name': 'Kapitalforsikring og -opsparing',
                                        'id': 'Skatter 23.3',
                                        'key': '3',
                                        'display_title': 'Skatter 23.3 - Kapitalforsikring og -opsparing'
                                    },


                                    'Skatter 23.4': {
                                        'name': 'Ordning uden fradragsret',
                                        'id': 'Skatter 23.4',
                                        'key': '4',
                                        'display_title': 'Skatter 23.4 - Ordning uden fradragsret'
                                    },


                                    'Skatter 23.5': {
                                        'name': 'Pensionsafkastbeskatning',
                                        'id': 'Skatter 23.5',
                                        'key': '5',
                                        'display_title': 'Skatter 23.5 - Pensionsafkastbeskatning'
                                    },


                                    'Skatter 23.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 23.9',
                                        'key': '9',
                                        'display_title': 'Skatter 23.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Pensionsbeskatning mv.',
                               'id': 'Skatter 2.3',
                               'key': '3',
                               'display_title': 'Skatter 2.3 - Pensionsbeskatning mv.'
                           },


                           'Skatter 2.4': {
                               'subs': {

                                    'Skatter 24.1': {
                                        'subs': {

                                             'Skatter 241.1': {
                                                 'name': 'Indskudskonto',
                                                 'id': 'Skatter 241.1',
                                                 'key': '1',
                                                 'display_title': 'Skatter 241.1 - Indskudskonto'
                                             },


                                             'Skatter 241.2': {
                                                 'name': 'Mellemregningskonto',
                                                 'id': 'Skatter 241.2',
                                                 'key': '2',
                                                 'display_title': 'Skatter 241.2 - Mellemregningskonto'
                                             },


                                             'Skatter 241.3': {
                                                 'name': 'Indkomstopgørelse',
                                                 'id': 'Skatter 241.3',
                                                 'key': '3',
                                                 'display_title': 'Skatter 241.3 - Indkomstopgørelse'
                                             },


                                             'Skatter 241.4': {
                                                 'name': 'Opsparet overskud',
                                                 'id': 'Skatter 241.4',
                                                 'key': '4',
                                                 'display_title': 'Skatter 241.4 - Opsparet overskud'
                                             },


                                             'Skatter 241.5': {
                                                 'name': 'Hævet overskud',
                                                 'id': 'Skatter 241.5',
                                                 'key': '5',
                                                 'display_title': 'Skatter 241.5 - Hævet overskud'
                                             },


                                             'Skatter 241.6': {
                                                 'name': 'Rentekorrektion',
                                                 'id': 'Skatter 241.6',
                                                 'key': '6',
                                                 'display_title': 'Skatter 241.6 - Rentekorrektion'
                                             },


                                             'Skatter 241.7': {
                                                 'name': 'Underskud',
                                                 'id': 'Skatter 241.7',
                                                 'key': '7',
                                                 'display_title': 'Skatter 241.7 - Underskud'
                                             },


                                             'Skatter 241.8': {
                                                 'name': 'Hæverækkefølgen',
                                                 'id': 'Skatter 241.8',
                                                 'key': '8',
                                                 'display_title': 'Skatter 241.8 - Hæverækkefølgen'
                                             },


                                             'Skatter 241.9': {
                                                 'name': 'Ophør',
                                                 'id': 'Skatter 241.9',
                                                 'key': '9',
                                                 'display_title': 'Skatter 241.9 - Ophør'
                                             }

                                        },
                                        'name': 'Virksomhedsordningen',
                                        'id': 'Skatter 24.1',
                                        'key': '1',
                                        'display_title': 'Skatter 24.1 - Virksomhedsordningen'
                                    },


                                    'Skatter 24.2': {
                                        'name': 'Kapitalafkastordningen',
                                        'id': 'Skatter 24.2',
                                        'key': '2',
                                        'display_title': 'Skatter 24.2 - Kapitalafkastordningen'
                                    },


                                    'Skatter 24.3': {
                                        'name': 'Kapitalafkastberegning for aktier og anparter',
                                        'id': 'Skatter 24.3',
                                        'key': '3',
                                        'display_title': 'Skatter 24.3 - Kapitalafkastberegning for aktier og anparter'
                                    },


                                    'Skatter 24.4': {
                                        'name': 'Udligningsordningen',
                                        'id': 'Skatter 24.4',
                                        'key': '4',
                                        'display_title': 'Skatter 24.4 - Udligningsordningen'
                                    },


                                    'Skatter 24.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 24.9',
                                        'key': '9',
                                        'display_title': 'Skatter 24.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Virksomhedsskatteordninger',
                               'id': 'Skatter 2.4',
                               'key': '4',
                               'display_title': 'Skatter 2.4 - Virksomhedsskatteordninger'
                           },


                           'Skatter 2.5': {
                               'name': 'Interessentskaber og kommanditselskaber m.v.',
                               'id': 'Skatter 2.5',
                               'key': '5',
                               'display_title': 'Skatter 2.5 - Interessentskaber og kommanditselskaber m.v.'
                           },


                           'Skatter 2.6': {
                               'name': 'Anpartsvirksomhed',
                               'id': 'Skatter 2.6',
                               'key': '6',
                               'display_title': 'Skatter 2.6 - Anpartsvirksomhed'
                           },


                           'Skatter 2.7': {
                               'name': 'Arbejdsmarkedsbidrag og særlig pensionsordning',
                               'id': 'Skatter 2.7',
                               'key': '7',
                               'display_title': 'Skatter 2.7 - Arbejdsmarkedsbidrag og særlig pensionsordning'
                           },


                           'Skatter 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Skatter 2.9',
                               'key': '9',
                               'display_title': 'Skatter 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Beskatning af fysiske personer',
                      'id': 'Skatter 2',
                      'key': '2',
                      'display_title': 'Skatter 2 - Beskatning af fysiske personer'
                  },


                  'Skatter 3': {
                      'subs': {

                           'Skatter 3.1': {
                               'subs': {

                                    'Skatter 31.1': {
                                        'subs': {

                                             'Skatter 311.1': {
                                                 'name': 'Udbytte',
                                                 'id': 'Skatter 311.1',
                                                 'key': '1',
                                                 'display_title': 'Skatter 311.1 - Udbytte'
                                             },


                                             'Skatter 311.2': {
                                                 'name': 'Udlandslempelse',
                                                 'id': 'Skatter 311.2',
                                                 'key': '2',
                                                 'display_title': 'Skatter 311.2 - Udlandslempelse'
                                             },


                                             'Skatter 311.3': {
                                                 'name': 'Sambeskatning',
                                                 'id': 'Skatter 311.3',
                                                 'key': '3',
                                                 'display_title': 'Skatter 311.3 - Sambeskatning'
                                             },


                                             'Skatter 311.4': {
                                                 'name': 'Transfer pricing',
                                                 'id': 'Skatter 311.4',
                                                 'key': '4',
                                                 'display_title': 'Skatter 311.4 - Transfer pricing'
                                             },


                                             'Skatter 311.5': {
                                                 'name': 'Underskudsfremførsel',
                                                 'id': 'Skatter 311.5',
                                                 'key': '5',
                                                 'display_title': 'Skatter 311.5 - Underskudsfremførsel'
                                             },


                                             'Skatter 311.6': {
                                                 'name': 'Koncerninterne forsikringspræmier',
                                                 'id': 'Skatter 311.6',
                                                 'key': '6',
                                                 'display_title': 'Skatter 311.6 - Koncerninterne forsikringspræmier'
                                             },


                                             'Skatter 311.7': {
                                                 'name': 'Tynd kapitalisering',
                                                 'id': 'Skatter 311.7',
                                                 'key': '7',
                                                 'display_title': 'Skatter 311.7 - Tynd kapitalisering'
                                             },


                                             'Skatter 311.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Skatter 311.9',
                                                 'key': '9',
                                                 'display_title': 'Skatter 311.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Aktie- og anpartsselskaber',
                                        'id': 'Skatter 31.1',
                                        'key': '1',
                                        'display_title': 'Skatter 31.1 - Aktie- og anpartsselskaber'
                                    },


                                    'Skatter 31.2': {
                                        'subs': {

                                             'Skatter 312.1': {
                                                 'name': 'Indkøbsforeninger',
                                                 'id': 'Skatter 312.1',
                                                 'key': '1',
                                                 'display_title': 'Skatter 312.1 - Indkøbsforeninger'
                                             },


                                             'Skatter 312.2': {
                                                 'name': 'Produktions- og salgsforeninger',
                                                 'id': 'Skatter 312.2',
                                                 'key': '2',
                                                 'display_title': 'Skatter 312.2 - Produktions- og salgsforeninger'
                                             },


                                             'Skatter 312.3': {
                                                 'name': 'Blandede andelsforeninger',
                                                 'id': 'Skatter 312.3',
                                                 'key': '3',
                                                 'display_title': 'Skatter 312.3 - Blandede andelsforeninger'
                                             },


                                             'Skatter 312.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Skatter 312.9',
                                                 'key': '9',
                                                 'display_title': 'Skatter 312.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Andelsbeskatning',
                                        'id': 'Skatter 31.2',
                                        'key': '2',
                                        'display_title': 'Skatter 31.2 - Andelsbeskatning'
                                    },


                                    'Skatter 31.3': {
                                        'name': 'Foreningsbeskatning',
                                        'id': 'Skatter 31.3',
                                        'key': '3',
                                        'display_title': 'Skatter 31.3 - Foreningsbeskatning'
                                    },


                                    'Skatter 31.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 31.9',
                                        'key': '9',
                                        'display_title': 'Skatter 31.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Selskabsbeskatning',
                               'id': 'Skatter 3.1',
                               'key': '1',
                               'display_title': 'Skatter 3.1 - Selskabsbeskatning'
                           },


                           'Skatter 3.2': {
                               'subs': {

                                    'Skatter 32.1': {
                                        'name': 'Fonde',
                                        'id': 'Skatter 32.1',
                                        'key': '1',
                                        'display_title': 'Skatter 32.1 - Fonde'
                                    },


                                    'Skatter 32.2': {
                                        'name': 'Brancheforeninger',
                                        'id': 'Skatter 32.2',
                                        'key': '2',
                                        'display_title': 'Skatter 32.2 - Brancheforeninger'
                                    },


                                    'Skatter 32.3': {
                                        'name': 'Arbejdsmarkedssammenslutninger',
                                        'id': 'Skatter 32.3',
                                        'key': '3',
                                        'display_title': 'Skatter 32.3 - Arbejdsmarkedssammenslutninger'
                                    },


                                    'Skatter 32.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 32.9',
                                        'key': '9',
                                        'display_title': 'Skatter 32.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Fondsbeskatning',
                               'id': 'Skatter 3.2',
                               'key': '2',
                               'display_title': 'Skatter 3.2 - Fondsbeskatning'
                           },


                           'Skatter 3.3': {
                               'subs': {

                                    'Skatter 33.1': {
                                        'name': 'Salg',
                                        'id': 'Skatter 33.1',
                                        'key': '1',
                                        'display_title': 'Skatter 33.1 - Salg'
                                    },


                                    'Skatter 33.2': {
                                        'name': 'Virksomhedsomdannelse',
                                        'id': 'Skatter 33.2',
                                        'key': '2',
                                        'display_title': 'Skatter 33.2 - Virksomhedsomdannelse'
                                    },


                                    'Skatter 33.3': {
                                        'name': 'Fusion',
                                        'id': 'Skatter 33.3',
                                        'key': '3',
                                        'display_title': 'Skatter 33.3 - Fusion'
                                    },


                                    'Skatter 33.4': {
                                        'name': 'Spaltning',
                                        'id': 'Skatter 33.4',
                                        'key': '4',
                                        'display_title': 'Skatter 33.4 - Spaltning'
                                    },


                                    'Skatter 33.5': {
                                        'name': 'Likvidation og konkurs',
                                        'id': 'Skatter 33.5',
                                        'key': '5',
                                        'display_title': 'Skatter 33.5 - Likvidation og konkurs'
                                    },


                                    'Skatter 33.6': {
                                        'name': 'Tilførsel af aktiver',
                                        'id': 'Skatter 33.6',
                                        'key': '6',
                                        'display_title': 'Skatter 33.6 - Tilførsel af aktiver'
                                    },


                                    'Skatter 33.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 33.9',
                                        'key': '9',
                                        'display_title': 'Skatter 33.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ophør af virksomhed',
                               'id': 'Skatter 3.3',
                               'key': '3',
                               'display_title': 'Skatter 3.3 - Ophør af virksomhed'
                           },


                           'Skatter 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Skatter 3.9',
                               'key': '9',
                               'display_title': 'Skatter 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Beskatning af selskaber, foreninger og fonde m.v.',
                      'id': 'Skatter 3',
                      'key': '3',
                      'display_title': 'Skatter 3 - Beskatning af selskaber, foreninger og fonde m.v.'
                  },


                  'Skatter 4': {
                      'subs': {

                           'Skatter 4.1': {
                               'subs': {

                                    'Skatter 41.1': {
                                        'name': 'Selvstændige erhvervsdrivende',
                                        'id': 'Skatter 41.1',
                                        'key': '1',
                                        'display_title': 'Skatter 41.1 - Selvstændige erhvervsdrivende'
                                    },


                                    'Skatter 41.2': {
                                        'name': 'Lønmodtagere m.v.',
                                        'id': 'Skatter 41.2',
                                        'key': '2',
                                        'display_title': 'Skatter 41.2 - Lønmodtagere m.v.'
                                    },


                                    'Skatter 41.3': {
                                        'name': 'Hobbyvirksomhed',
                                        'id': 'Skatter 41.3',
                                        'key': '3',
                                        'display_title': 'Skatter 41.3 - Hobbyvirksomhed'
                                    },


                                    'Skatter 41.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 41.9',
                                        'key': '9',
                                        'display_title': 'Skatter 41.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Driftsomkostninger',
                               'id': 'Skatter 4.1',
                               'key': '1',
                               'display_title': 'Skatter 4.1 - Driftsomkostninger'
                           },


                           'Skatter 4.2': {
                               'name': 'Anlægs- og privatudgifter',
                               'id': 'Skatter 4.2',
                               'key': '2',
                               'display_title': 'Skatter 4.2 - Anlægs- og privatudgifter'
                           },


                           'Skatter 4.3': {
                               'name': 'Periodeafgrænsning',
                               'id': 'Skatter 4.3',
                               'key': '3',
                               'display_title': 'Skatter 4.3 - Periodeafgrænsning'
                           },


                           'Skatter 4.4': {
                               'subs': {

                                    'Skatter 44.1': {
                                        'name': 'Driftsmidler',
                                        'id': 'Skatter 44.1',
                                        'key': '1',
                                        'display_title': 'Skatter 44.1 - Driftsmidler'
                                    },


                                    'Skatter 44.2': {
                                        'name': 'Skibe',
                                        'id': 'Skatter 44.2',
                                        'key': '2',
                                        'display_title': 'Skatter 44.2 - Skibe'
                                    },


                                    'Skatter 44.3': {
                                        'name': 'Bygninger og installationer',
                                        'id': 'Skatter 44.3',
                                        'key': '3',
                                        'display_title': 'Skatter 44.3 - Bygninger og installationer'
                                    },


                                    'Skatter 44.4': {
                                        'name': 'Immaterielle aktiver',
                                        'id': 'Skatter 44.4',
                                        'key': '4',
                                        'display_title': 'Skatter 44.4 - Immaterielle aktiver'
                                    },


                                    'Skatter 44.5': {
                                        'name': 'Goodwill',
                                        'id': 'Skatter 44.5',
                                        'key': '5',
                                        'display_title': 'Skatter 44.5 - Goodwill'
                                    },


                                    'Skatter 44.6': {
                                        'name': 'Kunstnerisk udsmykning',
                                        'id': 'Skatter 44.6',
                                        'key': '6',
                                        'display_title': 'Skatter 44.6 - Kunstnerisk udsmykning'
                                    },


                                    'Skatter 44.7': {
                                        'name': 'Drænings- og markvandingsanlæg',
                                        'id': 'Skatter 44.7',
                                        'key': '7',
                                        'display_title': 'Skatter 44.7 - Drænings- og markvandingsanlæg'
                                    },


                                    'Skatter 44.8': {
                                        'name': 'Software',
                                        'id': 'Skatter 44.8',
                                        'key': '8',
                                        'display_title': 'Skatter 44.8 - Software'
                                    },


                                    'Skatter 44.9': {
                                        'name': 'Andre aktiver',
                                        'id': 'Skatter 44.9',
                                        'key': '9',
                                        'display_title': 'Skatter 44.9 - Andre aktiver'
                                    }

                               },
                               'name': 'Afskrivninger',
                               'id': 'Skatter 4.4',
                               'key': '4',
                               'display_title': 'Skatter 4.4 - Afskrivninger'
                           },


                           'Skatter 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Skatter 4.9',
                               'key': '9',
                               'display_title': 'Skatter 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Fradrag i indkomst',
                      'id': 'Skatter 4',
                      'key': '4',
                      'display_title': 'Skatter 4 - Fradrag i indkomst'
                  },


                  'Skatter 5': {
                      'subs': {

                           'Skatter 5.1': {
                               'name': 'Næring',
                               'id': 'Skatter 5.1',
                               'key': '1',
                               'display_title': 'Skatter 5.1 - Næring'
                           },


                           'Skatter 5.2': {
                               'name': 'Spekulation',
                               'id': 'Skatter 5.2',
                               'key': '2',
                               'display_title': 'Skatter 5.2 - Spekulation'
                           },


                           'Skatter 5.3': {
                               'subs': {

                                    'Skatter 53.1': {
                                        'name': 'Én- og tofamiliehuse m.v.',
                                        'id': 'Skatter 53.1',
                                        'key': '1',
                                        'display_title': 'Skatter 53.1 - Én- og tofamiliehuse m.v.'
                                    },


                                    'Skatter 53.2': {
                                        'name': 'Stuehuse på landet m.v.',
                                        'id': 'Skatter 53.2',
                                        'key': '2',
                                        'display_title': 'Skatter 53.2 - Stuehuse på landet m.v.'
                                    },


                                    'Skatter 53.3': {
                                        'name': 'Andre ejerformer',
                                        'id': 'Skatter 53.3',
                                        'key': '3',
                                        'display_title': 'Skatter 53.3 - Andre ejerformer'
                                    },


                                    'Skatter 53.4': {
                                        'name': 'Førstegangssalg af ejerlejligheder',
                                        'id': 'Skatter 53.4',
                                        'key': '4',
                                        'display_title': 'Skatter 53.4 - Førstegangssalg af ejerlejligheder'
                                    },


                                    'Skatter 53.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 53.9',
                                        'key': '9',
                                        'display_title': 'Skatter 53.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ejendomsavancebeskatning',
                               'id': 'Skatter 5.3',
                               'key': '3',
                               'display_title': 'Skatter 5.3 - Ejendomsavancebeskatning'
                           },


                           'Skatter 5.4': {
                               'subs': {

                                    'Skatter 54.1': {
                                        'name': 'Fordringer i danske kroner',
                                        'id': 'Skatter 54.1',
                                        'key': '1',
                                        'display_title': 'Skatter 54.1 - Fordringer i danske kroner'
                                    },


                                    'Skatter 54.2': {
                                        'name': 'Gæld i danske kroner',
                                        'id': 'Skatter 54.2',
                                        'key': '2',
                                        'display_title': 'Skatter 54.2 - Gæld i danske kroner'
                                    },


                                    'Skatter 54.3': {
                                        'name': 'Fordringer og gæld i fremmed valuta',
                                        'id': 'Skatter 54.3',
                                        'key': '3',
                                        'display_title': 'Skatter 54.3 - Fordringer og gæld i fremmed valuta'
                                    },


                                    'Skatter 54.4': {
                                        'name': 'Fordringer erhvervet for lånte midler',
                                        'id': 'Skatter 54.4',
                                        'key': '4',
                                        'display_title': 'Skatter 54.4 - Fordringer erhvervet for lånte midler'
                                    },


                                    'Skatter 54.5': {
                                        'name': 'Lån udbetalt til overkurs',
                                        'id': 'Skatter 54.5',
                                        'key': '5',
                                        'display_title': 'Skatter 54.5 - Lån udbetalt til overkurs'
                                    },


                                    'Skatter 54.6': {
                                        'name': 'Realisationsprincippet',
                                        'id': 'Skatter 54.6',
                                        'key': '6',
                                        'display_title': 'Skatter 54.6 - Realisationsprincippet'
                                    },


                                    'Skatter 54.7': {
                                        'name': 'Lagerprincippet',
                                        'id': 'Skatter 54.7',
                                        'key': '7',
                                        'display_title': 'Skatter 54.7 - Lagerprincippet'
                                    },


                                    'Skatter 54.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 54.9',
                                        'key': '9',
                                        'display_title': 'Skatter 54.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Kursgevinster',
                               'id': 'Skatter 5.4',
                               'key': '4',
                               'display_title': 'Skatter 5.4 - Kursgevinster'
                           },


                           'Skatter 5.5': {
                               'subs': {

                                    'Skatter 55.1': {
                                        'name': 'Næringsaktier',
                                        'id': 'Skatter 55.1',
                                        'key': '1',
                                        'display_title': 'Skatter 55.1 - Næringsaktier'
                                    },


                                    'Skatter 55.2': {
                                        'name': 'Korttidsaktier',
                                        'id': 'Skatter 55.2',
                                        'key': '2',
                                        'display_title': 'Skatter 55.2 - Korttidsaktier'
                                    },


                                    'Skatter 55.3': {
                                        'name': 'Langtidsaktier',
                                        'id': 'Skatter 55.3',
                                        'key': '3',
                                        'display_title': 'Skatter 55.3 - Langtidsaktier'
                                    },


                                    'Skatter 55.4': {
                                        'name': 'Gennemsnitsmetoden',
                                        'id': 'Skatter 55.4',
                                        'key': '4',
                                        'display_title': 'Skatter 55.4 - Gennemsnitsmetoden'
                                    },


                                    'Skatter 55.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 55.9',
                                        'key': '9',
                                        'display_title': 'Skatter 55.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Aktieavancebeskatning',
                               'id': 'Skatter 5.5',
                               'key': '5',
                               'display_title': 'Skatter 5.5 - Aktieavancebeskatning'
                           },


                           'Skatter 5.6': {
                               'subs': {

                                    'Skatter 56.1': {
                                        'name': 'Goodwill',
                                        'id': 'Skatter 56.1',
                                        'key': '1',
                                        'display_title': 'Skatter 56.1 - Goodwill'
                                    },


                                    'Skatter 56.2': {
                                        'name': 'Tidsbegrænsede og tidsubegrænsede rettigheder',
                                        'id': 'Skatter 56.2',
                                        'key': '2',
                                        'display_title': 'Skatter 56.2 - Tidsbegrænsede og tidsubegrænsede rettigheder'
                                    },


                                    'Skatter 56.3': {
                                        'name': 'Udbytte-, forpagtnings- og lejekontrakter',
                                        'id': 'Skatter 56.3',
                                        'key': '3',
                                        'display_title': 'Skatter 56.3 - Udbytte-, forpagtnings- og lejekontrakter'
                                    },


                                    'Skatter 56.4': {
                                        'name': 'Opgivelse af agentur og lign.',
                                        'id': 'Skatter 56.4',
                                        'key': '4',
                                        'display_title': 'Skatter 56.4 - Opgivelse af agentur og lign.'
                                    },


                                    'Skatter 56.5': {
                                        'name': 'Konkurrenceklausuler',
                                        'id': 'Skatter 56.5',
                                        'key': '5',
                                        'display_title': 'Skatter 56.5 - Konkurrenceklausuler'
                                    },


                                    'Skatter 56.9': {
                                        'name': 'Andre aktiver',
                                        'id': 'Skatter 56.9',
                                        'key': '9',
                                        'display_title': 'Skatter 56.9 - Andre aktiver'
                                    }

                               },
                               'name': 'Immaterielle aktiver',
                               'id': 'Skatter 5.6',
                               'key': '6',
                               'display_title': 'Skatter 5.6 - Immaterielle aktiver'
                           },


                           'Skatter 5.9': {
                               'name': 'Andre aktiver',
                               'id': 'Skatter 5.9',
                               'key': '9',
                               'display_title': 'Skatter 5.9 - Andre aktiver'
                           }

                      },
                      'name': 'Kapitalgevinster',
                      'id': 'Skatter 5',
                      'key': '5',
                      'display_title': 'Skatter 5 - Kapitalgevinster'
                  },


                  'Skatter 6': {
                      'subs': {

                           'Skatter 6.1': {
                               'subs': {

                                    'Skatter 61.1': {
                                        'name': 'Skattefritagne boer',
                                        'id': 'Skatter 61.1',
                                        'key': '1',
                                        'display_title': 'Skatter 61.1 - Skattefritagne boer'
                                    },


                                    'Skatter 61.2': {
                                        'name': 'Ikke skattefritagne boer',
                                        'id': 'Skatter 61.2',
                                        'key': '2',
                                        'display_title': 'Skatter 61.2 - Ikke skattefritagne boer'
                                    },


                                    'Skatter 61.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 61.9',
                                        'key': '9',
                                        'display_title': 'Skatter 61.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Dødsboer',
                               'id': 'Skatter 6.1',
                               'key': '1',
                               'display_title': 'Skatter 6.1 - Dødsboer'
                           },


                           'Skatter 6.2': {
                               'subs': {

                                    'Skatter 62.1': {
                                        'name': 'Skattefritagne boer',
                                        'id': 'Skatter 62.1',
                                        'key': '1',
                                        'display_title': 'Skatter 62.1 - Skattefritagne boer'
                                    },


                                    'Skatter 62.2': {
                                        'name': 'Ikke skattefritagne boer',
                                        'id': 'Skatter 62.2',
                                        'key': '2',
                                        'display_title': 'Skatter 62.2 - Ikke skattefritagne boer'
                                    },


                                    'Skatter 62.3': {
                                        'name': 'Ægtefællen som skattesubjekt',
                                        'id': 'Skatter 62.3',
                                        'key': '3',
                                        'display_title': 'Skatter 62.3 - Ægtefællen som skattesubjekt'
                                    },


                                    'Skatter 62.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 62.9',
                                        'key': '9',
                                        'display_title': 'Skatter 62.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Uskiftet bo',
                               'id': 'Skatter 6.2',
                               'key': '2',
                               'display_title': 'Skatter 6.2 - Uskiftet bo'
                           },


                           'Skatter 6.3': {
                               'subs': {

                                    'Skatter 63.1': {
                                        'name': 'Efterlevende ægtefælle',
                                        'id': 'Skatter 63.1',
                                        'key': '1',
                                        'display_title': 'Skatter 63.1 - Efterlevende ægtefælle'
                                    },


                                    'Skatter 63.2': {
                                        'name': 'Arvinger',
                                        'id': 'Skatter 63.2',
                                        'key': '2',
                                        'display_title': 'Skatter 63.2 - Arvinger'
                                    },


                                    'Skatter 63.3': {
                                        'name': 'Legatarer',
                                        'id': 'Skatter 63.3',
                                        'key': '3',
                                        'display_title': 'Skatter 63.3 - Legatarer'
                                    },


                                    'Skatter 63.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 63.9',
                                        'key': '9',
                                        'display_title': 'Skatter 63.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Lodtagere',
                               'id': 'Skatter 6.3',
                               'key': '3',
                               'display_title': 'Skatter 6.3 - Lodtagere'
                           },


                           'Skatter 6.4': {
                               'name': 'Genoptagelsesboer',
                               'id': 'Skatter 6.4',
                               'key': '4',
                               'display_title': 'Skatter 6.4 - Genoptagelsesboer'
                           },


                           'Skatter 6.5': {
                               'name': 'Succession',
                               'id': 'Skatter 6.5',
                               'key': '5',
                               'display_title': 'Skatter 6.5 - Succession'
                           },


                           'Skatter 6.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Skatter 6.9',
                               'key': '9',
                               'display_title': 'Skatter 6.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Dødsbobeskatning',
                      'id': 'Skatter 6',
                      'key': '6',
                      'display_title': 'Skatter 6 - Dødsbobeskatning'
                  },


                  'Skatter 7': {
                      'subs': {

                           'Skatter 7.1': {
                               'name': 'Skattekontrol og strafansvar',
                               'id': 'Skatter 7.1',
                               'key': '1',
                               'display_title': 'Skatter 7.1 - Skattekontrol og strafansvar'
                           },


                           'Skatter 7.2': {
                               'subs': {

                                    'Skatter 72.1': {
                                        'name': 'Kildeskat',
                                        'id': 'Skatter 72.1',
                                        'key': '1',
                                        'display_title': 'Skatter 72.1 - Kildeskat'
                                    },


                                    'Skatter 72.2': {
                                        'name': 'Inddrivelse',
                                        'id': 'Skatter 72.2',
                                        'key': '2',
                                        'display_title': 'Skatter 72.2 - Inddrivelse'
                                    },


                                    'Skatter 72.3': {
                                        'name': 'Forældelse',
                                        'id': 'Skatter 72.3',
                                        'key': '3',
                                        'display_title': 'Skatter 72.3 - Forældelse'
                                    },


                                    'Skatter 72.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 72.9',
                                        'key': '9',
                                        'display_title': 'Skatter 72.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Skatteopkrævning',
                               'id': 'Skatter 7.2',
                               'key': '2',
                               'display_title': 'Skatter 7.2 - Skatteopkrævning'
                           },


                           'Skatter 7.3': {
                               'name': 'Genoptagelse og andre forvaltningsspørgsmål',
                               'id': 'Skatter 7.3',
                               'key': '3',
                               'display_title': 'Skatter 7.3 - Genoptagelse og andre forvaltningsspørgsmål'
                           },


                           'Skatter 7.4': {
                               'name': 'Domstolsprøvelse, se også Forvaltningsret 1.5',
                               'id': 'Skatter 7.4',
                               'key': '4',
                               'display_title': 'Skatter 7.4 - Domstolsprøvelse, se også Forvaltningsret 1.5'
                           },


                           'Skatter 7.5': {
                               'name': 'Skatteforbehold og omgørelse',
                               'id': 'Skatter 7.5',
                               'key': '5',
                               'display_title': 'Skatter 7.5 - Skatteforbehold og omgørelse'
                           },


                           'Skatter 7.6': {
                               'name': 'Forventningsprincippet',
                               'id': 'Skatter 7.6',
                               'key': '6',
                               'display_title': 'Skatter 7.6 - Forventningsprincippet'
                           },


                           'Skatter 7.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Skatter 7.9',
                               'key': '9',
                               'display_title': 'Skatter 7.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Skatteproces',
                      'id': 'Skatter 7',
                      'key': '7',
                      'display_title': 'Skatter 7 - Skatteproces'
                  },


                  'Skatter 8': {
                      'name': 'Omgåelse og proforma',
                      'id': 'Skatter 8',
                      'key': '8',
                      'display_title': 'Skatter 8 - Omgåelse og proforma'
                  },


                  'Skatter 9': {
                      'subs': {

                           'Skatter 9.1': {
                               'subs': {

                                    'Skatter 91.1': {
                                        'name': 'Vurdering',
                                        'id': 'Skatter 91.1',
                                        'key': '1',
                                        'display_title': 'Skatter 91.1 - Vurdering'
                                    },


                                    'Skatter 91.2': {
                                        'name': 'Ejendomsværdiskat',
                                        'id': 'Skatter 91.2',
                                        'key': '2',
                                        'display_title': 'Skatter 91.2 - Ejendomsværdiskat'
                                    },


                                    'Skatter 91.3': {
                                        'name': 'Grundskyld',
                                        'id': 'Skatter 91.3',
                                        'key': '3',
                                        'display_title': 'Skatter 91.3 - Grundskyld'
                                    },


                                    'Skatter 91.4': {
                                        'name': 'Dækningsafgift',
                                        'id': 'Skatter 91.4',
                                        'key': '4',
                                        'display_title': 'Skatter 91.4 - Dækningsafgift'
                                    },


                                    'Skatter 91.5': {
                                        'name': 'Frigørelsesafgift',
                                        'id': 'Skatter 91.5',
                                        'key': '5',
                                        'display_title': 'Skatter 91.5 - Frigørelsesafgift'
                                    },


                                    'Skatter 91.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 91.9',
                                        'key': '9',
                                        'display_title': 'Skatter 91.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ejendomsskat',
                               'id': 'Skatter 9.1',
                               'key': '1',
                               'display_title': 'Skatter 9.1 - Ejendomsskat'
                           },


                           'Skatter 9.2': {
                               'subs': {

                                    'Skatter 92.1': {
                                        'name': 'Omfattet virksomhed',
                                        'id': 'Skatter 92.1',
                                        'key': '1',
                                        'display_title': 'Skatter 92.1 - Omfattet virksomhed'
                                    },


                                    'Skatter 92.2': {
                                        'name': 'Indkomstopgørelse',
                                        'id': 'Skatter 92.2',
                                        'key': '2',
                                        'display_title': 'Skatter 92.2 - Indkomstopgørelse'
                                    },


                                    'Skatter 92.3': {
                                        'name': 'Avancer og genvundne afskrivninger',
                                        'id': 'Skatter 92.3',
                                        'key': '3',
                                        'display_title': 'Skatter 92.3 - Avancer og genvundne afskrivninger'
                                    },


                                    'Skatter 92.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Skatter 92.9',
                                        'key': '9',
                                        'display_title': 'Skatter 92.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Tonnageskat',
                               'id': 'Skatter 9.2',
                               'key': '2',
                               'display_title': 'Skatter 9.2 - Tonnageskat'
                           },


                           'Skatter 9.9': {
                               'name': 'Øvrige skatter',
                               'id': 'Skatter 9.9',
                               'key': '9',
                               'display_title': 'Skatter 9.9 - Øvrige skatter'
                           }

                      },
                      'name': 'Andre skatter',
                      'id': 'Skatter 9',
                      'key': '9',
                      'display_title': 'Skatter 9 - Andre skatter'
                  }

             },
             'name': 'Skatter (se også Afgifter)',
             'id': 'Skatter',
             'key': 'skatter',
             'display_title': 'Skatter'
         },


         'Statsforfatningsret': {
             'subs': {

                  'Statsforfatningsret 1': {
                      'name': 'Riget og de enkelte landsdele. Statsborgerret, se også Færøerne og Grønland',
                      'id': 'Statsforfatningsret 1',
                      'key': '1',
                      'display_title': 'Statsforfatningsret 1 - Riget og de enkelte landsdele. Statsborgerret, se også Færøerne og Grønland'
                  },


                  'Statsforfatningsret 2': {
                      'subs': {

                           'Statsforfatningsret 2.1': {
                               'subs': {

                                    'Statsforfatningsret 21.1': {
                                        'name': 'Valg, valgret og valgbarhed',
                                        'id': 'Statsforfatningsret 21.1',
                                        'key': '1',
                                        'display_title': 'Statsforfatningsret 21.1 - Valg, valgret og valgbarhed'
                                    },


                                    'Statsforfatningsret 21.2': {
                                        'name': 'Lovens forhold til grundloven',
                                        'id': 'Statsforfatningsret 21.2',
                                        'key': '2',
                                        'display_title': 'Statsforfatningsret 21.2 - Lovens forhold til grundloven'
                                    },


                                    'Statsforfatningsret 21.3': {
                                        'name': 'Lovens ikrafttræden m.v.',
                                        'id': 'Statsforfatningsret 21.3',
                                        'key': '3',
                                        'display_title': 'Statsforfatningsret 21.3 - Lovens ikrafttræden m.v.'
                                    },


                                    'Statsforfatningsret 21.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Statsforfatningsret 21.9',
                                        'key': '9',
                                        'display_title': 'Statsforfatningsret 21.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Folketinget',
                               'id': 'Statsforfatningsret 2.1',
                               'key': '1',
                               'display_title': 'Statsforfatningsret 2.1 - Folketinget'
                           },


                           'Statsforfatningsret 2.2': {
                               'name': 'Andre statsorganer',
                               'id': 'Statsforfatningsret 2.2',
                               'key': '2',
                               'display_title': 'Statsforfatningsret 2.2 - Andre statsorganer'
                           },


                           'Statsforfatningsret 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Statsforfatningsret 2.9',
                               'key': '9',
                               'display_title': 'Statsforfatningsret 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Statsorganerne',
                      'id': 'Statsforfatningsret 2',
                      'key': '2',
                      'display_title': 'Statsforfatningsret 2 - Statsorganerne'
                  },


                  'Statsforfatningsret 3': {
                      'subs': {

                           'Statsforfatningsret 3.1': {
                               'subs': {

                                    'Statsforfatningsret 31.1': {
                                        'name': 'Ekspropriationsbegrebet',
                                        'id': 'Statsforfatningsret 31.1',
                                        'key': '1',
                                        'display_title': 'Statsforfatningsret 31.1 - Ekspropriationsbegrebet'
                                    },


                                    'Statsforfatningsret 31.2': {
                                        'name': 'Ekspropriationens lovlighed',
                                        'id': 'Statsforfatningsret 31.2',
                                        'key': '2',
                                        'display_title': 'Statsforfatningsret 31.2 - Ekspropriationens lovlighed'
                                    },


                                    'Statsforfatningsret 31.3': {
                                        'name': 'Erstatningskravet',
                                        'id': 'Statsforfatningsret 31.3',
                                        'key': '3',
                                        'display_title': 'Statsforfatningsret 31.3 - Erstatningskravet'
                                    },


                                    'Statsforfatningsret 31.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Statsforfatningsret 31.9',
                                        'key': '9',
                                        'display_title': 'Statsforfatningsret 31.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ejendomsrettens beskyttelse. Ekspropriation',
                               'id': 'Statsforfatningsret 3.1',
                               'key': '1',
                               'display_title': 'Statsforfatningsret 3.1 - Ejendomsrettens beskyttelse. Ekspropriation'
                           },


                           'Statsforfatningsret 3.2': {
                               'name': 'De øvrige frihedsrettigheder, se også Personspørgsmål 3 og 4 og Menneskerettigheder',
                               'id': 'Statsforfatningsret 3.2',
                               'key': '2',
                               'display_title': 'Statsforfatningsret 3.2 - De øvrige frihedsrettigheder, se også Personspørgsmål 3 og 4 og Menneskerettigheder'
                           },


                           'Statsforfatningsret 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Statsforfatningsret 3.9',
                               'key': '9',
                               'display_title': 'Statsforfatningsret 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Frihedsrettighederne',
                      'id': 'Statsforfatningsret 3',
                      'key': '3',
                      'display_title': 'Statsforfatningsret 3 - Frihedsrettighederne'
                  },


                  'Statsforfatningsret 4': {
                      'name': 'Bevillingsret, se også Forvaltningsret 3.4',
                      'id': 'Statsforfatningsret 4',
                      'key': '4',
                      'display_title': 'Statsforfatningsret 4 - Bevillingsret, se også Forvaltningsret 3.4'
                  },


                  'Statsforfatningsret 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Statsforfatningsret 9',
                      'key': '9',
                      'display_title': 'Statsforfatningsret 9 - Andre spørgsmål'
                  }

             },
             'name': 'Statsforfatningsret',
             'id': 'Statsforfatningsret',
             'key': 'statsforfatningsret',
             'display_title': 'Statsforfatningsret'
         },


         'Strafferet': {
             'subs': {

                  'Strafferet 1': {
                      'subs': {

                           'Strafferet 1.1': {
                               'name': 'Forsæt',
                               'id': 'Strafferet 1.1',
                               'key': '1',
                               'display_title': 'Strafferet 1.1 - Forsæt'
                           },


                           'Strafferet 1.2': {
                               'name': 'Uagtsomhed',
                               'id': 'Strafferet 1.2',
                               'key': '2',
                               'display_title': 'Strafferet 1.2 - Uagtsomhed'
                           },


                           'Strafferet 1.3': {
                               'name': 'Årsagssammenhæng og adækvans',
                               'id': 'Strafferet 1.3',
                               'key': '3',
                               'display_title': 'Strafferet 1.3 - Årsagssammenhæng og adækvans'
                           },


                           'Strafferet 1.4': {
                               'name': 'Forsøg',
                               'id': 'Strafferet 1.4',
                               'key': '4',
                               'display_title': 'Strafferet 1.4 - Forsøg'
                           },


                           'Strafferet 1.5': {
                               'name': 'Medvirken',
                               'id': 'Strafferet 1.5',
                               'key': '5',
                               'display_title': 'Strafferet 1.5 - Medvirken'
                           },


                           'Strafferet 1.6': {
                               'name': 'Nødværge og nødret',
                               'id': 'Strafferet 1.6',
                               'key': '6',
                               'display_title': 'Strafferet 1.6 - Nødværge og nødret'
                           },


                           'Strafferet 1.7': {
                               'subs': {

                                    'Strafferet 17.1': {
                                        'name': 'Sindssygdom og sindssygelignende tilstande',
                                        'id': 'Strafferet 17.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 17.1 - Sindssygdom og sindssygelignende tilstande'
                                    },


                                    'Strafferet 17.2': {
                                        'name': 'Retarderede',
                                        'id': 'Strafferet 17.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 17.2 - Retarderede'
                                    },


                                    'Strafferet 17.3': {
                                        'name': 'Rustilstande',
                                        'id': 'Strafferet 17.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 17.3 - Rustilstande'
                                    },


                                    'Strafferet 17.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Strafferet 17.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 17.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Tilregnelighed',
                               'id': 'Strafferet 1.7',
                               'key': '7',
                               'display_title': 'Strafferet 1.7 - Tilregnelighed'
                           },


                           'Strafferet 1.8': {
                               'name': 'Forældelse',
                               'id': 'Strafferet 1.8',
                               'key': '8',
                               'display_title': 'Strafferet 1.8 - Forældelse'
                           },


                           'Strafferet 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Strafferet 1.9',
                               'key': '9',
                               'display_title': 'Strafferet 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Almindelige strafbetingelser',
                      'id': 'Strafferet 1',
                      'key': '1',
                      'display_title': 'Strafferet 1 - Almindelige strafbetingelser'
                  },


                  'Strafferet 2': {
                      'subs': {

                           'Strafferet 2.1': {
                               'subs': {

                                    'Strafferet 21.1': {
                                        'subs': {

                                             'Strafferet 211.1': {
                                                 'name': 'Manddrab',
                                                 'id': 'Strafferet 211.1',
                                                 'key': '1',
                                                 'display_title': 'Strafferet 211.1 - Manddrab'
                                             },


                                             'Strafferet 211.2': {
                                                 'name': 'Medlidenhedsdrab',
                                                 'id': 'Strafferet 211.2',
                                                 'key': '2',
                                                 'display_title': 'Strafferet 211.2 - Medlidenhedsdrab'
                                             },


                                             'Strafferet 211.3': {
                                                 'name': 'Uagtsomt manddrab',
                                                 'id': 'Strafferet 211.3',
                                                 'key': '3',
                                                 'display_title': 'Strafferet 211.3 - Uagtsomt manddrab'
                                             },


                                             'Strafferet 211.9': {
                                                 'name': 'Andre spørgsmål',
                                                 'id': 'Strafferet 211.9',
                                                 'key': '9',
                                                 'display_title': 'Strafferet 211.9 - Andre spørgsmål'
                                             }

                                        },
                                        'name': 'Drab',
                                        'id': 'Strafferet 21.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 21.1 - Drab'
                                    },


                                    'Strafferet 21.2': {
                                        'name': 'Voldsforbrydelser',
                                        'id': 'Strafferet 21.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 21.2 - Voldsforbrydelser'
                                    },


                                    'Strafferet 21.3': {
                                        'name': 'Andre forbrydelser',
                                        'id': 'Strafferet 21.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 21.3 - Andre forbrydelser'
                                    },


                                    'Strafferet 21.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Strafferet 21.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 21.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Forbrydelser mod liv og legeme',
                               'id': 'Strafferet 2.1',
                               'key': '1',
                               'display_title': 'Strafferet 2.1 - Forbrydelser mod liv og legeme'
                           },


                           'Strafferet 2.2': {
                               'subs': {

                                    'Strafferet 22.1': {
                                        'name': 'Frihedskrænkelser',
                                        'id': 'Strafferet 22.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 22.1 - Frihedskrænkelser'
                                    },


                                    'Strafferet 22.2': {
                                        'name': 'Fredskrænkelser',
                                        'id': 'Strafferet 22.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 22.2 - Fredskrænkelser'
                                    },


                                    'Strafferet 22.3': {
                                        'name': 'Æreskrænkelser',
                                        'id': 'Strafferet 22.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 22.3 - Æreskrænkelser'
                                    },


                                    'Strafferet 22.4': {
                                        'name': 'Racediskrimination',
                                        'id': 'Strafferet 22.4',
                                        'key': '4',
                                        'display_title': 'Strafferet 22.4 - Racediskrimination'
                                    },


                                    'Strafferet 22.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Strafferet 22.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 22.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Forbrydelser mod frihed, fred og ære',
                               'id': 'Strafferet 2.2',
                               'key': '2',
                               'display_title': 'Strafferet 2.2 - Forbrydelser mod frihed, fred og ære'
                           },


                           'Strafferet 2.3': {
                               'subs': {

                                    'Strafferet 23.1': {
                                        'name': 'Voldtægt og lignende forbrydelser',
                                        'id': 'Strafferet 23.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 23.1 - Voldtægt og lignende forbrydelser'
                                    },


                                    'Strafferet 23.2': {
                                        'name': 'Blufærdighedskrænkelse',
                                        'id': 'Strafferet 23.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 23.2 - Blufærdighedskrænkelse'
                                    },


                                    'Strafferet 23.3': {
                                        'name': 'Blodskam',
                                        'id': 'Strafferet 23.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 23.3 - Blodskam'
                                    },


                                    'Strafferet 23.4': {
                                        'name': 'Rufferi m.v.',
                                        'id': 'Strafferet 23.4',
                                        'key': '4',
                                        'display_title': 'Strafferet 23.4 - Rufferi m.v.'
                                    },


                                    'Strafferet 23.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Strafferet 23.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 23.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Seksualforbrydelser',
                               'id': 'Strafferet 2.3',
                               'key': '3',
                               'display_title': 'Strafferet 2.3 - Seksualforbrydelser'
                           },


                           'Strafferet 2.4': {
                               'subs': {

                                    'Strafferet 24.1': {
                                        'name': 'Tyveri',
                                        'id': 'Strafferet 24.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 24.1 - Tyveri'
                                    },


                                    'Strafferet 24.2': {
                                        'name': 'Brugstyveri og hærværkshandlinger',
                                        'id': 'Strafferet 24.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 24.2 - Brugstyveri og hærværkshandlinger'
                                    },


                                    'Strafferet 24.3': {
                                        'name': 'Hittegods, ulovlig omgang med',
                                        'id': 'Strafferet 24.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 24.3 - Hittegods, ulovlig omgang med'
                                    },


                                    'Strafferet 24.4': {
                                        'name': 'Underslæb',
                                        'id': 'Strafferet 24.4',
                                        'key': '4',
                                        'display_title': 'Strafferet 24.4 - Underslæb'
                                    },


                                    'Strafferet 24.5': {
                                        'name': 'Bedrageri - Mandatsvig',
                                        'id': 'Strafferet 24.5',
                                        'key': '5',
                                        'display_title': 'Strafferet 24.5 - Bedrageri - Mandatsvig'
                                    },


                                    'Strafferet 24.6': {
                                        'name': 'Skyldnersvig og andre konkursforbrydelser',
                                        'id': 'Strafferet 24.6',
                                        'key': '6',
                                        'display_title': 'Strafferet 24.6 - Skyldnersvig og andre konkursforbrydelser'
                                    },


                                    'Strafferet 24.7': {
                                        'name': 'Hæleri',
                                        'id': 'Strafferet 24.7',
                                        'key': '7',
                                        'display_title': 'Strafferet 24.7 - Hæleri'
                                    },


                                    'Strafferet 24.8': {
                                        'name': 'Røveri m.v.',
                                        'id': 'Strafferet 24.8',
                                        'key': '8',
                                        'display_title': 'Strafferet 24.8 - Røveri m.v.'
                                    },


                                    'Strafferet 24.9': {
                                        'name': 'Selvtægt og anden strafbar formuekrænkelse (straffelovens kap. 29)',
                                        'id': 'Strafferet 24.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 24.9 - Selvtægt og anden strafbar formuekrænkelse (straffelovens kap. 29)'
                                    }

                               },
                               'name': 'Formueforbrydelser m.v.',
                               'id': 'Strafferet 2.4',
                               'key': '4',
                               'display_title': 'Strafferet 2.4 - Formueforbrydelser m.v.'
                           },


                           'Strafferet 2.5': {
                               'subs': {

                                    'Strafferet 25.1': {
                                        'name': 'Forbrydelser mod personer i offentlig tjeneste',
                                        'id': 'Strafferet 25.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 25.1 - Forbrydelser mod personer i offentlig tjeneste'
                                    },


                                    'Strafferet 25.2': {
                                        'name': 'Falsk forklaring for retten',
                                        'id': 'Strafferet 25.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 25.2 - Falsk forklaring for retten'
                                    },


                                    'Strafferet 25.3': {
                                        'name': 'Falsk anmeldelse o.l.',
                                        'id': 'Strafferet 25.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 25.3 - Falsk anmeldelse o.l.'
                                    },


                                    'Strafferet 25.4': {
                                        'name': 'Bigami',
                                        'id': 'Strafferet 25.4',
                                        'key': '4',
                                        'display_title': 'Strafferet 25.4 - Bigami'
                                    },


                                    'Strafferet 25.5': {
                                        'name': 'Embedsforbrydelser',
                                        'id': 'Strafferet 25.5',
                                        'key': '5',
                                        'display_title': 'Strafferet 25.5 - Embedsforbrydelser'
                                    },


                                    'Strafferet 25.6': {
                                        'name': 'Statsforbrydelser',
                                        'id': 'Strafferet 25.6',
                                        'key': '6',
                                        'display_title': 'Strafferet 25.6 - Statsforbrydelser'
                                    },


                                    'Strafferet 25.9': {
                                        'name': 'Andre forbrydelser vedrørende offentlige myndigheder',
                                        'id': 'Strafferet 25.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 25.9 - Andre forbrydelser vedrørende offentlige myndigheder'
                                    }

                               },
                               'name': 'Forbrydelser vedr. offentlige myndigheder m.v.',
                               'id': 'Strafferet 2.5',
                               'key': '5',
                               'display_title': 'Strafferet 2.5 - Forbrydelser vedr. offentlige myndigheder m.v.'
                           },


                           'Strafferet 2.6': {
                               'subs': {

                                    'Strafferet 26.1': {
                                        'name': 'Loven om euforiserende stoffer',
                                        'id': 'Strafferet 26.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 26.1 - Loven om euforiserende stoffer'
                                    },


                                    'Strafferet 26.2': {
                                        'name': 'Narkotikaforbrydelser',
                                        'id': 'Strafferet 26.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 26.2 - Narkotikaforbrydelser'
                                    },


                                    'Strafferet 26.3': {
                                        'name': 'Udlevering og udvisning, se også Forvaltningsret 296.2',
                                        'id': 'Strafferet 26.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 26.3 - Udlevering og udvisning, se også Forvaltningsret 296.2'
                                    },


                                    'Strafferet 26.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Strafferet 26.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 26.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Narkotikaforbrydelser m.v.',
                               'id': 'Strafferet 2.6',
                               'key': '6',
                               'display_title': 'Strafferet 2.6 - Narkotikaforbrydelser m.v.'
                           },


                           'Strafferet 2.7': {
                               'name': 'Brandstiftelse m.v.',
                               'id': 'Strafferet 2.7',
                               'key': '7',
                               'display_title': 'Strafferet 2.7 - Brandstiftelse m.v.'
                           },


                           'Strafferet 2.8': {
                               'subs': {

                                    'Strafferet 28.1': {
                                        'name': 'Dokumentfalsk',
                                        'id': 'Strafferet 28.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 28.1 - Dokumentfalsk'
                                    },


                                    'Strafferet 28.2': {
                                        'name': 'Erklæringer',
                                        'id': 'Strafferet 28.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 28.2 - Erklæringer'
                                    },


                                    'Strafferet 28.3': {
                                        'name': 'Dokumentmisbrug',
                                        'id': 'Strafferet 28.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 28.3 - Dokumentmisbrug'
                                    },


                                    'Strafferet 28.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Strafferet 28.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 28.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Dokumentforbrydelser',
                               'id': 'Strafferet 2.8',
                               'key': '8',
                               'display_title': 'Strafferet 2.8 - Dokumentforbrydelser'
                           },


                           'Strafferet 2.9': {
                               'subs': {

                                    'Strafferet 29.1': {
                                        'name': 'Svangerskabsafbrydelse',
                                        'id': 'Strafferet 29.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 29.1 - Svangerskabsafbrydelse'
                                    },


                                    'Strafferet 29.2': {
                                        'name': 'Dyreværn, se også Landbrug m.v. 32.1',
                                        'id': 'Strafferet 29.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 29.2 - Dyreværn, se også Landbrug m.v. 32.1'
                                    },


                                    'Strafferet 29.3': {
                                        'name': 'Politivedtægtsovertrædelser',
                                        'id': 'Strafferet 29.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 29.3 - Politivedtægtsovertrædelser'
                                    },


                                    'Strafferet 29.4': {
                                        'name': 'Våben',
                                        'id': 'Strafferet 29.4',
                                        'key': '4',
                                        'display_title': 'Strafferet 29.4 - Våben'
                                    },


                                    'Strafferet 29.9': {
                                        'name': 'Andre overtrædelser, se også Ansættelses- og arbejdsret 8,  Erhvervsret, Forvaltningsret 2, Færdselsret, Landbrug m.v., Markedsret, Miljøret, Pengevæsen m.v., Personspørgsmål, Presse og radio, Selskabsret, Skatter 7.1, Søfart, Transport og kommunikation samt Veje og vand',
                                        'id': 'Strafferet 29.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 29.9 - Andre overtrædelser, se også Ansættelses- og arbejdsret 8,  Erhvervsret, Forvaltningsret 2, Færdselsret, Landbrug m.v., Markedsret, Miljøret, Pengevæsen m.v., Personspørgsmål, Presse og radio, Selskabsret, Skatter 7.1, Søfart, Transport og kommunikation samt Veje og vand'
                                    }

                               },
                               'name': 'Strafbare overtrædelser af særlovgivningen',
                               'id': 'Strafferet 2.9',
                               'key': '9',
                               'display_title': 'Strafferet 2.9 - Strafbare overtrædelser af særlovgivningen'
                           }

                      },
                      'name': 'De enkelte forbrydelser',
                      'id': 'Strafferet 2',
                      'key': '2',
                      'display_title': 'Strafferet 2 - De enkelte forbrydelser'
                  },


                  'Strafferet 3': {
                      'subs': {

                           'Strafferet 3.1': {
                               'subs': {

                                    'Strafferet 31.1': {
                                        'name': 'Strafnedsættelse',
                                        'id': 'Strafferet 31.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 31.1 - Strafnedsættelse'
                                    },


                                    'Strafferet 31.2': {
                                        'name': 'Strafbortfald',
                                        'id': 'Strafferet 31.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 31.2 - Strafbortfald'
                                    },


                                    'Strafferet 31.3': {
                                        'name': 'Gentagelsesvirkning, se også Færdselsret 43.3 og 5.4',
                                        'id': 'Strafferet 31.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 31.3 - Gentagelsesvirkning, se også Færdselsret 43.3 og 5.4'
                                    },


                                    'Strafferet 31.4': {
                                        'name': 'Sammenstød af forbrydelser',
                                        'id': 'Strafferet 31.4',
                                        'key': '4',
                                        'display_title': 'Strafferet 31.4 - Sammenstød af forbrydelser'
                                    },


                                    'Strafferet 31.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Strafferet 31.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 31.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Strafudmåling',
                               'id': 'Strafferet 3.1',
                               'key': '1',
                               'display_title': 'Strafferet 3.1 - Strafudmåling'
                           },


                           'Strafferet 3.2': {
                               'subs': {

                                    'Strafferet 32.1': {
                                        'name': 'Betinget dom',
                                        'id': 'Strafferet 32.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 32.1 - Betinget dom'
                                    },


                                    'Strafferet 32.2': {
                                        'name': 'Samfundstjeneste m.v.',
                                        'id': 'Strafferet 32.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 32.2 - Samfundstjeneste m.v.'
                                    },


                                    'Strafferet 32.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Strafferet 32.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 32.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Betingede retsfølger',
                               'id': 'Strafferet 3.2',
                               'key': '2',
                               'display_title': 'Strafferet 3.2 - Betingede retsfølger'
                           },


                           'Strafferet 3.3': {
                               'name': 'Bøder',
                               'id': 'Strafferet 3.3',
                               'key': '3',
                               'display_title': 'Strafferet 3.3 - Bøder'
                           },


                           'Strafferet 3.4': {
                               'name': 'Konfiskation',
                               'id': 'Strafferet 3.4',
                               'key': '4',
                               'display_title': 'Strafferet 3.4 - Konfiskation'
                           },


                           'Strafferet 3.5': {
                               'name': 'Rettighedsfortabelse',
                               'id': 'Strafferet 3.5',
                               'key': '5',
                               'display_title': 'Strafferet 3.5 - Rettighedsfortabelse'
                           },


                           'Strafferet 3.6': {
                               'subs': {

                                    'Strafferet 36.1': {
                                        'name': 'Forvaring',
                                        'id': 'Strafferet 36.1',
                                        'key': '1',
                                        'display_title': 'Strafferet 36.1 - Forvaring'
                                    },


                                    'Strafferet 36.2': {
                                        'name': 'Sikkerhedsforanstaltninger',
                                        'id': 'Strafferet 36.2',
                                        'key': '2',
                                        'display_title': 'Strafferet 36.2 - Sikkerhedsforanstaltninger'
                                    },


                                    'Strafferet 36.3': {
                                        'name': 'Mentalundersøgelse',
                                        'id': 'Strafferet 36.3',
                                        'key': '3',
                                        'display_title': 'Strafferet 36.3 - Mentalundersøgelse'
                                    },


                                    'Strafferet 36.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Strafferet 36.9',
                                        'key': '9',
                                        'display_title': 'Strafferet 36.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Tidsubestemte foranstaltninger m.v.',
                               'id': 'Strafferet 3.6',
                               'key': '6',
                               'display_title': 'Strafferet 3.6 - Tidsubestemte foranstaltninger m.v.'
                           },


                           'Strafferet 3.7': {
                               'name': 'Straffuldbyrdelse',
                               'id': 'Strafferet 3.7',
                               'key': '7',
                               'display_title': 'Strafferet 3.7 - Straffuldbyrdelse'
                           },


                           'Strafferet 3.8': {
                               'name': 'Udlevering og udvisning',
                               'id': 'Strafferet 3.8',
                               'key': '8',
                               'display_title': 'Strafferet 3.8 - Udlevering og udvisning'
                           },


                           'Strafferet 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Strafferet 3.9',
                               'key': '9',
                               'display_title': 'Strafferet 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Sanktioner',
                      'id': 'Strafferet 3',
                      'key': '3',
                      'display_title': 'Strafferet 3 - Sanktioner'
                  },


                  'Strafferet 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Strafferet 9',
                      'key': '9',
                      'display_title': 'Strafferet 9 - Andre spørgsmål'
                  }

             },
             'name': 'Strafferet',
             'id': 'Strafferet',
             'key': 'strafferet',
             'display_title': 'Strafferet'
         },


         'Søfart': {
             'subs': {

                  'Søfart 1': {
                      'name': 'Personerne (herunder strafansvar), se også Ansættelses- og arbejdsret',
                      'id': 'Søfart 1',
                      'key': '1',
                      'display_title': 'Søfart 1 - Personerne (herunder strafansvar), se også Ansættelses- og arbejdsret'
                  },


                  'Søfart 2': {
                      'subs': {

                           'Søfart 2.1': {
                               'name': 'Befragtning i almindelighed. Konnossement. Fragten og befragterens ansvar',
                               'id': 'Søfart 2.1',
                               'key': '1',
                               'display_title': 'Søfart 2.1 - Befragtning i almindelighed. Konnossement. Fragten og befragterens ansvar'
                           },


                           'Søfart 2.2': {
                               'name': 'Ladning, losning og stuvning. Lasteevne',
                               'id': 'Søfart 2.2',
                               'key': '2',
                               'display_title': 'Søfart 2.2 - Ladning, losning og stuvning. Lasteevne'
                           },


                           'Søfart 2.3': {
                               'name': 'Liggetid',
                               'id': 'Søfart 2.3',
                               'key': '3',
                               'display_title': 'Søfart 2.3 - Liggetid'
                           },


                           'Søfart 2.4': {
                               'name': 'Forsinkelser uden for ladning og losning. Ishindringer',
                               'id': 'Søfart 2.4',
                               'key': '4',
                               'display_title': 'Søfart 2.4 - Forsinkelser uden for ladning og losning. Ishindringer'
                           },


                           'Søfart 2.5': {
                               'name': 'Ansvar for gods',
                               'id': 'Søfart 2.5',
                               'key': '5',
                               'display_title': 'Søfart 2.5 - Ansvar for gods'
                           },


                           'Søfart 2.6': {
                               'name': 'Ansvar for personer',
                               'id': 'Søfart 2.6',
                               'key': '6',
                               'display_title': 'Søfart 2.6 - Ansvar for personer'
                           },


                           'Søfart 2.7': {
                               'name': 'Tidsbefragtning',
                               'id': 'Søfart 2.7',
                               'key': '7',
                               'display_title': 'Søfart 2.7 - Tidsbefragtning'
                           },


                           'Søfart 2.8': {
                               'name': 'Ophævelse af fragtaftale',
                               'id': 'Søfart 2.8',
                               'key': '8',
                               'display_title': 'Søfart 2.8 - Ophævelse af fragtaftale'
                           },


                           'Søfart 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Søfart 2.9',
                               'key': '9',
                               'display_title': 'Søfart 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Befragtning',
                      'id': 'Søfart 2',
                      'key': '2',
                      'display_title': 'Søfart 2 - Befragtning'
                  },


                  'Søfart 3': {
                      'name': 'Groshaveri',
                      'id': 'Søfart 3',
                      'key': '3',
                      'display_title': 'Søfart 3 - Groshaveri'
                  },


                  'Søfart 4': {
                      'name': 'Påsejling og lignende',
                      'id': 'Søfart 4',
                      'key': '4',
                      'display_title': 'Søfart 4 - Påsejling og lignende'
                  },


                  'Søfart 5': {
                      'name': 'Forlis, stranding og bjærgning. Bugsering',
                      'id': 'Søfart 5',
                      'key': '5',
                      'display_title': 'Søfart 5 - Forlis, stranding og bjærgning. Bugsering'
                  },


                  'Søfart 6': {
                      'name': 'Skibsregistrering og søpant',
                      'id': 'Søfart 6',
                      'key': '6',
                      'display_title': 'Søfart 6 - Skibsregistrering og søpant'
                  },


                  'Søfart 7': {
                      'name': 'Skibes sikkerhed m.v.',
                      'id': 'Søfart 7',
                      'key': '7',
                      'display_title': 'Søfart 7 - Skibes sikkerhed m.v.'
                  },


                  'Søfart 8': {
                      'subs': {

                           'Søfart 8.1': {
                               'name': 'Farvandsvæsen',
                               'id': 'Søfart 8.1',
                               'key': '1',
                               'display_title': 'Søfart 8.1 - Farvandsvæsen'
                           },


                           'Søfart 8.2': {
                               'name': 'Lodsvæsen',
                               'id': 'Søfart 8.2',
                               'key': '2',
                               'display_title': 'Søfart 8.2 - Lodsvæsen'
                           },


                           'Søfart 8.3': {
                               'name': 'Havnevæsen',
                               'id': 'Søfart 8.3',
                               'key': '3',
                               'display_title': 'Søfart 8.3 - Havnevæsen'
                           },


                           'Søfart 8.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Søfart 8.9',
                               'key': '9',
                               'display_title': 'Søfart 8.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Farvands-, lods- og havnevæsen',
                      'id': 'Søfart 8',
                      'key': '8',
                      'display_title': 'Søfart 8 - Farvands-, lods- og havnevæsen'
                  },


                  'Søfart 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Søfart 9',
                      'key': '9',
                      'display_title': 'Søfart 9 - Andre spørgsmål'
                  }

             },
             'name': 'Søfart (se også Transport og kommunikation)',
             'id': 'Søfart',
             'key': 's_fart',
             'display_title': 'Søfart'
         },


         'Tinglysning': {
             'subs': {

                  'Tinglysning 1': {
                      'subs': {

                           'Tinglysning 1.1': {
                               'subs': {

                                    'Tinglysning 11.1': {
                                        'name': 'Betingede rettigheder',
                                        'id': 'Tinglysning 11.1',
                                        'key': '1',
                                        'display_title': 'Tinglysning 11.1 - Betingede rettigheder'
                                    },


                                    'Tinglysning 11.2': {
                                        'name': 'Rådighedsindskrænkninger',
                                        'id': 'Tinglysning 11.2',
                                        'key': '2',
                                        'display_title': 'Tinglysning 11.2 - Rådighedsindskrænkninger'
                                    },


                                    'Tinglysning 11.3': {
                                        'name': 'Spørgsmål om, hvorvidt retten hviler på den faste ejendom',
                                        'id': 'Tinglysning 11.3',
                                        'key': '3',
                                        'display_title': 'Tinglysning 11.3 - Spørgsmål om, hvorvidt retten hviler på den faste ejendom'
                                    },


                                    'Tinglysning 11.4': {
                                        'name': 'Brugsrettigheder',
                                        'id': 'Tinglysning 11.4',
                                        'key': '4',
                                        'display_title': 'Tinglysning 11.4 - Brugsrettigheder'
                                    },


                                    'Tinglysning 11.5': {
                                        'name': 'Skatter og afgifter',
                                        'id': 'Tinglysning 11.5',
                                        'key': '5',
                                        'display_title': 'Tinglysning 11.5 - Skatter og afgifter'
                                    },


                                    'Tinglysning 11.6': {
                                        'name': 'Foreløbig lysning af påstået ret',
                                        'id': 'Tinglysning 11.6',
                                        'key': '6',
                                        'display_title': 'Tinglysning 11.6 - Foreløbig lysning af påstået ret'
                                    },


                                    'Tinglysning 11.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Tinglysning 11.9',
                                        'key': '9',
                                        'display_title': 'Tinglysning 11.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Hvilke rettigheder skal eller kan tinglyses',
                               'id': 'Tinglysning 1.1',
                               'key': '1',
                               'display_title': 'Tinglysning 1.1 - Hvilke rettigheder skal eller kan tinglyses'
                           },


                           'Tinglysning 1.2': {
                               'subs': {

                                    'Tinglysning 12.1': {
                                        'name': 'Bestemt fast ejendom',
                                        'id': 'Tinglysning 12.1',
                                        'key': '1',
                                        'display_title': 'Tinglysning 12.1 - Bestemt fast ejendom'
                                    },


                                    'Tinglysning 12.2': {
                                        'name': 'Indhold endelig fastsat',
                                        'id': 'Tinglysning 12.2',
                                        'key': '2',
                                        'display_title': 'Tinglysning 12.2 - Indhold endelig fastsat'
                                    },


                                    'Tinglysning 12.3': {
                                        'name': 'Habilitet',
                                        'id': 'Tinglysning 12.3',
                                        'key': '3',
                                        'display_title': 'Tinglysning 12.3 - Habilitet'
                                    },


                                    'Tinglysning 12.4': {
                                        'name': 'Legitimation og fuldmagt',
                                        'id': 'Tinglysning 12.4',
                                        'key': '4',
                                        'display_title': 'Tinglysning 12.4 - Legitimation og fuldmagt'
                                    },


                                    'Tinglysning 12.5': {
                                        'name': 'Formkrav',
                                        'id': 'Tinglysning 12.5',
                                        'key': '5',
                                        'display_title': 'Tinglysning 12.5 - Formkrav'
                                    },


                                    'Tinglysning 12.6': {
                                        'name': 'Særlig om bygninger på fremmed grund',
                                        'id': 'Tinglysning 12.6',
                                        'key': '6',
                                        'display_title': 'Tinglysning 12.6 - Særlig om bygninger på fremmed grund'
                                    },


                                    'Tinglysning 12.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Tinglysning 12.9',
                                        'key': '9',
                                        'display_title': 'Tinglysning 12.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Grundlag og fremgangsmåde',
                               'id': 'Tinglysning 1.2',
                               'key': '2',
                               'display_title': 'Tinglysning 1.2 - Grundlag og fremgangsmåde'
                           },


                           'Tinglysning 1.3': {
                               'subs': {

                                    'Tinglysning 13.1': {
                                        'name': 'Forhold til tinglyst rådighedsindskrænkning',
                                        'id': 'Tinglysning 13.1',
                                        'key': '1',
                                        'display_title': 'Tinglysning 13.1 - Forhold til tinglyst rådighedsindskrænkning'
                                    },


                                    'Tinglysning 13.2': {
                                        'name': 'Privatretlige regler',
                                        'id': 'Tinglysning 13.2',
                                        'key': '2',
                                        'display_title': 'Tinglysning 13.2 - Privatretlige regler'
                                    },


                                    'Tinglysning 13.3': {
                                        'name': 'Offentligretlige regler',
                                        'id': 'Tinglysning 13.3',
                                        'key': '3',
                                        'display_title': 'Tinglysning 13.3 - Offentligretlige regler'
                                    },


                                    'Tinglysning 13.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Tinglysning 13.9',
                                        'key': '9',
                                        'display_title': 'Tinglysning 13.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Prøvelse af lovlighed og gyldighed',
                               'id': 'Tinglysning 1.3',
                               'key': '3',
                               'display_title': 'Tinglysning 1.3 - Prøvelse af lovlighed og gyldighed'
                           },


                           'Tinglysning 1.4': {
                               'name': 'Retsvirkninger',
                               'id': 'Tinglysning 1.4',
                               'key': '4',
                               'display_title': 'Tinglysning 1.4 - Retsvirkninger'
                           },


                           'Tinglysning 1.5': {
                               'subs': {

                                    'Tinglysning 15.1': {
                                        'name': 'Panterettigheder indbyrdes, se Pant og tilbageholdsret 2.3',
                                        'id': 'Tinglysning 15.1',
                                        'key': '1',
                                        'display_title': 'Tinglysning 15.1 - Panterettigheder indbyrdes, se Pant og tilbageholdsret 2.3'
                                    },


                                    'Tinglysning 15.2': {
                                        'name': 'Servitutter over for panterettigheder',
                                        'id': 'Tinglysning 15.2',
                                        'key': '2',
                                        'display_title': 'Tinglysning 15.2 - Servitutter over for panterettigheder'
                                    },


                                    'Tinglysning 15.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Tinglysning 15.9',
                                        'key': '9',
                                        'display_title': 'Tinglysning 15.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Forholdet mellem flere rettigheder',
                               'id': 'Tinglysning 1.5',
                               'key': '5',
                               'display_title': 'Tinglysning 1.5 - Forholdet mellem flere rettigheder'
                           },


                           'Tinglysning 1.6': {
                               'name': 'Udstykning og sammenlægning m.v.',
                               'id': 'Tinglysning 1.6',
                               'key': '6',
                               'display_title': 'Tinglysning 1.6 - Udstykning og sammenlægning m.v.'
                           },


                           'Tinglysning 1.7': {
                               'name': 'Rettelse og udslettelse',
                               'id': 'Tinglysning 1.7',
                               'key': '7',
                               'display_title': 'Tinglysning 1.7 - Rettelse og udslettelse'
                           },


                           'Tinglysning 1.8': {
                               'name': 'Ansvar for fejl',
                               'id': 'Tinglysning 1.8',
                               'key': '8',
                               'display_title': 'Tinglysning 1.8 - Ansvar for fejl'
                           },


                           'Tinglysning 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Tinglysning 1.9',
                               'key': '9',
                               'display_title': 'Tinglysning 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Fast ejendom',
                      'id': 'Tinglysning 1',
                      'key': '1',
                      'display_title': 'Tinglysning 1 - Fast ejendom'
                  },


                  'Tinglysning 2': {
                      'subs': {

                           'Tinglysning 2.1': {
                               'name': 'Lysningssted',
                               'id': 'Tinglysning 2.1',
                               'key': '1',
                               'display_title': 'Tinglysning 2.1 - Lysningssted'
                           },


                           'Tinglysning 2.2': {
                               'name': 'Pantets genstand',
                               'id': 'Tinglysning 2.2',
                               'key': '2',
                               'display_title': 'Tinglysning 2.2 - Pantets genstand'
                           },


                           'Tinglysning 2.3': {
                               'name': 'Formkrav',
                               'id': 'Tinglysning 2.3',
                               'key': '3',
                               'display_title': 'Tinglysning 2.3 - Formkrav'
                           },


                           'Tinglysning 2.4': {
                               'name': 'Retsvirkninger',
                               'id': 'Tinglysning 2.4',
                               'key': '4',
                               'display_title': 'Tinglysning 2.4 - Retsvirkninger'
                           },


                           'Tinglysning 2.5': {
                               'name': 'Særlig om pant i driftsinventar i erhvervsvirksomheder, der drives fra lejet ejendom',
                               'id': 'Tinglysning 2.5',
                               'key': '5',
                               'display_title': 'Tinglysning 2.5 - Særlig om pant i driftsinventar i erhvervsvirksomheder, der drives fra lejet ejendom'
                           },


                           'Tinglysning 2.6': {
                               'name': 'Ansvar for fejl',
                               'id': 'Tinglysning 2.6',
                               'key': '6',
                               'display_title': 'Tinglysning 2.6 - Ansvar for fejl'
                           },


                           'Tinglysning 2.7': {
                               'name': 'Bilbogen',
                               'id': 'Tinglysning 2.7',
                               'key': '7',
                               'display_title': 'Tinglysning 2.7 - Bilbogen'
                           },


                           'Tinglysning 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Tinglysning 2.9',
                               'key': '9',
                               'display_title': 'Tinglysning 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Underpant i løsøre',
                      'id': 'Tinglysning 2',
                      'key': '2',
                      'display_title': 'Tinglysning 2 - Underpant i løsøre'
                  },


                  'Tinglysning 3': {
                      'name': 'Høstpant og lignende, se også Pant og tilbageholdsret 8',
                      'id': 'Tinglysning 3',
                      'key': '3',
                      'display_title': 'Tinglysning 3 - Høstpant og lignende, se også Pant og tilbageholdsret 8'
                  },


                  'Tinglysning 4': {
                      'name': 'Værgemål med handleevnefratagelse',
                      'id': 'Tinglysning 4',
                      'key': '4',
                      'display_title': 'Tinglysning 4 - Værgemål med handleevnefratagelse'
                  },


                  'Tinglysning 5': {
                      'subs': {

                           'Tinglysning 5.1': {
                               'name': 'Ægtepagt',
                               'id': 'Tinglysning 5.1',
                               'key': '1',
                               'display_title': 'Tinglysning 5.1 - Ægtepagt'
                           },


                           'Tinglysning 5.2': {
                               'name': 'Vielsesattest',
                               'id': 'Tinglysning 5.2',
                               'key': '2',
                               'display_title': 'Tinglysning 5.2 - Vielsesattest'
                           },


                           'Tinglysning 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Tinglysning 5.9',
                               'key': '9',
                               'display_title': 'Tinglysning 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Ægtepagt m.v.',
                      'id': 'Tinglysning 5',
                      'key': '5',
                      'display_title': 'Tinglysning 5 - Ægtepagt m.v.'
                  },


                  'Tinglysning 6': {
                      'name': 'Kære',
                      'id': 'Tinglysning 6',
                      'key': '6',
                      'display_title': 'Tinglysning 6 - Kære'
                  },


                  'Tinglysning 7': {
                      'name': 'Afgift',
                      'id': 'Tinglysning 7',
                      'key': '7',
                      'display_title': 'Tinglysning 7 - Afgift'
                  },


                  'Tinglysning 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Tinglysning 9',
                      'key': '9',
                      'display_title': 'Tinglysning 9 - Andre spørgsmål'
                  }

             },
             'name': 'Tinglysning',
             'id': 'Tinglysning',
             'key': 'tinglysning',
             'display_title': 'Tinglysning'
         },


         'Transport og kommunikation': {
             'subs': {

                  'Transport og kommunikation 1': {
                      'name': 'Jernbaner',
                      'id': 'Transport og kommunikation 1',
                      'key': '1',
                      'display_title': 'Transport og kommunikation 1 - Jernbaner'
                  },


                  'Transport og kommunikation 2': {
                      'name': 'Post, telegraf og telefon',
                      'id': 'Transport og kommunikation 2',
                      'key': '2',
                      'display_title': 'Transport og kommunikation 2 - Post, telegraf og telefon'
                  },


                  'Transport og kommunikation 3': {
                      'name': 'Luftfart',
                      'id': 'Transport og kommunikation 3',
                      'key': '3',
                      'display_title': 'Transport og kommunikation 3 - Luftfart'
                  },


                  'Transport og kommunikation 4': {
                      'name': 'Godstransport med motorkøretøjer',
                      'id': 'Transport og kommunikation 4',
                      'key': '4',
                      'display_title': 'Transport og kommunikation 4 - Godstransport med motorkøretøjer'
                  },


                  'Transport og kommunikation 5': {
                      'name': 'Transportformidling, herunder Speditører, Skibsmæglere, Stevedorer, Terminalvirksomheder',
                      'id': 'Transport og kommunikation 5',
                      'key': '5',
                      'display_title': 'Transport og kommunikation 5 - Transportformidling, herunder Speditører, Skibsmæglere, Stevedorer, Terminalvirksomheder'
                  },


                  'Transport og kommunikation 6': {
                      'name': 'Kollektiv persontransport',
                      'id': 'Transport og kommunikation 6',
                      'key': '6',
                      'display_title': 'Transport og kommunikation 6 - Kollektiv persontransport'
                  },


                  'Transport og kommunikation 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Transport og kommunikation 9',
                      'key': '9',
                      'display_title': 'Transport og kommunikation 9 - Andre spørgsmål'
                  }

             },
             'name': 'Transport og kommunikation (se Søfart)',
             'id': 'Transport og kommunikation',
             'key': 'transport_og_kommunikation',
             'display_title': 'Transport og kommunikation'
         },


         'Udlændinge': {
             'subs': {

                  'Udlændinge 1': {
                      'subs': {

                           'Udlændinge 1.1': {
                               'name': 'Betingelser',
                               'id': 'Udlændinge 1.1',
                               'key': '1',
                               'display_title': 'Udlændinge 1.1 - Betingelser'
                           },


                           'Udlændinge 1.2': {
                               'name': 'Pas og anden rejselegitimation',
                               'id': 'Udlændinge 1.2',
                               'key': '2',
                               'display_title': 'Udlændinge 1.2 - Pas og anden rejselegitimation'
                           },


                           'Udlændinge 1.3': {
                               'subs': {

                                    'Udlændinge 13.1': {
                                        'name': 'Schengenvisum',
                                        'id': 'Udlændinge 13.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 13.1 - Schengenvisum'
                                    },


                                    'Udlændinge 13.2': {
                                        'name': 'Andre visa',
                                        'id': 'Udlændinge 13.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 13.2 - Andre visa'
                                    },


                                    'Udlændinge 13.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 13.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 13.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Visum',
                               'id': 'Udlændinge 1.3',
                               'key': '3',
                               'display_title': 'Udlændinge 1.3 - Visum'
                           },


                           'Udlændinge 1.4': {
                               'name': 'Afvisning, herunder overførelse m.v.',
                               'id': 'Udlændinge 1.4',
                               'key': '4',
                               'display_title': 'Udlændinge 1.4 - Afvisning, herunder overførelse m.v.'
                           },


                           'Udlændinge 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Udlændinge 1.9',
                               'key': '9',
                               'display_title': 'Udlændinge 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Indrejse',
                      'id': 'Udlændinge 1',
                      'key': '1',
                      'display_title': 'Udlændinge 1 - Indrejse'
                  },


                  'Udlændinge 2': {
                      'subs': {

                           'Udlændinge 2.1': {
                               'subs': {

                                    'Udlændinge 21.1': {
                                        'name': 'Betingelser',
                                        'id': 'Udlændinge 21.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 21.1 - Betingelser'
                                    },


                                    'Udlændinge 21.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 21.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 21.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Asyl',
                               'id': 'Udlændinge 2.1',
                               'key': '1',
                               'display_title': 'Udlændinge 2.1 - Asyl'
                           },


                           'Udlændinge 2.2': {
                               'subs': {

                                    'Udlændinge 22.1': {
                                        'name': 'Ægtefælle/samlever',
                                        'id': 'Udlændinge 22.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 22.1 - Ægtefælle/samlever'
                                    },


                                    'Udlændinge 22.2': {
                                        'name': 'Børn',
                                        'id': 'Udlændinge 22.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 22.2 - Børn'
                                    },


                                    'Udlændinge 22.3': {
                                        'name': 'Efter EU-retlige regler',
                                        'id': 'Udlændinge 22.3',
                                        'key': '3',
                                        'display_title': 'Udlændinge 22.3 - Efter EU-retlige regler'
                                    },


                                    'Udlændinge 22.4': {
                                        'name': 'Inddragelse, forlængelse og bortfald',
                                        'id': 'Udlændinge 22.4',
                                        'key': '4',
                                        'display_title': 'Udlændinge 22.4 - Inddragelse, forlængelse og bortfald'
                                    },


                                    'Udlændinge 22.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 22.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 22.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Familiesammenføring',
                               'id': 'Udlændinge 2.2',
                               'key': '2',
                               'display_title': 'Udlændinge 2.2 - Familiesammenføring'
                           },


                           'Udlændinge 2.3': {
                               'subs': {

                                    'Udlændinge 23.1': {
                                        'name': 'EØS/EU-borgere',
                                        'id': 'Udlændinge 23.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 23.1 - EØS/EU-borgere'
                                    },


                                    'Udlændinge 23.2': {
                                        'name': 'Beskæftigelses- eller erhvervsmæssige hensyn',
                                        'id': 'Udlændinge 23.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 23.2 - Beskæftigelses- eller erhvervsmæssige hensyn'
                                    },


                                    'Udlændinge 23.3': {
                                        'name': 'Særlige ordninger',
                                        'id': 'Udlændinge 23.3',
                                        'key': '3',
                                        'display_title': 'Udlændinge 23.3 - Særlige ordninger'
                                    },


                                    'Udlændinge 23.4': {
                                        'name': 'Inddragelse, forlængelse og bortfald',
                                        'id': 'Udlændinge 23.4',
                                        'key': '4',
                                        'display_title': 'Udlændinge 23.4 - Inddragelse, forlængelse og bortfald'
                                    },


                                    'Udlændinge 23.5': {
                                        'name': 'Arbejdsgiveres forhold',
                                        'id': 'Udlændinge 23.5',
                                        'key': '5',
                                        'display_title': 'Udlændinge 23.5 - Arbejdsgiveres forhold'
                                    },


                                    'Udlændinge 23.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 23.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 23.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Beskæftigelse',
                               'id': 'Udlændinge 2.3',
                               'key': '3',
                               'display_title': 'Udlændinge 2.3 - Beskæftigelse'
                           },


                           'Udlændinge 2.4': {
                               'subs': {

                                    'Udlændinge 24.1': {
                                        'name': 'Betingelser',
                                        'id': 'Udlændinge 24.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 24.1 - Betingelser'
                                    },


                                    'Udlændinge 24.2': {
                                        'name': 'Inddragelse, forlængelse og bortfald',
                                        'id': 'Udlændinge 24.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 24.2 - Inddragelse, forlængelse og bortfald'
                                    },


                                    'Udlændinge 24.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 24.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 24.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Humanitær opholdstilladelse',
                               'id': 'Udlændinge 2.4',
                               'key': '4',
                               'display_title': 'Udlændinge 2.4 - Humanitær opholdstilladelse'
                           },


                           'Udlændinge 2.5': {
                               'subs': {

                                    'Udlændinge 25.1': {
                                        'name': 'Udsendelseshindret',
                                        'id': 'Udlændinge 25.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 25.1 - Udsendelseshindret'
                                    },


                                    'Udlændinge 25.2': {
                                        'name': 'Uledsaget mindreårig',
                                        'id': 'Udlændinge 25.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 25.2 - Uledsaget mindreårig'
                                    },


                                    'Udlændinge 25.3': {
                                        'name': 'Uddannelse og kurser m.v.',
                                        'id': 'Udlændinge 25.3',
                                        'key': '3',
                                        'display_title': 'Udlændinge 25.3 - Uddannelse og kurser m.v.'
                                    },


                                    'Udlændinge 25.4': {
                                        'name': 'Au pair, praktikant m.v.',
                                        'id': 'Udlændinge 25.4',
                                        'key': '4',
                                        'display_title': 'Udlændinge 25.4 - Au pair, praktikant m.v.'
                                    },


                                    'Udlændinge 25.5': {
                                        'name': 'Andre grundlag',
                                        'id': 'Udlændinge 25.5',
                                        'key': '5',
                                        'display_title': 'Udlændinge 25.5 - Andre grundlag'
                                    },


                                    'Udlændinge 25.6': {
                                        'name': 'Inddragelse, forlængelse og bortfald',
                                        'id': 'Udlændinge 25.6',
                                        'key': '6',
                                        'display_title': 'Udlændinge 25.6 - Inddragelse, forlængelse og bortfald'
                                    },


                                    'Udlændinge 25.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 25.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 25.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Andre opholdsgrundlag',
                               'id': 'Udlændinge 2.5',
                               'key': '5',
                               'display_title': 'Udlændinge 2.5 - Andre opholdsgrundlag'
                           },


                           'Udlændinge 2.6': {
                               'subs': {

                                    'Udlændinge 26.1': {
                                        'name': 'Betingelser',
                                        'id': 'Udlændinge 26.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 26.1 - Betingelser'
                                    },


                                    'Udlændinge 26.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 26.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 26.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Tidsubegrænset ophold',
                               'id': 'Udlændinge 2.6',
                               'key': '6',
                               'display_title': 'Udlændinge 2.6 - Tidsubegrænset ophold'
                           },


                           'Udlændinge 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Udlændinge 2.9',
                               'key': '9',
                               'display_title': 'Udlændinge 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Tidsbegrænset ophold',
                      'id': 'Udlændinge 2',
                      'key': '2',
                      'display_title': 'Udlændinge 2 - Tidsbegrænset ophold'
                  },


                  'Udlændinge 3': {
                      'subs': {

                           'Udlændinge 3.1': {
                               'name': 'Betingelser',
                               'id': 'Udlændinge 3.1',
                               'key': '1',
                               'display_title': 'Udlændinge 3.1 - Betingelser'
                           },


                           'Udlændinge 3.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Udlændinge 3.9',
                               'key': '9',
                               'display_title': 'Udlændinge 3.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Erhvervelse af fast ejendom',
                      'id': 'Udlændinge 3',
                      'key': '3',
                      'display_title': 'Udlændinge 3 - Erhvervelse af fast ejendom'
                  },


                  'Udlændinge 4': {
                      'subs': {

                           'Udlændinge 4.1': {
                               'name': 'Udrejsefrist',
                               'id': 'Udlændinge 4.1',
                               'key': '1',
                               'display_title': 'Udlændinge 4.1 - Udrejsefrist'
                           },


                           'Udlændinge 4.2': {
                               'name': 'Opsættende virkning',
                               'id': 'Udlændinge 4.2',
                               'key': '2',
                               'display_title': 'Udlændinge 4.2 - Opsættende virkning'
                           },


                           'Udlændinge 4.3': {
                               'name': 'Udsendelse',
                               'id': 'Udlændinge 4.3',
                               'key': '3',
                               'display_title': 'Udlændinge 4.3 - Udsendelse'
                           },


                           'Udlændinge 4.4': {
                               'name': 'Udrejsekontrol',
                               'id': 'Udlændinge 4.4',
                               'key': '4',
                               'display_title': 'Udlændinge 4.4 - Udrejsekontrol'
                           },


                           'Udlændinge 4.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Udlændinge 4.9',
                               'key': '9',
                               'display_title': 'Udlændinge 4.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Udrejse',
                      'id': 'Udlændinge 4',
                      'key': '4',
                      'display_title': 'Udlændinge 4 - Udrejse'
                  },


                  'Udlændinge 5': {
                      'subs': {

                           'Udlændinge 5.1': {
                               'subs': {

                                    'Udlændinge 51.1': {
                                        'name': 'Betinget udvisning',
                                        'id': 'Udlændinge 51.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 51.1 - Betinget udvisning'
                                    },


                                    'Udlændinge 51.2': {
                                        'name': 'Udvisning i øvrigt',
                                        'id': 'Udlændinge 51.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 51.2 - Udvisning i øvrigt'
                                    },


                                    'Udlændinge 51.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 51.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 51.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ved straffedom',
                               'id': 'Udlændinge 5.1',
                               'key': '1',
                               'display_title': 'Udlændinge 5.1 - Ved straffedom'
                           },


                           'Udlændinge 5.2': {
                               'subs': {

                                    'Udlændinge 52.1': {
                                        'name': 'Ulovligt ophold',
                                        'id': 'Udlændinge 52.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 52.1 - Ulovligt ophold'
                                    },


                                    'Udlændinge 52.2': {
                                        'name': 'Hensyn til statens sikkerhed',
                                        'id': 'Udlændinge 52.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 52.2 - Hensyn til statens sikkerhed'
                                    },


                                    'Udlændinge 52.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 52.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 52.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Administrativt bestemt udvisning',
                               'id': 'Udlændinge 5.2',
                               'key': '2',
                               'display_title': 'Udlændinge 5.2 - Administrativt bestemt udvisning'
                           },


                           'Udlændinge 5.3': {
                               'subs': {

                                    'Udlændinge 53.1': {
                                        'name': 'Indrejseforbuddets længde',
                                        'id': 'Udlændinge 53.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 53.1 - Indrejseforbuddets længde'
                                    },


                                    'Udlændinge 53.2': {
                                        'name': 'Ophævelse af indrejseforbud',
                                        'id': 'Udlændinge 53.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 53.2 - Ophævelse af indrejseforbud'
                                    },


                                    'Udlændinge 53.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 53.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 53.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Indrejseforbud',
                               'id': 'Udlændinge 5.3',
                               'key': '3',
                               'display_title': 'Udlændinge 5.3 - Indrejseforbud'
                           },


                           'Udlændinge 5.4': {
                               'subs': {

                                    'Udlændinge 54.1': {
                                        'name': 'På grund af væsentlige ændringer i udlændingens forhold',
                                        'id': 'Udlændinge 54.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 54.1 - På grund af væsentlige ændringer i udlændingens forhold'
                                    },


                                    'Udlændinge 54.2': {
                                        'name': 'Af helbredsmæssige grunde ved foranstaltningsdømte',
                                        'id': 'Udlændinge 54.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 54.2 - Af helbredsmæssige grunde ved foranstaltningsdømte'
                                    },


                                    'Udlændinge 54.3': {
                                        'name': 'Endnu ikke udsendte EØS/EU-borgere',
                                        'id': 'Udlændinge 54.3',
                                        'key': '3',
                                        'display_title': 'Udlændinge 54.3 - Endnu ikke udsendte EØS/EU-borgere'
                                    },


                                    'Udlændinge 54.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 54.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 54.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ophævelse af udvisning sket ved dom',
                               'id': 'Udlændinge 5.4',
                               'key': '4',
                               'display_title': 'Udlændinge 5.4 - Ophævelse af udvisning sket ved dom'
                           },


                           'Udlændinge 5.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Udlændinge 5.9',
                               'key': '9',
                               'display_title': 'Udlændinge 5.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Udvisning',
                      'id': 'Udlændinge 5',
                      'key': '5',
                      'display_title': 'Udlændinge 5 - Udvisning'
                  },


                  'Udlændinge 6': {
                      'subs': {

                           'Udlændinge 6.1': {
                               'subs': {

                                    'Udlændinge 61.1': {
                                        'name': 'Frihedsberøvelse',
                                        'id': 'Udlændinge 61.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 61.1 - Frihedsberøvelse'
                                    },


                                    'Udlændinge 61.2': {
                                        'name': 'Pasdeponering og sikkerhedsstillelse',
                                        'id': 'Udlændinge 61.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 61.2 - Pasdeponering og sikkerhedsstillelse'
                                    },


                                    'Udlændinge 61.3': {
                                        'name': 'Opholds- og flyttepåbud',
                                        'id': 'Udlændinge 61.3',
                                        'key': '3',
                                        'display_title': 'Udlændinge 61.3 - Opholds- og flyttepåbud'
                                    },


                                    'Udlændinge 61.4': {
                                        'name': 'Fremmøde- og meldepligt',
                                        'id': 'Udlændinge 61.4',
                                        'key': '4',
                                        'display_title': 'Udlændinge 61.4 - Fremmøde- og meldepligt'
                                    },


                                    'Udlændinge 61.5': {
                                        'name': 'Forhold i forbindelse med indkvartering',
                                        'id': 'Udlændinge 61.5',
                                        'key': '5',
                                        'display_title': 'Udlændinge 61.5 - Forhold i forbindelse med indkvartering'
                                    },


                                    'Udlændinge 61.6': {
                                        'name': 'Diverse ydelser m.v.',
                                        'id': 'Udlændinge 61.6',
                                        'key': '6',
                                        'display_title': 'Udlændinge 61.6 - Diverse ydelser m.v.'
                                    },


                                    'Udlændinge 61.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 61.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 61.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ved indrejse, processuelt ophold og udrejse m.v.',
                               'id': 'Udlændinge 6.1',
                               'key': '1',
                               'display_title': 'Udlændinge 6.1 - Ved indrejse, processuelt ophold og udrejse m.v.'
                           },


                           'Udlændinge 6.2': {
                               'subs': {

                                    'Udlændinge 62.1': {
                                        'name': 'Varetægtsfængsling efter udlændingeloven',
                                        'id': 'Udlændinge 62.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 62.1 - Varetægtsfængsling efter udlændingeloven'
                                    },


                                    'Udlændinge 62.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 62.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 62.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Ved strafbare forhold',
                               'id': 'Udlændinge 6.2',
                               'key': '2',
                               'display_title': 'Udlændinge 6.2 - Ved strafbare forhold'
                           },


                           'Udlændinge 6.3': {
                               'subs': {

                                    'Udlændinge 63.1': {
                                        'name': 'Arbejde uden fornøden tilladelse',
                                        'id': 'Udlændinge 63.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 63.1 - Arbejde uden fornøden tilladelse'
                                    },


                                    'Udlændinge 63.2': {
                                        'name': 'Andre overtrædelser',
                                        'id': 'Udlændinge 63.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 63.2 - Andre overtrædelser'
                                    },


                                    'Udlændinge 63.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 63.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 63.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Strafbare overtrædelser af udlændingeloven',
                               'id': 'Udlændinge 6.3',
                               'key': '3',
                               'display_title': 'Udlændinge 6.3 - Strafbare overtrædelser af udlændingeloven'
                           },


                           'Udlændinge 6.4': {
                               'subs': {

                                    'Udlændinge 64.1': {
                                        'name': 'Udenlandske statsborgere',
                                        'id': 'Udlændinge 64.1',
                                        'key': '1',
                                        'display_title': 'Udlændinge 64.1 - Udenlandske statsborgere'
                                    },


                                    'Udlændinge 64.2': {
                                        'name': 'Danske statsborgere',
                                        'id': 'Udlændinge 64.2',
                                        'key': '2',
                                        'display_title': 'Udlændinge 64.2 - Danske statsborgere'
                                    },


                                    'Udlændinge 64.9': {
                                        'name': 'Andre spørgsmål',
                                        'id': 'Udlændinge 64.9',
                                        'key': '9',
                                        'display_title': 'Udlændinge 64.9 - Andre spørgsmål'
                                    }

                               },
                               'name': 'Administrativt bestemt udlevering',
                               'id': 'Udlændinge 6.4',
                               'key': '4',
                               'display_title': 'Udlændinge 6.4 - Administrativt bestemt udlevering'
                           },


                           'Udlændinge 6.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Udlændinge 6.9',
                               'key': '9',
                               'display_title': 'Udlændinge 6.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Forskellige foranstaltninger m.v.',
                      'id': 'Udlændinge 6',
                      'key': '6',
                      'display_title': 'Udlændinge 6 - Forskellige foranstaltninger m.v.'
                  },


                  'Udlændinge 7': {
                      'subs': {

                           'Udlændinge 7.1': {
                               'name': 'Erhvervelse',
                               'id': 'Udlændinge 7.1',
                               'key': '1',
                               'display_title': 'Udlændinge 7.1 - Erhvervelse'
                           },


                           'Udlændinge 7.2': {
                               'name': 'Frakendelse',
                               'id': 'Udlændinge 7.2',
                               'key': '2',
                               'display_title': 'Udlændinge 7.2 - Frakendelse'
                           },


                           'Udlændinge 7.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Udlændinge 7.9',
                               'key': '9',
                               'display_title': 'Udlændinge 7.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Indfødsret',
                      'id': 'Udlændinge 7',
                      'key': '7',
                      'display_title': 'Udlændinge 7 - Indfødsret'
                  },


                  'Udlændinge 8': {
                      'subs': {

                           'Udlændinge 8.1': {
                               'name': 'Kompetencespørgsmål',
                               'id': 'Udlændinge 8.1',
                               'key': '1',
                               'display_title': 'Udlændinge 8.1 - Kompetencespørgsmål'
                           },


                           'Udlændinge 8.2': {
                               'name': 'Sagens gang',
                               'id': 'Udlændinge 8.2',
                               'key': '2',
                               'display_title': 'Udlændinge 8.2 - Sagens gang'
                           },


                           'Udlændinge 8.3': {
                               'name': 'Sagens afgørelse',
                               'id': 'Udlændinge 8.3',
                               'key': '3',
                               'display_title': 'Udlændinge 8.3 - Sagens afgørelse'
                           },


                           'Udlændinge 8.4': {
                               'name': 'Rekurs',
                               'id': 'Udlændinge 8.4',
                               'key': '4',
                               'display_title': 'Udlændinge 8.4 - Rekurs'
                           },


                           'Udlændinge 8.5': {
                               'name': 'Domstolsprøvelse',
                               'id': 'Udlændinge 8.5',
                               'key': '5',
                               'display_title': 'Udlændinge 8.5 - Domstolsprøvelse'
                           },


                           'Udlændinge 8.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Udlændinge 8.9',
                               'key': '9',
                               'display_title': 'Udlændinge 8.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Sagsbehandling',
                      'id': 'Udlændinge 8',
                      'key': '8',
                      'display_title': 'Udlændinge 8 - Sagsbehandling'
                  },


                  'Udlændinge 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Udlændinge 9',
                      'key': '9',
                      'display_title': 'Udlændinge 9 - Andre spørgsmål'
                  }

             },
             'name': 'Udlændinge',
             'id': 'Udlændinge',
             'key': 'udlandinge',
             'display_title': 'Udlændinge'
         },


         'Veje og vand': {
             'subs': {

                  'Veje og vand 1': {
                      'subs': {

                           'Veje og vand 1.1': {
                               'name': 'Offentlige veje',
                               'id': 'Veje og vand 1.1',
                               'key': '1',
                               'display_title': 'Veje og vand 1.1 - Offentlige veje'
                           },


                           'Veje og vand 1.2': {
                               'name': 'Private veje',
                               'id': 'Veje og vand 1.2',
                               'key': '2',
                               'display_title': 'Veje og vand 1.2 - Private veje'
                           },


                           'Veje og vand 1.3': {
                               'name': 'Stier',
                               'id': 'Veje og vand 1.3',
                               'key': '3',
                               'display_title': 'Veje og vand 1.3 - Stier'
                           },


                           'Veje og vand 1.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Veje og vand 1.9',
                               'key': '9',
                               'display_title': 'Veje og vand 1.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Veje',
                      'id': 'Veje og vand 1',
                      'key': '1',
                      'display_title': 'Veje og vand 1 - Veje'
                  },


                  'Veje og vand 2': {
                      'subs': {

                           'Veje og vand 2.1': {
                               'name': 'Naturlige vandløb og søer',
                               'id': 'Veje og vand 2.1',
                               'key': '1',
                               'display_title': 'Veje og vand 2.1 - Naturlige vandløb og søer'
                           },


                           'Veje og vand 2.2': {
                               'name': 'Vandafledning, kloakker, dræning og lignende, se også Miljøret 1.3',
                               'id': 'Veje og vand 2.2',
                               'key': '2',
                               'display_title': 'Veje og vand 2.2 - Vandafledning, kloakker, dræning og lignende, se også Miljøret 1.3'
                           },


                           'Veje og vand 2.3': {
                               'name': 'Grundvand',
                               'id': 'Veje og vand 2.3',
                               'key': '3',
                               'display_title': 'Veje og vand 2.3 - Grundvand'
                           },


                           'Veje og vand 2.4': {
                               'name': 'Vandværker og vandledninger',
                               'id': 'Veje og vand 2.4',
                               'key': '4',
                               'display_title': 'Veje og vand 2.4 - Vandværker og vandledninger'
                           },


                           'Veje og vand 2.9': {
                               'name': 'Andre spørgsmål',
                               'id': 'Veje og vand 2.9',
                               'key': '9',
                               'display_title': 'Veje og vand 2.9 - Andre spørgsmål'
                           }

                      },
                      'name': 'Vand',
                      'id': 'Veje og vand 2',
                      'key': '2',
                      'display_title': 'Veje og vand 2 - Vand'
                  },


                  'Veje og vand 9': {
                      'name': 'Andre spørgsmål',
                      'id': 'Veje og vand 9',
                      'key': '9',
                      'display_title': 'Veje og vand 9 - Andre spørgsmål'
                  }

             },
             'name': 'Veje og vand',
             'id': 'Veje og vand',
             'key': 'veje_og_vand',
             'display_title': 'Veje og vand'
         }

};
