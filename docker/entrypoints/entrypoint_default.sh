#!/bin/bash

# Set environments and dirs, but only output stuff if something goes
# wrong.
source docker/bin/set_env_and_make_dirs.sh > /tmp/set-env-$$.log 2>&1
status=$?
if [[ $status -ne 0 ]]; then
    cat /tmp/set-env-$$.log
    rm /tmp/set-env-$$.log
    exit $status
fi

rm /tmp/set-env-$$.log

set -e

if [[ -z "${WAIT_FOR_DB_TIMER}" ]]; then
    WAIT_FOR_DB_TIMER=10
fi

# TODO Cleaner directory handling
pushd /var/www/www.ombudsmanden.dk/ >/dev/null

if [[ -z "$BARE_MODE" ]]; then
    echo "Testing database connection for ${WAIT_FOR_DB_TIMER} seconds"
    perl docker/bin/test_database_connection.pl ${WAIT_FOR_DB_TIMER}

    # Run migrations
    perl /var/www/obvius/db/migrate.pl --confname ombud

    if [[ ! -z "$CLEAR_CACHE" ]]; then
        echo "Clearing apache cache"
        perl docker/bin/clear_apachecache.pl
    fi
else
    echo "Running in bare mode, skipping database operations"
fi

popd >/dev/null

if [[ ! -z "$CYCLE_DOCTYPES" ]]; then
    echo "Cycling doctypes"
    yes | /var/www/www.ombudsmanden.dk/db/cycle_doctypes_etc.sh >/dev/null
fi

#if [[ ! -z "$IMPORT_PATH" ]]; then
#    /var/www/www.ku.dk/bin/import_dumps.sh "$IMPORT_PATH"
#fi

if [[ ! -z "$UPDATE_SOLR" ]]; then
    echo "Updating SOLR"
    perl /var/www/www.ombudsmanden.dk/bin/new_solr_build_index.pl \
	    --core http://solr:8983/solr/ombud2017 --confname ombud -p -q 1 99999 \
	    && curl 'http://solr:8983/solr/ombud2017/update?commit=true' \
	    && perl /var/www/www.ombudsmanden.dk/bin/new_solr_cleanup_index.pl
fi

# Setup a certificate for encrypting emails
if [[ ! -z "${OBVIUS_CONFIG_OMBUD_FORM_SECURE_MAIL_FROM}" && \
      ! -e /var/www/www.ombudsmanden.dk/certificates/${OBVIUS_CONFIG_OMBUD_FORM_SECURE_MAIL_FROM} ]]; then
    /var/www/www.ombudsmanden.dk/bin/make_selfsigned_email_certificate.sh ${OBVIUS_CONFIG_OMBUD_FORM_SECURE_MAIL_FROM} --force
    chown -R www-data /var/www/www.ombudsmanden.dk/certificates/${OBVIUS_CONFIG_OMBUD_FORM_SECURE_MAIL_FROM}
fi

exec "$@"
