#!/bin/bash

# Set environments and dirs, but only output stuff if something goes
# wrong.
source docker/bin/set_env_and_make_dirs.sh > /tmp/set-env-$$.log 2>&1
status=$?
if [[ $status -ne 0 ]]; then
    cat /tmp/set-env-$$.log
    rm /tmp/set-env-$$.log
    exit $status
fi

rm /tmp/set-env-$$.log

set -e

if [[ ! -f /var/www/www.ombudsmanden.dk/var/queue_last_processed.time ]]; then
  touch /var/www/www.ombudsmanden.dk/var/queue_last_processed.time
fi
chown www-data:www-data /var/www/www.ombudsmanden.dk/var/queue_last_processed.time

# Hand off to the CMD
exec "$@"
