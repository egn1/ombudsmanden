#!/usr/bin/perl
use Obvius;
use DBI;
use strict;
use warnings;

my $attempts = $ARGV[0];
my $config = Obvius::Config->new('ombud');

for (1..$attempts) {
    eval {
        DBI->connect(
            $config->param('dsn'),
            $config->param('normal_db_login'),
            $config->param('normal_db_passwd'),
            {RaiseError => 1}
        );
    };
    if($@) {
        sleep(1);
    } else {
        exit 0;
    }
}
print STDERR "Failed to establish database connection\n";
print STDERR $@;
exit 1;
