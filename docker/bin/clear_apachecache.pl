#!/usr/bin/perl

use Obvius;
use Obvius::Config;
use strict;
use warnings;

my $obvius = Obvius->new(Obvius::Config->new('ombud'));
my $table_name = $obvius->config->param('mysql_apachecache_table');
$obvius->dbh->do("DELETE FROM $table_name");
