#!/bin/bash
if [[ -z "${OBVIUS_CONFNAME}" ]]; then
    echo "OBVIUS_CONFNAME must be set." 1>&2
    return 1
fi
UPPERCASE_CONFNAME=$(echo $OBVIUS_CONFNAME | tr a-z A-Z | tr -d ' ')

if [[ -z "${OBVIUS_ENVIRONMENT}" ]]; then
    echo "OBVIUS_ENVIRONMENT must be set. Valid values are 'development', 'test' and 'production'" 1>&2
    return 1
fi

if [[ -f "/etc/obvius/${OBVIUS_CONFNAME}-${OBVIUS_ENVIRONMENT}.conf" ]]; then
    cp "/etc/obvius/${OBVIUS_CONFNAME}-${OBVIUS_ENVIRONMENT}.conf" "/etc/obvius/${OBVIUS_CONFNAME}-environment.conf"
else
    echo "No such environment config file /etc/obvius/${OBVIUS_CONFNAME}-${OBVIUS_ENVIRONMENT}.conf" 1>&2
    return 1
fi

OBVIUS_CONFIG_DOCKERHOST_HOSTNAME="OBVIUS_CONFIG_${UPPERCASE_CONFNAME}_DOCKERHOST_HOSTNAME"
if [[ -z "${!OBVIUS_CONFIG_DOCKERHOST_HOSTNAME}" ]] && [[ -f /etc/dockerhostname ]]; then
  declare "OBVIUS_CONFIG_${UPPERCASE_CONFNAME}_DOCKERHOST_HOSTNAME=`cat /etc/dockerhostname`"
  export "${OBVIUS_CONFIG_DOCKERHOST_HOSTNAME}"
fi

# Append environment variables to /etc/obvius/${OBVIUS_CONFNAME}-environment.conf
cat >> "/etc/obvius/${OBVIUS_CONFNAME}-environment.conf" << EOT

#################################################
# Local config added from environment variables #
#################################################
EOT
env | grep "OBVIUS_CONFIG_${UPPERCASE_CONFNAME}_" | sed -e "s/^OBVIUS_CONFIG_${UPPERCASE_CONFNAME}_//" | sort >> "/etc/obvius/${OBVIUS_CONFNAME}-environment.conf"

# Add Apache2 environment variables to script that can be sourced from /etc/apache2/envvars
if [[ -f /var/www/www.ombudsmanden.dk/conf/default_apache_environment.env ]]; then
    grep '^export ' /var/www/www.ombudsmanden.dk/conf/default_apache_environment.env | sed -e 's/export //;s/=.*//' | while read varname; do
        varvalue="${!varname}";
        if [[ ! -z "${varvalue}" ]]; then
            echo "export ${varname}=${varvalue}" >> /etc/apache2/envvars_obvius.env
        fi;
    done
fi

# Make a file with the full config
TMP_CONFIG_FILE=/tmp/_temp_full_config.txt
perl -MObvius::Config -e 'print Obvius::Config->new("ombud")->get_normalized_text' > /tmp/_temp_full_config.txt

function dir_config_value {
    local CONF_KEY="$1"
    local VALUE=$(grep "^${CONF_KEY} = " "${TMP_CONFIG_FILE}" | cut -d " " -f 3- | sed -e 's/\/$//')
    echo $VALUE
}

DOCS_DIR=$(dir_config_value "docs_dir")
DOWNLOADS_DIR="$(dir_config_value "download_folder")"
# Ensure DOWNLOADS_DIR starts with a slash
DOWNLOADS_DIR=$(echo "${DOWNLOADS_DIR}" | sed -e 's/^\([^\/]\)/\/\1/')
# And prefix DOWNLOADS_DIR with DOCS_DIR
DOWNLOADS_DIR="${DOCS_DIR}${DOWNLOADS_DIR}"
UPLOAD_DIR=$(dir_config_value "upload_dir")
if [[ "${UPLOAD_DIR}" == "" ]]; then
    UPLOAD_DIR="${DOCS_DIR}/upload"
fi

ERROR_DIR=$(dir_config_value "error_dir")
if [[ "${ERROR_DIR}" == "" ]]; then
    ERROR_DIR="${DOCS_DIR}/errors"
fi

FORM_UPLOAD_DIR=$(dir_config_value "form_upload_dir")
STATIC_RSS_DIR=$(dir_config_value "static_rss_dir")
GEOIP_DIR=$(dir_config_value "geoip_dir")
IMPORTEXPORT_DIR=$(dir_config_value "importexport_dir")

rm "${TMP_CONFIG_FILE}"

function install_dir {
    local DIR="$1"
    local TARGET="$2"

    if [[ -e "${DIR}" ]]; then
        return;
    fi

    if [[ "$TARGET" == "" ]]; then
        TARGET="$DIR"
    fi

    if [[ "$TARGET" == "$DIR" ]]; then
        echo "Setting up empty www-data dir in ${DIR}"
        install -g www-data -o www-data -d "${DIR}"
    else
        if [[ ! -e "${TARGET}" ]]; then
            echo "Setting up empty www-data dir in ${TARGET}"
            install -g www-data -o www-data -d "${TARGET}"
        fi
        echo "Symlinking ${DIR} to ${TARGET}"
        ln -s "${TARGET}" "$DIR"
    fi
}

install_dir "/var/www/www.ombudsmanden.dk/docs/downloads" "${DOWNLOADS_DIR}"
install_dir "/var/www/www.ombudsmanden.dk/docs/upload" "${UPLOAD_DIR}"
install_dir "/var/www/www.ombudsmanden.dk/var/form_uploads" "${FORM_UPLOAD_DIR}"
install_dir "/var/www/www.ombudsmanden.dk/docs/rss_files" "${STATIC_RSS_DIR}"
install_dir "/var/www/www.ombudsmanden.dk/docs/errors" "${ERROR_DIR}"
install_dir "/var/www/www.ombudsmanden.dk/var/docker_shared" "${DOCKER_SHARED_DIR}"

# Import / export dirs
install_dir "/var/www/www.ombudsmanden.dk/var/importexport" ${IMPORTEXPORT_DIR}
install_dir "/var/www/www.ombudsmanden.dk/var/importexport/local"
install_dir "/var/www/www.ombudsmanden.dk/var/importexport/remote"


if [[ -z "${OBVIUS_LOGNAME}" ]]; then
    OBVIUS_LOGNAME=`hostname`
    if [[ -z "${OBVIUS_LOGNAME}" ]]; then
        OBVIUS_LOGNAME="default"
    fi
    export OBVIUS_LOGNAME
fi

if [[ -z "${APACHE_SERVER_ADMIN}" ]]; then
    APACHE_SERVER_ADMIN="obviusadmin@magenta.dk"
    export APACHE_SERVER_ADMIN
fi

if [[ ! -e /var/www/www.ombudsmanden.dk/logs/${OBVIUS_LOGNAME} ]]; then
    install -g www-data -o www-data -d /var/www/www.ombudsmanden.dk/logs/${OBVIUS_LOGNAME}
fi

if [[ ! -e /var/www/www.ombudsmanden.dk/var/document_cache ]]; then
    install -g www-data -o www-data -d /var/www/www.ombudsmanden.dk/var/document_cache
fi

if [[ ! -e /var/www/www.ombudsmanden.dk/docs/cache ]]; then
    ln -s ../var/document_cache /var/www/www.ombudsmanden.dk/docs/cache
fi

if [[ ! -e /var/www/www.ombudsmanden.dk/var/obj ]]; then
    install -g www-data -o www-data -d /var/www/www.ombudsmanden.dk/var/obj
fi

chown -R www-data:www-data /var/www/www.ombudsmanden.dk/var

# Check that hostmap file exists
if [[ ! -e /var/www/www.ombudsmanden.dk/var/docker_shared/hostmap.conf ]]; then
    install -g www-data -o www-data /dev/null /var/www/www.ombudsmanden.dk/var/docker_shared/hostmap.conf
fi

# Make sure /var/www/www.ombudsmanden.dk/conf/hostmap.conf is a hostmap pointing to the file above.
if [[ ! -L /var/www/www.ombudsmanden.dk/conf/hostmap.conf ]]; then
    if [[ -e /var/www/www.ombudsmanden.dk/conf/hostmap.conf ]]; then
        rm /var/www/www.ombudsmanden.dk/conf/hostmap.conf
    fi
    ln -s /var/www/www.ombudsmanden.dk/var/docker_shared/hostmap.conf /var/www/www.ombudsmanden.dk/conf/hostmap.conf
    chown www-data:www-data /var/www/www.ombudsmanden.dk/conf/hostmap.conf
fi

# Make sure log directory exists
if [[ ! -d "/var/www/www.ombudsmanden.dk/logs/${OBVIUS_LOGNAME}" ]]; then
    mkdir -p "/var/www/www.ombudsmanden.dk/logs/${OBVIUS_LOGNAME}"
fi

echo "ServerName ${OBVIUS_LOGNAME}" > /var/www/www.ombudsmanden.dk/conf/names.conf
