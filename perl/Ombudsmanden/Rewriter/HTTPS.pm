package Ombudsmanden::Rewriter::HTTPS;

use strict;
use warnings;

use WebObvius::Rewriter::RewriteRule qw(REDIRECT PASSTHROUGH);


our @ISA = qw(WebObvius::Rewriter::RewriteRule);

sub setup {
    my ($this, $rewriter) = @_;

    my $config = $rewriter->{config};
    $this->{roothost} = $config->param('roothost');

    # Needed in both admin and public
    $this->{is_admin_rewriter} = 1;
}

sub rewrite {
    my ($this, %args) = @_;

    return undef unless($args{is_front_server});

    my $https = $args{https} || 'off';
    return undef if($https eq 'on');

    if($args{uri} =~ m!^/admin/!) {
        return (REDIRECT, 'https://' . $this->{roothost} . $args{uri});
    } elsif($args{hostname} =~ m!^(ssl|ssl-born)!) {
        return (REDIRECT, 'https://' . $args{hostname} . $args{uri});
    }

    return undef;
}

1;