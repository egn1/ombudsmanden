package Ombudsmanden::Rewriter::SubSites;

use Obvius::Hostmap;
use WebObvius::Rewriter::RewriteRule qw(REDIRECT REWRITE);

our @ISA = qw(WebObvius::Rewriter::RewriteRule);

sub setup {
    my ($this, $rewriter) = @_;

    # Need to rewrite in both admin and public
    $this->{is_admin_rewriter} = 1;

    my $config = $rewriter->{config};

    $this->{roothost} = $config->param('roothost');
    die "Trying to use subsite rewrites on a site with no roothost" unless($this->{roothost});

    my $map_file = $config->param('hostmap_file');
    die "No 'hostmap_file' defined in config: Can't rewrite subsites" unless($map_file);

    $this->{hostmap} = Obvius::Hostmap->create_hostmap( $map_file, $this->{roothost}, debug => 1 );
}

sub hostmap {
    return shift->{hostmap};
}

sub rewrite {
    my ($this, %args) = @_;

    return undef unless($args{is_front_server});

    my $roothost = $this->{roothost};
    my $hostname = lc($args{hostname}) || '';

    #Make sure hostmap is correctly loaded:
    $this->hostmap->get_hostmap;

    my $subsite_uri = $this->hostmap->host_to_uri($args{hostname}) || '';
    my $protocol = ($args{https} || '') eq 'on' ? 'https' : 'http';

    # Stage one: Redirect admin requests on any host to the proper URI on
    # the roothost:
    if($args{uri} =~ m!^/admin(/(.*)|$)!) {
        # If already on the root we don't need any more subsite rewriting
        return undef if($hostname eq $roothost and $protocol eq 'https');

        # Force https:
        $protocol = 'https';

        my $rest = $1 || '/';
        if($subsite_uri) {
            $subsite_uri =~ s!/$!!;
            return (REDIRECT, "$protocol://$roothost/admin$subsite_uri$rest");
        } else {
            return (REDIRECT, "$protocol://$roothost/admin$rest");
        }
    }

    # Skip system URLs
    return undef if($args{uri} =~ m!^/system/!);

    # Store original URI for later use
    my $orig_uri = $args{uri};

    my $rewritten = 0;

    # Rewrite according to found subsite:
    if($subsite_uri) {
        $args{uri} =~ s!^/!!;
        $args{uri} = "$subsite_uri$args{uri}";
        $rewritten = 1;
    }


    # Now, redirect to the correct hostname if we're not already on it:
    my ($new_uri, $new_host, $subsiteuri) = $this->hostmap->translate_uri(
        $args{uri},
        $hostname
    );

    my $force_redirect = 0;

    # Force https for klageskema and ssl subsites
    if($protocol eq 'http' &&
       (
            $args{uri} =~ m{/klageskema(/|$)} ||
            ($new_host || $hostname) =~ m{^ssl(-born)\.}
       )
    ) {
        $force_redirect = 1;
        if($new_uri =~ m{^/}) {
            $new_uri = "https://" . $hostname . $orig_uri;
        } else {
            $new_uri =~ s{^http://}{https://};
        }
    }

    # TODO: Handle SSL redirects here? Or maybe in Obvius::Hostmap?
    if(($new_host and $new_host ne $hostname) || $force_redirect) {
        return (REDIRECT, $new_uri);
    }

    if($rewritten) {
        return (REWRITE, $args{uri});
    } else {
        return undef;
    }
}

1;
