package Ombudsmanden::ExportImport;

use strict;
use warnings;
use utf8;

use URI::Escape;

use base 'Obvius::ExportImport';

# Use "Standard" doctype for creating parent documents
sub folder_doctypename { return "Standard" }

# Ombudsmanden have no database representation of subsites
sub list_subsites { return [] }
sub export_subsites {}
sub import_subsites {}

# Utility method used by docidify
sub hostmap {
    my ($self) = @_;

    $self->{hostmap} ||= Obvius::Hostmap->new_with_obvius(
        $self->obvius
    );

    return $self->{hostmap};
}

# Local docidify method
sub docidify {
    my ($self, $url) = @_;

    my $original_url = $url;

    my $obvius = $self->obvius;

    # Remove achor and querystring:
    $url =~ s!#.*$!!;
    $url =~ s!\?.*$!!;

    # This is the part of the URL we want to replace with /<docid>.docid. Save it.
    my $replace_part = $url;

    # If we're on any admin host, remove hostname and admin part
    $url =~ s!^https?://[^/]+/admin/!/!;

    # If the URL does not start with a slash, try to do hostmap url-to-uri conversion
    if($url !~ m{^/}) {
        my $hostmap = $self->hostmap;
        # Translate full URLs pointing to other subsites:
        if(my $translated = $hostmap->url_to_uri($url)) {
            $url = $translated;
        }
    }

    if($url =~ m!^/!) {
        my $utf8_url = Obvius::CharsetTools::mixed2utf8(uri_unescape($url));
        my @urls = ($utf8_url);
        if($url =~ m{^/admin/}) {
            my $non_admin_url = $utf8_url;
            $non_admin_url =~ s{^/admin/}{/};
            push(@urls, $non_admin_url);
        }
        foreach my $url (@urls) {
            if(my $d = $obvius->lookup_document($url)) {
                $url = "/" . $d->Id . ".docid";
                return $url;
            }
        }
    }

    return $original_url;
}

1;
