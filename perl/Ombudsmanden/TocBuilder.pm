package Ombudsmanden::TocBuilder;
use strict;
use warnings;

use base "Exporter";
our @EXPORT_OK = qw(reset_toc add_heading output_toc inject_toc_links);
our @EXPORT = @EXPORT_OK;

use HTML::Parser;

my $top;
my $current_pos;
my $outofband;

sub reset_toc {
    $top = {
        ref_id => undef,
        title => "",
        level => 0,
        display_number => undef,
        sub_entries => [],
        next_numbered_child => 1,
        parent => undef,
    };
    $current_pos = $top;
    $outofband = "AA";
}

reset_toc();

sub make_node {
    my ($parent, $title, $is_numbered) = @_;

    my $ref_id;
    my $display_number;

    if ($is_numbered eq "numbered") {
        my $number = $parent->{next_numbered_child}++;
        $ref_id = join("-", ($parent->{ref_id} || ("chapter"), $number));
        $display_number = join(".", ($parent->{display_number} || (), $number));
    } elsif ($is_numbered eq "outofband") {
        $ref_id = $outofband++;
        $display_number = "";
    } else {
        # empty/invisible intermediate node
    }

    my $node = {
        ref_id => $ref_id,
        display_number => $display_number,
        title => $title,
        level => $parent->{level} + 1,
        sub_entries => [],
        next_numbered_child => 1,
        parent => $parent,
    };
    push(@{$parent->{sub_entries}}, $node);
    return $node;
}

sub add_heading {
    my ($level, $title, $is_numbered) = @_;
    # Add missing empty levels
    if (defined($level)) {
        while (defined($current_pos->{level}) && $current_pos->{level} < ($level - 1)) {
            $current_pos = make_node($current_pos, "", "empty");
        }
        while (defined($current_pos->{level}) && $current_pos->{level} >= $level) {
            $current_pos = $current_pos->{parent};
        }
    }
    $current_pos = make_node($current_pos, $title, $is_numbered);
    return $current_pos;
}

sub output_node {
    my ($incl_level, $node) = @_;
    # maximum toc depth
    if ($node->{level} > $incl_level) {
        return;
    }
    my @format_li = ('<a href="#%s" class="toc-level-%s"><span>%s</span></a>', qw(ref_id level title));
    my @res;

    if ($node->{title}) {
        $node->{title} =~ s/\s+/ /g;
        push(@res, sprintf($format_li[0], map { $node->{$_} } splice(@format_li,1)));
    }
    if ($node->{level} < $incl_level && @{$node->{sub_entries}}) {
        push(@res, '<ol class="slide-in-content toc-nav-list">');
        for my $subnode (@{$node->{sub_entries}}) {
            push(@res, output_node($incl_level, $subnode));
        }
        push(@res, "</ol>");
    }
    if ($node ne $top) {
        unshift(@res, "<li>");
        push(@res, "</li>");
    }
    my $indent = ("    " x $node->{level});
    return "\n$indent" . join("\n$indent", @res);
}

sub output_toc {
    if (!@{$top->{sub_entries}}) {
        return "";
    }
    return "\n" . output_node(1, $top) . "\n";
    #    return "\n<ol>\n" . output_node($top) . "\n</ol>\n";
}

sub inject_toc_links {
    my($html) = @_;
    my $input = $_;
    my $toctext = 0;
    my $copyhtml = "";
    my $auxsecid = "A";
    my $current_node;
    my $desired_level;

    reset_toc();
    my $parser = HTML::Parser->new(
        api_version => 3,
        start_h => [
            sub {
                my($this, $tagname, $text, $attr, $attrseq) = @_;
                if ($tagname =~ m/^h([123456])/) {
                    my $l = $1; # FOR PARAMETRIZED SELECTION CHANGE LOGIC HERE ^ ->

                    if (defined($attr->{class}) && $attr->{class} =~ m/(spot-title-toc|numbered)/) {

                        if ($l && (!$desired_level || $desired_level > $l)) {
                            $desired_level = $l;
                        }
                        my $abs_level = $l - $desired_level + 1;

                        if (!$attr->{class}) {
                            $text =~ s/>/class="">/;
                        }
                        $text =~ s/class="(.*)"/class="$1 numbered-heading-level-$abs_level"/;

                        $current_node = add_heading($abs_level, "", "numbered");
                        $toctext = 1;
                        my $target = $current_node->{ref_id};
                        my $insert = "";
                        if (!($attr->{class} =~ m/spot-title-toc/)) {
                            $insert = $current_node->{display_number};
                        }
                        $text .= "<span class=\"section-anchor\" id=\"$target\">" . $insert . "</span> ";

                    }
                } elsif ($toctext && $tagname eq "span") {
                    # clear letter from title, if this is a spot-title-toc
                    if ($attr->{class} =~ m/spot-title-text/) {
                        $current_node->{title} = "";
                    }
                }
                $copyhtml .= $text;
            },
            "self, tagname, text, attr, attrseq"
        ],
        end_h => [
            sub {
                my ($this, $tagname, $text, $attr)=@_;
                $text ||= "";
                $attr ||= "";
                if ($tagname =~ m/h(\d)/) {
                    $toctext = 0;
                }
                $copyhtml .= $text;
            },
            "self, tagname, text, attr"
        ],
        text_h => [
            sub {
                my ($this, $text) = @_;
                if ($toctext) {
                    $current_node->{title} .= $text;
                }
                $copyhtml .= $text;
            },
            "self, text"
        ]
    );
    $parser->parse($input);
    $parser->eof();
    my $tableofcontents = output_toc();
    $copyhtml =~ s/__TOC_PLACEHOLDER__/$tableofcontents/;
    return $copyhtml;
}
