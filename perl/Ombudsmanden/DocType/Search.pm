package Ombudsmanden::DocType::Search;

########################################################################
#
# Search.pm - local overrides for the Search doctype
#
# Copyright (C) 2005 Magenta ApS, Denmark (http://www.magenta-aps.dk/)
#
# Author: Adam Sj�gren (asjo@magenta-aps.dk)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
#
########################################################################

# $Id: Search.pm,v 1.1.2.1 2010/10/18 13:15:47 jubk Exp $

use strict;
use warnings;

use Obvius::DocType::Search;

our @ISA = qw( Obvius::DocType::Search );
our ( $VERSION ) = '$Revision: 1.1.2.1 $ ' =~ /\$Revision:\s+([^\s]+)/;

sub get_search_words {
    my ($this, $words)=@_;

    return () unless ($words);

    if ($words =~ /�/) {
	$words = lc(utf8($words)->latin1);
    } else {
	$words = lc($words);
    }

    return ($words =~ /\b([\w.]+[*]?)/g); # Notice the added '.'
}

1;
__END__


=head1 NAME

Ombudsmanden::DocType::Search - local modifications of the Search doctype

=head1 SYNOPSIS

  use'd automatically on Ombudsmanden.

=head1 DESCRIPTION

Local version of the get_search_words-function.

=head1 AUTHOR

Adam Sj�gren, E<lt>asjo@magenta-aps.dkE<gt>

=head1 SEE ALSO

L<Obvius::DocType::Search>.

=cut
