package Ombudsmanden::DocType::Form;

use strict; use warnings;

use Obvius; # For OBVIUS_OK and other constants
use Obvius::DocType::Form;
use Spreadsheet::WriteExcel;
use Date::Calc qw(Week_of_Year);
use Obvius::Translations qw(gettext);
use POSIX qw(strftime);
use Ombudsmanden::ComplaintHandler;

our @ISA = qw( Obvius::DocType::Form );
our $VERSION="1.0";

# Don't allow download of any data since we're not storing any
sub raw_document_data {
    my ($this, $doc, $vdoc, $obvius, $input) = @_;
    
    $input->no_cache(1);
    return undef if !$input->pnotes('site') || !$input->pnotes('site')->param('is_admin');
    return undef if !$obvius->can_view_document($doc);
    
    if($input->param('download_excel_statistics')) {
        return $this->generate_excel_data($doc, $obvius);
    }
    if(my $id = $input->param('download_encrypted_email')) {
        return $this->download_encrypted_email($doc, $obvius, $id);
    }
    
    return undef;
}

sub generate_excel_data {
    my ($this, $doc, $obvius) = @_;

    my $name = $doc->Name;
    my $tempfile="/tmp/" . $name . ".xls";
    my $workbook=Spreadsheet::WriteExcel->new($tempfile);
    my $worksheet=$workbook->addworksheet();
    
    my @headers = ("Timestamp", "Year", "Month", "Day", "Week", "Hour", "Minute");
    
    # Headers:
    my $header_format=$workbook->addformat();
    $header_format->set_bold();
    $worksheet->write_row(0, 0, \@headers, $header_format);

    # Data:
    my $data_format=$workbook->addformat();
    $data_format->set_align('top');
    $data_format->set_locked(0);
    
    my $query = $obvius->dbh->prepare("SELECT submit_time FROM form_submit_log ORDER BY submit_time");
    $query->execute;

    my $i = 1;
    while(my ($submit_time) = $query->fetchrow_array) {
        my ($y, $m, $d, $hr, $min) = ($submit_time =~ m!^(\d\d\d\d)-(\d\d)-(\d\d)\s+(\d\d):(\d\d)!);
        my ($w, $wy) = Week_of_Year($y, $m, $d);
        my $week = sprintf("%02d/%04d", $w, $wy);
        
        $worksheet->write_row($i++, 0, [$submit_time, $y, $m, $d, $week, $hr, $min], $data_format);
    }
    $workbook->close();

    my ($data, $fh);
    eval {
        open $fh, $tempfile or die "Couldn't open $tempfile, stopping";
        do { local $/; $data = <$fh> };
        close $fh;
    };
    unlink $tempfile;
    
    return ("application/vnd.ms-excel", $data, $name . ".xls", "attachment");
}

sub download_encrypted_email {
    my ($this, $doc, $obvius, $id) = @_;
    my $sth = $obvius->dbh->prepare('SELECT encrypted_email FROM form_submit_log WHERE id = ?');
    $sth->execute($id);
    if(my ($data) = $sth->fetchrow_array) {
        return ("text/plain", $data, "encrypted_email_$id.txt", "attachment");
    } else {
        return (undef, undef, undef);
    }
}

sub action {
    my ($this, $input, $output, $doc, $vdoc, $obvius) = @_;
    
    my $formdata = Obvius::DocType::Form::get_formdata($vdoc, $obvius);
    Obvius::DocType::Form::add_submitted_values($formdata, $input) if $input->param('obvius_form_submitted');
    $output->param(formdata => $formdata);
    
    return $this->handle_submitted($input, $output, $vdoc, $obvius) 
      if $input->param('obvius_form_submitted');
    
    return OBVIUS_OK;
}

sub handle_submitted {
    my ($this, $input, $output, $vdoc, $obvius) = @_;

    my $formdata = Obvius::DocType::Form::get_formdata($vdoc, $obvius);
    my $formspec = Obvius::DocType::Form::convert_formspec($formdata);
    my ($result, @data) = create_new_entry($input, $formspec, $vdoc->Docid, $obvius);


    if (!$result) {
        my $upload_notification;
        if (ref $data[0]) {
            $output->param(invalid => $data[0]{invalid});
            $output->param(not_unique => $data[0]{non_unique});
            for my $formelem (@$formdata) {
                $formelem->{invalid} = $data[0]{outfields}{$formelem->{name}}{invalid};
                $formelem->{not_unique} = $data[0]{outfields}{$formelem->{name}}{non_unique};
                $formelem->{_submitted_value} = $data[0]{outfields}{$formelem->{name}}{value};
                $upload_notification = gettext('FormDoctype:upload_error');
            }
            if ($upload_notification) {
                notify_upload_error(
                    $upload_notification, $formdata, $data[0]{invalid},
                    $data[0]{outfields}
                );
            }
            $output->param(formdata => $formdata);
        }
        store_validation_errors($input, $data[0]{invalid}, $obvius, $vdoc->DocId);
        return OBVIUS_OK;
    } else {
        # Copy out upload data closure
        my $outfields = $data[0] || {};
        my $formdata = $output->param('formdata') || [];
        for my $field (@$formdata) {
            if(my $upload = $outfields->{$field->{name}}->{uploaded_files}) {
                $field->{uploaded_files} = $upload;
                $field->{upload_info} = $outfields->{$field->{name}}->{value};
            }
        }
    }

    # Stuff for sending emails to the form submitter:
    #my ($entry_nr, $entry_id, $fields, $count) = @data;
    #
    #for my $field (values %$formspec) {
    #     my $val = $fields->{$field->{name}};
    #     send_mail($val->{value}, $obvius, $vdoc, $formspec, $fields, $entry_nr) if ($field->{type} eq 'email');
    #}
    
    $output->param(submitted_data_ok => 1);
    return OBVIUS_OK;
}

sub store_validation_errors {
    my ($input, $invalid, $obvius, $docid) = @_;

    my $handler = Ombudsmanden::ComplaintHandler->new($obvius, undef, $input);
    my $session = $handler->get_complaint_session($docid) || {};

    # Calculate current submit attempt
    my $sth = $obvius->dbh->prepare(q|
        SELECT MAX(submit_attempt_number)
        FROM validation_errors
        WHERE session_id = ?
    |);
    $sth->execute($session->{id});
    my ($nr) = $sth->fetchrow_array;
    $nr ||= 0;
    $nr++;

    my $timestamp = strftime "%Y-%m-%d %H:%M:%S", localtime;

    $sth = $obvius->dbh->prepare(q|
        INSERT into validation_errors
            (`session_id`, `submit_attempt_number`, `timestamp`,
             `failed_field`)
        VALUES 
            (?, ?, ?, ?)
    |);

    for my $invalid_field (@$invalid) {
        $sth->execute(
            $session->{id}, $nr, $timestamp,
            $invalid_field
        );
    }

    $handler->update_complaint_session($session->{id});
}

sub notify_upload_error {
    my ($upload_notification, $formdata_ref, $invalid_ref, $outfields) = @_;
    my %sort_map;
    my $sort_idx = 0;
    for my $field (@$formdata_ref) {
        $sort_map{$field->{name}} = ++$sort_idx;
        if ($field->{type} eq 'upload') {
            # Only register field that had actual files uploaded
            my $outfield = $outfields->{$field->{name}};
            next unless($outfield);
            next unless(@{$outfield->{uploaded_files} || []});

            $field->{invalid} = $upload_notification;
            if(!grep { $_ eq $field->{name} } @$invalid_ref) {
                push(@$invalid_ref, $field->{name});
            }
        }
    }
    
    @$invalid_ref = sort { $sort_map{$a} <=> $sort_map{$b} } @$invalid_ref;
}

sub create_new_entry {
    my ($input, $formspec, $docid, $obvius) = @_;
    
    my @res = validate_full_entry($input, $formspec, $docid, $obvius);
    return @res if !$res[0];
    
    my %fields = %{$res[1]};
    
    return (1, \%fields);
}

sub send_mail {
    return 1;
}

sub get_upload_file {
    return undef;
}

sub handle_upload_file {
     my ($docid, $input, $field, $obvius) = @_;
     
     die "Not an upload file" if ($field->{type} ne 'upload');
     
     my $upload = $input->param('_incoming_' . $field->{name});
     return undef if !$upload;
     
     my ($path, $type, $filename) = retrieve_file($docid, $upload, $obvius);
     return {path => $path, type => $type, filename => $filename, upload => $upload};
}

sub retrieve_file {
    my ($docid, $upload, $obvius) = @_;
    
    die "Form:toobig"
      if ($upload->param('size') > 8_000_000);

    die "Form:exefile" if $upload->param('filename') =~ /\.exe$/i; #"
    
    my $upload_file_name = $upload->param('filename') || rand;
    $upload_file_name =~ s/[^a-zA-Z\d.]/_/g;


    return ($upload_file_name, $upload->param('mimetype'), $upload_file_name);
}


# This method replaces the above methods and provices all the neccessary
# data needed to access information about uploaded files and their data.
sub build_upload_data {
    my ($input, $fielddata) = @_;

    my $upload;
    my $nr = 0;
    my @fileinfo;
    do {
        $nr++;
        my $uploadname = sprintf('_incoming_%s__%02d', $fielddata->{name}, $nr);
        $upload = $input->param($uploadname);
        if($upload) {
            push(@fileinfo, $upload);
        }
    } while($upload);

    $fielddata->{uploaded_files} = \@fileinfo;
}

sub validate_full_entry {
    my ($input, $formspec, $docid, $obvius) = @_;
    my @invalid_upload_fields;
    my %input = ref $input ne 'HASH' && $input->UNIVERSAL::can('param') ? 
                map { $_ => $input->param($_) } keys %$formspec : %$input;  

    my %uploads_by_name;

    # Extract upload information and store it in the forspec as well as
    # in %uploads_by_name.
    for my $val (grep { $_->{type} eq 'upload' } values %$formspec) {
        build_upload_data($input, $val);
        $uploads_by_name{$val->{name}} = $val->{uploaded_files};
    }
    
    my $outfields = Obvius::DocType::Form::preprocess_fields($formspec, \%input, $obvius->config->param('charset'));
    
    for my $fn (keys %$formspec) {
        Obvius::DocType::Form::validate_entry($formspec->{$fn}, $outfields->{$fn}, $outfields, $docid, $obvius);
    }

    # Add upload information to outfields, so they can be used in templates.
    for my $fn (keys %$outfields) {
        if(my $u = $uploads_by_name{$fn}) {
            $outfields->{$fn}->{uploaded_files} = $u;
        }
    }
    
    $outfields->{$_->{name}}{invalid}  = $_->{error} for @invalid_upload_fields;
    my @invalid_fields = grep { $_->{invalid} 
                                || $_->{mandatory_failed} 
                                || $_->{options_failed}} values %$outfields;

    my @non_unique_fields = grep { $_->{unique_failed}} values %$outfields;
    
    if (@invalid_fields || @non_unique_fields) {
        return (undef, {invalid => [ map { $_->{name} } @invalid_fields ],
                        non_unique => [ map { $_->{name} } @non_unique_fields ],
                        outfields => $outfields
                       },
               );
    }
    
    return (1,$outfields);
}


1;
