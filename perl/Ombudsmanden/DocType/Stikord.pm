package Ombudsmanden::DocType::Stikord;

########################################################################
#
# Stikord.pm - Redirect for the doctype Stikord
#
# Copyright (C) 2004 Magenta ApS, Denmark (http://www.magenta-aps.dk/)
#
# Author: Martin Skøtt (martin@magenta-aps.dk)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
#
########################################################################

# $Id: Stikord.pm,v 1.1.2.1 2010/10/18 13:15:47 jubk Exp $

use strict;
use warnings;

use Obvius;

use Obvius::DocType;

our @ISA = qw( Obvius::DocType );
our ( $VERSION ) = '$Revision: 1.1.2.1 $ ' =~ /\$Revision:\s+([^\s]+)/;

# action - just returns OK, as the "work" of the document type is done
#          by the alternate_location-method.
sub action {
    my ($this, $input, $output, $doc, $vdoc, $obvius) = @_;
    return OBVIUS_OK;
}

# alternate_location - returns the contents of the hovedreg-field, so that
#                      Obvius can redirect to the address stored there.
sub alternate_location {
    my ($this, $doc, $vdoc, $obvius) = @_;

    $this->tracer($doc, $vdoc, $obvius) if ($this->{DEBUG});


    my $url = $obvius->get_version_field($vdoc, 'hovedreg');

    # Add fixup for URLs on subsites starting with /:

    if($url =~ m!^/!) {
        $url = "https://" . $obvius->config->param('roothost') . $url;
    }

    return $url;
}

1;
