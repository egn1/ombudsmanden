package Ombudsmanden::Search;
use strict;
use warnings;

use base "Exporter";
our @EXPORT_OK = qw(search);
our @EXPORT = @EXPORT_OK;

use LWP::UserAgent; use HTTP::Request; use HTTP::Request::Common;
use URI::Escape;

my $solr_core_url = "http://localhost:8983/solr/ombud2017";

my $MAXROWS = 100;
my $MAXSTART = 20000;
my $MAXPAGE = 1000;

sub check_query_param { my($q) = @_; return 1; }
sub check_sort_param { my($r) = @_; return 0 unless $r eq "f_docdate"; return 1; }
sub check_rows_param { my($r) = @_; return 0 unless $r =~ m/^\d+$/ and $r <= $MAXROWS; return 1; }
sub check_start_param { my($s) = @_; return 0 unless $s =~ m/^\d+$/ and $s <= $MAXSTART; return 1; }
sub check_page_param { my($p) = @_; return 0 unless $p =~ m/^\d+$/ and $p <= $MAXPAGE; return 1; }
sub check_facet_hreg_param { my($h) = @_; return 0 unless $h =~ m|^\d+-| or $h eq ""; return 1; }
sub check_facet_mat_type_param { my($h) = @_; return 1; }
sub check_facet_forvret_emne_param { my($h) = @_; return 1; }
sub check_facet_spec_sagsomr_param { my($h) = @_; return 1; }
sub check_facet_all_param { my($h) = @_; return 1; }

sub check_filter_param { my($q) = @_; return 1; }

sub set_solr_core_url {
    my ($self, $url) = @_;

    if(!ref($self) && $self ne __PACKAGE__) {
        $url = $self;
    }

    $solr_core_url = $url;

    return;
}


my %allowed = ( query => \&check_query_param,
    sort => \&check_sort_param,
    rows => \&check_rows_param,
    start => \&check_start_param,
    page => \&check_page_param,
    facet_hreg => \&check_facet_hreg_param,
    facet_mat_type => \&check_facet_mat_type_param,
    facet_aarstal => \&check_facet_mat_type_param,
    facet_myndighed => \&check_facet_mat_type_param,
    facet_insttype => \&check_facet_mat_type_param,
    facet_geografisk => \&check_facet_mat_type_param,
    facet_forvret_emne => \&check_facet_forvret_emne_param,
    facet_spec_sagsomr => \&check_facet_spec_sagsomr_param,
    facet_all => \&check_facet_all_param,

    filter_mat => \&check_filter_param,
    filter_aarstal => \&check_filter_param,
    filter_hreg => \&check_filter_param,
    filter_myndighed => \&check_filter_param,
    filter_inspektionstype => \&check_filter_param,
    filter_inspomraade => \&check_filter_param,
    filter_sagsomraade => \&check_filter_param,
    filter_forvret_emne => \&check_filter_param,
# insp_type geo_omr myndighed 
);
my @all_facets = qw(f_spec_sagsomr f_mat_type f_forvret_emne k_catpath k_publyear f_myndighed f_inst_type f_geografisk);


my %arg_map = (
    sort => "sort=%s%%20desc",
    queryold => sub {
        my ($value) = @_;
        return "q=" . uri_escape(join(" OR ",
            sprintf('title1:(%s)^10', $value),
            sprintf('title2:(%s)^7', $value),
            sprintf('textmeta:(%s)^4', $value),
            sprintf('htmlbody:(%s)^1', $value),
        ));
    },
    query => sub {
        my ($value) = @_;
        my @sagsnr_filter = ();

        # Take anything from the query that looks like a sagsnr and add it to @sagsnr_filter
        while ($value =~ /(^| *)(\d[-.\/\d]+)/g) {
            push(@sagsnr_filter, $2);
        }
        # Convert the values in @sagsnr_filter into search expressions
        # for each term, make a subquery with all-dot separators and all-dash separators.
        if (@sagsnr_filter > 0) {
            my @q;
            for my $t (@sagsnr_filter) {
                $t =~ tr/-/./;
                my $t1 = $t;
                push(@q, $t);
                $t =~ tr/./-/;
                if ($t ne $t1) {
                    push(@q, $t);
                }
            }
            @sagsnr_filter = @q;
        }
        
        my @terms;

        # Only process a search string that contains non-whitespaces and
        # does not equal search-for-everything strings.
        if($value && $value =~ m{\S} && $value ne "*" && $value ne "*:*" ) {
            # Split rest of query string into search terms
            @terms = split(/\s+/, $value);
        }

        # Remove empty strings
        @terms = grep { length($_) } @terms;

        my @query;
        my $content_query = join(" AND ",
            map {
                "(" . join(
                    " OR ",
                    "title1:(\"$_\")^10",
                    "title2:(\"$_\")^7",
                    "textmeta:(\"$_\")^4",
                    "htmlbody:(\"$_\")^1",
                ) .")"
            } @terms
        );
        if (length($content_query)) {
            push(@query, $content_query);
        }
        if (@sagsnr_filter > 0) {
            push(
                @query, map {
                    "(f_sagsnr:*$_*)"
                } @sagsnr_filter
            );
        }

        # If no query was specified search on everything
        if(!@query) {
            push(@query, "*:*");
        }

        return "q=" . uri_escape(join(" OR ", @query));
    },
    rows => "rows=%s",
    start => "start=%s",
    facet_hreg => "facet.field={!ex=f3}k_catpath&facet.k_catpath.prefix=%s",
    facet_mat_type => "facet.field=f_mat_type",
    facet_mat_type_ex => "facet.field={!ex=f1,f2,f3,f4,f5,f6,f7,f8}f_mat_type",
    facet_myndighed => "facet.field={!ex=f4}f_myndighed",
    facet_insttype => "facet.field={!ex=f5}f_inst_type",
    facet_geografisk => "facet.field={!ex=f6}f_geografisk",
    facet_aarstal => "facet.field={!ex=f2}k_publyear",
    facet_forvret_emne => "facet.field={!ex=f8}f_forvret_emne",
    facet_spec_sagsomr => "facet.field={!ex=f7}f_spec_sagsomr",
#    facet_all => join("&", map{ "facet.field=$_" } @all_facets)."&f.k_catpath.facet.prefix=%s&facet=on",
    # NOTE some sources googles say this should work;
    # it DOESN'T WORK: &facet.field=k_catpath&facet.k_catpath.prefix=%s&facet=on",
    # the f.<fieldname>.facet.prefix version works. Probably changed API in some Solr version.  
    filter_mat => "fq={!tag=f1}f_mat_type:%s",
    filter_aarstal => "fq={!tag=f2}k_publyear:%s",
    filter_hreg => "fq={!tag=f3}k_catpath:%s",
    filter_myndighed => "fq={!tag=f4}f_myndighed:%s",
    filter_inspektionstype => "fq={!tag=f5}f_inst_type:%s",
    filter_inspomraade => "fq={!tag=f6}f_geografisk:%s",
    filter_sagsomraade => "fq={!tag=f7}f_spec_sagsomr:%s",
    filter_forvret_emne => "fq={!tag=f8}f_forvret_emne:%s"
);

sub format_argument {
    my($key, $ARGS) =  @_;

    # Get function or formatter from arg_map
    my $fct_format = $arg_map{$key};

    # Get argument from $ARGS
    my $value = $ARGS->{$key};
    
    if(defined $value and $fct_format =~ m/fq/) {
        $value =~ s/ /\\ /g;
    }
    
    if (ref($fct_format) eq 'CODE') {
        return $fct_format->($value);
    } elsif(defined $value && $fct_format =~ m{%s}) {
        return sprintf($fct_format, $value);
    } elsif($fct_format =~ m{%s}) {
        return sprintf($fct_format, "");
    } else {
        return $fct_format;
    }
}

sub search {
    my %ARGS;  # either a hashref { query => "foo*", rows => 10, page => 0 } or  just the key-value list

    if(ref $_[0] eq "HASH") {
        %ARGS = %{$_[0]}
    } elsif(scalar(@_)%2 != 1) {
        %ARGS = @_;
    } else {
        return undef;
    }

    my @facet_on = ();

    for my $k (keys %ARGS) {
        @facet_on = ("facet=on") if substr($k,0,5) eq "facet";
        my $ok = defined $allowed{$k} and $allowed{$k}->($ARGS{$k});
        return undef unless $ok;
    }

    # my $query = $ARGS{query}; # not used. as Solr expects q to provide some resultset for facetting, it should always be set.
    # my $kind = $ARGS{kind} || "default";
    
    my @fields = qw(
        id docid k_uri f_title f_docdate f_sagsnr f_excerpt f_mat_type f_forvret_emne
        f_spec_sagsomr k_url title1
    );

    my $highlight = join("", 
        "hl=on",
        "&hl.simple.pre=%3Cspan%20class=%22search-highlight%22%3E",
        "&hl.simple.post=%3C/span%3E",
        "&hl.fl=title1,title2,textmeta,textbody",
    );

    my $format = "wt=json&indent=on";

    my @arguments = map { format_argument($_, \%ARGS) } keys %ARGS;

    my $final_url = "$solr_core_url/select?".join("&",
        "fl=".join(",", @fields),
        $highlight,
        $format,
        @facet_on,
        @arguments,
    );

    my $request = GET($final_url);
    my $ua = LWP::UserAgent->new;
    my $response = $ua->request($request);
    my $out = $response->decoded_content;
    return $out;
}
    
1;
