package Ombudsmanden::MasonCommands;

use strict;
use warnings;

=head1 NAME

Ombudsmanden::MasonCommands -
    Site-specific Mason commands for Ombudsmanden after 2017

=head1 SYNOPSIS

Inside a mason component:

    <& sitecommands->expand_widget("editor", %ARGS) &>

or

    <%init>
    return sitecommands->render_widget("image", %ARGS);
    </%init>

or

    % sitecommands->render_widget($type, %widget_args);

=head1 DESCRIPTION

This module provides global methods that are accessible from mason components
for the Ombudsmanden 2017 site. The module ensures that the global variables in
mason can be used within methods in the module.
Methods can be accessed from mason by calling the sitecommands()
(really WebObvius::MasonCommands::sitecommands) method
from within a mason component. This will return reference to the module and
make sure global variables are initialized.

For this to work "site_packagename" in /etc/obvius/ombud2017.conf must be
set to Ombudsmanden::MasonCommands.

=cut

# Make sure WebObvius::MasonCommands is loaded
use WebObvius::MasonCommands;

# Set up and alias global variables
our ($m, $doc, $mcms, $prefix, $uri, $vdoc, $obvius, $doctype, $r);

my $initialized = 0;

=head1 METHODS

=head2 initialize(%global_refs)

Configures the global variables used in the module. Supposed to be called from
WebObvius::MasonCommmands.

=cut
sub initialize {
    my ($classname, %globals_refs) = @_;

    if(!$initialized) {
        # This aliases the module variables to the references passed in
        # the %global_refs has. Special case for $m which will always
        # point to $HTML::Mason::Commands::m inside components and not to the
        # actual "in_package" module configured for mason.
        # $m is always set to $HTML::Mason::Commands::m inside components
        *m = \$HTML::Mason::Commands::m;
        *doc = $globals_refs{doc} if($globals_refs{doc});
        *mcms = $globals_refs{mcms} if($globals_refs{mcms});
        *prefix = $globals_refs{prefix} if($globals_refs{prefix});
        *uri = $globals_refs{uri} if($globals_refs{uri});
        *vdoc = $globals_refs{vdoc} if($globals_refs{mcms});
        *obvius = $globals_refs{obvius} if($globals_refs{obvius});
        *doctype = $globals_refs{doctype} if($globals_refs{doctype});
        *r = $globals_refs{r} if($globals_refs{r});
        $initialized = 1;
    }

    return $classname;
}

=head2 render_widget($widget_type, %widget_args)

Renders the specified widget in the same way as calling it with the <& .. &>
mason syntax.

=cut
sub render_widget {
    my ($self, $widget_type, %args) = @_;

    return $m->comp(
        "/doctypes/gridcontent_files/widgets/${widget_type}",
        %args
    );
}

=head2 expand_widget($widget_type, %widget_args)

Renders the specified widget and returns the resulting HTML.

=cut
sub expand_widget {
    my ($self, $widget_type, %args) = @_;

    return $m->scomp(
        "/doctypes/gridcontent_files/widgets/${widget_type}",
        %args
    );
}

=head2 uri_to_docid($uri)

Convert public uri to internal docid-based uri.

=cut

sub uri_to_docid {
    my $self = shift;
    my $uri = shift;
    return "" unless($uri);
    if(my $d = $obvius->lookup_document($uri)) {
        return "/" . $d->Id . ".docid";
    }
    return $uri;
}

=head2 docid_to_uri("/$docid.docid")

Convert internal docid-based uri to public uri

=cut

sub docid_to_uri {
    my $self = shift;
    my $docid = shift;
    return "" unless($docid);
    if($docid =~ m!^/(\d+)\.docid!) {
        if(my $d = $obvius->get_doc_by_id($1)) {
            return $obvius->get_doc_uri($d);
        }
    }
    return $docid;
}


1;
