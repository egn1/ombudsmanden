package Ombudsmanden::ComplaintHandler;

use strict;
use warnings;

use Date::Calc;
use Data::Dumper;
use Obvius::Hostmap;
use Obvius;
use Obvius::Config;
use Obvius::Data;
use Obvius::Log;
use Obvius::CharsetTools qw(mixed2perl);
use Obvius::Translations qw(__);
use WebObvius::Cache::Cache;
use POSIX qw(ceil strftime);
use utf8;
use Net::IDN::Encode qq(domain_to_unicode);
use Net::IDN::Encode qq(domain_to_unicode);
use Net::SMTP;
use Time::HiRes;

sub new {
    my ($classname, $obvius_or_confname, $mason, $request, $prefix, %args) = @_;

    if (!$obvius_or_confname) {
        die "You must provide an obvius object or a configuration name as the first argument";
    }
    my $self = {%args};
    $self->{mason} = $mason;
    $self->{request} = $request;
    $self->{prefix} = $prefix;
    my $oref = ref($obvius_or_confname) || '';
    if (!$oref) {
        $self->{_confname} = $obvius_or_confname;
    } elsif ($oref eq 'Obvius::Config') {
        $self->{_obvius_config} = $obvius_or_confname;
    } elsif ($oref eq 'Obvius') {
        $self->{_obvius} = $obvius_or_confname;
        $self->{_obvius_config} = $self->{_obvius}->config;
    } else {
        die "Don't know how to handle first argument of type $oref";
    }
    return bless($self, $classname);
}

=head2 obvius

  my $obvius = $dpr->obvius;

returns the associated L<Obvius> object.

=cut


sub obvius {
    my ($self) = @_;

    my $obvius = $self->{_obvius};
    unless($obvius) {
        my $config = $self->obvius_config;
        my $log = new Obvius::Log qw(notice);
        $obvius = Obvius->new(
            $config,
            undef, undef, undef, undef, undef,
            log => $log
        );
        $obvius->{USER} = 'admin';
        $self->{_obvius} = $obvius;
    }

    return $obvius;
}

=head2 obvius_config

  my $obvius_config = $dpr->obvius_config;

returns the associated L<Obvius::Config> object.

=cut


sub obvius_config {
    my ($self) = @_;
    my $config = $self->{_obvius_config};
    unless($config) {
        my $confname = $self->{_confname};
        if(!$confname) {
            die "Can not load obvius-config: No confname specified"
        }
        $config = $self->{_obvius_config} = Obvius::Config->new($confname);
    }

    return $config;
}


sub send_complaint {
    my ($self, $docid, $output) = @_;

    my ($mailserver, $mailfrom, $mailto) = $self->get_mail_meta();

    my $r = $self->{request};
    my $formdata = $output->param('formdata');

    my $error_message;
    my $mail_id;
    my $mailtxt;
    my $submission_status = 'sent';

    my $now = [Time::HiRes::gettimeofday];
    # timestamps are so precise that calling gettimeofday twice in a row gives two different timestamps
    my $time = {start=>$now, interval=>$now};

    ($mailtxt, $error_message) = $self->generate_encrypted_email($mailfrom, $mailto, $formdata);
    $self->log_duration("generate_encrypted_email", $time);
    return $error_message if $error_message; # exiting early in case we failed to even encrypt the message (likely because of upload file size)

    ($error_message, $mail_id) = $self->send_email($mailserver, $mailfrom, $mailto, $mailtxt, $r->notes('now'));
    $self->log_duration("send_email", $time);
    $submission_status = 'failed' if $error_message; # don't exit early, instead store failure & encrypted mail in database

    $self->save_encrypted_complaint($docid, $mailtxt, $submission_status, $mail_id);
    $self->log_duration("save_encrypted_complaint", $time);

    return $error_message;
}

sub generate_encrypted_email {
    my ($self, $mailfrom, $mailto, $formdata) = @_;
    my $m = $self->{mason};
    my $mailtxt = $m->scomp(
        '/doctypes/form_files/mail/encrypted_message.txt',
        mailfrom => $mailfrom, mailto => $mailto, formdata => $formdata
    );

    my $mailtxt_error;
    $mailtxt_error = "Kunne ikke generere mailbesked" unless $mailtxt;

    # According to Seyit the @ombudsmanden.dk mail-server can handle at least
    # 200Mb attachments. The documentation on the form says submitters can
    # attach up to 120Mb and encryption and base64 encoding of files each
    # add an overhead of 33%, so it should be safe to use the following:
    #   120 * 1.33 * 1.33 ~= 212
    #
    if(length($mailtxt) > 212*1000*1000) {
        $mailtxt_error =  "Din klage fylder for meget til at kunne sendes via krypteret email.
                    Reducer eventuelt antallet eller størrelsen af bilag.
                    Alle bilag må sammenlagt maksimalt fylde 35MB.
                    Ring hvis du har spørgsmål til denne besked.";
        $mailtxt = undef; # make sure we don't store abnormally large e-mail in database
    }
    return ($mailtxt, $mailtxt_error);
}

sub get_mail_meta {
    my ($self) = @_;
    my $obvius = $self->obvius();

    my $mailserver = $obvius->config->param('ombudsmanden_smtp_server') ||
        $obvius->config->param('smtp') ||
        'localhost';

    my $mailfrom = $obvius->config->param('form_secure_mail_from');
    my $mailto = $obvius->config->param('form_secure_mail_to');

    return ($mailserver, $mailfrom, $mailto);
}

sub resend_complaint {
    my ($self, $docid) = @_;

    my ($mailserver, $mailfrom, $mailto) = $self->get_mail_meta();

    my $mailtxt = $self->get_encrypted_complaint();

    my $submission_status = 'retried';

    my ($resend_complaint_error, $mail_id) = $self->send_email($mailserver, $mailfrom, $mailto, $mailtxt);
    $submission_status = 'retried_failed' if $resend_complaint_error;

    $self->update_submission_status($submission_status, undef, $mail_id);

    return $resend_complaint_error;
}


sub send_email {
    my ($self, $server, $from, $to, $txt, $now) = @_;

    my $mail_error;
    my $mail_id;

    my $smtp = Net::SMTP->new($server, Timeout=>5, Debug => 0) or $mail_error = 'Error connecting to SMTP: '. $server . ' timeout after 5 seconds';

    $mail_error = "Simulated error"                             if($self->obvius_config->param('simulate_mail_error'));
    $mail_error = "Failed to specify a sender [$from]\n"        unless ($mail_error or $smtp->mail($from));
    $mail_error = "Failed to specify a recipient [$to]\n"       unless ($mail_error or $smtp->to($to));
    $mail_error = "Failed to send email data\n"                 unless ($mail_error or $smtp->data([$txt]));
    if (!$mail_error) {
        $smtp->dataend();
        my $responseline = $smtp->message();
        if ($responseline =~ m/queued as ([^\s]+)/) {
            $mail_id = $1;
        } elsif ($responseline =~ m/id=([^\s]+)/) {
            $mail_id = $1;
        }
    }
    $mail_error = "Failed to quit\n"                            unless ($mail_error or $smtp->quit);

    if ( $mail_error ) {
        $self->handle_mail_error($mail_error);
    }
    eval {
        $now ||= strftime("%Y-%m-%d %H:%M:%S", localtime);
        my $maillog;
        my $file = "/var/www/www.ombudsmanden.dk/var/klageskema_mail.log";
        open($maillog, ">>", $file);
        my $msg = $mail_error || "OK";
        print $maillog "$now: $msg\n";
        close($maillog);
    };
    if($@) {
        print STDERR "Error while writing to maillog: $@";
    }

    return ($mail_error, $mail_id);
}

sub handle_mail_error {
    my ($self, $mail_error) = @_;

    my $obvius = $self->obvius;

    # Print it out to the error log
    my $today = strftime( "%Y-%m-%d %H:%M:%S", localtime );
    print STDERR "\n$today: after_submit mail error: $mail_error\n";

    # Collect adresses we want to send to
    my $notify_list = $obvius->config->param('mail_error_notification_list') ||
                      'obviusadmin@magenta-aps.dk';

    my @recipients = grep { $_ } split(/\s*,\s*/, $notify_list);

    my $from = 'webmaster@ombudsmanden.dk';
    foreach my $to (@recipients) {
        eval {
            my $smtp = Net::SMTP->new('localhost', Timeout=>5, Debug => 1);
            die "Could not connect to mailserver within 5 seconds" unless($smtp);

            my $txt = $self->{mason}->scomp(
                '/doctypes/form_files/mail/mail_error_notification.mason',
                to => $to,
                mail_error => $mail_error
            );
            die "Could not generate message" unless($txt);

            die "Failed to specify a sender [$from]"  unless ($smtp->mail($from));
            die "Failed to specify a recipient [$to]" unless ($smtp->to($to));
            die "Failed to send email data"           unless ($smtp->data([$txt]));
            die "Failed to quit"                      unless ($smtp->quit);
        };
        if(my $alert_mail_error = $@) {
            print STDERR "\nError while sending alert mail: $alert_mail_error\n";
        }
    }
}

sub get_complaint_session {
    my ($self, $docid) = @_;

    if(!$docid) {
        die(__PACKAGE__ . "::get_complaint_session called without a docid");
    }

    my $input = $self->{request};
    my $uuid = $input->param('obvius_submission_uuid');

    if(!$uuid) {
        warn("get_complaint_session called without an uuid\n");
        return undef;
    }

    my $anonymized_user = $input->param('obvius_anonymized_user');
    my $obvius = $self->obvius();

    my $sth = $obvius->dbh->prepare(q|
        SELECT
            *
        FROM
            complaint_sessions
        WHERE
            submission_uuid = ?
    |);
    $sth->execute($uuid);

    my $result = $sth->fetchrow_hashref;
    $sth->finish;

    # If submitter wasn't registered, create one.
    if(!$result) {
        my $now = strftime("%Y-%m-%d %H:%M:%S", localtime);
        $sth = $obvius->dbh->prepare(q|
            INSERT INTO complaint_sessions
                (`submission_uuid`, `docid`, `anonymized_user`,
                 `created`, `updated`)
            VALUES
                (?, ?, ?,
                 ?, ?)
        |);
        $sth->execute($uuid, $docid, $anonymized_user,
                      $now, $now);
        my $id = $obvius->dbh->{mysql_insertid};
        $sth->finish;

        $sth = $obvius->dbh->prepare(
            'SELECT * FROM complaint_sessions WHERE id = ?'
        );
        $sth->execute($id);
        $result = $sth->fetchrow_hashref;
        $sth->finish;
    }

    return $result;
}

sub update_complaint_session {
    my ($self, $id) = @_;

    if(!$id) {
        return;
    }

    my $obvius = $self->obvius;
    my $now = strftime("%Y-%m-%d %H:%M:%S", localtime);

    my $greatest_update_time = '';

    my $sth = $obvius->dbh->prepare(q|
        select
            created,
            updated
        from
            complaint_sessions
        where
            id = ?
    |);
    $sth->execute($id);
    my ($created, $updated) = $sth->fetchrow_array;
    if($created && $updated gt $greatest_update_time) {
        $greatest_update_time = $created;
    }
    if($updated && $updated gt $greatest_update_time) {
        $greatest_update_time = $updated;
    }

    $sth = $obvius->dbh->prepare(q|
        SELECT MAX(timestamp)
        FROM `validation_errors`
        WHERE session_id = ?
    |);
    $sth->execute($id);
    my ($max_validation_time) = $sth->fetchrow_array;
    if($max_validation_time && $max_validation_time gt $greatest_update_time) {
        $greatest_update_time = $max_validation_time;
    }

    $sth = $obvius->dbh->prepare(q|
        SELECT timestamp, status
        FROM `submitted_complaints`
        WHERE session_id = ?
        ORDER BY timestamp desc
        LIMIT 1
    |);
    $sth->execute($id);
    my ($max_submit_time, $last_status) = $sth->fetchrow_array;
    if($max_submit_time && $max_submit_time gt $greatest_update_time) {
        $greatest_update_time = $max_submit_time;
    }

    if(!$greatest_update_time) {
        $greatest_update_time = $now;
    }

    $sth = $obvius->dbh->prepare(q|
        update complaint_sessions
        SET
            updated = ?,
            latest_status = ?
        where id = ?
    |);

    $sth->execute($greatest_update_time, $last_status, $id);
}

sub get_submitted_name {
    my ($self) = @_;

    my $r = $self->{request};

    foreach my $key ($r->param) {
        if($key =~ m{navn}i) {
            return $r->param($key) || '';
        }
    }

    return '';
}

sub get_submitted_address {
    my ($self) = @_;

    my $r = $self->{request};

    foreach my $key ($r->param) {
        if($key =~ m{adresse}i && $key !~ m{e-?mail}i) {
            return $r->param($key) || '';
        }
    }

    return '';
}

sub save_encrypted_complaint {
    my ($self, $docid, $mailtxt, $submission_status, $mail_id) = @_;

    my $obvius = $self->obvius();
    my $r = $self->{request};
    my $anonymized_user = $r->param('obvius_anonymized_user');
    my $submission_uuid = $r->param('obvius_submission_uuid') || '';
    my $session = $self->get_complaint_session($docid) || {};
    my $timestamp = $r->notes('now');
    my $submitter_name = $self->get_submitted_name();
    my $submitter_address = $self->get_submitted_address();

    my $sth = $obvius->dbh->prepare(q|
        INSERT INTO
            submitted_complaints
            (session_id, timestamp, name, address, encrypted_mail, status, mail_id)
        VALUES
            (?, ?, ?, ?, ?, ?, ?)
    |);
    $sth->execute(
        $session->{id}, $timestamp, $submitter_name, $submitter_address,
        $mailtxt, $submission_status, $mail_id
    );
    $self->update_complaint_session($session->{id})
}

sub get_encrypted_complaint {
    my ($self) = @_;

    my $submitted_id = $self->{request}->param('id');
    my $obvius = $self->obvius();

    my $get_complaint_sql = qq|
        SELECT  submitted_complaints.encrypted_mail   email
        FROM    submitted_complaints
        WHERE  submitted_complaints.id = ?
    |;

    my $sth = $obvius->dbh->prepare($get_complaint_sql);

    $sth->execute($submitted_id);

    my $rec = $sth->fetchrow_hashref;

    return $rec->{email};

}

# Creates a copy of an existing registration of a submitted form,
# but gives it a new status and a new updated timestamp
sub update_submission_status {
    my ($self, $submission_status, $complaint_id, $mail_id) = @_;

    $complaint_id ||= $self->{request}->param('id');
    my $obvius = $self->obvius();

    # Fetch original entry
    my $sth = $obvius->dbh->prepare(q|
        select * from submitted_complaints where id = ?
    |);
    $sth->execute($complaint_id);
    my $rec = $sth->fetchrow_hashref;

    if(!$rec) {
        return;
    }

    # Create new entry
    my @fields = qw(
        session_id timestamp name address encrypted_mail status mail_id
    );
    my $sql_fields = join(", ", map { "`$_`"} @fields);
    my $qms = join(", ", map {"?"} @fields);

    # Set the new status and timestamp
    $rec->{status} = $submission_status;
    $rec->{timestamp} = strftime("%Y-%m-%d %H:%M:%S", localtime);
    $rec->{mail_id} = $mail_id;

    my @values = map { $rec->{$_} } @fields;

    $sth = $obvius->dbh->prepare(qq|
        INSERT INTO submitted_complaints
            ($sql_fields)
        VALUES
            ($qms)
    |);
    $sth->execute(@values);

    # Update overall complaint fields
    $self->update_complaint_session($rec->{session_id});
}

sub log_duration {
    my ($self, $msg, $time) = @_;

    # Use the same time object for both comparisons instead of the built-in default in tv_interval.
    # this way we are sure that the first call to log_duration gives the same number
    # for $interval_duration and $total_duration
    my $now = [Time::HiRes::gettimeofday];
    my $interval_duration = Time::HiRes::tv_interval($time->{interval}, $now);
    my $total_duration = Time::HiRes::tv_interval($time->{start}, $now);
    print STDERR "ComplaintHandler <$msg> <$interval_duration> <$total_duration>\n";

    # Update interval time but not start time, so we're ready for next call to log_duration
    $time->{interval} = $now;
    return $time;
}

1;
