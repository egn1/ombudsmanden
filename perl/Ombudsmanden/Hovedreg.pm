package Ombudsmanden::Hovedreg;
use strict;
use warnings;

use base "Exporter";
our @EXPORT_OK = qw(make_csv make_html get_node hreg_ids_to_catpaths);
our @EXPORT = @EXPORT_OK;

use JSON;
use Data::Dumper;
my $hovedreg = undef;
my $facets = undef;

my $outv = "";

sub vprint {
foreach my $e (@_) {
        $outv .= sprintf "%s", $e;
    }
}
sub vprintf { my ($f,@a) = @_;  $outv .= sprintf $f, @a; }

my $ul_format = qq(<li id="%s">\n    <label tabindex="0">\n        <input name="topic" type="radio" value="%s" tabindex="-1">%s</label>\n);
my $ispace = "    ";
my $indent = 0;
my $ul_nest_begin = sub { vprint $ispace x $indent,"<ul>\n"; $indent++ };
my $ul_nest_end = sub { $indent--; vprint $ispace x $indent, "</ul>\n"; };
my $ul_node_begin = sub { my($catpath, $basecat, $id, $display_title, $name, $key) = @_;
    my $uform = $ul_format;
    my $ind = $ispace x $indent;
    $uform =~ s/^/$ind/gm;
    my $nodefacet = "";

    $nodefacet = " (".$facets->{$catpath}.")" || "" if defined $facets and defined $facets->{$catpath};
    vprintf($uform, $catpath, $catpath, $name.$nodefacet);
    $indent++
};
my $ul_node_end = sub { $indent-- ; vprint  $ispace x $indent, "</li>\n"; };

sub csv_vprintf{ vprintf("%s:%s:%s:%s:%s:%s\n", @_); }

my($do_node, $do_node_end, $do_nest_begin, $do_nest_end) = (\&csv_vprintf, undef, undef, undef);

sub traverse { my ($lev, $b, $k, $o, @p) = @_;
	push @p, $o->{key};
	my @okeys = (grep { !/subs/ } sort keys %{ $o });
	warn "Strange keys: ",@okeys unless join(" ", @okeys) eq "display_title id key name";
	warn "$k does not equal ".$o->{id} if $k ne $o->{id};
	$do_node->("$lev-".join("/",@p), $b, map { $o->{$_} } qw(id display_title name key));
	if(ref $o->{subs} eq "HASH") {
		$do_nest_begin->() if defined $do_nest_begin;
		for my $subk (sort keys %{$o->{subs}}) { traverse($lev+1,$b, $subk, $o->{subs}->{$subk}, @p); }
		$do_nest_end->() if defined $do_nest_end;
	}
	$do_node_end->() if defined $do_node_end;
}

sub load_hreg {
    return $hovedreg if defined $hovedreg;

    my $jsonstr;

    local $/;
    $/ = undef;
    open F,"/var/www/www.ombudsmanden.dk/docs/javascript/hovedregister.js" or die "can't open hovedregister.js: $!";
    $jsonstr = <F>;
    close F;

    $jsonstr =~ s/var +\w+ +=//;
    $jsonstr =~ s/'/"/g;
    $jsonstr =~ s/};/}/;

    $hovedreg = from_json($jsonstr);
    return $hovedreg;
}

sub make_csv {
    my $json_hreg = load_hreg();

    for my $k (sort keys %{$json_hreg}) {
        traverse(0, $k, $k, $json_hreg->{$k});
    }
    return $outv;
}

sub make_toplevel {
    my $json_hreg = load_hreg();
    return sort keys %{$json_hreg};
}

sub get_node { my($nodekey) = @_;
    my $json_hreg = load_hreg();
    if($nodekey =~m/^\d/) {
	return "NOT IMPLEMENTED";
    } else {
        my @k = split/:/,$nodekey;
        my $k = shift @k;
        my $node = $json_hreg->{$k};
        while($k = shift @k and defined $node and defined $node->{subs} and defined $node->{subs}->{$k}) {
            $node = $node->{subs}->{$k};
        }
        return $node;
    }
}

sub hreg_ids_to_catpaths { my(@ids) = @_;
    my $json_hreg = load_hreg();
    my %catpath = ();
    for my $hbase (@ids) {
        my @cat = ();
        if($hbase =~ m/^(.*) (\d)(\d*\.\d|)$/) {
            @cat = ($json_hreg->{$1}->{key}, grep(!/\./, split("", "$2$3")));
        }
        for( my $l = 0; $l < scalar(@cat); $l++ ) {
            $catpath{ "$l-".join("/",@cat[0..$l])} = 1;
        }
    }
    return sort keys %catpath;
}

sub make_nodestr {
    my $node = get_node(@_);
    return map { "$_ => ".(ref($node->{$_}) ?"..." : $node->{$_})."\n" } sort { ($a eq "subs")?1:($b eq "subs")?-1:0 } keys %{$node};
}

sub make_html { my($facets_in) = @_;
    $outv = "";
    $facets = undef;
    $facets = $facets_in if defined($facets_in) and ref($facets_in) eq "HASH";
    my $json_hreg = load_hreg();
    ($do_node, $do_node_end, $do_nest_begin, $do_nest_end) = ($ul_node_begin, $ul_node_end, $ul_nest_begin, $ul_nest_end);
    vprint qq(<ul id="topic-list">\n);
    $indent++;
    for my $letter (sort keys %{ { map { substr($_,0,1) => 1 } keys %{$json_hreg} } }) {
      vprint $ispace x $indent++, qq(<li class="col-4"><h2 class="letter">$letter</h2>\n);
      vprint $ispace x $indent++, qq(  <ul>\n);
      for my $k (grep { /^$letter/ } sort keys %{$json_hreg}) {
	traverse(0, $k, $k, $json_hreg->{$k});
      }
      $indent--; vprint $ispace x $indent, "</ul>\n";
      $indent--; vprint $ispace x $indent, "</li>\n";
    }
    vprint "</ul>\n";
    return $outv;
}

sub make_dump {
    my $json_hreg = load_hreg();
    return Data::Dumper->Dump( [ $json_hreg ] );
}

sub shelltest {  # run with perl -Iperl -MOmbudsmanden::Hovedreg -e 'Ombudsmanden::Hovedreg::shelltest;' csv
#    print $ARGV[0];
    print make_dump if $ARGV[0] eq "dump";
    print make_csv if $ARGV[0] eq "csv";
    print make_html if $ARGV[0] eq "html";
    print map { "$_\n" } make_toplevel if $ARGV[0] eq "top";
    print make_nodestr($ARGV[1]) if $ARGV[0] eq "node" and $ARGV[1] =~m/^(\d-|[A-Z]).*/;
    print map { "$_\n" } hreg_ids_to_catpaths(@ARGV) if shift(@ARGV) eq "paths";
}

1;
