package Ombudsmanden::HttpsLinksFilter;

use strict;
use warnings;

my $enabled = 0;

my %https_hosts;
my $default_mappings_file = "/var/www/www.ombudsmanden.dk/conf/https_mappings.conf";
our $debug = 0;
my ($mapping_file, $local_mapping_file, $hostmap, $roothost);
my $last_update = 0;

=pod

Builds a map of subdomains from the hostmap + configured roothost + $default_mappings_file
The mapping file can optionally define a different [sub]domain to map to (by writing domain => otherdomain),
otherwise the mapping falls back to domain => domain
Filters output and replaces occurrences of http://{domain} with https://{mapped_domain} if the domain is found in the map

=cut

sub initialize {
    my ($self, $obvius) = @_;

    my $config = $obvius->config;
    
    if(!$config->param('always_https_mode')) {
        $enabled = 0;
        return;
    }

    $enabled = 1;

    # Load the default mapping file
    $mapping_file = $config->param('https_mappings_file') || $default_mappings_file;
    # Load hosts from the local hostmap
    $hostmap = $obvius->config->param('hostmap') || Obvius::Hostmap->new_with_obvius($obvius);
    $roothost = $obvius->config->param('roothost');

    $local_mapping_file = $mapping_file;
    $local_mapping_file =~ s{([.][^.]+)$}{_local$1};
}

sub reload_https_map {
    my ($self) = @_;

    # Don't reload if hostmap has not been touched
    if($hostmap) {
        my $timestamp_file = $hostmap->{path};
        my $file_timestamp = (stat $timestamp_file)[9];
        if($file_timestamp <= $last_update) {
            return;
        }
        $last_update = $file_timestamp;
    }

    %https_hosts = ();

    if(-f $mapping_file) {
        $self->add_mapping_file($mapping_file);
    }

    if($hostmap) {
        # Add roothost
        $https_hosts{$roothost} = $roothost;
        for my $host (keys %{$hostmap->{forwardmap}}) {
            if($hostmap->is_https($host)) {
                $https_hosts{$host} = $host;
            }
        }
    }

    if(-f $local_mapping_file) {
        $self->add_mapping_file($local_mapping_file);
    }
}

sub add_mapping_file {
    my ($self, $filename) = @_;

    open(FH, $filename) or die "Could not open $filename";
    while(my $line = <FH>) {
        $self->add_mapping_line($line);
    }
    close(FH);
}

sub add_mapping_line {
    my ($self, $line) = @_;

    # remove leading and trailing whitespaces
    $line =~ s{^\s+|\s+$}{}gs;
    my ($host, $dest) = split(/\s*=>\s*/, $line);
    if(!$host) {
        return;
    }
    # Add mapping, defaulting to the original hostname
    $https_hosts{$host} = ($dest || $host);
}

sub filter {
    my ($self, $input) = @_;

    if(!$enabled) {
        return $input;
    }

    # Reload https map data, if needed
    $self->reload_https_map;

    $input =~ s{http://([^/\s"]+)}{$self->translate($1)}ge;

    return $input;
}

sub translate {
    my ($self, $host) = @_;

    if(my $new_host = $https_hosts{$host}) {
        return "https://${new_host}";
    } else {
        return "http://${host}";
    }
}

1;
