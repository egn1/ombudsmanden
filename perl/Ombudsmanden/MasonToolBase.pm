package Ombudsmanden::MasonToolBase;

use strict;
use warnings;
use utf8;

# Inherit basic stuff from the Obvius module, but allow
# Ombudsmanden::Toolbase to override methods.
use base qw(Obvius::MasonToolBase Ombudsmanden::ToolBase);

# Add override methods here when neccessary

1;
