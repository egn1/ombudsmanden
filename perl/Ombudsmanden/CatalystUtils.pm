package Ombudsmanden::CatalystUtils;

use strict;
use warnings;
use utf8;

use Moose;
use namespace::autoclean;

use Carp;
use Catalyst::Runtime 5.80;

extends qw(WebObvius::CatalystUtils::Base Catalyst);

our $VERSION = '0.01';
$VERSION = eval $VERSION;

__PACKAGE__->config(
    name => 'Ombudsmanden::CatalystUtils',
    # Disable deprecated behavior needed by old applications
    disable_component_resolution_regex_fallback => 1,
    setup_components => {
        search_extra => ["WebObvius::CatalystUtils::Controller"],
    }
);

sub basic_auth_realm {
    return "Ombudsmanden";
}

__PACKAGE__->setup();


1;
