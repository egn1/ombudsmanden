package Ombudsmanden::MailTools;

use strict;
use warnings;

use HTML::TreeBuilder;
use HTML::FormatText;
use Email::MIME;
use Obvius::CharsetTools qw(mixed2perl mixed2utf8);
use Obvius::Config;
use HTML::Mason::Interp;

# Hardcoded fetch of mailserver config, should be OK when running
# inside containers, as config name will never change there.
my $default_mailserver = Obvius::Config->new('ombud')->param('smtp') ||
                         'localhost';

sub textify_html {
    my ($html) = @_;

    my $tree = HTML::TreeBuilder->new_from_content($html);
    return $tree->format(HTML::FormatText->new);
}

sub html_email {
    my ($headers, $html_content, %options) = @_;

    my $text_content = $options{text_content} || textify_html($html_content);

    my $email = Email::MIME->create();
    $headers = [ %$headers ] if(ref($headers) eq 'HASH');

    for(my $i = scalar(@$headers);$i > 1;$i -= 2) {
        my $header = $headers->[$i-2];
        my $value = $headers->[$i-1];
        $value = join(", ", @$value) if(ref($value) eq 'ARRAY');
        $email->header_str_set($header, mixed2perl($value));
    }

    $email->content_type_set("multipart/mixed");
    $email->encoding_set('8bit');
    $email->disposition_set('inline');

    $email->parts_set([
        Email::MIME->create(
            attributes => { content_type => "multipart/alternative" },
            parts => [
                Email::MIME->create(
                    attributes => {
                        content_type => "text/plain",
                        encoding => "quoted-printable",
                        charset => 'utf-8',
                    },
                    body => mixed2utf8($text_content)
                ),
                Email::MIME->create(
                    attributes => {
                        content_type => "text/html",
                        encoding => "quoted-printable",
                        charset => 'utf-8',
                    },
                    body => mixed2utf8($html_content)
                ),
            ]
        ),
        @{$options{extra_parts} || []}
    ]);


    send_mime_mail($email, %options) if($options{send_after_generating});

    return $email->as_string;
}

sub extract_emails_from_header {
    my ($header) = @_;

    return () unless($header);

    $header = [split(/\s*,\s*/, $header)] unless(ref($header) eq 'ARRAY');
    my @result;
    foreach my $addr (@$header) {
        if($addr =~ m!<([^>]+)>.*?$!) {
            push(@result, $1);
        } else {
            push(@result, $addr);
        }
    }

    return @result;
}

sub send_mime_mail {
    my ($email, %options) = @_;

    unless(ref($email) eq 'Email::MIME') {
        $email = Email::MIME->new($email);
    }

    my $headers = $email->header_obj;

    my $from = $headers->header_raw("From");
    my $to = $headers->header_raw("To");
    my $cc = $headers->header_raw("Cc");
    my $bcc = $headers->header_raw("Cc");

    send_mail(
        $from,
        $to,
        $email->as_string,
        cc => $cc,
        bcc => $bcc,
        %options
    );
}

sub send_mail {
    my ($from, $to, $body, %options) = @_;

    # Make all options lowercase
    foreach my $k (keys %options) {
        $options{lc($k)} = $options{$k};
    }

    my @recipients = (
        extract_emails_from_header($to),
        extract_emails_from_header($options{cc}),
        extract_emails_from_header($options{bcc}),
    );

    die "No recipients specified" unless(@recipients);

    my $mailserver = $options{mailserver} || $default_mailserver;
    my $smtp = Net::SMTP->new(
        $mailserver,
        Debug => ($options{debug} || $options{debug_mail_sending} || 0)
    );
    die "Could not connect to mailserver '$mailserver'" unless($smtp);

    $smtp->mail((extract_emails_from_header($from))[0])
        or die "Could not specify sender";

    foreach my $recipient (@recipients) {
        # Parse names like "Name Lastname <email@example.com>"
        $smtp->to($recipient)
            or die "Could not specify recipient '$recipient'";
    }

    $smtp->data($body) or die "Could not send message body";

    $smtp->quit or die "Failed to close connection to mail server";
}

sub make_obvius {
    my ($obj) = @_;

    $obj ||= 'ombud';

    my $ref = ref($obj);

    # Might already be an obvius object
    return $obj if($ref eq 'Obvius');

    # Could be an Obvius::Config
    if($ref eq 'Obvius::Config') {
        require "Obvius.pm";
        return Obvius->new($obj);
    }

    # Now fail unless we're dealing with a scalar
    die "Do not know how to make obvius from $obj" if($ref);

    # Assume we have a scalar and try to make a config from it
    require "Obvius.pm";

    my $config = Obvius::Config->new($obj);
    die "Could not make an obvius config for $obj" unless($config);

    return Obvius->new($config);
}

sub expand_template {
    my ($obvius, $template, %context) = @_;

    $obvius = make_obvius($obvius);
    my $base_dir = $obvius->config->param('sitebase');
    die "No sitebase defined" unless($base_dir);
    $base_dir =~ s{/$}{};

    my $output = '';
    my $user = $ENV{MOD_PERL} ? 'mod_perl' :
               $ENV{USER} || getpwuid($<);
    die "No username in environment" unless($user);
    my $interp = new HTML::Mason::Interp(
        comp_root => $base_dir . '/mason/mail/',
        data_dir => $base_dir . "/var/masontmp_for_${user}" ,
        out_method => \$output,
        # This ensures that stuff like translation is available
        in_package => "WebObvius::MasonCommands",
    );

    $interp->exec($template, obvius => $obvius, %context);
    return $output;

}

1;