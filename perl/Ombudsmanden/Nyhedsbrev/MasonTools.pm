package Ombudsmanden::Nyhedsbrev::MasonTools;

use strict;
use warnings;
use utf8;

use Exporter;
use Ombudsmanden::Nyhedsbrev;

our @ISA = qw(Exporter);

my $mason_package = '';

sub import {
    if(!$mason_package) {
        my $m = $HTML::Mason::Commands::m;
        $mason_package = $m->{ah}->{interp}->{compiler}->{in_package};
    }

    Ombudsmanden::Nyhedsbrev::MasonTools->export_to_level(1, @_);
}

sub mason_variable {
    my ($self, $varname) = @_;

    if($varname eq "m") {
        return $HTML::Mason::Commands::m->instance;
    }

    my $full_var =  ${mason_package} . "::" . ${varname};
    no strict 'refs';
    my $var = ${$full_var};
    use strict 'refs';

    return $var;
}

sub m { $_[0]->mason_variable("m") }
sub r { $_[0]->mason_variable("r") }
sub obvius { $_[0]->mason_variable("obvius") }
sub doc { $_[0]->mason_variable("doc") }
sub vdoc { $_[0]->mason_variable("vdoc") }

sub instance {
    my $r = $_[0]->r;

    if(!$r->pnotes('nyhedsbrev_tool')) {
        $r->pnotes('nyhedsbrev_tool' => bless({}, __PACKAGE__))
    }

    return $r->pnotes('nyhedsbrev_tool');
}


# Adds a message to the messages system in Obvius, regardles
# of whether messages are stored in session or on pnotes.
sub add_message {
    my ($self, $message, $status) = @_;

    $status ||= "OK";

    my $r = $self->r;

    my $session = $r->pnotes('obvius_session');
    my ($messages, $existing_status);
    if($session) {
        $existing_status = $session->{status};
        $messages = $session->{message};
        $messages = [$messages] unless(ref($messages));
    } else {
        $existing_status = $r->pnotes('status');
        $messages = $r->pnotes("message");
    }
    if($existing_status && $existing_status ne $status) {
        warn("Mixing message types $existing_status and $status");
    }

    $messages ||= [];
    $messages = [$messages] unless(ref($messages));

    push(@$messages, $message);

    if($session) {
        $session->{status} = $status;
        $session->{message} = $messages;
    } else {
        $r->pnotes(status => $status);
        $r->pnotes('message' => $messages);
    }

}

# Adds an error to the messages system in Obvius, regardles
# of whether messages are stored in session or on pnotes.
sub add_error {
    my ($self, $error) = @_;

    return $self->add_message($error, "ERROR");
}

sub data {
    my ($self) = @_;

    my $r = $self->r;
    my $obvius = $self->obvius;

    unless($r->pnotes('nyhedsbrev_data')) {
        $r->pnotes(
            nyhedsbrev_data => Ombudsmanden::Nyhedsbrev->new($obvius)
        );
    }

    return $r->pnotes('nyhedsbrev_data');
}

# Calculate whether title should be white by checking if the sum of the
# rpg color codes is less than 3 times 128.
sub hex2titleclass {
    my ($self, $hexcode) = @_;

    my ($r, $g, $b) = ($hexcode =~ m{^\#(..)(..)(..)});
    if(!defined($r)) {
        return '';
    }
    my $sum = 0;
    foreach my $h (($r, $g, $b)) {
        my $v = hex("0x${h}");
        $sum += $v;
    }

    if($sum < 128*3) {
        return "white";
    }

    return "";
}

sub redirect {
    my ($self, $url) = @_;

    my $r = $self->r;
    my $m = $self->m;

    # Release the session
    if(my $session = $r->pnotes('obvius_session')) {
        my $session_id = $session->{_session_id};
        if($session_id) {
            my $separator = ($url =~ m{\?}) ? "&" : "?";
            $url .= $separator . "obvius_session_id=" . $session_id;
        }
    }

    return $m->redirect($url);
}

sub redirect_with_message {
    my ($self, $message, $url) = @_;

    $url ||= "?obvius_command_subscription_areas=1";

    $self->add_message($message);
    $self->redirect($url);
}
1;