package Ombudsmanden::DocType;

########################################################################
#
# DocType.pm - Namespace placeholder
#
# Copyright (C) 2004 Magenta ApS, Denmark (http://www.magenta-aps.dk/)
#
# Author: Martin Sk�tt (martin@magenta-aps.dk)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
#
########################################################################

# $Id: DocType.pm,v 1.2 2004/11/25 14:39:03 martin Exp $

use strict;
use warnings;

use Obvius::DocType;
our @ISA = qw( Obvius::DocType );
our ( $VERSION ) = '$Revision: 1.2 $ ' =~ /\$Revision:\s+([^\s]+)/;

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

Ombudsmanden::DocType - Perl extension for blah blah blah

=head1 SYNOPSIS

  use Ombudsmanden::DocType;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for Ombudsmanden::DocType, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.


=head1 AUTHOR

Martin Sk�tt, E<lt>marrtin@magenta-aps.dkE<gt>

=head1 SEE ALSO

L<perl>.

=cut
