package Ombudsmanden::Cache::SolrUpdater;

use strict;
use warnings;

use Data::Dumper;

my $script = "/var/www/www.ombudsmanden.dk/bin/new_solr_build_index.pl";

my $count = 0;

sub new {
    my ($class, $obvius, %options) = @_;

    return bless {obvius => $obvius}, $class;
}

sub find_and_flush {
    my ($this, $cache_objs) = @_;
    
    my $docids = $this->find_dirty($cache_objs);

    if(scalar(@$docids)) {
        $this->flush({
            command => "update_solr",
            items => $docids
        });
    }    
}

sub flush {
    my ($this, $commands) = @_;

    my $obvius = $this->{obvius};
    my $config = $obvius->config;
    
    $commands = [$commands] if (ref($commands) ne 'ARRAY');
    my $confname = $this->{obvius}->config->param('name');
    for my $cmd (grep { ($_->{command} || '') eq 'update_solr' } @$commands) {
        my $items = $cmd->{items} || [];
        if(@$items) {
            my $tmpfile = "/tmp/SolrUpdater-$$-$count.txt";
            $count++;
            open(FH, ">$tmpfile") or die "Couldn't open $tmpfile for writing";
            for my $i (@$items) {
                print FH $i, "\n";
            }
            close(FH);
            # print STDERR "Running perl $script $confname $tmpfile >/dev/null &\n";
            my $command = join(" ",
                "/usr/bin/perl",
                $script,
                "--core", ($config->param('solr_core') || "ombud2017"),
                "--confname", ($config->param('name') || "ombud"),
                "--from-file", $tmpfile
            );
            system("$command >/dev/null &");
        }
    }
}

sub find_dirty {
    my ($this, $objects) = @_;

    return [] unless(
        $objects and
        $objects->{collection} and
        @{$objects->{collection}}
    );
    $objects = $objects->{collection};
    # Maps the following to unique docids:
    # - docids
    # - uris
    # - recursive clearing by docid
    # - recursive clearing by path

    my @paths;
    my @docids;
    my @rec_paths;
    my @rec_docids;

    for my $obj (@$objects) {
        # Skip clear cache requests from the web interface
        next if($obj->{clear_cache_request});

        if($obj->{clear_recursively}) {
            if(my $p = $obj->{uri}) {
                push(@rec_paths, "rec:" . $p);
            } elsif(my $id = $obj->{docid}) {
                push(@rec_docids, "rec:" . $id);
            }
        } elsif($obj->{document_moved}) {
            push(@rec_docids, $obj->{docid});
        } else {
            if(my $id = $obj->{docid}) {
                push(@docids, $id);
            } elsif(my $p = $obj->{uri}) {
                push(@paths, $p);
            }
        }
    }

    return [
        @rec_docids,
        @rec_paths,
        @paths,
        @docids
    ];
}

1;