package Ombudsmanden::Cache::HovedKategori;

use strict;
use warnings;

use Cache::FileCache;
use HTML::Mason::Utils;
use Obvius::CharsetTools;

our $VERSION = "1.0";

sub new {
    my ($class, $obvius, %options) = @_;

    return bless {obvius => $obvius}, $class;
}

sub find_and_flush {
    my ($this, $cache_objs) = @_;

    my $dirty = $this->find_dirty($cache_objs);
    if(ref($dirty)) {
        return unless(@$dirty);
        my $cache = $this->get_cache;
        for my $docid (@$dirty) {
            $cache->remove($docid);
        }
    } elsif($dirty eq 'clear all') {
        $this->get_cache->clear();
    }
}

sub find_dirty {
    my ($this, $objects) = @_;

    return [] unless(
        $objects and
        $objects->{collection} and
        @{$objects->{collection}}
    );
    $objects = $objects->{collection};

    my $obvius = $this->{obvius};
    my %docids;

    for my $obj (@$objects) {
        # Skip clear cache requests from the web interface
        next if($obj->{clear_cache_request});

        if(my $uri = $obj->{uri}) {
            # If document was deleted, we should just clear all
            return "clear all" unless($obvius->lookup_document($uri));
        }
        if(my $docid = $obj->{docid}) {
            $docids{$docid}++;
        } 
    }

    my %cats;
    foreach my $docid (keys %docids) {
        if(my $doc = $obvius->get_doc_by_id($docid)) {
            my $vdoc = $obvius->get_public_version($doc);
            # If we can't find a vdoc, clear all
            return "clear all" unless($vdoc);
            my $hoved_cat = $obvius->get_version_field($vdoc, 'cat_hoved');
            next unless($hoved_cat);
            $hoved_cat = [ $hoved_cat ] unless(ref($hoved_cat));
            for my $cat (@$hoved_cat) {
                $cats{$cat}++;
            }
        }
    }

    my %dirty_docids=();
    foreach my $cat (keys %cats) {
        my $ref_path = $this->build_anchor_link($cat, 1);

        my @path = $obvius->get_doc_by_path($ref_path);
        for my $doc (@path) {
            $dirty_docids{ $doc->Id } = 1;
        }
    }
    return [ keys %dirty_docids ];
}

# This method converts a category string to a valid url. and anchor Eg.:
# Søfart 12.3 is converted to /udtalelser/hovedregister/s_fart/1/2/3/#S_fart_12.3
sub build_anchor_link {
    my ($self, $cat, $skip_anchor) = @_;

    # Remove any utf8-encoding
    $cat = Obvius::CharsetTools::mixed2perl($cat);

    my @data = split(/\s/, $cat);

    my $sub_cats = pop(@data); #Numeric sub categories (12.3)
    $sub_cats =~ s/\.//;
    my @sub_cats = split(//, $sub_cats);
    $sub_cats = join ("/",@sub_cats);

    my $head_cat = lc(join("_", @data));
    $head_cat =~ tr/a-z/_/c; # I don't think 's' (squash) is right?

    my $anchor='';
    unless ($skip_anchor) {
        $anchor = $cat;
        $anchor =~ tr/a-zA-Z1-9./_/cs;
        $anchor = "#" . $anchor;
    }

    my $link = join("/",
        "/da/find/udtalelser/hovedregister",
        $head_cat,
        $sub_cats,
        $anchor
    );
    return $link;
}

sub get_cache {
    my ($this) = @_;
    my $namespace = HTML::Mason::Utils::data_cache_namespace(
        "[method 'get_num_of_here_docs_below' of " .
        "/sitecomp/doctypes/KategoriDokument]",
    );
    return Cache::FileCache->new({
        namespace => $namespace,
        cache_root => '/var/www/www.ombudsmanden.dk/var/common/cache/'
    });
}

1;