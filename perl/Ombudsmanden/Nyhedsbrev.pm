package Ombudsmanden::Nyhedsbrev;

=head1 NAME

  Ombudsmanden::Nyhedsbrev

=cut

use strict;
use warnings;
use utf8;

use lib '/var/www/obvius/perl';

use Obvius;
use Obvius::Config;
use Obvius::CharsetTools qw(mixed2utf8 mixed2perl);

=head1 SYNOPSIS

This module contains static registrations of newsletter paths and
configurations used on the www.ombudsmanden.dk website.

Usage:

  use Ombudsmanden::Nyhedsbrev;

  my $newsletter_data = Ombudsmanden::Nyhedsbrev->new($obvius);

  my %mappings = $newsletter_data->area_mappings;
  my @newsletters = $newsletter_data->newsletters;

  ...

=cut

# Keep track of whenter global initialization has been done
my $initialized = 0;

# Cached Link doctype object.
my $link_doctype;

# Keys for settings that can be overwritten using config
my @config_keys = qw(
    subject
    header_image
    title
    header_background_color
    subscription_uri
);

# Keys for area settings that can be overwritten in config
my @area_config_keys = qw(
    area_title
);

# This allows mapping one area to work as a virtual area that maps to multiple
# root paths.
my %virtual_area_mappings;

# Collection of newsletters on the site.
# Each newsletter might consist of multiple subscription areas.
my @default_newsletters = (
    {
        config_name => "nyheder",
        subject => 'Nyhed fra ombudsmanden',
        header_image => 'nyheder',
        title => 'Nyheder',
        header_background_color => '#fdf3f3',
        sections => [
            {
                config_name => "nyheder",
                area_title => '',
                paths => [
                    '/da/find/nyheder/alle/',
                ],
            },
        ]
    },
    {
        config_name => "faglig_opdatering",
        subject => 'Faglig opdatering fra ombudsmanden',
        header_image => 'fagligopdatering',
        title => 'Faglig opdatering',
        header_background_color => '#002d65',
        teaser => q|
            Faglig opdatering udsendes hver anden måned og indeholder nye -
            eller væsentligt opdaterede - overblik fra Myndighedsguiden og
            udvalgte sager og rapporter.
        |,
        subscription_uri => "/da/overblik/",
        sections => [
            {
                config_name => "fra_myndighedsguiden",
                area_title => 'Nye overblik i Myndighedsguiden',
                paths => [
                    '/da/myndighedsguiden/generel_forvaltningsret/',
                    '/da/myndighedsguiden/specifikke_sagsomraader/',
                ],
            },
            {
                config_name => "sager_og_rapporter",
                area_title => 'Udtalelser og rapporter',
                paths => [
                    '/da/find/udtalelser/beretningssager/alle_bsager/',
                    '/da/find/rapporter/'
                ],
            },
        ]
    },
    {
        config_name => "skatteomraadet",
        subject => 'Opdatering fra ombudsmandens skatteside',
        header_image => 'skat',
        title => 'Skat',
        header_background_color => '#fdf3f3',
        sections => [
            {
                config_name => "skatteomraadet",
                area_title => '',
                paths => [
                    '/da/ombudsmandensarbejde/skatteomraadet/',
                ]
            }
        ]
    },
    {
        config_name => "ledigestillinger",
        subject => 'Ledige stillinger ved Folketingets Ombudsmand',
        header_image => 'ledigestillinger',
        title => 'Ledige stillinger',
        header_background_color => '#edf4fe',
        sections => [
            {
                config_name => "ledigestillinger",
                area_title => 'Ledige stillinger ved Folketingets Ombudsmand',
                paths => [
                    '/da/jobs/',
                ],
            }
        ]
    },
);

sub new {
    my ($classname, $obvius) = @_;

    if(!$obvius) {
        die "You must specify an Obvius object when calling " .
            __PACKAGE__ . "::new";
    }
    
    my $obj = bless({obvius => $obvius}, $classname);
    $obj->initialize;
    
    return $obj;
}

sub new_from_confname {
    my ($classname, $confname) = @_;

    my $config = Obvius::Config->new($confname);
    my $obvius = Obvius->new($config);
    return $classname->new($obvius);
}

# Obvius accessor
sub obvius { return $_[0]->{obvius} }

sub initialize {
    my ($self) = @_;
    
    if($initialized) {
        return;
    }
    
    my $obvius = $self->obvius;

    $link_doctype = $obvius->get_doctype_by_name("Link");
    
    $self->populate_virtual_area_mappings;
    
    $initialized = 1;
}

# Adjust newsletter data from local configuration, allowing the adjustment
# of URLs, titles and the like.
sub localize_default_newsletters {
    my ($self) = @_;

    my $obvius = $self->obvius;
    my $config = $obvius->config;

    my @result;
    
    foreach my $data (@default_newsletters) {
        
        my $config_name = $data->{"config_name"};
        if(!$config_name) {
            die "No config name specified for area " . $data->{title};
        }
        foreach my $config_key (@config_keys) {
            my $config_val = $config->param(
                "newsletter_${config_name}_${config_key}"
            );
            if($config_val) {
                $data->{$config_key} = mixed2perl($config_val);
            }
        }
        my @sections;
        foreach my $area (@{ $data->{sections} }) {
            
            my $config_name = $area->{"config_name"};

            foreach my $config_key (@area_config_keys) {
                my $config_val = $config->param(
                    "newsletter_area_${config_name}_${config_key}"
                );
                if($config_val) {
                    $area->{$config_key} = mixed2perl($config_val);
                }
                my $uri_key = "newsletter_area_${config_name}_paths";
                my $uri_idx = 0;
                foreach my $url (@{ $area->{paths} }) {
                    my $config_val = $config->param("${uri_key}_${uri_idx}");
                    if($config_val) {
                        $area->{paths}->[$uri_idx] = $config_val;
                    }
                    $uri_idx++;
                }
                # Check for added URIs
                while(my $val = $config->param("${uri_key}_${uri_idx}")) {
                    $area->{paths}->[$uri_idx] = $val;
                    $uri_idx++;
                }
            }
            
            push(@sections, $area);
        }

        $data->{sections} = \@sections;
        
        push(@result, $data);
    }

    @default_newsletters = @result;
}

# Populates virtual_area_mappings so any area that has the key
# "subscription_uri" will become a virtual area which will look in the
# individual folders for each sub-area for subscription documents.
sub populate_virtual_area_mappings {
    my ($self) = @_;

    foreach my $area ($self->newsletters) {
        if(my $master_uri = $area->{subscription_uri}) {
            my @area_paths = map {
                @{ $_->{paths} }
            } @{ $area->{sections} };
            $virtual_area_mappings{$master_uri} = \@area_paths;
        }
    }
}

=head1 METHODS

=head2 area_mappings

Returns a hash that maps subscription areas to a list of source folders.

Usage:

  my %mappings = $newsletter_data->area_mappings;

=cut
sub area_mappings { return %virtual_area_mappings }

=head2 newsletters

Usage:

Returns a list of subscribeable areas, their settings and subpartitions.

  my @newsletters = $newsletter_data->newsletters;

=cut
sub newsletters {
    my ($self) = @_;

    my $obvius = $self->obvius;

    unless($self->{cached_newsletters}) {
        my @newsletters;

        my @areas = $self->search_areas({}, order_by => "id");

        foreach my $area (@areas) {
            if($area->{root_docid}) {
                my $doc = $obvius->get_doc_by_id($area->{root_docid});
                if($doc) {
                    $area->{subscription_uri} = $obvius->get_doc_uri($doc);
                }
            }

            push(@newsletters, $area);



            my @sections = $self->search_sections(
                {area_id => $area->{id}},
                order_by => "id"
            );
            $area->{sections} = \@sections;
            foreach my $section (@sections) {
                my @paths = $self->search_paths(
                    {section_id => $section->{id}},
                    order_by => "id"
                );
                $section->{paths} = [ map { $_->{path} } @paths];
                $section->{path_data} = \@paths;
            }
        }

        $self->{cached_newsletters} = \@newsletters;
    }

    return @{$self->{cached_newsletters}}
}

=head2 newsletters_copy

Usage:

Returns a copy of the list of subscribeable areas, their settings and
subpartitions. This so the copy can be manipulated without changing
the original source.

  my @newsletters = $newsletter_data->newsletters_copy;

=cut
sub newsletters_copy {
    my ($self) = @_;
    my @result;
    foreach my $nl ($self->newsletters) {
        my %nl_copy = %$nl;
        my @sections;
        foreach my $section (@{ $nl->{sections} }) {
            my %section = %$section;
            $section{paths} = [@{ $section->{paths}}];
            push(@sections, \%section);
        }
        $nl_copy{sections} = \@sections;
        push(@result, \%nl_copy);
    }
    
    return @result;
}


# Cache for storing areas that has been resolved
my %resolve_area_cache;

# Cache for storing mappings of areas to the docid that is registered in
# subscriptions for the area.
my %area_to_subscription_docid;

=head2 resolve_area_uri

Converts an URI into a list of root-documents for the area by applying
virtual area mappings and following Link documents.

Usage:

  my @obvius_docs = $newsletter_data->resolve_area_uri($uri);

=cut
sub resolve_area_uri {
    my ($self, $uri) = @_;
    
    my $obvius = $self->obvius;

    if(exists $resolve_area_cache{$uri}) {
        return @{$resolve_area_cache{$uri}};
    }

    my @result;
    if(exists $virtual_area_mappings{$uri}) {
        @result = map {
            $_ eq $uri ?
            $obvius->lookup_document($uri) :
            $self->resolve_area_uri($_)
        } @{ $virtual_area_mappings{$uri} };
    } else {
        my $doc = $obvius->lookup_document($uri);
        if($doc) {
            my $vdoc = $obvius->get_public_version($doc) ||
                       $obvius->get_latest_version($doc);
            if($vdoc) {
                if($vdoc->Type == $link_doctype->Id) {
                    $obvius->get_version_fields($vdoc, ['url']);
                    if(my $url = $vdoc->field('url')) {
                        if($url eq $uri) {
                            @result = ($doc);
                        } else {
                            @result = $self->resolve_area_uri($url);
                        }
                    }
                } else {
                    @result = ($doc);
                }
            }
        }
    }

    # Save a reverse map so we can map resolved areas back to the subscription
    # docid that is used to set the last_update.
    if(my $doc = $obvius->lookup_document($uri)) {
        foreach my $res_doc (@result) {
            $area_to_subscription_docid{$res_doc->Id} = $doc->Id;
        }
    }

    $resolve_area_cache{$uri} = \@result;
    return @result;
}

=head2 resolve_subscription_docid

Finds the docid that is used in the subscription table for a given
subscribable area, specified by a docid.

Usage:

  my $sub_docid = $newsletter_data->resolve_subscription_docid($docid);

=cut
sub resolve_subscription_docid {
    my ($self, $docid) = @_;

    while(
        exists $area_to_subscription_docid{$docid} &&
        $area_to_subscription_docid{$docid} != $docid
    ) {
        $docid = $area_to_subscription_docid{$docid};
    }

    return $docid;
}

=head2 subscription_roots

Returns a list of documents that are roots under which newsletter content
is located.

Usage:

  my @root_docs = $newsletter_data->subscription_roots;

=cut
sub subscription_roots {
    my ($self) = @_;

    my @result;
    
    foreach my $newsletter ($self->newsletters) {
        if(my $uri = $newsletter->{subscription_uri}) {
            push(@result, $self->resolve_area_uri($uri));
        } else {
            foreach my $area (@{ $newsletter->{sections} }) {
                foreach my $uri (@{ $area->{paths} }) {
                    push(@result, $self->resolve_area_uri($uri));
                }
            }
        }
    }
    
    return @result;
}

=head2 subscribeable_paths

Returns a list of the URIs subscribers can subscribe to. All area source
URLs will be returned, whether they are public or not.

Usage:

  my @urls = $newsletter_data->subscribeable_paths;

=cut
sub subscribeable_paths {
    my ($self) = @_;

    return grep { $_ } map {
        $_->{subscription_uri}
    } $self->newsletters;
}

=head2 subscription_choices_data

Returns a list of newsletters that subscribers can subscribe to. Each
newsletter will be marked up with additional data specifying if its
root exists and whether the root is public.

Usage:

  my @list = $newsletter_data->subscription_choices_data;

  foreach my $nl (@list) {
    if($nl->{exists} && $nl->{is_public}) {
        print "Newsletter docid is ", $nl->{docid}, " and it is public\n";
    }
  }
  
=cut
sub subscription_choices_data {
    my ($self) = @_;

    my $obvius = $self->obvius;
    
    my @result;
    
    foreach my $newsletter ($self->newsletters_copy) {
        my $subscription_uri = (
            $newsletter->{subscription_uri} ||
            $newsletter->{sections}->[0]->{paths}->[0]
        );
        $newsletter->{root_uri} = $subscription_uri;
        my $subscription_doc = $obvius->lookup_document($subscription_uri);
        
        if($subscription_doc) {
            $newsletter->{docid} = $subscription_doc->Id;
            $newsletter->{exists} = 1;
            $newsletter->{is_public} = $obvius->is_public_document(
                $subscription_doc
            );
        } else {
            $newsletter->{docid} = undef;
            $newsletter->{exists} = 0;
            $newsletter->{is_public} = 0;
        }
        push(@result, $newsletter);
    }
    
    return @result;
}

=head2 subscribeable_vdocs

Returns a list of $vdoc objects for documents subscribers can subscribe to.
Only public documents with a fully public path will be returned.

Usage:

  my @vdocs = $newsletter_data->subscribeable_vdocs;

=cut
sub subscribeable_vdocs {
    my ($self) = @_;

    my @paths = $self->subscribeable_paths;

    if(@paths) {
        my $obvius = $self->obvius;

        my $sort_val = 0;
        my %sort_map = map { $_ => $sort_val++ } @paths;
        my $paths = join(", ", map { $obvius->dbh->quote($_) } @paths);

        my $vdocs = $obvius->search(
            ['title'], # Add title so it can be extracted from results
            "path in ($paths)",
            notexpired=>1,
            public=>1
        ) || {};

        return sort {
            $sort_map{$a->param('path')} <=> $sort_map{$b->param('path')}
        } @$vdocs;
    } else {
        # Return an empty list
        return @paths;
    }
}

sub is_subscribeable_uri {
    my ($self, $uri) = @_;

    $uri ||= '';

    my $doc = $self->obvius->lookup_document($uri);
    return 0 unless($doc);

    my $result = $self->get_area(root_docid => $doc->Id);

    return $result ? 1 : 0;
}


# Get pending documents since last newsletter was sent out.
# $subscriptio_area can be either an Obvius document ($doc),
# an obvius uri ($uri) or a docid.
sub get_pending_documents {
    my ($self, $subscription_area) = @_;

    my $uri;
    my $docid;
    my $doc;
    my $obvius = $self->obvius;
    my @result;

    my $ref = ref($subscription_area);
    if(!$ref) {
        if($subscription_area =~ m{^\d+$}) {
            $docid = $subscription_area;
            $doc = $obvius->get_doc_by_id($docid);
            $uri = $obvius->get_doc_uri($doc);
        } else {
            $uri = $subscription_area;
            $doc = $obvius->lookup_document($uri);
            $docid = $doc->Id;
        }
    } elsif($ref eq "Obvius::Document") {
        $doc = $subscription_area;
        $uri = $obvius->get_doc_uri($doc);
        $docid = $doc->Id;
    } else {
        warn("Don't know how to look up pending subscriptions from " .
             "$subscription_area ($ref)");
        return @result;
    }

    my $sth = $obvius->dbh->prepare(q|
        select MAX(last_update)
        from subscriptions
        where docid = ?
    |);
    $sth->execute($docid);

    my ($most_recent) = $sth->fetchrow_array;
    my $last_update_condition = "1=1";
    if($most_recent) {
        $last_update_condition = "published > '$most_recent'";
    }

    my @roots = $self->resolve_area_uri($uri);

    foreach my $root_uri (
        map { $obvius->get_doc_uri($_) } @roots
    ) {
        my $qpath = $obvius->dbh->quote($root_uri . '%');
        my $new_docs = $obvius->search(
            ['in_subscription', 'published', 'docdate'],
            "in_subscription > 0 and " .
            "$last_update_condition and " .
            "path like $qpath",
            notexpired => 1,
            public => 1,
            order => "docdate"
        ) || [];
        for my $v (@$new_docs) {
            $obvius->get_version_fields($v, ['title', 'teaser', 'docdate']);
            my $d = $obvius->get_doc_by_id($v->DocId);
            $v->param('uri' => $obvius->get_doc_uri($d));
        }
        push(@result, @$new_docs);
    }

    return @result;
}


my %table_fields = (
    subscription_areas => [qw(
        title config_name subject root_docid banner_text
        header_background_color header_image
    )],
    subscription_area_sections => [qw(area_id area_title)],
    subscription_area_paths => [qw(section_id path)],
);

# DB methods
sub create_area {
    my ($self, $area_data) = @_;

    my $sth = $self->obvius->dbh->prepare(q|
        INSERT INTO subscription_areas
            (title, config_name, subject, root_docid,
             banner_text, header_background_color, header_image)
        VALUES
            (?, ?, ?, ?, ?, ?, ?)
    |);
    $sth->execute(
        $area_data->{title} || '',
        $area_data->{config_name} || '',
        $area_data->{subject} || '',
        $area_data->{root_docid} || 0,
        $area_data->{banner_text} || '',
        $area_data->{header_background_color} || '',
        $area_data->{header_image} || '',
    );
    return $self->obvius->dbh->{mysql_insertid};
}

sub create_section {
    my ($self, $section_data) = @_;

    my $sth = $self->obvius->dbh->prepare(q|
        INSERT INTO subscription_area_sections
            (area_id, area_title)
        VALUES
            (?, ?)
    |);
    $sth->execute(
        $section_data->{area_id},
        $section_data->{area_title} || '',
    );

    return $self->obvius->dbh->{mysql_insertid};
}

sub create_path {
    my ($self, $path_data) = @_;

    my $sth = $self->obvius->dbh->prepare(q|
        INSERT INTO subscription_area_paths
            (section_id, path)
        VALUES
            (?, ?)
    |);
    $sth->execute(
        $path_data->{section_id},
        $path_data->{path} || '',
    );

    return $self->obvius->dbh->{mysql_insertid};
}

# Converts a hash into SQL conditions with placeholders and the args
# that should be used for matching them
sub build_sql_conditions {
    my ($self, %filter) = @_;

    if(!%filter) {
        return ('1=1', []);
    }

    my @keys = sort keys %filter;

    my $conditions = "(" . join(") AND (", map { "`${_}` = ?" } @keys) . ")";
    my @args = map { $filter{$_} } @keys;
    
    return ($conditions, \@args);
}

sub build_update {
    my ($self, $fieldlist, $data) = @_;

}

sub update_table {
    my ($self, $tablename, $new_data, $condition) = @_;

    my @set_sql;
    my @args;

    my $fieldlist = $table_fields{$tablename};

    foreach my $field (@$fieldlist) {
        if(exists $new_data->{$field}) {
            push(@set_sql, "`$field` = ?");
            push(@args, $new_data->{$field});
        }
    }

    if(!@set_sql) {
        return;
    }

    my $set_sql = join(", ", @set_sql);

    my ($condition_sql, $condition_args) = $self->build_sql_conditions(
        %$condition
    );

    push(@args, @$condition_args);

    my $sth = $self->obvius->dbh->prepare(qq|
        UPDATE `${tablename}`
            SET ${set_sql}
        WHERE
            (${condition_sql})
    |);
    $sth->execute(@args);
    
}

sub update_areas {
    my ($self, $new_data, $condition) = @_;

    return $self->update_table(
        "subscription_areas",
         $new_data, $condition
    );
}

sub update_sections {
    my ($self, $new_data, $condition) = @_;

    return $self->update_table(
        "subscription_area_sections",
         $new_data, $condition
    );
}

sub update_paths {
    my ($self, $new_data, $condition) = @_;

    return $self->update_table(
        "subscription_area_paths",
         $new_data, $condition
    );
}

sub search_table {
    my ($self, $tablename, $condidtion_hash, %options) = @_;

    my ($conditions, $args) = $self->build_sql_conditions(%$condidtion_hash);

    my @extra;

    if(my $order_by = $options{order_by}) {
        push(@extra, "ORDER BY $order_by");
    }
    if(my $group_by = $options{group_by}) {
        push(@extra, "GROUP BY $group_by");
    }

    if(my $limit = $options{limit}) {
        if(my $offset = $options{offset}) {
            push(@extra, "LIMIT $offset, $limit");
        } else {
            push(@extra, "LIMIT $limit");
        }
    }

    my $extra = join("\n        ", @extra);
    my $sth = $self->obvius->dbh->prepare(qq|
        SELECT *
        FROM `${tablename}`
        WHERE ${conditions}
        $extra
    |);
    $sth->execute(@$args);
    my @result;
    while(my $res = $sth->fetchrow_hashref) {
        push(@result, $res);
    }
    $sth->finish;

    return @result;
}

sub search_areas {
    my ($self, $conditions, %options) = @_;

    return $self->search_table(
        "subscription_areas", $conditions, %options
    );
}

sub search_sections {
    my ($self, $conditions, %options) = @_;

    return $self->search_table(
        "subscription_area_sections", $conditions, %options
    );
}

sub search_paths {
    my ($self, $conditions, %options) = @_;

    return $self->search_table(
        "subscription_area_paths", $conditions, %options
    );
}


sub find_from_table {
    my ($self, $tablename, %by) = @_;

    my @result = $self->search_table(
        $tablename,
        \%by,
        limit => 1
    );

    return $result[0];
}

sub get_area {
    my ($self, %by) = @_;

    return $self->find_from_table("subscription_areas", %by)
}

sub get_section {
    my ($self, %by) = @_;

    return $self->find_from_table("subscription_area_sections", %by)
}

sub get_path {
    my ($self, %by) = @_;

    return $self->find_from_table("subscription_area_paths", %by)
}

sub delete_from_table {
    my ($self, $tablename, %by) = @_;

    my ($conditions, $args) = $self->build_sql_conditions(%by);

    my $sth = $self->obvius->dbh->prepare(qq|
        DELETE
        FROM `${tablename}`
        WHERE ${conditions}
    |);
    $sth->execute(@$args);
}

sub delete_areas {
    my ($self, %by) = @_;

    return $self->delete_from_table("subscription_areas", %by)
}

sub delete_sections {
    my ($self, %by) = @_;

    return $self->delete_from_table("subscription_area_sections", %by)
}

sub delete_paths {
    my ($self, %by) = @_;

    return $self->delete_from_table("subscription_area_paths", %by)
}

sub import_defaults {
    my ($self) = @_;

    my $obvius = $self->obvius;

    # Update with any overwritings from config
    $self->localize_default_newsletters;

    foreach my $area (@default_newsletters) {
        my $subscription_uri =
            $area->{subscription_uri} || $area->{sections}->[0]->{paths}->[0];

        my $doc = $obvius->lookup_document($subscription_uri);
    
        # Check if area already registered
        my $existing = $self->get_area(config_name => $area->{config_name});
        next if($existing);

        my $area_id = $self->create_area({
            title => $area->{title},
            config_name => $area->{config_name},
            subject => $area->{subject},
            root_docid => ($doc ? $doc->Id : 0),
            header_background_color => $area->{header_background_color},
            header_image => $area->{header_image}
        });

        foreach my $section (@{$area->{sections}}) {
            my $section_id = $self->create_section({
                area_id => $area_id,
                area_title => $section->{area_title}
            });

            foreach my $path (@{$section->{paths}}) {
                $self->create_path({
                    section_id => $section_id,
                    path => $path
                });
            }
        }
    }
}

1;
