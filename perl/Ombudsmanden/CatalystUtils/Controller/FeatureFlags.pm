package Ombudsmanden::CatalystUtils::Controller::FeatureFlags;
use Moose;
use namespace::autoclean;

use JSON;

BEGIN { extends 'Catalyst::Controller'; }

sub get_featureflags :Path {
    my ($self, $c) = @_;
    my $obvius = $c->obvius;
    my $config = $obvius->config;
    my $host = $config->param('roothost');
    my $dockerhost = $config->param('dockerhost_hostname');
    my %flags = map {
        # Turn the value into a real boolean
        $_ => bless(do{\(my $o = $config->param($_))}, 'JSON::PP::Boolean')
    } grep {
        # Use only config vars beginning with "feature_flag" or "use_"
        /^(FEATURE_FLAG|USE_)/
    } $config->param();

    my $json = JSON->new->ascii->canonical;
    my $buffer = $json->encode({flags=>\%flags, host=>$host, dockerhost=>$dockerhost});
    $c->response->content_type('application/json; charset=utf-8');
    $c->response->content_length(length($buffer));
    $c->response->body($buffer);
}

1;
