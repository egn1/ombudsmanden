package Ombudsmanden::ComplaintSearch;

use strict;
use warnings;

use Date::Calc;
use Obvius::Hostmap;
use Data::Dumper;
use Obvius;
use Obvius::Config;
use Obvius::Log;
use Obvius::CharsetTools qw(mixed2perl);
use Obvius::Translations qw(__);
use WebObvius::Cache::Cache;
use POSIX qw(ceil strftime);
use utf8;
use Obvius::Translations qw(__);
use Net::IDN::Encode qq(domain_to_unicode);
use Net::IDN::Encode qq(domain_to_unicode);


sub new {
    my ($classname, $obvius_or_confname, $mason, $request, $prefix, %args) = @_;

    if (!$obvius_or_confname) {
        die "You must provide an obvius object or a configuration name as the first argument";
    }
    my $self = {%args};
    $self->{mason} = $mason;
    $self->{request} = $request;
    $self->{prefix} = $prefix;
    my $oref = ref($obvius_or_confname) || '';
    if (!$oref) {
        $self->{_confname} = $obvius_or_confname;
    } elsif ($oref eq 'Obvius::Config') {
        $self->{_obvius_config} = $obvius_or_confname;
    } elsif ($oref eq 'Obvius') {
        $self->{_obvius} = $obvius_or_confname;
        $self->{_obvius_config} = $self->{_obvius}->config;
    } else {
        die "Don't know how to handle first argument of type $oref";
    }
    return bless($self, $classname);
}

=head2 obvius

  my $obvius = $dpr->obvius;

returns the associated L<Obvius> object.

=cut


sub obvius {
    my ($self) = @_;
    my $obvius = $self->{_obvius};
    unless($obvius) {
        my $config = $self->obvius_config;
        my $log = new Obvius::Log qw(notice);
        $obvius = Obvius->new(
            $config,
            undef, undef, undef, undef, undef,
            log => $log
        );
        $obvius->{USER} = 'admin';
        $self->{_obvius} = $obvius;
    }

    return $obvius;
}

=head2 obvius_config

  my $obvius_config = $dpr->obvius_config;

returns the associated L<Obvius::Config> object.

=cut


sub obvius_config {
    my ($self) = @_;
    my $config = $self->{_obvius_config};
    unless($config) {
        my $confname = $self->{_confname};
        if(!$confname) {
            die "Can not load obvius-config: No confname specified"
        }
        $config = $self->{_obvius_config} = Obvius::Config->new($confname);
    }

    return $config;
}

=head2 hostmap

  my $hostmap = $dpr->hostmap;

returns the associated L<Obvius::Hostmap> object.

=cut

sub hostmap {
    my ($self) = @_;
    my $hostmap = $self->{_hostmap};
    unless($hostmap) {
        $hostmap = $self->obvius_config->param('hostmap') ||
            Obvius::Hostmap->new_with_obvius($self->obvius);
        $self->{_hostmap} = $hostmap;
    }
    return $hostmap;
}

sub debug {
    my ($self, $message) = @_;

    if(!$self->{debug}) {
        return;
    }

    my ($package, $filename, $line) = caller;

    $message = ref($message) ?
        substr(Data::Dumper::Dumper($message), 7) :
        $message;

    print STDERR sprintf('%s:%d: %s', $filename, $line, $message), "\n";
}

sub condition_string {
    my ($self) = @_;

    if (!defined($self->{_condition_string})) {

        my @cond;
        my @args_array;

        my $request = $self->{request};
        my $date_from = $request->param("from-date") || '';
        my $date_to = $request->param('to-date') || '';
        my $formdoc = $request->param('form-doc') || 'empty';

        if ($date_from) {
            # This filters out sessions that ends before the specified date
            push(@cond, qq|
                COALESCE(complaint_sessions.updated, complaint_sessions.created) >= ?
            |);

            push @args_array, $date_from;
        }

        if ($date_to) {
            # This filters out sessions that starts after the specified date
            push(@cond, qq|
                complaint_sessions.created <= ?
            |);

            push @args_array, $date_to;
        }

        if ($formdoc && $formdoc ne "empty") {
            push(@cond, "complaint_sessions.docid = ?");
            push @args_array, $formdoc;
        }

        my $cond = "";
        if(@cond) {
            $cond = "(" . join(") AND (", @cond) . ")";
        }
        $self->{_condition_string} = {'cond' => $cond, 'array' => \@args_array };
    }

    return $self->{_condition_string};
}

sub tablespec {
    my ($self, $fieldsref) = @_;

    $fieldsref ||= [];

    my $res = q|
        complaint_sessions
        LEFT JOIN submitted_complaints ON (
            complaint_sessions.id = submitted_complaints.session_id
            AND
            submitted_complaints.id = (
                SELECT s2.id
                FROM submitted_complaints s2
                WHERE session_id = complaint_sessions.id
                ORDER BY timestamp desc
                LIMIT 1
            )
        )
    |;
    foreach my $f (@$fieldsref) {
        if ($f->{extra_tables}) {
            $res .= "\n" . $f->{extra_tables};
        }
    }
    return $res;
}

sub formdocs {
    # get fields for form id dropdown
    my ($self, %args) = @_;

    my $obvius = $self->obvius();

    my $formdocs = $obvius->search(
        [],
        "type = 34"
    ) || [];

    my @result;

    foreach my $vdoc (@$formdocs) {
        push(@result, {
            id => $vdoc->DocId,
            name => $obvius->get_version_field($vdoc, "title") .
                    " (" . $vdoc->Path . ")"
        });
    }

    return @result;
}

my %verbose_status = (
    sent            => "Afsendt med success",
    failed          => "Fejl ved afsendelse af E-mail",
    retried         => "Gen-afsendt med success",
    retried_failed  => "Fejl ved forsøg på genudsendelsen af E-mail",
);

sub verbose_status {
    my ($self, $status) = @_;

    return $verbose_status{$status} || $status;
}


sub datafields {
    my ($self, $onlySelected) = @_;

    $onlySelected ||= 0;

    my $obvius = $self->obvius();

    if (!defined($self->{_datafields})) {
        $self->{_datafields} = [
            {
                name => 'id',
                title => 'Id',
                overview => 1,
                default => 1,
                output_sql => 'complaint_sessions.id',
            },
            {
                name => 'timestamp',
                title => 'Seneste opdatering',
                overview => 1,
                default => 1,
                output_sql => "complaint_sessions.updated",
            },
            {
                name                 => 'formular',
                title                => 'Formular',
                overview             => 1,
                default              => 1,
                output_sql           => 'complaint_sessions.docid',
                to_display_value     => sub {
                    my ($docid, $rec) = @_;

                    my ($title, $uri) = ("", "");
                    my $doc = $obvius->get_doc_by_id($docid);
                    if($doc) {
                        $uri = $obvius->get_doc_uri($doc);
                        my $vdoc = $obvius->get_public_version($doc) ||
                                   $obvius->get_latest_version($doc);
                        if($vdoc) {
                            $obvius->get_version_fields($vdoc, ["title", "short_title"]);
                            $title = $vdoc->field('short_title') || $vdoc->field('title');
                        } 
                    }
                    return sprintf(
                        '<a href="/admin%s" onclick="this.target=\'_blank\'">%s (%s)</a>',
                        $uri, $title, $uri
                    );
                }
            },
            {
                name                 => 'name',
                title                => 'Navn',
                overview             => 1,
                default              => 1,
                output_sql           => 'submitted_complaints.name',
            },
            {
                name => 'address',
                title => "Adresse",
                overview => 1,
                default => 1,
                output_sql => 'submitted_complaints.address',
            },
            {
                name => 'anonymized_user',
                title => "Anonymiseret bruger",
                overview => 1,
                default => 1,
                output_sql => 'complaint_sessions.anonymized_user',
            },
            {
                name => 'status',
                title => "Seneste status",
                overview => 1,
                default => 1,
                output_sql => 'submitted_complaints.status',
                to_display_value => sub {
                    my ($val, $rec) = @_;

                    if(!defined($val) || $val eq "") {
                        return "E-mail ikke afsendt";
                    }

                    return $self->verbose_status($val);
                }
            },
            {
                name => 'submission_attempts',
                title => "Antal forsøg på udfyldning før success",
                overview => 1,
                default => 1,
                output_sql => q|(
                    select max(submit_attempt_number)
                    from validation_errors attempts
                    where attempts.session_id = complaint_sessions.id
                )|,
            },
            {
                name => 'validation_errors_total',
                title => "Totalt antal valideringsfejl",
                overview => 1,
                default => 1,
                output_sql => q|(
                    select count(1)
                    from validation_errors errors
                    where
                        errors.session_id = complaint_sessions.id
                )|,
            },
            {
                name     => 'mail_id',
                title    => "Mail ID",
                overview => 1,
                default  => 1,
                output_sql => 'submitted_complaints.mail_id',
            }
        ];
    }

    if ($onlySelected) {
        my @fields;
        my %fieldmap = map {
            $_->{name} => $_
        } @{$self->{_datafields}};

        my @param = $self->{request}->param("fields"); # Fetch field names from request

        if (!scalar(@param)) { # If no fields are defined, get the fields that are set as "default"
            @param = $self->load_field_cookie();
            if (!scalar(@param)) {
                @param = map {
                    $_->{name} if $_->{default}
                } @{$self->{_datafields}};
            }
        }

        for my $fieldvalue (@param) {
            if (defined($fieldvalue)) { # and find the name in our field definition, pushing the definition onto our output array
                my $field = $fieldmap{$fieldvalue};
                if ($field) {
                    push(@fields, $field);
                }
            } else {
                last;
            }
        }
        return @fields;
    }

    return @{$self->{_datafields}};
}

sub search {

    my ($self, %args) = @_;

    my $obvius = $self->obvius();

    my @fields = $self->datafields($args{allfields} ? 0 : 1);

    my ($sort,$sort_desc) = $args{sorting} ?
        @{$args{sorting}} :
        $self->sorting();

    # Defaults
    if(!$args{allfields}) {

        my @allfields = $self->datafields(0);

        my %fieldmap = map {
            $_->{name} => $_
        } @allfields;

        my %seen_fields = map { $_->{name} => 1 } @fields;

        for my $field (@fields) {
            if (defined($field->{depends})) {
                for my $depfield (@{$field->{depends}}) {
                    if ($fieldmap{$depfield} and not $seen_fields{$depfield}) {
                        push(@fields, $fieldmap{$depfield});
                    }
                }
            }
        }
    }
    #### Do search

    # ORDER BY
    my $sort_sql = $sort_desc ? "$sort DESC" : $sort;

    # WHERE
    my $conditions = $self->condition_string();
    my $condition_string = $conditions->{'cond'};
    my $placeholder_values = $conditions->{'array'};

    # FROM
    my $tablespec = $self->tablespec(\@fields);

    my $offset = defined($args{offset}) ?
        $args{offset} :
        $self->offset();
    my $limit = defined($args{limit}) ?
        $args{limit} :
        $self->limit();

    ## SELECT
    my $field_sql = join(
        ",",
        map {
            (
                ($args{alldata} or $_->{name} eq $sort) ?
                    ($_->{sort_sql} || $_->{output_sql}) :
                    $_->{output_sql}
            ) . " " . $_->{name}
        } @fields
    );

    # QUERY STRING
    my $thesql;

    if ($condition_string) {
        $thesql = qq|
            SELECT $field_sql
            FROM $tablespec
            WHERE $condition_string
            ORDER BY $sort_sql
            LIMIT $offset, $limit
        |;
    } else {
        $thesql = qq|
            SELECT $field_sql
            FROM $tablespec
            ORDER BY $sort_sql
            LIMIT $offset, $limit
        |;

    }

    $self->debug($thesql);

    # statement handle
    my $sth = $obvius->dbh->prepare($thesql);

    $sth->execute(@$placeholder_values);

    my @result;

    # while there are result rows for statement handle
    while (my $rec = $sth->fetchrow_hashref) {
        push(@result, $rec);
    }
    return @result;
}

sub validation_meta {
    my ($self) = @_;

    my $session_id = $self->{request}->param('session_id');
    my $obvius = $self->obvius();

    # This query was based on SQL generated by setting "debug => 0"
    # in the initialization of this module in the mason component
    # mason/admin/action/email_registration_table and adding the field
    # "submitted_complaints.id submit_id"
    my $validation_meta_sql = qq|
        SELECT
            submitted_complaints.id submit_id,
            complaint_sessions.updated timestamp,
            complaint_sessions.docid formular,
            submitted_complaints.name name,
            submitted_complaints.address address,
            complaint_sessions.anonymized_user anonymized_user,
            submitted_complaints.status status
            FROM 
                complaint_sessions
                LEFT JOIN submitted_complaints ON (
                    complaint_sessions.id = submitted_complaints.session_id
                )
            WHERE
                complaint_sessions.id = ?
            ORDER BY timestamp
    |;

    my $sth = $obvius->dbh->prepare($validation_meta_sql);

    $sth->execute($session_id);

    my @result;

    while (my $rec = $sth->fetchrow_hashref) {
        push(@result, $rec);
    }
    return @result;
}

sub validation_result {
    my ($self) = @_;

    # submitted complaint id from details link
    my $session_id = $self->{request}->param('session_id');
    my $obvius = $self->obvius();

    my $validation_sql = qq|
        SELECT  validation_errors.timestamp              timestamp,
                validation_errors.submit_attempt_number  attempt_nr,
                validation_errors.failed_field           failed
        FROM    validation_errors
        WHERE   validation_errors.session_id = ?
        ORDER BY
            validation_errors.timestamp ASC,
            validation_errors.failed_field ASC
    |;

    my $sth = $obvius->dbh->prepare($validation_sql);

    $sth->execute($session_id);

    my @results;
    my $last_timestamp = '';
    my $current_result = undef;

    # while there are result rows for statement handle
    while (my $rec = $sth->fetchrow_hashref) {
        $rec->{type} = "Udfyldning med valideringsfejl";
        if($last_timestamp ne $rec->{timestamp}) {
            $current_result = $rec;
            $rec->{failed_fields} = [];
            push(@results, $current_result);
            $last_timestamp = $rec->{timestamp};
        }
        push(@{$current_result->{failed_fields}}, $rec->{failed});
    }

    foreach my $res (@results) {
        $res->{failed_html} = join("<br />\n", @{$res->{failed_fields}});
    }

    $sth = $obvius->dbh->prepare(q|
        SELECT
            timestamp, status
        FROM
            submitted_complaints
        WHERE
            session_id = ?
    |);
    $sth->execute($session_id);
    while(my $rec = $sth->fetchrow_hashref) {
        $rec->{type} = "E-mail forsøgt afsendt";
        push(@results, $rec);
    }

    @results = sort { $a->{timestamp} cmp $b->{timestamp} } @results;

    return @results;
}

sub get_docid_from_complaint_id {
    my ($self) = @_;

    my $submitted_id = $self->{request}->param('id');
    my $obvius = $self->obvius();

    my $docid_sql = qq|
        SELECT
            complaint_sessions.docid
        FROM
            submitted_complaints
            join
            complaint_sessions on (
                submitted_complaints.session_id = complaint_sessions.id
            )
        WHERE
            submitted_complaints.id = ?
    |;

    my $sth = $obvius->dbh->prepare($docid_sql);
    $sth->execute($submitted_id);

    # while there are result rows for statement handle
    my $rec = $sth->fetchrow_hashref;

    return $rec->{docid};

}

sub get_complaint_id_from_submit_id {
    my ($self, $submit_id) = @_;

    my $sth = $self->obvius->dbh->prepare(
        "select session_id from submitted_complaints where id = ?"
    );
    $sth->execute($submit_id);

    my $result = $sth->fetchrow_array;
    $sth->finish;

    return $result;
}

sub limit {
    my ($self) = @_;
    my $limit = 100;
    #left here in case limit selector should be activated in the future
    #my $limit = $self->{request}->param('limit') || 100;
    #if (!($limit =~ s!^(\d+).*!$1!)) {
    #    return 100;
    #}
    return $limit;
}

sub page {
    my ($self) = @_;
    my $page = $self->{request}->param('page') || 1;
    if (!($page =~ s!^(\d+).*!$1!)) {
        return 1;
    }
    return $page;
}

sub offset {
    my ($self) = @_;
    my $limit = $self->limit();
    my $page = $self->page();
    return $page * $limit - $limit;
}

sub total {
    my ($self) = @_;

    my $conditions = $self->condition_string();
    my $placeholder_values = $conditions->{'array'};
    my $tablespec = $self->tablespec();

    if ($conditions->{cond}) {
        my $sql_stmt = "SELECT count(1) FROM $tablespec WHERE $conditions->{cond}";
        return $self->obvius->dbh->selectrow_arrayref(
            $sql_stmt, undef, @$placeholder_values
        )->[0];
    } else {
        return $self->obvius->dbh->selectrow_arrayref(
            "SELECT count(1) FROM $tablespec"
        )->[0];
    }
}

sub sorting {
    my ($self) = @_;
    my @fields = $self->datafields();
    my $allowed_sort = ();
    for my $field (@fields) {
        $allowed_sort->{$field->{name}} = 1;
    }

    my $sort = $self->{request}->param('sort') || 'timestamp'; #'name';
    my $sort_desc = ($sort =~ s!_desc$!!) ? 1 : 0;
    if (!$allowed_sort->{$sort}) {
        $sort = 'timestamp';
        $sort_desc = 1;
    }
    return ($sort, $sort_desc);
}


sub load_field_cookie {
    my ($self) = @_;
    my $cookie = $self->{mason}->comp("/shared/get_cookie", cookie=>"subsite_fields");
    if ($cookie) {
        return split(',', "$cookie");
    }
    return ();
}


sub mixed2latin1 {
    return Obvius::CharsetTools::mixed2charset(shift, 'iso-8859-1');
}


1;
