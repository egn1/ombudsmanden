create table if not exists subscription_areas (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(2048) NOT NULL DEFAULT '',
  `config_name` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(2048) NOT NULL DEFAULT '',
  `root_docid` int(8) unsigned NOT NULL DEFAULT '0',
  `header_background_color` varchar(255) NOT NULL DEFAULT '',
  `header_image` varchar(2048) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

create table if not exists subscription_area_sections (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `area_id`int(8) unsigned NOT NULL,
  `area_title` varchar(2048) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  CONSTRAINT `sub_area_sec_area_ref`
        FOREIGN KEY (`area_id`)
        REFERENCES `subscription_areas` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

create table if not exists subscription_area_paths (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `section_id`int(8) unsigned NOT NULL,
  `path` varchar(2048) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  CONSTRAINT `sub_area_url_sec_ref`
        FOREIGN KEY (`section_id`)
        REFERENCES `subscription_area_sections` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

delimiter $$

drop procedure if exists __tmp_database_change $$
create procedure __tmp_database_change()
begin

IF NOT EXISTS(
    SELECT *
    FROM information_schema.COLUMNS
    WHERE
        TABLE_SCHEMA=DATABASE() AND
        COLUMN_NAME='banner_text' AND
        TABLE_NAME='subscription_areas'
) THEN
    ALTER TABLE `subscription_areas`
    ADD COLUMN `banner_text` text NULL default ''
    AFTER `root_docid`;
END IF;

END $$

call __tmp_database_change() $$
drop procedure if exists __tmp_database_change $$

delimiter ;
