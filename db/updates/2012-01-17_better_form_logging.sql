delimiter $$

drop procedure if exists __tmp_better_form_logging $$
create procedure __tmp_better_form_logging()
begin
    declare a integer unsigned;

    SET a = 0;

    SELECT COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE
        table_schema = 'ombud'
        AND
        table_name = 'form_submit_log'
        AND
        column_name = 'id'
    INTO a;

    if (a < 1) then
        ALTER TABLE form_submit_log
        DROP PRIMARY KEY,
        ADD COLUMN id int(8) unsigned NOT NULL auto_increment FIRST,
        ADD PRIMARY KEY (id);
    end if;

    SET a = 0;

    SELECT COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE
        table_schema = 'ombud'
        AND
        table_name = 'form_submit_log'
        AND
        column_name = 'submitter_ip'
    INTO a;

    if (a < 1) then
        ALTER TABLE form_submit_log
        ADD COLUMN (submitter_ip varchar(255) default '');
    end if;

    SET a = 0;

    SELECT COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE
        table_schema = 'ombud'
        AND
        table_name = 'form_submit_log'
        AND
        column_name = 'encrypted_email'
    INTO a;

    if (a < 1) then
        ALTER TABLE form_submit_log
        ADD COLUMN (encrypted_email text default '');
    end if;
end $$
call __tmp_better_form_logging();
drop procedure if exists __tmp_better_form_logging $$

delimiter ;
