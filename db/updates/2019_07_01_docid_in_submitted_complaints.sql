-- create table if not exists submitted_complaints;
create table if not exists submitted_complaints (
     `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
     `anonymized_user` varchar(2048) NOT NULL DEFAULT '',
     `submission_uuid` varchar(255) NOT NULL DEFAULT '',
     `timestamp` datetime,
     `failed_field` varchar(255) NOT NULL DEFAULT '',
     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

set names utf8;
delimiter $$

-- Add docid column
drop procedure if exists __perform_db_change $$
create procedure __perform_db_change()
begin
    declare a integer unsigned;

    SET a = 0;

    SELECT COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE
        table_schema = DATABASE()
        AND
        table_name = 'submitted_complaints'
        AND
        column_name = 'docid'
    INTO a;

    if(a < 1) then
        ALTER TABLE `submitted_complaints`
        ADD COLUMN `docid` int(8) unsigned
        AFTER `id`;
    end if;
end $$

call __perform_db_change();
drop procedure if exists __perform_db_change $$

delimiter ;