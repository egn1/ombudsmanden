-- Old data:
-- <%init>
-- return ({ value => "Beskæftigelsesministeriet" },
--        { value => "Finansministeriet" },
--        { value => "Forsvarsministeriet" },
--        { value => "Indenrigs- og Sundhedsministeriet" },
--        { value => "Justitsministeriet" },
--        { value => "Kirkeministeriet" },
--        { value => "Klima- og Energiministeriet" },
--        { value => "Kulturministeriet" },
--        { value => "Miljøministeriet" },
--        { value => "Ministeriet for Familie og Forbrugeranliggender" },
--        { value => "Ministeriet for Flygtninge, Indvandrere og Integration" },
--        { value => "Ministeriet for Fødevarer, Landbrug og Fiskeri" },
--        { value => "Ministeriet for Sundhed og Forebyggelse" },
--        { value => "Ministeriet for Videnskab, Teknologi og Udvikling" },
--        { value => "Skatteministeriet" },
--        { value => "Socialministeriet" },
--        { value => "Statsministeriet" },
--        { value => "Trafikministeriet" },
--        { value => "Udenrigsministeriet" },
--        { value => "Undervisningsministeriet" },
--        { value => "Velfærdsministeriet" },
--        { value => "Økonomi- og Erhvervsministeriet" },
--        { value => "Kommunerne" },);
-- </%init>

set names utf8;

-- Indenrigs- og Sundhedsministeriet => Økonomi- og Indenrigsministeriet
UPDATE vfields
SET text_value = "Økonomi- og Indenrigsministeriet"
WHERE name in ('myndighed', 'display_myndighed')
AND text_value = "Indenrigs- og Sundhedsministeriet";

-- Kirkeministeriet => Ministeriet for Ligestilling og Kirke
UPDATE vfields
SET text_value = "Ministeriet for Ligestilling og Kirke"
WHERE name in ('myndighed', 'display_myndighed')
AND text_value = "Kirkeministeriet";

-- Klima- og Energiministeriet => Klima-, Energi- og Bygningsministeriet
UPDATE vfields
SET text_value = "Klima-, Energi- og Bygningsministeriet"
WHERE name in ('myndighed', 'display_myndighed')
AND text_value = "Klima- og Energiministeriet";

-- Ministeriet for Videnskab, Teknologi og Udvikling => Ministeriet for Forskning, Innovation og Videregående Uddannelse
UPDATE vfields
SET text_value = "Ministeriet for Forskning, Innovation og Videregående Uddannelse"
WHERE name in ('myndighed', 'display_myndighed')
AND text_value = "Ministeriet for Videnskab, Teknologi og Udvikling";

-- Socialministeriet - erstattes med Social- og Integrationsministeriet
UPDATE vfields
SET text_value = "Social- og Integrationsministeriet"
WHERE name in ('myndighed', 'display_myndighed')
AND text_value = "Socialministeriet";

-- Trafikministeriet - erstattes med Transportministeriet
UPDATE vfields
SET text_value = "Transportministeriet"
WHERE name in ('myndighed', 'display_myndighed')
AND text_value = "Trafikministeriet";

-- Undervisningsministeriet - erstattes med Ministeriet for Børn og Undervisning
UPDATE vfields
SET text_value = "Ministeriet for Børn og Undervisning"
WHERE name in ('myndighed', 'display_myndighed')
AND text_value = "Undervisningsministeriet";

-- Velfærdsministeriet - erstattes med Ministeriet for Sundhed og Forebyggelse
UPDATE vfields
SET text_value = "Ministeriet for Sundhed og Forebyggelse"
WHERE name in ('myndighed', 'display_myndighed')
AND text_value = "Velfærdsministeriet";

-- Økonomi- og Erhvervsministeriet - erstattes med Erhvervs- og Vækstministeriet
UPDATE vfields
SET text_value = "Erhvervs- og Vækstministeriet"
WHERE name in ('myndighed', 'display_myndighed')
AND text_value = "Økonomi- og Erhvervsministeriet";
