-- drop table submitted_complaints;
create table if not exists submitted_complaints(
   `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
   `anonymized_user` varchar(2048) NOT NULL DEFAULT '',
   `submission_uuid` varchar(255) NOT NULL DEFAULT '',
   `timestamp` datetime,
   `name` varchar(255) NOT NULL DEFAULT '',
   `address` text,
   `encrypted_mail` blob,
   `status` enum('sent', 'failed', 'retried', 'retried_failed'),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;