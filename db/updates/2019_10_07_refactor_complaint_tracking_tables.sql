CREATE TABLE IF NOT EXISTS `complaint_sessions` (
   `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
   `submission_uuid` varchar(255) NOT NULL DEFAULT '',
   `anonymized_user` varchar(2048) NOT NULL DEFAULT '',
   `docid` int(8) unsigned DEFAULT NULL,
   `latest_status` varchar(255) DEFAULT NULL,
   `created` datetime NOT NULL,
   `updated` datetime NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

set names utf8;
delimiter $$

drop procedure if exists __perform_db_change $$
create procedure __perform_db_change()
begin
    declare a integer unsigned;

    SET a = 0;

    SELECT COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE
        table_schema = DATABASE()
        AND
        table_name = 'validation_errors'
        AND
        column_name = 'submission_uuid'
    INTO a;

    if(a > 0) then
        -- Delete all existing entries, so we get no conflicts when adding
        -- new column
        DELETE FROM `validation_errors`;

        ALTER TABLE `validation_errors`
            DROP COLUMN `submission_uuid`,
            DROP COLUMN `docid`,
            DROP COLUMN `anonymized_user`,
            ADD COLUMN `session_id` int(8) unsigned AFTER `id`,
            ADD COLUMN `submit_attempt_number` int(8) unsigned AFTER `session_id`,
            ADD FOREIGN KEY (`session_id`) REFERENCES `complaint_sessions`(`id`)
        ;
    end if;

    SET a = 0;

    SELECT COUNT(*)
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE
        table_schema = DATABASE()
        AND
        table_name = 'submitted_complaints'
        AND
        column_name = 'submission_uuid'
    INTO a;

    if(a > 0) then
        -- Delete all existing entries, so we get no conflicts when adding
        -- new column
        DELETE FROM `submitted_complaints`;

        ALTER TABLE `submitted_complaints`
            DROP COLUMN `submission_uuid`,
            DROP COLUMN `docid`,
            DROP COLUMN `anonymized_user`,
            ADD COLUMN `session_id` int(8) unsigned AFTER `id`,
            ADD FOREIGN KEY (`session_id`) REFERENCES `complaint_sessions`(`id`)
        ;
    end if;

end $$

call __perform_db_change();
drop procedure if exists __perform_db_change $$

delimiter ;
