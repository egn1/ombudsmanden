CREATE TABLE form_submit_log (
  docid int(8) unsigned NOT NULL,
  submit_time datetime NOT NULL,
  PRIMARY KEY (docid, submit_time)
) type=InnoDB;
