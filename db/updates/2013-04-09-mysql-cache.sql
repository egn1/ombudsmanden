DROP TABLE IF EXISTS `apachecache_home`;
CREATE TABLE `apachecache_home` (
    `uri` blob NOT NULL DEFAULT '',
    `querystring` varchar(255) NOT NULL DEFAULT '',
    `cache_uri` varchar(255) NOT NULL DEFAULT '',
    PRIMARY KEY (`uri`(255), `querystring`(255)),
    KEY `apachecache_home_querystring_idx` (`querystring`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
