create table if not exists `newsletter_send_time` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `start` datetime NOT NULL,
  `end` datetime NULL,
  PRIMARY KEY (`id`),
  KEY `start_idx` (`start`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
