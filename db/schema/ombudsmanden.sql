/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_leftmenu_cache` (
  `id` varchar(255) NOT NULL,
  `cache_data` mediumblob,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annotations` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` smallint(5) unsigned NOT NULL DEFAULT '0',
  `text` text,
  PRIMARY KEY (`id`),
  KEY `docid` (`docid`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apache_edit_sessions` (
  `id` char(32) NOT NULL,
  `a_session` mediumblob,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apache_user_sessions` (
  `id` char(32) NOT NULL,
  `a_session` mediumblob,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apachecache_home` (
  `uri` mediumblob NOT NULL,
  `querystring` varchar(255) NOT NULL DEFAULT '',
  `cache_uri` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uri`(255),`querystring`),
  KEY `apachecache_home_querystring_idx` (`querystring`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applied_migrations` (
  `migration` varchar(255) NOT NULL,
  `applied_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`migration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` char(9) NOT NULL DEFAULT '',
  `name` char(127) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `name` varchar(127) NOT NULL DEFAULT '',
  `email` varchar(63) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  PRIMARY KEY (`docid`,`date`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `complaint_sessions` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `submission_uuid` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `anonymized_user` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `docid` int(8) unsigned DEFAULT NULL,
  `latest_status` varchar(255) COLLATE utf8_danish_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `name` varchar(127) NOT NULL DEFAULT '',
  `value` varchar(127) NOT NULL DEFAULT '',
  `descriptions` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docid_path` (
  `path` varchar(1024) DEFAULT NULL,
  `docid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`docid`),
  KEY `path` (`path`(767))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docparams_backup` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `deletion_time` datetime NOT NULL,
  `docid` int(8) unsigned NOT NULL,
  `name` varchar(127) NOT NULL,
  `value` longtext,
  `type` int(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docparms` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(127) NOT NULL DEFAULT '',
  `value` longtext,
  `type` int(8) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctypes` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL DEFAULT '',
  `parent` int(8) unsigned NOT NULL,
  `basis` int(1) unsigned NOT NULL DEFAULT '0',
  `searchable` int(1) NOT NULL DEFAULT '1',
  `sortorder_field_is` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `type` int(8) unsigned NOT NULL DEFAULT '0',
  `owner` smallint(5) unsigned NOT NULL DEFAULT '0',
  `grp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `accessrules` text CHARACTER SET latin1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parent` (`parent`,`name`),
  KEY `parent_2` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_document_insert` AFTER INSERT ON `documents` FOR EACH ROW call insert_docid_path(new.id) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_document_update` AFTER UPDATE ON `documents` FOR EACH ROW begin
    if (new.parent != old.parent) or (new.name != old.name) then
       call update_move(new.id);
    end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_document_delete` AFTER DELETE ON `documents` FOR EACH ROW begin
        declare p text default NULL;
        select path from docid_path where docid=old.id into p;  
        insert into documents_backup (id, parent, name, type, owner,grp, accessrules, 
                                      path,date_deleted, delete_user) values 
                                   (old.id, old.parent, old.name, old.type, old.owner, 
                                    old.grp, old.accessrules, p, now(), ifnull(@user, 1));
	delete d from docid_path d where d.docid = old.id;
	call clean_internal_proxies(old.id);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_backup` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(8) unsigned NOT NULL DEFAULT '0',
  `name` char(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `type` int(8) unsigned NOT NULL,
  `owner` smallint(5) unsigned NOT NULL DEFAULT '0',
  `grp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `accessrules` text CHARACTER SET latin1,
  `path` varchar(2048) CHARACTER SET latin1 NOT NULL,
  `date_deleted` datetime NOT NULL,
  `delete_user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `path` (`path`(767)),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `editpages` (
  `doctypeid` int(8) unsigned NOT NULL,
  `page` char(5) NOT NULL,
  `title` varchar(127) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `fieldlist` text NOT NULL,
  PRIMARY KEY (`doctypeid`,`page`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldspecs` (
  `doctypeid` int(8) unsigned NOT NULL,
  `name` varchar(127) NOT NULL DEFAULT '',
  `type` int(8) unsigned NOT NULL,
  `repeatable` tinyint(1) unsigned NOT NULL,
  `optional` tinyint(1) unsigned NOT NULL,
  `searchable` tinyint(1) unsigned NOT NULL,
  `sortable` tinyint(1) unsigned NOT NULL,
  `publish` tinyint(1) unsigned NOT NULL,
  `threshold` tinyint(1) unsigned NOT NULL DEFAULT '128',
  `default_value` text,
  `extra` text,
  PRIMARY KEY (`doctypeid`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldtypes` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL DEFAULT '',
  `edit` varchar(127) NOT NULL DEFAULT 'line',
  `edit_args` text NOT NULL,
  `validate` varchar(127) NOT NULL DEFAULT 'none',
  `validate_args` text NOT NULL,
  `search` varchar(127) NOT NULL DEFAULT 'none',
  `search_args` text NOT NULL,
  `bin` tinyint(1) NOT NULL DEFAULT '0',
  `value_field` enum('text','int','double','date') NOT NULL DEFAULT 'text',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_submit_log` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(8) unsigned NOT NULL,
  `submit_time` datetime NOT NULL,
  `submitter_ip` varchar(255) DEFAULT '',
  `encrypted_email` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(10) unsigned NOT NULL DEFAULT '0',
  `entry` longtext,
  PRIMARY KEY (`id`),
  KEY `docid` (`docid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formdata_backup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(10) unsigned NOT NULL DEFAULT '0',
  `entry` longtext,
  PRIMARY KEY (`id`),
  KEY `docid` (`docid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formdata_entry` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(10) unsigned NOT NULL,
  `entry_nr` int(10) unsigned NOT NULL,
  `time` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  UNIQUE KEY `docid` (`docid`,`entry_nr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formdata_entry_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_danish_ci NOT NULL,
  `value` mediumtext COLLATE utf8_danish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`entry_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freetextindex` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(255) NOT NULL DEFAULT '',
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `weight` double unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `words` (`word`,`docid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freetextindex_versions` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`docid`,`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(31) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_groups_delete` AFTER DELETE ON `groups` FOR EACH ROW begin
	update documents set grp=1 where grp=old.id;
	delete g from grp_user g where g.grp=old.id; 
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grp_user` (
  `grp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `user` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`grp`,`user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internal_proxy_documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(10) unsigned DEFAULT NULL,
  `version` datetime DEFAULT NULL,
  `dependent_on` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `docid_2` (`docid`,`version`),
  KEY `docid` (`docid`,`version`),
  KEY `dependent_on` (`dependent_on`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internal_proxy_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `relation_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_2` (`name`,`relation_id`),
  KEY `name` (`name`),
  KEY `relation_id` (`relation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kalendertilmeldinger` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `navn` varchar(255) NOT NULL DEFAULT '',
  `stillingsbetegnelse` varchar(255) NOT NULL DEFAULT '',
  `arbejdsplads` varchar(255) NOT NULL DEFAULT '',
  `adresse` varchar(255) NOT NULL DEFAULT '',
  `postnr` int(5) unsigned DEFAULT NULL,
  `adresse_by` varchar(255) NOT NULL DEFAULT '',
  `land` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `telefonnummer` varchar(255) NOT NULL DEFAULT '',
  `mobilnummer` varchar(255) NOT NULL DEFAULT '',
  `faktura_navn` varchar(255) NOT NULL DEFAULT '',
  `faktura_adresse` varchar(255) NOT NULL DEFAULT '',
  `faktura_postnr` int(5) unsigned DEFAULT NULL,
  `faktura_adresse_by` varchar(255) NOT NULL DEFAULT '',
  `faktura_land` varchar(255) NOT NULL DEFAULT '',
  `paper_praesentation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pp_title` varchar(255) NOT NULL DEFAULT '',
  `oensker_hotel` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hotel_bemaerkninger` text,
  `bemaerkninger` text,
  `tilmeldingstidspunkt` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(63) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyword` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_secrets` (
  `login` varchar(31) NOT NULL,
  `secret` varchar(32) NOT NULL,
  `time` int(10) unsigned NOT NULL,
  KEY `login` (`login`,`secret`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_sessions` (
  `login` varchar(31) NOT NULL,
  `session_id` char(32) NOT NULL,
  `last_access` int(12) NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `newsboxid` int(8) unsigned NOT NULL DEFAULT '0',
  `seq` int(8) unsigned NOT NULL DEFAULT '0',
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `start` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `end` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`newsboxid`,`seq`),
  KEY `start` (`start`),
  KEY `end` (`end`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsboxes` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `type` enum('chronological','reverse_chronological','manual_placement') NOT NULL DEFAULT 'chronological',
  PRIMARY KEY (`docid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_send_time` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `start_idx` (`start`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_sent_docs` (
  `newsletter_id` int(10) unsigned NOT NULL DEFAULT '0',
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`newsletter_id`,`docid`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL DEFAULT '',
  `subject` varchar(127) NOT NULL DEFAULT '',
  `sent` datetime DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `user` smallint(5) unsigned NOT NULL DEFAULT '0',
  `command` varchar(127) NOT NULL DEFAULT '',
  `args` text,
  `status` varchar(63) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shortcuts` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(8) unsigned NOT NULL,
  `docid` int(8) unsigned NOT NULL,
  `name` varchar(512) NOT NULL,
  `order_number` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submitted_complaints` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(8) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `address` text COLLATE utf8_danish_ci,
  `encrypted_mail` longblob,
  `status` enum('sent','failed','retried','retried_failed') COLLATE utf8_danish_ci DEFAULT NULL,
  `mail_id` varchar(255) COLLATE utf8_danish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `submitted_complaints_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `complaint_sessions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber_categories` (
  `subscriber` int(10) unsigned NOT NULL DEFAULT '0',
  `category` varchar(127) NOT NULL DEFAULT '',
  PRIMARY KEY (`subscriber`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL DEFAULT '',
  `company` varchar(127) NOT NULL DEFAULT '',
  `passwd` varchar(63) NOT NULL DEFAULT '',
  `email` varchar(63) NOT NULL DEFAULT '',
  `suspended` tinyint(3) NOT NULL DEFAULT '0',
  `cookie` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_areas` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `config_name` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `subject` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `root_docid` int(8) unsigned NOT NULL DEFAULT '0',
  `banner_text` text COLLATE utf8_danish_ci,
  `header_background_color` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `header_image` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_area_sections` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `area_id` int(8) unsigned NOT NULL,
  `area_title` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `sub_area_sec_area_ref` (`area_id`),
  CONSTRAINT `sub_area_sec_area_ref` FOREIGN KEY (`area_id`) REFERENCES `subscription_areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_area_paths` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(8) unsigned NOT NULL,
  `path` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `sub_area_url_sec_ref` (`section_id`),
  CONSTRAINT `sub_area_url_sec_ref` FOREIGN KEY (`section_id`) REFERENCES `subscription_area_sections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `subscriber` int(10) unsigned NOT NULL DEFAULT '0',
  `last_update` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`docid`,`subscriber`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `synonyms` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `synonyms` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblprojekter` (
  `Id` int(11) DEFAULT '0',
  `titel` text,
  `beskrivelse` text,
  `noegleord` varchar(255) DEFAULT NULL,
  `projektstart` varchar(50) DEFAULT NULL,
  `projektslut` varchar(50) DEFAULT NULL,
  `medarbejdende_forskere` text,
  `brugergrupper` varchar(250) DEFAULT NULL,
  `andre_brugergruper` varchar(255) DEFAULT NULL,
  `formidling` text,
  `email` varchar(255) DEFAULT NULL,
  `titel_projektansvar` varchar(50) DEFAULT NULL,
  `projektansvar` varchar(255) DEFAULT NULL,
  `institution` varchar(255) DEFAULT NULL,
  `projektnr` int(11) DEFAULT '0',
  KEY `Id` (`Id`),
  KEY `projektnr` (`projektnr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_surveillance_docs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `docid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`docid`),
  KEY `docid` (`docid`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_surveillance_sites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `docid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`docid`),
  KEY `user_id` (`user_id`),
  KEY `docid` (`docid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(31) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `passwd` varchar(63) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name` varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `email` varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `notes` text CHARACTER SET latin1 NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `can_manage_users` tinyint(1) NOT NULL DEFAULT '0',
  `can_manage_groups` tinyint(1) NOT NULL DEFAULT '0',
  `surveillance` text CHARACTER SET latin1,
  `is_admin` int(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_user_insert` AFTER INSERT ON `users` FOR EACH ROW begin 
      call delete_user_surveillance(new.id);
      if new.surveillance is not null and new.surveillance != '' then
          call create_user_surveillance(new.id, new.surveillance);
      end if;	  
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_user_update` AFTER UPDATE ON `users` FOR EACH ROW begin 
      call delete_user_surveillance(new.id);
      if new.surveillance is not null and new.surveillance != '' then
            call create_user_surveillance(new.id, new.surveillance);
      end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_user_delete` AFTER DELETE ON `users` FOR EACH ROW begin
	call delete_user_surveillance(old.id);
	update documents set owner = 1 where owner = old.id;
	update versions set user = 1 where user = old.id;
	
	delete from grp_user where user=old.id;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `validation_errors` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(8) unsigned DEFAULT NULL,
  `submit_attempt_number` int(8) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `failed_field` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `validation_errors_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `complaint_sessions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `type` int(8) unsigned NOT NULL DEFAULT '0',
  `public` tinyint(8) NOT NULL DEFAULT '0',
  `valid` tinyint(8) NOT NULL DEFAULT '0',
  `lang` char(2) NOT NULL DEFAULT 'da',
  `user` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`docid`,`version`),
  KEY `type` (`type`),
  KEY `versions_public_idx` (`public`,`type`,`docid`,`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_version_delete` AFTER DELETE ON `versions` FOR EACH ROW begin
      insert into versions_backup (docid, version, type, public, 
                                   valid, lang, user) values 
                                  (old.docid, old.version, old.type, old.public, 
                                   old.valid, old.lang, old.user);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions_backup` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `type` int(8) unsigned NOT NULL DEFAULT '0',
  `public` tinyint(8) NOT NULL DEFAULT '0',
  `valid` tinyint(8) NOT NULL DEFAULT '0',
  `lang` char(2) NOT NULL DEFAULT 'da',
  `user` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`docid`,`version`),
  KEY `type` (`type`),
  KEY `public` (`public`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vfields` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `name` varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `text_value` longtext COLLATE utf8_danish_ci,
  `int_value` int(8) DEFAULT NULL,
  `double_value` double DEFAULT NULL,
  `date_value` datetime DEFAULT NULL,
  KEY `vfields_docid_version_name_int_value_idx` (`docid`,`version`,`name`,`int_value`),
  KEY `vfields_docid_version_name_double_value_idx` (`docid`,`version`,`name`,`double_value`),
  KEY `vfields_docid_version_name_date_value_idx` (`docid`,`version`,`name`,`date_value`),
  KEY `vfields_docid_version_name_text_value_idx` (`docid`,`version`,`name`,`text_value`(16)),
  KEY `vfields_text_idx` (`name`,`text_value`(255)),
  KEY `vfields_date_idx` (`name`,`date_value`),
  KEY `vfields_int_idx` (`name`,`int_value`),
  KEY `vfields_double_idx` (`name`,`double_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_vfield_delete` AFTER DELETE ON `vfields` FOR EACH ROW begin 
      insert into vfields_backup (docid, version, name, text_value, int_value, 
                                  date_value, double_value) values
                                 (old.docid, old.version, old.name, old.text_value, old.int_value, 
                                  old.date_value, old.double_value);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vfields_backup` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `name` varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `text_value` longtext CHARACTER SET latin1,
  `int_value` int(8) DEFAULT NULL,
  `double_value` double DEFAULT NULL,
  `date_value` datetime DEFAULT NULL,
  KEY `vfields_docid_version_name_int_value_idx` (`docid`,`version`,`name`,`int_value`),
  KEY `vfields_docid_version_name_double_value_idx` (`docid`,`version`,`name`,`double_value`),
  KEY `vfields_docid_version_name_date_value_idx` (`docid`,`version`,`name`,`date_value`),
  KEY `vfields_docid_version_name_text_value_idx` (`docid`,`version`,`name`,`text_value`(16))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voters` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `cookie` char(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`docid`,`cookie`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `votes` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `answer` char(32) NOT NULL DEFAULT '',
  `total` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`docid`,`answer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE FUNCTION `intervals_overlap`(siv1 datetime, eiv1 datetime, siv2 datetime, eiv2 datetime) RETURNS int(11)
    DETERMINISTIC
begin
	if eiv1 is null or eiv1 = '0000-00-00' then
	   set eiv1 = siv1;
	end if;
	if eiv2 is null or eiv2 = '0000-00-00' then
	   set eiv2 = siv2;
	end if;
	return not ((eiv2 < siv1 and eiv2 < eiv1) or (eiv1 < siv2 and eiv1 < eiv2));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `add_vfield`(docid integer unsigned, version datetime, name varchar(1024), text_value varchar(16384), int_value integer, double_value double, date_value datetime)
begin
	insert into vfields 
	       (docid, version, name, text_value, int_value, double_value, date_value) values
	       (docid, version, ucase(name), text_value, int_value, double_value, date_value);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `backup_recursive_subdocs`()
begin 
	declare id integer unsigned default 0;
	create temporary table if not exists 
	       recursive_subdocs_table (id integer unsigned primary key auto_increment) engine=heap;
	create temporary table if not exists 
	       recursive_subdocs_backup_list (id integer unsigned primary key auto_increment) engine=heap;
	create temporary table if not exists
	       recursive_subdocs_backup (backup_id integer unsigned, recursive_id integer unsigned) engine=heap;
	
	insert into recursive_subdocs_backup_list values ();
	set id = last_insert_id();
	
	insert into recursive_subdocs_backup (backup_id, recursive_id) 
	       select id,recursive_subdocs_table.id from recursive_subdocs_table;
	
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `check_internal_proxy_status`(out good integer unsigned)
begin
	declare times integer unsigned default 10;
	create temporary table if not exists internal_proxy_status_table (docid integer unsigned, version datetime, primary key (docid, version) ) engine = heap;
	create temporary table if not exists internal_proxy_status_table2 (docid integer unsigned, version datetime, primary key (docid, version) ) engine = heap;
	delete ip from internal_proxy_status_table ip;
	delete ip2 from internal_proxy_status_table ip2;
	insert ignore into internal_proxy_status_table (docid) 
	       select docid from internal_proxy_documents i2;
	
	while times > 0 and not good do
	      set times = times - 1;
	      delete ip2 from internal_proxy_status_table2 ip2;
	      insert ignore into internal_proxy_status_table2 
	      	     select i.docid, i.version from internal_proxy_documents i 
		     join internal_proxy_status_table ip on
		     (i.dependent_on = ip.docid);
	      delete ip from internal_proxy_status_table ip;
	      insert ignore into internal_proxy_status_table 
	      	     select * from internal_proxy_status_table2 ip2;
              call cleanup_internal_proxy_status_table();
	      select not count(*) into good from internal_proxy_status_table ip;
	end while;
	delete ip from internal_proxy_status_table ip;
	delete ip2 from internal_proxy_status_table ip2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `cleanup_internal_proxy_status_table`()
begin
	declare done integer unsigned default 0;
	declare v2 datetime default null;
	declare d integer unsigned default null;
	declare v datetime default null;
	declare c cursor for (select d.docid, d.version from internal_proxy_status_table ip);
	declare continue handler for not found set done=1;
	
	create temporary table if not exists to_be_deleted (docid integer unsigned, version datetime, primary key( docid, version));
	delete t from to_be_deleted t;
	open c;
	fetch c into d,v;
	while not done do
	      call public_or_latest_version(d, v2);
	      if v2 != v then
	      	 insert into to_be_deleted values (d,v);
	      end if;     
	      fetch c into d,v;
       end while;
       
       close c;
       delete ip from internal_proxy_status_table ip natural join to_be_deleted;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `clean_internal_proxies`(docid integer unsigned)
begin
	declare a integer unsigned default null;
	declare done integer unsigned default 0;
	declare c cursor for (select i.id from internal_proxy_documents i where i.docid = docid);
	declare c2 cursor for (select i.id from internal_proxy_documents i where i.dependent_on = docid);
        declare continue handler for not found set done=1;
	
	open c;
	fetch c into a;
	
	while not done do
	   update internal_proxy_documents i join internal_proxy_documents i2 on 
	   	      (i.docid = i2.dependent_on) set i2.dependent_on = i.dependent_on 
		      where i.id = a;
	   delete i from internal_proxy_fields i  where i.relation_id = a;
	   delete i from internal_proxy_documents i where i.id = a;
	   fetch c into a;
	end while;
	close c;
	set done = 0;
	open c2;
	fetch c2 into a;
	
	while not done do
	   delete from internal_proxy_fields  where i.relation_id = a;
	   delete i from internal_proxy_documents i where i.id = a;
	   fetch c2 into a;
	end while;
	close c2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `copy_batch`(out finally integer)
begin 
      declare done integer default 0;
      declare cur_id integer unsigned;
      declare cur_parent integer unsigned;
      declare new_docid integer unsigned;
      declare curs cursor for 
              (select recursive_subdocs_table.id, t.new_docid from 
                      documents d join recursive_subdocs_table on (recursive_subdocs_table.id = d.id)
                      join tree_copier_helper t on (d.parent = t.old_docid) 
                      where not exists (select * from batch_copier_helper b where b.id = recursive_subdocs_table.id));
     declare continue handler for not found set done=1;
     create temporary table if not exists batch_copier_helper (id integer unsigned primary key) engine=heap;
     delete b from batch_copier_helper b;
     insert into batch_copier_helper (id) select old_docid from tree_copier_helper t;
     select not count(*) into finally from 
             recursive_subdocs_table left join tree_copier_helper t on (t.old_docid = recursive_subdocs_table.id) 
             where t.old_docid is null;
     if not finally then
         open curs;
         fetch curs into cur_id, cur_parent;
         while not done do
               call copy_id(cur_id, cur_parent, NULL, new_docid);
               insert into tree_copier_helper (old_docid, new_docid) values (cur_id, new_docid);
               fetch curs into cur_id, cur_parent;
         end while;
         close curs;
       end if;  
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `copy_docparams`(fromid integer unsigned, toid integer unsigned)
begin
	replace into docparms (docid, name, value, type) select toid, name, value, type from docparms where docid=fromid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `copy_id`(did integer unsigned, 
                         new_parent integer unsigned, 
                         new_name varchar(1024), 
                         out new_docid integer unsigned)
begin
        insert into documents (parent, name, type, owner, grp, accessrules) 
	        select coalesce(new_parent, d.parent), coalesce(new_name, d.name), d.type, d.owner, d.grp, d.accessrules 
        	from documents d where d.id=did;
	set new_docid = last_insert_id();
		     
        insert into versions (docid, version,type,public,valid,lang, user)
        select new_docid, version, type, public,valid,lang, user from versions where docid=did;
        
        insert into vfields (docid, version,name,text_value,int_value, double_value,date_value)
        select new_docid,version,name,text_value,int_value,double_value,date_value from vfields where docid=did; 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `copy_tree`(docid integer unsigned, new_parent integer unsigned, new_name varchar(1024))
begin 
      declare done integer default 0;
      declare new_did integer unsigned;
      
      create temporary table if not exists tree_copier_helper (old_docid integer unsigned, new_docid integer unsigned, index (old_docid), index (new_docid)) engine=heap;
      delete t from tree_copier_helper t;
      call backup_recursive_subdocs();
      call recursive_subdocs(docid);
      
      call copy_id(docid, new_parent, new_name, new_did);
      insert into tree_copier_helper (old_docid, new_docid) values (docid, new_did);
      
      while not done do call copy_batch(done); end while;
      call restore_recursive_subdocs();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `create_internal_proxy_docid_update_table`(docid integer unsigned, v datetime)
begin
	declare old_len integer unsigned default 0;
	declare new_len integer unsigned default 1;
	create temporary table if not exists internal_proxy_docid_update_table(id integer unsigned auto_increment primary key, 
	       		       					               docid integer unsigned, 
									       version datetime,
								               unique(docid, version)) engine=heap;
	create temporary table if not exists internal_proxy_docid_update_table2(id integer unsigned auto_increment primary key, 
	       		       					                docid integer unsigned, 
										version datetime,
								                unique (docid,version)) engine=heap;
	delete d from internal_proxy_docid_update_table d;
	delete d2 from internal_proxy_docid_update_table2 d2;
	
	insert into internal_proxy_docid_update_table (docid, version) values (docid, v);
	
	while old_len != new_len do
	      set old_len = new_len;
	      insert ignore into internal_proxy_docid_update_table2 select * from 
	      	     internal_proxy_docid_update_table d;
	      insert ignore into internal_proxy_docid_update_table (docid, version) 
	      	     select i.docid, i.version from 
	      	     internal_proxy_documents i join internal_proxy_docid_update_table2 d2 
		     on (i.dependent_on = d2.docid);
	      select count(*) into new_len from internal_proxy_docid_update_table d;
	end while;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `create_user_surveillance`(user_id integer unsigned, surveillance_data text)
begin
	declare len integer unsigned;
	declare pos integer unsigned default 0;
	declare cur_data text;
	declare did text;
	declare site_or_page text;
	declare sd text;
	create temporary table if not exists surv_temp (docid integer unsigned, site text);
	delete from surv_temp;
	set sd = concat(surveillance_data, ';');
	set len = length(sd) - length(replace(sd, ';', ''));
	while pos < len do
	      set cur_data = substring_index(substring_index(sd, ';', pos + 1), ';', -1);
	      set did = substring_index(cur_data, ':', -1);
	      set site_or_page = substring_index(cur_data, ':', 1);
	      set pos = pos + 1;
	      insert into surv_temp (docid, site) values (did, site_or_page);
	end while;
	
	replace into user_surveillance_sites (docid, user_id)
	       select surv_temp.docid, user_id from 
	       surv_temp where site = 'omrade';
	replace into user_surveillance_docs  (docid, user_id)
	       select surv_temp.docid, user_id from 
	       surv_temp where site = 'webside';
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_document`(did integer unsigned)
begin
	delete d from documents d where id=did;
	delete vf from vfields vf where docid=did;
	delete ver from versions ver where docid=did;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_tree`(docid integer unsigned)
begin
       call backup_recursive_subdocs();
       call recursive_subdocs(docid);
       call really_delete_tree();
       call restore_recursive_subdocs();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_user_surveillance`(user_id integer unsigned)
begin
	delete from user_surveillance_docs where user_surveillance_docs.user_id = user_id;
        delete from user_surveillance_sites where user_surveillance_sites.user_id = user_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `do_search`(in_path varchar(1024), 
                           in_pattern varchar(1024), 
                           owner integer unsigned, 
                           grp integer unsigned, 
                           newer_than datetime, 
                           older_than datetime
                           )
begin
        declare pattern varchar(1024);
        declare path varchar(1026);
        set path = concat(in_path, "%");
        set pattern = concat('%', replace(in_pattern, '*', '%'), '%');
        select distinct(d.id) from
               documents d join docid_path dp on (d.id = dp.docid and dp.path like path)
               join versions v on (d.id = v.docid and (v.public = 1 or v.version = (select max(v2.version) from versions v2 where v2.docid  = d.id)))
               left join vfields vf1 on 
                    (v.docid = vf1.docid and vf1.version = v.version and 
                    vf1.name in ('content', 'title', 'short_title'))
        where    
                (pattern is null or vf1.text_value like pattern or d.name like pattern) and
                (owner is null or d.owner = owner) and
                (grp is null or d.grp = grp) and
                (older_than is null or v.version < older_than) and
                (newer_than is null or v.version > newer_than);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `find_doc_by_path`(path varchar(1024), out docid integer unsigned)
begin
     declare len integer unsigned;
     declare pos integer unsigned default 1;
     declare tid integer unsigned;
     declare cid integer unsigned default 1;
     declare pp varchar(255);
     if not path regexp "/$" then
        set path = concat(path, "/");
     end if;
     if trim(path) = '/' then
        set docid = 1;
     else         
        set len = length(path) - length(replace(path, '/', ''));
        scanner:while pos < len do 
           set pp = substring_index(substring_index(path, '/', pos + 1), '/', -1);
           set pos = pos + 1;
           if pp = '' then iterate scanner; end if;
           set tid = 0;
           
           select d.id into tid from documents d
                  where d.name = pp AND d.parent = cid;
           if tid = 0 then leave scanner; end if;
           set cid = tid;
     end while ;
     set docid = tid;
   end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `find_path_by_docid`(docid integer, out outpath varchar(1024))
begin 
      declare res text default '';
      declare docid2 integer unsigned;
      while docid != 1 do
            set docid2 = null;
            select concat(d.name,'/', res), d.parent into res, docid2 from documents d where d.id = docid;
            set docid = docid2;
      end while;
      
      select case when docid = 1 then concat('/', res) end path into outpath;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `insert_docid_path`(did integer unsigned)
begin
        declare path varchar(1024);
        call find_path_by_docid(did, path);
        delete docid_path from docid_path where docid=did;
        insert into docid_path (docid, path) values (did, path);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `insert_internal_proxy_entry`(docid integer unsigned, version datetime, dependent_on integer unsigned, fields varchar(16384))
begin
	declare cur varchar(128) default '';
	declare good integer default 0;
	declare len integer unsigned default 0;
	declare pos integer unsigned default 0;
	declare id integer unsigned;
	
	insert into internal_proxy_documents (docid, version, dependent_on) values (docid, version, dependent_on);
	set id = last_insert_id();
	
	call check_internal_proxy_status(good);
	
	if not good then
	   call ERROR_YOU_HAVE_CREATED_A_CYCLE();
	end if;
	delete i from internal_proxy_fields i where relation_id = id;
	set len = length(fields) - length(replace(fields, ',', ''));
	while pos < len + 1 do
	      set cur = substring_index(substring_index(fields, ',', pos + 1), ',', -1);
	      insert into internal_proxy_fields (relation_id, name) values (id, cur);
	      set pos = pos + 1;
	end while;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `internal_proxy_when_document_updated`(docid integer unsigned)
begin
        declare done integer default 0;
        declare a integer unsigned;
	declare v datetime;
        declare c cursor for (select d.docid, d.version from internal_proxy_documents d where d.dependent_on = docid);
        declare continue handler for not found set done=1;
        
        open c;
        fetch c into a,v;
        while not done do
              call update_internal_proxies(a, v);
              fetch c into a,v;
        end while;
        close c;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `is_internal_proxy_document`(docid integer unsigned)
begin
	select count(*) as is_ from internal_proxy_documents i where i.docid = docid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `move_document`(docid integer unsigned, new_parent integer unsigned, new_name varchar(1024))
begin
        update documents set parent=new_parent,name=coalesce(new_name,name)  where id=docid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `new_internal_proxy_entry`(docid integer unsigned, version datetime, depends_on integer unsigned, fields varchar(16384))
begin
	call insert_internal_proxy_entry(docid, version, depends_on, fields);
	call update_internal_proxy_document(docid, version);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `public_or_latest_version`(did integer unsigned, out version datetime)
begin
        select v.version into version from versions v 
               where v.docid = did and 
                     (v.public = 1 or 
                     ((not exists 
                          (select * from versions v2 where 
                                  v2.docid = did and 
                                  v2.public = 1))
                      and (v.version in (select max(v3.version) 
                          from versions v3 where v3.docid = did)))) limit 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `publish_version`(docid integer unsigned, version datetime, lang varchar(100))
begin
        update versions v set public = 0 where (lang is null or (v.lang = lang));
        update versions v set public = 1 where (v.docid = docid and v.version = version);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `read_vfields`(docid integer unsigned, version datetime, names varchar(16384))
begin
	declare pos integer unsigned default 0;
	declare len integer unsigned default 0;
	
	create temporary table if not exists read_vfields_helper (name varchar(1024) primary key) engine = heap;
	delete r from read_vfields_helper r;
	set len = length(names) - length(replace(names, ",", ""));
	while pos < len or (pos = 0 and len = 0) do
	      insert into read_vfields_helper values (substring_index(substring_index(names, ",", pos + 1), ",", -1));
	end while;
	
	select vf.* from vfields vf join read_vfields_helper r on (vf.docid = docid and vf.version = version and vf.name = r.name);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `read_vfields_from_pol`(docid integer unsigned, names varchar(16384))
begin
	declare version datetime default null;
	
	call public_or_latest_version(docid, version);
	if version is null then
	   call ERROR_NO_PUBLIC_OR_LATEST_VERSION();
	end if;	
	
	call read_vfields(docid, version, names);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `really_delete_tree`()
begin 
	declare done integer default 0;
	declare a integer unsigned;
     	declare c cursor for (select * from recursive_subdocs_table);
        declare continue handler for not found set done=1;
		
	open c;
	fetch c into a;
	while not done do call delete_document(a); fetch c into a; end while;
	delete from recursive_subdocs_table;
	close c;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `recursive_subdocs`(docid integer unsigned)
begin
        declare old_len integer unsigned default 0;
        declare new_len integer unsigned default 1;
	
	create temporary table if not exists recursive_subdocs_table2 
	       (id integer unsigned primary key) engine=heap;
	
	delete from recursive_subdocs_table;
        insert into recursive_subdocs_table set id=docid;
	
        subdocs:while old_len != new_len do
                      set old_len = new_len;
		      delete r2 from recursive_subdocs_table2 r2;
		      insert ignore into recursive_subdocs_table2 (id)
		      	     select id from recursive_subdocs_table;
                      insert ignore into recursive_subdocs_table (id) select                 
                             d.id from recursive_subdocs_table2 r2 join 
                             documents d on (r2.id = d.parent); 
                       select count(*) into new_len from recursive_subdocs_table;
         end while;
	 drop temporary table recursive_subdocs_table2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `recursive_subdocs_trigger`(docid integer unsigned)
begin
        declare old_len integer unsigned default 0;
        declare new_len integer unsigned default 1;
	
	create temporary table if not exists recursive_subdocs_table2 
	       (id integer unsigned primary key) engine=heap;
	
	delete from recursive_subdocs_trigger_table;
        insert into recursive_subdocs_trigger_table set id=docid;
	
        subdocs:while old_len != new_len do
                      set old_len = new_len;
		      delete r2 from recursive_subdocs_table2 r2;
		      insert ignore into recursive_subdocs_table2 (id)
		      	     select id from recursive_subdocs_trigger_table;
                      insert ignore into recursive_subdocs_trigger_table (id) select
                             d.id from recursive_subdocs_table2 r2 join 
                             documents d on (r2.id = d.parent); 
                      select count(*) into new_len from recursive_subdocs_trigger_table;
         end while;
	 drop temporary table recursive_subdocs_table2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `restore_recursive_subdocs`()
begin
	declare id integer unsigned;
	drop temporary table recursive_subdocs_table;
	create temporary table if not exists recursive_subdocs_table (id integer unsigned primary key auto_increment);
	select max(id) into id from recursive_subdocs_backup_list rl;
	insert into recursive_subdocs_table
	       select recursive_id from recursive_subdocs_backup rsb where rsb.backup_id = id;
	
	delete rl from recursive_subdocs_backup_list rl where rl.id = id;
	delete rsb from recursive_subdocs_backup rsb where rsb.backup_id = id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `unpublish_document`(docid integer unsigned, lang varchar(100))
begin
        update versions v set public = 0 where v.docid = docid and (lang is null or (v.lang = lang));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_internal_proxies`(docid integer unsigned, version datetime)
begin 
      call create_internal_proxy_docid_update_table(docid, version);
      call update_internal_proxies_do();
      delete d from internal_proxy_docid_update_table d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_internal_proxies_do`()
begin
	declare done integer default 0;
	declare a integer unsigned;
	declare v datetime;
	declare c cursor for (select d.docid, d.version from internal_proxy_docid_update_table d order by id asc);
        declare continue handler for not found set done=1;
	
	open c;
	fetch c into a, v;
	while not done do
	      call update_internal_proxy_document(a, v);
	      fetch c into a, v;
	end while;
	close c;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_internal_proxy_docids`(docids varchar(16384))
begin
      declare len integer unsigned;
      declare cur varchar(128) default '';
      declare pos integer unsigned default 0;
      
      start transaction;
      set len = length(docids) - length(replace(docids, ',', ''));
      while pos < len + 1 do
            set cur = substring_index(substring_index(docids, ',', pos + 1), ',', -1);
	    call internal_proxy_when_document_updated(convert(cur, unsigned));
	    set pos = pos + 1;
      end while;
      commit;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_internal_proxy_document`(docid integer unsigned, version datetime)
begin
	declare dep_docid integer unsigned default 0;
	declare dep_version datetime default null;
	
	if version is null then
	   call ERROR_NO_PUBLIC_OR_LATEST_VERSION();
	end if;
	select dependent_on into dep_docid from internal_proxy_documents i
	       where i.docid = docid and i.version = version;
	call public_or_latest_version(dep_docid, dep_version);
	       	
	if dep_version is null then
	   call ERROR_NO_PUBLIC_OR_LATEST_VERSION();
	end if;
	delete vf from vfields vf where
	       vf.docid = docid and vf.version = version and
	       vf.name not in (select ipf.name from internal_proxy_documents ipd join internal_proxy_fields ipf on 
	       	   	  (ipf.relation_id = ipd.id) where ipd.version = version and ipd.docid = docid);
	insert ignore into vfields (docid, version, name, text_value, double_value, date_value, int_value)
	       select docid, version, vf.name, vf.text_value, vf.double_value, vf.date_value, vf.int_value
	       from vfields vf where 
	       vf.docid = dep_docid and vf.version = dep_version and vf.name not in 
	       (select ipf.name from internal_proxy_documents ipd join internal_proxy_fields ipf on 
	       	   	  (ipf.relation_id = ipd.id) where ipd.docid = docid and ipd.version = version);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_move`(docid integer unsigned)
begin
	create temporary table recursive_subdocs_trigger_table (id integer unsigned primary key);
        call recursive_subdocs_trigger(docid);
 	call update_move_internal();
	drop temporary table recursive_subdocs_trigger_table;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_move_internal`()
begin
        declare path varchar(1024);
        declare a int unsigned default 0;
        declare done int default 0;
        declare curs cursor for (select * from recursive_subdocs_trigger_table);
        declare continue handler for not found set done=1;
        open curs;
        fetch curs into a;
        while not done do
              call find_path_by_docid(a, path);
	      delete docid_path from docid_path where docid=a;
              replace into docid_path (docid, path) values (a, path);
              fetch curs into a;
        end while;
        
        close curs;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$1$PLlfkYGB$xiQKV/rCgQ08YgUtbcYZV0','Admin','obviusadmin@magenta-aps.dk','',1,2,1,NULL,0);
INSERT INTO `users` VALUES (2,'nobody','','nogroup','nobody@magenta-aps.dk','',1,2,1,NULL,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Admin');
INSERT INTO `groups` VALUES (2,'nogroup');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `grp_user`
--

LOCK TABLES `grp_user` WRITE;
/*!40000 ALTER TABLE `grp_user` DISABLE KEYS */;
INSERT INTO `grp_user` VALUES (1,1);
INSERT INTO `grp_user` VALUES (2,2);
/*!40000 ALTER TABLE `grp_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `doctypes`
--

LOCK TABLES `doctypes` WRITE;
/*!40000 ALTER TABLE `doctypes` DISABLE KEYS */;
INSERT INTO `doctypes` VALUES (1,'Base',0,1,1,'sortorder'),(2,'Standard',1,1,1,NULL),(3,'Search',2,1,1,NULL),(4,'ComboSearch',3,1,1,NULL),(5,'KeywordSearch',3,1,1,NULL),(6,'HTML',1,1,1,NULL),(7,'Upload',1,1,1,NULL),(8,'Quiz',2,1,1,NULL),(9,'QuizQuestion',1,1,1,NULL),(10,'MultiChoice',2,1,1,NULL),(11,'OrderForm',6,1,1,NULL),(12,'CreateDocument',1,1,1,NULL),(13,'Sitemap',2,1,1,NULL),(14,'Subscribe',2,1,1,NULL),(15,'Image',0,1,1,NULL),(16,'Link',1,1,1,NULL),(17,'DBSearch',1,1,1,NULL),(18,'MailData',2,1,1,NULL),(19,'TableList',0,1,1,NULL),(20,'CalendarEvent',1,1,1,NULL),(21,'Calendar',1,1,1,NULL),(22,'SubDocuments',1,1,1,NULL),(23,'KategoriDokument',2,1,1,NULL),(24,'Sag',2,1,1,NULL),(25,'Stikord',2,1,1,NULL),(26,'Stikordsregister',2,1,1,NULL),(27,'HovedregisterForside',2,1,1,NULL),(28,'InspektionsOversigt',2,1,1,NULL),(29,'BeretningsOversigt',2,1,1,NULL),(30,'RSSFeed',4,1,1,NULL),(31,'SagsSoegning',1,1,1,NULL),(32,'Forside',2,1,1,NULL),(33,'NyesteSager',1,1,1,NULL),(34,'Form',1,1,1,NULL),(35,'Find',1,1,1,NULL),(36,'NavigationsSide',1,1,1,NULL),(37,'SpecifikkeSagsomraader',36,1,1,NULL),(38,'Gridside',1,1,1,NULL),(39,'FileUpload',1,1,1,NULL);
/*!40000 ALTER TABLE `doctypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `editpages`
--

LOCK TABLES `editpages` WRITE;
/*!40000 ALTER TABLE `editpages` DISABLE KEYS */;
INSERT INTO `editpages` VALUES (2,'1','Text and pictures','','title Title\nshort_title Short title\npdf_link Link til PDF-version\nhtml_link Link til HTML-version\nmat_type Materialetype\nteaser Teaser;rows=4\ncontent Text\ntoppic Top picture\nnewsauthorpic Forfatterbillede til nyheder\nnewspic Stort billede til nyheder\nauthor Author;distinct=1,no_distinct_doctype=1\ndocdate Date (yyyy-mm-dd)\nexpires Expiring'),(2,'2','Hovedregister','','myndighed Myndighed\ncat_hoved Kategorier til hovedregisteret;basepath=/da/find/udtalelser/hovedregister/'),(2,'3','Øvrige registre','','inst_type Inspektioner\ngeografisk Geografisk\nkeyword Choose the appropriate keywords for this document:'),(2,'4','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(2,'5','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1\nshow_toc Show table of contents;label_0=No, label_1=Yes, reverse_options=1\nshow_date Show date;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1\nshow_subdoc_teaser Show teaser on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nshow_subdoc_date Show date on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nsortorder Sort order of sub documents - sort according to\npagesize Number of subdocuments on one page\nsubscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual, subtitle=Subscription, nopagenav=1'),(2,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(3,'1','Free text search','A Search-document makes it possible to create a free-text search of the\nentire website.','title Title\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshort_title Short title\nteaser Teaser;rows=4\nform Alternative search-form (leave empty for default)\ndocdate Date (YYYY-MM-DD)\nseq Order of succession;subtitle=Display-only, nopagenav=1'),(3,'P','Publish document','Publish now?',''),(4,'1','Search information','','title Title\nshort_title Short title\nteaser Teaser;rows=4\nsearch_expression Search expression;no_msie_editor=1\ntoppic Top picture\ndocdate Date (YYYY-MM-DD)\nexpires Expiring'),(4,'2','Keywords','','keyword Choose the appropriate keywords for this document:'),(4,'3','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1\nshow_date Show date;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nsortorder Sort order of search results - sort according to;subtitle=Search results, nopagenav=1,disabled=1\npagesize Number of search results on one page\nnew_window Open results in new window;label_0=No, label_1=Yes, reverse_options=1\nshow_new_titles Show alphabetic index;label_0=No, label_1=Yes, reverse_options=1\nshow_searchdoc_teaser Show teaser on search results;label_0=No, label_1=Yes, reverse_options=1\nshow_searchdoc_date Show date on search results;label_0=No, label_1=Yes, reverse_options=1\nshow_searchdoc_url Show urls to search results;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1,disabled=1\nsubscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual, subtitle=Subscription, nopagenav=1'),(4,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=Forside nyt, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(5,'1','Search information','In the fields below you can specify your local search','title Title\nshort_title Short title\nteaser Teaser;rows=4\nbase Base of search\nsearch_type\nsearch_expression Type keyword;rows=1,no_msie_editor=1\ndocdate Date (YYYY-MM-DD)\nexpires Expiring'),(5,'2','Keywords','','keyword Choose the appropriate keywords for this document:'),(5,'3','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1\nshow_date Show date;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nsortorder Sort order of search results - sort according to;subtitle=Search results, nopagenav=1,disabled=1\npagesize Number of search results on one page\nnew_window Open results in new window;label_0=No, label_1=Yes, reverse_options=1\nshow_new_titles Show alphabetic index;label_0=No, label_1=Yes, reverse_options=1\nshow_searchdoc_teaser Show teaser on search results;label_0=No, label_1=Yes, reverse_options=1\nshow_searchdoc_date Show date on search results;label_0=No, label_1=Yes, reverse_options=1\nshow_searchdoc_url Show urls to search results;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1,disabled=1\nsubscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual, subtitle=Subscription, nopagenav=1'),(5,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=Forside nyt, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(6,'1','HTML and pictures','HTML-documents can contain HTML in the Text-fields. Using the \"Browse\"-button\nbelow each field a local HTML-file can be uploaded.','title Title\nshort_title Short title\nteaser Teaser;rows=4\nhtml_content HTML\ntoppic Top picture\ndocdate Date (YYYY-MM-DD)\nexpires Expiring'),(6,'2','Keywords','','keyword Choose the appropriate keywords for this document:'),(6,'3','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(6,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=Forside nyt, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(11,'1','Text','','title Title\nshort_title Short title\nteaser Teaser;rows=4\nhtml_content Form (HTML)\nmailto Send email to\nmailmsg Use email-template\ndocdate Date (YYYY-MM-DD)\nexpires Expiring'),(11,'2','Keywords','','keyword Choose keywords'),(11,'3','Display-only','','seq Order of succession\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1\nshow_subdoc_teaser Show teaser on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nshow_subdoc_date Show date on subdocuments;label_0=No, label_1=Yes, reverse_options=1'),(11,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=Forside nyt, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(13,'1','Text and levels','Documents of this type automatically generates a (dynamic) sitemap.','title Title\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshort_title Short title\ncontent Text;rows=4\nlevels Levels\ntoppic Top picture\ndocdate Date (YYYY-MM-DD)\nseq Order of succession;subtitle=Display-only, nopagenav=1\nroot Base of sitemap'),(13,'P','Publish document','Publish now?',''),(14,'1','Subscription data and display','','title Title\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshort_title Short title\nteaser Teaser (filled out);rows=4\ncontent Tekst;rows=4\ntoppic Top picture\ndocdate Date (YYYY-MM-DD)\nmailfrom Sender in subscription emails;subtitle=Subscription data, nopagenav=1,disabled=1\npasswdmsg The template used send subscription passwords\nseq Order of succession\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1'),(14,'P','Publish document','Publish the document',''),(15,'1','Image','Image-documents are a little special; they do not contain text\nbut instead image-data.','title Title\nshort_title Short title\nuploadfile Image-file;get_image_dimensions_from_upload=1\ndocdate Date (yyyy-mm-dd)'),(15,'2','Keywords','','keyword Choose the appropriate keywords for this document:'),(15,'P','Publish','Publishing the picture will make it visible on the public part\nof the website.',''),(16,'1','Link data','Link-documents are special because they redirect the user to\nthe web address when clicked. In effect they are placeholders,\nthat enable keeping classification and meta-data for external links.','title Title\nshort_title Short title\nteaser Teaser;rows=4\nurl Web address;choose=1\nauthor Author;distinct=1,no_distinct_doctype=1\nseq Order of succession\ndocdate Date (YYYY-MM-DD)\nexpires Expiring'),(16,'2','Keywords','','keyword Choose keywords'),(16,'3','Meta','The fields below are important if you want your web pages to be easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(16,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=Forside nyt, nopagenav=1\nfront_dura News duration'),(17,'1','Text','','title Title\nshort_title Short title\nteaser Teaser;rows=4\nform Alternative search-form (leave empty for default)\ntoppic Top picture\ndocdate Date (YYYY-MM-DD)\nexpires Expiring'),(17,'2','Display-only','','seq Order of succession\npagesize Number of subdocuments on one page\nsortorder Sort order of sub documents - sort according to\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1'),(17,'P','Publish document','Publish now?',''),(19,'1','Table information','This is an administrative documenttype only.','title Title\nshort_title Short title\nteaser Teaser\ntable Table\nfields Fields in list (one per line)\neditcomp Edit row component\nnewcomp New row component\ndocdate Date (yyyy-mm-dd)'),(19,'P','Publish document','Publish now?',''),(20,'1','Event Info','','title Title\nshort_title Short title\neventtype Event Type;distinct=1\ndocdate Date\neventtime Time (optional)\neventplace Place where the event occurs\ncontactinfo Contact info\neventinfo Other info'),(20,'2','Display-only','','seq Order of succession\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1\nshow_subdoc_teaser Show teaser on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nshow_subdoc_date Show date on subdocuments;label_0=No, label_1=Yes, reverse_options=1'),(20,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=Forside nyt, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(21,'1','Titel og visning','','show_as How to show calendar\nshow_event Show event by;label_type=Type, label_title=Title, reverse_options=1\ntitle Title\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nseq Order of succession\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\ndocdate Date\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1\nshow_subdoc_teaser Show teaser on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nshow_subdoc_date Show date on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nsortorder Sort order of sub documents - sort according to\npagesize Number of subdocuments on one page'),(21,'2','Search information','The fields below will be used to find relevant calendar events','startdate Events from this date\nenddate Events to this date\ns_event_path Only show events under this page (set to / for global search)\ns_event_type Event type is;subtitle=Limit the search for events,doctypename=CalendarEvent,fieldname=eventtype\ns_event_title Event title field contains\ns_event_contact Event contact info field contains\ns_event_place Event place field contains\ns_event_info Event info field contains\ns_event_order_by Order events by'),(21,'3','Subscription','','subscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual'),(21,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=Forside nyt, nopagenav=1\nfront_dura Frontpage news duration\nin_subscription Include in subscription?;label_0=No, label_1=Yes, reverse_options=1'),(23,'1','Text and pictures','','title Title\nshort_title Short title\ncat_id Klassifikations-id\nteaser Teaser;rows=4\ncontent Text\nauthor Author;distinct=1,no_distinct_doctype=1\ntoppic Top picture\ndocdate Date (yyyy-mm-dd)\nexpires Expiring'),(23,'2','Hovedregister','','myndighed Myndighed\ncat_hoved Kategorier til hovedregisteret;basepath=/da/find/udtalelser/hovedregister/'),(23,'3','Øvrige registre','','inst_type Inspektioner\ngeografisk Geografisk\nkeyword Choose the appropriate keywords for this document:'),(23,'4','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(23,'5','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(23,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(24,'1','Text and pictures','','title Title\nshort_title Short title\nsagsnr Sagsnummer;distinct=1, no_distinct_doctype=1\nsagstype Sagstype\nteksttype Tekst type\nrets_link Link til retsinfo;choose=1\npdf_link Link til PDF-version\nhtml_link Link til HTML-version\nteaser Teaser;rows=4\ncontent Text\nnyhedstitel Nyhedstitel\nnyhedsresume Nyhedsresume;rows=4\nauthor Author;distinct=1,no_distinct_doctype=1\ntoppic Top picture\ndocdate Date (yyyy-mm-dd)\nshow_date Show date;label_0=No, label_1=Yes, reverse_options=1\nnyeste_date Nye sager-dato\nexpires Expiring'),(24,'2','Hovedregister','','myndighed Myndighed\ncat_hoved Kategorier til hovedregisteret;basepath=/da/find/udtalelser/hovedregister/'),(24,'3','Øvrige registre','','inst_type Inspektioner\ngeografisk Geografisk\nkeyword Choose the appropriate keywords for this document:'),(24,'4','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(24,'5','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nsortorder Sort order of sub documents - sort according to\nsubscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual, subtitle=Subscription, nopagenav=1'),(24,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(25,'1','Titel og hovedregister','','title Title\nshort_title Short title\nhovedreg Kategori i hovedregisteret;navigate_start=/udtalelser/hovedregister/\nseq Order of succession;subtitle=Display-only, nopagenav=1\nauthor Author;distinct=1,no_distinct_doctype=1\ntoppic Top picture\ndocdate Date (yyyy-mm-dd)\nexpires Expiring'),(25,'2','Øvrige registre','','inst_type Inspektioner\ngeografisk Geografisk\nkeyword Choose the appropriate keywords for this document:'),(25,'3','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(25,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(26,'1','Text and pictures','','title Title\nshort_title Short title\nteaser Teaser;rows=4\nseq Order of succession;subtitle=Display-only, nopagenav=1\nauthor Author;distinct=1,no_distinct_doctype=1\ntoppic Top picture\ndocdate Date (yyyy-mm-dd)\nexpires Expiring'),(26,'2','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(26,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(27,'1','Text and pictures','','title Title\nshort_title Short title\nteaser Teaser;rows=4\nseq Order of succession;subtitle=Display-only, nopagenav=1\nauthor Author;distinct=1,no_distinct_doctype=1\ntoppic Top picture\ndocdate Date (yyyy-mm-dd)\nexpires Expiring'),(27,'2','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(27,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(28,'1','Text and pictures','','title Title\nshort_title Short title\nteaser Teaser;rows=4\ninstution_type Inspektions type\nseq Order of succession;subtitle=Display-only, nopagenav=1\nauthor Author;distinct=1,no_distinct_doctype=1\ntoppic Top picture\ndocdate Date (yyyy-mm-dd)\nexpires Expiring'),(28,'2','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(28,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(29,'1','Text and pictures','','title Title\nshort_title Short title\nteaser Teaser;rows=4\ndisplay_myndighed Vis myndighed\nseq Order of succession;subtitle=Display-only, nopagenav=1\ntoppic Top picture\nauthor Author;distinct=1,no_distinct_doctype=1\ndocdate Date (yyyy-mm-dd)\nexpires Expiring'),(29,'2','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(29,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(30,'1','Search information','','title Title\nshort_title Short title\nteaser Teaser;rows=4\nsearch_expression Search expression;no_msie_editor=1\ntoppic Top picture\ndocdate Date (YYYY-MM-DD)\nexpires Expiring'),(30,'2','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nsortorder Sort order of search results - sort according to;subtitle=Search results, nopagenav=1,disabled=1\npagesize Number of search results on one page'),(30,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=Forside nyt, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(31,'1','Text and pictures','','title Title\nshort_title Short title\ndocdate Date (yyyy-mm-dd)\nexpires Expiring\ntoppic Top picture'),(31,'2','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1\nshow_toc Show table of contents;label_0=No, label_1=Yes, reverse_options=1\nshow_date Show date;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1\nshow_subdoc_teaser Show teaser on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nshow_subdoc_date Show date on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nsortorder Sort order of sub documents - sort according to\npagesize Number of subdocuments on one page\nsubscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual, subtitle=Subscription, nopagenav=1'),(31,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(32,'1','Text and pictures','','title Title\nshort_title Short title\npdf_link Link til PDF-version\nhtml_link Link til HTML-version\nmat_type Materialetype\nteaser Teaser;rows=4\ncontent Text\ntoppic Top picture\nnewsauthorpic Forfatterbillede til nyheder\nnewspic Stort billede til nyheder\nauthor Author;distinct=1,no_distinct_doctype=1\ndocdate Date (yyyy-mm-dd)\nexpires Expiring'),(32,'2','Newsboxes','','box1_title Box 1 title\nbox1_content Box 1 content\nbox1_type Box 1 type;label_article=nyhedstype:Artikel,label_document=nyhedstype:Dokument,label_notice=nyhedstype:Opslag\nbox2_title Box 2 title\nbox2_content Box 2 content\nbox2_type Box 2 type;label_article=nyhedstype:Artikel,label_document=nyhedstype:Dokument,label_notice=nyhedstype:Opslag\nbox3_title Box 3 title\nbox3_content Box 3 content\nbox3_type Box 3 type;label_article=nyhedstype:Artikel,label_document=nyhedstype:Dokument,label_notice=nyhedstype:Opslag\nbox4_title Box 4 title\nbox4_content Box 4 content\nbox4_type Box 4 type;label_article=nyhedstype:Artikel,label_document=nyhedstype:Dokument,label_notice=nyhedstype:Opslag'),(32,'3','Right boxes','','rightboxes Vælg højrebokse'),(32,'4','Hovedregister','','myndighed Myndighed\ncat_hoved Kategorier til hovedregisteret;basepath=/da/find/udtalelser/hovedregister/'),(32,'5','Øvrige registre','','inst_type Inspektioner\ngeografisk Geografisk\nkeyword Choose the appropriate keywords for this document:'),(32,'6','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(32,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(33,'1','Text and pictures','','title Title\nshort_title Short title\ntoppic Top picture\ndocdate Date (yyyy-mm-dd)\nexpires Expiring'),(33,'2','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1\nshow_toc Show table of contents;label_0=No, label_1=Yes, reverse_options=1\nshow_date Show date;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1\nshow_subdoc_teaser Show teaser on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nshow_subdoc_date Show date on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nsortorder Sort order of sub documents - sort according to\npagesize Number of subdocuments on one page\nsubscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual, subtitle=Subscription, nopagenav=1'),(33,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(34,'1','Indhold','','title Title\nshort_title Short title\nseq Order of succession;subtitle=Display-only, nopagenav=1\nteaser Teaser;rows=4\nformtext Tekst over formular\naftersubmittext Tekst der vises efter udfyldning af formularen\nmailto Emailadresse data skal sendes til ved udfyldning (optional)\nauthor Sidens kontaktperson;distinct=1\ndocdate Oprettelsesdato (dd-mm-åååå)\nexpires Forældelsestidspunkt'),(34,'2','Formularfelter','','formdata Formularfelter'),(34,'3','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(34,'4','Kvittering','Husk at formularen skal indeholde et felt af typen Email for at denne kvittering kan sendes.','email_subject Emnelinje i svarsmail\nemail_text Tekst i email;no_msie_editor=1'),(34,'5','Overvågning','','entries_for_advert Antal indtastninger for advisering\nentries_for_close Antal indtastninger for lukning\nclose_message Tekst, der vises, hvis formularen er lukket for indtastning'),(34,'P','Publish document','',''),(35,'1','Text and pictures','','title Title\nshort_title Short title\ncontent_no_search Standardtekst hvis ingen søgning er udført\ncontent_no_results Tekst ved 0 søgeresultater\nhelp_path Sti til hjælpeside\ndocdate Date (yyyy-mm-dd)\nexpires Expiring\ntoppic Top picture'),(35,'2','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1\nshow_toc Show table of contents;label_0=No, label_1=Yes, reverse_options=1\nshow_date Show date;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1\nshow_subdoc_teaser Show teaser on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nshow_subdoc_date Show date on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nsortorder Sort order of sub documents - sort according to\npagesize Number of subdocuments on one page\nsubscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual, subtitle=Subscription, nopagenav=1'),(35,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(36,'1','Text and pictures','','title Title\nshort_title Short title\ndocdate Date (yyyy-mm-dd)\nexpires Expiring\ntoppic Top picture'),(36,'2','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1\nshow_toc Show table of contents;label_0=No, label_1=Yes, reverse_options=1\nshow_date Show date;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1\nshow_subdoc_teaser Show teaser on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nshow_subdoc_date Show date on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nsortorder Sort order of sub documents - sort according to\npagesize Number of subdocuments on one page\nsubscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual, subtitle=Subscription, nopagenav=1'),(36,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(37,'1','Text and pictures','','title Title\nshort_title Short title\ndocdate Date (yyyy-mm-dd)\nexpires Expiring\ntoppic Top picture'),(37,'2','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1\nshow_toc Show table of contents;label_0=No, label_1=Yes, reverse_options=1\nshow_date Show date;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1\nshow_subdoc_teaser Show teaser on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nshow_subdoc_date Show date on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nsortorder Sort order of sub documents - sort according to\npagesize Number of subdocuments on one page\nsubscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual, subtitle=Subscription, nopagenav=1'),(37,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(38,'1','Text and pictures','','title Title\nshort_title Short title\nsagsnr Sagsnummer;distinct=1, no_distinct_doctype=1\nmat_type Materialetype\nspec_sagsomr Specifikke sagsområder\nforvret_emne Forvaltningsretligt emne\nhero_display Visning af hero\nhero_align Justering af hero-indhold\nhero_picture_path Sti til hero-billede\nhero_button_path Sti til hero-knap\nhero_button_text Tekst til hero-knap\nteaser Teaser;rows=4\nexcerpt Tekst til udtræk;rows=4\nlegal_code Lovtekst;rows=4\npdf_link Link til PDF-version\nauthor Author;distinct=1,no_distinct_doctype=1\ndocdate Date (yyyy-mm-dd)\nexpires Expiring'),(38,'2','Gridindhold','','gridcontent Indhold'),(38,'3','Hovedregister','','myndighed Myndighed\ncat_hoved Kategorier til hovedregisteret;basepath=/da/find/udtalelser/hovedregister/'),(38,'4','Øvrige registre','','inst_type Inspektioner\ngeografisk Geografisk\nkeyword Choose the appropriate keywords for this document:'),(38,'5','Meta','The fields below are important if you want your web pages to\nbe easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(38,'6','Display','','seq Order of succession;subtitle=Display-only, nopagenav=1\nshow_title Show title;label_0=No, label_1=Yes, reverse_options=1\nshow_teaser Show teaser;label_0=No, label_1=Yes, reverse_options=1\nshow_toc Show table of contents;label_0=No, label_1=Yes, reverse_options=1\nshow_date Show date;label_0=No, label_1=Yes, reverse_options=1\nshow_news Show news;label_0=No, label_1=Yes, reverse_options=1\nshow_subdocs Show subdocuments;label_0=No, label_1=Yes, reverse_options=1, subtitle=Subdocuments, nopagenav=1\nshow_subdoc_teaser Show teaser on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nshow_subdoc_date Show date on subdocuments;label_0=No, label_1=Yes, reverse_options=1\nsortorder Sort order of sub documents - sort according to\npagesize Number of subdocuments on one page\nsubscribeable Subscription possibility;label_none=No, label_automatic=Automatic, label_manual=Manual, subtitle=Subscription, nopagenav=1'),(38,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=News, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1'),(39,'1','Upload','The Upload-document if for all types of binary files besides\nimages (for instance PDF-files, Word-documents, Excel-files etc.)','title Title\nshort_title Short title\nteaser Teaser;rows=4\nmimetype MIME-type;distinct=1\nuploadfile Upload data\nauthor Author;distinct=1,no_distinct_doctype=1\nseq Order of succession\ndocdate Date (YYYY-MM-DD)\nexpires Expiring'),(39,'2','Keywords','','keyword Choose the appropriate keywords for this document:'),(39,'3','Meta','The fields below are important if you want your web pages to be easily found by search machines and users of the Internet','docref Reference\ncontributors Contributors\nsource Source'),(39,'P','Publish document','','front_prio Priority;label_0=Not_on_list, label_1=Always_last, label_2=Low_priority, label_3=High_priority, subtitle=Forside nyt, nopagenav=1\nfront_dura News duration\nin_subscription Include in subscription;label_0=No, label_1=Yes, reverse_options=1');
/*!40000 ALTER TABLE `editpages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fieldtypes`
--

LOCK TABLES `fieldtypes` WRITE;
/*!40000 ALTER TABLE `fieldtypes` DISABLE KEYS */;
INSERT INTO `fieldtypes` VALUES (1,'halign','radio','right|left|center','regexp','^(right|left|center)$','none','',0,'text'),(2,'category','category','categories.id','xref','categories.id','none','',0,'text'),(3,'keyword','keyword','keywords.id','xref','keywords.id','matchColumn','name',0,'int'),(4,'imagedata','imageupload','','none','','none','',1,'text'),(5,'date','date','','regexp','^\\d\\d\\d\\d-\\d\\d-\\d\\d( 00:00:00)?$','none','',0,'date'),(6,'datetime','datetime','','regexp','^\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d$','none','',0,'date'),(7,'time','time','','regexp','^\\d\\d:\\d\\d:\\d\\d$','none','',0,'date'),(8,'title','line','','regexp','.','none','',0,'text'),(9,'line','line','','none','','none','',0,'text'),(10,'text','tinymce3','','none','','none','',0,'text'),(11,'textwupload','textwupload','','none','','none','',0,'text'),(12,'double','line','','regexp','^-?\\d+(\\.\\d+)?','none','',0,'double'),(13,'lang','line','','regexp','^\\w\\w$','none','',0,'text'),(14,'require','radio','normal|teaser|fullinfo','regexp','^(teaser|fullinfo|normal)$','none','',0,'text'),(15,'bool','radio','0|1','regexp','^[01]$','none','',0,'int'),(16,'int>0','line','','regexp','^[1-9]\\d*$','none','',0,'int'),(17,'int>=0','line','','regexp','^\\d+$','none','',0,'int'),(18,'int','line','','regexp','^-?\\d+$','none','',0,'int'),(19,'appdata','fileupload','','none','','none','',1,'text'),(20,'sortorder','sortorder','','regexp','.','none','',0,'text'),(21,'template','xref','templates.id','xref','templates.id','none','',0,'int'),(22,'path','path','','special','DocumentPathCheck','none','',0,'int'),(23,'searchtype','radio','keyword|category|month|weeks','regexp','^(keyword|category|month|weeks)$','none','',0,'text'),(24,'email','line','','regexp','^[^@]+@[^@]+\\.\\w+$','none','',0,'text'),(25,'priority','radio','0|1|2|3','regexp','^[0123]$','none','',0,'int'),(26,'subscribeable','radio','none|automatic|manual','regexp','^(none|automatic|manual)$','none','',0,'text'),(27,'showcal','radio','2D|list','regexp','^(2D|list)$','none','',0,'text'),(28,'publishmode','radio','immediate|moderator','regexp','^(immediate|moderator)$','none','',0,'text'),(29,'publish_on','publishon','','regexp','^\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d$','none','',0,'date'),(30,'orderevents','radio','+title|-docdate|+docdate|+eventtype|+contactinfo','regexp','^(\\+title|-docdate|\\+docdate|\\+eventtype|\\+contactinfo)$','none','',0,'text'),(31,'pagesize','pagesize','','regexp','^\\d+$','none','',0,'int'),(32,'fileupload2','fileupload2','','none','','none','',0,'text'),(33,'priority6','radio','1|2|3|4|5','regexp','^[12345]$','none','',0,'int'),(34,'showevent','radio','type|title','regexp','^(type|title)$','none','',0,'text'),(35,'multicat','multicategory','','none','','none','',0,'text'),(36,'catid','catid','','none','','none','',0,'text'),(37,'myndighed','myndighed','','none','','none','',0,'text'),(38,'insttype','insttype','','none','','none','',0,'text'),(39,'geografisk','geografisk','','none','','none','',0,'text'),(40,'sagstype','radio','beretning|inspektion','regexp','^(beretning|inspektion)$','none','',0,'text'),(41,'teksttype','radio','resume|fuldtekst','regexp','^(resume|fuldtekst)$','none','',0,'text'),(42,'formdata','formdata','','none','','none','',0,'text'),(43,'simpletext','text','','none','','none','',0,'text'),(44,'nyhedstype','radio','article|document|notice','regexp','^(article|document|notice)$','none','',0,'text'),(45,'multipath','multipath','','none','','none','',0,'text'),(46,'docidlink','docidlink','','special','DocidLink','none','',0,'text'),(47,'datewithnull','datewithnull','','regexp','^\\d\\d\\d\\d-\\d\\d-\\d\\d( 00:00:00)?$','none','',0,'date'),(48,'mattype','mattype','','none','','none','',0,'text'),(49,'specsagsomr','specsagsomr','','none','','none','',0,'text'),(50,'forvemne','forvemne','','none','','none','',0,'text'),(51,'gridcontent','gridcontent','','none','','none','',0,'text'),(52,'herodisplay','herodisplay','','none','','none','',0,'text'),(53,'sequence','sequence','','regexp','^-?\\d+(\\.\\d+)?','none','',0,'double');
/*!40000 ALTER TABLE `fieldtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fieldspecs`
--

LOCK TABLES `fieldspecs` WRITE;
/*!40000 ALTER TABLE `fieldspecs` DISABLE KEYS */;
INSERT INTO `fieldspecs` VALUES (1,'cat_hoved',35,1,1,1,1,0,128,NULL,NULL),(1,'docdate',5,0,0,1,1,0,0,NULL,NULL),(1,'expires',6,0,0,1,1,0,0,'9999-01-01 00:00:00',NULL),(1,'forvret_emne',50,0,1,1,1,0,128,NULL,NULL),(1,'front_dura',17,0,0,1,1,1,128,'0',NULL),(1,'front_prio',25,0,0,1,1,1,128,'0',NULL),(1,'geografisk',39,0,1,1,1,0,128,NULL,NULL),(1,'hero_align',1,0,0,0,0,0,0,'center',NULL),(1,'hero_button_path',46,0,1,1,1,0,128,NULL,NULL),(1,'hero_button_text',9,0,1,1,1,0,0,NULL,NULL),(1,'hero_display',52,0,0,1,1,0,128,'use local',NULL),(1,'hero_picture_path',46,0,1,1,1,0,128,NULL,NULL),(1,'inst_type',38,0,1,1,1,0,128,NULL,NULL),(1,'in_subscription',15,0,0,1,1,1,128,'0',NULL),(1,'keyword',3,1,1,1,1,0,128,NULL,NULL),(1,'mat_type',48,0,1,1,1,0,128,NULL,NULL),(1,'mimetype',9,0,0,1,1,0,0,NULL,NULL),(1,'myndighed',37,0,1,1,1,0,128,NULL,NULL),(1,'pagesize',31,0,1,0,0,0,128,NULL,NULL),(1,'published',6,0,0,1,1,1,128,NULL,NULL),(1,'publish_on',29,0,0,1,1,1,128,'0000-01-01 00:00:00',NULL),(1,'sec',18,0,1,1,1,1,128,'0',NULL),(1,'seq',53,0,0,0,1,0,0,'10.00',NULL),(1,'short_title',9,0,0,1,1,0,0,NULL,NULL),(1,'show_date',15,0,0,0,0,0,128,'0',NULL),(1,'show_news',15,0,0,0,0,0,128,'0',NULL),(1,'show_subdocs',15,0,0,0,0,0,128,'0',NULL),(1,'show_subdoc_date',15,0,0,0,0,0,128,'0',NULL),(1,'show_subdoc_teaser',15,0,0,0,0,0,128,'0',NULL),(1,'show_teaser',15,0,0,0,0,0,128,'1',NULL),(1,'show_title',15,0,0,0,0,0,128,'1',NULL),(1,'show_toc',15,0,0,0,0,0,128,'0',NULL),(1,'sortorder',20,0,0,0,0,0,128,'+seq,+title',NULL),(1,'spec_sagsomr',49,0,1,1,1,0,128,NULL,NULL),(1,'subscribeable',26,0,0,1,0,0,128,'none',NULL),(1,'title',8,0,0,1,1,0,0,NULL,NULL),(1,'toppic',46,0,1,1,1,0,128,NULL,NULL),(2,'author',9,0,0,1,1,0,64,NULL,NULL),(2,'content',10,0,0,1,1,0,128,NULL,NULL),(2,'contributors',9,0,0,1,1,0,64,NULL,NULL),(2,'docref',9,0,0,1,1,0,64,NULL,NULL),(2,'html_link',22,0,1,1,1,0,128,NULL,NULL),(2,'newsauthorpic',46,0,1,1,1,0,128,NULL,NULL),(2,'newspic',46,0,1,1,1,0,128,NULL,NULL),(2,'pdf_link',22,0,1,1,1,0,128,NULL,NULL),(2,'picture',22,0,1,1,1,0,128,NULL,NULL),(2,'section_news',15,0,0,0,0,0,128,'0',NULL),(2,'source',9,0,0,1,1,0,64,NULL,NULL),(2,'teaser',10,0,0,1,0,0,64,NULL,NULL),(2,'toppic',46,0,1,1,1,0,128,NULL,NULL),(2,'url',9,0,0,1,1,0,64,NULL,NULL),(3,'form',11,0,0,1,1,0,128,NULL,NULL),(3,'new_window',15,0,0,0,0,0,128,'0',NULL),(3,'search_expression',11,0,0,0,0,0,128,NULL,NULL),(3,'show_new_titles',15,0,0,0,0,0,128,'0',NULL),(3,'show_searchdoc_date',15,0,0,0,0,0,128,'0',NULL),(3,'show_searchdoc_teaser',15,0,0,0,0,0,128,'0',NULL),(3,'show_searchdoc_url',15,0,0,0,0,0,128,'0',NULL),(3,'show_teasers',15,0,0,0,0,0,128,'0',NULL),(3,'show_urls',15,0,0,0,0,0,128,'0',NULL),(4,'show_new_titles',15,0,0,0,0,0,128,'0',NULL),(4,'toppic',46,0,1,1,1,0,128,NULL,NULL),(5,'base',22,0,0,0,0,0,128,NULL,NULL),(5,'search_type',23,0,0,0,0,0,128,'keyword',NULL),(5,'show_new_titles',15,0,0,0,0,0,128,'0',NULL),(6,'author',9,0,0,1,1,0,64,NULL,NULL),(6,'contributors',9,0,0,1,1,0,64,NULL,NULL),(6,'docref',9,0,0,1,1,0,64,NULL,NULL),(6,'html_content',11,0,0,1,1,0,128,NULL,NULL),(6,'source',9,0,0,1,1,0,64,NULL,NULL),(6,'teaser',10,0,0,1,0,0,64,NULL,NULL),(6,'url',9,0,0,1,1,0,64,NULL,NULL),(7,'author',9,0,0,1,1,0,64,NULL,NULL),(7,'contributors',9,0,0,1,1,0,64,NULL,NULL),(7,'docref',9,0,0,1,1,0,64,NULL,NULL),(7,'size',17,0,0,0,0,0,192,NULL,NULL),(7,'source',9,0,0,1,1,0,64,NULL,NULL),(7,'teaser',10,0,0,1,0,0,64,NULL,NULL),(7,'uploaddata',19,0,0,0,0,0,192,NULL,NULL),(7,'url',9,0,0,1,1,0,64,NULL,NULL),(8,'mailmsg',9,0,0,0,0,0,128,NULL,NULL),(8,'mailto',24,0,0,0,0,0,128,NULL,NULL),(8,'requireallanswers',15,0,0,0,0,0,128,NULL,NULL),(9,'answer',10,1,0,0,0,0,128,NULL,NULL),(9,'correctanswer',10,0,0,0,0,0,128,NULL,NULL),(9,'question',10,0,0,0,0,0,128,NULL,NULL),(9,'url',9,0,0,0,0,0,128,NULL,NULL),(10,'bar_width',16,0,0,0,0,0,128,'65',NULL),(10,'vote_option',9,1,0,0,0,0,128,NULL,NULL),(11,'mailmsg',9,0,0,0,0,0,128,NULL,NULL),(11,'mailto',24,0,0,0,0,0,128,NULL,NULL),(12,'doctype',9,0,0,1,1,0,128,NULL,NULL),(12,'email',24,0,1,1,1,0,128,NULL,NULL),(12,'form',11,0,0,1,0,0,128,NULL,NULL),(12,'language',13,0,0,1,1,0,128,'da',NULL),(12,'name_prefix',9,0,0,1,1,0,128,NULL,NULL),(12,'publish_mode',28,0,0,1,1,0,128,'moderator',NULL),(12,'subscribe_include',15,0,0,1,1,0,128,'0',NULL),(12,'where',22,0,0,1,1,0,128,NULL,NULL),(13,'levels',33,0,0,0,0,0,128,'2',NULL),(13,'root',22,0,1,0,0,0,128,NULL,NULL),(14,'mailfrom',24,0,0,0,0,0,128,NULL,NULL),(14,'passwdmsg',9,0,0,0,0,0,128,'mail/subscribe_passwd',NULL),(14,'show_teaser',15,0,0,0,0,0,128,'0',NULL),(14,'show_title',15,0,0,0,0,0,128,'0',NULL),(14,'toppic',46,0,1,1,1,0,128,NULL,NULL),(15,'align',1,0,0,0,0,0,0,'center',NULL),(15,'data',4,0,0,0,0,0,192,NULL,NULL),(15,'docdate',5,0,0,1,1,0,0,NULL,NULL),(15,'expires',6,0,0,1,1,0,0,'9999-01-01 00:00:00',NULL),(15,'height',17,0,0,0,0,0,0,NULL,NULL),(15,'keyword',3,1,1,1,1,0,128,NULL,NULL),(15,'mimetype',9,0,0,1,1,0,0,NULL,NULL),(15,'published',6,0,0,1,1,1,0,NULL,NULL),(15,'seq',53,0,0,0,1,0,0,'-10.00',NULL),(15,'short_title',9,0,0,1,1,0,0,NULL,NULL),(15,'size',17,0,0,0,0,0,0,NULL,NULL),(15,'title',8,0,0,1,1,0,0,NULL,NULL),(15,'uploadfile',32,0,0,0,0,0,0,NULL,NULL),(15,'width',17,0,0,0,0,0,0,NULL,NULL),(16,'author',9,0,0,1,1,0,64,NULL,NULL),(16,'contributors',9,0,0,1,1,0,64,NULL,NULL),(16,'docref',9,0,0,1,1,0,64,NULL,NULL),(16,'source',9,0,0,1,1,0,64,NULL,NULL),(16,'teaser',10,0,0,1,0,0,64,NULL,NULL),(16,'url',9,0,0,1,1,0,64,NULL,NULL),(17,'form',11,0,0,1,1,0,128,NULL,NULL),(17,'show_teaser',15,0,0,0,0,0,128,'0',NULL),(17,'show_title',15,0,0,0,0,0,128,'0',NULL),(17,'teaser',10,0,0,1,1,0,128,NULL,NULL),(18,'mailfrom',24,0,0,0,0,0,128,NULL,NULL),(19,'docdate',5,0,0,1,1,0,0,NULL,NULL),(19,'editcomp',9,0,0,0,0,0,64,NULL,NULL),(19,'expires',6,0,0,1,1,0,0,'9999-01-01 00:00:00',NULL),(19,'fields',10,0,0,0,0,0,64,NULL,NULL),(19,'newcomp',9,0,0,0,0,0,64,NULL,NULL),(19,'pagesize',31,0,1,0,0,0,128,NULL,NULL),(19,'published',6,0,0,1,1,1,128,NULL,NULL),(19,'seq',53,0,0,0,1,0,0,'10.00',NULL),(19,'short_title',9,0,0,1,1,0,0,NULL,NULL),(19,'sortorder',20,0,0,0,0,0,128,'+id',NULL),(19,'table',9,0,0,0,0,0,64,NULL,NULL),(19,'teaser',10,0,0,1,0,0,64,NULL,NULL),(19,'title',8,0,0,1,1,0,0,NULL,NULL),(20,'contactinfo',9,0,0,1,1,0,64,NULL,NULL),(20,'eventinfo',10,0,0,1,1,0,128,NULL,NULL),(20,'eventplace',9,0,0,1,1,0,64,NULL,NULL),(20,'eventtime',9,0,0,1,1,0,64,NULL,NULL),(20,'eventtype',9,0,0,1,1,0,64,NULL,NULL),(21,'enddate',5,0,0,0,0,0,128,NULL,NULL),(21,'show_as',27,0,0,1,0,0,128,'2D',NULL),(21,'show_event',34,0,0,1,0,0,128,'title',NULL),(21,'startdate',5,0,0,0,0,0,128,NULL,NULL),(21,'s_event_contact',9,0,0,0,0,0,128,NULL,NULL),(21,'s_event_info',9,0,0,0,0,0,128,NULL,NULL),(21,'s_event_order_by',30,0,0,0,0,0,128,'-docdate',NULL),(21,'s_event_path',22,0,0,0,0,0,128,NULL,NULL),(21,'s_event_place',9,0,0,0,0,0,128,NULL,NULL),(21,'s_event_title',9,0,0,0,0,0,128,NULL,NULL),(21,'s_event_type',9,0,0,0,0,0,128,NULL,NULL),(22,'author',9,0,0,1,1,0,64,NULL,NULL),(22,'contributors',9,0,0,1,1,0,64,NULL,NULL),(22,'docref',9,0,0,1,1,0,64,NULL,NULL),(22,'logo',22,0,1,1,1,0,128,NULL,NULL),(22,'picture',22,0,1,1,1,0,128,NULL,NULL),(22,'show_teasers',15,0,0,1,1,0,128,'0',NULL),(22,'source',9,0,0,1,1,0,64,NULL,NULL),(22,'teaser',10,0,0,1,0,0,64,NULL,NULL),(22,'url',9,0,0,1,1,0,64,NULL,NULL),(23,'cat_id',36,0,0,1,1,0,128,NULL,NULL),(24,'html_link',22,0,1,1,1,0,128,NULL,NULL),(24,'nyeste_date',47,0,1,1,1,0,128,NULL,NULL),(24,'nyhedsresume',10,0,0,1,1,0,128,NULL,NULL),(24,'nyhedstitel',9,0,0,1,1,0,128,NULL,NULL),(24,'pdf_link',22,0,1,1,1,0,128,NULL,NULL),(24,'rets_link',9,0,0,1,1,0,128,NULL,NULL),(24,'sagsnr',9,0,0,1,1,0,128,NULL,NULL),(24,'sagstype',40,0,0,1,0,0,128,NULL,NULL),(24,'teksttype',41,0,0,0,0,0,128,'fuldtekst',NULL),(25,'hovedreg',22,0,0,1,1,0,128,NULL,NULL),(28,'instution_type',38,0,0,1,1,0,128,NULL,NULL),(29,'display_myndighed',37,0,1,1,1,0,128,NULL,NULL),(32,'box1_content',10,0,1,1,1,0,128,NULL,NULL),(32,'box1_title',9,0,1,1,1,0,128,NULL,NULL),(32,'box1_type',44,0,1,1,1,0,128,'article',NULL),(32,'box2_content',10,0,1,1,1,0,128,NULL,NULL),(32,'box2_title',9,0,1,1,1,0,128,NULL,NULL),(32,'box2_type',44,0,1,1,1,0,128,'article',NULL),(32,'box3_content',10,0,1,1,1,0,128,NULL,NULL),(32,'box3_title',9,0,1,1,1,0,128,NULL,NULL),(32,'box3_type',44,0,1,1,1,0,128,'article',NULL),(32,'box4_content',10,0,1,1,1,0,128,NULL,NULL),(32,'box4_title',9,0,1,1,1,0,128,NULL,NULL),(32,'box4_type',44,0,1,1,1,0,128,'article',NULL),(32,'rightboxes',45,1,1,1,1,0,128,NULL,NULL),(34,'aftersubmittext',10,0,0,1,1,0,128,NULL,NULL),(34,'author',9,0,0,1,1,0,64,NULL,NULL),(34,'captcha',15,0,0,1,1,0,128,'0',NULL),(34,'close_message',10,0,1,1,1,0,128,NULL,NULL),(34,'contributors',9,0,0,1,1,0,64,NULL,NULL),(34,'docref',9,0,0,1,1,0,64,NULL,NULL),(34,'email_subject',9,0,1,1,1,0,128,NULL,NULL),(34,'email_text',43,0,1,1,1,0,128,NULL,NULL),(34,'entries_for_advert',18,0,1,1,1,0,128,'0',NULL),(34,'entries_for_close',18,0,1,1,1,0,128,'0',NULL),(34,'formdata',42,0,0,1,1,0,128,NULL,NULL),(34,'formtext',10,0,0,1,1,0,128,NULL,NULL),(34,'mailto',24,0,1,1,1,0,128,NULL,NULL),(34,'source',9,0,0,1,1,0,64,NULL,NULL),(34,'teaser',10,0,0,1,0,0,64,NULL,NULL),(35,'content_no_results',10,0,0,1,1,0,128,NULL,NULL),(35,'content_no_search',10,0,0,1,1,0,128,NULL,NULL),(35,'help_path',46,0,1,1,1,0,128,NULL,NULL),(38,'author',9,0,0,1,1,0,64,NULL,NULL),(38,'contributors',9,0,0,1,1,0,64,NULL,NULL),(38,'docref',9,0,0,1,1,0,64,NULL,NULL),(38,'excerpt',10,0,0,1,0,0,64,NULL,NULL),(38,'gridcontent',51,0,0,1,1,0,128,NULL,NULL),(38,'legal_code',10,0,0,1,0,0,128,NULL,NULL),(38,'pdf_link',22,0,1,1,1,0,128,NULL,NULL),(38,'sagsnr',9,0,0,1,1,0,128,NULL,NULL),(38,'source',9,0,0,1,1,0,64,NULL,NULL),(38,'teaser',10,0,0,1,0,0,64,NULL,NULL),(39,'author',9,0,0,1,1,0,64,NULL,NULL),(39,'contributors',9,0,0,1,1,0,64,NULL,NULL),(39,'disable_cache',15,0,0,1,1,0,128,'0',NULL),(39,'docref',9,0,0,1,1,0,64,NULL,NULL),(39,'seq',53,0,0,0,1,0,0,'-10.00',NULL),(39,'size',17,0,0,0,0,0,192,NULL,NULL),(39,'source',9,0,0,1,1,0,64,NULL,NULL),(39,'teaser',10,0,0,1,0,0,64,NULL,NULL),(39,'uploadfile',32,0,0,0,0,0,192,NULL,NULL);
/*!40000 ALTER TABLE `fieldspecs` ENABLE KEYS */;
UNLOCK TABLES;


-- Dump documents for root and admin

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` VALUES
(1,0,'dummy',2,1,1,'admin=create,edit,delete,publish,modes,admin\nOWNER=create,edit,delete,publish,modes\nGROUP+create,edit,delete,publish\nALL+view\n@Admin+admin'),
(2,1,'admin',6,1,1,NULL);
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `versions` WRITE;
/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` VALUES
(1,'2018-02-22 11:10:16',16,1,0,'da',1),
(2,'2019-12-04 11:41:43',6,1,0,'da',6);
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `vfields` WRITE;
/*!40000 ALTER TABLE `vfields` DISABLE KEYS */;
INSERT INTO `vfields` VALUES
(1,'2018-02-22 11:10:16','AUTHOR','nasl',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','CONTRIBUTORS','',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','DOCDATE',NULL,NULL,NULL,'2003-03-28 00:00:00'),(1,'2018-02-22 11:10:16','DOCREF','',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','EXPIRES',NULL,NULL,NULL,'9999-01-01 00:00:00'),(1,'2018-02-22 11:10:16','FRONT_DURA',NULL,0,NULL,NULL),(1,'2018-02-22 11:10:16','FRONT_PRIO',NULL,0,NULL,NULL),(1,'2018-02-22 11:10:16','HERO_ALIGN','center',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','HERO_DISPLAY','use local',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','IN_SUBSCRIPTION',NULL,0,NULL,NULL),(1,'2018-02-22 11:10:16','MIMETYPE','',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','PUBLISHED',NULL,NULL,NULL,'2018-02-22 11:10:16'),(1,'2018-02-22 11:10:16','PUBLISH_ON',NULL,NULL,NULL,'1000-01-01 00:00:00'),(1,'2018-02-22 11:10:16','SEC',NULL,0,NULL,NULL),(1,'2018-02-22 11:10:16','SEQ',NULL,NULL,0,NULL),(1,'2018-02-22 11:10:16','SHORT_TITLE','Folketingets Ombudsmand',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','SHOW_DATE',NULL,0,NULL,NULL),(1,'2018-02-22 11:10:16','SHOW_NEWS',NULL,1,NULL,NULL),(1,'2018-02-22 11:10:16','SHOW_SUBDOCS',NULL,0,NULL,NULL),(1,'2018-02-22 11:10:16','SHOW_SUBDOC_DATE',NULL,0,NULL,NULL),(1,'2018-02-22 11:10:16','SHOW_SUBDOC_TEASER',NULL,0,NULL,NULL),(1,'2018-02-22 11:10:16','SHOW_TEASER',NULL,1,NULL,NULL),(1,'2018-02-22 11:10:16','SHOW_TITLE',NULL,0,NULL,NULL),(1,'2018-02-22 11:10:16','SHOW_TOC',NULL,0,NULL,NULL),(1,'2018-02-22 11:10:16','SORTORDER','+seq,+title',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','SOURCE','Folketingets ombudsmand',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','SUBSCRIBEABLE','none',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','TEASER','',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','TITLE','Folketingets Ombudsmand',NULL,NULL,NULL),(1,'2018-02-22 11:10:16','URL','/da/',NULL,NULL,NULL),
(2,'2019-12-04 11:41:43','AUTHOR',NULL,NULL,NULL,NULL),(2,'2019-12-04 11:41:43','CONTRIBUTORS','',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','DOCDATE',NULL,NULL,NULL,'2003-03-31 00:00:00'),(2,'2019-12-04 11:41:43','DOCREF','',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','EXPIRES',NULL,NULL,NULL,'9999-01-01 00:00:00'),(2,'2019-12-04 11:41:43','FRONT_DURA',NULL,0,NULL,NULL),(2,'2019-12-04 11:41:43','FRONT_PRIO',NULL,0,NULL,NULL),(2,'2019-12-04 11:41:43','HERO_ALIGN','center',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','HERO_DISPLAY','use local',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','HTML_CONTENT','<p>\r\nAdministration af brugere, grupper, abonnenter og login-adgang:\r\n<ul>\r\n<li><a href=\"./?obvius_command_users=1\">Brugere</a></li>\r\n<li><a href=\"./?obvius_command_groups=1\">Grupper</a></li>\r\n<li><a href=\"./?obvius_command_subscribers=1\">Abonnenter</a></li>\r\n<li><a href=\"./?obvius_command_subscribables=1\">Abonnements-oversigt</a></li>\r\n<li><a href=\"./?obvius_app_download_subscription_lists=1\" onclick=\"this.target=\'_blank\'\">Download abonnements-oversigt i excel-format (åbner i nyt vindue)</a></li>\r\n<li><a href=\"./?obvius_command_keywords=1\">Nøgleord</a></li>\r\n<li><a href=\"./?obvius_command_queue=1\">Kommando-kø</a></li>\r\n<li><a href=\"./?obvius_command_newsletter_sending_time=1\">Tidsforbrug ved udsendelse af nyhedsbrev</a></li>\r\n<li><a href=\"./?obvius_command_subscription_areas=1\">Administrér nyhedsbrev-områder</a></li>\r\n<li><a href=\"./?obvius_command_klageskema_mail_log=1\">Log over mails afsendt fra klageskema</a></li>\r\n<li><a href=\"./?obvius_command_email_registration_table=1\">Overblik over sendte klager og valideringsfejl ved udfyldning af klager</a></li>\r\n\r\n</ul>\r\n</p>\r\n\r\n<p>\r\nServer-cache:\r\n<ul>\r\n<li><a href=\"./?obvius_op=clear_cache\">Ryd server-cache</a>.\r\n</ul>\r\n</p>\r\n\r\n<p>\r\nAdminJump:\r\n</p>\r\n\r\n<p>\r\nHøjreklik på AdminJump-linket nedenfor og vælg \"Føj til foretrukne...\".  Hvis værktøjslinien \"Hyperlinks\" ikke er slået til, så gør det i menuen \"Vis/Værktøjslinier/Hyperlinks\" - og træk derefter linket fra foretrukne menuen ned i værktøjslinien. Knappen \"AdminJump\" i værktøjslinien kan nu bruges til at hoppe direkte fra en side på det offentlige website og til administrationsdelens tilsvarende side (og tilbage igen).\r\n<ul>\r\n<li><a href=\"javascript:q=location.href;if(q&&q!=%22%22){q=String(q);r=new RegExp(%22http[s]?(://[^/]*/)(.*)%22);m=q.match(r);if(m){admin=%22admin/%22;ra=new RegExp(%22admin/(.*)%22);ma=m[2].match(ra);if(ma){admin=%22%22;m[2]=ma[1];}location.href=%22http%22+m[1]+admin+m[2];}else{alert(%22Dont know how to admin %22+q+%22, sorry%22);location.href=location.href;}}else{location.href=location.href;}\">AdminJump</a></li>\r\n<li>Edit-jump: Admin-jump direkte til første redigeringsside: <a href=\"javascript:q=location.href;if(q&&q!=%22%22){q=String(q);admin=%22admin/%22;r=new RegExp(%22^(https?://[^/]*/)([^?]*)%22);m=q.match(r);if(m){start=m[1];path=m[2];args=%22%22;ra=new RegExp(%22^%22+admin+%22(.*)%22);ma=path.match(ra);if(ma){path=ma[1];args=%22%22}else{path=admin+path;args=\'?obvius_command_edit=1\';}location.href=start+path+args;}else{alert(%22Dontknowhowtoadmin%22+q+%22,sorry%22)};}\">EditJump</a></li>\r\n</ul>\r\n</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','IN_SUBSCRIPTION',NULL,0,NULL,NULL),(2,'2019-12-04 11:41:43','MIMETYPE',NULL,NULL,NULL,NULL),(2,'2019-12-04 11:41:43','PUBLISHED',NULL,NULL,NULL,'2019-12-04 11:41:43'),(2,'2019-12-04 11:41:43','PUBLISH_ON',NULL,NULL,NULL,'0000-01-01 00:00:00'),(2,'2019-12-04 11:41:43','SEC',NULL,0,NULL,NULL),(2,'2019-12-04 11:41:43','SEQ',NULL,NULL,-100,NULL),(2,'2019-12-04 11:41:43','SHORT_TITLE','Administration',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','SHOW_DATE',NULL,0,NULL,NULL),(2,'2019-12-04 11:41:43','SHOW_NEWS',NULL,0,NULL,NULL),(2,'2019-12-04 11:41:43','SHOW_SUBDOCS',NULL,0,NULL,NULL),(2,'2019-12-04 11:41:43','SHOW_SUBDOC_DATE',NULL,0,NULL,NULL),(2,'2019-12-04 11:41:43','SHOW_SUBDOC_TEASER',NULL,0,NULL,NULL),(2,'2019-12-04 11:41:43','SHOW_TEASER',NULL,1,NULL,NULL),(2,'2019-12-04 11:41:43','SHOW_TITLE',NULL,1,NULL,NULL),(2,'2019-12-04 11:41:43','SHOW_TOC',NULL,0,NULL,NULL),(2,'2019-12-04 11:41:43','SORTORDER','+seq,+title',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','SOURCE','',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','SUBSCRIBEABLE','none',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','TEASER','',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','TITLE','Administration',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','TOPPIC','',NULL,NULL,NULL),(2,'2019-12-04 11:41:43','URL',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `vfields` ENABLE KEYS */;
UNLOCK TABLES;


/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
