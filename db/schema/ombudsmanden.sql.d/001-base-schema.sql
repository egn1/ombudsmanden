/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_leftmenu_cache` (
  `id` varchar(255) NOT NULL,
  `cache_data` mediumblob,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annotations` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` smallint(5) unsigned NOT NULL DEFAULT '0',
  `text` text,
  PRIMARY KEY (`id`),
  KEY `docid` (`docid`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apache_edit_sessions` (
  `id` char(32) NOT NULL,
  `a_session` mediumblob,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apache_user_sessions` (
  `id` char(32) NOT NULL,
  `a_session` mediumblob,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apachecache_home` (
  `uri` mediumblob NOT NULL,
  `querystring` varchar(255) NOT NULL DEFAULT '',
  `cache_uri` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`uri`(255),`querystring`),
  KEY `apachecache_home_querystring_idx` (`querystring`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applied_migrations` (
  `migration` varchar(255) NOT NULL,
  `applied_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`migration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` char(9) NOT NULL DEFAULT '',
  `name` char(127) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `name` varchar(127) NOT NULL DEFAULT '',
  `email` varchar(63) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  PRIMARY KEY (`docid`,`date`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `complaint_sessions` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `submission_uuid` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `anonymized_user` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `docid` int(8) unsigned DEFAULT NULL,
  `latest_status` varchar(255) COLLATE utf8_danish_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `name` varchar(127) NOT NULL DEFAULT '',
  `value` varchar(127) NOT NULL DEFAULT '',
  `descriptions` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docid_path` (
  `path` varchar(1024) DEFAULT NULL,
  `docid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`docid`),
  KEY `path` (`path`(767))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docparams_backup` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `deletion_time` datetime NOT NULL,
  `docid` int(8) unsigned NOT NULL,
  `name` varchar(127) NOT NULL,
  `value` longtext,
  `type` int(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docparms` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(127) NOT NULL DEFAULT '',
  `value` longtext,
  `type` int(8) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctypes` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL DEFAULT '',
  `parent` int(8) unsigned NOT NULL,
  `basis` int(1) unsigned NOT NULL DEFAULT '0',
  `searchable` int(1) NOT NULL DEFAULT '1',
  `sortorder_field_is` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `type` int(8) unsigned NOT NULL DEFAULT '0',
  `owner` smallint(5) unsigned NOT NULL DEFAULT '0',
  `grp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `accessrules` text CHARACTER SET latin1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parent` (`parent`,`name`),
  KEY `parent_2` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_document_insert` AFTER INSERT ON `documents` FOR EACH ROW call insert_docid_path(new.id) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_document_update` AFTER UPDATE ON `documents` FOR EACH ROW begin
    if (new.parent != old.parent) or (new.name != old.name) then
       call update_move(new.id);
    end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_document_delete` AFTER DELETE ON `documents` FOR EACH ROW begin
        declare p text default NULL;
        select path from docid_path where docid=old.id into p;  
        insert into documents_backup (id, parent, name, type, owner,grp, accessrules, 
                                      path,date_deleted, delete_user) values 
                                   (old.id, old.parent, old.name, old.type, old.owner, 
                                    old.grp, old.accessrules, p, now(), ifnull(@user, 1));
	delete d from docid_path d where d.docid = old.id;
	call clean_internal_proxies(old.id);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_backup` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(8) unsigned NOT NULL DEFAULT '0',
  `name` char(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `type` int(8) unsigned NOT NULL,
  `owner` smallint(5) unsigned NOT NULL DEFAULT '0',
  `grp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `accessrules` text CHARACTER SET latin1,
  `path` varchar(2048) CHARACTER SET latin1 NOT NULL,
  `date_deleted` datetime NOT NULL,
  `delete_user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `path` (`path`(767)),
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `editpages` (
  `doctypeid` int(8) unsigned NOT NULL,
  `page` char(5) NOT NULL,
  `title` varchar(127) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `fieldlist` text NOT NULL,
  PRIMARY KEY (`doctypeid`,`page`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldspecs` (
  `doctypeid` int(8) unsigned NOT NULL,
  `name` varchar(127) NOT NULL DEFAULT '',
  `type` int(8) unsigned NOT NULL,
  `repeatable` tinyint(1) unsigned NOT NULL,
  `optional` tinyint(1) unsigned NOT NULL,
  `searchable` tinyint(1) unsigned NOT NULL,
  `sortable` tinyint(1) unsigned NOT NULL,
  `publish` tinyint(1) unsigned NOT NULL,
  `threshold` tinyint(1) unsigned NOT NULL DEFAULT '128',
  `default_value` text,
  `extra` text,
  PRIMARY KEY (`doctypeid`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldtypes` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL DEFAULT '',
  `edit` varchar(127) NOT NULL DEFAULT 'line',
  `edit_args` text NOT NULL,
  `validate` varchar(127) NOT NULL DEFAULT 'none',
  `validate_args` text NOT NULL,
  `search` varchar(127) NOT NULL DEFAULT 'none',
  `search_args` text NOT NULL,
  `bin` tinyint(1) NOT NULL DEFAULT '0',
  `value_field` enum('text','int','double','date') NOT NULL DEFAULT 'text',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_submit_log` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(8) unsigned NOT NULL,
  `submit_time` datetime NOT NULL,
  `submitter_ip` varchar(255) DEFAULT '',
  `encrypted_email` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formdata` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(10) unsigned NOT NULL DEFAULT '0',
  `entry` longtext,
  PRIMARY KEY (`id`),
  KEY `docid` (`docid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formdata_backup` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(10) unsigned NOT NULL DEFAULT '0',
  `entry` longtext,
  PRIMARY KEY (`id`),
  KEY `docid` (`docid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formdata_entry` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(10) unsigned NOT NULL,
  `entry_nr` int(10) unsigned NOT NULL,
  `time` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  UNIQUE KEY `docid` (`docid`,`entry_nr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formdata_entry_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_danish_ci NOT NULL,
  `value` mediumtext COLLATE utf8_danish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`entry_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freetextindex` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(255) NOT NULL DEFAULT '',
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `weight` double unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `words` (`word`,`docid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freetextindex_versions` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`docid`,`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(31) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_groups_delete` AFTER DELETE ON `groups` FOR EACH ROW begin
	update documents set grp=1 where grp=old.id;
	delete g from grp_user g where g.grp=old.id; 
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grp_user` (
  `grp` smallint(5) unsigned NOT NULL DEFAULT '0',
  `user` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`grp`,`user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internal_proxy_documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(10) unsigned DEFAULT NULL,
  `version` datetime DEFAULT NULL,
  `dependent_on` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `docid_2` (`docid`,`version`),
  KEY `docid` (`docid`,`version`),
  KEY `dependent_on` (`dependent_on`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internal_proxy_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `relation_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_2` (`name`,`relation_id`),
  KEY `name` (`name`),
  KEY `relation_id` (`relation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kalendertilmeldinger` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `navn` varchar(255) NOT NULL DEFAULT '',
  `stillingsbetegnelse` varchar(255) NOT NULL DEFAULT '',
  `arbejdsplads` varchar(255) NOT NULL DEFAULT '',
  `adresse` varchar(255) NOT NULL DEFAULT '',
  `postnr` int(5) unsigned DEFAULT NULL,
  `adresse_by` varchar(255) NOT NULL DEFAULT '',
  `land` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `telefonnummer` varchar(255) NOT NULL DEFAULT '',
  `mobilnummer` varchar(255) NOT NULL DEFAULT '',
  `faktura_navn` varchar(255) NOT NULL DEFAULT '',
  `faktura_adresse` varchar(255) NOT NULL DEFAULT '',
  `faktura_postnr` int(5) unsigned DEFAULT NULL,
  `faktura_adresse_by` varchar(255) NOT NULL DEFAULT '',
  `faktura_land` varchar(255) NOT NULL DEFAULT '',
  `paper_praesentation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pp_title` varchar(255) NOT NULL DEFAULT '',
  `oensker_hotel` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hotel_bemaerkninger` text,
  `bemaerkninger` text,
  `tilmeldingstidspunkt` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(63) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyword` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_secrets` (
  `login` varchar(31) NOT NULL,
  `secret` varchar(32) NOT NULL,
  `time` int(10) unsigned NOT NULL,
  KEY `login` (`login`,`secret`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_sessions` (
  `login` varchar(31) NOT NULL,
  `session_id` char(32) NOT NULL,
  `last_access` int(12) NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `newsboxid` int(8) unsigned NOT NULL DEFAULT '0',
  `seq` int(8) unsigned NOT NULL DEFAULT '0',
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `start` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `end` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`newsboxid`,`seq`),
  KEY `start` (`start`),
  KEY `end` (`end`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsboxes` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `type` enum('chronological','reverse_chronological','manual_placement') NOT NULL DEFAULT 'chronological',
  PRIMARY KEY (`docid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_send_time` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `start_idx` (`start`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_sent_docs` (
  `newsletter_id` int(10) unsigned NOT NULL DEFAULT '0',
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`newsletter_id`,`docid`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL DEFAULT '',
  `subject` varchar(127) NOT NULL DEFAULT '',
  `sent` datetime DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `user` smallint(5) unsigned NOT NULL DEFAULT '0',
  `command` varchar(127) NOT NULL DEFAULT '',
  `args` text,
  `status` varchar(63) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`),
  KEY `date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shortcuts` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(8) unsigned NOT NULL,
  `docid` int(8) unsigned NOT NULL,
  `name` varchar(512) NOT NULL,
  `order_number` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submitted_complaints` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(8) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `address` text COLLATE utf8_danish_ci,
  `encrypted_mail` longblob,
  `status` enum('sent','failed','retried','retried_failed') COLLATE utf8_danish_ci DEFAULT NULL,
  `mail_id` varchar(255) COLLATE utf8_danish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `submitted_complaints_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `complaint_sessions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber_categories` (
  `subscriber` int(10) unsigned NOT NULL DEFAULT '0',
  `category` varchar(127) NOT NULL DEFAULT '',
  PRIMARY KEY (`subscriber`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL DEFAULT '',
  `company` varchar(127) NOT NULL DEFAULT '',
  `passwd` varchar(63) NOT NULL DEFAULT '',
  `email` varchar(63) NOT NULL DEFAULT '',
  `suspended` tinyint(3) NOT NULL DEFAULT '0',
  `cookie` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_areas` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `config_name` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `subject` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `root_docid` int(8) unsigned NOT NULL DEFAULT '0',
  `banner_text` text COLLATE utf8_danish_ci,
  `header_background_color` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  `header_image` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_area_sections` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `area_id` int(8) unsigned NOT NULL,
  `area_title` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `sub_area_sec_area_ref` (`area_id`),
  CONSTRAINT `sub_area_sec_area_ref` FOREIGN KEY (`area_id`) REFERENCES `subscription_areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_area_paths` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(8) unsigned NOT NULL,
  `path` varchar(2048) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `sub_area_url_sec_ref` (`section_id`),
  CONSTRAINT `sub_area_url_sec_ref` FOREIGN KEY (`section_id`) REFERENCES `subscription_area_sections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriptions` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `subscriber` int(10) unsigned NOT NULL DEFAULT '0',
  `last_update` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`docid`,`subscriber`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `synonyms` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `synonyms` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblprojekter` (
  `Id` int(11) DEFAULT '0',
  `titel` text,
  `beskrivelse` text,
  `noegleord` varchar(255) DEFAULT NULL,
  `projektstart` varchar(50) DEFAULT NULL,
  `projektslut` varchar(50) DEFAULT NULL,
  `medarbejdende_forskere` text,
  `brugergrupper` varchar(250) DEFAULT NULL,
  `andre_brugergruper` varchar(255) DEFAULT NULL,
  `formidling` text,
  `email` varchar(255) DEFAULT NULL,
  `titel_projektansvar` varchar(50) DEFAULT NULL,
  `projektansvar` varchar(255) DEFAULT NULL,
  `institution` varchar(255) DEFAULT NULL,
  `projektnr` int(11) DEFAULT '0',
  KEY `Id` (`Id`),
  KEY `projektnr` (`projektnr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_surveillance_docs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `docid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`docid`),
  KEY `docid` (`docid`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_surveillance_sites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `docid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_2` (`user_id`,`docid`),
  KEY `user_id` (`user_id`),
  KEY `docid` (`docid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(31) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `passwd` varchar(63) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name` varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `email` varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `notes` text CHARACTER SET latin1 NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `can_manage_users` tinyint(1) NOT NULL DEFAULT '0',
  `can_manage_groups` tinyint(1) NOT NULL DEFAULT '0',
  `surveillance` text CHARACTER SET latin1,
  `is_admin` int(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_user_insert` AFTER INSERT ON `users` FOR EACH ROW begin 
      call delete_user_surveillance(new.id);
      if new.surveillance is not null and new.surveillance != '' then
          call create_user_surveillance(new.id, new.surveillance);
      end if;	  
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_user_update` AFTER UPDATE ON `users` FOR EACH ROW begin 
      call delete_user_surveillance(new.id);
      if new.surveillance is not null and new.surveillance != '' then
            call create_user_surveillance(new.id, new.surveillance);
      end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_user_delete` AFTER DELETE ON `users` FOR EACH ROW begin
	call delete_user_surveillance(old.id);
	update documents set owner = 1 where owner = old.id;
	update versions set user = 1 where user = old.id;
	
	delete from grp_user where user=old.id;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `validation_errors` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(8) unsigned DEFAULT NULL,
  `submit_attempt_number` int(8) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `failed_field` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  CONSTRAINT `validation_errors_ibfk_1` FOREIGN KEY (`session_id`) REFERENCES `complaint_sessions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `type` int(8) unsigned NOT NULL DEFAULT '0',
  `public` tinyint(8) NOT NULL DEFAULT '0',
  `valid` tinyint(8) NOT NULL DEFAULT '0',
  `lang` char(2) NOT NULL DEFAULT 'da',
  `user` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`docid`,`version`),
  KEY `type` (`type`),
  KEY `versions_public_idx` (`public`,`type`,`docid`,`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_version_delete` AFTER DELETE ON `versions` FOR EACH ROW begin
      insert into versions_backup (docid, version, type, public, 
                                   valid, lang, user) values 
                                  (old.docid, old.version, old.type, old.public, 
                                   old.valid, old.lang, old.user);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions_backup` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `type` int(8) unsigned NOT NULL DEFAULT '0',
  `public` tinyint(8) NOT NULL DEFAULT '0',
  `valid` tinyint(8) NOT NULL DEFAULT '0',
  `lang` char(2) NOT NULL DEFAULT 'da',
  `user` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`docid`,`version`),
  KEY `type` (`type`),
  KEY `public` (`public`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vfields` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `name` varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `text_value` longtext COLLATE utf8_danish_ci,
  `int_value` int(8) DEFAULT NULL,
  `double_value` double DEFAULT NULL,
  `date_value` datetime DEFAULT NULL,
  KEY `vfields_docid_version_name_int_value_idx` (`docid`,`version`,`name`,`int_value`),
  KEY `vfields_docid_version_name_double_value_idx` (`docid`,`version`,`name`,`double_value`),
  KEY `vfields_docid_version_name_date_value_idx` (`docid`,`version`,`name`,`date_value`),
  KEY `vfields_docid_version_name_text_value_idx` (`docid`,`version`,`name`,`text_value`(16)),
  KEY `vfields_text_idx` (`name`,`text_value`(255)),
  KEY `vfields_date_idx` (`name`,`date_value`),
  KEY `vfields_int_idx` (`name`,`int_value`),
  KEY `vfields_double_idx` (`name`,`double_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER `post_vfield_delete` AFTER DELETE ON `vfields` FOR EACH ROW begin 
      insert into vfields_backup (docid, version, name, text_value, int_value, 
                                  date_value, double_value) values
                                 (old.docid, old.version, old.name, old.text_value, old.int_value, 
                                  old.date_value, old.double_value);
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vfields_backup` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `version` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `name` varchar(127) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `text_value` longtext CHARACTER SET latin1,
  `int_value` int(8) DEFAULT NULL,
  `double_value` double DEFAULT NULL,
  `date_value` datetime DEFAULT NULL,
  KEY `vfields_docid_version_name_int_value_idx` (`docid`,`version`,`name`,`int_value`),
  KEY `vfields_docid_version_name_double_value_idx` (`docid`,`version`,`name`,`double_value`),
  KEY `vfields_docid_version_name_date_value_idx` (`docid`,`version`,`name`,`date_value`),
  KEY `vfields_docid_version_name_text_value_idx` (`docid`,`version`,`name`,`text_value`(16))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voters` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `cookie` char(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`docid`,`cookie`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `votes` (
  `docid` int(8) unsigned NOT NULL DEFAULT '0',
  `answer` char(32) NOT NULL DEFAULT '',
  `total` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`docid`,`answer`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE FUNCTION `intervals_overlap`(siv1 datetime, eiv1 datetime, siv2 datetime, eiv2 datetime) RETURNS int(11)
    DETERMINISTIC
begin
	if eiv1 is null or eiv1 = '0000-00-00' then
	   set eiv1 = siv1;
	end if;
	if eiv2 is null or eiv2 = '0000-00-00' then
	   set eiv2 = siv2;
	end if;
	return not ((eiv2 < siv1 and eiv2 < eiv1) or (eiv1 < siv2 and eiv1 < eiv2));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `add_vfield`(docid integer unsigned, version datetime, name varchar(1024), text_value varchar(16384), int_value integer, double_value double, date_value datetime)
begin
	insert into vfields 
	       (docid, version, name, text_value, int_value, double_value, date_value) values
	       (docid, version, ucase(name), text_value, int_value, double_value, date_value);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `backup_recursive_subdocs`()
begin 
	declare id integer unsigned default 0;
	create temporary table if not exists 
	       recursive_subdocs_table (id integer unsigned primary key auto_increment) engine=heap;
	create temporary table if not exists 
	       recursive_subdocs_backup_list (id integer unsigned primary key auto_increment) engine=heap;
	create temporary table if not exists
	       recursive_subdocs_backup (backup_id integer unsigned, recursive_id integer unsigned) engine=heap;
	
	insert into recursive_subdocs_backup_list values ();
	set id = last_insert_id();
	
	insert into recursive_subdocs_backup (backup_id, recursive_id) 
	       select id,recursive_subdocs_table.id from recursive_subdocs_table;
	
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `check_internal_proxy_status`(out good integer unsigned)
begin
	declare times integer unsigned default 10;
	create temporary table if not exists internal_proxy_status_table (docid integer unsigned, version datetime, primary key (docid, version) ) engine = heap;
	create temporary table if not exists internal_proxy_status_table2 (docid integer unsigned, version datetime, primary key (docid, version) ) engine = heap;
	delete ip from internal_proxy_status_table ip;
	delete ip2 from internal_proxy_status_table ip2;
	insert ignore into internal_proxy_status_table (docid) 
	       select docid from internal_proxy_documents i2;
	
	while times > 0 and not good do
	      set times = times - 1;
	      delete ip2 from internal_proxy_status_table2 ip2;
	      insert ignore into internal_proxy_status_table2 
	      	     select i.docid, i.version from internal_proxy_documents i 
		     join internal_proxy_status_table ip on
		     (i.dependent_on = ip.docid);
	      delete ip from internal_proxy_status_table ip;
	      insert ignore into internal_proxy_status_table 
	      	     select * from internal_proxy_status_table2 ip2;
              call cleanup_internal_proxy_status_table();
	      select not count(*) into good from internal_proxy_status_table ip;
	end while;
	delete ip from internal_proxy_status_table ip;
	delete ip2 from internal_proxy_status_table ip2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `cleanup_internal_proxy_status_table`()
begin
	declare done integer unsigned default 0;
	declare v2 datetime default null;
	declare d integer unsigned default null;
	declare v datetime default null;
	declare c cursor for (select d.docid, d.version from internal_proxy_status_table ip);
	declare continue handler for not found set done=1;
	
	create temporary table if not exists to_be_deleted (docid integer unsigned, version datetime, primary key( docid, version));
	delete t from to_be_deleted t;
	open c;
	fetch c into d,v;
	while not done do
	      call public_or_latest_version(d, v2);
	      if v2 != v then
	      	 insert into to_be_deleted values (d,v);
	      end if;     
	      fetch c into d,v;
       end while;
       
       close c;
       delete ip from internal_proxy_status_table ip natural join to_be_deleted;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `clean_internal_proxies`(docid integer unsigned)
begin
	declare a integer unsigned default null;
	declare done integer unsigned default 0;
	declare c cursor for (select i.id from internal_proxy_documents i where i.docid = docid);
	declare c2 cursor for (select i.id from internal_proxy_documents i where i.dependent_on = docid);
        declare continue handler for not found set done=1;
	
	open c;
	fetch c into a;
	
	while not done do
	   update internal_proxy_documents i join internal_proxy_documents i2 on 
	   	      (i.docid = i2.dependent_on) set i2.dependent_on = i.dependent_on 
		      where i.id = a;
	   delete i from internal_proxy_fields i  where i.relation_id = a;
	   delete i from internal_proxy_documents i where i.id = a;
	   fetch c into a;
	end while;
	close c;
	set done = 0;
	open c2;
	fetch c2 into a;
	
	while not done do
	   delete from internal_proxy_fields  where i.relation_id = a;
	   delete i from internal_proxy_documents i where i.id = a;
	   fetch c2 into a;
	end while;
	close c2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `copy_batch`(out finally integer)
begin 
      declare done integer default 0;
      declare cur_id integer unsigned;
      declare cur_parent integer unsigned;
      declare new_docid integer unsigned;
      declare curs cursor for 
              (select recursive_subdocs_table.id, t.new_docid from 
                      documents d join recursive_subdocs_table on (recursive_subdocs_table.id = d.id)
                      join tree_copier_helper t on (d.parent = t.old_docid) 
                      where not exists (select * from batch_copier_helper b where b.id = recursive_subdocs_table.id));
     declare continue handler for not found set done=1;
     create temporary table if not exists batch_copier_helper (id integer unsigned primary key) engine=heap;
     delete b from batch_copier_helper b;
     insert into batch_copier_helper (id) select old_docid from tree_copier_helper t;
     select not count(*) into finally from 
             recursive_subdocs_table left join tree_copier_helper t on (t.old_docid = recursive_subdocs_table.id) 
             where t.old_docid is null;
     if not finally then
         open curs;
         fetch curs into cur_id, cur_parent;
         while not done do
               call copy_id(cur_id, cur_parent, NULL, new_docid);
               insert into tree_copier_helper (old_docid, new_docid) values (cur_id, new_docid);
               fetch curs into cur_id, cur_parent;
         end while;
         close curs;
       end if;  
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `copy_docparams`(fromid integer unsigned, toid integer unsigned)
begin
	replace into docparms (docid, name, value, type) select toid, name, value, type from docparms where docid=fromid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `copy_id`(did integer unsigned, 
                         new_parent integer unsigned, 
                         new_name varchar(1024), 
                         out new_docid integer unsigned)
begin
        insert into documents (parent, name, type, owner, grp, accessrules) 
	        select coalesce(new_parent, d.parent), coalesce(new_name, d.name), d.type, d.owner, d.grp, d.accessrules 
        	from documents d where d.id=did;
	set new_docid = last_insert_id();
		     
        insert into versions (docid, version,type,public,valid,lang, user)
        select new_docid, version, type, public,valid,lang, user from versions where docid=did;
        
        insert into vfields (docid, version,name,text_value,int_value, double_value,date_value)
        select new_docid,version,name,text_value,int_value,double_value,date_value from vfields where docid=did; 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `copy_tree`(docid integer unsigned, new_parent integer unsigned, new_name varchar(1024))
begin 
      declare done integer default 0;
      declare new_did integer unsigned;
      
      create temporary table if not exists tree_copier_helper (old_docid integer unsigned, new_docid integer unsigned, index (old_docid), index (new_docid)) engine=heap;
      delete t from tree_copier_helper t;
      call backup_recursive_subdocs();
      call recursive_subdocs(docid);
      
      call copy_id(docid, new_parent, new_name, new_did);
      insert into tree_copier_helper (old_docid, new_docid) values (docid, new_did);
      
      while not done do call copy_batch(done); end while;
      call restore_recursive_subdocs();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `create_internal_proxy_docid_update_table`(docid integer unsigned, v datetime)
begin
	declare old_len integer unsigned default 0;
	declare new_len integer unsigned default 1;
	create temporary table if not exists internal_proxy_docid_update_table(id integer unsigned auto_increment primary key, 
	       		       					               docid integer unsigned, 
									       version datetime,
								               unique(docid, version)) engine=heap;
	create temporary table if not exists internal_proxy_docid_update_table2(id integer unsigned auto_increment primary key, 
	       		       					                docid integer unsigned, 
										version datetime,
								                unique (docid,version)) engine=heap;
	delete d from internal_proxy_docid_update_table d;
	delete d2 from internal_proxy_docid_update_table2 d2;
	
	insert into internal_proxy_docid_update_table (docid, version) values (docid, v);
	
	while old_len != new_len do
	      set old_len = new_len;
	      insert ignore into internal_proxy_docid_update_table2 select * from 
	      	     internal_proxy_docid_update_table d;
	      insert ignore into internal_proxy_docid_update_table (docid, version) 
	      	     select i.docid, i.version from 
	      	     internal_proxy_documents i join internal_proxy_docid_update_table2 d2 
		     on (i.dependent_on = d2.docid);
	      select count(*) into new_len from internal_proxy_docid_update_table d;
	end while;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `create_user_surveillance`(user_id integer unsigned, surveillance_data text)
begin
	declare len integer unsigned;
	declare pos integer unsigned default 0;
	declare cur_data text;
	declare did text;
	declare site_or_page text;
	declare sd text;
	create temporary table if not exists surv_temp (docid integer unsigned, site text);
	delete from surv_temp;
	set sd = concat(surveillance_data, ';');
	set len = length(sd) - length(replace(sd, ';', ''));
	while pos < len do
	      set cur_data = substring_index(substring_index(sd, ';', pos + 1), ';', -1);
	      set did = substring_index(cur_data, ':', -1);
	      set site_or_page = substring_index(cur_data, ':', 1);
	      set pos = pos + 1;
	      insert into surv_temp (docid, site) values (did, site_or_page);
	end while;
	
	replace into user_surveillance_sites (docid, user_id)
	       select surv_temp.docid, user_id from 
	       surv_temp where site = 'omrade';
	replace into user_surveillance_docs  (docid, user_id)
	       select surv_temp.docid, user_id from 
	       surv_temp where site = 'webside';
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_document`(did integer unsigned)
begin
	delete d from documents d where id=did;
	delete vf from vfields vf where docid=did;
	delete ver from versions ver where docid=did;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_tree`(docid integer unsigned)
begin
       call backup_recursive_subdocs();
       call recursive_subdocs(docid);
       call really_delete_tree();
       call restore_recursive_subdocs();
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `delete_user_surveillance`(user_id integer unsigned)
begin
	delete from user_surveillance_docs where user_surveillance_docs.user_id = user_id;
        delete from user_surveillance_sites where user_surveillance_sites.user_id = user_id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `do_search`(in_path varchar(1024), 
                           in_pattern varchar(1024), 
                           owner integer unsigned, 
                           grp integer unsigned, 
                           newer_than datetime, 
                           older_than datetime
                           )
begin
        declare pattern varchar(1024);
        declare path varchar(1026);
        set path = concat(in_path, "%");
        set pattern = concat('%', replace(in_pattern, '*', '%'), '%');
        select distinct(d.id) from
               documents d join docid_path dp on (d.id = dp.docid and dp.path like path)
               join versions v on (d.id = v.docid and (v.public = 1 or v.version = (select max(v2.version) from versions v2 where v2.docid  = d.id)))
               left join vfields vf1 on 
                    (v.docid = vf1.docid and vf1.version = v.version and 
                    vf1.name in ('content', 'title', 'short_title'))
        where    
                (pattern is null or vf1.text_value like pattern or d.name like pattern) and
                (owner is null or d.owner = owner) and
                (grp is null or d.grp = grp) and
                (older_than is null or v.version < older_than) and
                (newer_than is null or v.version > newer_than);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `find_doc_by_path`(path varchar(1024), out docid integer unsigned)
begin
     declare len integer unsigned;
     declare pos integer unsigned default 1;
     declare tid integer unsigned;
     declare cid integer unsigned default 1;
     declare pp varchar(255);
     if not path regexp "/$" then
        set path = concat(path, "/");
     end if;
     if trim(path) = '/' then
        set docid = 1;
     else         
        set len = length(path) - length(replace(path, '/', ''));
        scanner:while pos < len do 
           set pp = substring_index(substring_index(path, '/', pos + 1), '/', -1);
           set pos = pos + 1;
           if pp = '' then iterate scanner; end if;
           set tid = 0;
           
           select d.id into tid from documents d
                  where d.name = pp AND d.parent = cid;
           if tid = 0 then leave scanner; end if;
           set cid = tid;
     end while ;
     set docid = tid;
   end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `find_path_by_docid`(docid integer, out outpath varchar(1024))
begin 
      declare res text default '';
      declare docid2 integer unsigned;
      while docid != 1 do
            set docid2 = null;
            select concat(d.name,'/', res), d.parent into res, docid2 from documents d where d.id = docid;
            set docid = docid2;
      end while;
      
      select case when docid = 1 then concat('/', res) end path into outpath;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `insert_docid_path`(did integer unsigned)
begin
        declare path varchar(1024);
        call find_path_by_docid(did, path);
        delete docid_path from docid_path where docid=did;
        insert into docid_path (docid, path) values (did, path);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `insert_internal_proxy_entry`(docid integer unsigned, version datetime, dependent_on integer unsigned, fields varchar(16384))
begin
	declare cur varchar(128) default '';
	declare good integer default 0;
	declare len integer unsigned default 0;
	declare pos integer unsigned default 0;
	declare id integer unsigned;
	
	insert into internal_proxy_documents (docid, version, dependent_on) values (docid, version, dependent_on);
	set id = last_insert_id();
	
	call check_internal_proxy_status(good);
	
	if not good then
	   call ERROR_YOU_HAVE_CREATED_A_CYCLE();
	end if;
	delete i from internal_proxy_fields i where relation_id = id;
	set len = length(fields) - length(replace(fields, ',', ''));
	while pos < len + 1 do
	      set cur = substring_index(substring_index(fields, ',', pos + 1), ',', -1);
	      insert into internal_proxy_fields (relation_id, name) values (id, cur);
	      set pos = pos + 1;
	end while;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `internal_proxy_when_document_updated`(docid integer unsigned)
begin
        declare done integer default 0;
        declare a integer unsigned;
	declare v datetime;
        declare c cursor for (select d.docid, d.version from internal_proxy_documents d where d.dependent_on = docid);
        declare continue handler for not found set done=1;
        
        open c;
        fetch c into a,v;
        while not done do
              call update_internal_proxies(a, v);
              fetch c into a,v;
        end while;
        close c;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `is_internal_proxy_document`(docid integer unsigned)
begin
	select count(*) as is_ from internal_proxy_documents i where i.docid = docid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `move_document`(docid integer unsigned, new_parent integer unsigned, new_name varchar(1024))
begin
        update documents set parent=new_parent,name=coalesce(new_name,name)  where id=docid;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `new_internal_proxy_entry`(docid integer unsigned, version datetime, depends_on integer unsigned, fields varchar(16384))
begin
	call insert_internal_proxy_entry(docid, version, depends_on, fields);
	call update_internal_proxy_document(docid, version);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `public_or_latest_version`(did integer unsigned, out version datetime)
begin
        select v.version into version from versions v 
               where v.docid = did and 
                     (v.public = 1 or 
                     ((not exists 
                          (select * from versions v2 where 
                                  v2.docid = did and 
                                  v2.public = 1))
                      and (v.version in (select max(v3.version) 
                          from versions v3 where v3.docid = did)))) limit 1;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `publish_version`(docid integer unsigned, version datetime, lang varchar(100))
begin
        update versions v set public = 0 where (lang is null or (v.lang = lang));
        update versions v set public = 1 where (v.docid = docid and v.version = version);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `read_vfields`(docid integer unsigned, version datetime, names varchar(16384))
begin
	declare pos integer unsigned default 0;
	declare len integer unsigned default 0;
	
	create temporary table if not exists read_vfields_helper (name varchar(1024) primary key) engine = heap;
	delete r from read_vfields_helper r;
	set len = length(names) - length(replace(names, ",", ""));
	while pos < len or (pos = 0 and len = 0) do
	      insert into read_vfields_helper values (substring_index(substring_index(names, ",", pos + 1), ",", -1));
	end while;
	
	select vf.* from vfields vf join read_vfields_helper r on (vf.docid = docid and vf.version = version and vf.name = r.name);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `read_vfields_from_pol`(docid integer unsigned, names varchar(16384))
begin
	declare version datetime default null;
	
	call public_or_latest_version(docid, version);
	if version is null then
	   call ERROR_NO_PUBLIC_OR_LATEST_VERSION();
	end if;	
	
	call read_vfields(docid, version, names);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `really_delete_tree`()
begin 
	declare done integer default 0;
	declare a integer unsigned;
     	declare c cursor for (select * from recursive_subdocs_table);
        declare continue handler for not found set done=1;
		
	open c;
	fetch c into a;
	while not done do call delete_document(a); fetch c into a; end while;
	delete from recursive_subdocs_table;
	close c;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `recursive_subdocs`(docid integer unsigned)
begin
        declare old_len integer unsigned default 0;
        declare new_len integer unsigned default 1;
	
	create temporary table if not exists recursive_subdocs_table2 
	       (id integer unsigned primary key) engine=heap;
	
	delete from recursive_subdocs_table;
        insert into recursive_subdocs_table set id=docid;
	
        subdocs:while old_len != new_len do
                      set old_len = new_len;
		      delete r2 from recursive_subdocs_table2 r2;
		      insert ignore into recursive_subdocs_table2 (id)
		      	     select id from recursive_subdocs_table;
                      insert ignore into recursive_subdocs_table (id) select                 
                             d.id from recursive_subdocs_table2 r2 join 
                             documents d on (r2.id = d.parent); 
                       select count(*) into new_len from recursive_subdocs_table;
         end while;
	 drop temporary table recursive_subdocs_table2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `recursive_subdocs_trigger`(docid integer unsigned)
begin
        declare old_len integer unsigned default 0;
        declare new_len integer unsigned default 1;
	
	create temporary table if not exists recursive_subdocs_table2 
	       (id integer unsigned primary key) engine=heap;
	
	delete from recursive_subdocs_trigger_table;
        insert into recursive_subdocs_trigger_table set id=docid;
	
        subdocs:while old_len != new_len do
                      set old_len = new_len;
		      delete r2 from recursive_subdocs_table2 r2;
		      insert ignore into recursive_subdocs_table2 (id)
		      	     select id from recursive_subdocs_trigger_table;
                      insert ignore into recursive_subdocs_trigger_table (id) select
                             d.id from recursive_subdocs_table2 r2 join 
                             documents d on (r2.id = d.parent); 
                      select count(*) into new_len from recursive_subdocs_trigger_table;
         end while;
	 drop temporary table recursive_subdocs_table2;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `restore_recursive_subdocs`()
begin
	declare id integer unsigned;
	drop temporary table recursive_subdocs_table;
	create temporary table if not exists recursive_subdocs_table (id integer unsigned primary key auto_increment);
	select max(id) into id from recursive_subdocs_backup_list rl;
	insert into recursive_subdocs_table
	       select recursive_id from recursive_subdocs_backup rsb where rsb.backup_id = id;
	
	delete rl from recursive_subdocs_backup_list rl where rl.id = id;
	delete rsb from recursive_subdocs_backup rsb where rsb.backup_id = id;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `unpublish_document`(docid integer unsigned, lang varchar(100))
begin
        update versions v set public = 0 where v.docid = docid and (lang is null or (v.lang = lang));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_internal_proxies`(docid integer unsigned, version datetime)
begin 
      call create_internal_proxy_docid_update_table(docid, version);
      call update_internal_proxies_do();
      delete d from internal_proxy_docid_update_table d;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_internal_proxies_do`()
begin
	declare done integer default 0;
	declare a integer unsigned;
	declare v datetime;
	declare c cursor for (select d.docid, d.version from internal_proxy_docid_update_table d order by id asc);
        declare continue handler for not found set done=1;
	
	open c;
	fetch c into a, v;
	while not done do
	      call update_internal_proxy_document(a, v);
	      fetch c into a, v;
	end while;
	close c;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_internal_proxy_docids`(docids varchar(16384))
begin
      declare len integer unsigned;
      declare cur varchar(128) default '';
      declare pos integer unsigned default 0;
      
      start transaction;
      set len = length(docids) - length(replace(docids, ',', ''));
      while pos < len + 1 do
            set cur = substring_index(substring_index(docids, ',', pos + 1), ',', -1);
	    call internal_proxy_when_document_updated(convert(cur, unsigned));
	    set pos = pos + 1;
      end while;
      commit;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_internal_proxy_document`(docid integer unsigned, version datetime)
begin
	declare dep_docid integer unsigned default 0;
	declare dep_version datetime default null;
	
	if version is null then
	   call ERROR_NO_PUBLIC_OR_LATEST_VERSION();
	end if;
	select dependent_on into dep_docid from internal_proxy_documents i
	       where i.docid = docid and i.version = version;
	call public_or_latest_version(dep_docid, dep_version);
	       	
	if dep_version is null then
	   call ERROR_NO_PUBLIC_OR_LATEST_VERSION();
	end if;
	delete vf from vfields vf where
	       vf.docid = docid and vf.version = version and
	       vf.name not in (select ipf.name from internal_proxy_documents ipd join internal_proxy_fields ipf on 
	       	   	  (ipf.relation_id = ipd.id) where ipd.version = version and ipd.docid = docid);
	insert ignore into vfields (docid, version, name, text_value, double_value, date_value, int_value)
	       select docid, version, vf.name, vf.text_value, vf.double_value, vf.date_value, vf.int_value
	       from vfields vf where 
	       vf.docid = dep_docid and vf.version = dep_version and vf.name not in 
	       (select ipf.name from internal_proxy_documents ipd join internal_proxy_fields ipf on 
	       	   	  (ipf.relation_id = ipd.id) where ipd.docid = docid and ipd.version = version);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_move`(docid integer unsigned)
begin
	create temporary table recursive_subdocs_trigger_table (id integer unsigned primary key);
        call recursive_subdocs_trigger(docid);
 	call update_move_internal();
	drop temporary table recursive_subdocs_trigger_table;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE PROCEDURE `update_move_internal`()
begin
        declare path varchar(1024);
        declare a int unsigned default 0;
        declare done int default 0;
        declare curs cursor for (select * from recursive_subdocs_trigger_table);
        declare continue handler for not found set done=1;
        open curs;
        fetch curs into a;
        while not done do
              call find_path_by_docid(a, path);
	      delete docid_path from docid_path where docid=a;
              replace into docid_path (docid, path) values (a, path);
              fetch curs into a;
        end while;
        
        close curs;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
