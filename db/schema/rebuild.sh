#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

pushd "${DIR}" > /dev/null
for x in *.sql.d; do
    FILE=$(echo $x | sed -e 's/\.d$//')
    test -d "${x}" && cat ${x}/* > $FILE
done
popd > /dev/null
