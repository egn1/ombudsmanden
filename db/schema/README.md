# Database schema files

This folder contains scripts for creating the basic structures for databases.

The script `rebuild.sh` can be used to concatenate multiple scripts from a
`<name>.sql.d/` into the file `<name>.sql`.

A dump of a database with only table structures, routines and trigges can
be created using the `dump_structure.pl` script which generates a lean
dump and processes it, removing auto-increment counters and `DEFINER`
attributes.
