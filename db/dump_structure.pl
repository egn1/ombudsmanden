#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Obvius::Config;
use Carp;
use DateTime;

my $config = Obvius::Config->new('ombud');

# Only tables + procedures + triggers, no superfluous comments
my $flags = '--no-data --routines --triggers --compact';

my $dirname = join('/', $config->param('sitebase'), 'db', 'schema', 'ombudsmanden.sql.d');
$dirname =~ s!//!/!gm; # Remove duplicate slashes
my $date = DateTime->now()->ymd;
my $filename = "$dirname/001-base-schema-$date.sql";
my $tmp_filename = "$filename.tmp";

my @mysqldump_options;

my $dsn = $config->param('dsn');
$dsn =~ s{^DBI:mysql:}{};
my %dsn = map { split(/=/, $_, 2) } split(/\s*;\s*/, $dsn);

if(my $host = $dsn{host}) {
    push(@mysqldump_options, "-h ${host}");
}
if(my $port = $dsn{port}) {
    push(@mysqldump_options, "-P ${port}");
}
push(@mysqldump_options, "-u " . $config->param('normal_db_login'));
push(@mysqldump_options, "--password=" . $config->param('normal_db_passwd'));
push(@mysqldump_options, "--no-tablespaces");

# Perform dump
system(join(" ",
    "mysqldump",
    @mysqldump_options,
    $flags,
    $dsn{database},
    "> $tmp_filename"
));

# Remove auto-increment and definer
open(FH, "<", $tmp_filename) or croak "Could not open $tmp_filename for reading";
open(FILE, ">", $filename) or croak "Could not open $filename for writing";
while(my $line = <FH>) {
    my $new_line = process_line($line);
    print FILE $new_line;
}
close(FH);
close(FILE);

unlink($tmp_filename);

sub process_line {
    my ($line) = @_;

    $line =~ s{/\*!50017 DEFINER=.*?\*/}{};
    $line =~ s{CREATE DEFINER=[^ ]* }{CREATE };
    $line =~ s{/\*!50013 DEFINER=.*?\*/}{};
    $line =~ s{ AUTO_INCREMENT=[0-9]*}{};

    return $line;
}
