-- MySQL dump 8.21
--
-- Host: localhost    Database: euo
---------------------------------------------------------
-- Server version	3.23.49

--
-- Table structure for table 'freetextindex'
--

DROP TABLE IF EXISTS freetextindex;
CREATE TABLE freetextindex (
  id int(8) unsigned NOT NULL auto_increment,
  word varchar(255) NOT NULL default '',
  docid int(8) unsigned NOT NULL default '0',
  weight double unsigned NOT NULL default '0',
  PRIMARY KEY  (id),
  KEY words (word,docid)
) TYPE=MyISAM;

--
-- Dumping data for table 'freetextindex'
--
-- WHERE:  1=0



-- MySQL dump 8.21
--
-- Host: localhost    Database: akf
---------------------------------------------------------
-- Server version	3.23.49

--
-- Table structure for table 'versions'
--

DROP TABLE IF EXISTS freetextindex_versions;
CREATE TABLE freetextindex_versions (
  docid int(8) unsigned NOT NULL default '0',
  version datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (docid,version)
) TYPE=MyISAM;

--
-- Dumping data for table 'versions'
--
-- WHERE:  1=0



