#!/bin/bash

PERL5LIB=/var/www/obvius/perl:/var/www/db.advokatsamfundet.dk/perl \
    perl -MObvius::SiteTools -e \
    'Obvius::SiteTools::update_doctypes("ombud")'
