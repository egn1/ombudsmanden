#!/usr/bin/perl

use strict;
use warnings;

# Script for managing the Ombudsmanden development environment.
# Usage:
#
#   perl manage.pl <command> [<options>]
#
# To see a list of available commands and their options # run the script
# without any options or with the "help" command.
#
# Example:
#
#   perl manage.pl help
#

use Getopt::Long;
use File::Basename;

my $dirname = dirname(__FILE__);
my %commands;

# Main script

sub main {
    my @args = @ARGV;

    my $nr_args = scalar(@args);
    my $command = shift @args;

    if($nr_args == 0) {
        print _usage(), "\n";
        exit 1;
    }

    if($nr_args == 1 && $command eq 'help') {
        print _usage(), "\n";
        exit 0;
    }

    my $cmd_data = $commands{$command};
    if(!$cmd_data)  {
        print "No such command '$command'\n";
        exit 1;
    }

    # Change pwd to the directory with the script
    chdir($dirname);
    $cmd_data->{method}->(@args);
}


#----------------------------------------------------------------------------#
# Commands                                                                   #
#----------------------------------------------------------------------------#

#  Commands execute their specified method. Before execution the directory
#  is change to the directory containing the manange.pl script, so relative
#  paths can be used.

sub dbshell_command {
    system('. dev-environment/db/db.env && docker exec -it ombud-db mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_DATABASE');
}
_register_command(
    'dbshell',
    'Runs an interactive database shell inside db container',
    \&dbshell_command,
);

sub dbshell_matomo_command {
    system('. dev-environment/matomo-db/db.env && docker exec -it ombud-matomo-db mysql -u $MARIADB_USER --password=$MARIADB_PASSWORD $MARIADB_DATABASE');
}
_register_command(
    'dbshell_matomo',
    'Runs an interactive database shell inside matomo-db container',
    \&dbshell_matomo_command,
);

#----------------------------------------------------------------------------#
# Internal methods                                                           #
#----------------------------------------------------------------------------#

# Registers a new command. Arguments are:
#
#  name          - The name of the command
#  description   - Short description used in the list of available commands
#  code_ref      - The code that peforms the command
#  help_code_ref - (Optional) method that prints out the help for the command
#
sub _register_command {
    my ($name, $description, $code_ref, $help_code_ref) = @_;

    # Default help code is just to call the main method with
    # a single "help" argument.
    $help_code_ref ||= sub { print $description, "\n" };

    # If help is passed in as a string convert to a method that outputs that
    # string.
    if(ref($help_code_ref) ne "CODE") {
        my $string = $help_code_ref;
        $help_code_ref = sub { print $string };
    }

    $commands{$name} = {
        name => $name,
        description => $description,
        method => $code_ref,
        help_method => $help_code_ref
    };
}

# Prints out usage information
sub _usage {
    return join "\n",
    'Usage:',
    '',
    '  perl manage.pl <command> <arguments>',
    '',
    '  Available commands:',
    '',
    (map {
        '    ' .
        $commands{$_}->{name} .
        "\n\n      " .
        $commands{$_}->{description} .
        "\n"
    } sort keys %commands),
    '',
    '  For general help run `perl manage.pl help`. For help with a specific',
    '  command run `perl manage.pl help <commandname>`.',
    '';
}

# Executing the script
main();
