/* 
   - Copyright (C) 2021 Magenta ApS, http://magenta.dk.
   - Contact: info@magenta.dk.
   -
   - This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

// Global instance binding
var cookiePopup = null

// Initiator function
function cookiePopupConstructor(cp_element, cp_conf, cp_data) {
    var cp = {

        // Configuration values pulled from conf parameter
        conf: cp_conf,

        // Known cookie information pulled from global conf object
        cookiedata: cp_data,

        // Configure language from HTML document 'lang' attribute
        lang: document.getElementsByTagName('html')[0].lang,

        prefs: null,

        events: null,

        single_select_mode: null,

        /*
         * Methods
         */

        // Method for toggling popup visibility
        toggle: function(toggle) {
            if (toggle === 'open') {
                cp.popup_el.style.display = 'block'
                window.location.hash = '#cp-title'
            } else if (toggle === 'close') {
                cp.popup_el.style.display = 'none'
            } else {
                if (cp.popup_el.style.display === 'none') {
                    cp.popup_el.style.display = 'block'
                    window.location.hash = '#cp-title'
                } else {
                    cp.popup_el.style.display = 'none'
                }
            }
        },

        // Method for translating template strings or data objects
        translate: function(translatable) {
            if (translatable === undefined) {
                return ''
            }
            if (typeof translatable === 'object') {
                if (translatable[cp.lang]) {
                    return translatable[cp.lang]
                } else {
                    return translatable[0]
                }
            }
            if (typeof translatable === 'string') {
                var t = cp.conf.translations[translatable]
                if (t) {
                    if (t[cp.lang]) {
                        return t[cp.lang]
                    } else {
                        return t
                    }
                } else {
                    return translatable
                }
            }
        },

        eventFactory: function(event_name) {
            // Create event
            var obj = {}
            obj[event_name] = document.createEvent('Event')
            // Define event name
            obj[event_name].initEvent(event_name, true, true)
            return obj[event_name]
        },

        createEvents: function(categories) {
            var events = {}
            // Create events for accepting/rejecting individual categories
            for (var c in categories) {
                var cat = categories[c].category,
                    acceptstr = cat + 'CookiesAccepted',
                    declinestr = cat + 'CookiesDeclined'
                events[acceptstr] = cp.eventFactory(acceptstr)
                events[declinestr] = cp.eventFactory(declinestr)
            }
            return events
        },

        // Method to set user preferences cookie
        // NOTE that cookie data is limited to 4Kb total for all cookies
        setUserPrefs: function(prefdata) {
            var cookie_str = cp.conf.storageId + '=' + JSON.stringify(prefdata) + ';'
            if (cp.conf.storageDomain) {
                cookie_str += 'domain=' + cp.conf.storageDomain + ';'
            }
            cookie_str += 'SameSite=' + cp.conf.sameSitePolicy + ";Path=/;" + cp.getExpiryString()
            document.cookie = cookie_str
        },

        getExpiryString: function() {
            var days = cp.conf.daysUntilExpiry
            if (!days) return
            var date = new Date()
            date.setTime(date.getTime()+(days*24*60*60*1000))
            return 'expires=' + date.toGMTString()
        },

        getUserPrefs: function(conf, cookiedata) {
            // Check cookie for already saved user preferences
            var regex = new RegExp(conf.storageId)
            var cookies = document.cookie.split(';')
            var cookie = cookies.filter(function(c) {
                return regex.test(c)
            })
            if (cookie.length === 1) {
                return JSON.parse(cookie[0].replace(conf.storageId + '=', ''))
            } else {
                var data = {}
                for (var d in cookiedata) {
                    data[cookiedata[d].category] = null
                }
                return data
            }
        },

        buildTRow: function(data) {
            var tr = document.createElement('tr')
            var tds = ''
            for (var d in data) {
                tds += '<td>' + cp.translate(data[d]) + '</td>'
            }
            tr.innerHTML = tds
            return tr
        },

        buildTBody: function(data) {
            var tbod = document.createElement('tbody')
            for (var i in data.items) {
                tbod.appendChild(cp.buildTRow(data.items[i]))
            }
            return tbod
        },

        buildTHead: function(data) {
            var keys = Object.keys(data)
            var thead = document.createElement('thead')
            for (var k in keys) {
                var th = document.createElement('th')
                th.innerText = cp.translate(keys[k])
                thead.appendChild(th)
            }
            return thead
        },

        buildCategoryTable: function(data) {
            var div = document.createElement('div')
            var p = document.createElement('p')
            var table = document.createElement('table')
            var caption = document.createElement('caption')
            table.classList.add('cp-table')
            caption.innerText = cp.translate(data.category_title) + ' cookies'
            p.classList.add('cp-category-description')
            p.innerText = cp.translate(data.category_description)
            div.id = 'cp-table-' + data.category
            div.className = 'cp-category-body'
            div.appendChild(p)
            table.appendChild(caption)
            table.appendChild(cp.buildTHead(data.items[0]))
            table.appendChild(cp.buildTBody(data))
            div.appendChild(table)
            return div
        },

        buildCategoryHeader: function(data) {
            var div = document.createElement('div')
            var input = ''
            if (!cp.single_select_mode) {
                input += '<input type="checkbox" '
                if (cp.prefs[data.category]) {
                    input += 'checked '
                }
                input += 'class="cp-checkbox" id="check-' + data.category + '" data-category="' + data.category + '">'
                + '<label for="check-' + data.category + '" aria-label="' + cp.translate('Accept') + ' ' + cp.translate(data.category_title) + ' ' + cp.translate('cookies') + '" class="cp-check-label">'
                    + cp.translate('Accept') + ' ' + cp.translate(data.category_title) + ' ' + cp.translate('cookies')
                + '</label>'
            }
            input += '<button aria-expanded="false" aria-controls="cp-table-' + data.category + '" class="cp-select open">'
                     + cp.translate(data.category_title)
                   + '</button>'
            div.innerHTML = input
            div.classList.add('cp-category-header')
            div.querySelector('button').addEventListener('click', function(ev) {
                cp.categoryToggleHandler(this, data)
            })
            return div
        },

        findCookieCategory: function(cat_key) {
            return cp.cookiedata.filter(function(d) {
                return d.category === cat_key
            })
        },

        updateAcceptedStatus: function(prefs) {
            var status_el = cp_el.querySelector('.cp-accept-status'),
                status_accepted = cp.cookiedata.filter(function(cdata) {
                    return prefs[cdata.category] 
                }).map(function(s) {
                    return cp.translate(s.category_title)
                }),
                status_declined = cp.cookiedata.filter(function(cdata) {
                    return prefs[cdata.category] === false
                }).map(function(s) {
                    return cp.translate(s.category_title)
                })
            status_el.innerHTML = ''
            if (status_accepted.length > 0) {
                for (var s in status_accepted) {
                    if (s == 0) {
                        status_el.innerHTML = cp.translate("You've accepted cookies for") + ': ' + status_accepted[s]
                    } else {
                        status_el.innerHTML += ', ' + status_accepted[s]
                    }
                }
                if (status_declined.length > 0) {
                    status_el.innerHTML += '<br>'
                }
                status_el.style.display = 'block'
            }
            if (status_declined.length > 0) {
                for (var s in status_declined) {
                    if (s == 0) {
                        status_el.innerHTML += cp.translate("You've declined use of cookies for") + ': ' + status_declined[s]
                    } else {
                        status_el.innerHTML += ', ' + status_declined[s]
                    }
                }
                status_el.style.display = 'block'
            }
        },

        updateCategoryHeader: function(prefs) {
            var checkboxes = cp_el.querySelectorAll('.cp-checkbox')
            for (let c = 0; c < checkboxes.length; c++) {
                checkboxes[c].checked = prefs[checkboxes[c].dataset.category]
            }
        },

        buildListitem: function(data) {
            var li = document.createElement('li')
            li.classList.add('cp-list-item')
            li.classList.add('cp-category-' + data.category)
            li.appendChild(cp.buildCategoryHeader(data))
            li.appendChild(cp.buildCategoryTable(data))
            return li
        },

        eventDispatcher: function(accept_decline, category) {
            if (category) {
                document.dispatchEvent(cp.events[ category + 'Cookies' + accept_decline ])
            } else {
                for (let c in cp.cookiedata) {
                    document.dispatchEvent(cp.events[ cp.cookiedata[c].category + 'Cookies' + accept_decline ])
                }
            }
        },

        accept: function(pref_key) {
            cp.prefs[pref_key] = true
            cp.eventDispatcher('Accepted', pref_key)
        },

        decline: function(pref_key) {
            cp.prefs[pref_key] = false
            cp.eventDispatcher('Declined', pref_key)
        },

        acceptHandler: function(mode) {
            cp.toggle('close')
            if (mode === 'all') {
                for (let p in cp.prefs) {
                    cp.accept(p)
                }
            } else if (mode === 'none') {
                for (let p in cp.prefs) {
                    cp.decline(p)
                }
            } else {
                for (let p in cp.prefs) {
                    if (cp.prefs[p]) {
                        cp.accept(p)
                    } else {
                        cp.decline(p)
                    }
                }
            }
            cp.setUserPrefs(cp.prefs)
            cp.updateAcceptedStatus(cp.prefs)
            cp.updateCategoryHeader(cp.prefs)
        },

        checkExistingPrefs: function(prefs) {
            let prefs_exist = false
            for (let p in prefs) {
                if (prefs[p] === true) {
                    prefs_exist = true
                    cp.eventDispatcher('Accepted', p)
                } else if (prefs[p] === false) {
                    prefs_exist = true
                    cp.eventDispatcher('Declined', p)
                }
            }
            if (!prefs_exist) {
                cp.toggle('open')
            }
            cp.updateAcceptedStatus(prefs)
            cp.updateCategoryHeader(prefs)
            cp.setSelectMode()
        },

        categoryToggleHandler: function(button_el, category_data) {
            var list_item = document.querySelector('.cp-category-' + category_data.category)
            list_item.classList.toggle('cp-list-item-open')
            button_el.getAttribute('aria-expanded') === 'false' ? button_el.setAttribute('aria-expanded', true) : button_el.setAttribute('aria-expanded', false)
        },

        setSelectMode: function() {
            let has_selected = false
            for (let p in cp.prefs) {
                if (cp.prefs[p] === true) {
                    has_selected = true
                }
            }
            if (!has_selected || cp.single_select_mode) {
                select_some_mode = false
                cp.btn_accept_selected.style.display = 'none'
                cp.btn_accept_all.style.display = 'inline-block'
            } else if (has_selected) {
                select_some_mode = true
                cp.btn_accept_selected.style.display = 'inline-block'
                cp.btn_accept_all.style.display = 'none'
            }
        },

        categorySelect: function(target) {
            cp.prefs[target.dataset.category] = target.checked
            cp.setSelectMode()
        }
    }
    
    // Defining rough HTML template
    cp.template = ''
        + '<section id="cookie-popup" aria-hidden="false" aria-labelledby="cp-title" role="dialog" aria-describedby="cp-desc" class="cp" style="display: none;">'
        + '  <div role="document" class="cp-wrapper">'
        + '    <header class="cp-header">'
        + '      <h2 id="cp-title">' + cp.translate('Cookie consent') + '</h2>'
        + '    </header>'
        + '    <div id="cp-desc" class="cp-intro">'
        + '      <p class="cp-accept-status" style="display: none;"></p>'
        + '      <p>'
                   + cp.translate('Please consent or reject to cookies used on this site')
        + '      </p>'
        + '    </div>'
        + '    <ul class="cp-list"></ul>'
        + '    <div class="cp-buttons">'
        + '      <button id="cp-reject-all" class="cp-btn">'
                + cp.translate('Reject all')
        + '      </button>'
        + '      <button id="cp-accept-all" class="cp-btn">'
                + cp.translate('Accept all')
        + '      </button>'
        + '      <button id="cp-accept-selected" class="cp-btn" style="display: none;">'
                + cp.translate('Accept selected')
        + '      </button>'
        + '    </div>'
        + '  </div>'
        + '</section>'

    // Build HTML markup
    var cp_el = document.querySelector(cp_element)
    cp_el.innerHTML = cp.template

    cp.popup_el = cp_el.querySelector('.cp')
    cp.cat_list_el = cp_el.querySelector('.cp-list'),
    cp.btn_reject_all = cp_el.querySelector('#cp-reject-all'),
    cp.btn_accept_all = cp_el.querySelector('#cp-accept-all'),
    cp.btn_accept_selected = cp_el.querySelector('#cp-accept-selected')

    // Parse data, create events
    cp.events = cp.createEvents(cp.cookiedata)
    cp.prefs = cp.getUserPrefs(cp.conf, cp.cookiedata)
    cp.single_select_mode = cp.cookiedata.length > 1 ? false : true
    cp.checkExistingPrefs(cp.prefs)

    for (var d in cp.cookiedata) {
        cp.cat_list_el.appendChild(cp.buildListitem(cp.cookiedata[d]))
    }

    // Add event listeners
    var checkboxes = cp_el.querySelectorAll('input[type="checkbox"]')
    for (let c = 0; c < checkboxes.length; c++) {
        checkboxes[c].addEventListener('change', function(ev) {
            cp.categorySelect(this)
        })
    }
    cp.btn_reject_all.addEventListener('click', function(ev) {
        cp.acceptHandler('none')
    })
    cp.btn_accept_all.addEventListener('click', function(ev) {
        cp.acceptHandler('all')
    })
    cp.btn_accept_selected.addEventListener('click', function(ev) {
        cp.acceptHandler('selected')
    })

    // Return public method
    return cp.toggle
}

// Initiate cookiePopup
window.onload = function(ev) {
    cookiePopup = cookiePopupConstructor('cookie-popup', GLOBAL_cookieconf, GLOBAL_cookiedata)
    // Use cookiePopup('open'|'close'|undefined) to open and close the popup
}