// Ombudsmanden.dk cookie popup configuration object
var GLOBAL_cookieconf = {
    // The cookie storing user preferences will have this name
    storageId: "ombudsmanden-dk-cookie-consent",

    // Set cookie domain for storing preferences. This is useful if you plan on using preferences across subsites (ie. www.place.com & other.place.com).
    storageDomain: false, 

    // Set translation strings for your langagues. 
    // English defaults are present in the system but they can also be overridden here.
    translations: {
        "Accept": {
            en: "Accept",
            da: "Acceptér"
        },
        "Accept all": {
            en: "Accept all",
            da: "Acceptér alle"
        },
        "Accept selected": {
            da: "Acceptér valgte"
        },
        "cookies": {
            en: "cookies",
            da: "cookies"
        },
        "Cookie consent": {
            en: "Accept use of cookies at ombudsmanden.dk",
            da: "Accept af cookies fra www.ombudsmanden.dk"
        },
        "expiry": {
            en: "Expires after",
            da: "Udgår efter"
        },
        "name": {
            en: "Name",
            da: "Navn"
        },
        "Please consent or reject to cookies used on this site": {
            en: "We would like to store data on your device to let Ombudsmanden.dk collect and analyse web statistics.<br><a class='laes_mere_cookies btn' href='http://en.ombudsmanden.dk/cookies/'>Read more about cookies on www.ombudsmanden.dk</a>",
            da: "Vi vil gerne gemme oplysninger på din computer eller mobil, så Ombudsmanden kan indsamle webstatistik.<br><a class='laes_mere_cookies btn' href='http://www.ombudsmanden.dk/omhjemmesiden/cookies/'>Læs mere om cookies på www.ombudsmanden.dk</a>"
        },
        "Reject all": {
            en: "Reject all",
            da: "Afvis alle"
        },
        "You've accepted cookies for": {
            en: "You've accepted",
            da: "Du har accepteret"
        },
        "You've declined use of cookies for": {
            en: "You've declined",
            da: "Du har afvist"
        }
    },

    daysUntilExpiry: 365,

    sameSitePolicy: "Strict",
}

// Provide information on known cookies that the user can accept or decline
var GLOBAL_cookiedata = [
    {
        category: "all",
        category_title: {
            en: "All cookies",
            da: "Alle cookies"
        },
        category_description: undefined,
        items: [
            {
                name: "_pk_id",
                expiry: {
                    en: "12 months",
                    da: "12 måneder"
                }
            },
            {
                name: "_pk_ref",
                expiry: {    
                    en: "6 months",
                    da: "6 months"
                }
            },
            {
                name: "_pk_ses",
                expiry: {    
                    en: "30 minutes",
                    da: "30 minutter"
                }
            },
            {
                name: "_pk_testcookie",
                expiry: {    
                    en: "Session only",
                    da: "Udgår når du lukker din browser"
                }
            }
        ]
    }
]