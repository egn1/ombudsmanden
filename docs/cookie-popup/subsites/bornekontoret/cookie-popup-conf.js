// Ombudsmanden.dk cookie popup configuration object
var GLOBAL_cookieconf = {
    // The cookie storing user preferences will have this name
    storageId: "ombudsmanden-bornekontoret-dk-cookie-consent",

    // Set cookie domain for storing preferences. This is useful if you plan on using preferences across subsites (ie. www.place.com & other.place.com).
    storageDomain: false, 

    // Set translation strings for your langagues. 
    // English defaults are present in the system but they can also be overridden here.
    translations: {
        "Accept": {
            en: "Accept",
            da: "Accepter"
        },
        "Accept all": {
            en: "Accept all",
            da: "Acceptér alle"
        },
        "Accept selected": {
            da: "Acceptér valgte"
        },
        "cookies": {
            en: "cookies",
            da: "cookies"
        },
        "Cookie consent": {
            en: "Accept use of cookies at boernekontoret.ombudsmanden.dk",
            da: "Accept af cookies fra boernekontoret.ombudsmanden.dk"
        },
        "expiry": {
            en: "Expires after",
            da: "Udgår efter"
        },
        "name": {
            en: "Cookie",
            da: "Cookie"
        },
        "Please consent or reject to cookies used on this site": {
            en: "We would like to store data on your device. <br><a class='laes_mere_cookies' href='http://en.ombudsmanden.dk/cookies/'>Read more about cookies on www.ombudsmanden.dk</a>",
            da: "Vi vil gerne gemme oplysninger på din computer eller mobil. <br><a class='laes_mere_cookies' href='http://www.ombudsmanden.dk/omhjemmesiden/cookies/'>Læs mere om cookies på www.ombudsmanden.dk</a>"
        },
        "Reject all": {
            en: "Reject all",
            da: "Afvis alle"
        },
        "You've accepted cookies for": {
            en: "You've accepted cookies for",
            da: "Du har accepteret cookies for"
        },
        "You've declined use of cookies for": {
            en: "You've declined use of cookies for",
            da: "Du har afvist cookies for"
        }
    },

    daysUntilExpiry: 365,

    sameSitePolicy: "Strict",
}

// Provide information on known cookies that the user can accept or decline
var GLOBAL_cookiedata = [
    {
        category: "statistics",
        category_title: {
            en: "Statistics",
            da: "Statistik"
        },
        category_description: "Ombudsmanden vil gerne kunne indsamle webstatistik.",
        items: [
            {
                name: "_pk_id",
                expiry: {
                    en: "12 months",
                    da: "12 måneder"
                }
            },
            {
                name: "_pk_ref",
                expiry: {
                    en: "6 months",
                    da: "6 måneder"
                }
            },
            {
                name: "_pk_ses",
                expiry: {
                    en: "30 minutes",
                    da: "30 minutter"
                }
            },
            {
                name: "_pk_testcookie",
                expiry: {
                    en: "Expires when your session ends",
                    da: "Udgår når du lukker din browser."
                }
            }
        ]
    },
    {
        category: "marketing",
        category_title: {
            en: "Targeted ads",
            da: "Målrettede reklamer"
        },
        category_description: "Google vil gerne spore din adfærd og lave målrettet annoncering, når du afspiller videoer fra Youtube.",
        items: [
            {
                name: "NID",
                expiry: {
                    en: "6 months",
                    da: "6 måneder"
                }
            },
            {
                name: "ANID",
                expiry: {
                    en: "14 days",
                    da: "14 dage"
                }
            },
            {
                name: "1P_JAR",
                expiry: {
                    en: "1 month",
                    da: "1 måned"
                }
            },
            {
                name: "CONSENT",
                expiry: {
                    en: "18 years",
                    da: "18 år"
                }
            }
        ]
    },
    {
        category: "personification",
        category_title: {
            en: "Personal settings",
            da: "Personlige indstillinger"
        },
        category_description: "Så Google kan gemme indstillinger, når du afspiller videoer fra Youtube.",
        items: [
            {
                name: "DV",
                expiry: {
                    en: "15 minutes",
                    da: "15 minutter"
                }
            },
            {
                name: "yt-remote-device-id",
                expiry: {
                    en: "12 months",
                    da: "12 måneder"
                }
            },
            {
                name: "yt-remote-connected-devices",
                expiry: {
                    en: "1 day",
                    da: "1 døgn"
                }
            },
            {
                name: "yt-player-headers-readable",
                expiry: {
                    en: "1 month",
                    da: "1 måned"
                }
            },
            {
                name: "yt-player-bandwidth",
                expiry: {
                    en: "1 month",
                    da: "1 måned"
                }
            },
            {
                name: "yt-remote-session-app",
                expiry: {
                    en: "Expires when your session ends",
                    da: "Udgår når du lukker din browser."
                }
            },
            {
                name: "yt-remote-cast-installed",
                expiry: {
                    en: "Expires when your session ends",
                    da: "Udgår når du lukker din browser."
                }
            },
            {
                name: "yt-remote-session-name",
                expiry: {
                    en: "Expires when your session ends",
                    da: "Udgår når du lukker din browser."
                }
            },
            {
                name: "yt-remote-fast-check-period",
                expiry: {
                    en: "Expires when your session ends",
                    da: "Udgår når du lukker din browser."
                }
            }
        ]
    }
]