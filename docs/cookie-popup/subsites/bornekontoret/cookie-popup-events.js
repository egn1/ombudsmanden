// Add event listeners to deal with cookie-dropping content

// Utility method to clear cookies
function clearCookies() {

    var cookielist = document.cookie.split(' ').map(function(c) {
        return c.split('=')[0]
    })

    function parseDomain(hostname) {
        var d = hostname.split('.')
        if (d.length > 1) {
            d[0] = ''
            return d.join('.')
        } else {
            return hostname
        }
    }

    function deleteData(names) {
        for (var n in names) {
            if (names[n] !== 'ombudsmanden-bornekontoret-dk-cookie-consent') {
                // Try to delete cookie of that name
                document.cookie = names[n] + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/;SameSite=Strict;domain=' + parseDomain(location.hostname)
            }
        }
    }
    
    deleteData(cookielist)
}

function clearYoutubes() {
    // Youtube videos
    var youtubes = document.querySelectorAll('iframe[src*="youtu"]')
    for (var y = 0; y < youtubes.length; y++) {
        if ( document.querySelector('html').lang === 'da' ) {
            youtubes[y].outerHTML = '<p class="cc-no-video"><strong>Acceptér cookies for at se denne video.</strong></p>'
        } else {
            youtubes[y].outerHTML = '<p class="cc-no-video"><strong>Accept cookies to view this video.</strong></p>'
        }
    }
}


// When cookies are accepted:
document.addEventListener('statisticsCookiesAccepted', function() {
    initiateMatomo()
})

// When cookies are declined:
document.addEventListener('statisticsCookiesDeclined', function() {
    clearCookies()
})
document.addEventListener('marketingCookiesDeclined', function() {
    clearYoutubes()
    clearCookies()
})
document.addEventListener('personificationCookiesDeclined', function() {
    clearYoutubes()
    clearCookies()
})