// Add event listeners to deal with cookie-dropping content

// Utility method to clear cookies
function clearCookies(cookielist) {

    function parseDomain(hostname) {
        var d = hostname.split('.')
        if (d.length > 1) {
            d[0] = ''
            return d.join('.')
        } else {
            return hostname
        }
    }

    function deleteData(names) {
        for (var n in names) {
            if (names[n] !== 'ombudsmanden-dk-cookie-consent') {
                // Try to delete cookie of that name
                document.cookie = names[n] + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/;SameSite=Strict;domain=' + parseDomain(location.hostname)
            }
        }
    }
    
    deleteData(cookielist)
}

// When cookies are accepted:
document.addEventListener('allCookiesAccepted', function() {
    initiateMatomo()
})

// When cookies are declined:
document.addEventListener('allCookiesDeclined', function() {

    // Youtube videos
    var youtubes = document.querySelectorAll('iframe[src*="youtu"]')
    for (var y = 0; y < youtubes.length; y++) {
        if ( document.querySelector('html').lang === 'da' ) {
            youtubes[y].outerHTML = '<p class="cc-no-video"><strong>Acceptér cookies for at se denne video.</strong></p>'
        } else {
            youtubes[y].outerHTML = '<p class="cc-no-video"><strong>Accept cookies to view this video.</strong></p>'
        }
    }

    // Remove old cookies
    var all_cookie_names = document.cookie.split(' ').map(function(c) {
        return c.split('=')[0]
    })
    clearCookies(all_cookie_names)
})