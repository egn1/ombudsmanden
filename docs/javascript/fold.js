/* Functions for folding and unfolding teasers in hovedregisteret.

   Copyright 2004 (C), Magenta ApS. Under the GPL.

*/

function toggle_cases() {
  var elt=toggle_cases.arguments[0].firstChild;
  var off_text=toggle_cases.arguments[1];
  var on_text=toggle_cases.arguments[2];

  var i;
  var off_on;
  for(i=3; i<toggle_cases.arguments.length; i++) {
    off_on=toggle_case(toggle_cases.arguments[i]);
  }

  /* Here off_on only reflects the effect on the last of the
     arguments, but they should be in sync */
  if (off_on) {
    elt.nodeValue=off_text;
  }
  else {
    elt.nodeValue=on_text;
  }

  return false;
}

function toggle_case(id) {
  var elt=document.getElementById(id);
  if (!elt) { alert('Can not find '+id); return false; }

  if (elt.style.display!='block') {
    elt.style.display='block';
    return false;
  }
  else {
    elt.style.display='none';
    return true;
  }
}

/* Script for opening the "Tip en ven" window */

function open_tip_en_ven(url) {
    var url = document.location.href;
    window.open("./?tip_en_ven=1&url=" + escape(url), "tip_en_ven", "width=450,height=530,scrollbars=yes,resizable=yes");
    return false;
}