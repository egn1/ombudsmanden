/*
  Scripts for main site layout added by: Frank Thomsen @ Magenta ApS, 2012
*/

  // Add info to topmenu links  
  $('.nav-primary .top1 .top-padding').append(' <span class="linkInfo">Find udtalelser og andet</span>');
  $('.nav-primary .top2 .top-padding').append(' <span class="linkInfo">Klag til Ombudsmanden</span>');
  $('.nav-primary .top3 .top-padding').append(' <span class="linkInfo">Om Folketingets Ombudsmand</span>');
  