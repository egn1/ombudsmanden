(function(GridContent, $, undefined) {
    //Private Property
    var colsByRowClass = {
        "single-column-half": [2],
        "single-column-narrow": [4],
        "single-column": [6],
        // "two-one-column": [4, 2],
        // "one-two-column": [2, 4],
        "one-one-column": [3, 3],
        // "triple-column": [2, 2, 2]
    };
    var templates = {};
    var translations = {};
    var defaultFields = {};

    //Private Method
    function saveValue(gcElem) {
        var val = [];
        $.each(gcElem.find("tr.rowcontent"), function() {
            var columns = [];
            $.each($(this).find("ul.gclist"), function() {
                var items = [];
                $.each($(this).find("li.gcboxitem"), function() {
                    items.push($(this).data());
                });
                columns.push(items);
            });
            val.push({
                type: $(this).data("type"),
                columns: columns,
                columnbgs: $(this).data("columnbgs"),
                backgroundcolor: $(this).data("backgroundcolor")
            });
        });
        valElem = document.getElementById(gcElem.data("valueId"));
        $(valElem).val(JSON.stringify(val));
    }

    function loadFromData(gcElem, data) {
        var target = gcElem.find("tbody").first();
        $.each(data, function(i, row) {
            var elem = createRow(row.type, row.columns || [], row.backgroundcolor || "bg-transparent", row.columnbgs || [], gcElem);
            if(elem) {
                target.append(elem);
            }
        });
    }

    function createItem(data, gcElem) {
        var html = GridContent.expandTemplate('item', data);
        var item = $(html);
        item.find(".edit").on("click", function () {
            GridContent.activeElement = gcElem;
            GridContent.editItem($(this).parent().parent());
        });
        item.find(".delete").on("click", function() {
            if (confirm('Vil du slette dette element?')) {
              $(this).parent().parent().remove();
              saveValue(gcElem);
            }
        });
        item.find(".heading").html(GridContent.translate(
            "item-type-" + data.type
        ));

        var content = data.renderedHTML;
        if(content) {
            item.find("div.gcboxcontent").html(content);
            delete data.renderedHTML;
        }
        item.data(data);
        if(!content)
            GridContent.loadItemHTML(item);

        return item;
    }


    function createRow(className, columnsData, backgroundColor, columnBgs, gcElem) {
        var cols = colsByRowClass[className];
        if(! cols)
            return null;

        var colBgs = [];
        var columnHtml = '';
        $.each(cols, function(i, c) {
            columnHtml += GridContent.expandTemplate('column', {
                "colspan": c
            });
            colBgs.push(columnBgs[i] || "bg-transparent");
        });
        var rowHtml = GridContent.expandTemplate('row', {
            "class": className,
            "backgroundcolor": backgroundColor,
            "columns": columnHtml
            // we could add a columnbgs key here, but what would it do in the expanded markup?
        });
        var row = $(rowHtml);
        row.data("type", className);
        row.data("backgroundcolor", backgroundColor);
        row.prev(".rowheading").find(".backgroundcolor").val(backgroundColor); // set the actual dropdown element
        row.data("columnbgs", colBgs);
        row.find(".col-backgroundcolor").each(function(idx){ // set the dropdowns for column bg to their appropriate values
          $(this).val(colBgs[idx]);
        });
        if(columnsData) {
            $.each($(row).find("ul.gclist"), function(i, list) {
                $.each(columnsData[i] || [], function() {
                    var item = createItem(this, gcElem);
                    $(list).append(item);
                });
            });
        }

        row.find("th .moveup").on("click", function() {
            var header = $(this).parent().parent();
            var content = header.next();
            var prev = header.prev().prev();
            if (!prev.hasClass("rowheading"))
                prev = prev.prev();
            prev.before(header);
            prev.before(content);
            saveValue(gcElem);
        });
        row.find("th .movedown").on("click", function() {
            var header = $(this).parent().parent();
            var content = header.next();
            var next = content.next();
            if (!next.hasClass("rowheading"))
                next = next.next();
            next.next().after(content);
            next.next().after(header);
            saveValue(gcElem);
        });
        row.find("th .backgroundcolor").on("change", function() {
          // update the class that controls background color
          var header = $(this).parent().parent();
          var content = header.next();
          content.data("backgroundcolor", $(this).val());
          saveValue(gcElem);
        });
        row.find("th .close").on("click", function() {
            var tr = $(this).parent().parent();
            if (confirm('Vil du slette denne række?')) {
              tr.next().remove();
              tr.remove();
              saveValue(gcElem);
            } else {
            }
        });

        row.find(".addmoreitems").on("click", function(e){
            GridContent.activeElement = gcElem;
            GridContent.activeList = $(this).parent().prev();
            GridContent.openDialog(
                'gridcontent-dialog-additem', gcElem, e
            );
        });

        row.find(".gccolumn").each(function(idx) {
          // do this inside .each() to get the proper index for the onchange event
          $(this).find(".col-backgroundcolor").on("change", function(){
            row.data("columnbgs")[idx] = $(this).val();
            saveValue(gcElem);
          });
        });

        return row;
    }

    $.extend(
        GridContent,
        // Public properties
        {
            version: 0.1,
            data: {},
            valueElements: {},
            activeElement: null,
            activeList: null
        },
        // Public methods
        {
            Init: function() {
                var templateElems = $('#gridcontent-templates .template');
                $.each(templateElems, function() {
                    c = $(this);
                    var name = c.attr('id') || '';
                    name = name.replace(/^gridcontent\-template\-/, '');
                    if(name) {
                        templates[name] = c.html();
                    }
                });
            },
            StartUp: function(valueElement, fieldName) {
                GridContent.valueElements[fieldName] = valueElement;
                var elemId = '#obvius_gridcontent_' + fieldName;
                var gcElem = $(elemId);
                /* jshint -W061 */
                var data = eval ($(valueElement).val());
                /* jshint +W061 */
                gcElem.data("valueId", $(valueElement).attr("id"));

                loadFromData(gcElem, data);

                $(elemId + '_addrow, ' + elemId + '_addrow2').on("click", function(e){
                    GridContent.openDialog(
                        'gridcontent-dialog-addrow', gcElem, e
                    );
                });

                GridContent.initDragSort(gcElem);
            },
            initDragSort: function(gcElem) {
                var elems = gcElem.find("td.gccolumn ul");
                elems.dragsort("destroy");
                elems.dragsort({
                  //dragSelector: "h3.gcboxtitle",
                  itemSelector: "li.gcboxitem",
                  dragBetween: true,
                  placeHolderTemplate: '<li class="gcboxitem gcboxitemplaceholder"></li>',
                  dragEnd: function() {
                    saveValue(gcElem);
                  }
                });

                saveValue(gcElem);
            },
            expandTemplate: function (templateName, data) {
                var html = templates[templateName] || '';
                var expander = function(fullmatch, key) {
                    return data[key.toLowerCase()] || fullmatch;
                };
                html = html.replace(/<!--#([^#]+)#-->/g, expander);
                return html.replace(/#([^#]+)#/g, expander);
            },
            openDialog: function(id, gcElem, ev, arg) {
                GridContent.activeElement = gcElem;
                $("div.gridcontent-dialog").hide();
                var dlgElem = $('#' + id);
                dlgElem.css({
                    "margin-left": '-' + (dlgElem.width()/2) + "px"
                });
                dlgElem.show();
                dlgElem.trigger("dialogopen", [arg]);
            },
            addRow: function(className, columnData) {
                GridContent.activeElement.find("tbody").append(
                    createRow(className, columnData, "bg-transparent", [], GridContent.activeElement)
                );
                GridContent.initDragSort(GridContent.activeElement);
            },
            addItem: function(className, ev) {
                var data = {
                    type: className,
                    renderedHTML: className + "-placeholder"
                };
                $.extend(data, defaultFields[className] || {});
                GridContent.activeItem = createItem(
                    data,
                    GridContent.activeElement
                );
                GridContent.activeList.append(GridContent.activeItem);
                GridContent.initDragSort(GridContent.activeElement);
                GridContent.editItem(GridContent.activeItem, ev);
            },
            editItem: function(item, ev) {
                GridContent.activeItem = item;
                GridContent.openDialog(
                    'gridcontent-dialog-edit-' + item.data("type"),
                    GridContent.activeElement,
                    ev
                );
            },
            updateItem: function(item, data) {
                item.data(data);
                GridContent.loadItemHTML(item);
                saveValue(GridContent.activeElement);
            },
            loadItemHTML: function(item) {
                var data = item.data();
                var container = item.find("div.gcitem");
                if(data.type == "html") {
                    container.html(data.content.replace(
                        /<script[^>]*>.*?<\/script>/g, ''
                    ));
                } else {
                    container.html(
                        '<span class="loadingmessage">' +
                        GridContent.translate('data-placeholder-message') +
                        '</span>'
                    );
                    container.load(
                        "./",
                        $.extend(
                            {obvius_app_load_gridcontent_data: 1},
                            item.data()
                        )
                    );
                }
            },
            addTranslations: function(t) {
                $.extend(translations, t);
            },
            translate: function(key) {
                return translations[key] || key;
            },
            setWidgetDefaults: function(widgetName, defaults) {
                defaultFields[widgetName] = defaults;
            }
        }
    );
})( window.GridContent = window.GridContent || {}, jQuery );

$(GridContent.Init);
