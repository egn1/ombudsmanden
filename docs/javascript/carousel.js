$(document).ready(function(){
  var wrapper = $(".carousel-slide").closest("[class*='col-']") // find slide wrapper
  var container = wrapper.closest("section[class*='row']") // find slide container
  wrapper.addClass("carousel-wrapper") // add classes to wrapper
  container.addClass("carousel-container") // add classes to container
  wrapper.after($("<div>", { // append pagination
    class:"carousel-pagination"
  }))
  var swiper = new Swiper(".carousel-container", {
    wrapperClass: "carousel-wrapper",
    slideClass: "carousel-slide",
    //slidesPerView: "auto",
    pagination: {
      el: ".carousel-pagination",
      clickable: true
    }
  })
})
