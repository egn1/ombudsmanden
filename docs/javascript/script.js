/* 
 * Scripts added by: Toke Fritzemeier @ Magenta, 2017
 * Revised by Frank Thomsen @ Magenta, 2020
 */

$(document).ready(function(){

  // Toggling accordions (examples and fact boxes; header controls; topic browser on search page)
  $(".accordion-toggle").click(function(){
    var targ = $(this).parent(".accordion");
    var cnt = $(this).siblings(".accordion-content");

    Omb.toggleAriaExpanded(this)
    Omb.updateAccordion(targ, cnt, "toggle");
  }).keypress(function(e){ // also manually bind space key action as this is not a button
    if(e.which == 32) {
      e.preventDefault();
      $(this).trigger("click");
    }
  });

  // Toggling the global header content toggle
  $("#header-controls-toggle").click(function(){
    var siteHeader = $("#site-header")
    // Toggle aria-expanded
    Omb.toggleAriaExpanded(this)
    // If we have "open" now, that means we just toggled it *on*.
    // If so, we should bind a handler to the keypress event in order to enable ESC keystroke to close menu.
    // If not, we should remove said handler
    if(siteHeader.hasClass("open")) {
      Omb.toggleInOrder(siteHeader, 'open', 'transition', 150)
      $("body").toggleClass("globalnav-open")
      $("body").on("keydown", Omb.closeHeaderContent)
    } else {
      Omb.toggleInOrder(siteHeader, 'transition', 'open', 10)
      $("body").toggleClass("globalnav-open")
      $("body").off("keydown", Omb.closeHeaderContent)
    }
  })

  // Toggling main menu
  $("#nav-top-toggle").click(function(){
    var wrapper = $("#nav-top");
    var content = $("#nav-top > .col-8");

    // Toggle aria-expanded
    Omb.toggleAriaExpanded(this)

    Omb.updateAccordion(wrapper, content, "toggle");
  });

  // Clicking the search icon in the header
  $("body:not(.doctype-find) #search-toggle").click(function(){
    var search = $("#search");
    var form = search.find("form[name='header-search'] .form-controls"); 
    var isOpen = search.hasClass("open");
    var dur = Omb.secondsToMillis(form.css("transition-duration"));

    // Toggle aria-expanded
    Omb.toggleAriaExpanded(this)

    Omb.updateAccordion(search, form, "toggle");

    setTimeout(function(){
      search.toggleClass("overflow-visible"); // we need to toggle overflow because otherwise the typeahead box would be invisible
      if(!isOpen) $("#header-search-input").focus(); // if we're not already open, that means we're transitioning to open state. If so, focus the input field
    }, search.hasClass("open") ? dur : 0);
  });

  // clicking search icon in header on Find doctype, and move cursor to end of field
  $("body.doctype-find #search-toggle").click(function(){
    var input = $("#query");
    var len = input.val().length;
    input.focus();
    input[0].setSelectionRange(len, len);
  });

  function extract_highlighting(highligt_data) {
    if(!highligt_data) {
        return "";
    }
    var result = [];
    $.each(highligt_data, function(k, v) {
        $.merge(result, v);
    });
    return result.join(" ");
  }

  // Using the header search field
  var xhr_typeahead;
  $("#header-search-input").focus(function(){
    // attach ESC key handler
    $("body").on("keydown.OmbTypeahead", function(e){
      if(e.keyCode == 27) {
        Omb.resetTypeahead();
        $("#header-search-input").blur();
      }
    });
  }).keyup(Omb.debounce(function(){
    var val = $(this).val();
    var prevVal = $(this).attr("data-previous");
    var container = $("#search-typeahead");
    var content = $("#search-results");
    var delay = Omb.secondsToMillis(content.css("transition-duration"));
    var mode; // accordion control variables

    if(val !== prevVal) { // only do something if actual query changed
      if(val.length > 0) { // if we have a query string
        $(this).attr("autocomplete", "off"); // disable autocomplete so it doesn't interfere with the typeahead table
        if(xhr_typeahead && xhr_typeahead.readyState < 4) { // clear any old incomplete requests
          xhr_typeahead.abort();
        }
        // perform the search query
        xhr_typeahead = $.ajax({          
          url: "/system/search?query=" + encodeURIComponent(val.replace(/^\s+|\s+$/g, "")), // trim the string. URI encode everything.
          dataType: "json",
        }).done(function(data){
          content.html("");
          var results = [];
          $.each(data.response.docs || [], function(i, res){
            var result = $("<p/>", { // generate the markup for each result
              class:"search-result",
              html:function(){
                return $("<a/>", {
                  href:res.k_url[0],
                  html:function(){
                    return $("<small/>", {
                      text:res.f_title[0],
                    }).add($("<span/>", {
                      html: extract_highlighting(data.highlighting[res.id]) ,
                    }));
                  },
                });
              },
            });
            results.push(result);
          });
          content.html(results); // append all the results
          $("#search-num-results").html(data.response.numFound + " resultat" + (data.response.numFound == 1 ? "" : "er")); // set number of results
          content.css("height", ""); // clear the old explicitly set height so we don't just get that as a measurement
          if(prevVal.length == 0) { // if we came from an empty string, it's time to toggle the typeahead table
            mode = "toggle";
            container.addClass("overflow-visible");
          }
          Omb.updateAccordion(container, content, mode);
        });
      } else { // if value length is not greater than 0, we deleted the last character of the input field and should close the typeahead
        $(this).attr("autocomplete", "on"); // re-enable autocomplete
        mode = "close";
        Omb.updateAccordion(container, content, mode);
        setTimeout(function(){
          container.removeClass("overflow-visible"); // delay the toggling of overflow to let the animation complete
        }, delay);
      }
      $(this).attr("data-previous", val); // update data attribute to reflect previous input value
    }
  }, 180)).blur(function(){ // the 180 is the debounce duration
    $(this).attr("autocomplete", "on"); // enable autocomplete explicitly as we may have disabled it prior to this event
    $("body").off("keydown.OmbTypeahead"); // remove ESC key event handler
  });

  // Using the "reset" function of the header search field
  $("#search button[type=reset]").click(Omb.resetTypeahead);

  // Clicking slide-in toggles
  $(".slide-in-toggle").click(function(){
    // set convenience variables
    var body = $("body");
    var main = $("#container");
    var delay = main.css("transition-duration"); // we need to wait this amout before removing the .flyout-open class, otherwise we'll get a horz scrollbar

    var targId = "#" + $(this).attr("data-slide-target");
    var $targ = $(targId);
    var targOpen = $targ.hasClass("open");

    Omb.toggleAriaExpanded(document.querySelector(targId + '-toggle'))

    // handle the current target
    if(targOpen){ // if it's already open, close it
      Omb.resetSlideIn($targ, delay);
      body.addClass("transitioning"); // use a special class while transitioning to closed state
      setTimeout(function(){ // remove the transitioning class again once we're done
        body.removeClass("transitioning");
      }, Omb.secondsToMillis(body.css("transition-duration")));
    } else { // if it's not yet open, open it
      body.addClass("flyout-open");
      $("html").on("click.OmbSlideIn", function(){
        Omb.resetSlideIn($(".slide-in"), $("#container").css("transition-duration"));
      });
      $targ.addClass("transition open");
    }
  });

  // cancel bubbling for clicks within slide-in panels, so we don't accidentally close them
  // do the same for clicks within header controls/header content
  // do the same for clicks within search (to not accidentally close typeahead)
  $(".slide-in, .slide-in-toggle, #header-controls-wrapper, #header-content, #search").click(function(e){
    e.stopPropagation();
  });

  // close slide-in panels and header content by clicking anywhere else
  // also close typeahead table
  $("html").click(function(){
    Omb.updateAccordion($("#nav-top"), $("#nav-top > .col-8"), "close"); // reset the open state of the main nav
  });

  // clicking a link in the #toc-nav scrolls to the specific target
  $("#toc-nav a").click(function(e){
    e.preventDefault(); // prevent default link action
    var delay = $("#container").css("transition-duration");
    var href = $(this).attr("href")
    var targ = href.split("#")[1];
    
    Omb.state.sectionPick = targ // store this target in Ombud.state so we can scroll to it when closing the nav.

    var wait = 0;
    if(window.matchMedia("(max-width: 576px)").matches){ // if we're on a small screen, close the #toc-nav before scrolling
      wait = Omb.secondsToMillis(delay);
      $("#toc-nav-toggle").trigger("click");
    }
    setTimeout(function() {
      Omb.scrollTo('span[id="' + targ + '"]')
    }, wait)
  });

  // expanding all subjects on navigational pages
  $(".show-all-subdocs").click(function(){
    $(".subdoc:nth-child(n+7)").toggle();
    var text = $(".show-all-subdocs").text();
    $(".show-all-subdocs").text(text == "Vis alle dokumenter" ? "Vis færre dokumenter" : "Vis alle dokumenter");
  });

  // switching between grid and list view in search results/specific subject areas
  $(".preview-mode-selector-grid").click(function(){
    $("#fullsearch-results").removeClass("list").addClass("grid");
  });
  $(".preview-mode-selector-list").click(function(){
    $("#fullsearch-results").removeClass("grid").addClass("list");
  });

  // expanding/collapsing items on process page
  $(".ombud-process .step button").click(function(){
    $(this).closest(".left, .right").toggleClass("open");
  });

  // clicking the print icon above the content column
  $(".omb-print").click(function(){
    window.print();
  })

  $(window).scroll(function(){
    // /* --- scrollspy functionality to update active link in #toc-nav when we scroll the page --- */ //
    // update the active menu item when we scroll
    // based on https://jsfiddle.net/mekwall/up4nu/

    // get scroll position
    var fromTop = $(this).scrollTop();
    var winHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight; // use vanilla JS because $(window).height() gives incorrect values with no <!doctype html> set
    var fromBottom = fromTop + winHeight;
    var idx;
    for(var i = Omb.sections.length - 1; i >= 0; i--) {
      var thisTop = Omb.sections.eq(i).offset().top;
      // this if() is a three-parter: (1) generally, if we scrolled enough that top of section is above middle of screen,
      // or (2) if we're at the first heading (i == 0) where top of section may not be above middle of screen,
      //  or (3) if we're at the last heading, and the top of section is above bottom of screen, as it may never reach above middle of screen.
      if(thisTop < fromTop + .5 * winHeight || i == 0 || (i == Omb.sections.length - 1 && thisTop < fromBottom)) {
        idx = i;
        break;
      }
    }

    // only actually do something if we're at a new index
    if(Omb.lastIndex !== idx){
      Omb.lastIndex = idx;
      Omb.menuItems.removeClass("active");
      Omb.menuItems.eq(idx).addClass("active");
    }
    // /* --- end scrollspy functionality --- */ //
  });

  // Handle the visibility of the header if appropriate display size
  $(window).scroll(
    Omb.debounce(function(){
      Omb.handleHeader();
    }, 100)
  );
});

var Omb = (function(){

  var state = {
    sectionPick: null
  }

  // cache scrollspy related selectors for performance reasons
  var menuItems = $(".toc-nav-list > li");
  var sections = $("#content").find("h1.numbered-heading, h1.spot-title-toc").find("span[id^=chapter]");
  var numSections = sections.length;
  var lastIndex;

  // variable for keeping track of scroll state
  var siteHeader = $("#site-header");
  var siteHeaderHeight = siteHeader.outerHeight();
  var previousScroll = $(window).scrollTop(); // should be zero, but maybe we reloaded the page while scrolled down

  // Enable toggle of classes with a delay in between
  // Useful for transitions where an intermediate state is needed, ie display: none -> display: block; width: 0; -> display: block; width: 100%
  function toggleInOrder(element, class1, class2, duration, callback) {
    element.toggleClass(class1);
    setTimeout(function(){
      element.toggleClass(class2);
      if (callback) {
        callback()
      }
    }, duration)
  }

  // Function to toggle accordions
  function updateAccordion(target, content, mode) {
    if(mode == "toggle") {
      if (target.hasClass('open')) {
        toggleInOrder(target, 'open', 'transition', secondsToMillis(content.css("transition-duration")))
      } else {
        toggleInOrder(target, 'transition', 'open', 10)
      }
    } else if(mode == "close") {
      target.removeClass("open transition")
    }
  }

  function scrollTo(elementQuery) {
    document.querySelector(elementQuery).scrollIntoView({behavior: 'smooth'})
  }

  // convenience function to reset open slide-ins
  function resetSlideIn(jqobj, dur){
    dur = secondsToMillis(dur); // we still need to use delay, because if we remove the class on body immediately we'll get the horizontal scrollbar

    $("html").off("click.OmbSlideIn"); // remove click handler from <html>

    // reset margin-left on container and remove "open" class on parent to collapse slide-in
    $("#container").css({
      "margin-left":""
    });
    jqobj.removeClass("open");

    setTimeout(function(){
      $("body").removeClass("flyout-open");
      jqobj.removeClass("transition");
       // if this is the #toc-nav we're toggling, check if the user clicked a
       // link in the nav, and scroll to the position of this section when
       // we're done collapsing the slide-in
      if(jqobj.attr("id") == "toc-nav" && Omb.state.sectionPick) {
        // Scrolling target section into view after layout changes
        // For unknown reasons, we need to delay this a little longer 
        setTimeout(function() { 
          scrollTo('span[id="' + Omb.state.sectionPick + '"]')
        }, dur)
      }
    }, dur);
  }

  // convenience function to convert jQ return value for "transition-duration" to milliseconds
  function secondsToMillis(inp) {
    return parseFloat(inp) * 1000 + 20; // convert the duration to ms; jQuery returns seconds. Add a bit to make sure animation is complete
  }

  // /* --- debounce function by David Walsh, https://davidwalsh.name/javascript-debounce-function --- */ //
  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  function debounce(func, wait, immediate) {
  	var timeout;
  	return function() {
  		var context = this, args = arguments;
  		var later = function() {
  			timeout = null;
  			if (!immediate) func.apply(context, args);
  		};
  		var callNow = immediate && !timeout;
  		clearTimeout(timeout);
  		timeout = setTimeout(later, wait);
  		if (callNow) func.apply(context, args);
  	};
  }

  // function to close header content by pressing a key on the keyboard
  function closeHeaderContent(e){
    if(e.keyCode == 27) {
      $("#header-content").removeClass("open");
      $("body").removeClass("globalnav-open");
    }
  }

  // function to reset typeahead content and animate the content field
  function resetTypeahead(){
    var container = $("#search-typeahead");
    var content = $("#search-results");
    var delay = Omb.secondsToMillis(content.css("transition-duration"));

    Omb.updateAccordion(container, content, "close");

    setTimeout(function(){
      $("#header-search-input").attr("autocomplete", "on").attr("data-previous", ""); // re-enable autocomplete, reset data-previous
      $("#search-num-results").add(content).html(""); // reset the markup of the elements
      container.removeClass("overflow-visible"); // delay the toggling of overflow to let the animation on the accordion complete
    }, delay);
  }

  // function to show/hide header depending on scroll direction
  // Based on https://codepen.io/philipbenton/pen/Ftmfz
  function handleHeader() {
    if(window.matchMedia("(max-width: 576px)").matches){ // only do something if we're below the smallest breakpoint (i.e. smartphone-sized)
      var currentScroll = $(window).scrollTop();
      var winHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight; // use vanilla JS because $(window).height() gives incorrect values with no <!doctype html> set
      if (currentScroll > 0 && currentScroll < $(document).height() - winHeight){ // this translates to "if the scroll position is within the boundaries of the content part of the window"
        if(currentScroll > siteHeaderHeight) {
          siteHeader.stick_in_parent({
            parent:"body",
          });
        }
        if (currentScroll > previousScroll && currentScroll > siteHeaderHeight){ // if we're scrolling down AND we're past the site header height
          siteHeader.removeClass("is-visible").addClass("is-hidden");
        } else {
          siteHeader.removeClass("is-hidden").addClass("is-visible"); // else set to visible. This can also occur when we're NOT past the site header height, which we need
        }
      } else if(currentScroll <= 0) { // if we're back within the original range of the header
        siteHeader.trigger("sticky_kit:detach");
        if(currentScroll <= 0) { // if we're also back at top of page
          siteHeader.removeClass("is-visible is-hidden");
        }
      }
      previousScroll = currentScroll;
    }
  }

  function toggleAriaExpanded(btnElmt) {
    if (btnElmt.getAttribute('aria-expanded') === 'true') {
      btnElmt.setAttribute('aria-expanded', 'false')
    } else {
      btnElmt.setAttribute('aria-expanded', 'true')
    }
  }

  return {
    state:state,
    scrollTo:scrollTo,
    menuItems:menuItems,
    sections:sections,
    numSections:numSections,
    lastIndex:lastIndex,
    toggleInOrder:toggleInOrder,
    updateAccordion:updateAccordion,
    resetSlideIn:resetSlideIn,
    secondsToMillis:secondsToMillis,
    debounce:debounce,
    closeHeaderContent:closeHeaderContent,
    resetTypeahead:resetTypeahead,
    handleHeader:handleHeader,
    toggleAriaExpanded: toggleAriaExpanded
  }
})();
