/*
This file is a port of fixWordRTF.js from www.ombudsmanden.dk.

This either re-use or re-implement various methods found in obvius_tinymce.js.
Methods being re-implemented are prefixed "ombudsmanden_" instead of "obvius_".

$Id: ombudsmanden_tinymce.js,v 1.3 2005/06/29 12:43:48 martin Exp $
*/

function ombudscleanup(html) {
    return ombudsmanden_tinymce_html_cleanup("insert_to_editor", html);
}


function ombudsmanden_tinymce_html_cleanup(type, content) {
    if(type != "insert_to_editor") {
        return content;
    }
    alert ("Starting custom cleanup");
	var startTime = new Date().getTime();
    
    var tmpContainer = document.createElement('div');
    tmpContainer.innerHTML = content;

    obvius_tinymce_removeUnusedAttribute(tmpContainer,"OL","style")
    obvius_tinymce_removeUnusedAttribute(tmpContainer,"UL","style")
    obvius_tinymce_removeUnusedAttribute(tmpContainer,"SPAN","lang")
    obvius_tinymce_removeUnusedAttribute(tmpContainer,"LI","style")
    obvius_tinymce_removeUnusedAttribute(tmpContainer,"P","class")
    obvius_tinymce_removeUnusedAttribute(tmpContainer,"P","style")
    obvius_tinymce_removeUnusedAttribute(tmpContainer,"FONT","face")
    obvius_tinymce_removeUnusedAttribute(tmpContainer,"FONT","style")
    obvius_tinymce_removeUnusedAttribute(tmpContainer,"FONT","size")
    obvius_tinymce_removeUnusedAttribute(tmpContainer,"SPAN","style")
    obvius_tinymce_removeUnusedAttribute(tmpContainer,"A","name")

    //Note: the sequence is different from obvius_tinymce_html_cleanup
    //in order to match the exact behaviour of fixWordRTF.js
    ombudsmanden_tinymce_parseFrameInnerHTML(tmpContainer);

    obvius_tinymce_removeEmptyTags(tmpContainer);

    ombudsmanden_tinymce_replaceWordParagraphs(tmpContainer);
    ombudsmanden_tinymce_mergeQuoteTrippleDot(tmpContainer);
    ombudsmanden_tinymce_removeBlindLinks(tmpContainer);
    ombudsmanden_tinymce_putTitleOnTargetBlank(tmpContainer);

    obvius_tinymce_removeMutipleNBSP(tmpContainer);

    // Fixup wrong placement of strong and b tags:
    obvius_tinymce_remove_wrong_span_type_tags(tmpContainer, 'b');
    obvius_tinymce_remove_wrong_span_type_tags(tmpContainer, 'strong');

    // Fixup <p> inside <caption> on tables:
    obvius_tinymce_fix_caption_p_tags(tmpContainer);

    alert ("Done fixing: " + (new Date().getTime()-startTime));
    
    
    content = tmpContainer.innerHTML;
    return content;
}

function ombudsmanden_tinymce_parseFrameInnerHTML(rootElem){

    strTmp = rootElem.innerHTML;

    strTmp = strTmp.replace(/<\?xml.*\/>/g,'')
    strTmp = strTmp.replace(/\s*mso-[^;"]*;*(\n|\r)*/g,'')
    strTmp = strTmp.replace(/\s*page-break-after[^;]*;/g,'')
    strTmp = strTmp.replace(/\s*tab-interval:[^'"]*['"]/g,'')
    strTmp = strTmp.replace(/\s*tab-stops:[^'";]*;?/g,'')
    strTmp = strTmp.replace(/\s*LETTER-SPACING:[^\s'";]*;?/g,'')
    strTmp = strTmp.replace(/\s*class=MsoNormal/g,'')
    strTmp = strTmp.replace(/\s*class=MsoBodyText[2345678]?/g,'')

    strTmp = strTmp.replace(/<o:p>/g,'')
    strTmp = strTmp.replace(/<\/o:p>/g,'')

    strTmp = strTmp.replace(/<v:[^>]*>/g,'')
    strTmp = strTmp.replace(/<\/v:[^>]*>/g,'')

    strTmp = strTmp.replace(/<w:[^>]*>/g,'')
    strTmp = strTmp.replace(/<\/w:[^>]*>/g,'')

	strTmp = strTmp.replace(/<st\d:[^>]*>/g,'')
	strTmp = strTmp.replace(/<\/st\d:[^>]*>/g,'')

	strTmp = strTmp.replace(/<FONT[^>]*>/gi,'')
	strTmp = strTmp.replace(/<\/FONT[^>]*>/gi,'')

	strTmp = strTmp.replace(/�/g, '...'); //Note: \205 is a single character (dig: 133, hex: 85, unicode: U+2026)
	strTmp = strTmp.replace(/�/g, '"'); //Note: \224 is a single character (dig: 148, hex: 94, unicode: U+201D)
	strTmp = strTmp.replace(/�/g, '"'); //Note: \223 is a single character (dig: 147, hex: 93, unicode: U+201C)

	//Spans are used many, unnecessary, places
	strTmp = strTmp.replace(/<SPAN[^>]*>/gi,'')
	strTmp = strTmp.replace(/<\/SPAN[^>]*>/gi,'')

	//Word/IE has a nasty habit of inserting lots of <a></a> (empty) - remove them
    strTmp = strTmp.replace(/<a><\/a>/gi,'');

	//Word has an equally bad habit of inserting empty headers
	strTmp = strTmp.replace(/<h[123456]>&nbsp;<\/h[123456]>/gi,'');

    //.. and empty paragraphs too..
    strTmp = strTmp.replace(/<P[^>]*>&nbsp;<\/P>/gi,'');

    rootElem.innerHTML = strTmp;
}

function ombudsmanden_tinymce_replaceWordParagraphs(rootElem)
{
    /* Word has a nasty habit of using <P> of some class instead of the correct
        HTML tag. This method tries to do something about it.
    */
  var cur_func = "ombudsmanden_tinymce_replaceWordParagraphs";
  //alert("Start: " + cur_func);
  
    var classMap = new Object();
    classMap["Hoved1"] = "h1";//Headings
    classMap["Hoved2"] = "h2";
    classMap["Hoved3"] = "h3";
    classMap["Hoved4"] = "h4";
    classMap["Afsnit-hoved1"] = "h1";
    classMap["Afsnit-hoved2"] = "h2";
    classMap["Afsnit-hoved3"] = "h3";
    classMap["Afsnit-hoved4"] = "h4";
    classMap["Ombudsmandsudtalelse"] = "cite";
    classMap["Afsnit-udtalelse"] = "cite";

    tmpArray = rootElem.getElementsByTagName("P");

    //Walk backwards through the array since we are modifying it as we go along
    for(i=tmpArray.length-1;i>=0;i--) {
        if (classMap[tmpArray[i].className] != null) {
            var newElem = document.createElement(classMap[tmpArray[i].className]);

            //Add all descendants of the old node to the new node
            var children = tmpArray[i].childNodes;
            for (n=0;n<children.length;n++) {
                nNode = children[n].cloneNode(true); //We need to clone the child because its removed later on
                newElem.appendChild(nNode);
            }

            tmpArray[i].parentNode.replaceChild(newElem, tmpArray[i]);
        }
    }
  //alert("End: " + cur_func);
}

function ombudsmanden_tinymce_mergeQuoteTrippleDot (rootElem)
{
  /*Ombudsmandens quotes often start with '<p>"...</p>'. This method merges '"...'
	down into the next paragraph. See ticket #1550.
	'..."' is merged up into the previous paragraph.
	Windows writes "..." as a single char (\205) so the string.replace() in the top is needed
	for this method to work.
	The replacement is done i three steps in order to avoid all sorts of nasty problems
   */
  var cur_func = "ombudsmanden_tinymce_mergeQuoteTrippleDot";
  //alert("Start: " + cur_func);
  
  var tmpArray = rootElem.getElementsByTagName("P");

  var beginQuotes = new Array();
  var endQuotes = new Array();

  //First sort out all the relevant nodes
  for (i=0;i<tmpArray.length;i++) {
	if (tmpArray[i].firstChild != null) {
	  if (tmpArray[i].firstChild.nodeValue == "\"...") {
		beginQuotes[beginQuotes.length] = tmpArray[i];
	  } 
	  else if (tmpArray[i].firstChild.nodeValue == "...\"") {
		endQuotes[endQuotes.length] = tmpArray[i];
	  }
	}
  }

  //Then do something about them
  for (i=0;i<beginQuotes.length;i++) {
	var sibling = beginQuotes[i].nextSibling;
	sibling.insertBefore(rootElem.document.createElement("br"),sibling.firstChild);
	sibling.insertBefore(rootElem.document.createTextNode("\"..."),sibling.firstChild);
  }
  for (i=0;i<endQuotes.length;i++) {
	var sibling = endQuotes[i].previousSibling;
	sibling.appendChild(rootElem.document.createElement("br"));
	sibling.appendChild(rootElem.document.createTextNode("...\""));
  }

  //Delete all the, now, irrelevant nodes
  for (i=0;i<beginQuotes.length;i++) {
	beginQuotes[i].parentNode.removeChild(beginQuotes[i]);
  }
  for (i=0;i<endQuotes.length;i++) {
	endQuotes[i].parentNode.removeChild(endQuotes[i]);
  }
  //alert("End: " + cur_func);
}

function ombudsmanden_tinymce_removeBlindLinks (rootElem)
{
  /*
	After the various regex's and cleaning functions a lot of <a> elements without
	attributes are often left behind. These are useless so lets remove them, but
	without losing their contents. 
	There is no clean way to check if an element has any attributes so instead  we 
	check for href, id, and name. The link is removed if none of those are found.

	Blind <a> elements are often found around headings. Example:
	  <h1><a><strong>Foo Bar</strong></a></h1>
   */
  var cur_func = "ombudsmanden_tinymce_removeBlindLinks";
  //alert("Start: " + cur_func);


  var tmpArray;
  var requiredAttributes = ["href", "id", "name"]; //Element must have at least one of these attributes
  var removeLinks = new Array();
  
  tmpArray = rootElem.getElementsByTagName("A");

  for (i=0;i<tmpArray.length;i++) {

	/* The browser makers didn't find it necessary to implement Node.attributes correctly so we do this instead */
	var hasAttributes = new Boolean (0); //False

	for (a=0;a<requiredAttributes.length;a++) {
	  if (tmpArray[i].getAttribute(requiredAttributes[a]) != "") {
		hasAttributes = 1;
	  }
	}
	
	if (hasAttributes == 0) {
	  linkParent = tmpArray[i].parentNode;
	  linkChildren = tmpArray[i].childNodes;

	  //Place all children before the link
	  for (j=0;j<linkChildren.length;j++) {
		linkParent.insertBefore(linkChildren[j], tmpArray[i]);
	  }

	  //Put the link on death row
	  removeLinks[removeLinks.length] = tmpArray[i];	  
	}
	
  }
  
  //Remove the blind (and now childless) links
  for (i=0;i<removeLinks.length;i++) {
	removeLinks[i].parentNode.removeChild(removeLinks[i]);
  }
  //alert("End: " + cur_func);
}

function ombudsmanden_tinymce_putTitleOnTargetBlank (rootElem)
{
  /*
	Put a title tag on all links with target="_blank" telling users 
	that the link opens in a new window.
   */
  var cur_func = "ombudsmanden_tinymce_putTitleOnTargetBlank";
  //alert("Start: " + cur_func);


  var tmpArray = rootElem.getElementsByTagName("A");

  for (i=0;i<tmpArray.length;i++) {
	var link = tmpArray[i];
	
	if ((link.getAttribute("target") != "") && (link.getAttribute("title") == "")) {
	  link.setAttribute("title", "Siden �bnes i et nyt vindue");
	}
  }
  //alert("End: " + cur_func);
}
