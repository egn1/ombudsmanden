/* menu.js - dropdown menus for standard-compliant browsers.

   Copyright (C) Magenta Aps, 2003, by J�rgen Ulrik B. Krag. Under the GPL.
*/

/*

Image handling functions

*/


function newImage(arg) {
    if (document.images) {
        rslt = new Image();
        rslt.src = arg;
        return rslt;
    }
}

function changeImages() {
    if (document.images && (preloadFlag == true)) {
        for (var i=0; i<changeImages.arguments.length; i+=2) {
            document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
        }
    }
}

var preloadFlag = false;
var preloadImgs = new Array();
var imageRefs = new Array();


preloadImgs[preloadImgs.length] = "/grafik/topmenu/dot.gif";

function preloadImages() {
    if (document.images) {
        for(var i=0; i < preloadImgs.length; i++) {
            imageRefs[i] = newImage(preloadImgs[i]);
        }

        preloadFlag = true;

        if(window.spg_preloadImages) {
            window.spg_preloadImages();
        }
    }
}

function open_close(id1, id2, strOpen, strClose) {
    var elem1 =  document.getElementById(id1);
    if(! elem1) {
        return;
    }

    var display = elem1.style.display;

    var action;


    if(! display || display == 'none') {
        action = 'open';
    }

    // Open/Close the div
    if(action == 'open') {
        elem1.style.display = 'block';
    } else {
        elem1.style.display = 'none';
    }

    if(id2) {
        // Open/Close the open/close button

        // Determine type
        var elem2 = document.getElementById(id2);
        if(elem2) {
            if(elem2.tagName.toLowerCase() == 'img') {
                var src = elem2.src;

                if(action == 'open') {
                    if(strClose) {
                        elem2.src = strClose;
                    } else {
                        elem2.src = src.replace(/(\.[^.]+)$/, "-open$1");
                    }
                } else {
                    if(strOpen) {
                        elem2.src = strOpen;
                    } else {
                        elem2.src = src.replace(/-open(\.[^.]+)$/, "$1");
                    }
                }
            } else {
                if(action == 'open') {
                    if(strClose) {
                        elem2.innerHTML = strClose;
                    } else {
                        elem2.innerHTML = 'Luk';
                    }
                } else {
                    if(strOpen) {
                        elem2.innerHTML = strOpen;
                    } else {
                        elem2.innerHTML = '�ben';
                    }
                }
            }
        }
    }
}

function open_all() {
    var args = window.open_all.arguments;
    var argIndex = 0;
    while(args[argIndex]) {
        var item = args[argIndex];
        if(item.length) {
            var elem = document.getElementById(item[0])
            if(elem && elem.style.display != 'block') {
                open_close(item[0], item[1], item[2], item[3]);
            }
        }
        argIndex += 1;
    }
}

function close_all() {
    var args = window.close_all.arguments;
    var argIndex = 0;
    while(args[argIndex]) {
        var item = args[argIndex];
        if(item.length) {
            var elem = document.getElementById(item[0])
            if(elem && elem.style.display != 'none') {
                open_close(item[0], item[1], item[2], item[3]);
            }
        }
        argIndex += 1;
    }
}

function open_close_all(elem_id, open_func, close_func, strOpen, strClose) {
    var elem =  document.getElementById(elem_id);
    if(! elem) {
        return;
    }

    var action;
    if(elem.tagName.toLowerCase() == 'img') {
        if(elem.src.match(/vis_alle.gif$/) || elem.src == strOpen) {
            action = 'open';
        } else {
            action = 'close';
        }
    } else {
        var tmpHTML = elem.innerHTML + "";
        var strTest = strOpen + "";
        if(tmpHTML.toLowerCase() == 'vis alle' || tmpHTML.toLowerCase() == strTest.toLowerCase()) {
            action = 'open';
        } else {
            action = 'close';
        }
    }

    if(action == 'open') {
        open_func();
    } else{
        close_func();
    }

    if(elem.tagName.toLowerCase() == 'img') {
        if(action == 'open') {
            if(strClose) {
                elem.src = strClose;
            } else {
                elem.src = '/grafik/open_close/skjul_alle.gif';
            }
        } else {
            if(strOpen) {
                elem.src = strOpen;
            } else {
                elem.src = '/grafik/open_close/vis_alle.gif';
            }
        }
    } else {
        if(action == 'open') {
            if(strClose) {
                elem.innerHTML = strClose;
            } else {
                elem.innerHTML = 'Skjul alle';
            }
        } else {
            if(strOpen) {
                elem.innerHTML = strOpen;
            } else {
                elem.innerHTML = 'Vis alle';
            }
        }
    }
}

function OpenWin(url, w, h) {
    if (w == null || w == 0) w = 350;
    if (h == null || h == 0) h = 450;
    features = ('toolbar=0,location=0,directories=0,status=0,'
        +'menubar=0,scrollbars=1,resizable=1,copyhistory=0,'
        +'width='+w+',height='+h);
    window.open (url + '', 'OpenedWin', features)
}

