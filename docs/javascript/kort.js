/* JavaScript functions imported for use in DBSearch when showing the map:

   Copyright (C) 2004, Magenta ApS. Under the GPL.
 */

function newImage(arg) {
  if (document.images) {
    rslt = new Image();
    rslt.src = arg;
    return rslt;
  }
}

function changeImagesArray(array) {
  if (document.images && (preloadFlag == true)) {
    for (var i=0; i<array.length; i+=2) {
      var elt=document.getElementById(array[i]);
      elt.src = array[i+1];
    }
  }
}

function changeImages() {
  changeImagesArray(changeImages.arguments);

  /* Change the relevant drop-down based on the value of 'selected': */
  if (!areaname[selected]) { return }
  var elt=document.getElementById('geografisk');
  if (!elt) { return }
  elt.value=areaname[selected];
}

function toggleImages() {
  for (var i=0; i<toggleImages.arguments.length; i+=2) {
    if (selected == toggleImages.arguments[i]) {
      changeImagesArray(toggleImages.arguments[i+1]);
    }
  }
}

var selected = '';
var preloadFlag = false;
function preloadImages() {
  if (document.images) {
    index_01_nordjylland_sel = newImage("/grafik/dbsearch/index_01-nordjylland_sel.gif");
    index_01_viborg_sel = newImage("/grafik/dbsearch/index_01-viborg_sel.gif");
    index_01_aarhus_sel = newImage("/grafik/dbsearch/index_01-aarhus_sel.gif");
    index_01_ringkoebing_sel = newImage("/grafik/dbsearch/index_01-ringkoebing_sel.gif");
    index_01_vejle_sel = newImage("/grafik/dbsearch/index_01-vejle_sel.gif");
    index_01_ribe_sel = newImage("/grafik/dbsearch/index_01-ribe_sel.gif");
    index_01_soenderjylland_sel = newImage("/grafik/dbsearch/index_01-soenderjylland_sel.gif");
    index_01_fyn_sel = newImage("/grafik/dbsearch/index_01-fyn_sel.gif");
    index_01_storstroem_sel = newImage("/grafik/dbsearch/index_01-storstroem_sel.gif");
    index_01_frederiksborg_sel = newImage("/grafik/dbsearch/index_01-frederiksborg_sel.gif");
    index_01_vestsjaelland_sel = newImage("/grafik/dbsearch/index_01-vestsjaelland_sel.gif");
    index_01_storkoebenhavn_sel = newImage("/grafik/dbsearch/index_01-storkoebenhavn_sel.gif");
    index_01_roskilde_sel = newImage("/grafik/dbsearch/index_01-roskilde_sel.gif");
    index_01_bornholm_sel = newImage("/grafik/dbsearch/index_01-bornholm_sel.gif");
    index_01_groenland_sel=''; /* These are here, false, for easy check in colour_area */
    index_01_faeroeerne_sel='';
    preloadFlag = true;
  }
}

function colour_area(area) {
  if(eval('index_01_'+area+'_sel')) { /* Area selected, show: */
    selected=area;
    changeImages('index_01', '/grafik/dbsearch/index_01-'+area+'_sel.gif');
  }
  else { /* Can't show selected area, show neutral map: */
    selected='';
    changeImages('index_01', '/grafik/dbsearch/index_01.gif');
  }

  return false;
}

function check_dbsearch_submit(form) {
  if(form.data1.value.length<3 &&
     form.data2.value=='' && form.data3.value=='' &&
     form.data4.value=='' && form.data5.value=='') {
    alert('Søgningen kræver ord på 3 bogstaver eller mere for at kunne søge.');
    return false;
  }

  return true;
}

/* New functions for the new search thingie */

function check_dbsearch_submit2(form) {
  if(form.freetext.value.length<3 &&
    form.year.value=='' && form.myndighed.value=='' &&
    form.inspektioner.value=='' && form.geografisk.value=='' &&
    form.sagsnr=='') {
    alert('Søgningen kræver ord på 3 bogstaver eller mere for at kunne søge.');
    return false;
  }

  return true;
}

function select_region(regionname) {
    // Change the bacground image on the dkkort:
    set_dkkort_background(regionname);
    set_dkkort_dropdown_selection(regionname);

    return false;
}

var region_gifs = {
                    'Nordjylland': 'Nordjylland',
                    'Midtjylland': 'Midtjylland',
                    'Syddanmark': 'Syd',
                    'Hovedstaden': 'Hovedstaden',
                    'Sjælland': 'Sjaelland'
                };


function set_dkkort_background(regionname) {
    var dkDiv = document.getElementById('dkkort');
    var gifname = region_gifs[regionname];
    if(gifname) {
        dkDiv.style.background = "url(/grafik/dkkort/Danmark_" + gifname + ".gif)";
    } else {
        dkDiv.style.background = "url(/grafik/dkkort/Danmark.gif)";
    }

    return false;
}

function set_dkkort_dropdown_selection(regionname) {
    var elem = document.getElementById('geografisk');
    for(var i=0;i<elem.options.length;i++) {
        if(elem.options[i].value == regionname) {
            elem.selectedIndex = i;
            return false;
        }
    }
    elem.selectedIndex = 0;
    return false;
}

function region_dropdown_changed(elem) {
    var value = elem.options[elem.selectedIndex].value;
    set_dkkort_background(value);
    return false;
}