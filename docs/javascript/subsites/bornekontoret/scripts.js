//  Naviation javascript for responsive layout
$('.nav h2 button').bind('click', function(){
  var button = $(this);
  var targetElm = $('#' + button.attr('aria-controls'));
  var isExpanded = button.attr('aria-expanded') == 'true'; // attributes are strings, so we need to compare to 'true'
  var parentNav = button.parents('.nav');

  if (isExpanded) { // if the target is expanded, collapse it
    targetElm.addClass('hidden-small'); // add the 'hidden-small' class to hide it from view
    parentNav.removeClass('expanded');
  } else {
    targetElm.removeClass('hidden-small'); //  remove the 'hidden-small' class since we're about to show the target
    parentNav.addClass('expanded');
  }
  button.attr('aria-expanded', !isExpanded); // flip the expanded state of the button
});

// Enable accordion functionality
if ($('.accordion').length > 0) {
    $('.accordion').each(function() {
        $(this).find(':first-child').addClass('acc-trigger');
        var content = $(this).children(':not(.acc-trigger)');
        content.hide();
        $(this).find('.acc-trigger').on('click', function(e) {
            content.toggle('fast');
            $(this).toggleClass('open');
        });
    });
};
