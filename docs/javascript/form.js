$(document).ready(function(){
  // set correct classes of file upload elements on document ready.
  // do this instead of hard-setting them in the markup, so that if we don't have
  // javascript, we can still use the form
  $(".custom-file-fields label").addClass("custom-file");

  // update upload fields when their values change
  $(".custom-file input[type=file]").change(function(){
    var elm = $(this);
    if(elm.val() != "") {
      elm.closest("label").removeClass("empty"); // remove the empty class from containing label

      // add an entry to the file list
      elm.closest(".custom-file-fields").next(".custom-file-list").append(function(){
        return $("<li/>", {
          "data-target":elm.attr("id"),
          html:"<span class=\"filename\">" + elm[0].files[0].name + "</span>\
          <span class=\"filesize\">" + Math.round(elm[0].files[0].size/1000) + " kb" + "</span>\
          <button type=\"button\" class=\"no-styling\"><span class=\"sr-only\">Fjern fil</span><span aria-hidden=\"true\">&times;</span></button>",
        });
      });
    } else {
      elm.closest("label").addClass("empty"); // re-add the empty class
      elm.closest(".custom-file-fields").next(".custom-file-list").find("li[data-target=" + elm.attr("id") + "]").remove(); // remove the corresponding list item
    }
  });

  // clicking a remove button in the file list
  $(".custom-file-list").on("click", "li button", function(){
    var li = $(this).closest("li");
    var id = li.attr("data-target");
    $(document.getElementById(id)).val(""); // reset the actual input item
    $("label[for=" + id + "]").addClass("empty"); // re-add the empty class to the containing label
    setTimeout(function(){
      li.remove(); // destroy the list item, but do it in setTimeout to push it to the end of the thread. We need access to $(this) in the other event handler
    },0);
  });

  // enable unload warnings once an input element has changed
  $("form[name=pageform] input, form[name=pageform] textarea").on("change", function(){
    window.onbeforeunload = function(){
      return true;
    };
  });

  // disable warnings when submitting the form,
  // as the user shouldn't get a warning when they
  // actually want to submit the form
  $("form[name=pageform]").submit(function(){
    window.onbeforeunload = null;
  });

  if($(".form-page").length > 1) { // only attach all these handlers if we actually have pagination

    // set up pagination
    $("<div/>", {
      class: "row-center",
      html: function(){
        return $("<div/>", {
          class: "col-8 pager form-pager",
          html: function(){
            return $("<button/>", {
              class: "prev hidden",
              text: "Forrige"
            }).add($("<div/>", {
              class: "page-numbers",
              html: function(){
                var markup = "";
                for(var i=1;i<=$(".form-page").length;i++) {
                  markup += '<button class="no-styling as-link page-number-' + i + '">' + i + '</button>\
                  '; // we want line breaks in order to space the characters properly
                }
                return markup;
              }
            })).add($("<button/>", {
              class: "next",
              text: "Næste"
            }));
          }
        });
      }
    }).appendTo($(".obviusform"));

    // set up step counter in teaser field inside hero
    $("#page-description").append(function(){
      return $("<div/>", {
        html: function(){
          return "<span class=\"current\">" + 1 + "</span> af " + $(".form-page").length;
        },
        id:"form-page-counter",
      });
    });

    // hide everything but page 1 of the form
    $(".form-page:not(#form-page-1)").addClass("sr-only");

    // attach click handlers to page numbers; these don't change but always go to their specific index
    $(".page-numbers button").click(function(){
      var idx = parseInt($(this).text()); // parse as int just to be sure
      OmbForm.goToFormPage(idx, true);
    });

    // Init next/prev pager buttons by going to page 1
    OmbForm.updatePagerButtons(1);

    // if we have error links on page load, focus on the first error
    $(".error-list > li:first-child > a").focus()

    // if we have error links, clicking these links should also switch
    // pages if appropriate
    $(".error-list li a").click(function(e){
      e.preventDefault();
      var href = $(this).attr("href").substr(1); // remove hashmark from href attr with substr
      var idx = OmbForm.getParentPageIndex($("span[id=" + href + "]"));
      OmbForm.goToFormPage(idx, true);
      $("[id=" + href + "]").focus();
    });

    // Remove the upload event handlers we attached when we didn't yet know whether the form was paginated
    $(".custom-file input[type=file]").off("change.OmbTabIndex");
    $(".custom-file-list").off("click.OmbTabIndex", "li button");

  }
});

// add popstate listener
window.onpopstate = function(evt){
  if(evt.state !== null) { // we might not have a state object if we're returning to the firstmost state
    OmbForm.goToFormPage(evt.state.page, false);
  } else {
    OmbForm.goToFormPage(1, false); // default to page 1
  }
}

var OmbForm = (function(){
  // onpopstate callback fires *after* the actual popping of the state, so we need to keep track of previous state manually
  // https://stackoverflow.com/questions/36525412/can-you-get-the-previous-history-state-object-in-js
  this.prevState = null;
  function goToFormPage(n, doPushState) {
    var prevPage = OmbForm.prevState !== null ? OmbForm.prevState.page : null; // catch null state
    if(n != prevPage) { // only do something if we're going to a new page
      var targetPage = $("#form-page-" + n);
      if (targetPage.length) { // only attempt pagination if we have a target page
        targetPage.removeClass("sr-only"); // show the requested page
        $(window).scrollTop($("form[name=pageform]").offset().top); // scroll to top of form
        $("form[name=pageform] input:visible, form[name=pageform] select:visible, form[name=pageform] textarea:visible").first().focus() // focus on the first visible input
        $(".form-page:not(#form-page-" + n + ")").addClass("sr-only"); // hide other pages
        $("#form-page-counter .current").text(n); // update text in description field in hero

        updatePagerButtons(n);

        if(doPushState) {
          history.pushState({
            page: n
          }, "", "?page-" + n);
        }
        OmbForm.prevState = history.state; // either results in {page: x} or null
      }
    }
  }
  function updatePagerButtons(n) {
    if(n > 1) {
      $(".form-pager .prev").click(function(){
        goToFormPage(n - 1, true);
      }).removeClass('hidden');
    } else {
      $(".form-pager .prev").off("click").addClass('hidden'); // we need to remove old event handlers, otherwise the handler function will execute multiple times
    }
    if(n < $(".form-page").length) {
      $(".form-pager .next").click(function(){
        goToFormPage(n + 1, true);
      }).removeClass('hidden');
    } else {
      $(".form-pager .next").off("click").addClass('hidden');
    }
    $(".page-numbers button").removeClass("disabled"); // re-enable any previously disabled page numbers
    $(".page-number-" + n).addClass("disabled"); // disable the page number for the page we went to
  }
  function getParentPageIndex(elm) {
    var page = elm.closest(".form-page"); // find the page the anchor belongs to
    var idx = $("form[name=pageform] .form-page").index(page); // find the zero-based index of said page among all the pages
    return idx + 1; // add 1 for 1-based value
  }
  return {
    prevState:prevState,
    goToFormPage:goToFormPage,
    updatePagerButtons:updatePagerButtons,
    getParentPageIndex:getParentPageIndex
  }
})();
