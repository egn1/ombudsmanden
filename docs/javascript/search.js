$(document).ready(function(){
  
  // Initialize form with the correct contextual facets displayed
  OmbSearch.setContextFacetDisplay(OmbSearch.getUrlParam('mat_type'));

  // add a class when submitting the form, so that the user can see that a new request has been sent
  $("form[name=fullsearch]").submit(function(){
    $(this).addClass("submitting");
  });

  // reset input field
  $("#search-query button[type=reset]").click(function(){
    $("#query").attr("value", "");
  });

  // Selecting a search facet should display matching contextual facets
  $('#search-facets input[name="mat_type"]').change(function(ev){
    OmbSearch.setContextFacetDisplay(ev.target.value)
  });

  // reset entire form
  $("#reset-all").click(function(){
    // Resetting the search is the same as removing URL parameters. So we can change to the same URL with no query parameters. window.location = window.location.pathname */
    window.location = window.location.pathname;
  });

  // clicking a radio or label in the topic list
  $("#topic-list label").click(function(e){
    e.stopPropagation();
    var parent = $(this).parent();

    OmbSearch.selectTaxonomyWord(parent);
  }).keypress(function(e){ // also manually bind space key action as this is not a radio
    if(e.which == 32) {
      e.preventDefault();
      $(this).trigger("click");
    }
  });;

  // submitting a new query with currently selected topic
  $("#search-topic-use").click(function(){
    $("form[name=fullsearch]").submit();
  });

  // resetting current topic choice
  $("#search-topic-reset").click(function(){
    $("#topic-list").find(".selected, .unselected").removeClass("selected unselected"); // clear all choices
    $("#topic-list input[type=radio]").prop("checked", false); // also clear actual radioes

    // hide the topic type heading
    $("#search-topictype").hide();

    OmbSearch.updateTaxonomyQuery(); // reset the topic breadcrumb

    //$("#search-topic-toggle").trigger("click"); // close the topic browser

    $("form[name=fullsearch]").submit(); // submit a new query, because otherwise facet counts on level 2 facets will not be correct
  });

  // toggling the topic selector
  $("#search-topic-toggle").click(function(){
    $("#search-topic-navbar").toggleClass("open"); // toggle open state on navbar
    var wrap = $("#wrapper-search-topic");
    var topics = $("#search-topic");
    var dur = Omb.secondsToMillis(topics.css("transition-duration"));

    Omb.toggleAriaExpanded(this)
    Omb.updateAccordion(wrap, topics, "toggle", false);

    setTimeout(function(){
      //wrap.toggleClass("overflow-visible"); // we need to toggle overflow because otherwise the contents of topic box would be invisible
      //topics.css("height", ""); // remove the height attr so we can have dynamic height
      $("#search-topic-actions").toggleClass("open"); // toggle "open" state of actions once animation is complete
    }, wrap.hasClass("transition") ? dur : 0);

    // toggle the event handler for clicking a <li> in the topic breadcrumb; we should not be able to trigger an update of the taxonomy query if the topic browser is closed
    if(wrap.hasClass("open")) {
      $("#topics-breadcrumb").on("click", "li", OmbSearch.breadcrumbHandler);
      $("#topics-breadcrumb button").prop("disabled", 0);
    } else {
      $("#topics-breadcrumb").off("click", "li", OmbSearch.breadcrumbHandler);
      $("#topics-breadcrumb button").prop("disabled", 1);
    }
  });

  // handle view mode when switching between grid and list view in search results
  $(".preview-mode-selector-grid").click(function(){
    document.forms.fullsearch.viewmode.value = "grid";
    $("form[name=fullsearch]").submit();
  });
  $(".preview-mode-selector-list").click(function(){
    document.forms.fullsearch.viewmode.value = "list";
    $("form[name=fullsearch]").submit();
  });
});

var OmbSearch = (function(){

  function setContextFacetDisplay(facet) {
    setElementVisibility($("#wrapper-aarstal"), function() {
      return (facet !== 'Alt' && facet !== false)
    })
    setElementVisibility($("#wrapper-myndighed"), function() {
      return facet === 'Udtalelser'
    })
    setElementVisibility($("#wrapper-inspektionstype"), function() {
      return facet === 'Tilsyn'
    })
    setElementVisibility($("#wrapper-inspektionsomraade"), function() {
      return facet === 'Tilsyn'
    })
    setElementVisibility($("#wrapper-sagsomraade"), function() {
      return facet === 'Overblik'
    })
    setElementVisibility($("#wrapper-forvretemne"), function() {
      return facet === 'Overblik'
    })
  }

  function setElementVisibility(elmnt, callback) {
    var selectEl = elmnt.find('select').first();
    if (callback()) {
      elmnt.css('display', 'inline-block');
      selectEl.attr('disabled', false);
    } else {
      elmnt.css('display', 'none');
      selectEl.attr('disabled', true);
    };
  }

  function getUrlParam(param) {
    var params = window.location.search.split('&');
    var prm = params.filter(function(p) {
      return p.indexOf(param) !== -1;
    });
    if (prm.length === 1) {
      var val = prm[0].split('=')[1];
      return val;
    } else {
      return false;
    };
  }

  function selectTaxonomyWord(targ){
    var notTarg = targ.siblings().find("li").addBack().add(targ.parents("li").siblings().find("li").addBack()); // siblings to the current choice, children to those siblings, siblings to the ancestors of the current choice, and children of these ancestor siblings.
    notTarg.addClass("unselected").removeClass("selected"); // add the unselected class and remove the selected class to these elements to handle visibility
    notTarg.children("label").children("input[type=radio]").prop("checked", false); // also handle the actual radioes of these elements

    var lineage = targ.parents("li").addBack(); // the lineage of the current choice is its parents plus itself (hence the addBack())
    lineage.addClass("selected").removeClass("unselected"); // select the current choice along with lineage
    lineage.children("label").children("input[type=radio]").prop("checked", true); // handle the actual radio of the element

    var children = targ.find("li");
    children.removeClass("selected unselected"); // clear any settings on children of current choice, so we're ready for next selection
    children.children("label").children("input[type=radio]").prop("checked", false); // also handle the actual radioes of these children

    // update the topic type heading
    $("#search-topictype").show().text(targ.children("label").text());

    updateTaxonomyQuery();

    $("html, body").animate({
      scrollTop: Math.floor($("#topic-browser").offset().top) // scroll back up to the top of the topic explorer
    }, 150);
  }
  function updateTaxonomyQuery(){
    $("#topics-breadcrumb").html(""); // reset old breadcrumb
    var isOpen = $("#search-topic-navbar").hasClass("open");
    $("#topic-list li.selected:not(.col-4)").each(function(){ // for the pathway down the tree, push an element to the array corresponding to the respective node. we're not interested in the letters which are purely representational
      var node = $(this);
      var id = node.attr("id");
      var text = node.children("label").text();
      $("#topics-breadcrumb").find("ul").addBack().last().append( // append to the last found <ul> so we get proper nesting
        $("<li/>", {
          html: function(){
            return "<button class=\"no-styling\"" + (isOpen ? "" : " disabled") + ">" + text + "<span class=\"sr-only\"> Vælg dette emne og fravælg alle dets underemner</span></button><ul></ul>"; // append an extra <ul></ul> to make the correct nesting
          },
          "data-target":id
        })
      );
    });
  }
  function breadcrumbHandler(e) {
    e.stopPropagation(); // stop bubbling because otherwise we'll propagate to the outermost li, which is the first selection the user made.
    var id = $(this).attr("data-target");
    var targ = $(document.getElementById(id));

    selectTaxonomyWord(targ);
  }
  function updateContextualFacets(val, toggleVisibility, handleTopicBrowser, handleSearchHeading){
    if(toggleVisibility) $("select[name=aarstal], select[name=myndighed], select[name=inspektionstype], select[name=inspektionsomraade], select[name=forvretemne], select[name=sagsomraade]").closest(".custom-select").css("display", ""); // reset any previously changed dropdowns
    document.forms.fullsearch.aarstal.value = ""; // always reset aarstal form value, but keep the field visible (select[name=aarstal]) is not included in the selector above
    if(val == "Udtalelser"){
      document.forms.fullsearch.inspektionstype.value = ""; // reset inspektionstype
      document.forms.fullsearch.inspektionsomraade.value = ""; // reset inspektionsomraade
      document.forms.fullsearch.forvretemne.value = ""; // reset forvaltningsretligt emne
      document.forms.fullsearch.sagsomraade.value = ""; // reset specifikt sagsområde
      if(toggleVisibility) $("select[name=aarstal], select[name=myndighed]").closest(".custom-select").css("display", "inline-block");
      if(handleTopicBrowser) $("#topic-browser").show();
    } else if(val == "Tilsyn"){
      document.forms.fullsearch.myndighed.value = ""; // reset myndighed
      document.forms.fullsearch.forvretemne.value = ""; // reset forvaltningsretligt emne
      document.forms.fullsearch.sagsomraade.value = ""; // reset specifikt sagsområde
      if(toggleVisibility) $("select[name=aarstal], select[name=inspektionstype], select[name=inspektionsomraade]").closest(".custom-select").css("display", "inline-block");
    } else if(val == "Overblik"){
      document.forms.fullsearch.myndighed.value = ""; // reset myndighed
      document.forms.fullsearch.inspektionstype.value = ""; // reset inspektionstype
      document.forms.fullsearch.inspektionsomraade.value = ""; // reset inspektionsomraade
      if(toggleVisibility) $("select[name=aarstal], select[name=forvretemne], select[name=sagsomraade]").closest(".custom-select").css("display", "inline-block");
    } else {
      document.forms.fullsearch.myndighed.value = ""; // reset myndighed
      document.forms.fullsearch.inspektionstype.value = ""; // reset inspektionstype
      document.forms.fullsearch.inspektionsomraade.value = ""; // reset inspektionsomraade
      document.forms.fullsearch.forvretemne.value = ""; // reset forvaltningsretligt emne
      document.forms.fullsearch.sagsomraade.value = ""; // reset specifikt sagsområde
    }

    if(handleTopicBrowser) {
      if(val != "Udtalelser") {
        $("#topic-browser").hide(); // hide topic browser
        $("#search-topic-reset").trigger("click"); // reset topic choices
        $("#wrapper-search-topic").removeClass("open overflow-visible"); // reset the topic wrapper
        $("#search-topic").css("height", ""); // reset the calculated height
        $("#search-topic-navbar").removeClass("open"); // reset accordion
      }
    }

    if(handleSearchHeading) {
      var heading = $("#search-doctype");
      if(val != "Alt") { // toggle the doctype heading
        var height = heading[0].scrollHeight + "px";
        $("#search-doctype-type").text(val.toLowerCase());
        heading.addClass("visible");
        heading.css("height", height);
      } else {
        heading.css("height", "");
        heading.removeClass("visible");
      }
    }
  }
  return {
    selectTaxonomyWord:selectTaxonomyWord,
    updateTaxonomyQuery:updateTaxonomyQuery,
    breadcrumbHandler:breadcrumbHandler,
    updateContextualFacets:updateContextualFacets,
    setContextFacetDisplay: setContextFacetDisplay,
    getUrlParam: getUrlParam
  }
})();
