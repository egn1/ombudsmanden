%if($show_box) {
(function() {
    var cookietest = function() {
        var div = document.createElement('div');
        div.id = 'cookie_box';
        if ( window.cookie_message_lang == 'da' ) {
            div.innerHTML = '' +
              '<h2>Accept af cookies fra www.ombudsmanden.dk</h2>' +
              '<p>Vi bruger cookies på www.ombudsmanden.dk. Vi bruger cookies til webstatistik, hvis du afspiller en film eller tilmelder dig vores nyhedsbrev. ' + 
              '<a class="laes_mere_cookies btn" href="http://www.ombudsmanden.dk/omhjemmesiden/cookies/">Læs mere om cookies på www.ombudsmanden.dk</a></p>' +
              '<a class="accept_cookies btn primary-btn a-button" href="javascript:void(0)" onclick="accept_ombud_cookies()">Accepter cookies</a>';
        } else {
            div.className = 'english';
            div.innerHTML = '' +
              '<h2>Accept of cookies from ombudsmanden.dk</h2>' +
              '<p>We use cookies at www.ombudsmanden.dk. We use cookies for site analytics, when you click on a film or subscribe to our newsletter. ' +
              '<a class="laes_mere_cookies btn" href="http://en.ombudsmanden.dk/cookies/">Read more about cookies on www.ombudsmanden.dk</a></p>' +
              '<a class="accept_cookies btn primary-btn a-button" href="javascript:void(0)" onclick="accept_ombud_cookies()">Accept cookies</a>';
        }
        var body = document.getElementsByTagName('body')[0];
        body.insertBefore(div, body.firstChild);
    };
    if(document.addEventListener) {   // Mozilla, Opera, Webkit are all happy with this
	document.addEventListener("DOMContentLoaded", function() {
	    document.removeEventListener( "DOMContentLoaded", arguments.callee, false);
	    cookietest();
	}, false);
    }
    else if(document.attachEvent) {   // IE is different...
	document.attachEvent("onreadystatechange", function() {
	    if(document.readyState === "complete") {
		document.detachEvent("onreadystatechange", arguments.callee);
		cookietest();
	    }
	});
    }

})();

function accept_ombud_cookies() {
    var div = document.getElementById('cookie_box');
    div.parentNode.removeChild(div);

    var scripts = document.getElementsByTagName('script');
    var newsrc;
    for(var i=0;i<scripts.length;i++) {
        if((scripts[i].src || '').match(/\/system\/cookies.js$/)) {
            newsrc = scripts[i].src + "?set=1";
            break;
        }
    }
    if(newsrc) {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.src = newsrc;
        document.getElementsByTagName('head')[0].appendChild(s);
    }
    return false;
}
%}
<%once>
use CGI::Cookie;
</%once>
<%init>
my $show_box = !($ENV{HTTP_COOKIE} =~ m!accept_cookies\s*=\s*yes!);
if($r->param('set')) {
    my $cookie = new CGI::Cookie(
        -name => "accept_cookies",
        -value => "yes",
        -expires => "+12M",
        -path => "/"
    );
    $cookie->bake($r);
    $show_box = 0;
}
$r->content_type('text/javascript');
$r->no_cache(1) if($show_box);
</%init>
<%filter>
$r->set_content_length(length($_));
</%filter>
