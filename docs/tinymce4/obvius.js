(function(win, $) {
    var current_callback = null;

    // Call the editors callback whenever the navigator result field is changed
    $('#obvius_tinymce4_navigator_result').on("navigatorselect", function(a, b, c) {
        if(current_callback) {
            current_callback($(this).val());
            current_callback = null;
        }
    });

    function start_navigator(field_name, url, type, arg, options, win, window_callback) {
        var start_url = tinyMCE.activeEditor.getParam('document_base_url');

        // Make URL relative to /
        start_url = start_url.replace(/^https?:\/\/[^\/]+/, "");

        // If running https try to start the navigator under the current URI
        var uri = document.location.href.match(/https/) ? start_url : '/';

        if(url && url.charAt(0) == '/') {
            start_url = url;
            // Remove query string:
            start_url = start_url.replace(/\?.*$/, "");
            // Add last /:
            if(! start_url.match(/\/$/)) {
                start_url += "/";
            }
        }

        var doctype_extra = '';
        if(type == 'image') {
            doctype_extra = "&doctype=Image&prefix=/admin";
        }

        /* The win argument - window object of the opening window - is
           optional for compability with the previous use of Tiny MCE in
           Obvius: */
        if (!win) {
            win=window;
        }

        var obvius_navigator_argument = (
            "?obvius_app_ruby_navigator=1" +
            doctype_extra +
            '&fieldname=' +
            field_name +
            "&path=" + start_url
        );

        url = "/admin" + uri + obvius_navigator_argument;

        win = win.open(url, '', options);
        if(window_callback)
            window_callback(win);

        return false;

    }

    window.obvius_tinymce4_filepicker = function(callback, value, meta) {
        current_callback = callback;
        start_navigator(
            'obvius_tinymce4_navigator_result',
            value,
            meta.filetype,
            'obvius_app_navigator=1',
            'width=700, height=432, status=yes, scrollbars=1, resizable=1',
            window
        );
    };

})(window, jQuery);
