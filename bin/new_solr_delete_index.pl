#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use lib '/var/www/www.ombudsmanden.dk/perl';
use lib '/var/www/obvius/perl';

use Obvius::Config;
use LWP::UserAgent;
use Data::Dumper;
use JSON;

my $config = Obvius::Config->new('ombud');

my $solr_port = $config->param('solr_port') || 8983;
my $solr_host = $config->param('solr_host') || 'localhost';
my $solr_core = $config->param('solr_core') || 'ombud2017';

my $solr_base_url = "http://${solr_host}:${solr_port}/solr/${solr_core}";

my $response = LWP::UserAgent->new()->post(
    $solr_base_url . '/update?commit=false',
    'Content-Type' => "application/json",
    Content => '{ "delete": { "query": "*:*" }}'
);

my $result = eval { JSON::from_json($response->content) || {} };
my $status = $result->{responseHeader}->{status};
if(!defined($status) || $status != 0) {
    print STDERR "Something went wrong: " . $response->content, "\n";
}
