#!/usr/bin/perl

use strict;
use warnings;

use lib '/var/www/obvius/perl';
use lib '/var/www/www.ombudsmanden.dk/perl';

use Obvius;
use Obvius::Config;
use MIME::Base64;

my %type2extension = (
    "application/pdf" => "pdf",
    "image/png" => "png",
    "image/jpeg" => "jpg",
    "image/pjpeg" => "jpeg",
    "application/x-zip-compressed" => "zip",
);

my $obvius = Obvius->new(Obvius::Config->new($ARGV[0] || "ombud"));

my $upload_id = $obvius->get_doctype_by_name("Upload")->Id;
my $fileupload_doctype = $obvius->get_doctype_by_name("FileUpload");
my $fileupload_id = $fileupload_doctype->Id;

my $finder = $obvius->dbh->prepare(q|
    select
        versions.docid docid,
        versions.version version,
        vf_data.text_value value,
        documents.name name,
        vf_mimetype.text_value mimetype
    from
        documents
        join versions on (
            documents.id = versions.docid
        )
        left join vfields vf_data on (
            versions.docid = vf_data.docid
            and
            versions.version = vf_data.version
            and
            vf_data.name = 'uploaddata'
        )
        left join vfields vf_mimetype on (
            versions.docid = vf_mimetype.docid
            and
            versions.version = vf_mimetype.version
            and
            vf_mimetype.name = 'mimetype'
        )
    where
        versions.type = ?
    order by
        versions.docid, versions.version
|);

my $deleter = $obvius->dbh->prepare(q|
    delete from
        vfields
    where
        name = 'uploadfile'
        and
        docid = ?
        and
        version = ?
|);

my $inserter = $obvius->dbh->prepare(q|
    insert into vfields
        (docid, version, name, text_value)
    values
        (?, ?, 'uploadfile', ?)
|);

my $version_updater = $obvius->dbh->prepare(q|
    update
        versions
    set
        type = ?
    where
        docid = ?
        and
        version = ?
|);

$finder->execute($upload_id);
while(my $rec = $finder->fetchrow_hashref) {
    my $data = '';
    eval {
        $data = decode_base64($rec->{value});
    };
    if($@) {
        warn $@;
    }
    open(my $fh, '<', \$data);
    my $mimetype = $rec->{mimetype} || 'application/octet-steam';
    my $name = $rec->{name};
    # If filename does not have an extension, give it one before saving to disk
    if(($name !~ m{[.]\w+$}) and $type2extension{$mimetype}) {
        $name = $name . "." . $type2extension{$mimetype};
    }
    my $value = $fileupload_doctype->place_file_in_upload(
        $obvius,
        $fh,
        $mimetype,
        $name
    );
    print STDERR sprintf(
        '%s, %s => %s',
        $rec->{docid},
        $rec->{version},
        $value
    ), "\n";
    # Delete any existing uploadfile field
    $deleter->execute($rec->{docid}, $rec->{version});
    # Insert new uploadfile field
    $inserter->execute($rec->{docid}, $rec->{version}, $value);
    # Change the version type
    $version_updater->execute($fileupload_id, $rec->{docid}, $rec->{version});
}