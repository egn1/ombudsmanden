#!/usr/bin/perl

use strict;
use warnings;

use locale;
use utf8;
use Encode;

use lib '/var/www/obvius/perl';
use lib '/var/www/www.ombudsmanden.dk/perl';

use Data::Dumper;
use Obvius; use Obvius::Config;
use Ombudsmanden::Hovedreg qw(hreg_ids_to_catpaths);
use HTML::Parser; use XML::Simple; use MIME::Base64; use JSON;
use LWP::UserAgent; use HTTP::Request; use HTTP::Request::Common;
use Obvius::CharsetTools qw(mixed2utf8);
use Obvius::Hostmap;
use Getopt::Long;
use File::Temp;
use MIME::Base64;

my %fieldname_map = (
    aftersubmittext => "htmlbody",
    align => "noindex",
    answer => "htmlbody",
    author => "textmeta",
    bar_width => "noindex",
    base => "noindex",
    box1_content => "htmlbody",
    box1_title => "title3",
    box1_type => "noindex",
    box2_content => "htmlbody",
    box2_title => "title3",
    box2_type => "noindex",
    box3_content => "htmlbody",
    box3_title => "title3",
    box3_type => "noindex",
    box4_content => "htmlbody",
    box4_title => "title3",
    box4_type => "noindex",
    captcha => "noindex",
    cat_hoved => "main_category",
    cat_id => "noindex",
    close_message => "htmlbody",
    contactinfo => "textmeta",
    content => "htmlbody",
    contributors => "textmeta",
    correctanswer => "noindex",
    data => "noindex",
    disable_cache => "noindex",
    display_myndighed => "noindex",
    docdate => "f_docdate",
    docref => "textmeta",
    doctype => "noindex",
    editcomp => "noindex",
    email => "textmeta",
    email_subject => "textmeta",
    email_text => "htmlbody",
    enddate => "date",
    entries_for_advert => "noindex",
    entries_for_close => "noindex",
    eventinfo => "htmlbody",
    eventplace => "textmeta",
    eventtime => "textmeta",
    eventtype => "textmeta",
    excerpt => "f_excerpt",
    expires => "noindex",
    fields => "noindex",
    form => "htmlbody",
    formdata => "noindex",
    formtext => "htmlbody",
    forvret_emne => "f_forvret_emne",
    front_dura => "noindex",
    front_prio => "noindex",
    geografisk => "f_geografisk",
    gridcontent => "gridcontent",
    height => "noindex",
    hero_align => "noindex",
    hero_button_path => "noindex",
    hero_button_text => "noindex",
    hero_display => "noindex",
    hero_picture_path => "noindex",
    hovedreg => "noindex",
    html_content => "htmlbody",
    html_link => "noindex",
    inst_type => "f_inst_type",
    instution_type => "textmeta",
    in_subscription => "noindex",
    keyword => "xref",
    language => "noindex",
    legal_code => "textmeta",
    levels => "noindex",
    logo => "noindex",
    mailfrom => "noindex",
    mailmsg => "htmlbody",
    mailto => "textmeta",
    mat_type => "f_mat_type",
    mimetype => "noindex",
    myndighed => "f_myndighed",
    name_prefix => "noindex",
    newcomp => "noindex",
    newsauthorpic => "noindex",
    newspic => "noindex",
    new_window => "noindex",
    nyeste_date => "date",
    nyhedsresume => "htmlbody",
    nyhedstitel => "title1",
    pagesize => "noindex",
    passwdmsg => "noindex",
    pdf_link => "noindex",
    picture => "noindex",
    published => "date",
    publish_mode => "noindex",
    publish_on => "noindex",
    question => "htmlbody",
    requireallanswers => "noindex",
    rets_link => "textmeta",
    rightboxes => "noindex",
    root => "noindex",
    sagsnr => "f_sagsnr",
    sagstype => "textmeta",
    search_expression => "noindex",
    search_type => "noindex",
    sec => "noindex",
    section_news => "noindex",
    seq => "noindex",
    s_event_contact => "textmeta",
    s_event_info => "textmeta",
    s_event_order_by => "noindex",
    s_event_path => "noindex",
    s_event_place => "textmeta",
    s_event_title => "textmeta",
    s_event_type => "textmeta",
    short_title => "title1",
    show_as => "noindex",
    show_date => "noindex",
    show_event => "noindex",
    show_news => "noindex",
    show_new_titles => "noindex",
    show_searchdoc_date => "noindex",
    show_searchdoc_teaser => "noindex",
    show_searchdoc_url => "noindex",
    show_subdoc_date => "noindex",
    show_subdocs => "noindex",
    show_subdoc_teaser => "noindex",
    show_teaser => "noindex",
    show_teasers => "noindex",
    show_title => "noindex",
    show_toc => "noindex",
    show_urls => "noindex",
    size => "noindex",
    sortorder => "noindex",
    source => "textmeta",
    spec_sagsomr => "f_spec_sagsomr",
    startdate => "date",
    subscribeable => "noindex",
    subscribe_include => "noindex",
    table => "textmeta",
    teaser => "htmlbody",
    teksttype => "noindex",
    title => "f_title",
    toppic => "noindex",
    twitter_card => "noindex",
    twitter_description => "textmeta",
    twitter_site => "noindex",
    twitter_title => "title1",
    uploaddata => "uploaddata",
    uploadfile => "uploadfile",
    url => "textmeta",
    vote_option => "noindex",
    where => "noindex",
    width => "noindex",
);

my %doctypes_map = (
    Base => 0,
    BeretningsOversigt => 1,
    Calendar => 1,
    CalendarEvent => 1,
    ComboSearch => 1,
    CreateDocument => 0,
    DBSearch => 0,
    FileUpload => 1,
    Find => 0,
    Form => 1,
    Forside => 1,
    Gridside => 1,
    HovedregisterForside => 1,
    HTML => 1,
    Image => 0,
    InspektionsOversigt => 1,
    KategoriDokument => 1,
    KeywordSearch => 1,
    Link => 1,
    MailData => 0,
    MultiChoice => 1,
    NavigationsSide => 1,
    NyesteSager => 1,
    OrderForm => 1,
    Quiz => 1,
    QuizQuestion => 1,
    RSSFeed => 1,
    Sag => 1,
    SagsSoegning => 1,
    Search => 0,
    Sitemap => 1,
    SpecifikkeSagsomraader => 1,
    Standard => 1,
    Stikord => 1,
    Stikordsregister => 1,
    SubDocuments => 1,
    Subscribe => 1,
    TableList => 1,
    Upload => 1,
);

my $opt_do_special_subtrees = 1;

my %special_subtree_handling = (
    "/da/find/udtalelser/beretningssager/alle_bsager/" => [ f_mat_type => ["Udtalelser"] ],
    "/find/udtalelser/beretningssager/alle_bsager/" => [ f_mat_type => ["Udtalelser"] ],
    "/da/find/nyheder/alle/" => [ f_mat_type => ["Nyheder"] ],
    "/find/nyheder/alle/" => [ f_mat_type => ["Nyheder"] ],
    "/da/find/inspektioner/alle_inspektioner/" => [ f_mat_type => ["Tilsyn"] ],
    "/find/inspektioner/alle_inspektioner/" => [ f_mat_type => ["Tilsyn"] ]
);

my $docs_dir;
my @empty_result = ("htmlbody" => []);

sub slurp_file {
    my ($filename) = @_;

    my $data = '';
    eval {
        local $/ = undef;
        open(FH, $filename) or die "Could not open $filename";
        binmode(FH);
        $data = <FH>;
        close(FH);
    };
    if($@) {
        warn "Error while slurping file: $@";
    }
    
    return $data;
}

my %converters = (
    gridcontent => sub {
        my ($fieldname, $value_array, $doc, $vdoc) = @_;

        return (htmlbody => [map { fix_gridcontent($_) } @$value_array]);
    },
    uploaddata => sub {
        my ($fieldname, $value_array, $doc, $vdoc) = @_;
        my $value = shift(@$value_array);
        if(!$value) {
            return @empty_result;
        }
        eval {
            # Base64 decode from database value
            $value = decode_base64($value);
        };
        if($@) {
            warn("Error while decoding base64 data from database: $@");
            return @empty_result;
        }
        my $mimetype = $vdoc->field('mimetype') || '';
        if($mimetype =~ m{^text/}) {
            return (htmlbody => [$value]);
        } elsif($mimetype eq 'application/pdf') {
            my ($fh, $in_filename) = File::Temp::tempfile(SUFFIX => '.pdf');
            binmode($fh);
            print $fh $value;
            close($fh);
            my ($fh2, $out_filename) = File::Temp::tempfile(SUFFIX => '.html');
            close($fh2);

            system(
                '/usr/bin/pdftohtml',
                '-q', '-i', '-noframes',
                $in_filename,
                $out_filename
            ) if -x "/usr/bin/pdftohtml";

            my $data = slurp_file($out_filename);
            unlink($in_filename);
            unlink($out_filename);
            # Replace nonbreaking spaces with normal space for nicer
            # highlighting.
            $data =~ s{&#160;}{ }g;
            return (htmlbody => [$data]);
        }

        return @empty_result;
    },
    uploadfile => sub {
        my ($fieldname, $value_array, $doc, $vdoc) = @_;

        my $value = shift(@$value_array);
        if(!$value) {
            return @empty_result;
        }
        $value =~ s{^\s+|\s+$}{}g;
        my $file_path = $docs_dir . $value;
        if(!-f $file_path) {
            return @empty_result;
        }
        my $mimetype = $vdoc->field('mimetype') || '';
        if($mimetype =~ m{^text/}) {
            my $data = slurp_file($file_path);
            return (htmlbody => [$data]);
        } elsif($mimetype eq 'application/pdf') {
            my ($fh, $tmp_filename) = File::Temp::tempfile(SUFFIX => '.html');
            close($fh);
            system(
                '/usr/bin/pdftohtml',
                '-q', '-i', '-noframes',
                $file_path,
                $tmp_filename
            );
            my $data = slurp_file($tmp_filename);
            unlink($tmp_filename);
            # Replace nonbreaking spaces with normal space for nicer
            # highlighting.
            $data =~ s{&#160;}{ }g;
            return (htmlbody => [$data]);
        }

        return @empty_result;
    },
    xref => sub {
        my ($fieldname, $value_array, $doc, $vdoc) = @_;

        return ("textmeta" => [
            map { $_->param('value') || $_->param('name') } @$value_array
        ]);
    },
);

my ($obvius, $hostmap, $add_to_docids) = (undef, undef, 0);
my $respprint = 0;

sub get_all_public_docids {
    my ($low_docid, $high_docid) = @_;

    my @conds = ("versions.public = 1");
    my @args;
    if(defined($low_docid)) {
        push(@conds, "versions.docid >= ?");
        push(@args, $low_docid);
    }
    if(defined($high_docid)) {
        push(@conds, "versions.docid <= ?");
        push(@args, $high_docid);
    }

    my @exclude_doctypes = grep { !$doctypes_map{$_} } keys %doctypes_map;
    push(@conds,
        "doctypes.name not in (" .
            join(",", map { "?" } @exclude_doctypes) .
        ")"
    );
    push(@args, @exclude_doctypes);


    my $conds = join(" AND ", @conds);


    my $sth = $obvius->dbh->prepare(qq|
        select versions.docid, LOWER(path)
        from versions
        join docid_path on versions.docid = docid_path.docid
        join doctypes on versions.type = doctypes.id
        where ${conds}
        order by path
    |);
    $sth->execute(@args);
    my @result;
    my $hidden_path = '';
    while(my ($docid, $path) = $sth->fetchrow_array) {
        next if($hidden_path and
                substr($path, 0, length($hidden_path)) eq $hidden_path);
        my $doc = $obvius->get_doc_by_id($docid);
        if($obvius->is_public_document($doc)) {
            $hidden_path = '';
            push(@result, $docid);
        } else {
            $hidden_path = $path;
        }
    }
    return @result;
}

sub json_all_public_docids {
    return join("\n",
        "[",
        (
            map {
               sprintf('    {"docid": %d}', $_)
            } get_all_public_docids(@_)
        ),
        "]"
    ) . "\n";
}

sub map_value { my($key,$val) = @_;
    if(!ref $val) {
        return ($key, mixed2utf8($val));
    } elsif(ref($val) eq "ARRAY") {
        my @res = map { (map_value(undef, $_))[1] } @$val;
        return($key, [ @res ]);
    }
    return ();
}

sub gridcontent_fmap {
    my($gc, $func) = @_;

    if(ref($gc) eq "ARRAY") {
        for my $e (@{$gc}) {
            gridcontent_fmap($e, $func);
        }
    } elsif(ref($gc) =~ /HASH/) {
        for my $k (keys %{$gc}) {
            if($k =~ /content/ or $k =~ /title/) {
                $func->($gc->{$k}."\n");
            } else {
                gridcontent_fmap($gc->{$k}, $func);
            }
        }
    }
}

sub fix_gridcontent { my($jsonstr) = @_;
	return undef unless defined $jsonstr and $jsonstr ne "";
	my $html = "";
	my $appf = sub { $html .= $_[0]; };
	my $jsonobj;
	eval { $jsonobj = decode_json($jsonstr); };
	if($@) {
		die "parsehtml: jsonstr '$jsonstr' does not decode: $@";
	} 
	gridcontent_fmap($jsonobj, $appf);
	return $html;
}

sub get_public_url {
    my ($uri) = @_;

    my $url = $hostmap->translate_uri($uri, ":bogus:");

    return $url;
}
sub reref { my($s) = @_;
	return $s if ref $s;
	return [ $s ];
}

sub checkaddtoparent {
    my($d) = @_;
    my $parent = undef;
    my $r = $d;
    my $docparent = $d->{parent};
    if (($d->{doctype_name} eq "Upload" or $d->{doctype_name} eq "FileUpload") && (!($d->{k_uri} =~ m{/da/ombudsmandensarbejde/skatteomraadet}))) {
        # Special case for UploadDocuments that are not under /da/ombudsmandensarbejde/skatteomraadet (see #46115);
        # do not create an SOLR entry for the document, but instead add data to parent document by applying the parent's id
        $r = {
            id => $d->{parent},
            protohtmlbody => { add => [ @{reref $d->{htmlbody}} ] },
            prototextbody => { add => [ @{reref $d->{textbody}} ] },
        };
        $parent = $docparent;
    }
    else {
        $r->{prototextmeta} = $r->{textmeta};
        $r->{prototextbody} = $r->{textbody};
        $r->{protohtmlbody} = $r->{htmlbody};
        delete $r->{textmeta};
        delete $r->{textbody};
        delete $r->{htmlbody};
    }
    delete $r->{parent};
    return ($r, $parent);
}

sub get_doc_data { my($docid) = @_;
    my $result = {};

    my $doc = $obvius->get_doc_by_id($docid);
    $result->{docid} = $docid;
    $result->{parent} = $doc->Parent;

    my $doc_uri = $obvius->get_doc_uri($doc);
    $result->{k_uri} = $doc_uri;
    if($opt_do_special_subtrees) {
        for my $pf (keys %special_subtree_handling) {
            if(substr($doc_uri,0,length($pf)) eq $pf and $doc_uri ne $pf) {
                my($k,$v) = @{$special_subtree_handling{$pf}};
                $result->{$k} = [@$v];
            }
        }
    }

    my $doc_url = get_public_url($doc_uri);
    $result->{k_url} = $doc_url;

    my $vers = $obvius->get_public_version($doc); 

    my @data = ();
    if($vers) { 
        $obvius->get_version_fields($vers,2000,undef);
        my $fields = $vers->param('fields');
        $result->{doctype_id} = $vers->param('type');
        if(my $dt = $obvius->get_doctype_by_id($vers->param('type'))) {
            $result->{doctype_name} = $dt->param('name');
        }

        my @catpath = hreg_ids_to_catpaths(@{ $fields->{CAT_HOVED} } );

        $result->{k_publyear} = substr($fields->{DOCDATE}||'',0,4);
        # FOR TEST
        #$result->{k_cat_hoved} = [ @{$fields->{CAT_HOVED}} ];

        for my $k (sort map { lc($_) } keys %$fields) {
            my $destfield = $fieldname_map{$k};
            if(!$destfield) {
                warn "No destination field defined for field $k";
                next;
            }
            if($destfield eq 'noindex') {
                next;
            }
            my $value = $fields->param($k);
            if(!defined($value)) {
                $value = [];
            }
            if(ref($value) ne "ARRAY") {
                $value = [$value];
            }
            my $converter = $converters{$destfield};
            if($converter) {
                ($destfield, $value) = $converter->($k, $value, $doc, $vers);
            }

            my $dest_array = $result->{$destfield};
            if(!$dest_array) {
                $dest_array = $result->{$destfield} = [];
            }

            push(@$dest_array, grep {$_} @$value);
        }
        $result->{"k_site"} = $obvius->config->param('sitename');
        $result->{"k_catpath"} = [ @catpath ];
	
        # Make a copy of htmlbody that is stripped of HTML tags
        my @textbody;
        foreach my $html (@{$result->{htmlbody} || []}) {
            my $text = $html;
            $text =~ s{<[^>]+>}{}g;
            push(@textbody, $text);
        }
        $result->{textbody} = \@textbody;

        # Remove empty values, unpack single-values and make sure everthing
        # is utf-8.
        foreach my $k (keys %$result) {
            my $v = $result->{$k};
            if(ref($v) eq 'ARRAY') {
                my $count = scalar(@$v);
                if($count == 0) {
                    delete $result->{$k};
                } elsif($count == 1) {
                    $result->{$k} = mixed2utf8($v->[0]);
                } else {
                    $result->{$k} = [map { mixed2utf8($_) } @$v]
                }
            }
        } 
        return %$result;
    }
    else {
        warn "No doc $docid version";
	return ();
    }
}

my $solr_base_url = "http://localhost:8983/solr/ombud2017";


##
## Post a document with a file to the extract handler
##
sub post_solr_file { my($id,$docdata,$filespec, $opt) = @_;
    my @content;
    my $extract_uri = $solr_base_url."/update/extract";

    for my $k (keys %$docdata) {
        my $values = $docdata->{$k};
        next unless(defined($values));
        $values = [$values] unless(ref($values) eq 'ARRAY');
        foreach my $value (@$values) {
            $value = mixed2utf8($value);
            push(@content, ("literal.${k}" => $value));
        }
    }
    push @content, "file" => [ $filespec ] unless ref $filespec;
    push @content, "file" => $filespec if ref $filespec eq "ARRAY";
    
    my $request = POST(
        $extract_uri,
        Content_Type => 'form-data',
        Content => \@content
    );

 #   print $request->as_string(),"\n";
    my $ua = LWP::UserAgent->new;
    my $response = $ua->request($request); 
    if($opt->{respprint}) {    print $response->as_string(),"\n"; }
}

##
## Post a document with "plain" fields using the json handler
##
sub post_solr_nofile {
    my($docdata, $opt) = @_;

    #my $extract_uri = $solr_base_url."/update/json/docs";
    my $extract_uri = $solr_base_url."/update/json";

    my $content_json = ref $docdata?to_json($docdata, $opt->{quiet}?{}:{ pretty => 1}):$docdata;

    my $request = HTTP::Request->new(
        POST => $extract_uri,
        [
            "Content-Type" => "application/json",
            "Content-Transfer-Encoding" => "binary"
        ],
        $content_json
    );

    my $ua = LWP::UserAgent->new;
    my $response = undef;
    if(! $opt->{quiet}) {
        print "REQ:\n",$request->as_string(),"\n";
    }
    if($opt->{post}) {
	$response = $ua->request($request);
    }
    if($opt->{respprint}) {
        print "RESP:\n", defined $response?$response->as_string():"no response","\n";
    }
}

sub select_doc_data { my($docid, @fields) = @_;
    my @docdata = get_doc_data($docid);
    return undef unless @docdata;
    my $docdata = { @docdata };
    if(@fields) {
        return { map { $_ => $docdata->{$_} } @fields };
    } else {
        return $docdata;
    }
}

sub usage {
	print <<ENDUSE;
Usage:
    bin/new_solr_build_index.pl [ -p ] [ -q ] [ -r ] [ -f f_fld1,v_fld2,d_fld3,k_fld4... ] doc_id1 [ doc_id2 ]

Without flags, dumps JSON record for the range of documents.
	-p	Post to $solr_base_url/update/json/docs as JSON
	-q	Do not dump JSON request to stdout
	-r	Print the response from Solr for each posted record
	-f <fieldname>[,<fieldname2>...]
                List of fields to include. If not present, include all fields.
                This option can be included more than once.
		Fields beginning with d_ are from the document object,
		fields beginning with v_ are from the version object,
		fields beginning with f_ are version fields,
		fields beginning with k_ are konstructed fields.
        --confname <configname>
                Name of Obvius configname to use for source database.
        --core <core-url-or-name>
                Solr core URL or name to store index.
                Defaults to $solr_base_url

ENDUSE
	exit;
}


my $opt = {
    post => 0,
    quiet => 0,
    respprint => 0,
    "from-file" => "",
    "to-file" => "",
};

my($post,$quiet) = (0,0);
my @fields = ();
my $confname = "ombud";
my $core = "";

GetOptions(
    p => \$opt->{post},
    q => \$opt->{quiet},
    r => \$opt->{respprint},
    "from-file=s" => \$opt->{"from-file"},
    "to-file=s" => \$opt->{"to-file"},
    "f=s" => \@fields,
    "confname=s" => \$confname,
    "core=s" => \$core
);
if($core =~ m/^\w+$/) {
    $solr_base_url =~ s/ombud2017$/$core/;
} elsif($core =~ m%^http://%) {
    $solr_base_url = $core;
} elsif($core) {
    die "Error: core $core not understood.";
}

# Make it possible to specify fields as comma-separated lists.
@fields = split(/\s*,\s*/, join(",", @fields));
usage unless @ARGV or grep {$_} ($post, $quiet, $respprint, $opt->{"from-file"});

my $log=Obvius::Log->new('notice');
my $conf = new Obvius::Config($confname);
die "No obvius with configname ${confname}" unless($conf);
$obvius = new Obvius($conf, undef, undef, undef, undef, undef, log=>$log);
die "No obvius with configname ${confname}" unless($obvius);
$obvius->execute_command("set names utf8");
$obvius->cache(0);
$hostmap = $obvius->config->param('hostmap') ||
           Obvius::Hostmap->new_with_obvius($obvius);
$add_to_docids = 0;

# Configure the docs directory
$docs_dir = $obvius->config->param('sitebase');
if(!$docs_dir) {
    die "The configuration for $confname have not specified sitebase"
}
$docs_dir =~ s{/$}{};
$docs_dir .= '/docs';
if(!-d $docs_dir) {
    die "$docs_dir is not a directory";
}

my $docid1 = shift @ARGV;
my $docid2 = shift @ARGV || $docid1;

my %indexed = ();

sub index_doc { my($docid, $output_array_ref) = @_;
    my $docdata = select_doc_data($docid,@fields);
    my $parent = undef;
    if($docdata) {
        $docdata->{"id"} = ($docid + $add_to_docids);
        ($docdata,$parent) = checkaddtoparent($docdata);
        if(defined $parent and not $indexed{$parent}) {
            index_doc($parent);
        }
        if($output_array_ref) {
            push(@$output_array_ref, $docdata);
        } else {
            post_solr_nofile([ $docdata ], $opt);
        }
    }
    $indexed{$docid} = 1;

    return $docdata ? 1 : 0;
}

my ($path_sth, $docid_sth, $path_sth2);




sub docids_under_path {
    my ($path) = @_;

    $path_sth ||= $obvius->dbh->prepare(q|
        select
            docid_path.docid
        from
            docid_path
        where
            path LIKE ?
    |);
    $path_sth->execute($path. '%');

    my @result;
    while(my ($docid) = $path_sth->fetchrow_array) {
        push(@result, $docid);
    }
    
    return @result;
}

sub docids_under_docid {
    my ($docid) = @_;

    my $path = docid2path($docid);
    if($path) {
        return docids_under_path($path);
    }

    return ();
}

sub path2docid {
    my ($path) = @_;

    $path_sth2 ||= $obvius->dbh->prepare(q|
        select
            docid_path.docid
        from
            docid_path
            join
            versions on (
                docid_path.docid = versions.docid
                and
                versions.public = 1
            )
        where
            path = ?
    |);
    $path_sth2->execute($path);
    if(my ($docid) = $path_sth2->fetchrow_array) {
        return ($docid);
    }
    return ();
}

sub docid2path {
    my ($docid) = @_;

    $docid_sth ||= $obvius->dbh->prepare(q|
        select
            docid_path.path
        from
            docid_path
            join
            versions on (
                docid_path.docid = versions.docid
                and
                versions.public = 1
            )
        where
            docid_path.docid = ?
    |);
    $docid_sth->execute($docid);

    my ($path) = $docid_sth->fetchrow_array;

    return $path;
}


sub program2docids {
    my ($line) = @_;

    my @docids;
    my @missing_paths;
    my @missing_recursive_paths;

    $line =~ s{^\s+}{};
    $line =~ s{\s+$}{};

    if(!defined($line) || $line eq "") {
        return ();
    }

    if($line =~ s{^rec:}{}) {
        if($line =~ m{^/}) {
            my @tmp =  docids_under_path($line);
            if(@tmp) {
                push(@docids, @tmp);
            } else {
                push(@missing_recursive_paths, $line);
            }
        } elsif($line =~ m{^\d+$}) {
            push(@docids, docids_under_docid($line));
        }
    } else {
        if($line =~ m{^/}) {
            my $docid = path2docid($line);
            if(defined($docid)) {
                push(@docids, $docid);
            } else {
                push(@missing_paths, $line);
            }
        } elsif($line =~ m{^\d+$}) {
            push(@docids, int($line));
        }
    }

    return {
        docids                  => \@docids,
        missing_paths           => \@missing_paths,
        missing_recursive_paths => \@missing_recursive_paths,
    }
}

my %public_path_cache;
my $public_sth;

sub is_public_docid {
    my ($docid) = @_;

    if(exists $public_path_cache{$docid}) {
        return $public_path_cache{$docid};
    }

    $public_sth ||= $obvius->dbh->prepare(q|
        select
            documents.parent parent,
            versions.public has_public_version
        from
            documents
            left join versions on (
                documents.id = versions.docid
                AND
                versions.public = 1
             )
         where
            documents.id = ?
    |);

    my @seen_docids;
    my $public = 1;

    $public_sth->execute($docid);
    my ($parent, $has_public_version) = $public_sth->fetchrow_array;

    if(!$has_public_version) {
        $public_path_cache{$docid} = 0;
        return 0;
    }

    # Check parent for public status, except for the root document, which just uses has_public_version
    my $result = $docid == 1 ? $has_public_version : is_public_docid($parent);;
    $public_path_cache{$docid} = $result ? 1 : 0;

    return $result;
}

sub process_input_file {
    my ($filename) = @_;

    my %docids;
    my @missing_docids;
    my %paths;
    my %rec_paths;
    my $fh;
    if($filename eq "-") {
        $fh = \*STDIN;
    } else {
        open($fh, "<$filename") || die "Could not open $filename for reading";
    }
    while(my $line = <$fh>) {
        # print STDERR "Processing line $line";
        my $data = program2docids($line);
        foreach my $docid (@{ $data->{docids} }) {
            if(defined($docid) && $docid ne "") {
                $docids{$docid}++;
            }
        }
        foreach my $path (@{ $data->{missing_paths} }) {
            if($path) {
                $paths{$path}++;
            }
        }
        foreach my $path (@{ $data->{missing_recursive_paths} }) {
            if($path) {
                $rec_paths{$path}++;
            }
        }
    }
    close($fh);
    if($filename ne '-') {
        unlink($filename);
    }
    my @data;
    foreach my $docid (sort { $a <=> $b } keys %docids) {
        if(is_public_docid($docid)) {
            index_doc($docid, \@data);
        } else {
            push(@missing_docids, $docid);
        }
    }

    my $ua = LWP::UserAgent->new();

    # Send new document data to SOLR
    if(@data) {
        my $req = HTTP::Request->new(POST => $solr_base_url . "/update/json?commit=true");
        $req->content_type("application/json; charset=utf-8");
        $req->content(JSON::encode_json(Obvius::CharsetTools::mixed2perl(\@data)));
        my $res = $ua->request($req);
    }

    # Delete old data
    my @matches;

    my @paths;
    # Match paths with qutoes for direct paths ("/some/path/")
    push(@paths, map { '"'  . $_ . '"' } sort keys %paths);
    # and quotemeta and wildcard for recursive paths (\/some\/path\/*)
    push(@paths, map { quotemeta($_) . "*" . "" } sort keys %rec_paths);

    if(@paths) {
        push(@matches, sprintf('k_uri:(%s)', join(' OR ', @paths)));
    }

    # Match docids by OR'ing them together
    if(@missing_docids) {
        push(@matches, sprintf('id:(%s)', join(' OR ', @missing_docids)));
    }

    if(@matches) {
        my $expr = "(" . join(") OR (", @matches) . ")";
        my $data = { delete => { query => $expr } };

        my $req = HTTP::Request->new(POST => $solr_base_url . "/update/json?commit=true");
        $req->content_type("application/json; charset=utf-8");
        $req->content(JSON::encode_json(Obvius::CharsetTools::mixed2perl($data)));
        my $res = $ua->request($req);
    }

}

sub main {
    if(my $filename = $opt->{"from-file"}) {
        process_input_file($filename)
    }
    elsif (my $output_file = $opt->{"to-file"}) {
        my @data;
        for my $docid (get_all_public_docids($docid1, $docid2)) {
            index_doc($docid, \@data);
        }

        my $json = JSON::encode_json(Obvius::CharsetTools::mixed2perl(\@data));
        open my $fh, ">", $output_file;
        print $fh $json;
        close $fh;
    } else {
        for my $docid (get_all_public_docids($docid1, $docid2)) {
            index_doc($docid);
        }
    }
}
main
# post_solr_nofile('[ { "id": "2570", "textmeta": { "add": [ "foobar" ] } } ]', $opt);
