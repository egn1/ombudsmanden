#!/usr/bin/perl

# Removes old sessions from obvius-sites. Sessions will be expired
# according to the config-paramters cleanup_edit_sessions_after and
# cleanup_user_sessions_after. These config parameters specify the
# number of days a session should be allowed to live.

use strict;
use warnings;

use lib '/var/www/obvius/perl';
use lib '/var/www/www.ombudsmanden.dk/perl';

use Obvius;
use Obvius::Config;
use Data::Dumper;

my @conf_files = ("ombud");

foreach my $confname (@conf_files) {
    my $config = new Obvius::Config($confname);
    my $edit_days = $config->param('cleanup_edit_sessions_after');
    my $user_days = $config->param('cleanup_user_sessions_after');
    next unless($edit_days || $user_days);
    my $obvius = new Obvius($config);
    if($edit_days) {
        $obvius->dbh->do("
            delete from apache_edit_sessions
            where timestamp < DATE_SUB(NOW(),
            INTERVAL $edit_days DAY)
        ");
    }
    if($user_days) {
        $obvius->dbh->do("
            delete from apache_edit_sessions
            where timestamp < DATE_SUB(NOW(),
            INTERVAL $user_days DAY)
        ");
    }
}
