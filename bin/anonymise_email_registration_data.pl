#!/usr/bin/perl
use strict;
use warnings;

use lib '/var/www/obvius/perl';
use lib '/var/www/www.ombudsmanden.dk/perl';

use Obvius;
use Obvius::Config;

my $no_of_days_before_expiry = 14;

my $obvius = Obvius->new(Obvius::Config->new('ombud'));
# my $salt = $obvius->config->param('anonymous_formusers_salt') || '$1$8yls31xc$';

my $select_sth = $obvius->dbh->prepare('SELECT id, name, address from submitted_complaints where timestamp < NOW() - INTERVAL ? DAY');
my $update_sth = $obvius->dbh->prepare('UPDATE submitted_complaints SET name = ?, address = ? where id = ?');

$select_sth->execute($no_of_days_before_expiry);

while (my $row = $select_sth->fetchrow_hashref) {
    $update_sth->execute(anonymize($row->{name}), anonymize($row->{address}), $row->{id});
}

# For now, we are simply clearing the name & address fields. Can be extended with pseudonymisation if necessary.
sub anonymize {
    my ($input) = @_;
    return "";
    #return substr(crypt($input, $salt), 12);
}
