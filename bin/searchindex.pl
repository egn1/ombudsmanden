#!/usr/bin/perl

use strict;
use warnings;

use locale;

use lib '/home/httpd/obvius/perl_blib';

use Data::Dumper;

use Obvius;
use Obvius::Config;
use HTML::Parser;
use XML::Simple;
use MIME::Base64;
use utf8;

# Read data from the freetext indexmap. If the map is out of date
# (eg. if you added a new doctype) you can autogenerate a new by running
# bin/editpages_to_indexmap.pl /var/www/www.akf.dk/conf/freetext_indexmap.xml > output.xml
my $freetext_indexmap_file = '/var/www/www.ombudsmanden.dk/conf/freetext_indexmap.xml';
my $field_data = XMLin($freetext_indexmap_file, keyattr => [], forcearray => [qw(field doctype editpage)]);
die "Couldn't read field data" unless($field_data);

my %doctypes_by_id;

for my $doctype (@{ $field_data->{doctype} }) {
    $doctypes_by_id{$doctype->{id}} = $doctype;

    $doctype->{fieldlist} = [];
    $doctype->{fieldmap} = {};
    for my $editpage (@{ $doctype->{editpage} }) {
        for my $field (@{ $editpage->{field} }) {
            if($field->{weight} && $field->{weight} > 0.00) {
                push(@{ $doctype->{fieldlist} }, $field->{name});
                $doctype->{fieldmap}->{$field->{name}} = $field;
            }
        }
    }
}

my $log=Obvius::Log->new('notice');
my $conf = new Obvius::Config('ombud');
my $obvius = new Obvius($conf, undef, undef, undef, undef, undef, log=>$log);
$obvius->execute_command("set names utf8");



#########################################################
#                                                       #
#                   Setup html parser                   #
#                                                       #
#########################################################

my $out = '';
my $parser = HTML::Parser->new(
    api_version=>3,
    default_h=> [ sub { my($this, $text)=@_; $out.=$text }, "self, text" ],
    start_h=> [sub {
            my($this, $tagname, $text, $attr)=@_;

            # Output alt and title attributes as text:
            if(my $alt = $attr->{alt}) {
                $out .= "$alt\n";
            }
            if(my $title = $attr->{title}) {
                $out .= "$title\n";
            }
        }, "self, tagname, text, attr" ],
    end_h=> [sub { my($this)=@_; }, "self" ],
    text_h=> [ sub {
            my ($this, $text)=@_;
            $out.=$text;
        },
        "self, text" ]);

sub html2text {
    my $html = shift;

    $out = '';
    $parser->parse($html);

    return $out;
}

#########################################################
#                                                       #
#                     Setup db set                      #
#                                                       #
#########################################################

my $iset = DBIx::Recordset->SetupObject(
    {
        '!DataSource' => $obvius->{DB},
        '!Table'      => 'freetextindex_versions',
    }
);

my $set = DBIx::Recordset->SetupObject(
    {
        '!DataSource' => $obvius->{DB},
        '!Table'      => 'freetextindex',
    }
);

my %indexed_versions;
$iset->Search();
while(my $rec = $iset->Next) {
    $indexed_versions{$rec->{docid} . "^" . $rec->{version}} = 1;
}


# Find active documents:
my $docs = $obvius->search([], "public=1") || [];

my %active_versions = map {
$_->DocId . "^" . $_->Version => 1
} @$docs;

# Now remove any of the old indexed versions that are not in the active versions:
for(keys %indexed_versions) {
    unless($active_versions{$_}) {
        my ($docid, $version) = split("^", $_);
        $iset->Delete({ docid => $docid, version => $version });
        $set->Delete({ docid => $docid });
    }
}

my $start = time;

my $processed_words = 0;


# Do the actual indexing
for my $vdoc (@$docs) {
    # Skip if we already indexed this version:
    next if($indexed_versions{$vdoc->DocId . "^" . $vdoc->Version});

    print STDERR "Indexing " . $vdoc->DocId . ", " . $vdoc->Version . "\n";

    my @doc_words;

    my $doctype = $doctypes_by_id{$vdoc->Type};
    die "No doctype definition found for " . $vdoc->Type . "\n" unless($doctype);

    $obvius->get_version_fields($vdoc, [ @{ $doctype->{fieldlist}} ]);

    my %wordcount;

    for my $fieldname (@{ $doctype->{fieldlist}}) {
        my $fielddata = $doctype->{fieldmap}->{$fieldname};
        my @words = harvest_words($vdoc, $fieldname, $fielddata);
        for my $word (@words) {
            #if ($word =~ /§/) {
            #    print "$word\n";
            #}
            next unless(length($word) > 1);
            $wordcount{$word} += $fielddata->{weight};
        }
    }

    # Delete old words for the given docid:
    $set->Delete({docid => $vdoc->DocId});
    $iset->Delete({docid => $vdoc->DocId});

    # Insert the words:
    $obvius->{DB}->do("INSERT INTO freetextindex VALUES (" . join(
            "),(",
            map {
            "NULL, '$_'," . $vdoc->DocId . "," . $wordcount{$_}
            } keys %wordcount
        ) . ")");
    # And insert into the list of indexed versions:
    $iset->Insert({docid => $vdoc->DocId, version => $vdoc->Version});
}

sub harvest_words {
    my ($vdoc, $fieldname, $fielddata) = @_;

    my $type = $fielddata->{indextype};
    my @words;
    # Define regular expressions for performing the match
    my $section = qr/§/;
    my $section_with_number = qr/$section\s*\d+/;

    my $nr = qr/nr\.?/;
    my $nr_with_number = qr/$nr\s+\d+/;
    my $nr_tail = qr/,? $nr_with_number/;

 
    my $pcs = qr/[Ss]tk\.?/;
    my $pcs_with_number = qr/$pcs\s+\d+/;
    my $pcs_tail = qr/,? $pcs_with_number$nr_tail?/;

    my $word = qr/[\d\w]+/;

    my $match = qr/$section_with_number$pcs_tail|$word/;

    if($type eq 'normal') {
        my $value = $vdoc->field($fieldname) || [];
        $value = [ $value ] unless(ref($value));
        for my $text (@$value) {
            if(ref($text)) {
                if($text->UNIVERSAL::can('param')) {
                    # Try to catch the different kinds of xrefs here:
                    $text = ($text->param('title') || $text->param('name') || $text->param('id') || '');
                }
            }
            $text = fix_enc($text);
            push(@words, ($text =~ m!$match!g));
            #if ($text =~ /§/) {
            #    print "ORIG: TEXT = $text\n";
            #    for my $w ($text =~ m!$match!g) {
            #        print "JUST MATCHED: $w\n";
            #    }
            # }
        }
    } elsif ($type eq 'html') {
        my $value = $vdoc->field($fieldname) || [];
        $value = [ $value ] unless(ref($value));
        for my $text (@$value) {
            $text = html2text(fix_enc($text));
            push(@words, ($text =~ m!$match!g));
        }
    } elsif ($type eq 'upload') {
        my $mimetype = $obvius->get_version_field($vdoc, 'mimetype') || '';
        if($mimetype eq 'application/pdf') {
            if($vdoc->field($fieldname)) {
                local $/ = undef;
                my $tmpfile = "/tmp/" . $vdoc->DocId . $fieldname . "_tmp.pdf";
                open(FH, ">$tmpfile");
                print FH decode_base64($vdoc->field($fieldname));
                close(FH);
                my $text = `/usr/bin/pdftotext $tmpfile -`;
                unlink($tmpfile);
                $text = lc(fix_enc($text));
                @words = ($text =~ m!$match!g);
            }
        }
    } else {
        die "Unknown indextype $type";
    }
    #for my $word (@words) {
    #    print "JUST HARVESTED: $word\n" if $word =~ /§/;
    # }
    return @words;
}

sub fix_enc {
    my ($text) = @_;

#    return $text;

    use Encode;
    my $str = $text;
    my $out = '';

    if (Encode::is_utf8($str)) {
        $str = Encode::encode('LATIN-1', $str);
    }
    while ($str) {
        $out .= Encode::decode('UTF-8', $str, Encode::FB_QUIET);
        if ($str) {
            $out .= Encode::decode('LATIN-1', substr($str, 0, 1));
            $str = substr($str, 1);
        }
    }
    $text = $out;  

    #utf8::upgrade($text);

    return $text;

}
