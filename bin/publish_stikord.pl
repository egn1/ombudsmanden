#!/usr/bin/perl -w

# This script is used to automatically publish Stikord on Ombudsmanden.dk.
# The Stikord documents still need to be created manually, but there is no need to
# publish them manually (saves quite a few clicks).
# The latest version of a Stikord document automatically gets published unless the 
# document allready has a published version.

use strict;
use warnings;

use lib '/var/www/www.ombudsmanden.dk/perl/';
use lib '/var/www/obvius/perl/';

use Obvius;
use Obvius::Config;
use Obvius::Log;

use POSIX qw(strftime);

my $conf = new Obvius::Config('ombud'); #Sitename
my $log = new Obvius::Log qw(notice);
my $obvius = new Obvius($conf,undef,undef,undef,undef,undef,log => $log);
$obvius->{USER} = "admin";

my $stikord_parent = $obvius->lookup_document("/stikordsregister/alle/"); #Container for Stikord

unless ($stikord_parent) {
	die ("Failed to retrieve parent document. Check the path.\n");
}

my $all_stikord = $obvius->get_document_subdocs_latest($stikord_parent);

my $stikord_type_id = $obvius->get_doctype_by_name("Stikord")->Id;
my $count_published = 0;
my $count_errors = 0;

foreach my $stikord (@$all_stikord) {
  next unless $stikord->param('type') eq $stikord_type_id; #Only chech Stikord
  next if $stikord->param('public'); #vdoc is public

  my $doc = $obvius->get_doc_by_id($stikord->DocId);
  next if $obvius->get_public_version($doc); #Document has a public version


  $obvius->get_version_fields($stikord, 255, 'PUBLISH_FIELDS');

  # Set published
  my $publish_fields = $stikord->publish_fields;
  $publish_fields->param(PUBLISHED => strftime('%Y-%m-%d %H:%M:%S', localtime));
  $publish_fields->param(in_subscription => 0);

  my $publish_error;
  $obvius->publish_version($stikord, \$publish_error);

  if ($publish_error) {
	print ("Couldn't publish: " . $stikord->Title . "\n");
	print ("Error: $publish_error\n");
	$count_errors++;
  } else {
	print ("Published: " . $stikord->Title . "\n");
	$count_published++;
  }
}

print ("\nPublished $count_published documents. $count_errors errors ocurred\n");
