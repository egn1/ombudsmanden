#!/usr/bin/perl

use strict;
use warnings;
use Obvius;
use Obvius::Config;

use LWP::UserAgent;
my $ua = LWP::UserAgent->new;

my $config = Obvius::Config->new('ombud');

my $solr_port = $config->param('solr_port') || 8983;
my $solr_host = $config->param('solr_host') || 'localhost';
my $solr_core = $config->param('solr_core') || 'ombud2017';

my $solr_base_url = "http://${solr_host}:${solr_port}/solr/${solr_core}";

my $response = LWP::UserAgent->new()->get(
    $solr_base_url . '/update?commit=true',
);
