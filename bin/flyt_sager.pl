#!/usr/bin/perl

use strict;
use warnings;

use Obvius;
use Obvius::Config;
use Obvius::Log;

my $conf = new Obvius::Config('ombud');
my $obvius = new Obvius($conf);
$obvius->param('LOG' => new Obvius::Log('notice'));


# XXX!
$obvius->{USER} = 'admin';
$obvius->cache(1);

my $bsager_path = '/udtalelser/beretningssager/alle_bsager/';
my $isager_path = '/inspektioner/alle_inspektioner/';
my $bsager_arkiv_path = $bsager_path . 'arkiv/';
my $isager_arkiv_path = $isager_path . 'arkiv/';

my $bsager_doc = $obvius->lookup_document($bsager_path);
die "Couldn't find root doc for B-sager" unless($bsager_doc);

my $isager_doc = $obvius->lookup_document($isager_path);
die "Couldn't find root doc for I-sager" unless($isager_doc);

my $bsager_arkiv_doc = $obvius->lookup_document($bsager_arkiv_path);
die "Couldn't find archive doc for B-sager" unless($bsager_arkiv_doc);

my $isager_arkiv_doc = $obvius->lookup_document($isager_arkiv_path);
die "Couldn't find archive doc for I-sager" unless($isager_arkiv_doc);

my $sags_doctype = $obvius->get_doctype_by_name('Sag');
die "No Sag doctype" unless($sags_doctype);


my $b_docparams = $obvius->get_docparams($bsager_doc);
my $b_method = 'by number';
if(my $b_method_docparm = $b_docparams->param('archive_method')) {
    $b_method = 'by date' if($b_method_docparm->Value =~ m!by date!i);
}
my $b_days = 90;
if(my $b_days_docparm = $b_docparams->param('archive_days')) {
    if($b_days_docparm->Value =~ m!(\d+)!) {
        $b_days = $1;
    }
}

my $b_number = 20;
if(my $b_number_docparm = $b_docparams->param('archive_number')) {
    if($b_number_docparm->Value =~ m!(\d+)!) {
        $b_number = $1;
    }
}


my $b_where = "parent = " . $bsager_doc->Id . " AND type = " . $sags_doctype->Id;
if($b_method eq 'by date') {
    $b_where .= " AND docdate <= (NOW() - INTERVAL $b_days DAY)";
}

my $bsager = $obvius->search(
                                ['docdate'],
                                $b_where,
                                needs_document_fields => ['parent'],
                                order => 'docdate DESC'
                            ) || [];

if($b_method eq 'by number') {
    splice(@$bsager, 0, $b_number);
}

for(@$bsager) {
    my $doc = $obvius->get_doc_by_id($_->DocId);
    my $old_path = $obvius->get_doc_uri($doc);
    my $new_path = $bsager_arkiv_path . $doc->Name . "/";
    print "Moving $old_path to $new_path\n";
    my $err = '';
    $obvius->rename_document($doc, $new_path, \$err);
    if($err) {
        die "Something went wrong: $err\n";
    }
}


my $i_docparams = $obvius->get_docparams($isager_doc);
my $i_method = 'by number';
if(my $i_method_docparm = $i_docparams->param('archive_method')) {
    $i_method = 'by date' if($i_method_docparm->Value =~ m!by date!i);
}
my $i_days = 90;
if(my $i_days_docparm = $i_docparams->param('archive_days')) {
    if($i_days_docparm->Value =~ m!(\d+)!) {
        $i_days = $1;
    }
}

my $i_number = 20;
if(my $i_number_docparm = $i_docparams->param('archive_number')) {
    if($i_number_docparm->Value =~ m!(\d+)!) {
        $i_number = $1;
    }
}


my $i_where = "parent = " . $isager_doc->Id . " AND type = " . $sags_doctype->Id;
if($i_method eq 'by date') {
    $i_where .= " AND docdate <= (NOW() - INTERVAL $i_days DAY)";
}


my $isager = $obvius->search(
                                ['docdate'],
                                $i_where,
                                needs_document_fields => ['parent'],
                                order => 'docdate DESC'
                            ) || [];

if($i_method eq 'by number') {
    splice(@$isager, 0, $i_number);
}

for(@$isager) {
    my $doc = $obvius->get_doc_by_id($_->DocId);
    my $old_path = $obvius->get_doc_uri($doc);
    my $new_path = $isager_arkiv_path . $doc->Name . "/";
    print "Moving $old_path to $new_path\n";
    my $err = '';
    $obvius->rename_document($doc, $new_path, \$err);
    if($err) {
        die "Something went wrong: $err\n";
    }
}
