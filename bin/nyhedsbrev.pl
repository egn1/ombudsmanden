#!/usr/bin/perl
# $Id: nyhedsbrev.pl,v 1.1.2.2 2010/11/15 14:56:33 jubk Exp $
use strict;
use warnings;

use lib '/var/www/obvius/perl';
use lib '/var/www/www.ombudsmanden.dk/perl';

use Obvius;
use Obvius::Config;
use Obvius::Log;
use MIME::Words qw(encode_mimeword);
use MIME::QuotedPrint;
use Obvius::CharsetTools qw(mixed2utf8);
use Ombudsmanden::Nyhedsbrev;
use Email::Valid;

use Encode;

use HTML::Mason::Interp;

use Net::SMTP;
use POSIX qw(strftime);

use Getopt::Long;
use Carp;

use locale;
use Fcntl ':flock';

use Data::Dumper;

my $site = 'ombud';
my $sender = 'bounce@magenta-aps.dk';
my $sitename = 'www.ombudsmanden.dk';
my $debug = 0;
my $debug_email = '';
my $skip_subscription_update = 0;

my $log = new Obvius::Log ($debug ? 'debug' : 'alert');

croak ("No site defined")
    unless (defined($site));

croak ("No sender defined")
    unless(defined($sender));

my $conf = new Obvius::Config($site);
croak ("Could not get config for $site")
    unless(defined($conf));

my $obvius = new Obvius(
    $conf,
    undef,undef,undef,undef,undef, # Hmmmmm >_<
    log => $log
);

#print Dumper($obvius);
croak ("Could not get Obvius object for $site")
    unless(defined($obvius));

croak ("Must have a sitename") unless($sitename);
my $base_dir = '/var/www/'. $sitename;

my $newsletter_data = Ombudsmanden::Nyhedsbrev->new($obvius);

## Setup data:

# Collection of newsletters on the site.
# Each newsletter might consist of multiple subscription areas.
my @newsletters = $newsletter_data->newsletters_copy;

my $mason_output = '';
my $interp = new HTML::Mason::Interp(
    comp_root => $base_dir . '/mason/mail/',
    data_dir => $base_dir . '/var/mail/',
    out_method => \$mason_output
);


my $active_map;

my @manual_docids;
for my $arg (@ARGV) {
    if($arg =~ m!^\s*(\d+)\s*!) {
        push(@manual_docids, $1);
    } else {
        croak("Unknown command line argument: $arg");
    }
}

if(@manual_docids) {
    $active_map = { map { $_ => 1 } @manual_docids };
}


my %newsletters_lookup;
my %doc_lists;

## "Main" program part

my $lock_file = "/tmp/$sitename.newsletter.lock";
open F, ">$lock_file";
flock F, LOCK_EX;
my $lock_now = localtime();
print F $lock_now;

my $exitstatus = 0;
eval { main() };
if($@) {
    print STDERR "Subscription script failed with the following message:\n$@\n";
    $exitstatus = 1;
}

flock F, LOCK_UN;
close F;
exit($exitstatus);

sub main {
    use Time::HiRes; my $t0 = [Time::HiRes::gettimeofday()];
    # Make a timestamp now so subscribers wont miss documents published while
    # the subscription system is running.
    my $now = strftime('%Y-%m-%d %H:%M:%S', localtime);

    my $time_track_id;
    eval {
        my $sth = $obvius->dbh->prepare(q|
            insert into newsletter_send_time (start) values (?)
        |);
        $sth->execute($now);
        ($time_track_id) = $sth->{mysql_insertid};
        $sth->finish;
    };
    warn $@ if($@);

    # Yesterday - For subscriptions with an all-zero last_update
    my $yesterday = strftime('%Y-%m-%d %H:%M:%S', localtime(time() - 24*60*60));

    my @subscription_roots = $newsletter_data->subscription_roots;

    for my $n (@newsletters) {
        # Disable all newsletters by default if we have been given an explicit
        # list of docids to send to.
        $n->{disabled} = 1 if($active_map);
        for my $s (@{ $n->{sections} || [] }) {
            my @docs;
            $s->{docs} = \@docs;
            foreach my $uri (@{ $s->{paths} }) {
                my $d = $obvius->lookup_document($uri);
                if(!$d) {
                    warn "Could not find document for subscription area $uri";
                    next;
                }
                $newsletters_lookup{$uri} = $n;
                $newsletters_lookup{$d->Id} = $n;
                push(@docs, $d);
                # Enable the newsletter if the docid of one of its areas has
                # been passed as command line argument.
                my $sub_docid = $newsletter_data->resolve_subscription_docid(
                    $d->Id
                );
                if($active_map and $active_map->{$sub_docid}) {
                    $n->{disabled} = undef;
                }
            }
        }
    }


    my $sth = $obvius->dbh->prepare(q|
        select
            MIN(last_update)
        from
            subscriptions
        where
            last_update != '0000-00-00 00:00:00'
            and
            last_update IS NOT NULL
            and
            docid=?
    |);

    # Fetch list of recently published news items under each of the
    # subscription roots.
    # Each search for published newsitems will be limited to the oldest
    # "last_update" on a subscription entry for the given newsletter docid.
    for my $d (@subscription_roots) {
        my $uri = $obvius->get_doc_uri($d);
        my $newsletter = $newsletters_lookup{$uri};
        unless($newsletter) {
            warn "Subscription area $uri does not belong to any newsletter " .
                "- no mails will be sent for this area";
            next;
        }
        next if($newsletter->{disabled});

        $sth->execute(
            $newsletter_data->resolve_subscription_docid($d->Id)
        );

        my $oldest_last_update = $yesterday;
        my ($val) = $sth->fetchrow_array;
        if($val) {
            $oldest_last_update = ($val le $yesterday) ? $val : $yesterday;
        }

        $d->param('oldest_last_update' => $oldest_last_update);

        my $qpath = $obvius->dbh->quote($uri . '%');
        my $new_docs = $obvius->search(
            ['in_subscription', 'published', 'docdate'],
            "in_subscription > 0 and " .
            "published > '$oldest_last_update' and " .
            "path like $qpath",
            notexpired => 1,
            public => 1,
            order => "docdate"
        ) || [];
        for my $v (@$new_docs) {
            $obvius->get_version_fields($v, ['title', 'teaser', 'docdate']);
            my $d = $obvius->get_doc_by_id($v->DocId);
            $v->param('uri' => $obvius->get_doc_uri($d));
        }

        $doc_lists{$d->Id} = $new_docs;
    }
    $sth = undef;

    # For each non-suspended subscriber, loop over their subscriptions and
    # store their last_update timestamp for each subscription root under the
    # roots docid in %active_subscriptions.
    # Afterwards loop over all newsletters and use the timestamps in
    # %active_subscriptions to filter the documents saved in %doc_lists to
    # figure out whether the subscriber should recieve any newsitems for each
    # area.
    # Finally send out each newslatter that has areas with found documents.
    my $subscribers = $obvius->get_all_subscribers || [];
    for my $subscriber (@$subscribers) {
        next if $subscriber->{suspended};

        # Gather data from active subscriptions and store last_update for
        # each root-docid in %active_subscriptions.
        my %active_subscriptions;
        my $subscriptions_ref = $obvius->get_subscriptions({
            subscriber => $subscriber->{id}
        }) || [];
        for my $s (@$subscriptions_ref) {
            my $last_update = $s->{last_update} eq '0000-00-00 00:00:00' ?
                              $yesterday :
                              $s->{last_update};
            my $root_doc = $obvius->get_doc_by_id($s->{docid});
            if(!$root_doc) {
                next;
            }
            my @docs = $newsletter_data->resolve_area_uri(
                $obvius->get_doc_uri($root_doc)
            );
            foreach my $doc (@docs) {
                $active_subscriptions{$doc->Id} = $last_update;
            }
        }

        # Filter potential news items with the timestamps in
        # %active_subscriptions, adding areas containing newsitems to
        # @areas_to_send.
        my @newsletters_to_send;
        for my $newsletter (@newsletters) {
            my @areas_to_send;
            my @docids;
            for my $section (@{ $newsletter->{sections} }) {
                my @docs_to_send;
                foreach my $doc (@{ $section->{docs} }) {
                    my $docid = $doc->Id;
                    my $last_update = $active_subscriptions{$docid};
                    next unless($last_update);
                    push(
                        @docs_to_send,
                        grep {
                            $_->Published ge $last_update
                        } @{ $doc_lists{$docid} }
                    );
                }
                if(@docs_to_send) {
                    push(@areas_to_send, {
                        area => $section,
                        docs => \@docs_to_send,
                    });
                    push(@docids, map { $_->DocId } @docs_to_send);
                }
            }
            if(@areas_to_send) {
                push(@newsletters_to_send, {
                    newsletter => $newsletter,
                    areas => \@areas_to_send,
                    content_key => join(",", @docids)
                });
            }
        }

        # Send out newsletters that have areas with documents in them.
        for my $n (@newsletters_to_send) {
            my $error = send_newsletter_to_subscriber(
                $n->{newsletter},
                $n->{areas},
                $subscriber,
                $n->{content_key} || ''
            );
            if($error) {
                warn "Mail error: $error";
                next;
            }
            unless($debug or $debug_email or $skip_subscription_update) {
                # Change last_update for any area where new documents
                # were sent out.
                my %subscription_docids;
                for my $area (@{$n->{areas}}) {
                    foreach my $doc (@{ $area->{area}->{docs} }) {
                        my $docid = $doc->Id;
                        $docid = $newsletter_data->resolve_subscription_docid(
                            $docid
                        );
                        $subscription_docids{$docid}++;
                    }
                }
                foreach my $docid (keys %subscription_docids) {
                    $obvius->update_subscription(
                        { last_update => $now },
                        $subscriber->{id},
                        $docid
                    );
                }
            }
        }
    }
    eval {
        my $sth = $obvius->dbh->prepare(q|
            update newsletter_send_time set end = NOW() where id = ?
        |);
        $sth->execute($time_track_id);
        $sth->finish;
    };
    warn $@ if($@);
    print "Spent ", Time::HiRes::tv_interval(
        $t0, [Time::HiRes::gettimeofday()]
    ), "\n";
}

my $debug_email_sent = 0;

sub run_mason {
    my ($template, %args) = @_;

    my $retval = $interp->exec($template, %args);
    if($retval) {
        die "Mason failed for template $template with code $retval";
    } else {
        my $result = $mason_output;
        $mason_output = '';
        return $result;
    }

}

sub send_newsletter_to_subscriber {
    my ($newsletter_data, $areas, $subscriber, $cache_key) = @_;

    if($debug_email) {
        return if($debug_email_sent);
        $subscriber->{email} = $debug_email;
        $debug_email_sent = 1;
    }

    my $mail_error;
    my $mailto = $subscriber->{email};

    # Bail out if trying to send to an invalid email address
    my $mailto_ok = Email::Valid->address($mailto);
    if(!$mailto_ok) {
        return "Invalid e-mail address: $mailto";
    }

    $cache_key ||= '';

    my $templatename = $newsletter_data->{template} || 'default';

    my $templatedir = "${base_dir}/mason/mail/nyhedsbrev/${templatename}";
    unless(-d $templatedir) {
        die "Template dir $templatedir does not exist"
    }

    my $mail_content;
    eval {
        $mail_content = run_mason(
            "/nyhedsbrev/${templatename}/envelope.mason",
            obvius => $obvius,
            newsletter => $newsletter_data,
            areas => $areas,
            subscriber => $subscriber
        );

    };
    if($@) {
        print STDERR "Mason error: $@";
        return;
    }

    if($debug) {
        print STDERR "Not sending this mail (because of DEBUG), sender: $sender\n";
        print STDERR $mail_content ."\n";
    } else {
        my $smtp = Net::SMTP->new('localhost', Timeout=>30, Debug => $debug);
        $mail_error = "Failed to specify a sender [$sender]\n"      unless ($smtp->mail($sender));
        $mail_error = "Failed to specify a recipient [$mailto]\n"   unless ($mail_error or $smtp->to($mailto));

        $mail_error = "Failed to send a message\n"                  unless ($mail_error or $smtp->data([$mail_content]));
        $mail_error = "Failed to quit\n"                            unless ($mail_error or $smtp->quit);
    }

    return $mail_error;
}

