#!/usr/bin/perl -w

# Import subscribers from csv from Ombudsmanden. Takes CSV lines on STDIN

use lib '/var/www/www.ombudsmanden.dk/perl/';
use lib '/var/www/obvius/perl/';

use Obvius;
use Obvius::Config;
use Obvius::Log;

use DBIx::Recordset;
use Data::Dumper;

my $conf = new Obvius::Config('ombud'); #Sitename
my $log = new Obvius::Log qw(notice);
my $obvius = new Obvius($conf,undef,undef,undef,undef,undef,log => $log);

# Map subscription areas to relevant DocId
my %sub_map = (
			   'presse' => $obvius->lookup_document('/nyt_og_presse/alle/')->Id,
			   'beretning' => $obvius->lookup_document('/udtalelser/beretningssager/alle_bsager/')->Id,
			   'inspektion' => $obvius->lookup_document('/inspektioner/alle_inspektioner/')->Id
			   );

# Need to create our own Recordset object to use the !Serial functionality
my $set;
$set=DBIx::Recordset->SetupObject( {'!DataSource'=>$obvius->{DB},
									'!Table' => 'subscribers',
									'!Serial' => 'id'
									});

while (my $line = <STDIN>) {
	addSubscriber($line);
}

sub addSubscriber {
	my ($line) = @_;
	#"id","emails","presse","beretning","inspektion"
	$line =~ /^(\d*),\"(.*)\",(\d),(\d),(\d)/;

	my %subscriber = ('email'=>$2);
	$set->Insert(\%subscriber);
	my $subscriberID = $set->LastSerial();

	my @subscriptions;

	push (@subscriptions, $sub_map{'presse'}) if ($3);
	push (@subscriptions, $sub_map{'beretning'}) if ($4);
	push (@subscriptions, $sub_map{'inspektion'}) if ($5);

	map {
		my %record = ('docid' => $_, 'subscriber' => $subscriberID);
		$obvius->insert_table_record('subscriptions',\%record);
	} @subscriptions;
}
