#!/bin/bash

set -e

EMAIL="$1"
FORCE_ARG="$2"

if [[ "${EMAIL}" == "" ]]; then
    echo "You must specify an email address as the first argument"
    exit 1
fi

if [[ "${FORCE_ARG}" != '--force' ]]; then
    echo ""
    echo -n "You are about to create a certificate for '${EMAIL}', continue (Y/n)? >"
    read yesno

    if [[ "${yesno}" == "y" || "${yesno}" == "Y" || "${yesno}" == "" ]]; then
        echo -n ""
    else
        echo "Ok, exiting"
        exit 1
    fi
fi


test -d /var/www/www.ombudsmanden.dk/certificates ||
    mkdir /var/www/www.ombudsmanden.dk/certificates

if [[ -d "/var/www/www.ombudsmanden.dk/certificates/${EMAIL}" ]]; then
    echo "/var/www/www.ombudsmanden.dk/certificates/${EMAIL} already exists, exiting!"
    exit 1
fi

mkdir /var/www/www.ombudsmanden.dk/certificates/${EMAIL}

pushd "/var/www/www.ombudsmanden.dk/certificates/${EMAIL}" > /dev/null

PASSPHRASE=`head /dev/urandom | tr -dc A-Za-z0-9 | head -c 12`
echo "$PASSPHRASE" > passphrase.txt

# Generate CA key
openssl genrsa -des3 -out ca.key  -passout "file:passphrase.txt" 4096
# Generate CA
openssl req -new -x509 -days 3650 -key ca.key -out ca.crt \
    -passin "file:passphrase.txt" \
    -subj "/C=DA/ST=Denmark/L=Aarhus/O=Magenta Aps/OU=Magenta Aarhus/CN=smime-test-ca.magenta.dk"
# generate smime key
openssl genrsa -des3 -out key.pem -passout "file:passphrase.txt" 4096
openssl req -new -days 3650 -key key.pem -out cert.csr \
    -passin "file:passphrase.txt" \
    -subj "/C=DA/ST=Denmark/L=Aarhus/O=Magenta Aps/OU=Magenta Aarhus/CN=${EMAIL}"
# Sign certificate
openssl x509 -req -days 365 -in cert.csr -CA ca.crt -CAkey ca.key \
    -set_serial 1 \
    -out cert.pem \
    -setalias "Self Signed SMIME" \
    -addtrust emailProtection \
    -addreject clientAuth \
    -addreject serverAuth \
    -trustout \
    -passin "file:passphrase.txt"
# Export as pkcs12
openssl pkcs12 -export -in cert.pem -inkey key.pem -out "${EMAIL}.p12" \
    -passin "file:passphrase.txt" \
    -passout "pass:${PASSPHRASE}"

echo ""
echo "Certificate created and exported to `pwd`/${EMAIL}.p12"
echo "Passphrase used for keys and archives is '${PASSPHRASE}'"
echo ""

popd > /dev/null
