#!/usr/bin/perl

use strict;
use warnings;

use lib '/home/httpd/obvius/perl_blib';

use Data::Dumper;
use Obvius;
use Obvius::Config;
use Obvius::Data;
use POSIX qw(strftime);

my $level = 0;
my $niv1;

my @codes;

my %parents_by_id;

my $conf = new Obvius::Config('ombud');
my $obvius = new Obvius($conf);

open(FH, "klaskoder.txt") or die "Couldn't open input file";
while(my $line = <FH>) {
    chomp($line);
    $line =~ s/^\s+//;
    next unless($line);
    if($line =~ /^[^\d]/i) {
        my $id;
        if($line =~ /^(.*)\s\(/) {
            $id = $1;
        } else {
            $id = $line;
        }
        push(@codes, { id => $id, idcode=> $id, title => $line, parent => undef});
        $niv1 = $id;
        $parents_by_id{$id} = $codes[-1];
    } else {
        my ($code, $rest) = ($line =~ /^([\d.]+)\s*(.*)/);

        # Remove any .s to make things easier
        $code =~ s/[^\d]//g;

        # Find the parent code by removing the last digit
        my $parentcode= $code;
        $parentcode =~ s/\d$//;
        if($parentcode) {
            $parentcode = "$niv1 $parentcode";
        } else {
            $parentcode = $niv1;
        }
        #Convert to xxx.y format
        $parentcode =~ s/(\d)(\d)$/$1.$2/;

        my $parent = $parents_by_id{$parentcode};
        unless($parent) {
            die "No parent for id '$niv1 $code'\n";
        }

        my ($id) = ($code =~ /(\d)$/);

        # Convert to xxx.y format:
        $code =~ s/(\d)(\d)$/$1.$2/;

        push(@codes, {id => "$niv1 $code", idcode => $id, title => $rest, parent => $parent});


        $parents_by_id{"$niv1 $code"} = $codes[-1];
    }
}
close(FH);

my $basepath = '/hovedregisteret/';

my $basedoc = $obvius->lookup_document($basepath);

for(@codes) {
    my $parentdoc;
    if(my $p = $_->{parent}) {
        $parentdoc = $p->{document};
    } else {
        $parentdoc = $basedoc;
    }
    unless($parentdoc) {
        print STDERR Dumper($_);
        die "No parent found";
    }

    my $fields = new Obvius::Data;

    $fields->param('title' => $_->{title});
    $fields->param('short_title' => $_->{title});
    $fields->param('cat_id' => $_->{idcode});

    my $name = lc($_->{idcode});
    $name =~ s/[^\w]/_/g;

    print STDERR "Creating '" . $_->{id} . ", '" . $_->{title} . "'\n";

    my $vdoc = create_document($obvius, $parentdoc, $name, 23, $fields);
    publish_vdoc($obvius, $vdoc, new Obvius::Data);

    my $doc = $obvius->get_doc_by_id($vdoc->DocId);
    $_->{document} = $doc;

}


sub create_document {
    my ($obvius, $parent, $name, $doctypenr, $fields, %options) = @_;


    my $docfields = new Obvius::Data;

    my $doctype = $obvius->get_doctype_by_id($doctypenr);

    # Default values
    for(keys %{$doctype->{FIELDS}}) {
        my $default_value = $doctype->{FIELDS}->{$_}->{DEFAULT_VALUE};
        $docfields->param($_ => $default_value)  if(defined($default_value));
    }

    # Passed on fields
    for($fields->param) {
        $docfields->param($_ => $fields->param($_));
    }

    # Docdate
    unless($options{no_auto_docdate}) {
        $docfields->param(docdate => strftime('%Y-%m-%d 00:00:00', localtime));
    }

    my $language = $options{language} || 'da';
    my $doctypeid = $doctype->Id;
    my $ownerid = $options{ownerid} || 1;
    my $groupid = $options{groupid} || 1;


    my $create_error;
    my $backup_user = $obvius->{USER};
    $obvius->{USER} = 'admin';
    my ($new_docid, $new_version) = $obvius->create_new_document($parent, $name, $doctypeid, $language, $docfields, $ownerid, $groupid, \$create_error);
    $obvius->{USER} = $backup_user;

    if($create_error) {
        print STDERR "Create error: $create_error\n";
        return undef;
    } else {
        my $new_doc = $obvius->get_doc_by_id($new_docid);
        my $new_vdoc = $obvius->get_version($new_doc, $new_version);
        return $new_vdoc;
    }
}

sub publish_vdoc {
    my ($obvius, $new_vdoc, $extra_fields) = @_;

        my $publish_error;

        # Start out with som defaults
        $obvius->get_version_fields($new_vdoc, 255, 'PUBLISH_FIELDS');

        # Set published
        my $publish_fields = $new_vdoc->publish_fields;

        $publish_fields->param(PUBLISHED => strftime('%Y-%m-%d %H:%M:%S', localtime));

        # Passed on fields
        for($extra_fields->param) {
            $publish_fields->param($_ => $extra_fields->param($_));
        }


        my $backup_user = $obvius->{USER};
        $obvius->{USER} = 'admin'; # XXX Don't try this at home
        $obvius->publish_version($new_vdoc, \$publish_error);
        $obvius->{USER} = $backup_user;

        if($publish_error) {
            print STDERR "Publish error: $publish_error\n";
            return undef;
        }

        return 1;
}
