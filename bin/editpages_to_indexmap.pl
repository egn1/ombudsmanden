#!/usr/bin/perl

use strict;
use warnings;

use locale;

use lib '/home/httpd/obvius/perl_blib';

use Data::Dumper;

use Obvius;
use Obvius::Config;
use HTML::Parser;

use XML::Simple;

my $log=Obvius::Log->new('notice');
my $conf = new Obvius::Config('ombud');
my $obvius = new Obvius($conf, undef, undef, undef, undef, undef, log=>$log);

my %special_fieldtypes = (
                            text => 'html',
                            textwupload => 'html',
                            appdata => 'upload',
                            plaintext => 'html'
                        );


#########################################################
#                                                       #
#                     Setup db set                      #
#                                                       #
#########################################################


my $dset = DBIx::Recordset->SetupObject(
                                        {
                                            '!DataSource' => $obvius->{DB},
                                            '!Table'      => 'doctypes',
                                        }
                                    );

my $eset = DBIx::Recordset->SetupObject(
                                        {
                                            '!DataSource' => $obvius->{DB},
                                            '!Table'      => 'editpages',
                                        }
                                    );

$dset->Search({'$order' => 'id'});

my @doctypes;
while(my $rec = $dset->Next) {
    push(@doctypes, $rec);
}
$dset->Disconnect;

my %existing_weights;

for(split(/\s*,\s*/, 'title,short_title,teaser,content,author,docdate,docref,contributors,source,keyword,myndighed,inst_type,geografisk,cat_hoved')) {
    $existing_weights{$_} = 100.0;
}


# If a file was specified on the command line, assume it's an existing map
# and parse it for weights:
if(my $existing_file = $ARGV[0]) {
    if(-r $existing_file) {
        my $data = XMLin($existing_file, forcearray => ['doctype', 'field'], keyattr=> []);
        for my $doctype (@{ $data->{doctype} || [] } ) {
            for my $editpage (@{ $doctype->{editpage} || [] }) {
                for my $field (@{ $editpage->{field} || [] }) {
                    $existing_weights{$doctype->{id} . "^" . $field->{name}} = $field->{weight};
                }
            }
        }
    }
}


print "<?xml version='1.0' encoding='ISO-8859-1' standalone='yes'?>\n";
print "<doctypes>\n";

my $blah = 0;
for(@doctypes) {
    my $doctype = $obvius->get_doctype_by_id($_->{id});

    my @editpages;
    $eset->Search({'$order' => 'doctypeid, page', doctypeid => $_->{id} });
    while (my $rec = $eset->Next) {
        push(@editpages, $rec);
    }

    next unless(scalar(@editpages));

    print "<doctype>\n";
    print "  <name>" . $_->{name} . "</name>\n";
    print "  <id>" . $_->{id} . "</id>\n";

    for my $editpage (@editpages) {
        print "  <editpage>\n";
        print "    <title>" . $editpage->{title} . "</title>\n";
        print "    <page>" . $editpage->{page} . "</page>\n";
        my $fields = parse_fieldlist($editpage->{fieldlist}) || [];
        for(@$fields) {
            my $fieldspec = $obvius->get_fieldspec($_->{name}, $doctype);
            my $fieldtype = $fieldspec->param('fieldtype');
            print "    <field>\n";
            print "      <title>" . $_->{title} . "</title>\n";
            print "      <name>" . $_->{name} . "</name>\n";
            print "      <indextype>" . ($special_fieldtypes{$fieldtype->param('name')} || 'normal') . "</indextype>\n";
            print "      <binary>" . $fieldtype->param('bin') . "</binary>\n";
            my $weight = $existing_weights{$editpage->{doctypeid} . "^" . $_->{name}} || $existing_weights{$_->{name}} || 0.0;
            print "      <weight>" . sprintf('%.2f', $weight) . "</weight>\n";
            print "    </field>\n";
        }
        print "  </editpage>\n";
    }

    print "</doctype>\n";

}

print "</doctypes>\n";


sub parse_fieldlist {
    my $fieldlist = shift || '';

    my @lines = split("\n", $fieldlist);

    my @result;
    for(@lines) {
        next unless(/^([^;]+);?/);
        my $text = $1;
        my ($fieldname, $title) = ($text =~ m!^(\S+)\s+(.*)$!);
        if($fieldname and $title) {
            push(@result, {name => $fieldname, title => $title});
        }
    }

    return \@result;
}