UPDATE vfields SET text_value = "Hovedstaden" WHERE name="geografisk" AND text_value = "Bornholm";
UPDATE vfields SET text_value = "Hovedstaden" WHERE name="geografisk" AND text_value = "Stork�benhavn";
UPDATE vfields SET text_value = "Hovedstaden" WHERE name="geografisk" AND text_value = "Frederiksborg";

UPDATE vfields SET text_value = "Syddanmark" WHERE name="geografisk" AND text_value = "Fyn";
UPDATE vfields SET text_value = "Syddanmark" WHERE name="geografisk" AND text_value = "S�nderjylland";
UPDATE vfields SET text_value = "Syddanmark" WHERE name="geografisk" AND text_value = "Vejle";
UPDATE vfields SET text_value = "Syddanmark" WHERE name="geografisk" AND text_value = "Ribe";

# No change for Nordjylland

UPDATE vfields SET text_value = "Midtjylland" WHERE name="geografisk" AND text_value = "Ringk�bing";
UPDATE vfields SET text_value = "Midtjylland" WHERE name="geografisk" AND text_value = "Viborg";
UPDATE vfields SET text_value = "Midtjylland" WHERE name="geografisk" AND text_value = "�rhus";

UPDATE vfields SET text_value = "Sj�lland" WHERE name="geografisk" AND text_value = "Roskilde";
UPDATE vfields SET text_value = "Sj�lland" WHERE name="geografisk" AND text_value = "Storstr�m";
UPDATE vfields SET text_value = "Sj�lland" WHERE name="geografisk" AND text_value = "Vestsj�lland";

# No change for F�r�erne

# No change for Gr�nland
