#!/usr/bin/perl

use strict;
use warnings;

use Obvius;
use Obvius::Config;

use Data::Dumper;

my $conf = new Obvius::Config('ombud');
my $obvius = new Obvius($conf);

my $dbh = $obvius->dbh;

my $existing_docid = $ARGV[0];
my $new_docid = $ARGV[1];

unless(
    $existing_docid &&
    $existing_docid =~ m!^\d+$! &&
    $new_docid &&
    $new_docid =~ m!^\d+$!
    ) {
    print "Usage: $0 <docid-of-existing-subscription-area> <docid-of-new-subscription-area>\n";
    print "\n";
    print "All subscribers of the existing area will automatically be subscribed to the new area\n";
    exit 1;
}

my $edoc = $obvius->get_doc_by_id($existing_docid) or die "No doc with id $existing_docid";
my $evdoc = $obvius->get_public_version($edoc) || $obvius->get_latest_version($edoc);
die "The specified existing area is not subscribeable" unless($obvius->get_version_field($evdoc, 'subscribeable'));

my $ndoc = $obvius->get_doc_by_id($new_docid) or die "No doc with id $new_docid";
my $nvdoc = $obvius->get_public_version($ndoc) || $obvius->get_latest_version($ndoc);
die "The specified new area is not subscribeable" unless($obvius->get_version_field($evdoc, 'subscribeable'));

my $sth = $dbh->prepare(
    "SELECT subscribers.name, subscribers.id " .
    "FROM subscribers " .
    "LEFT JOIN subscriptions AS new ON (subscribers.id = new.subscriber AND new.docid = ?), " .
    "subscriptions AS existing " .
    "WHERE subscribers.id = existing.subscriber " .
    "AND existing.docid = ? " .
    "AND new.docid IS NULL"
    );
$sth->execute($new_docid, $existing_docid);

my $insert_sth = $dbh->prepare("INSERT INTO subscriptions (subscriber, docid) VALUES (?, ?)");

while(my $rec = $sth->fetchrow_hashref) {
    print "Auto-registering " . $rec->{name} . "\n";
    $insert_sth->execute($rec->{id}, $new_docid);
}
