#!/usr/bin/perl

use strict;
use warnings;

use Obvius::Hostmap;

# Don't buffer output or we'll get a deadlock:
$| = 1;

#our $debug = 0;

my $map_file = '/var/www/www.ombudsmanden.dk/conf/hostmap.conf';
my $roothost_file = '/var/www/www.ombudsmanden.dk/conf/roothost.conf';

my $roothost;

# Read roothost from file:
open(FH, $roothost_file) or die "Couldn't open roothost file";
my $line = <FH>;
close(FH);

($roothost) = ($line =~ m!:([^\]]+)!);

die "Couldn't read roothost from $roothost_file" unless($roothost);

my $reversemap = Obvius::Hostmap->create_hostmap( $map_file, $roothost );

# Read the lines sent over by apache. Accepts two formats,
# one being just an URI, and the other being the old http_host
# and the URI joined together by an &-char, like this:
#
#   %{HTTP_HOST}&%{URI}
#
# When a HTTP_HOST is used the script will return NULL if the
# reversemap host found is the same as the old host.
#
while(my $uri = <STDIN>) {
    my $old_host = '';
    my $new_uri;
    my $new_host;
    my $subsiteuri = '/';

    chomp($uri);

    if($uri =~ s!^([^&]+)&!!) {
        $old_host = $1;
    }

    ($new_uri, $new_host, $subsiteuri, undef)  = $reversemap->translate_uri($uri, $old_host);

    if($new_host and $new_host ne $old_host) {
        # Redir to ssl site directly:
        $new_uri =~ s!^http:!https:! if($new_host =~ m!^ssl\.!);
        print "$new_uri\n";
    } else {
        print "NULL\n";
    }
}
