#!/usr/bin/perl 
local $| = 1;

my $base = 5000;
my $number_of_servers = 1;
my $port = 0;
open F, ">/tmp/debug";

while(<STDIN>) {
     chomp;
     $port += 1;
     $port %= $number_of_servers;
     my $real_port = $base + $port;
     my $str = "http://localhost:$real_port/$_\n";
     print F $str;
     print $str;
}
