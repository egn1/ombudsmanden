#!/usr/bin/perl

use strict;
use warnings;

use lib '/var/www/obvius/perl';
use lib '/var/www/www.ombudsmanden.dk/perl';

use Obvius::Config;
use WebObvius::Rewriter;
use WebObvius::Rewriter::ObviusRules;
use Ombudsmanden::Rewriter::HTTPS;
use Ombudsmanden::Rewriter::SubSites;

my $config = new Obvius::Config('ombud');
#$config->param('mysqlcache_querystring_mapper' => new KU::QueryStringMapper());
my $rewriter = new WebObvius::Rewriter($config,
    debug => $config->param('debug_rewrites')
);

$| = 1;

# Mangle STDOUT to STDERR so we don't accidentically print to it from the
# modules.
open(REALSTDOUT, '>&', STDOUT);
open(STDOUT, '>&', STDERR);

select REALSTDOUT; $| = 1;
select STDOUT; $| = 1;

$rewriter->add_rewriters(
    new WebObvius::Rewriter::ObviusRules::StaticDocs,
    new WebObvius::Rewriter::ObviusRules::Navigator,
    new WebObvius::Rewriter::ObviusRules::SubSites,
    new WebObvius::Rewriter::ObviusRules::LowerCaser,
    new WebObvius::Rewriter::ObviusRules::MysqlCache,
);

while (my $in = <>) {
    chomp ($in);
    my $line = 'NULL';
    eval { $line = $rewriter->rewrite($in); };
    if($@) {
        warn $@;
        $line = 'NULL';
    }
    if($line =~ m!\n!) {
        my @t = localtime;
        print STDERR sprintf("[%04d-%02d-%02d %02d:%02d:%02d] ", $t[5] + 1900, $t[4] + 1, $t[3], $t[2], $t[1], $t[0]);
        print STDERR "Linebreak found in output from rewrite script: ";
        print STDERR "'$line', with input: $in\n";
        $line =~ s!\n!!g;
    }
    print REALSTDOUT $line . "\n";
}
