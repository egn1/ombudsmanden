#!/usr/bin/perl

use strict;
use warnings;

use lib '/var/www/www.ombudsmanden.dk/perl';

use Obvius::Config;
use Obvius::Hostmap;
use Obvius::Log::Apache;

use WebObvius::Site::Mason;
use WebObvius::Access;
use WebObvius::Admin;

use Ombudsmanden::MasonCommands;

use Ombudsmanden::Cache::HovedKategori;

use Ombudsmanden::HttpsLinksFilter;
use Ombudsmanden::Nyhedsbrev;
use Ombudsmanden::Search;

package Ombudsmanden::Site::Common;
our @ISA = qw( WebObvius::Site::Mason WebObvius::Access );

package Ombudsmanden::Site::Public;
our @ISA = qw( WebObvius::Site::Mason WebObvius::Access );

package Ombudsmanden::Site::Admin;
our @ISA = qw( WebObvius::Site::Mason WebObvius::Admin );

# Namespace for running Mason seperately for mason-cache handling:
package Ombudsmanden::Site::Admin::CacheHandling;
our ( $VERSION ) = '$Revision: 1.3.2.2 $ ' =~ /\$Revision:\s+([^\s]+)/;

package Ombudsmanden::Site;

print STDERR "Starting Ombudsmanden...\n";

my $obvius_config=new Obvius::Config('ombud');

if(my $solr_url = $obvius_config->param('solr_base_url')) {
    Ombudsmanden::Search->set_solr_core_url($solr_url);
}

if($obvius_config->param('use_source_perl_modules')) {
    for my $libpath (qw(
        /var/www/obvius/perl
        /var/www/www.ombudsmanden.dk/perl
    )) {
	unshift(@INC, $libpath) unless grep { $_ eq $libpath } @INC;
    }
}

my $prototype_obvius = Obvius->new($obvius_config)
    or die "Cannot instantiate Obvius\n";

Ombudsmanden::HttpsLinksFilter->initialize($prototype_obvius);

# Initialise newsletter module, so global variables are set in it
Ombudsmanden::Nyhedsbrev->new($prototype_obvius);
    
my $sitename = 'www.ombudsmanden.dk';

my $globalbase ="/var/www";
my $base = "$globalbase/$sitename";
my $log = new Obvius::Log::Apache;

$obvius_config-> param('roothost',
        $obvius_config-> param('roothost') ||
        $obvius_config-> read_roothost_conf( "$base/conf/roothost.conf") ||
        $sitename
);

$obvius_config-> param('hostmap',
        Obvius::Hostmap-> new(
                "$base/conf/hostmap.conf",
                $obvius_config-> param('roothost')
        )
);

my %ombud_shared_settings = (
    obvius_args => {
        fieldspecs     => $prototype_obvius->{FIELDSPECS},
        fieldtypes     => $prototype_obvius->{FIELDTYPES},
        doctypes       => $prototype_obvius->{DOCTYPES},
    },
    debug => $obvius_config->Debug,
    benchmark => $obvius_config->Benchmark,
    obvius_config => $obvius_config,
    log => $log,
    sitename => $sitename,
    base => $base,
    search_words_log => "$base/htdig/log/search_words.log",
);

my $subsite='';
our $Common = Ombudsmanden::Site::Common->new(
    %ombud_shared_settings,
    comp_root=>[
                [docroot  =>"$base/docs"],
                [sitecomp =>"$base/mason/common"],
                [globalcommoncomp =>"$globalbase/obvius/mason/common"],
               ],
    site => 'common',
    edit_sessions=> "$base/var/edit_sessions",
    out_method => \$subsite,
    cache_directory => "$base/var",
    log => $log
);

my $publicsite='';
our $Public = Ombudsmanden::Site::Public->new(
    %ombud_shared_settings,
    out_method => \$publicsite,
    cache_directory => "$base/var",
    site => 'public',
    webobvius_cache_directory=>"$base/var/document_cache",
    webobvius_cache_index=>"$base/var/document_cache.txt",
    post_max => 4*1024*1024,
    subsite => $Common,
    comp_root=>[
        [docroot  =>"$base/docs"],
        [sitecomp =>"$base/mason/public"],
        [globalpubliccomp =>"$globalbase/obvius/mason/public"],
        [commoncomp => "$base/mason/common"],
        [globalcommoncomp =>"$globalbase/obvius/mason/common"],
    ],
);

our $Admin = Ombudsmanden::Site::Admin->new(
    %ombud_shared_settings,
    webobvius_cache_directory=>"$base/var/document_cache",
    webobvius_cache_index=>"$base/var/document_cache.txt",
    synonyms_file=>"$base/htdig/common/synonyms",
    subsite => $Common,
    site => 'admin',
    edit_sessions=> "$base/var/edit_sessions",
    is_admin => 1,
    comp_root=>[
                [docroot  =>"$base/docs"],
                [sitecomp =>"$base/mason/admin"],
                [admincomp=>"$globalbase/obvius/mason/admin"],
                [commoncomp => "$base/mason/common"],
                [globalcommoncomp =>"$globalbase/obvius/mason/common"],
               ],
);

# Use basic HTML escaping since we have mixed iso-8859-1 and utf8 data >_<
if($HTML::Mason::VERSION >= 1.14) {
    $Common->{handler}->interp->set_escape( h => \&HTML::Mason::Escapes::basic_html_escape );
    $Public->{handler}->interp->set_escape( h => \&HTML::Mason::Escapes::basic_html_escape );
    $Admin->{handler}->interp->set_escape( h => \&HTML::Mason::Escapes::basic_html_escape );
}

require "Ombudsmanden/CatalystUtils.pm";

# References class created earlier in this file
Ombudsmanden::CatalystUtils->register_site('ombud' => 'Ombudsmanden::Site');

1;
